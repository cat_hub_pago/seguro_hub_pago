/**
 * FopLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cencosud.cl.paymentHub.fop;

public class FopLocator extends org.apache.axis.client.Service implements cencosud.cl.paymentHub.fop.Fop {

    public FopLocator() {
    }


    public FopLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FopLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FopServicePort
    private java.lang.String FopServicePort_address = "http://pagos.qa.cencosud.com:80/payment_hub/services/soap/v2/FopService";

    public java.lang.String getFopServicePortAddress() {
        return FopServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FopServicePortWSDDServiceName = "FopServicePort";

    public java.lang.String getFopServicePortWSDDServiceName() {
        return FopServicePortWSDDServiceName;
    }

    public void setFopServicePortWSDDServiceName(java.lang.String name) {
        FopServicePortWSDDServiceName = name;
    }

    public cencosud.cl.paymentHub.fop.FopService getFopServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FopServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFopServicePort(endpoint);
    }

    public cencosud.cl.paymentHub.fop.FopService getFopServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cencosud.cl.paymentHub.fop.FopServicePortBindingStub _stub = new cencosud.cl.paymentHub.fop.FopServicePortBindingStub(portAddress, this);
            _stub.setPortName(getFopServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFopServicePortEndpointAddress(java.lang.String address) {
        FopServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cencosud.cl.paymentHub.fop.FopService.class.isAssignableFrom(serviceEndpointInterface)) {
                cencosud.cl.paymentHub.fop.FopServicePortBindingStub _stub = new cencosud.cl.paymentHub.fop.FopServicePortBindingStub(new java.net.URL(FopServicePort_address), this);
                _stub.setPortName(getFopServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FopServicePort".equals(inputPortName)) {
            return getFopServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://cl.cencosud/paymentHub/services", "Fop");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://cl.cencosud/paymentHub/services", "FopServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FopServicePort".equals(portName)) {
            setFopServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
