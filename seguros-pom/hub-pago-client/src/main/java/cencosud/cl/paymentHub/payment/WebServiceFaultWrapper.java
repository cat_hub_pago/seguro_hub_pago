/**
 * WebServiceFaultWrapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cencosud.cl.paymentHub.payment;

public class WebServiceFaultWrapper  extends org.apache.axis.AxisFault  implements java.io.Serializable {
    private java.lang.String xmlError;

    public WebServiceFaultWrapper() {
    }

    public WebServiceFaultWrapper(
           java.lang.String xmlError) {
        this.xmlError = xmlError;
    }


    /**
     * Gets the xmlError value for this WebServiceFaultWrapper.
     * 
     * @return xmlError
     */
    public java.lang.String getXmlError() {
        return xmlError;
    }


    /**
     * Sets the xmlError value for this WebServiceFaultWrapper.
     * 
     * @param xmlError
     */
    public void setXmlError(java.lang.String xmlError) {
        this.xmlError = xmlError;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WebServiceFaultWrapper)) return false;
        WebServiceFaultWrapper other = (WebServiceFaultWrapper) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.xmlError==null && other.getXmlError()==null) || 
             (this.xmlError!=null &&
              this.xmlError.equals(other.getXmlError())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXmlError() != null) {
            _hashCode += getXmlError().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WebServiceFaultWrapper.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://cl.cencosud/paymentHub/services", "webServiceFaultWrapper"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xmlError");
        elemField.setXmlName(new javax.xml.namespace.QName("", "xmlError"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }


    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, this);
    }
}
