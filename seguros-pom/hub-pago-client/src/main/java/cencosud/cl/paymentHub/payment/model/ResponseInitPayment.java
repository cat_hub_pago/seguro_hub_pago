package cencosud.cl.paymentHub.payment.model;

/**
 * 
 * @author Virtual
 *
 */
public class ResponseInitPayment {
	private String paymentId;
	private String paymentUrl;

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	@Override
	public String toString() {
		return "ResponseInitPayment [paymentId=" + paymentId + ", paymentUrl="
				+ paymentUrl + "]";
	}

}
