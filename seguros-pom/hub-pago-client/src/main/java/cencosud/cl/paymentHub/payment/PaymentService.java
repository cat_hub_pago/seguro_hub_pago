/**
 * PaymentService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cencosud.cl.paymentHub.payment;

public interface PaymentService extends java.rmi.Remote {
    public java.lang.String get(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper;
    public java.lang.String getByOrder(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper;
    public java.lang.String init(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper;
    public java.lang.String confirm(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper;
    public java.lang.String confirmByOrder(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper;
    public java.lang.String nullify(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper;
    public java.lang.String nullifyByOrder(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper;
}
