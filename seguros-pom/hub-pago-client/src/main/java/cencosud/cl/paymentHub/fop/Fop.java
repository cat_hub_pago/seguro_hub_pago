/**
 * Fop.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cencosud.cl.paymentHub.fop;

public interface Fop extends javax.xml.rpc.Service {
    public java.lang.String getFopServicePortAddress();

    public cencosud.cl.paymentHub.fop.FopService getFopServicePort() throws javax.xml.rpc.ServiceException;

    public cencosud.cl.paymentHub.fop.FopService getFopServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
