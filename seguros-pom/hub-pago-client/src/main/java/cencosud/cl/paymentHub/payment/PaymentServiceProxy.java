package cencosud.cl.paymentHub.payment;

public class PaymentServiceProxy implements cencosud.cl.paymentHub.payment.PaymentService {
  private String _endpoint = null;
  private cencosud.cl.paymentHub.payment.PaymentService paymentService = null;
  
  public PaymentServiceProxy() {
    _initPaymentServiceProxy();
  }
  
  public PaymentServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initPaymentServiceProxy();
  }
  
  private void _initPaymentServiceProxy() {
    try {
      paymentService = (new cencosud.cl.paymentHub.payment.PaymentLocator()).getPaymentServicePort();
      if (paymentService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)paymentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)paymentService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (paymentService != null)
      ((javax.xml.rpc.Stub)paymentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cencosud.cl.paymentHub.payment.PaymentService getPaymentService() {
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService;
  }
  
  public java.lang.String get(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.get(request);
  }
  
  public java.lang.String getByOrder(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.getByOrder(request);
  }
  
  public java.lang.String init(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.init(request);
  }
  
  public java.lang.String confirm(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.confirm(request);
  }
  
  public java.lang.String confirmByOrder(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.confirmByOrder(request);
  }
  
  public java.lang.String nullify(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.nullify(request);
  }
  
  public java.lang.String nullifyByOrder(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.payment.WebServiceFaultWrapper{
    if (paymentService == null)
      _initPaymentServiceProxy();
    return paymentService.nullifyByOrder(request);
  }
  
  
}