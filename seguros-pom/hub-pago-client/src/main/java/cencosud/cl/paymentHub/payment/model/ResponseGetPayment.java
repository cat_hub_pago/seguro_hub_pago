package cencosud.cl.paymentHub.payment.model;

/**
 * 
 * @author Virtual
 * 
 */
public class ResponseGetPayment {

	private String status;
	private String amount;
	private String currency;
	private String authorizationCode;
	private String authorizationDate;
	private String paymentType;
	private String installmentsNumber;
	private String sessionId;
	private String accountingDate;
	private String transactionDate;
	private String urlRedirection;
	private String paymentTypeCode;
	private String responseCode;
	private String commerceCode;
	private String buyOrder;
	private String cardNumber;
	private String sharesAmount;
	private String sharesNumber;
	private String paymentId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getBuyOrder() {
		return buyOrder;
	}

	public void setBuyOrder(String buyOrder) {
		this.buyOrder = buyOrder;
	}

	public String getAuthorizationDate() {
		return authorizationDate;
	}

	public void setAuthorizationDate(String authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getInstallmentsNumber() {
		return installmentsNumber;
	}

	public void setInstallmentsNumber(String installmentsNumber) {
		this.installmentsNumber = installmentsNumber;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAccountingDate() {
		return accountingDate;
	}

	public void setAccountingDate(String accountingDate) {
		this.accountingDate = accountingDate;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getUrlRedirection() {
		return urlRedirection;
	}

	public void setUrlRedirection(String urlRedirection) {
		this.urlRedirection = urlRedirection;
	}

	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}

	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getCommerceCode() {
		return commerceCode;
	}

	public void setCommerceCode(String commerceCode) {
		this.commerceCode = commerceCode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getSharesAmount() {
		return sharesAmount;
	}

	public void setSharesAmount(String sharesAmount) {
		this.sharesAmount = sharesAmount;
	}

	public String getSharesNumber() {
		return sharesNumber;
	}

	public void setSharesNumber(String sharesNumber) {
		this.sharesNumber = sharesNumber;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	@Override
	public String toString() {
		return "ResponseGetPayment [status=" + status + ", amount=" + amount
				+ ", currency=" + currency + ", authorizationCode="
				+ authorizationCode + ", authorizationDate="
				+ authorizationDate + ", paymentType=" + paymentType
				+ ", installmentsNumber=" + installmentsNumber + ", sessionId="
				+ sessionId + ", accountingDate=" + accountingDate
				+ ", transactionDate=" + transactionDate + ", urlRedirection="
				+ urlRedirection + ", paymentTypeCode=" + paymentTypeCode
				+ ", responseCode=" + responseCode + ", commerceCode="
				+ commerceCode + ", buyOrder=" + buyOrder + ", cardNumber="
				+ cardNumber + ", sharesAmount=" + sharesAmount
				+ ", sharesNumber=" + sharesNumber + ", paymentId=" + paymentId
				+ "]";
	}

}
