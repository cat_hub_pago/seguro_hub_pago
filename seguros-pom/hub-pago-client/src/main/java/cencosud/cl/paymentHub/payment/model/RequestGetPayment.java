package cencosud.cl.paymentHub.payment.model;

/**
 * 
 * @author Virtual
 * 
 */
public class RequestGetPayment {

	private String paymentId;

	public RequestGetPayment() {
		super();
	}

	public RequestGetPayment(String paymentId) {
		super();
		this.paymentId = paymentId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	@Override
	public String toString() {
		return "RequestGetPayment [paymentId=" + paymentId + "]";
	}

}
