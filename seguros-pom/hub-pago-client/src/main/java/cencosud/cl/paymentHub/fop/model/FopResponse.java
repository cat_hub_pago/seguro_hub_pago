package cencosud.cl.paymentHub.fop.model;

public class FopResponse {

	private String id;
	private String name;
	private String descriptionHtml;
	private String logoUrl;
	private String useDiscountAmount;
	private String isLoyalty;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescriptionHtml() {
		return descriptionHtml;
	}

	public void setDescriptionHtml(String descriptionHtml) {
		this.descriptionHtml = descriptionHtml;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getUseDiscountAmount() {
		return useDiscountAmount;
	}

	public void setUseDiscountAmount(String useDiscountAmount) {
		this.useDiscountAmount = useDiscountAmount;
	}

	public String getIsLoyalty() {
		return isLoyalty;
	}

	public void setIsLoyalty(String isLoyalty) {
		this.isLoyalty = isLoyalty;
	}
	
	

	public FopResponse(String id, String name, String descriptionHtml,
			String logoUrl, String useDiscountAmount, String isLoyalty) {
		super();
		this.id = id;
		this.name = name;
		this.descriptionHtml = descriptionHtml;
		this.logoUrl = logoUrl;
		this.useDiscountAmount = useDiscountAmount;
		this.isLoyalty = isLoyalty;
	}
	

	public FopResponse() {
	}
	

	@Override
	public String toString() {
		return "FopResponse [id=" + id + ", name=" + name
				+ ", descriptionHtml=" + descriptionHtml + ", logoUrl="
				+ logoUrl + ", useDiscountAmount=" + useDiscountAmount
				+ ", isLoyalty=" + isLoyalty + "]";
	}

}
