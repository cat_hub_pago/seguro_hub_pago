package cencosud.cl.paymentHub.fop.service;

import java.util.List;

import cencosud.cl.paymentHub.fop.model.FopResponse;


/**
 * 
 * @author Virtual
 * 
 */
public interface IFopServiceClient {

	public List<FopResponse> getWebpayPaymentTypeByStoreNumber(String urlWsdl, String storeNumber);
}
