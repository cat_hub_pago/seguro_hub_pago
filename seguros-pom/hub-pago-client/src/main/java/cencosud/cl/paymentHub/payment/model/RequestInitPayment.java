package cencosud.cl.paymentHub.payment.model;



import java.io.Serializable;

public class RequestInitPayment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5933899258132132334L;
	private String applicationId;
	private String commerceSessionId;
	private String orderId;
	private String returnUrl;
	private ClientName clientName = new ClientName();
	private String amount;
	private String currency;
	private String discountedAmount;
	private String fopId;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getCommerceSessionId() {
		return commerceSessionId;
	}

	public void setCommerceSessionId(String commerceSessionId) {
		this.commerceSessionId = commerceSessionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public ClientName getClientName() {
		return clientName;
	}

	public void setClientName(ClientName clientName) {
		this.clientName = clientName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getFopId() {
		return fopId;
	}

	public void setFopId(String fopId) {
		this.fopId = fopId;
	}
	
	

	public String getDiscountedAmount() {
		return discountedAmount;
	}

	public void setDiscountedAmount(String discountedAmount) {
		this.discountedAmount = discountedAmount;
	}

	@Override
	public String toString() {
		return "RequestPayment [applicationId=" + applicationId
				+ ", commerceSessionId=" + commerceSessionId + ", orderId="
				+ orderId + ", returnUrl=" + returnUrl + ", clientName="
				+ clientName + ", amount=" + amount + ", currency=" + currency
				+ ", discountedAmount=" + discountedAmount + ", fopId=" + fopId
				+ "]";
	}
	
	

}
