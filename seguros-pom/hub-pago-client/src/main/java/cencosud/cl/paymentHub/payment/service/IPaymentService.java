package cencosud.cl.paymentHub.payment.service;

import cencosud.cl.paymentHub.payment.model.RequestInitPayment;
import cencosud.cl.paymentHub.payment.model.ResponseGetPayment;
import cencosud.cl.paymentHub.payment.model.ResponseInitPayment;

/**
 * 
 * @author Virtual
 * 
 */
public interface IPaymentService {

	public ResponseInitPayment initPayment(String urlWsdl, RequestInitPayment request);

	public ResponseGetPayment getPaymentStatusByPaymentId(String urlWsdl, String  paymentId);

}
