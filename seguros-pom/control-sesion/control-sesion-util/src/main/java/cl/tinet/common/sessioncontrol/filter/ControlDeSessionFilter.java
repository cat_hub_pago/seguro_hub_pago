package cl.tinet.common.sessioncontrol.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.sessioncontrol.util.ControlDeSesionUtil;
import cl.tinet.common.sessioncontrol.wrapper.HttpServletRequestPersistente;

/**
 * Filtro de control de sesi�n. Se encarga de reemplazar la instancia de sesi�n
 * especificada por una personalizada, especificada por configuraci�n.
 * 
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 02-08-2010
 */
public class ControlDeSessionFilter implements Filter {

    /**
     * Llave de par�metro de activaci�n/desactivaci�n en el archivo web.xml.
     * <p>
     * En caso de no especificarse el valor de este par�metro en el archivo
     * web.xml se asume que el filtro se encuentra activado.</p>
     * <p>
     * El valor de la llave de activaci�n es 
     * <b>{@value #LLAVE_ACTIVACION}</b>.</p>
     */
    public static final String LLAVE_ACTIVACION =
        "cl.tinet.common.sessioncontrol.filter.ACTIVADO";

    /**
     * Llave de configuraci�n en web.xml del par�metro de inicializaci�n
     * del filtro que permite establecer el nombre de la aplicaci�n a quien
     * corresponde la sesi�n.
     */
    public static final String LLAVE_NOMBRE_APLICACION =
        "cl.tinet.common.sessioncontrol.filter.NOMBRE_APLICACION";

    /**
     * Llave de configuraci�n en web.xml del par�metro de inicializaci�n
     * del filtro que permite establecer el listado de excepciones a la
     * persistencia en base de datos.
     */
    public static final String LLAVE_EXCEPCIONES =
        "cl.tinet.common.sessioncontrol.filter.EXCEPCIONES";

    /**
     * TODO Describir atributo LLAVE_CONTEXTO_BASE.
     */
    public static final String LLAVE_CONTEXTO_BASE =
        "cl.tinet.common.sessioncontrol.filter.CONTEXTO_BASE";

    /**
     * Listado vac�o de excepciones de persistencia. Valor predeterminado
     * en caso de que no se especifique el par�metro en el archivo web.xml.
     */
    private static final List < String > SIN_ELEMENTOS =
        new ArrayList < String >();

    /**
     * Variable de acceso al log.
     */
    private static Log logger = LogFactory.getLog(ControlDeSessionFilter.class);

    /**
     * Indica si es que el filtro se encuentra o no activado en la aplicaci�n.
     */
    private boolean activado = true;

    /**
     * Nombre de aplicaci�n predeterminado en caso de que el par�metro de
     * inicio {@link #LLAVE_NOMBRE_APLICACION} no se haya especificado.
     */
    private String nombreAplicacion = "CSF_APP";

    /**
     * Nombres de los atributos que no deben ser almacenados en la sesi�n
     * persistente debido a su reiterativo uso, con el fin de no perjudicar
     * el rendimiento de la aplicaci�n.
     */
    private List < String > excepciones = SIN_ELEMENTOS;

    /**
     * TODO Describir atributo contexto.
     */
    private String contextoBase;

    /**
     * Inicializa el filtro de control de sesi�n.
     * 
     * @see #LLAVE_ACTIVACION
     * @see #LLAVE_NOMBRE_APLICACION
     *
     * @param config datos de configuraci�n del filtro.
     */
    public void init(FilterConfig config) {
        String parametro = config.getInitParameter(LLAVE_ACTIVACION);
        if (parametro != null) {
            this.activado = Boolean.parseBoolean(parametro);
        }
        parametro = config.getInitParameter(LLAVE_NOMBRE_APLICACION);
        if (parametro != null) {
            this.nombreAplicacion = parametro;
        }
        parametro = config.getInitParameter(LLAVE_EXCEPCIONES);
        if (parametro != null) {
            List < String > listado = new ArrayList < String >();
            String[] elementos = parametro.split("\\s*,\\s*");
            for (int i = 0; i < elementos.length; i++) {
                listado.add(elementos[i]);
            }
            this.excepciones = listado;
        }
        this.contextoBase = config.getInitParameter(LLAVE_CONTEXTO_BASE);
        if (logger.isInfoEnabled()) {
            logger.info("Filtro de control de sesi�n inicializado.");
            logger.info("Nombre aplicacion: " + this.nombreAplicacion);
            logger.info("Filtro activado  : " + this.activado);
            logger.info("Excepciones      : " + this.excepciones);
            logger.info("Contexto base    : " + this.contextoBase);
        }
    }

    /**
     * Realiza el filtrado de la solicitud para el uso de la sesi�n
     * persistente.
     * <p>
     * En caso de que el filtro se encuentre activado el filtro reemplaza
     * la instancia de {@link HttpServletRequest} por una versi�n que se
     * encarga de utilizar la versi�n persistente de la sesi�n HTTP. La
     * clase request utilizada es {@link HttpServletRequestPersistente}.
     * </p>
     * @param req datos de la solicitud.
     * @param res datos de la respuesta.
     * @param chain cadena de filtros a ejecutar.
     * @throws IOException en caso de haber problemas generando la
     *          respuesta al usuario.
     * @throws ServletException en caso de ocurrir un error durante
     *          el procesamiento de la solicitud.
     */
    public void doFilter(ServletRequest req, ServletResponse res,
        FilterChain chain) throws IOException, ServletException {
        ServletRequest servletRequest = req;
        if (activado && (req instanceof HttpServletRequest)) {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;
            
            if(!(request.getRequestURI().contains("buscar-comunas") || 
				request.getRequestURI().contains("buscar-ciudades") )){
            	if (request instanceof HttpServletRequestPersistente) {
                    logger.debug("Request ya ha sido reemplazado. Ignorando.");
                } else {
                    String sid =
                        ControlDeSesionUtil.obtenerSID(this.nombreAplicacion,
                            request, response, this.contextoBase);
                    HttpServletRequestPersistente rp =
                        new HttpServletRequestPersistente(request,
                        this.nombreAplicacion, sid, this.excepciones);
                    HttpSession session = rp.getSession(false);
                    if (session == null) {
                        logger.debug("La sesion ha expirado. Sin datos.");
                        //Si la sesion expira, se a�ade el siguiente atributo.
                        request.setAttribute("STATE", "expirada");
                    }
                    servletRequest = rp;
                }
            }	      	
        }
        chain.doFilter(servletRequest, res);
    }

    /**
     * Destruye el filtro de reemplazo de sesi�n.
     */
    public void destroy() {
        logger.info("Filtro de control de sesi�n destruido.");
    }
}
