package cl.tinet.common.sessioncontrol.service;

import cl.tinet.common.sessioncontrol.config.SessionControlConfig;
import cl.tinet.common.sessioncontrol.dao.DefaultSesionDAOFactory;

import com.tinet.exceptions.system.SystemException;

/**
 * Factory del servicio de sesi�n a utilizar por la aplicaci�n.
 * <p>
 * Permite obtener la instancia concreta de filtro servicio de
 * persistencia de sesi�n a utilizar, esto es: DAO directo o
 * EJB.
 * </p>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 12, 2010
 */
public class SesionServiceFactory {

    /**
     * Nombre de clase servicio remota predeterminada.
     */
    private static final String REMOTE_CLASS_NAME =
        "cl.tinet.common.sessioncontrol.delegate.GestorDeSesionDelegate";

    /**
     * Llave de configuraci�n que permite determinar si la llamada al servicio
     * de persistencia es remota o no.
     */
    public static final String INVOCACION_REMOTA_KEY =
        "cl.tinet.common.sessioncontrol.INVOCACION_REMOTA";

    /**
     * �nica instancia del factory.
     */
    private static SesionServiceFactory instance = new SesionServiceFactory();

    /**
     * Indicador de si la llamada al servicio de persistencia es remota o no.
     */
    private boolean remoto;

    /**
     * Clase servicio remoto a utilizar (si remoto es activo).
     */
    private Class < ? > claseServicio;

    /**
     * Constructor sin argumentos. Se define privado para evitar instanciaci�n
     * directa.
     */
    private SesionServiceFactory() {
        this.remoto =
            SessionControlConfig.getInstance().isTrue(INVOCACION_REMOTA_KEY);
        if (remoto) {
            try {
                claseServicio = Class.forName(REMOTE_CLASS_NAME);
            } catch (ClassNotFoundException cnfe) {
                throw new SystemException(cnfe);
            }
        }
    }

    /**
     * Retorna la �nica instancia del factory del servicio de persistencia de
     * sesi�n.
     * 
     * @return �nica instancia de factory.
     */
    public static SesionServiceFactory getInstance() {
        return instance;
    }

    /**
     * Retorna el servicio de persistencia de sesi�n que corresponde a la
     * aplicaci�n seg�n configuraci�n.
     *
     * @return servicio de persistencia.
     */
    public SesionService getSesionService() {
        if (remoto) {
            try {
                return (SesionService) this.claseServicio.newInstance();
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InstantiationException ie) {
                throw new SystemException(ie);
            }
        }
        return DefaultSesionDAOFactory.getInstance().getSesionDAO();
    }
}
