package cl.tinet.common.sessioncontrol.wrapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import cl.tinet.common.sessioncontrol.service.SesionService;
import cl.tinet.common.sessioncontrol.service.SesionServiceFactory;
import cl.tinet.common.util.resource.ResourceLeakUtil;

/**
 * Implementaci�n concreta de {@link HttpSession} que persiste los datos
 * que se le establecen en base de datos.
 * 
 * FIXME Serializar y desserializar objetos en capa de presentacion.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 2, 2010
 */
public class HttpSessionPersistente implements HttpSession, Serializable {

    /**
     * Clase interna que permite enumerar los nombres de los atributos
     * almacenados en la sesi�n persistente.
     * <br/>
     * @author Roberto San Mart�n
     * @version 1.0
     * @created Sep 30, 2010
     */
    public static class Enumerator implements Enumeration < String > {

        /**
         * Elementos a enumerar.
         */
        private String[] elementos;

        /**
         * �ndice del pr�ximo elemento a retornar.
         */
        private int indice;

        /**
         * Constructor de la clase que recibe los datos a enumerar como
         * argumentos.
         * <br/>
         * @param valores datos a enumerar.
         */
        public Enumerator(String[] valores) {
            if (valores == null) {
                throw new NullPointerException("Debe especificar los valores.");
            }
            this.elementos = valores.clone();
        }

        /**
         * Indica si es que existen m�s elementos sobre los que enumerar.
         * <br/>
         * @return <code>true</code> si es que existen elementos por enumerar.
         */
        public boolean hasMoreElements() {
            return indice < elementos.length;
        }

        /**
         * Retorna el pr�ximo elemento a enumerar.
         * <br/>
         * @return pr�ximo elemento a enumerar.
         */
        public String nextElement() {
            return elementos[indice++];
        }
    }

    /**
     * Versi�n de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Nombre de la aplicaci�n.
     */
    private String applicationName;

    /**
     * Identificador de la sesi�n persistente.
     */
    private String sessionID;

    /**
     * Referencia a la sesi�n no persistente nativa.
     */
    private HttpSession session;

    /**
     * Listado de llaves que no deben ser persistidas y rescatadas
     * desde base de datos.
     */
    private List < String > excepciones;

    /**
     * Construye una nueva instancia de sesi�n persistente.
     * <p>
     * En caso de que la sesi�n ya exista en base de datos, actualiza la fecha
     * de �ltimo acceso. En caso contrario la crea como nueva. Si alguno de los
     * argumentos al constructor es <code>null</code> se lanza una
     * {@link NullPointerException} indicando el problema.
     * </p>
     * @param applicationName nombre de la aplicaci�n.
     * @param sessionID identificador de la sesi�n (largo 36 m�ximo).
     * @param listado listado de nombres de atributos que no deben
     *          ser almacenados y recuperados de forma persistente.
     * @param httpSession referencia a la sesi�n nativa.
     */
    public HttpSessionPersistente(String applicationName, String sessionID,
        List < String > listado, HttpSession httpSession) {
        if (applicationName == null) {
            throw new NullPointerException(
                "El ID de aplicaci�n no debe ser nulo.");
        }
        if (sessionID == null) {
            throw new NullPointerException("El ID de sesi�n no debe ser nulo.");
        }
        if (httpSession == null) {
            throw new NullPointerException("La sesion no debe ser nula.");
        }
        if (listado == null) {
            throw new NullPointerException("Las excepciones no deben ser null.");
        }
        if (httpSession instanceof HttpSessionPersistente) {
            throw new IllegalArgumentException(
                "La sesi�n ya se encuentra wrapeada.");
        }
        this.applicationName = applicationName;
        this.sessionID = sessionID;
        this.session = httpSession;
        this.excepciones = listado;
        SesionService svc = getSessionServide();
        try {
            svc.createSession(applicationName, sessionID);
        } finally {
            closeSvc(svc);
        }
    }

    /**
     * Returns the time when this session was created, measured in
     * milliseconds since midnight January 1, 1970 GMT.
     * <br/>
     * @return a long specifying when this session was created, expressed
     *          in milliseconds since 1/1/1970 GMT.
     */
    public long getCreationTime() {
        SesionService svc = getSessionServide();
        try {
            Date fechaCreacion = svc.getCreation(applicationName, sessionID);
            return fechaCreacion.getTime();
        } finally {
            closeSvc(svc);
        }
    }

    /**
     * Returns a string containing the unique identifier assigned to this
     * session. The identifier is assigned by the servlet container and is
     * implementation dependent.
     * <br/>
     * @return a string specifying the identifier assigned to this session.
     */
    public String getId() {
        return this.session.getId();
    }

    /**
     * Returns the last time the client sent a request associated with this
     * session, as the number of milliseconds since midnight January 1, 1970
     * GMT.
     * <p>
     * Actions that your application takes, such as getting or setting a value
     * associated with the session, do not affect the access time.
     * </p>
     * @return a long  representing the last time the client sent a request
     *          associated with this session, expressed in milliseconds since
     *          1/1/1970 GMT.
     */
    public long getLastAccessedTime() {
        SesionService svc = getSessionServide();
        try {
            return svc.getLastAccess(applicationName, sessionID).getTime();
        } finally {
            closeSvc(svc);
        }
    }

    /**
     * Returns the ServletContext to which this session belongs.
     * <br/>
     * @return The ServletContext object for the web application.
     */
    public ServletContext getServletContext() {
        return this.session.getServletContext();
    }

    /**
     * Specifies the time, in seconds, between client requests
     * before the servlet container will invalidate this session.
     * <p>
     * A negative time indicates the session should never timeout.
     * </p>
     * @param interval An integer specifying the number of seconds.
     */
    public void setMaxInactiveInterval(int interval) {
        this.session.setMaxInactiveInterval(interval);
    }

    /**
     * Returns the maximum time interval, in seconds, that the servlet
     * container will keep this session open between client accesses. After
     * this interval, the servlet container will invalidate the session.
     * <p>
     * The maximum time interval can be set with the setMaxInactiveInterval
     * method. A negative time indicates the session should never timeout.
     * </p>
     * @return an integer specifying the number of seconds this session
     *          remains open between client requests.
     */
    public int getMaxInactiveInterval() {
        return this.session.getMaxInactiveInterval();
    }

    /**
     * @deprecated As of Version 2.1, this method is deprecated and has no
     *          replacement. It will be removed in a future version of the
     *          Java Servlet API.
     */
    public HttpSessionContext getSessionContext() {
        return this.session.getSessionContext();
    }

    /**
     * Returns the object bound with the specified name in this session, or
     * null if no object is bound under the name.
     * <br/>
     * @param name a string specifying the name of the object.
     * @return the object with the specified name.
     */
    public Object getAttribute(String name) {
        Object valor = null;
        if (this.excepciones.contains(name)) {
            valor = this.session.getAttribute(name);
        } else {
            SesionService svc = getSessionServide();
            try {
                valor = svc.getAttribute(applicationName, sessionID, name);
            } finally {
                closeSvc(svc);
            }
        }
        return valor;
    }

    /**
     * @deprecated As of Version 2.2, this method is replaced by
     *          {@link #getAttribute(String)}
     * <br/>
     * @param name a string specifying the name of the object.
     * @return the object with the specified name.
     */
    public Object getValue(String name) {
        return getAttribute(name);
    }

    /**
     * Returns an Enumeration of String objects containing the
     * names of all the objects bound to this session.
     * <br/>
     * @return an Enumeration of String objects specifying the names of all
     *          the objects bound to this session.
     */
    public Enumeration < ? > getAttributeNames() {
        return new Enumerator(getNames());
    }

    /**
     * @deprecated As of Version 2.2, this method is replaced by
     * {@link #getAttributeNames()}.
     * <br/>
     * @return an array of String  objects specifying the names
     *          of all the objects bound to this session
     */
    public String[] getValueNames() {
        return getNames();
    }

    /**
     * Binds an object to this session, using the name specified.
     * <p>
     * If an object of the same name is already bound to the session, the
     * object is replaced. After this method executes, and if the new object
     * implements HttpSessionBindingListener, the container calls
     * HttpSessionBindingListener.valueBound. The container then notifies
     * any HttpSessionAttributeListeners in the web application.</p>
     * <p>
     * If an object was already bound to this session of this name that
     * implements HttpSessionBindingListener, its
     * HttpSessionBindingListener.valueUnbound method is called.</p>
     * <p>
     * If the value passed in is null, this has the same effect as calling
     * removeAttribute().
     * </p>
     * @param name the name to which the object is bound; cannot be null. 
     * @param value the object to be bound.
     */
    public void setAttribute(String name, Object value) {
        if (!this.excepciones.contains(name)) {
            SesionService svc = getSessionServide();
            try {
                svc.setAttribute(applicationName, sessionID, name, value);
            } finally {
                closeSvc(svc);
            }
        }
        this.session.setAttribute(name, value);
    }

    /**
     * @deprecated As of Version 2.2, this method is replaced by
     * {@link #setAttribute(String, Object)}.
     * <br/>
     * @param name the name to which the object is bound; cannot be null.
     * @param value the object to be bound; cannot be null.
     */
    public void putValue(String name, Object value) {
        setAttribute(name, value);
    }

    /**
     * Removes the object bound with the specified name from this session.
     * <p>
     * If the session does not have an object bound with the specified name,
     * this method does nothing. After this method executes, and if the object
     * implements HttpSessionBindingListener, the container calls
     * HttpSessionBindingListener.valueUnbound. The container then notifies
     * any HttpSessionAttributeListeners in the web application.
     * </p>
     * @param name the name of the object to remove from this session.
     */
    public void removeAttribute(String name) {
        SesionService svc = getSessionServide();
        try {
            svc.delAttribute(applicationName, sessionID, name);
        } finally {
            closeSvc(svc);
        }
    }

    /**
     * @deprecated As of Version 2.2, this method is replaced by
     * {@link #removeAttribute(String)}.
     * <br/>
     * @param name the name of the object to remove from this session.
     */
    public void removeValue(String name) {
        removeAttribute(name);
    }

    /**
     * Invalidates this session then unbinds any objects bound to it.
     */
    public void invalidate() {
        SesionService svc = getSessionServide();
        try {
            session.invalidate();
            svc.invalidate(applicationName, sessionID);
        } finally {
            closeSvc(svc);
        }
    }

    /**
     * Returns true if the client does not yet know about the session or if
     * the client chooses not to join the session.
     * <p>
     * For example, if the server used only cookie-based sessions, and the
     * client had disabled the use of cookies, then a session would be new on
     * each request.
     * </p>
     * @return if the server has created a session, but the client has not
     *          yet joined.
     */
    public boolean isNew() {
        return this.session.isNew();
    }

    /**
     * Obtiene implementaci�n de servicio de persistencia de sesi�n.
     *
     * @return implementaci�n concreta de servicio.
     */
    private SesionService getSessionServide() {
        return SesionServiceFactory.getInstance().getSesionService();
    }

    /**
     * Cierra el servicio de persistencia de sesi�n.
     * @param svc servicio a cerrar.
     */
    private void closeSvc(SesionService svc) {
        ResourceLeakUtil.close(svc);
    }

    /**
     * Retorna los nombres de los atributos en la sesi�n.
     * <p>
     * Incorpora tanto los nombres de atributos en sesi�n persistente como
     * los atributos en sesi�n no persistente.
     * </p>
     * @return todos los nombres de los atributos en sesi�n.
     */
    private String[] getNames() {
        SesionService svc = getSessionServide();
        try {
            List < String > nombres = new ArrayList < String >();
            String[] listP = svc.getAttributeNames(applicationName, sessionID);
            for (int i = 0; i < listP.length; i++) {
                nombres.add(listP[i]);
            }
            Enumeration < ? > listN = this.session.getAttributeNames();
            while (listN.hasMoreElements()) {
                nombres.add((String) listN.nextElement());
            }
            return nombres.toArray(new String[nombres.size()]);
        } finally {
            closeSvc(svc);
        }
    }
}
