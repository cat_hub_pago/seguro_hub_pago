package cl.tinet.common.sessioncontrol.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessGestorDeSesion_274d8a62
 */
public class EJSRemoteStatelessGestorDeSesion_274d8a62 extends EJSWrapper implements GestorDeSesion {
	/**
	 * EJSRemoteStatelessGestorDeSesion_274d8a62
	 */
	public EJSRemoteStatelessGestorDeSesion_274d8a62() throws java.rmi.RemoteException {
		super();	}
	/**
	 * deleteExpiredSessions
	 */
	public int deleteExpiredSessions() throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		int _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[0];
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.deleteExpiredSessions();
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * getAttribute
	 */
	public java.lang.Object getAttribute(java.lang.String aplicacion, java.lang.String sid, java.lang.String nombre) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.lang.Object _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
				_jacc_parms[2] = nombre;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.getAttribute(aplicacion, sid, nombre);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * getAttributeNames
	 */
	public java.lang.String[] getAttributeNames(java.lang.String aplicacion, java.lang.String sid) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.lang.String[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.getAttributeNames(aplicacion, sid);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * getCreation
	 */
	public java.util.Date getCreation(java.lang.String aplicacion, java.lang.String sid) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Date _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.getCreation(aplicacion, sid);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * getLastAccess
	 */
	public java.util.Date getLastAccess(java.lang.String aplicacion, java.lang.String sid) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Date _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.getLastAccess(aplicacion, sid);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * getAttributesMap
	 */
	public java.util.Map getAttributesMap(java.lang.String aplicacion, java.lang.String sid) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.getAttributesMap(aplicacion, sid);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * createSession
	 */
	public void createSession(java.lang.String aplicacion, java.lang.String sid) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			beanRef.createSession(aplicacion, sid);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * delAttribute
	 */
	public void delAttribute(java.lang.String aplicacion, java.lang.String sid, java.lang.String nombre) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
				_jacc_parms[2] = nombre;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			beanRef.delAttribute(aplicacion, sid, nombre);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * invalidate
	 */
	public void invalidate(java.lang.String aplicacion, java.lang.String sid) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			beanRef.invalidate(aplicacion, sid);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * setAttribute
	 */
	public void setAttribute(java.lang.String aplicacion, java.lang.String sid, java.lang.String nombre, java.lang.Object valor) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = aplicacion;
				_jacc_parms[1] = sid;
				_jacc_parms[2] = nombre;
				_jacc_parms[3] = valor;
			}
	cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean beanRef = (cl.tinet.common.sessioncontrol.ejb.GestorDeSesionBean)container.preInvoke(this, 9, _EJS_s, _jacc_parms);
			beanRef.setAttribute(aplicacion, sid, nombre, valor);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
}
