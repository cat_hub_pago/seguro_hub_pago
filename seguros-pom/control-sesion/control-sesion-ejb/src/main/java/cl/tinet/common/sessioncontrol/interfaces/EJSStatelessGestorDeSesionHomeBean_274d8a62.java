package cl.tinet.common.sessioncontrol.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessGestorDeSesionHomeBean_274d8a62
 */
public class EJSStatelessGestorDeSesionHomeBean_274d8a62 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessGestorDeSesionHomeBean_274d8a62
	 */
	public EJSStatelessGestorDeSesionHomeBean_274d8a62() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.tinet.common.sessioncontrol.interfaces.GestorDeSesion create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.tinet.common.sessioncontrol.interfaces.GestorDeSesion result = null;
boolean createFailed = false;
try {
	result = (cl.tinet.common.sessioncontrol.interfaces.GestorDeSesion) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
