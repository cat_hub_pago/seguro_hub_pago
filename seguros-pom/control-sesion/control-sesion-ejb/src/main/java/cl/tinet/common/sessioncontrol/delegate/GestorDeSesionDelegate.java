package cl.tinet.common.sessioncontrol.delegate;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Map;

import cl.tinet.common.sessioncontrol.interfaces.GestorDeSesion;
import cl.tinet.common.sessioncontrol.interfaces.GestorDeSesionHome;
import cl.tinet.common.sessioncontrol.service.SesionService;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Implementaci�n remota de servicios de persistencia de sesi�n.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 */
public class GestorDeSesionDelegate implements SesionService {

    /**
     * Referencia a EJB de gesti�n de sesi�n.
     */
    private GestorDeSesion gestorSesion;

    /**
     * Referencia a instancia home del EJB de gesti�n de sesi�n.
     */
    private GestorDeSesionHome gestorSesionHome;

    /**
     * Constructor de la clase. Inicializa el estado y deja listo para
     * realizar la invocaci�n.
     */
    public GestorDeSesionDelegate() {
        try {
            this.gestorSesionHome =
                (GestorDeSesionHome) ServiceLocator.singleton().getRemoteHome(
                    GestorDeSesionHome.JNDI_NAME, GestorDeSesionHome.class);
            this.gestorSesion = this.gestorSesionHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Permite establecer un valor en sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a establecer.
     * @param valor valor a establecer.
     */
    public void setAttribute(String aplicacion, String sid, String nombre,
        Object valor) {
        try {
            this.gestorSesion.setAttribute(aplicacion, sid, nombre, valor);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Permite eliminar un elemento de la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a eliminar.
     */
    public void delAttribute(String aplicacion, String sid, String nombre) {
        try {
            this.gestorSesion.delAttribute(aplicacion, sid, nombre);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Retorna el valor del atributo especificado.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a obtener.
     * @return valor del atributo asociado al nombre especificado.
     */
    public Object getAttribute(String aplicacion, String sid, String nombre) {
        try {
            return this.gestorSesion.getAttribute(aplicacion, sid, nombre);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Retorna los nombres de los atributos en la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return todos los nombres de los atributos en sesi�n.
     */
    public String[] getAttributeNames(String aplicacion, String sid) {
        try {
            return this.gestorSesion.getAttributeNames(aplicacion, sid);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Retorna todos los atributos establecidos en la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return {@link Map} con los datos en sesi�n indexados por
     *          nombre.
     */
    @SuppressWarnings("unchecked")
    public Map < String, ? > getAttributesMap(String aplicacion,
        String sid) {
        try {
            return this.gestorSesion.getAttributesMap(aplicacion, sid);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Elimina la sesi�n especificada.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     */
    public void invalidate(String aplicacion, String sid) {
        try {
            this.gestorSesion.invalidate(aplicacion, sid);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Elimina las sesiones que se encuentran expiradas.
     * <br/>
     * @return cantidad de sesiones eliminadas.
     */
    public int deleteExpiredSessions() {
        try {
            return this.gestorSesion.deleteExpiredSessions();
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Retorna la fecha de ultimo acceso a la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return fecha de ultimo acceso a la sesi�n.
     */
    public Date getLastAccess(String aplicacion, String sid) {
        try {
            return this.gestorSesion.getLastAccess(aplicacion, sid);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Retorna la fecha de creaci�n de la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return fecha de creaci�n de la sesi�n.
     */
    public Date getCreation(String aplicacion, String sid) {
        try {
            return this.gestorSesion.getCreation(aplicacion, sid);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }

    /**
     * Cierra los recursos utilizados.
     */
    public void close() {
    }

    /**
     * Verifica que el SID especificado no existe, en cuyo caso crea el registro
     * en la base de datos. En caso contrario, actualiza la fecha de ultimo
     * acceso para as� evitar que la sesi�n expire.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     */
    public void createSession(String aplicacion, String sid) {
        try {
            this.gestorSesion.createSession(aplicacion, sid);
        } catch (RemoteException re) {
            throw new SystemException(re.getCause());
        }
    }
}
