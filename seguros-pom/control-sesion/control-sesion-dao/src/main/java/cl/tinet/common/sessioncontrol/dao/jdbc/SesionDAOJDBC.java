package cl.tinet.common.sessioncontrol.dao.jdbc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.serial.SerialException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;
import cl.tinet.common.sessioncontrol.dao.SesionDAO;

import com.tinet.exceptions.system.SystemException;

/**
 * Servicio de acceso a datos para persistencia de sesion.
 * <br/>
 * @author Roberto San Martin
 * @version 1.0
 * @created 26-Jul-2010 15:32:04
 */
public class SesionDAOJDBC extends ManagedBaseDAOJDBC implements SesionDAO {
    /**
     * Logger de la clase
     */
    private static final Log logger =
        LogFactory.getLog(SesionDAOJDBC.class);

    private static final int LONGITUID_NOMBRE_APLICACION = 10;

    /**
     * SQL que permite crear una nueva sesion.
     */
    public static final String SQL_INSERT_SESSION =
        "INSERT INTO \"SESSION\" (ID_SESSION, FECHA_CREACION, FECHA_ACTUALIZACION) VALUES (?, ?, ?)";

    /**
     * SQL que permite actualizar la fecha de acceso a la sesion.
     */
    public static final String SQL_UPDATE_SESSION =
        "UPDATE \"SESSION\" SET FECHA_ACTUALIZACION = ? WHERE ID_SESSION = ?";

    /**
     * SQL que permite eliminar la sesion.
     */
    public static final String SQL_DELETE_SESSION =
        "DELETE \"SESSION\" WHERE ID_SESSION = ?";

    /**
     * SQL que permite eliminar las sesiones expiradas.
     */
    public static final String SQL_DELETE_SESSION_EXPIRED =
        "DELETE \"SESSION\" WHERE FECHA_ACTUALIZACION <= ?";

    /**
     * SQL que permite obtener datos de la sesion.
     */
    public static final String SQL_SELECT_SESSION =
        "SELECT FECHA_CREACION, FECHA_ACTUALIZACION FROM \"SESSION\" WHERE ID_SESSION = ?";

    /**
     * SQL que permite insertar un atributo a la sesion.
     */
    public static final String SQL_INSERT_SESSION_DATA =
        "INSERT INTO DATA_SESSION (ID_SESSION, NOMBRE_ATRIBUTO, VALOR_ATRIBUTO) VALUES (?, ?, ?)";

    /**
     * SQL que permite actualizar un dato en sesion.
     */
    public static final String SQL_UPDATE_SESSION_DATA =
        "UPDATE DATA_SESSION SET VALOR_ATRIBUTO = ? WHERE ID_SESSION = ? AND NOMBRE_ATRIBUTO = ?";

    /**
     * SQL que permite eliminar un atributo de la sesion.
     */
    public static final String SQL_DELETE_SESSION_DATA =
        "DELETE DATA_SESSION WHERE ID_SESSION = ? AND NOMBRE_ATRIBUTO = ?";

    /**
     * SQL que permite eliminar todos los atributos de la sesion.
     */
    public static final String SQL_DELETE_ALL_SESSION_DATA =
        "DELETE DATA_SESSION WHERE ID_SESSION = ?";

    /**
     * SQL que permite eliminar la data de las sesiones
     * expiradas.
     */
    public static final String SQL_DELETE_SESSION_DATA_EXPIRED =
        "DELETE DATA_SESSION DAT WHERE EXISTS (SELECT 1 FROM \"SESSION\" SES WHERE (SES.FECHA_ACTUALIZACION <= ?) AND (DAT.ID_SESSION = SES.ID_SESSION))";

    /**
     * SQL que permite obtener el valor de un atributo.
     */
    public static final String SQL_SELECT_SESSION_DATA =
        "SELECT VALOR_ATRIBUTO FROM DATA_SESSION WHERE ID_SESSION = ? AND NOMBRE_ATRIBUTO = ?";

    /**
     * SQL que permite obtener los nombres de los atributos en
     * sesion.
     */
    public static final String SQL_SELECT_SESSION_DATA_NAMES =
        "SELECT NOMBRE_ATRIBUTO FROM DATA_SESSION WHERE ID_SESSION = ?";

    /**
     * SQL que permite obtener todos los atributos con sus nombres.
     */
    public static final String SQL_SELECT_SESSION_DATA_ALL =
        "SELECT NOMBRE_ATRIBUTO, VALOR_ATRIBUTO FROM DATA_SESSION WHERE ID_SESSION = ?";

    /**
     * Llave de configuración que permite obtener al duración de una sesion
     * antes de ser eliminada de la base de datos por estar expirada.
     */
    public static final String DURACION_EXPIRACION_SESION_KEY =
        "cl.tinet.common.sessioncontrol.DURACION_EXPIRACION_SESION";

    /**
     * Retorna todos los atributos establecidos en la sesion.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @return {@link Map} con los datos en sesion indexados por
     *          nombre.
     */
    @SuppressWarnings("unchecked")
    public Map < String, ? > getAttributesMap(String aplicacion, String sid) {
        List < Map < String, ? >> listado =
            this.query(Map.class, SQL_SELECT_SESSION_DATA_ALL,
                new Object[] { this.generateSID(aplicacion, sid) });
        try {
            Map < String, Object > dato = new HashMap < String, Object >();
            for (Map < String, ? > map : listado) {
                dato.put((String) map.get("NOMBRE_ATRIBUTO"), this
                    .getObject((Blob) map.get("VALOR_ATRIBUTO")));
            }
            return dato;
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        } catch (SQLException sqle) {
            throw new SystemException(sqle);
        } catch (IOException ioe) {
            throw new SystemException(ioe);
        }
    }

    /**
     * Retorna los nombres de los atributos en la sesion.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @return todos los nombres de los atributos en sesion.
     */
    @SuppressWarnings("unchecked")
    public String[] getAttributeNames(String aplicacion, String sid) {
        List < Map < String, String > > nombres =
            this.query(Map.class, SQL_SELECT_SESSION_DATA_NAMES,
                new Object[] { this.generateSID(aplicacion, sid) });
        String[] resultado = new String[nombres.size()];
        int i = 0;
        for (Map < String, String > map : nombres) {
            resultado[i++] = map.get("NOMBRE_ATRIBUTO");
        }
        return resultado;
    }

    /**
     * Retorna el valor del atributo especificado.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @param nombre identificador del atributo a obtener.
     * @return valor del atributo asociado al nombre especificado.
     */
    @SuppressWarnings("unchecked")
    public Object getAttribute(String aplicacion, String sid, String nombre) {
        try {
            Map < String, ? > elemento =
                (Map < String, ? >) this.find(Map.class,
                    SQL_SELECT_SESSION_DATA, new Object[] {
                        this.generateSID(aplicacion, sid), nombre });
            if (elemento == null) {
                return null;
            }
            return this.getObject((Blob) elemento.get("VALOR_ATRIBUTO"));
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        } catch (SQLException sqle) {
            throw new SystemException(sqle);
        } catch (IOException ioe) {
            throw new SystemException(ioe);
        }
    }

    /**
     * Permite establecer un valor en sesion.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @param nombre identificador del atributo a establecer.
     * @param valor valor a establecer.
     */
    public void setAttribute(String aplicacion, String sid, String nombre,
        Object valor) {
        String sessionID = generateSID(aplicacion, sid);
        PreparedStatement insertStm = null;
        PreparedStatement updateStm = null;
        Connection connection = this.getConnection();
        try {
            int indice = 1;
            byte[] bytes = createBytes(valor);
            updateStm = connection.prepareStatement(SQL_UPDATE_SESSION_DATA);
            updateStm.setBinaryStream(indice++,
                new ByteArrayInputStream(bytes), bytes.length);
            updateStm.setString(indice++, sessionID);
            updateStm.setString(indice++, nombre);
            int cantidad = updateStm.executeUpdate();
            // Si es que el registro no existe, se debe crear.
            if (cantidad == 0) {
                indice = 1;
                insertStm =
                    connection.prepareStatement(SQL_INSERT_SESSION_DATA);
                insertStm.setString(indice++, sessionID);
                insertStm.setString(indice++, nombre);
                insertStm.setBinaryStream(indice++, new ByteArrayInputStream(
                    bytes), bytes.length);
                insertStm.execute();
            }
        } catch (SerialException se) {
            throw new SystemException(se);
        } catch (SQLException sqle) {
            logger.error("Error SQL: ", sqle);
            throw new SystemException(sqle);
        } catch (IOException ioe) {
            throw new SystemException(ioe);
        } finally {
            this.close(updateStm);
            this.close(insertStm);
        }
    }

    /**
     * Permite eliminar un elemento de la sesion.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @param nombre identificador del atributo a eliminar.
     */
    public void delAttribute(String aplicacion, String sid, String nombre) {
        this.update(SQL_DELETE_SESSION_DATA, new Object[] {
            this.generateSID(aplicacion, sid), nombre });
    }

    /**
     * Retorna la fecha de ultimo acceso a la sesion.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @return fecha de ultimo acceso a la sesion.
     */
    public Date getLastAccess(String aplicacion, String sid) {
        return getCampoSesion(aplicacion, sid, "FECHA_ACTUALIZACION");
    }

    /**
     * Retorna la fecha de creación de la sesion.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @return fecha de creación de la sesion.
     */
    public Date getCreation(String aplicacion, String sid) {
        return getCampoSesion(aplicacion, sid, "FECHA_CREACION");
    }

    /**
     * Verifica que el SID especificado no existe, en cuyo caso crea el registro
     * en la base de datos. En caso contrario, actualiza la fecha de ultimo
     * acceso para así evitar que la sesion expire.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     */
    public void createSession(String aplicacion, String sid) {
        Date fechaActual = new Date();
        String sessionID = generateSID(aplicacion, sid);
        int cantidad =
            this.update(SQL_UPDATE_SESSION, new Object[] { fechaActual,
                sessionID });
        if (cantidad == 0) {
            this.update(SQL_INSERT_SESSION, new Object[] { sessionID,
                fechaActual, fechaActual });
        }
    }

    /**
     * Elimina la sesion especificada.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     */
    public void invalidate(String aplicacion, String sid) {
        Object[] arguments = { generateSID(aplicacion, sid) };
        this.update(SQL_DELETE_ALL_SESSION_DATA, arguments);
        this.update(SQL_DELETE_SESSION, arguments);
    }

    /**
     * Elimina las sesiones que se encuentran expiradas.
     * <br/>
     * @return cantidad de sesiones eliminadas.
     */
    public int deleteExpiredSessions() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -getConfigurator().getInt(
            DURACION_EXPIRACION_SESION_KEY, getLocale()));
        Object[] arguments = { calendar.getTime() };
        this.update(SQL_DELETE_SESSION_DATA_EXPIRED, arguments);
        return this.update(SQL_DELETE_SESSION_EXPIRED, arguments);
    }

    /**
     * Modifica el formato del SID para mezclarlo con el ID de sesion.
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @return identificador con formato para búsqueda en base de datos.
     */
    private String generateSID(String aplicacion, String sid) {
        if (aplicacion == null) {
            throw new NullPointerException("La aplicacion no debe ser null.");
        }
        if (sid == null) {
            throw new NullPointerException(
                "El SID especificado no debe ser null");
        }
        if (aplicacion.length() > LONGITUID_NOMBRE_APLICACION) {
            throw new IllegalArgumentException(
                "El identificador de aplicacion no tener mas de "
                    + LONGITUID_NOMBRE_APLICACION + " caracteres.");
        }
        return ":"
            + StringUtils
                .rightPad(aplicacion, LONGITUID_NOMBRE_APLICACION, '_') + ":"
            + sid;
    }

    /**
     * Retorna los datos de la sesion persistente (fechas).
     * <br/>
     * @param aplicacion nombre de la aplicación.
     * @param sid identificador de la sesion.
     * @param campoSesion nombre del campo solicitado.
     * @return campo relacionado a la sesion (fecha de creación o
     *          de ultimo acceso).
     */
    @SuppressWarnings("unchecked")
    private Date getCampoSesion(String aplicacion, String sid,
        String campoSesion) {
        Map < String, Date > datos =
            (Map < String, Date >) this.find(Map.class, SQL_SELECT_SESSION,
                new Object[] { this.generateSID(aplicacion, sid) });
        Date fecha = null;
        if (datos != null) {
            fecha = datos.get(campoSesion);
        }
        return fecha;
    }

    /**
     * Transforma el objeto especificado en un arreglo de bytes.
     * <br/>
     * @param origen objeto a transformar.
     * @return objeto transformado.
     * @throws IOException en caso de error durante serialización.
     */
    protected byte[] createBytes(Object origen) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(origen);
        oos.close();
        return baos.toByteArray();
    }

    /**
     * Transforma el blob especificado en un objeto.
     * <br/>
     * @param blob origen de los datos.
     * @return objeto desde base de datos.
     * @throws IOException en caso de error durante la reconstrucci�n del
     *          objeto.
     * @throws ClassNotFoundException en caso de que la clase no se
     *          encuentre en el classpath de la aplicación.
     * @throws SQLException en caso de ocurrir un error rescatando el dato.
     */
    protected Object getObject(Blob blob) throws IOException,
        ClassNotFoundException, SQLException {
        byte[] bytes = blob.getBytes(1, (int) blob.length());
        ObjectInputStream ois =
            new ObjectInputStream(new ByteArrayInputStream(bytes));
        Object object = ois.readObject();
        ois.close();
        return object;
    }
}
