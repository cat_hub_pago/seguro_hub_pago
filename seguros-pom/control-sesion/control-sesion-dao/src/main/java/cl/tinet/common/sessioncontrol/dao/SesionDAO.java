package cl.tinet.common.sessioncontrol.dao;


import cl.tinet.common.dao.jdbc.BaseDAO;
import cl.tinet.common.sessioncontrol.service.SesionService;

/**
 * Interfaz del DAO de sesi�n.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 26-Jul-2010 15:32:04
 */
public interface SesionDAO extends BaseDAO, SesionService {
}
