package cl.tinet.common.sessioncontrol.dao.jdbc;

import org.apache.commons.dbutils.ResultSetHandler;

import cl.tinet.common.dao.jdbc.oracle.OracleJDBCUtil;

/**
 * Implementacion DAO de sesion persistente especializada para Oracle.
 * <br/>
 * @author Roberto San Martin
 * @version 1.0
 * @created Aug 17, 2010
 */
public class SesionDAOJDBCOracle extends SesionDAOJDBC {

    /**
     * Retorna el manejador de {@link java.sql.ResultSet} especializado para
     * Oracle.
     * 
     * @see OracleJDBCUtil#getResultSetHandler(Class)
     * 
     * @param resultClass clase esperada como resultado.
     * @return manejador de {@link java.sql.ResultSet} especializado.
     */
    @Override
    protected ResultSetHandler getResultSetHandler(Class resultClass) {
        return OracleJDBCUtil.getResultSetHandler(resultClass);
    }
}
