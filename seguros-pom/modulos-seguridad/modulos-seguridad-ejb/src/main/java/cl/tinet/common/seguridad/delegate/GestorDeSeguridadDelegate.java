package cl.tinet.common.seguridad.delegate;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.interfaces.GestorDeSeguridad;
import cl.tinet.common.seguridad.interfaces.GestorDeSeguridadHome;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.Rol;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.service.SeguridadService;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Delegate que contiene los servicios necesarios para 
 * la Autenticaci�n de Usuario.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 06/10/2010
 */
public class GestorDeSeguridadDelegate implements SeguridadService {

    /**
    * Variable de acceso al log.
    */
    private static Log logger =
        LogFactory.getLog(GestorDeSeguridadDelegate.class);

    /**
     * Interface.
     */
    private GestorDeSeguridadHome seguridadHome = null;

    /**
     * Bean.
     */
    private GestorDeSeguridad seguridadBean = null;

    /**
     * Constructor de la clase.
     */
    public GestorDeSeguridadDelegate() {

        try {
            this.seguridadHome =
                (GestorDeSeguridadHome) ServiceLocator.singleton()
                    .getRemoteHome(seguridadHome.JNDI_NAME,
                        GestorDeSeguridadHome.class);
            this.seguridadBean = this.seguridadHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Permite realizar la autenticaci�n del usuario. En caso de que la
     * autenticaci�n no sea exitosa se lanza una AutenticacionFallidaException.
     *
     * @param usuario nombre del usuario.
     * @param clave clave del usuario.
     * @param tipoUsuario tipo de usuario.
     * @return Usuario.
     * @throws RemoteException 
     * @throws AutenticacionException 
     */
    public Usuario autenticar(String usuario, String clave, int tipoUsuario)
        throws AutenticacionException {
        try {
            return this.seguridadBean.autenticar(usuario, clave, tipoUsuario);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }
    }

    /**
     * Permite realizar la autenticaci�n del usuario. En caso de que la
     * autenticaci�n no sea exitosa se lanza una AutenticacionFallidaException.
     *
     * @param usuario nombre del usuario.
     * @param clave clave del usuario.
     * @param tipoUsuario tipo de usuario.
     * @param locale localizaci�n.
     * @throws AutenticacionException En caso de error al autenticar usuario.
     * @return Usuario.
     */
    public Usuario autenticar(String usuario, String clave, int tipoUsuario,
        Long locale) throws AutenticacionException {

        Usuario resultado = null;
        try {
            resultado =
                (Usuario) this.seguridadBean.autenticar(usuario, clave,
                    tipoUsuario, locale);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Registra la cantidad de veces que el usuario ha 
     * fallado en su autenticaci�n.
     * @param b indicador para reiniciar el contador 
     * (true:aumenta, false: reinicia contador).
     * @param usuario login del usuario.
     * @return Numero de intentos.
     */
    public Integer loginFallido(boolean b, Usuario usuario) {
        Integer resultado = null;
        try {
            resultado = this.seguridadBean.loginFallido(b, usuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Cambia la clave temporal del usuario.
     * @param usuario nombre del usuario
     * @param claveTemporal clave temporal.
     * @param claveNueva nueva clave.
     * @param locale variable de localizaci�n.
     * @return indica si la clave fue cambiada.
     */
    public boolean cambiarClaveTemporal(Usuario usuario, String claveTemporal,
        String claveNueva, Long locale) {
        boolean resultado = false;
        try {
            resultado =
                this.seguridadBean.cambiarClaveTemporal(usuario, claveTemporal,
                    claveNueva, locale);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Genera una nueva clave temporal.
     * @param rut rut del usuario externo.
     * @param dv digito verificador del rut del usuario externo.
     * @param locale variable de localizaci�n.
     * @param tipoUsuario tipo de usuario.
     * @return indicador si gener� la clave.
     */
    public boolean generarClaveTemporal(String rut, String dv, long locale,
        int tipoUsuario) {
        boolean resultado = false;
        try {
            resultado =
                this.seguridadBean.generarClaveTemporal(rut, dv, locale,
                    tipoUsuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Registra un evento realizado por el usuario.
     * @param tipo tipo de evento.
     * @param idUsuario identificador del usuario.
     * @param idFuncionalidad identificador de la funcionalidad.
     * @param descripcion descripci�n de la acci�n.
     * @param date fecha.
     * @param locale identificador de localizaci�n.
     */

    public void registrarEventoAuditoria(int tipo, int idUsuario,
        Integer idFuncionalidad, String descripcion, Date date, int locale) {
        try {
            this.seguridadBean.registrarEventoAuditoria(tipo, idUsuario,
                idFuncionalidad, descripcion, date, locale);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene un usuario Externo.
     * @param userName nombre del usuario
     * @param locale variable de localizaci�n.
     * @return Usuario Externo.
     */
    public UsuarioExterno obtenerUsuarioExterno(String userName, int locale) {
        UsuarioExterno usuarioExterno = null;
        try {
            usuarioExterno =
                this.seguridadBean.obtenerUsuarioExterno(userName, locale);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return usuarioExterno;
    }

    /**
     * Validaci�n de un usuario externo.
     * @param userName nombre del usuario LDAP.
     * @param password contrasena del usuario LDAP.
     * @param  tipoUsuario tipo de usuario.
     * @return indicador de validaci�n.
     */
    public boolean validacionLDAP(String userName, String password,
        int tipoUsuario) {
        try {
            return this.seguridadBean.validacionLDAP(userName, password,
                tipoUsuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo autenticar.
     * @param usuario
     * @param clave
     * @param tipoUsuario
     * @param locale
     * @return
     * @throws AutenticacionException
     */
    public Usuario autenticar(String usuario, String clave, int tipoUsuario,
        Locale locale) throws AutenticacionException {
        try {
            return this.seguridadBean.autenticar(usuario, clave, tipoUsuario,
                locale);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }
    }

    /**
     * TODO Describir m�todo obtenerFuncionalidades.
     * @param usuario
     * @return
     */
    public List < Funcionalidad > obtenerFuncionalidades(Usuario usuario) {
        try {
            return this.seguridadBean.obtenerFuncionalidades(usuario);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }
    }

    /**
     * TODO Describir m�todo obtenerFuncionalidades.
     * @param usuario
     * @param locale
     * @return
     */
    public List < Funcionalidad > obtenerFuncionalidades(Usuario usuario,
        Locale locale) {
        try {
            return this.seguridadBean.obtenerFuncionalidades(usuario, locale);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }
    }

    /**
     * TODO Describir m�todo obtenerRoles.
     * @param usuario
     * @return
     */
    public List < Rol > obtenerRoles(Usuario usuario) {
        try {
            return this.seguridadBean.obtenerRoles(usuario);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }
    }

    /**
     * TODO Describir m�todo obtenerRoles.
     * @param usuario
     * @param locale
     * @return
     */
    public List < Rol > obtenerRoles(Usuario usuario, Locale locale) {
        try {
            return this.seguridadBean.obtenerRoles(usuario, locale);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }
    }

    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     */
    public void registrarEvento(Evento evento) {
        try {
            this.seguridadBean.registrarEvento(evento);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }

    }

    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     * @param locale
     */
    public void registrarEvento(Evento evento, Locale locale) {
        try {
            this.seguridadBean.registrarEvento(evento, locale);
        } catch (RemoteException e) {
            throw new SystemException(e.getCause());
        }
    }

	public Usuario getUsuario(String username, int tipo)
			throws AutenticacionException {
		// TODO Auto-generated method stub
		return null;
	}
}
