package cl.tinet.common.seguridad.dao.jdbc;

import java.sql.Connection;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.dao.jdbc.oracle.OracleJDBCUtil;
import cl.tinet.common.seguridad.config.SeguridadSQLConfig;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Usuario;

import com.tinet.exceptions.system.SystemException;

/**
 * TODO Falta descripcion de clase SeguridadDAOJDBCOracle.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 12, 2010
 */
public class SeguridadDAOJDBCReportesOracle extends SeguridadDAOJDBC {

    /**
     * TODO Describir atributo ESTADO_CLAVE_ACTIVA_KEY.
     */
    public static final String ESTADO_CLAVE_ACTIVA_KEY =
        "cl.tinet.common.seguridad.validacion.ESTADO_CLAVE_ACTIVA";

    public static final String SQL_GET_CLAVE_KEY = "SQL_GET_CLAVE";

    /**
     * TODO Describir atributo logger.
     */
    private static Log logger = LogFactory.getLog(SeguridadDAOJDBCOracle.class);

    /**
     * TODO Describir m�todo getResultSetHandler.
     * @param resultClass
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    protected ResultSetHandler getResultSetHandler(Class resultClass) {
        return OracleJDBCUtil.getResultSetHandler(resultClass);
    }

    @Override
    public Usuario getUsuario(String username, String password, int tipo,
        Locale locale) {
        
        Connection con = this.getConnection("cl.tinet.common.seguridad.dao.jdbc.SeguridadDAOJDBCReportesOracle");
        
        String className =
            this.getConfigurator().getString(USUARIO_CLASS_KEY + "_" + tipo,
                locale);
        logger.info("CLASE: " + className);
        if (className == null) {
            className = Usuario.class.getName();
        }
        try {
            Class < ? > clase = Class.forName(className);
            Usuario usuario =
                (Usuario) this.find(con, clase, obtenerSQL(SQL_GET_USUARIO_KEY + "_"
                    + tipo), new Object[] { tipo, username });
            return usuario;
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        }
    }

    /**
     * TODO Describir m�todo obtenerSQL.
     * @param sqlKey
     * @return
     */
    private String obtenerSQL(String sqlKey) {
        return SeguridadSQLConfig.getInstance().getString(sqlKey,
            this.getLocale());
    }

    /**
     * TODO Describir m�todo getUltimaClave.
     * 
     * @param idUsuario
     * @return
     */
    @Override
    public Clave getUltimaClave(int idUsuario) {
        Connection con = this.getConnection("cl.tinet.common.seguridad.dao.jdbc.SeguridadDAOJDBCReportesOracle");

        Clave clave = new Clave();
        int estadoActiva =
            this.getConfigurator().getInt(ESTADO_CLAVE_ACTIVA_KEY, getLocale());

        Map < String, String > mClave =
            (Map < String, String >) this.find(con, Map.class, obtenerSQL(SQL_GET_CLAVE_KEY),
                new Object[] { idUsuario });
        clave.setPassword(mClave.get("valor"));

        clave.setEstado(estadoActiva);
        clave.setIntentos(0);
        
        //FIXME: USUARIOS REPORTE: Resolver forma de setear idUsuario.
        Map < String, Object > mIdUsuario = (Map < String, Object >) this.find(Map.class,  obtenerSQL("SQL_GET_USUARIO_3_1"), new Object[]{});
        
        Number id = (Number)mIdUsuario.get("id_usuario");
        clave.setIdClave(id.longValue());
        
        return clave;
    }
}
