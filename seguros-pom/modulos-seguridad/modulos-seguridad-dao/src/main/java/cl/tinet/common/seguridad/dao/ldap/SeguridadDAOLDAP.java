package cl.tinet.common.seguridad.dao.ldap;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.jumbo.ldap.LDAPJumbo;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.dao.SeguridadDAO;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.TipoFuncionalidad;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.model.UsuarioInterno;
import cl.tinet.common.util.crypto.CryptoUtil;

/**
 * TODO Falta descripcion de clase SeguridadDAOJDBC.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Sep 2, 2010
 */

public class SeguridadDAOLDAP extends ManagedBaseDAOJDBC implements
    SeguridadDAO {

    private static final Log logger = LogFactory.getLog(SeguridadDAOLDAP.class);

    public static final String CANTIDAD_TIEMPO_BLOQUEADO =
        "cl.tinet.common.seguridad.dao.jdbc.SeguridadDAOJDBC.FECHA_LIMITE";

    /**
     * Sentencia que valida un usuario, con RUT, DV, PSW
     */
    public static final String SQL_VALIDACION_USUARIO =

        " Select USUARIO.ID_USUARIO as idusuario, USUARIO.TIPO_USUARIO as tipo,"
            + "  USUARIO.FECHA_CREACION AS fechaIngreso,"
            + "  USUARIO.USERNAME AS username,"
            + "  USUARIO.ES_ACTIVO AS activo,"
            + "  PSW.ID_ESTADO_PSW AS estado from USUARIO inner join PSW ON "
            + "  (PSW.ID_USUARIO = USUARIO.ID_USUARIO) inner join TIPO_ESTADO_PSW ON "
            + "  (PSW.ID_ESTADO_PSW =  TIPO_ESTADO_PSW.ID_ESTADO_PSW) WHERE "
            + "  USUARIO.USERNAME = ? AND " + " PSW.PSW = ? AND "
            + "  USUARIO.ID_PAIS = ?";

    /* Sentencia que obtiene los roles de un usuario especifico*/

    public static final String SQL_ROLES_FUNCIONALIDAD =

        "  Select "
            + " FUNCIONALIDAD.id_funcionalidad as id, "
            + " FUNCIONALIDAD.nombre  as nombre, "
            + " FUNCIONALIDAD.url as url,"
            + " FUNCIONALIDAD.id_tipo as idTipo "
            + " FROM USUARIO inner join USUARIO_ROL ON "
            + " (USUARIO_ROL.ID_USUARIO = USUARIO.ID_USUARIO) inner join rol on "
            + " (USUARIO_ROL.ID_ROL = ROL.ID_ROL) inner join permiso on "
            + " (PERMISO.ID_ROL = ROL.ID_ROL) inner join funcionalidad on "
            + " (PERMISO.ID_FUNCIONALIDAD = FUNCIONALIDAD.ID_FUNCIONALIDAD) inner join TIPO_FUNCIONALIDAD ON "
            + " (FUNCIONALIDAD.ID_TIPO = TIPO_FUNCIONALIDAD.ID_TIPO) "
            + "  WHERE USUARIO.USERNAME  = ? ";

    public static final String SQL_TIPO_FUNCIONALIDAD =

        "  Select "
            + " TIPO_FUNCIONALIDAD.ID_TIPO as id, "
            + " TIPO_FUNCIONALIDAD.NOMBRE  as nombre "
            + " FROM USUARIO inner join USUARIO_ROL ON "
            + " (USUARIO_ROL.ID_USUARIO = USUARIO.ID_USUARIO) inner join rol on "
            + " (USUARIO_ROL.ID_ROL = ROL.ID_ROL) inner join permiso on "
            + " (PERMISO.ID_ROL = ROL.ID_ROL) inner join funcionalidad on "
            + " (PERMISO.ID_FUNCIONALIDAD = FUNCIONALIDAD.ID_FUNCIONALIDAD) inner join TIPO_FUNCIONALIDAD ON "
            + " (FUNCIONALIDAD.ID_TIPO = TIPO_FUNCIONALIDAD.ID_TIPO) "
            + "  WHERE USUARIO.USERNAME  = ? ";

    public static final String SQL_BLOQUEAR_UN_USUARIO =
        "    update  "
            + "  PSW set FECHA_BLOQUEO =  {fn CURTIME()}, FECHA_MODIFICACION =  {fn CURTIME()}, "
            + "  ID_ESTADO_PSW = 2 " + "  where ID_USUARIO = "
            + "  (select USUARIO.ID_USUARIO FROM USUARIO WHERE USERNAME= ?) ";

    public static final String SQL_LOGIN_FALLIDO =
        " update usuario  set cantintentos = ( "
            + " select (nvl(cantintentos,0) + 1) from usuario  where USERNAME = ?) "
            + " where username=? ";

    public static final String SQL_INTENTOS_FALLIDOS_CERO =
        " update usuario set cantintentos = 0 where " + " USERNAME = ?";

    public static final String SQL_CANTIDAD_INTENTOS =
        " Select cantintentos as resultado from usuario where USUARIO.USERNAME = ? ";

    public static final String SQL_OBTENER_ESTADO_USUARIO =
        "   Select PSW.ID_ESTADO_PSW as estado from PSW inner join USUARIO on ( "
            + "  PSW.ID_USUARIO = USUARIO.ID_USUARIO) inner join TIPO_ESTADO_PSW  on("
            + "  PSW.ID_ESTADO_PSW = TIPO_ESTADO_PSW.ID_ESTADO_PSW) "
            + "  WHERE USUARIO.USERNAME = ? ";

    public static final String SQL_VALIDA_FECHA_USUARIO_BLOQUEADO =

        " Select PSW.FECHA_BLOQUEO as fecha  from PSW inner join USUARIO on ( "
            + " PSW.ID_USUARIO = USUARIO.ID_USUARIO) inner join TIPO_ESTADO_PSW  on( "
            + " PSW.ID_ESTADO_PSW = TIPO_ESTADO_PSW.ID_ESTADO_PSW) "
            + " WHERE USUARIO.USERNAME = ? ";

    public static final String SQL_OBTIENE_FECHAS_USUARIO =
        " Select PSW.FECHA_BLOQUEO as fechabloqueo, FECHA_MODIFICACION as fechamodificacion from PSW inner join USUARIO on ( "
            + " PSW.ID_USUARIO = USUARIO.ID_USUARIO) inner join TIPO_ESTADO_PSW  on( "
            + " PSW.ID_ESTADO_PSW = TIPO_ESTADO_PSW.ID_ESTADO_PSW) "
            + " WHERE USUARIO.USERNAME = ? ";

    public static final String SQL_DESBLOQUEAR_USUARIO =

        " Update PSW SET PSW.ID_ESTADO_PSW = 1, FECHA_MODIFICACION =  {fn CURTIME()} WHERE ID_USUARIO  = ( "
            + " Select ID_USUARIO from USUARIO WHERE USUARIO.USERNAME= ? )  ";

    public static final String SQL_USUARIO_EN_LA_BASE =
        " select count(*)as cantidad from psw inner join USUARIO on( "
            + " USUARIO.ID_USUARIO = PSW.ID_USUARIO) WHERE USUARIO.USERNAME= ? ";

    public static final String SQL_USUARIO_OBTENER_ID =
        "Select id_usuario from usuario where username = ?";

    public static final String SQL_USUARIO_N_ALTERNATIVO =

        " Select nombre, apellido_paterno, apellido_materno, Telefono_1, Telefono_2, "
            + " email, numero, rut_cliente, dv_cliente, numero_departamento, "
            + " numero, calle  "
            + " from USUARIO_EXTERNO inner join USUARIO ON ( "
            + " USUARIO.ID_USUARIO = USUARIO_EXTERNO.ID_USUARIO)  WHERE USUARIO_EXTERNO.ID_USUARIO = ( "
            + " Select USUARIO.ID_USUARIO FROM USUARIO WHERE  USUARIO.USERNAME = ?) ";

    public static final String SQL_CAMBIAR_ESTADO_USUARIO =
        " update PSW set PSW.ID_ESTADO_PSW = ? where PSW.ID_USUARIO = "
            + " (Select USUARIO.ID_USUARIO FROM USUARIO WHERE  USERNAME = ? AND ID_PAIS = ?)";

    public static final String SQL_CAMBIAR_CLAVE_USUARIO =
        "  update PSW set PSW.PSW= ?, FECHA_MODIFICACION =  {fn CURTIME()}  where PSW.ID_USUARIO = "
            + " (Select USUARIO.ID_USUARIO FROM USUARIO WHERE  USERNAME = ? AND ID_PAIS = ?)";

    public static final String SQL_CAMBIAR_CLAVE_TEMPORAL =
        "  Update PSW SET PSW.PSW=?, FECHA_MODIFICACION =   {fn CURTIME()} where ID_USUARIO = ( "
            + " SELECT ID_USUARIO FROM USUARIO WHERE USUARIO.USERNAME = ?) AND "
            + " PSW = ? ";

    public static final String SQL_INSERT_AUDITORIA =
        "insert into AUDITORIA(ID_AUDITORIA,ID_USUARIO,ID_FUNCIONALIDAD,DESCRIPCION,FECHA,ID_PAIS) "
            + " Values(INCREMENTO_AUDITORIA.NEXTVAL,?,?,?,?,?)";

    public static final String SQL_VALIDAR_EXISTENCIA_USUARIO =
        "Select count(USUARIO.ID_USUARIO) as total FROM USUARIO WHERE USUARIO.USERNAME= ? ";

    /**
     * TODO Describir atributo SQL_CANTIDAD_AUDITORIA.
     */
    public static final String SQL_CANTIDAD_AUDITORIA =
        "Select count(*)as total from auditoria";

    public static final String SQL_USUARIO_INTERNO =
        " SELECT ID_USUARIO_INT as IdUsuarioInt, "
            + " ID_USUARIO as IdUsuario , "
            + " EMAIL as email, "
            + " LOGIN as login, "
            + " ID_PAIS as IdPais, "
            + " FECHA_CREACION as fechaCreacion , "
            + " FECHA_MODIFICACION as fechaModifiacion  "
            + "     FROM VSP_SEG.USUARIO_INTERNO WHERE  "
            + "     USUARIO_INTERNO.ID_USUARIO = ( Select USUARIO.ID_USUARIO FROM USUARIO WHERE  USUARIO.USERNAME = ?)";

    /**
     * TODO Describir constructor de SeguridadDAOJDBC.
     */
    public SeguridadDAOLDAP() {

    }

    /**
     * TODO Describir m�todo autentificar.
     * @param rut
     * @param dv
     * @param clave
     * @return
     */
    public boolean autentificar(String rut, String dv, String clave) {

        Boolean resultado = false;
        CryptoUtil criptar = new CryptoUtil(this.getConfigurator());
        String claveCod =
            criptar.toBase64String(criptar.getEncrypted(clave)).trim();
        String[] arregloParametros = { rut, dv, claveCod };

        return resultado;

    }

    /**
     * TODO Describir m�todo obtenerRoles.
     * @param usuario
     * @return
     */
    public List obtenerRoles(Usuario usuario) {

        Object[] parametros = { usuario.getUsername() };

        List < ? > funcionalidades =
            (List < ? >) this.query(Funcionalidad.class,
                SQL_ROLES_FUNCIONALIDAD, parametros);

        List < ? > tipos =
            (List < ? >) this.query(TipoFuncionalidad.class,
                SQL_TIPO_FUNCIONALIDAD, parametros);

        Map < Integer, TipoFuncionalidad > tiposMap =
            new HashMap < Integer, TipoFuncionalidad >();
        for (Iterator < ? > iterator = tipos.iterator(); iterator.hasNext();) {
            TipoFuncionalidad tf = (TipoFuncionalidad) iterator.next();
            tiposMap.put(tf.getId(), tf);
        }

        for (Iterator < ? > iterator = funcionalidades.iterator(); iterator
            .hasNext();) {
            Funcionalidad f = (Funcionalidad) iterator.next();
            TipoFuncionalidad tf = tiposMap.get(f.getIdTipo());
            f.setTipo(tf);
        }

        return funcionalidades;
    }

    /**
     * TODO Describir m�todo obtenerFechas.
     * @param usuario
     * @param clave
     * @param tipo
     * @return
     */
    public Map obtenerFechas(String usuario, String clave, int tipo) {
        AbstractConfigurator config = this.getConfigurator();
        int tiempoBloqueo = config.getInt(CANTIDAD_TIEMPO_BLOQUEADO);
        Object[] parametros = { usuario.trim() };
        Map fechaMap =
            (Map) this.find(Map.class, SQL_OBTIENE_FECHAS_USUARIO, parametros);
        fechaMap.put("tiempoBloqueo", tiempoBloqueo);
        return fechaMap;
    }

    /**
     * TODO Describir m�todo obtenerUsuario.
     * @param usuario
     * @param clave
     * @param tipo
     * @param local
     * @return
     */
    public Usuario obtenerUsuario(String usuario, String clave, int tipo,
        Long local) {

        Object[] arregloParametros = { usuario.trim(), clave.trim(), local };
        Usuario user =
            (Usuario) this.find(Usuario.class, SQL_VALIDACION_USUARIO,
                arregloParametros);
        return user;
    }

    /**
     * TODO Describir m�todo obtenerEstadoUsuario.
     * @param usuario
     * @return
     */
    public Integer obtenerEstadoUsuario(Usuario usuario) {
        Object[] arregloParametros = { usuario.getUsername() };
        Map estado =
            (Map) this.find(Map.class, SQL_OBTENER_ESTADO_USUARIO,
                arregloParametros);
        Number cantidad = (Number) estado.get("estado");
        return cantidad.intValue();
    }

    /**
     * TODO Describir m�todo obtenerEstadoUsuario.
     * @param userName
     * @return
     */
    public Integer obtenerEstadoUsuario(String userName) {

        Object[] arregloParametros = { userName };
        Number cantidad;
        Map estado =
            (Map) this.find(Map.class, SQL_OBTENER_ESTADO_USUARIO,
                arregloParametros);
        if (estado != null) {
            cantidad = (Number) estado.get("estado");
        } else {
            cantidad = 0;
        }
        return cantidad.intValue();
    }

    /**
     * TODO Describir m�todo bloquearCuentaUsuario.
     * @param userName
     * @return
     */
    public int bloquearCuentaUsuario(String userName) {
        boolean resultado = false;
        Object[] arregloParametros = { userName };
        int cantidad = this.update(SQL_BLOQUEAR_UN_USUARIO, arregloParametros);
        return cantidad;
    }

    /**
     * 
     * Cuando es falso, vuelve a cero.
     * @param b
     * @param usuario
     * @return
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="Supports"
     */
    public Integer loginFallido(boolean b, Usuario usuario) {
        Object[] param = { usuario.getUsername() };
        Map result = (Map) this.find(Map.class, SQL_CANTIDAD_INTENTOS, param);
        Number resultado = (Number) result.get("resultado");
        Integer cantIntentos = Integer.valueOf(resultado.intValue());

        //Aumenta 1 los intentos
        if (b == true) {
            if (cantIntentos <= 3) {
                Object[] parametros =
                    { usuario.getUsername(), usuario.getUsername() };
                this.update(SQL_LOGIN_FALLIDO, parametros);
            }
        }
        //Vuelve a cero los intentos.
        else {
            Object[] parametros = { usuario.getUsername() };

            this.update(SQL_INTENTOS_FALLIDOS_CERO, parametros);
        }
        return cantIntentos;
    }

    /**
     * Valida si el Usuario existe en la BD
     * NO VALIDA USUARIOS, SOLO SU EXISTENCIA �
     * @param userTO
     * @return
     */
    public boolean usuarioInscrito(Usuario userTO) {
        Object[] param = { userTO.getUsername() };
        boolean resultado = false;
        Map result = (Map) this.find(Map.class, SQL_USUARIO_EN_LA_BASE, param);
        Number cantidad = (Number) result.get("cantidad");

        Integer cant = cantidad.intValue();
        if (cant == 1) {
            resultado = true;
        }

        return resultado;
    }

    /**
     * TODO Describir m�todo cambiarEstadoUsuario.
     * @param userName
     * @param locale
     * @param estado
     * @return
     */
    public boolean cambiarEstadoUsuario(String userName, long locale, int estado) {

        Object[] param = { estado, userName, locale };
        boolean devolver = false;

        int resultado = this.update(SQL_CAMBIAR_ESTADO_USUARIO, param);
        if (resultado != 0) {
            devolver = true;
        }

        return devolver;
    }

    /**
     * TODO Describir m�todo cambiarEstadoUsuario.
     * @param usuarioTO
     * @param locale
     * @param estado
     * @return
     */
    public boolean cambiarEstadoUsuario(Usuario usuarioTO, long locale,
        int estado) {

        Object[] param = { estado, usuarioTO.getUsername(), locale };
        boolean devolver = false;
        int resultado = this.update(SQL_CAMBIAR_ESTADO_USUARIO, param);
        if (resultado != 0) {
            devolver = true;
        }

        return devolver;
    }

    /**
     * Obtiene un map de usuario externo
     * @param rut
     * @param dv
     * @param locale
     * @return
     */
    public Map getUsuarioExterno(String rut, String dv, long locale) {
        return null;
    }

    /**
     * Obtiene un obejo UsuarioExterno con los datos del usuario.
     * @param usuario
     * @return
     */
    public UsuarioExterno getUsuarioExterno(Usuario usuario) {
        return null;
    }

    /**
     * TODO Describir m�todo cambiarClaveUsuario.
     * @param userName
     * @param locale
     * @param clave
     * @return
     */
    public boolean cambiarClaveUsuario(String userName, long locale,
        String clave) {
        Object[] param = { clave, userName, locale };
        boolean devolver = false;
        int resultado = this.update(SQL_CAMBIAR_CLAVE_USUARIO, param);
        if (resultado != 0) {
            devolver = true;
        }
        return devolver;
    }

    /**
     * TODO Describir m�todo cambiarClaveTemporal.
     * @param userName
     * @param claveTemporal
     * @param claveNueva
     * @return
     */
    public boolean cambiarClaveTemporal(String userName, String claveTemporal,
        String claveNueva) {
        Object[] param = { claveNueva, userName, claveTemporal };

        boolean devolver = false;

        int resultado = this.update(SQL_CAMBIAR_CLAVE_TEMPORAL, param);
        if (resultado != 0) {
            devolver = true;
        }

        return devolver;
    }

    /**
     * TODO Describir m�todo existenciaUsuario.
     * @param userName
     * @return
     */
    public boolean existenciaUsuario(String userName) {
        Object[] param = { userName };

        boolean devolver = false;
        Map resultadoMap =
            (Map) this.find(Map.class, SQL_VALIDAR_EXISTENCIA_USUARIO, param);

        Number cantidad = (Number) resultadoMap.get("total");

        if (cantidad.intValue() == 1) {
            devolver = true;
        }
        return devolver;
    }

    /**
     * TODO Describir m�todo registrarEventoAuditoria.
     * @param idUsuario
     * @param id_funcionalidad
     * @param descripcion
     * @param date
     * @param i
     */
    public void registrarEventoAuditoria(int idUsuario,
        Integer id_funcionalidad, String descripcion, java.util.Date date, int i) {

        Object[] parametros =
            { idUsuario, id_funcionalidad, descripcion, date, i };

        //this.insertWithoutGeneratedKey(SQL_INSERT_AUDITORIA, parametros);

    }

    /**
     * TODO Describir m�todo usuarioInscrito.
     * @param userTO
     * @param clave
     * @return
     */
    public boolean usuarioInscrito(Usuario userTO, String clave) {
        Object[] param = { userTO.getUsername() };
        boolean resultado = false;
        Map result = (Map) this.find(Map.class, SQL_USUARIO_EN_LA_BASE, param);
        Number cantidad = (Number) result.get("cantidad");

        Integer cant = cantidad.intValue();
        if (cant == 1) {
            CryptoUtil cryptoUtil =
                new CryptoUtil(SeguridadConfig.getInstance());
            String claveDescriptada =
                cryptoUtil.getDecrypted(cryptoUtil.fromBase64String(clave));
            LDAPJumbo ldap = this.iniciaObjectoLdap();

            if (ldap.existeMember(userTO.getUsername(), 2)) {
                logger.info("Existe: " + userTO.getUsername());
            } else {
                logger.info("NO existe " + userTO.getUsername());
            }
            if (ldap.validaUsuario(userTO.getUsername(), claveDescriptada, 2)) {
                resultado = true;
            }
        }

        return resultado;
    }

    public UsuarioInterno getUsuarioInterno(Usuario userTO) {
        Object[] param = { userTO.getUsername() };
        UsuarioInterno usuarioInterno;
        usuarioInterno =
            (UsuarioInterno) this.find(UsuarioInterno.class,
                SQL_USUARIO_INTERNO, param);

        return usuarioInterno;
    }

    public int obtenerIdUsuario(String usuario) {
        Object[] param = { usuario };
        Map resultado =
            (Map) this.find(Map.class, SQL_USUARIO_OBTENER_ID, param);
        Number cantidad = (Number) resultado.get("id_usuario");

        return cantidad.intValue();
    }

    /**
     * Inicia el objeto para consultas a Ldap
     * @return LDAPJumbo
     */
    private LDAPJumbo iniciaObjectoLdap() {

        InitialContext ctx;
        String urljndi1;
        String urljndi2;
        String userGroupLdap;
        String passGroupLdap;
        String ldapGroup;

        LDAPJumbo ldap = null;

        try {
            ctx = new InitialContext();

            urljndi1 = (String) ctx.lookup("java:comp/env/url/ldap1");
            urljndi2 = (String) ctx.lookup("java:comp/env/url/ldap2");
            userGroupLdap = (String) ctx.lookup("java:comp/env/username/ldap");
            passGroupLdap = (String) ctx.lookup("java:comp/env/password/ldap");
            ldapGroup = (String) ctx.lookup("java:comp/env/group/ldap");

            ldap =
                new LDAPJumbo(urljndi1, urljndi2, userGroupLdap, passGroupLdap,
                    "5000", ldapGroup);

            ctx.close();
        } catch (NamingException e1) {
            e1.printStackTrace();
        }

        return ldap;
    }

    public boolean validacionLDAP(String userName, String password) {
        boolean esValido = false;
        LDAPJumbo ldap = iniciaObjectoLdap();

        esValido = ldap.validaUsuario(userName, password, 1);
        return esValido;
    }

    public List < Funcionalidad > getFuncionalidades(Usuario usuario) {
        // TODO Auto-generated method stub
        return null;
    }

    public Clave getUltimaClave(int idUsuario) {
        // TODO Auto-generated method stub
        return null;
    }

    public Usuario getUsuario(String username, String password, int tipo,
        Locale locale) {
        // TODO Auto-generated method stub
        return null;
    }

    public void updateClave(Clave clave) {
        throw new UnsupportedOperationException(
            "La operaci�n para actualizar clave no est� soportada.");
    }

    public String obtenerDatosEmail(String nombre) {
        // TODO Auto-generated method stub
        return null;
    }

    public void registrarEvento(Evento evento) {
        throw new UnsupportedOperationException(
            "La operacion de registro de evento de auditor�a no se encuentra soportada.");
    }

    public InputStream obtenerCuerpoEmail(String nombre) {
        // TODO Auto-generated method stub
        return null;
    }
}
