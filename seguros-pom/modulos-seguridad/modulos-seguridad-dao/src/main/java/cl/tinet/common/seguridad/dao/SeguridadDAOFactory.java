package cl.tinet.common.seguridad.dao;

import java.util.Locale;

import com.tinet.exceptions.system.SystemException;

import cl.tinet.common.dao.AbstractDAOFactory;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.dao.jdbc.SeguridadDAOJDBC;
import cl.tinet.common.seguridad.dao.jdbc.SeguridadDAOJDBCLDAP;
import cl.tinet.common.seguridad.model.Usuario;

/**
 * TODO Describir daofactory de seguridad.
 * @author tinet
 * @version 1.0
 * @created 27-Ene-2010 17:12:09
 */
public class SeguridadDAOFactory extends AbstractDAOFactory {

    /**
     * TODO Describir atributo DEFAULT_FACTORY_KEY.
     */
    public static final String DEFAULT_FACTORY_KEY =
        "cl.tinet.common.seguridad.dao.DEFAULT_FACTORY";

    /**
     * TODO Describir atributo SEGURIDAD_DAO_CLASS_KEY.
     */
    private static final String SEGURIDAD_DAO_CLASS_KEY =
        "cl.tinet.common.seguridad.dao.SEGURIDAD_DAO_CLASS";

    /**
     * TODO Describir m�todo getInstance.
     * 
     * @param config
     * @param locale
     * @return
     */
    public static SeguridadDAOFactory getInstance() {
        return (SeguridadDAOFactory) loadFactory(SeguridadDAOFactory.class,
            DEFAULT_FACTORY_KEY, SeguridadConfig.getInstance(), Locale
                .getDefault());
    }

    /**
     * TODO Describir m�todo getInstance.
     * 
     * @param config
     * @param locale
     * @return
     */
    public static SeguridadDAOFactory getInstance(Locale locale) {
        return (SeguridadDAOFactory) loadFactory(SeguridadDAOFactory.class,
            DEFAULT_FACTORY_KEY, SeguridadConfig.getInstance(), locale);
    }

    /**
     * TODO Describir m�todo getSeguridadDAO.
     * 
     * @param tipo
     * @return
     */
    public SeguridadDAO getSeguridadDAO(int tipo) {
        String className =
            getConfigurator().getString(SEGURIDAD_DAO_CLASS_KEY + "_" + tipo,
                getLocale());
        if (className == null) {
            if (tipo == Usuario.USUARIO_INTERNO) {
                return (SeguridadDAO) getDAO(SeguridadDAO.class,
                    SeguridadDAOJDBCLDAP.class);
            } else if (tipo == Usuario.USUARIO_EXTERNO) {
                return (SeguridadDAO) getDAO(SeguridadDAO.class,
                    SeguridadDAOJDBC.class);
            }
        }
        try {
            return (SeguridadDAO) getDAO(SeguridadDAO.class, Class
                .forName(className));
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        }
    }
}