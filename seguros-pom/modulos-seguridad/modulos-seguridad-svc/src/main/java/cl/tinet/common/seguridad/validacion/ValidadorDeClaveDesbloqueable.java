package cl.tinet.common.seguridad.validacion;

import java.util.Calendar;
import java.util.Date;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Usuario;

/**
 * TODO Falta descripcion de clase ValidadorDeClaveDesbloqueable.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class ValidadorDeClaveDesbloqueable extends ValidadorDeClaveNormal {

    /**
     * TODO Describir atributo DURACION_BLOQUEO_KEY.
     */
    public static final String DURACION_BLOQUEO_KEY =
        "cl.tinet.common.seguridad.validacion.DURACION_BLOQUEO";

    /**
     * TODO Describir atributo CAMPO_DURACION_BLOQUEO_KEY.
     */
    public static final String CAMPO_DURACION_BLOQUEO_KEY =
        "cl.tinet.common.seguridad.validacion.CAMPO_DURACION_BLOQUEO";

    /**
     * TODO Describir m�todo isClaveBloqueada.
     * @param usuario
     * @param clave
     * @return
     */
    @Override
    protected boolean isClaveBloqueada(Usuario usuario, Clave clave) {
        boolean bloqueada = super.isClaveBloqueada(usuario, clave);
        SeguridadConfig config = SeguridadConfig.getInstance();
        int estadoActivo =
            config.getInt(ESTADO_CLAVE_ACTIVA_KEY, getLocale());
        if (bloqueada) {
            Date fechaBloqueo = clave.getFechaBloqueo();
            if (fechaBloqueo == null) {
                fechaBloqueo = new Date();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaBloqueo);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            int duracion = config.getInt(DURACION_BLOQUEO_KEY, getLocale());
            int campo = config.getInt(CAMPO_DURACION_BLOQUEO_KEY, getLocale());
            calendar.add(campo, duracion);
           /* boolean asd = calendar.getTimeInMillis() < System.currentTimeMillis();
            System.out.println("VALIDADORDECLAVEDESBLOQUEALE: USERNAME"+usuario.getUsername()+"");
            System.out.println("VALIDADORDECLAVEDESBLOQUEALE:"+asd+"");*/
            if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
            	//System.out.println("VALIDADORDECLAVEDESBLOQUEALE:ENTRO A MILLIS ESTANDO ACTIVO USERNAME"+usuario.getUsername()+"");
                // Se debe establecer la clave como activa.
                bloqueada = false;
                clave.setEstado(estadoActivo);
                clave.setIntentos(0);
                getSeguridadDAO().updateClave(clave);
            }
        }
        return bloqueada;
    }
}
