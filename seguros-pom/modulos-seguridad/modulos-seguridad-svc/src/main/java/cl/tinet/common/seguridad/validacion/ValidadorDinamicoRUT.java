package cl.tinet.common.seguridad.validacion;

import java.util.List;
import java.util.regex.Pattern;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.exception.CredencialInvalidaException;
import cl.tinet.common.util.validate.ValidacionUtil;

/**
 * TODO Falta descripcion de clase ValidadorDinamicoRUT.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class ValidadorDinamicoRUT extends ValidadorDinamicoBase {

    /**
     * TODO Describir atributo FORMATO_RUT_INVALIDO_KEY.
     */
    public static final String FORMATO_RUT_INVALIDO_KEY =
        "cl.tinet.common.seguridad.exception.FORMATO_RUT_INVALIDO";

    /**
     * TODO Describir atributo FORMATO_RUT_INVALIDO_KEY.
     */
    public static final String FORMATO_RUT_INVALIDO_MOD11_KEY =
        "cl.tinet.common.seguridad.exception.FORMATO_RUT_INVALIDO_MOD11";

    /**
     * TODO Describir atributo PATRON_RUT_KEY.
     */
    public static final String PATRON_RUT_KEY =
        "cl.tinet.common.seguridad.validacion.PATRON_RUT";

    @Override
    protected void validarUsername(String username,
        List < CredencialInvalidaException > errores) {
        if (username == null) {
            errores.add(new CredencialInvalidaException(USERNAME_NULO_KEY));
        } else {
            Pattern pattern =
                Pattern.compile(SeguridadConfig.getInstance().getString(
                    PATRON_RUT_KEY, getLocale()));
            if (!pattern.matcher(username).matches()) {
                errores.add(new CredencialInvalidaException(
                    FORMATO_RUT_INVALIDO_KEY, username));
            } else {
                String[] partes = username.replaceAll("\\.", "").split("-");
                long numRUT = Long.parseLong(partes[0]);
                char digito = partes[1].toUpperCase().charAt(0);
                if (!ValidacionUtil.isValidoRUT(numRUT, digito)) {
                    errores.add(new CredencialInvalidaException(
                    		FORMATO_RUT_INVALIDO_MOD11_KEY, username));
                }
            }
        }
    }
}
