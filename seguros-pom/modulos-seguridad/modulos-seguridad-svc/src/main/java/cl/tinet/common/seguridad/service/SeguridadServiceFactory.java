package cl.tinet.common.seguridad.service;

import java.util.Locale;

import com.tinet.exceptions.system.SystemException;

import cl.tinet.common.seguridad.config.SeguridadConfig;

/**
 * TODO Falta descripcion de clase SeguridadServiceFactory.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 11, 2010
 */
public class SeguridadServiceFactory {

    /**
     * TODO Describir atributo AUTH_SERVICE_DEFAULT_KEY.
     */
    public static final String AUTH_SERVICE_DEFAULT_KEY =
        "cl.tinet.common.seguridad.service.AUTH_SERVICE_DEFAULT";

    /**
     * TODO Describir atributo AUTH_SERVICE_REAL_KEY.
     */
    public static final String AUTH_SERVICE_REAL_KEY =
        "cl.tinet.common.seguridad.service.AUTH_SERVICE_REAL";

    /**
     * TODO Describir atributo AUTH_SERVICE_REMOTE_KEY.
     */
    public static final String AUTH_SERVICE_REMOTE_KEY =
        "cl.tinet.common.seguridad.service.AUTH_SERVICE_REMOTE";

    /**
     * TODO Describir atributo instance.
     */
    private static SeguridadServiceFactory instance =
        new SeguridadServiceFactory();

    /**
     * TODO Describir m�todo getInstance.
     * @return
     */
    public static SeguridadServiceFactory getInstance() {
        return instance;
    }

    /**
     * TODO Describir m�todo getAutenticacionService.
     * @param tipo
     * @return
     */
    public SeguridadService getSeguridadService(String tipoKey,
        Locale locale) {
        String svcClass =
            SeguridadConfig.getInstance().getString(tipoKey, locale);
        if (svcClass == null) {
            svcClass = SeguridadServiceReal.class.getName();
        }
        try {
            return (SeguridadService) Class.forName(svcClass).newInstance();
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        } catch (IllegalAccessException iae) {
            throw new SystemException(iae);
        } catch (InstantiationException ie) {
            throw new SystemException(ie);
        }
    }
}
