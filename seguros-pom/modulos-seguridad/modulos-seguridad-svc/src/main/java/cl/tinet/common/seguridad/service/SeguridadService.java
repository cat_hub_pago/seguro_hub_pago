package cl.tinet.common.seguridad.service;

import java.util.List;
import java.util.Locale;

import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.Rol;
import cl.tinet.common.seguridad.model.Usuario;

/**
 * TODO Falta descripcion de clase SeguridadService.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 7, 2010
 */
public interface SeguridadService {

    /**
     * TODO Describir m�todo autenticar.
     * @param usuario
     * @param clave
     * @param tipoUsuario
     * @return
     * @throws AutenticacionException 
     */
    Usuario autenticar(String usuario, String clave, int tipoUsuario) throws AutenticacionException;

    /**
     * TODO Describir m�todo autenticar.
     * @param usuario
     * @param clave
     * @param tipoUsuario
     * @param locale
     * @return
     * @throws AutenticacionException 
     */
    Usuario autenticar(String usuario, String clave, int tipoUsuario,
        Locale locale) throws AutenticacionException;
    
    /**
     * TODO Describir m�todo autenticar.
     * @param usuario
     * @param tipo
     * @return
     * @throws AutenticacionException 
     */
    public Usuario getUsuario(String username, int tipo) throws AutenticacionException;

    /**
     * TODO Describir m�todo autorizar.
     * @param usuario
     * @param tipoUsuario
     * @return
     */
    List < Funcionalidad > obtenerFuncionalidades(Usuario usuario);

    /**
     * TODO Describir m�todo autorizar.
     * @param usuario
     * @param tipoUsuario
     * @param locale
     * @return
     */
    List < Funcionalidad > obtenerFuncionalidades(Usuario usuario, Locale locale);

    /**
     * TODO Describir m�todo obtenerRoles.
     * @param usuario
     * @return
     */
    List < Rol > obtenerRoles(Usuario usuario);

    /**
     * TODO Describir m�todo obtenerRoles.
     * @param usuario
     * @param locale
     * @return
     */
    List < Rol > obtenerRoles(Usuario usuario, Locale locale);
    
    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     */
    void registrarEvento(Evento evento);
    
    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     * @param locale
     */
    void registrarEvento(Evento evento, Locale locale);
}
