package cl.tinet.common.seguridad.filter;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.seguridad.util.UserAgentUtil;

/**
 * Filtro que realiza la verificacion para determinar si debe redireccionar la
 * solicitud a la version movil de la aplicaci�n o no.
 * <p>
 * Para ello utiliza la clase {@link UserAgentUtil}.
 * </p>
 * 
 * @see UserAgentUtil
 * 
 * @author rsanmartin
 */
public class MobileFilter implements Filter {

	/**
	 * Llave de configuracion (init-parameter en web.xml) asociado a la URL
	 * donde redireccionar� el filtro a los dispositivos m�viles que accedan a
	 * la aplicaci�n.
	 * <p>
	 * El string de la URL de redireccion soporta especificar los siguientes
	 * parametros:
	 * <ul>
	 * <li>Par�metro 0: Esquema de la solicitud (http o https).</li>
	 * <li>Par�metro 1: Nombre del servidor de la solicitud.</li>
	 * <li>Par�metro 2: Puerto de la solicitud.</li>
	 * </ul>
	 * </p>
	 * <p>
	 * Un posible ejemplo de la URL de redirecci�n es:
	 * <code>{0}://{1}:{2,number,#}/soap/mobile/home.do</code> Y ser�a reemplazado
	 * por: <code>http://localhost:9080/soap/mobile/home.do</code>
	 * </p>
	 * 
	 * @see MessageFormat
	 */
	public static final String MOBILE_HOME_REDIRECT_KEY = "MOBILE_HOME_REDIRECT";

	/**
	 * Llave de configuraci�n (init-parameter en web.xml) asociado a los
	 * patrones de URL que ser�n exluidos del filtrado.
	 * <p>
	 * Esto con el fin de no filtrar algunar URL particulares ya sea por que
	 * deben quedar excluidas por requerimiento de negocio, o por que son parte
	 * del m�dulo destinado a incluir las p�ginas de dispositivos m�viles.
	 * </p>
	 * <p>
	 * Cada patron dentro del listado de patrones debe estar separado del resto
	 * por una coma (,).
	 * </p>
	 */
	public static final String MOBILE_EXCLUDED_PATTERNS_KEY = "MOBILE_EXCLUDED_PATTERNS";

	/**
	 * Arreglo de patrones a excluir vac�o. En caso de que no se especifique el
	 * parametro de inicializacion de patrones exluidos
	 * {@value #MOBILE_EXCLUDED_PATTERNS_KEY}, Se inicializa el listado de
	 * patrones de exclusion con el valor de este arreglo vac�o.
	 */
	private static final Pattern[] NO_PATTERNS = {};

	/**
	 * Variable de acceso al log del sistema.
	 */
	private static Log logger = LogFactory.getLog(MobileFilter.class);

	/**
	 * Url de redireccion al home para dispositivos m�viles.
	 */
	private String mobileHome;
	
	/**
	 * Url de redireccion cuando el dispositivo es BlackBerry con OS inferior a OS7.
	 */
	public static final String MOBILE_DEFAULT = "/soap/mobile/jsp/default.jsp";
	
		
	/**
	 * Patrones a excluir de la redireccion.
	 */
	private Pattern[] excludedPatterns;

	/**
	 * Destruye la instancia de filtro.
	 */
	public void destroy() {
		logger.info("Filtro de deteccion de moviles destruido.");
		
	}

	/**
	 * Realiza la l�gica de redireccion para dispositivos m�viles.
	 * 
	 * @see UserAgentUtil
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		logger.debug("Solicitud recibida.");
		if (req instanceof HttpServletRequest) {
			logger.debug("Validando si es que proviene desde dispositivo movil.");
			HttpServletRequest request = (HttpServletRequest) req;
			HttpServletResponse response = (HttpServletResponse) res;
			
			if (UserAgentUtil.isMobileDevice(request)) {
				String requestURI = request.getRequestURI();

				if(UserAgentUtil.isOSAccepted(request)) {
					if (!isExcludedURI(requestURI)) {
					
						String redirection = MessageFormat.format(mobileHome,
								request.getScheme(), request.getServerName(),
								request.getServerPort());
						if (logger.isDebugEnabled()) {
							logger.debug("Ruta de redireccion: " + redirection);
						}
						response.sendRedirect(redirection);
						return;
						
					}
				} else {
					String urlDesvio = request.getScheme() + "://" + 
			        	request.getServerName() + ":" +	request.getServerPort() + 
			        		MOBILE_DEFAULT ;
					if (!isExcludedURI(requestURI)) {
						response.sendRedirect(urlDesvio);
						return;	
					}
					if(requestURI.toLowerCase().indexOf("mobile")>=0 && !(requestURI.toLowerCase().indexOf(MOBILE_DEFAULT)>=0)){
						response.sendRedirect(urlDesvio);
						return;	
					}
				}
			} 	

		}
		logger.debug("No es dispositivo movil. Continua procesamiento.");
		chain.doFilter(req, res);
	}

	/**
	 * Incializa el filtro rescatando los par�metros de inicializacion.
	 * 
	 * @see #MOBILE_HOME_REDIRECT_KEY
	 * @see #MOBILE_EXCLUDED_PATTERNS_KEY
	 */
	public void init(FilterConfig config) throws ServletException {
		logger.debug("Inicializando filtro de deteccion de moviles.");
		String param = config.getInitParameter(MOBILE_HOME_REDIRECT_KEY);
		if (param == null) {
			logger.error("No se configur� home de redireccion para moviles.");
			throw new ServletException(
					"Debe inicializar filtro MobileFilter. Defina init-parameter "
							+ MOBILE_HOME_REDIRECT_KEY);
		}
		this.mobileHome = param;
		this.excludedPatterns = NO_PATTERNS;
		param = config.getInitParameter(MOBILE_EXCLUDED_PATTERNS_KEY);
		if (param != null) {
			String[] items = param.split("\\s*,\\s*");
			excludedPatterns = new Pattern[items.length];
			for (int i = 0; i < items.length; i++) {
				excludedPatterns[i] = Pattern.compile(items[i]);
			}
		}
		logger.info("Inicializacion de filtro de deteccion de moviles completada.");
		
		
	}
	
	private boolean isExcludedURI(String requestURI) {
		for (int i = 0; i < this.excludedPatterns.length; i++) {
			if (this.excludedPatterns[i].matcher(requestURI).matches()) {
				return true;
			}
		}
		return false;
	}
		
}
