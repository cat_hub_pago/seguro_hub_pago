package cl.tinet.common.seguridad.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.config.SeguridadErrorConfig;
import cl.tinet.common.seguridad.delegate.GestorDeSeguridadDelegate;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.util.crypto.CryptoUtil;
import cl.tinet.common.util.validate.ValidacionUtil;

/**
 * Servlet de autenticaci�n.
 * <p>
 * Permite efectuar la autenticaci�n del usuario utilizando la API de
 * autenticaci�n. Permite configuraci�n mediante variables de inicializaci�n
 * en archivo web.xml para determinar la p�gina a redirigir una vez que el
 * login es exitoso, o la p�gina a redirigir en caso de login fallido
 * (o para reintentar).
 * </p>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Ene-2010 17:08:13
 */
public class AutenticacionServlet extends HttpServlet {

    /**
     * TODO Describir atributo TIPO_USUARIO_PARAM_KEY.
     */
    public static final String TIPO_USUARIO_PARAM_KEY =
        "cl.tinet.common.seguridad.servlet.TIPO_USUARIO";

    /**
     * TODO Describir atributo URI_EXITO_PARAM_KEY.
     */
    public static final String URI_EXITO_PARAM_KEY =
        "cl.tinet.common.seguridad.servlet.URI_EXITO";

    /**
     * TODO Describir atributo URI_CAMBIO_CLAVE_PARAM_KEY.
     */
    public static final String URI_CAMBIO_CLAVE_PARAM_KEY =
        "cl.tinet.common.seguridad.servlet.URI_CAMBIO_CLAVE";

    /**
     * TODO Describir atributo URI_FRACASO_PARAM_KEY.
     */
    public static final String URI_FRACASO_PARAM_KEY =
        "cl.tinet.common.seguridad.servlet.URI_FRACASO";

    /**
     * TODO Describir atributo AUDITORIA_ACTIVADA_PARAM_KEY.
     */
    public static final String AUDITORIA_ACTIVADA_PARAM_KEY =
        "cl.tinet.common.seguridad.AUDITORIA_ACTIVADA";

    /**
     * TODO Describir atributo SEPARADOR.
     */
    private static final String SEPARADOR = "/";

    /**
     * Versi�n de la clase para serializaci�n.
     */
    private static final long serialVersionUID = -2556545434185702496L;

    private static final String RUT_INVALIDO =
        "cl.tinet.common.seguridad.error.RUT_INVALIDO";

    private static final String CLAVE_INVALIDA =
        "cl.tinet.common.seguridad.error.CLAVE_INVALIDA";

    private static final String CAMPOS_VACIOS =
        "cl.tinet.common.seguridad.error.CAMPOS_VACIOS";

    /**
     * TODO Describir atributo ERROR_AUTENTICACION_KEY.
     */
    public static final String ERROR_AUTENTICACION_KEY =
        "cl.tinet.common.seguridad.ERROR_AUTENTICACION_KEY";

    /**
     * Tipo de Usuario.
     */
    private int tipoUsuario;

    /**
     * TODO Describir atributo uriExito.
     */
    private String uriExito;

    /**
     * TODO Describir atributo uriCambiarClave.
     */
    private String uriCambiarClave;

    /**
     * TODO Describir atributo uriFracaso.
     */
    private String uriFracaso;

    /**
     * TODO Describir atributo auditoriaActivada.
     */
    private boolean auditoriaActivada = true;

    /**
     * Pagina de inicio.
     */
    private String paginaInicio;

    /**
     * Pagina de cliente no registrado.
     */
    private String paginaClienteNoRegistrado;

    /**
     * Pagina de cuenta (PSW) no activa.
     */
    private String paginaCuentaNoActiva; // PAGINA_CUENTA_NO_ACTIVA

    /**
     * Pagina de adventencia de intentos fallidos.
     */
    private String paginaIntentosFallidos; //PAGINA_INTENTOS_FALLIDOS

    /**
     * Pagina de cambio de pagina.
     */
    private String paginaCambioClave; //PAGINA_CAMBIO_CLAVE

    /**
     * Pagina de usuario desactivado.
     */
    private String paginaUsuarioDesactivado;

    /**
     * Pagina en caso de error.
     */
    private String paginaError;

    /**
     * Variable de acceso al log.
     */
    private static Log logger = LogFactory.getLog(AutenticacionServlet.class);

    /**
     * Constructor de la clase.
     */
    public AutenticacionServlet() {

    }

    @Override
    public void init() throws ServletException {
        logger.info("Inicializando servlet de login...");
        String parametro = getInitParameter(TIPO_USUARIO_PARAM_KEY);
        if (parametro == null) {
            throw new ServletException(
                "Debe especificar tipo de usuario en configuracion.");
        }
        this.tipoUsuario = Integer.parseInt(parametro);
        this.uriExito = getInitParameter(URI_EXITO_PARAM_KEY);
        if (this.uriExito == null) {
            throw new ServletException("Debe especificar URI de exito.");
        }
        this.uriCambiarClave = getInitParameter(URI_CAMBIO_CLAVE_PARAM_KEY);
        if (this.uriCambiarClave == null) {
            throw new ServletException(
                "Debe configurar URI de cambio de clave.");
        }
        this.uriFracaso = getInitParameter(URI_FRACASO_PARAM_KEY);
        if (this.uriFracaso == null) {
            throw new ServletException("Debe configurar URI de fracaso.");
        }
        parametro = getInitParameter(AUDITORIA_ACTIVADA_PARAM_KEY);
        if (parametro != null) {
            this.auditoriaActivada = Boolean.parseBoolean(parametro);
        }

        // FIXME Eliminar estas p�ginas...
        this.paginaInicio =
            this
                .getInitParameter("cl.tinet.common.seguridad.servlet.PAGINA_INICIO");

        this.paginaClienteNoRegistrado =
            this.getInitParameter("cl.tinet.common.seguridad.servlet."
                + "PAGINA_CLIENTE_NO_REGISTRADO");

        this.paginaCuentaNoActiva =
            this.getInitParameter("cl.tinet.common.seguridad.servlet."
                + "PAGINA_CUENTA_NO_ACTIVA");

        this.paginaIntentosFallidos =
            this.getInitParameter("cl.tinet.common.seguridad.servlet."
                + "PAGINA_INTENTOS_FALLIDOS");

        this.paginaCambioClave =
            this.getInitParameter("cl.tinet.common.seguridad.servlet."
                + "PAGINA_CAMBIO_CLAVE");

        this.paginaUsuarioDesactivado =
            this.getInitParameter("cl.tinet.common.seguridad.servlet."
                + "PAGINA_USUARIO_DESACTIVADO");

        this.paginaError =
            this.getInitParameter("cl.tinet.common.seguridad.servlet."
                + "ERROR_LOGIN");

    }

    /**
     * TODO Describir m�todo doPost.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        Locale locale = request.getLocale();
        String destino =
            obtenerDestino(URI_FRACASO_PARAM_KEY, request, this.uriFracaso);
        boolean redirect = true;
        try {
            Usuario usuario =
                SeguridadUtil.login(request, response, tipoUsuario, locale);
            destino =
                obtenerDestino(URI_EXITO_PARAM_KEY, request, this.uriExito);
            request.getSession().setAttribute("exito", destino);
            if (usuario.isCambioClaveRequerido()) {
                destino =
                    obtenerDestino(URI_CAMBIO_CLAVE_PARAM_KEY, request,
                        this.uriCambiarClave);
            }
        } catch (AutenticacionException e) {
            logger.debug("Error durante la autenticacion de usuario.", e);
            request.setAttribute(ERROR_AUTENTICACION_KEY, e.getMessage(locale).replaceAll("\n", ""));
            redirect = false;
        }
        if (!destino.startsWith(SEPARADOR)) {
            destino = SEPARADOR.concat(destino);
        }
        if (redirect) {
            destino = request.getContextPath().concat(destino);
            response.sendRedirect(destino);
        } else {
            request.getRequestDispatcher(destino).forward(request, response);
        }
    }

    /**
     * TODO Describir m�todo obtenerDestino.
     * @param parametro
     * @param request
     * @param predeterminado
     * @return
     */
    private String obtenerDestino(String parametro, HttpServletRequest request,
        String predeterminado) {
        String destino = request.getParameter(parametro);
        if (destino == null) {
            destino = predeterminado;
        }
        return destino;
    }

    /**
     * Metodo que realiza las validaciones del usuario externo.
     * @param rut rut del usuario
     * @param dv digito verificador
     * @param clave clave del usuario.
     * @param request request recibido.
     * @param response response recibido.
     * @throws IOException En caso de problemas de IO.
     * @throws ServletException En caso de problemas del servlet.
     */
    private void autenticacionDeUsuario(String userName, String clave,
        HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

        Usuario usuario = new Usuario();
        GestorDeSeguridadDelegate delegateSeguridad =
            new GestorDeSeguridadDelegate();

        CryptoUtil cryptoUtil = new CryptoUtil(SeguridadConfig.getInstance());
        clave = CryptoUtil.toBase64String(cryptoUtil.getEncrypted(clave), true);

        HttpSession session = request.getSession();

        try {
            usuario =
                delegateSeguridad.autenticar(userName, clave, tipoUsuario,
                    new Long(1));

            RequestDispatcher dispatcher;

            switch (usuario.getResultadoValidacion()) {

                case 1:
                    session.setAttribute("usuarioAutenticadoServlet", usuario);
                    delegateSeguridad.registrarEventoAuditoria(usuario
                        .getTipo(), usuario.getIdUsuario(), 3,
                        "El usuario se logea", new Date(), 1);
                    response.sendRedirect(request.getContextPath() + SEPARADOR
                        + paginaInicio);
                    break;

                case 2:
                    dispatcher =
                        getServletContext().getRequestDispatcher(
                            paginaClienteNoRegistrado);
                    dispatcher.forward(request, response);
                    break;

                case 3:
                    dispatcher =
                        getServletContext().getRequestDispatcher(
                            paginaCuentaNoActiva);
                    delegateSeguridad.registrarEventoAuditoria(usuario
                        .getTipo(), usuario.getIdUsuario(), 3,
                        "El usuario esta bloqueado", new Date(), 1);
                    dispatcher.forward(request, response);
                    break;

                case 4:
                    dispatcher =
                        getServletContext().getRequestDispatcher(
                            paginaIntentosFallidos);
                    dispatcher.forward(request, response);
                    delegateSeguridad.registrarEventoAuditoria(usuario
                        .getTipo(), usuario.getIdUsuario(), 2,
                        "El usuario falla al ingresar al login", new Date(), 1);

                    break;

                case 5:
                    SeguridadUtil.setAutenticado(usuario, request);
                    // TODO Buscar y eliminar referencias a siguiente variable en sesi�n (en java y JSP).
                    session.setAttribute("usuarioAutenticadoServlet", usuario);
                    dispatcher =
                        getServletContext().getRequestDispatcher(
                            paginaCambioClave);
                    dispatcher.forward(request, response);
                    break;
                case 6:
                    dispatcher =
                        getServletContext().getRequestDispatcher(
                            paginaCambioClave);
                    dispatcher.forward(request, response);

            }

        } catch (AutenticacionException e) {
            throw new ServletException();
        } catch (NullPointerException n) {
            throw new ServletException();
        }
    }

    /**
     * TODO Describir m�todo validacionLoginFront.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void validacionLoginFront(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher dispatcher =
            getServletContext().getRequestDispatcher(paginaError);
        String mensajeError = "";

        String rut = request.getParameter("rut");
        String dv = request.getParameter("dv");
        String clave = request.getParameter("clave");

        logger.debug("rut: " + rut);
        logger.debug("dv: " + dv);

        //Validacion Rut.
        if (rut != null && rut.length() > 0) {

            if (dv != null & dv.length() > 0) {
                try {
                    long numRut = Long.parseLong(rut);

                    if (!ValidacionUtil.isValidoRUT(numRut, dv.charAt(0))) {
                        // Rut invalido.
                        mensajeError =
                            SeguridadErrorConfig.getInstance().getString(
                                RUT_INVALIDO);
                        request.setAttribute("ERROR_LOGIN", mensajeError);
                        dispatcher.forward(request, response);

                    } else {

                        // Validacion password
                        if (clave != null && clave.length() > 0) {

                            // Validaci�n de Usuario
                            String userName = rut + "-" + dv;
                            this.autenticacionDeUsuario(userName, clave,
                                request, response);

                        } else {
                            mensajeError =
                                SeguridadErrorConfig.getInstance().getString(
                                    CAMPOS_VACIOS);
                            request.setAttribute("ERROR_LOGIN", mensajeError);
                            dispatcher.forward(request, response);
                        }
                    }
                } catch (NumberFormatException ne) {
                    mensajeError =
                        SeguridadErrorConfig.getInstance().getString(
                            RUT_INVALIDO);
                    request.setAttribute("ERROR_LOGIN", mensajeError);
                    dispatcher.forward(request, response);
                }
            } else {
                // DV vacio.
                mensajeError =
                    SeguridadErrorConfig.getInstance().getString(CAMPOS_VACIOS);
                request.setAttribute("ERROR_LOGIN", mensajeError);
                dispatcher.forward(request, response);

            }
        } else {
            // Rut vacio.
            mensajeError =
                SeguridadErrorConfig.getInstance().getString(CAMPOS_VACIOS);
            request.setAttribute("ERROR_LOGIN", mensajeError);
            dispatcher.forward(request, response);
        }
    }

    /**
     * TODO Describir m�todo validacionLoginBack.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void validacionLoginBack(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher dispatcher =
            getServletContext().getRequestDispatcher(paginaError);
        String mensajeError = "";

        String userName = request.getParameter("userName");
        String clave = request.getParameter("clave");

        logger.debug("userName: " + userName);

        //Validacion user name.
        if (userName != null && userName.length() > 0) {

            // Validacion password
            if (clave != null && clave.length() > 0) {
                this.autenticacionDeUsuario(userName, clave, request, response);

            } else {
                mensajeError =
                    SeguridadErrorConfig.getInstance().getString(CAMPOS_VACIOS);
                request.setAttribute("ERROR_LOGIN", mensajeError);
                dispatcher.forward(request, response);
            }
        } else {
            // User Name Vacio.
            mensajeError =
                SeguridadErrorConfig.getInstance().getString(CAMPOS_VACIOS);
            request.setAttribute("ERROR_LOGIN", mensajeError);
            dispatcher.forward(request, response);
        }

    }

}
