package cl.tinet.common.seguridad.filter;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.service.SeguridadService;
import cl.tinet.common.seguridad.service.SeguridadServiceFactory;
import cl.tinet.common.seguridad.util.SeguridadUtil;

/**
 * Filtro de autorizaci�n.
 * <p>
 * Valida que el usuario actual posea los permisos suficientes para realizar
 * la solicitud actual. En caso de que el usuario no se encuentre autenticado,
 * env�a al usuario el c�digo {@link HttpServletResponse#SC_UNAUTHORIZED}
 * (401).</p>
 * <p>
 * En caso de que el usuario no tenga permisos necesarios para cumplir con su
 * solicitud, se env�a c�digo {@link HttpServletResponse#SC_FORBIDDEN} (403),
 * indicando que el usuario no est� autorizado a visualizar la p�gina.
 * </p>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Ene-2010 17:08:13
 */
public class AutorizacionFilter implements Filter {

    /**
     * Llave de configuraci�n para indicar que el filtro de autorizaci�n
     * se encuentra o no activado. Corresponde al nombre de un par�metro
     * de inicio en el archivo web.xml.
     */
    public static final String LLAVE_ACTIVACION =
        "cl.tinet.common.seguridad.ACTIVADO";

    /**
     * TODO Describir atributo LLAVE_FILTRO_URI_ACTIVO.
     */
    public static final String LLAVE_FILTRO_URI_ACTIVADO =
        "cl.tinet.common.seguridad.FILTRO_URI_ACTIVADO";

    /**
     * TODO Describir atributo LLAVE_AUDITORIA_ACTIVADA.
     */
    private static final String LLAVE_AUDITORIA_ACTIVADA =
        "cl.tinet.common.seguridad.AUDITORIA_ACTIVADA";

    /**
     * TODO Describir atributo LLAVE_IGNORAR_CONTEXTO_URI.
     */
    public static final String LLAVE_IGNORAR_CONTEXTO_URI =
        "cl.tinet.common.seguridad.IGNORAR_CONTEXTO_URI";

    /**
     * TODO Describir atributo LLAVE_PATRONES_FILTRADOS.
     */
    public static final String LLAVE_PATRONES_FILTRADOS =
        "cl.tinet.common.seguridad.PATRONES_FILTRADOS";

    /**
     * TODO Describir atributo LLAVE_MENSAJE_EVENTO_ACCESO.
     */
    private static final String LLAVE_MENSAJE_EVENTO_ACCESO =
        "cl.tinet.common.seguridad.MENSAJE_EVENTO_ACCESO";

    /**
     * TODO Describir atributo SIN_PATRONES.
     */
    private static final Pattern[] SIN_PATRONES = {};

    /**
     * Variable de acceso a log.
     */
    private static Log logger = LogFactory.getLog(AutorizacionFilter.class);

    /**
     * Indicador de si el filtro de seguridad se encuentra o no activado
     * para una determinada aplicaci�n web.
     * <p>
     * Puede ser configurado a trav�s del par�metro de inicializaci�n
     * {@value #LLAVE_ACTIVACION}. Su valor predeterminado es
     * <code>true</code>.</p>
     */
    private boolean activado = true;

    /**
     * TODO Describir atributo autorizacionFuncional.
     */
    private boolean filtroURIActivado = true;

    /**
     * TODO Describir atributo auditoriaActivada.
     */
    private boolean auditoriaActivada = true;

    /**
     * TODO Describir atributo patronesFiltrados.
     */
    private Pattern[] patronesFiltrados = SIN_PATRONES;

    /**
     * TODO Describir atributo ignorarContexto.
     */
    private boolean ignorarContextoURI = true;

    /**
     * Inicializa el filtro de autorizaci�n.
     * 
     * @param config datos de configuraci�n del filtro.
     * @throws ServletException en caso de un error durante la inicializacion.
     */
    public void init(FilterConfig config) throws ServletException {
        logger.info("Inicializando filtro de autorizacion...");
        String parametro = config.getInitParameter(LLAVE_ACTIVACION);
        if (parametro != null) {
            this.activado = Boolean.parseBoolean(parametro);
        }
        parametro = config.getInitParameter(LLAVE_FILTRO_URI_ACTIVADO);
        if (parametro != null) {
            this.filtroURIActivado = Boolean.parseBoolean(parametro);
        }
        parametro = config.getInitParameter(LLAVE_AUDITORIA_ACTIVADA);
        if (parametro != null) {
            this.auditoriaActivada = Boolean.parseBoolean(parametro);
        }
        parametro = config.getInitParameter(LLAVE_PATRONES_FILTRADOS);
        String[] patrones = null;
        if (parametro != null) {
            patrones = parametro.split("\\s*,\\s*");
            this.patronesFiltrados = new Pattern[patrones.length];
            for (int i = 0; i < patrones.length; i++) {
                this.patronesFiltrados[i] = Pattern.compile(patrones[i]);
            }
        }
        parametro = config.getInitParameter(LLAVE_IGNORAR_CONTEXTO_URI);
        if (parametro != null) {
            this.ignorarContextoURI = Boolean.parseBoolean(parametro);
        }
        if (logger.isInfoEnabled()) {
            logger.info("Filtro de autorizacion inicializado.");
            logger.info("Filtro activado    : " + this.activado);
            logger.info("Filtro URI activado: " + this.filtroURIActivado);
            logger.info("Auditor�a activada : " + this.auditoriaActivada);
            logger.info("Patrones URI       : " + Arrays.toString(patrones));
            logger.info("Ignorar contexto   : " + this.ignorarContextoURI);
        }
    }

    /**
     * Operaci�n de filtrado.
     * <p>
     * Filtra las solicitudes realizadas por el usuario, de manera que este
     * s�lo pueda acceder a las funcionalidades a las que tiene permisos.
     * </p>
     * @param req datos de la solicitud del usuario.
     * @param res datos de la respuesta al usuario.
     * @param chain cadena de filtros a ejecutar.
     * @throws IOException en caso de error enviando la respuesta al usuario.
     * @throws ServletException en caso de un error generando la respuesta al
     *          usuario.
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        boolean continuar = true;
        if (this.activado && (req instanceof HttpServletRequest) && (res instanceof HttpServletResponse)) {
            HttpServletResponse response = (HttpServletResponse) res;
            HttpServletRequest request = (HttpServletRequest) req;
            HttpSession session = request.getSession(false);
            Usuario usuario = null;
            Funcionalidad funcionalidad = null;
            Locale locale = request.getLocale();

            // Validar si corresponde a compra sin registro
        	if(request.getSession().getAttribute("rutSinRegistro") == null){
                // Se obtiene la URI de la solicitud.
                String uri = request.getRequestURI();
                if (this.ignorarContextoURI) {
                    uri = uri.replaceFirst(request.getContextPath(), "");
                }

                continuar = (session != null);

                if (continuar) {
                    logger.info("Sesion existe. Se verifican permisos");

                    // Se verifica que el usuario se encuentre autenticado.
                    usuario = SeguridadUtil.getUsuario(request);
                    continuar = (usuario != null);

                    if (continuar) {
                        logger.info("Usuario autenticado. Continua validacion");

                        // Se verifica que el filtro de URI se encuentre activo.
                        if (this.filtroURIActivado) {
                            // Se asume que debe filtrar todo.
                            boolean debeFiltrar = true;
                            if (patronesFiltrados.length > 0) {
                                debeFiltrar = false;
                                for (int i = 0; i < patronesFiltrados.length; i++) {
                                    if (patronesFiltrados[i].matcher(uri).matches()) {
                                        debeFiltrar = true;
                                        break;
                                    }
                                }
                            }

                            // Se filtra solo las URL configuradas.
                            if (debeFiltrar || this.auditoriaActivada) {
                                Map < String, Funcionalidad > funcionMap =
                                    SeguridadUtil.getFuncionMapURI(request);
                                funcionalidad = funcionMap.get(uri);
                                if (debeFiltrar) {
                                    continuar = (funcionalidad != null);
                                }
                            }
                        }
                    }
                }

                // Se verifica si es que no esta autorizado para continuar.
                if (!continuar) {
                    logger.info("No autorizado. Reportando error");
                    int sc = HttpServletResponse.SC_UNAUTHORIZED;
                    if (usuario != null) {
                        sc = HttpServletResponse.SC_FORBIDDEN;
                    }
                    response.sendError(sc);
                    return;
                }

                // Se registra evento en el log de auditor�a.
                logger.debug("Registrando acceso exitoso en log de auditor�a");
                SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
                SeguridadService svc = factory.getSeguridadService(SeguridadServiceFactory.AUTH_SERVICE_DEFAULT_KEY, locale);
                String descripcion = SeguridadConfig.getInstance().getString(LLAVE_MENSAJE_EVENTO_ACCESO, locale);
                descripcion = MessageFormat.format(descripcion, usuario, funcionalidad);
                logger.debug(descripcion);
                svc.registrarEvento(new Evento(usuario, funcionalidad, descripcion), locale);
                logger.info("Acceso exitoso registrado");
        	}
        	logger.info("Acceso compra sin registro");
        }
        chain.doFilter(req, res);
    }

    /**
     * Destruye el filtro de autorizaci�n.
     */
    public void destroy() {
        logger.info("Filtro de autorizaci�n destruido");
    }
}