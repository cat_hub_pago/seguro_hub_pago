package cl.tinet.common.seguridad.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.Rol;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.service.SeguridadService;
import cl.tinet.common.seguridad.service.SeguridadServiceFactory;

/**
 * TODO Falta descripcion de clase SeguridadUtil.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 5, 2010
 */
public class SeguridadUtil {

    /**
     * TODO Describir atributo USUARIO_CONECTADO_KEY.
     */
    public static final String USUARIO_CONECTADO_KEY =
        "cl.tinet.common.seguridad.USUARIO_CONECTADO";

    /**
     * TODO Describir atributo FUNCIONALIDAD_MAP_KEY.
     */
    public static final String FUNCIONALIDAD_URL_MAP_KEY =
        "cl.tinet.common.seguridad.FUNCIONALIDAD_URL_MAP";

    /**
     * TODO Describir atributo FUNCIONALIDAD_MAP_KEY.
     */
    public static final String FUNCIONALIDAD_MAP_KEY =
        "cl.tinet.common.seguridad.FUNCIONALIDAD_MAP";

    /**
     * TODO Describir atributo logger.
     */
    private static Log logger = LogFactory.getLog(SeguridadUtil.class);

    /**
     * TODO Describir atributo USERNAME_PARAM_KEY.
     */
    public static final String USERNAME_PARAM_KEY = "username";

    /**
     * TODO Describir atributo PASSWORD_PARAM_KEY.
     */
    public static final String PASSWORD_PARAM_KEY = "password";
    
    public static final String REPORTES_SUBSTRING = "REP_";
    public static final int TIPO_USUARIO_REPORTES = 3;

    /**
     * TODO Describir constructor de SeguridadUtil.
     */
    protected SeguridadUtil() {
    }

    /**
     * TODO Describir m�todo isAutenticado.
     * @param request
     * @return
     */
    public static boolean isAutenticado(HttpServletRequest request) {
        return (getUsuario(request) != null);
    }

    /**
     * TODO Describir m�todo setAutenticado.
     * @param usuario
     * @param request
     */
    public static void setAutenticado(Usuario usuario,
        HttpServletRequest request) {
        if (usuario == null) {
            throw new NullPointerException("El usuario no debe ser nulo.");
        }
        request.getSession().setAttribute(USUARIO_CONECTADO_KEY, usuario);
    }

    /**
     * TODO Describir m�todo isAutorizado.
     * @param rol
     * @param request
     * @return
     */
    public static boolean isAutorizado(Rol rol, HttpServletRequest request) {
        throw new UnsupportedOperationException(
            "Operaci�n no soportada en esta versi�n de la API.");
    }

    /**
     * TODO Describir m�todo isAutorizado.
     * @param funcion
     * @param request
     * @return
     */
    public static boolean isAutorizado(Funcionalidad funcion,
        HttpServletRequest request) {
        if (funcion == null) {
            throw new NullPointerException("La funcionalidad no debe ser nula.");   
        }
        return isAutorizadoURI(funcion.getUrl(), request);
    }

    /**
     * TODO Describir m�todo isAutorizado.
     * @param request
     * @return
     */
    public static boolean isAutorizado(HttpServletRequest request) {
        if (request == null) {
            throw new NullPointerException("El request no debe ser nulo.");
        }
        String uri =
            request.getRequestURI().replaceFirst(request.getContextPath(), "");
        return isAutorizadoURI(uri, request);
    }

    /**
     * TODO Describir m�todo isAutorizadoURL.
     * @param url
     * @param request
     * @return
     */
    public static boolean isAutorizadoURI(String url, HttpServletRequest request) {
        if (url == null) {
            throw new NullPointerException(
                "La url especificada no debe ser nula.");
        }
        if (request == null) {
            throw new NullPointerException("El request no debe ser nulo.");
        }
        Map < ?, ? > funcionMap = getFuncionMapURI(request);
        return funcionMap.containsKey(url);
    }

    /**
     * TODO Describir m�todo getFuncionMapURI.
     * @param request
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map < String, Funcionalidad > getFuncionMapURI(
        HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        Map < String, Funcionalidad > funcionMap = null;
        if (session != null) {
            funcionMap =
                (Map < String, Funcionalidad >) session
                    .getAttribute(FUNCIONALIDAD_URL_MAP_KEY);
        }
        if (funcionMap == null) {
            funcionMap = Collections.emptyMap();
        }
        return funcionMap;
    }

    /**
     * TODO Describir m�todo setAutorizado.
     * @param funcionList
     * @param request
     */
    public static void setAutorizado(List < Funcionalidad > funcionList,
        HttpServletRequest request) {
        if (isAutenticado(request)) {
            logger.debug("Estableciendo permisos del usuario.");
            HttpSession session = request.getSession();
            Map < String, Funcionalidad > funcionMap =
                new HashMap < String, Funcionalidad >();
            Map < String, Funcionalidad > funcionURLMap =
                new HashMap < String, Funcionalidad >();
            for (Funcionalidad funcion : funcionList) {
                funcionMap.put(funcion.getNombre(), funcion);
                funcionURLMap.put(funcion.getUrl(), funcion);
            }
            session.setAttribute(FUNCIONALIDAD_URL_MAP_KEY, funcionURLMap);
            session.setAttribute(FUNCIONALIDAD_MAP_KEY, funcionMap);
            logger.debug("Permisos establecidos exitosamente.");
        } else {
            logger.debug("Usuario no autenticado. No se establecen permisos.");
        }
    }

    /**
     * TODO Describir m�todo getUsuario.
     * @param request
     * @return
     */
    public static Usuario getUsuario(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            Object usuario = session.getAttribute(USUARIO_CONECTADO_KEY);
            logger.info("Usuario conectado: " + usuario); 
            return (Usuario) usuario;
        }
        logger.info("No existe sesion. Usuario no autenticado");
        return null;
    }

    /**
     * TODO Describir m�todo login.
     * @param request
     * @param response
     * @param tipo
     * @throws AutenticacionException 
     */
    public static void login(HttpServletRequest request,
        HttpServletResponse response, int tipo) throws AutenticacionException {
        login(request, response, tipo, request.getLocale());
    }

    /**
     * TODO Describir m�todo verificarAutenticacion.
     * @param request
     * @param response
     * @param tipo
     * @param locale
     * @throws AutenticacionException 
     */
    public static Usuario login(HttpServletRequest request,
        HttpServletResponse response, int tipo, Locale locale)
        throws AutenticacionException {
        String username = request.getParameter(USERNAME_PARAM_KEY);
        String password = request.getParameter(PASSWORD_PARAM_KEY);
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService svc =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_DEFAULT_KEY, locale);
        
        //FIXME: USUARIOS REPORTE: Parche para usuarios de reportes.
        if(username.toUpperCase().startsWith(REPORTES_SUBSTRING)) {
            tipo = TIPO_USUARIO_REPORTES;
        } 
		 
        Usuario usuario =
            svc.autenticar(username, password, tipo, locale);
        setAutenticado(usuario, request);
        setAutorizado(svc.obtenerFuncionalidades(usuario, locale), request);
        return usuario;
    }
    
    /**
     * TODO Describir m�todo verificarAutenticacion.
     * @param request
     * @param response
     * @param tipo
     * @param locale
     * @throws AutenticacionException 
     */
    public static Usuario login(HttpServletRequest request, int tipo, Locale locale) throws AutenticacionException {
    	String username = request.getParameter(USERNAME_PARAM_KEY);
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService svc =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_DEFAULT_KEY, locale);
        
        //FIXME: USUARIOS REPORTE: Parche para usuarios de reportes.
        if(username.toUpperCase().startsWith(REPORTES_SUBSTRING)) {
            tipo = TIPO_USUARIO_REPORTES;
        }
        
        Usuario usuario = svc.getUsuario(username, tipo); 
        setAutenticado(usuario, request);
        setAutorizado(svc.obtenerFuncionalidades(usuario, locale), request);
        return usuario;
    }

    /**
     * TODO Describir m�todo logout.
     * @param request
     */
    public static void logout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            logger.debug("Invalidando sesion HTTP.");
            session.invalidate();
        }
    }
}