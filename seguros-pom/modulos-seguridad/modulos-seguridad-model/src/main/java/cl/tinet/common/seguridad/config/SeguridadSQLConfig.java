package cl.tinet.common.seguridad.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * TODO Falta descripcion de clase SeguridadSQLConfig.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 10, 2010
 */
public class SeguridadSQLConfig extends AbstractConfigurator {

    /**
     * TODO Describir atributo SQL_CONFIG_BASE_NAME.
     */
    public static final String SQL_CONFIG_BASE_NAME =
        "cl.tinet.common.seguridad.resource.SeguridadSQLConfig";

    /**
     * TODO Describir atributo instance.
     */
    private static SeguridadSQLConfig instance = new SeguridadSQLConfig();

    /**
     * TODO Describir m�todo getInstance.
     * @return
     */
    public static SeguridadSQLConfig getInstance() {
        return instance;
    }

    /**
     * TODO Describir m�todo loadBundle.
     * @param locale
     * @return
     */
    @Override
    public ResourceBundle loadBundle(Locale locale) {
        return ResourceBundle.getBundle(SQL_CONFIG_BASE_NAME, locale);
    }
}
