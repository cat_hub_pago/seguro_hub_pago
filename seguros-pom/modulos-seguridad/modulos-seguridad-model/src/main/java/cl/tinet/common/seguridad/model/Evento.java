package cl.tinet.common.seguridad.model;

import java.io.Serializable;
import java.util.Date;

/**
 * TODO Falta descripcion de clase Evento.
 * <br/>
 * @author Roberto San Mart�n Lagos
 * @version 1.0
 * @created Nov 17, 2010
 */
public class Evento implements Serializable {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo idUsuario.
     */
    private int idUsuario;

    /**
     * TODO Describir atributo idFuncionalidad.
     */
    private int idFuncionalidad;

    /**
     * TODO Describir atributo idPais.
     */
    private int idPais;

    /**
     * TODO Describir atributo fecha.
     */
    private Date fecha;

    /**
     * TODO Describir atributo descripcion.
     */
    private String descripcion;

    /**
     * TODO Describir atributo tipoUsuario.
     */
    private int tipoUsuario;

    /**
     * TODO Describir constructor de Evento.
     * @param usuario
     * @param descripcion
     */
    public Evento(Usuario usuario, String descripcion) {
        this(usuario, null, descripcion);
    }

    /**
     * TODO Describir constructor de Evento.
     * @param usuario
     * @param funcionalidad
     * @param descripcion
     */
    public Evento(Usuario usuario, Funcionalidad funcionalidad,
        String descripcion) {
        this(usuario, funcionalidad, descripcion, null);
    }

    /**
     * TODO Describir constructor de Evento.
     * @param usuario
     * @param funcionalidad
     * @param descripcion
     * @param fecha
     */
    public Evento(Usuario usuario, Funcionalidad funcionalidad,
        String descripcion, Date fecha) {
        if (usuario == null) {
            throw new NullPointerException("El usuario no debe ser nulo.");
        }
        if (descripcion == null) {
            throw new NullPointerException("La descripcion no debe ser nula.");
        }
        this.idUsuario = usuario.getIdUsuario();
        this.idPais = usuario.getIdPais();
        this.tipoUsuario = usuario.getTipo();
        this.fecha = fecha;
        if (funcionalidad != null) {
            this.idFuncionalidad = funcionalidad.getId();
        }
        if (this.fecha == null) {
            this.fecha = new Date();
        }
        this.descripcion = descripcion;
    }

    /**
     * @return retorna el valor del atributo idUsuario
     */
    public int getIdUsuario() {
        return idUsuario;
    }

    /**
     * @return retorna el valor del atributo idFuncionalidad
     */
    public int getIdFuncionalidad() {
        return idFuncionalidad;
    }

    /**
     * @return retorna el valor del atributo idPais
     */
    public int getIdPais() {
        return idPais;
    }

    /**
     * @return retorna el valor del atributo fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @return retorna el valor del atributo descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @return retorna el valor del atributo tipoUsuario
     */
    public int getTipoUsuario() {
        return tipoUsuario;
    }

    /**
     * @param idUsuario a establecer en el atributo idUsuario.
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @param idFuncionalidad a establecer en el atributo idFuncionalidad.
     */
    public void setIdFuncionalidad(int idFuncionalidad) {
        this.idFuncionalidad = idFuncionalidad;
    }

    /**
     * @param idPais a establecer en el atributo idPais.
     */
    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    /**
     * @param fecha a establecer en el atributo fecha.
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @param descripcion a establecer en el atributo descripcion.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @param tipoUsuario a establecer en el atributo tipoUsuario.
     */
    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
}
