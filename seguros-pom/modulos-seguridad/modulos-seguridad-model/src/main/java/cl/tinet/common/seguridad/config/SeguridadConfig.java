package cl.tinet.common.seguridad.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * TODO Falta descripcion de clase SeguridadConfig.
 * <br/>
 * @author tinet
 * @version 1.0
 * @created Jan 27, 2010
 */
public class SeguridadConfig extends AbstractConfigurator {

    /**
     * TODO Describir atributo instance.
     */
    private static SeguridadConfig instance = new SeguridadConfig();
    
    /**
     * TODO Describir atributo SEGURIDAD_BUNDLE.
     */
    public static final String SEGURIDAD_BUNDLE =
        "cl.tinet.common.seguridad.resource.SeguridadConfig";
    
    /**
     * @return retorna el valor del atributo instance
     */
    public static SeguridadConfig getInstance() {
        return instance;
    }

    /**
     * TODO Describir constructor de SeguridadConfig.
     */
    private SeguridadConfig() {
    }
    
    /**
     * TODO Describir m�todo loadBundle.
     * @param locale
     * @return
     */
    public ResourceBundle loadBundle(Locale locale) {
        return ResourceBundle.getBundle(SEGURIDAD_BUNDLE, locale);
    }
}
