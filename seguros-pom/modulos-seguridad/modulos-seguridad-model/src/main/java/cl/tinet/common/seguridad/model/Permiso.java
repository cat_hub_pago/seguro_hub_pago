package cl.tinet.common.seguridad.model;

import java.io.Serializable;

/**
 * @author tinet
 * @version 1.0
 * @created 27-Ene-2010 16:38:19
 */
public class Permiso implements Serializable {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo id.
     */
    private int id;

    /**
     * TODO Describir atributo funcionalidad.
     */
    private Funcionalidad funcionalidad;

    /**
     * TODO Describir atributo tipo.
     */
    private TipoPermiso tipo;

    public Permiso() {
    }

    public int getId() {
        return id;
    }

    /**
     * 
     * @param newVal
     */
    public void setId(int newVal) {
        id = newVal;
    }

    public Funcionalidad getFuncionalidad() {
        return funcionalidad;
    }

    /**
     * 
     * @param newVal
     */
    public void setFuncionalidad(Funcionalidad newVal) {
        funcionalidad = newVal;
    }

    public TipoPermiso getTipo() {
        return tipo;
    }

    /**
     * 
     * @param newVal
     */
    public void setTipo(TipoPermiso newVal) {
        tipo = newVal;
    }
}