package cl.tinet.common.seguridad.exception;

/**
 * TODO Falta descripcion de clase ClaveIncorrectaException.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class ClaveIncorrectaException extends AutenticacionException {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo CLAVE_INCORRECTA_KEY.
     */
    public static final String CLAVE_INCORRECTA_KEY =
        "cl.tinet.common.seguridad.exception.CLAVE_INCORRECTA";

    /**
     * TODO Describir constructor de ClaveIncorrectaException.
     * @param username
     * @param tipo
     */
    public ClaveIncorrectaException(String username, int tipo) {
        super(CLAVE_INCORRECTA_KEY, username, tipo);
    }
    
    
    /**
     * TODO Describir constructor de ClaveIncorrectaException.
     * @param username
     * @param tipo
     */
    public ClaveIncorrectaException(String username, int tipo, int intentos) {
        super(CLAVE_INCORRECTA_KEY, username, tipo, intentos);
    }
}
