package cl.tinet.common.seguridad.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author tinet
 * @version 1.0
 * @created 27-Ene-2010 16:38:19
 */
public class Rol implements Serializable {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo id.
     */
    private String id;

    /**
     * TODO Describir atributo nombre.
     */
    private String nombre;

    /**
     * TODO Describir atributo permisos.
     */
    private List permisos;

    public Rol() {
    }

    public String getId() {
        return id;
    }

    /**
     * 
     * @param newVal
     */
    public void setId(String newVal) {
        id = newVal;
    }

    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param newVal
     */
    public void setNombre(String newVal) {
        nombre = newVal;
    }

    public List getPermisos() {
        return permisos;
    }

    /**
     * 
     * @param newVal
     */
    public void setPermisos(List newVal) {
        permisos = newVal;
    }
}