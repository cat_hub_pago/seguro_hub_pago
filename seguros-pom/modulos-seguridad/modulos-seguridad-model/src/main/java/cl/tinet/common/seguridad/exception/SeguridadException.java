package cl.tinet.common.seguridad.exception;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.config.SeguridadErrorConfig;

public abstract class SeguridadException extends BusinessException {

    public SeguridadException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    public SeguridadException(BusinessException[] exceptions) {
        super(exceptions);
    }

    @Override
    public AbstractConfigurator loadConfigurator() {
        return SeguridadErrorConfig.getInstance();
    }
}