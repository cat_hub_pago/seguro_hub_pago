package cl.tinet.common.seguridad.model;

import java.io.Serializable;

/**
 * @author tinet
 * @version 1.0
 * @created 27-Ene-2010 16:38:19
 */
public class Funcionalidad implements Serializable {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Identificador del permiso.
     */
    private int id;

    /**
     * TODO Describir atributo nombre.
     */
    private String nombre;

    /**
     * TODO Describir atributo url.
     */
    private String url;

    private int idTipo;

    /**
     * TODO Describir atributo tipo.
     */
    private TipoFuncionalidad tipo;

    public Funcionalidad() {
    }

    /**
     * Identificador del permiso.
     */
    public int getId() {
        return id;
    }

    /**
     * Identificador del permiso.
     * 
     * @param newVal
     */
    public void setId(int newVal) {
        id = newVal;
    }

    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param newVal
     */
    public void setNombre(String newVal) {
        nombre = newVal;
    }

    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param newVal
     */
    public void setUrl(String newVal) {
        url = newVal;
    }

    /**
     * @return retorna el valor del atributo tipo
     */
    public TipoFuncionalidad getTipo() {
        return tipo;
    }

    /**
     * @param tipo a establecer en el atributo tipo.
     */
    public void setTipo(TipoFuncionalidad tipo) {
        this.tipo = tipo;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    @Override
    public String toString() {
        return getId() + "/" + getNombre();
    }
}