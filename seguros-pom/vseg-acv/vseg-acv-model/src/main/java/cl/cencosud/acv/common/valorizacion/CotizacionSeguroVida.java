package cl.cencosud.acv.common.valorizacion;

import java.util.Date;

import cl.cencosud.acv.common.Beneficiario;

/**
 * Contiene los datos de una cotizacion de tipo Vida. <br/>
 * 
 * @author miguelgarcia
 * @version 1.0
 * @created 04/10/2010
 */
public class CotizacionSeguroVida extends CotizacionSeguro {

    /**
     * Identificador de la clase para serializaci
     */
    private static final long serialVersionUID = -1886561251100528663L;

    /**
     * Fecha de nacimiento del Contratante.
     */
    private Date fechaNacimiento;

    /**
     * Base de c�lculo de la cotizaci�n.
     */
    private float baseCalculo;
	/**
	 * Id de Materia.
	 */
    private long idMateriaVida;

	/**
	 * Beneficiarios.
	 */
	private Beneficiario[] beneficiarios;
	
	/**
	 * Adicionales.
	 */
	private Beneficiario[] adicionales;
    
    
    public long getIdMateriaVida() {
        return idMateriaVida;
    }

    public void setIdMateriaVida(long idMateriaVida) {
        this.idMateriaVida = idMateriaVida;
    }

    /**
     * @return the fechaNacimiento
     */
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * @param fechaNacimiento
     *            the fechaNacimiento to set
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * @return the baseCalculo
     */
    public float getBaseCalculo() {
        return baseCalculo;
    }

    /**
     * @param baseCalculo
     *            the baseCalculo to set
     */
    public void setBaseCalculo(float baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

	public Beneficiario[] getBeneficiarios() {
		return beneficiarios;
	}

	public void setBeneficiarios(Beneficiario[] beneficiarios) {
		this.beneficiarios = beneficiarios;
	}

	public Beneficiario[] getAdicionales() {
		return adicionales;
	}

	public void setAdicionales(Beneficiario[] adicionales) {
		this.adicionales = adicionales;
	}

}
