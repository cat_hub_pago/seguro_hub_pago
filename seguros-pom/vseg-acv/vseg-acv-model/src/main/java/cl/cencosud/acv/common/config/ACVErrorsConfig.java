package cl.cencosud.acv.common.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * TODO Falta descripcion de clase ACVConfig.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Aug 20, 2010
 */
public class ACVErrorsConfig extends AbstractConfigurator {

    private static final String CONFIG_BUNDLE_NAME =
        "cl.cencosud.acv.common.resource.ACVErrorsConfig";
    /**
     * TODO Describir atributo instance.
     */
    private static ACVErrorsConfig instance = new ACVErrorsConfig();

    /**
     * TODO Describir m�todo getInstance.
     * @return
     */
    public static ACVErrorsConfig getInstance() {
        return instance;
    }

    /**
     * TODO Describir constructor de VSPErrorsConfig.
     */
    private ACVErrorsConfig() {
    }

    /**
     * TODO Describir m�todo loadBundle.
     * @param locale
     * @return
     */
    public ResourceBundle loadBundle(Locale locale) {
        return ResourceBundle.getBundle(CONFIG_BUNDLE_NAME, locale);
    }

}
