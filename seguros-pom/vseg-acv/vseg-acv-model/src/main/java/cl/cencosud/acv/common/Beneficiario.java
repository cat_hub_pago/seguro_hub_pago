package cl.cencosud.acv.common;

import java.io.Serializable;
import java.util.Date;

public class Beneficiario implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5263427434623663562L;

    private String nombre;

    private String apellidoPaterno;

    private String apellidoMaterno;

    private Date fechaNacimiento;

    private String parentesco;
    
    private String descParentesco;

    private String sexo;

    private float distribucion;

    private long rut;

    private char dv;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getDescParentesco() {
		return descParentesco;
	}

	public void setDescParentesco(String descParentesco) {
		this.descParentesco = descParentesco;
	}

	public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public float getDistribucion() {
        return distribucion;
    }

    public void setDistribucion(float distribucion) {
        this.distribucion = distribucion;
    }

    public long getRut() {
        return rut;
    }

    public void setRut(long rut) {
        this.rut = rut;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

}
