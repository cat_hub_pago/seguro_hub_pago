package cl.cencosud.acv.common.valorizacion;

/**
 * Contiene datos de una cotización de tipo Hogar. <br/>
 * 
 * @author miguelgarcia
 * @version 1.0
 * @created 14/10/2010
 */
public class CotizacionSeguroHogar extends CotizacionSeguro {

    /**
     * Identificador de la Clase Para Serializacion.
     */
    private static final long serialVersionUID = 7130163829778753277L;

    /**
     * Monto total asegurado para un edificio.
     */
    private float montoAseguradoEdificio;

    /**
     * Monto total asegurado para el contenido del hogar.
     */
    private float montoAseguradoContenido;

    /**
     * Monto total asegurado para robo.
     * 
     */
    private float montoAseguradoRobo;

    private long idMateriaHogar;

    public long getIdMateriaHogar() {
        return idMateriaHogar;
    }

    public void setIdMateriaHogar(long idMateriaHogar) {
        this.idMateriaHogar = idMateriaHogar;
    }

    /**
     * @return the montoAseguradoEdificio
     */
    public float getMontoAseguradoEdificio() {
        return montoAseguradoEdificio;
    }

    /**
     * @param montoAseguradoEdificio
     *            the montoAseguradoEdificio to set
     */
    public void setMontoAseguradoEdificio(float montoAseguradoEdificio) {
        this.montoAseguradoEdificio = montoAseguradoEdificio;
    }

    /**
     * @return the montoAseguradoContenido
     */
    public float getMontoAseguradoContenido() {
        return montoAseguradoContenido;
    }

    /**
     * @param montoAseguradoContenido
     *            the montoAseguradoContenido to set
     */
    public void setMontoAseguradoContenido(float montoAseguradoContenido) {
        this.montoAseguradoContenido = montoAseguradoContenido;
    }

    /**
     * @return the montoAseguradoRobo
     */
    public float getMontoAseguradoRobo() {
        return montoAseguradoRobo;
    }

    /**
     * @param montoAseguradoRobo
     *            the montoAseguradoRobo to set
     */
    public void setMontoAseguradoRobo(float montoAseguradoRobo) {
        this.montoAseguradoRobo = montoAseguradoRobo;
    }

}
