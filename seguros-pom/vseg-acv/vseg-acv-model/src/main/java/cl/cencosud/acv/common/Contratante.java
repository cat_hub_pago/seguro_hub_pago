package cl.cencosud.acv.common;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que representa a un Contrante en una Cotizaci�n.
 * 
 * @author Miguel Garc�a (TInet)
 * @version 1.0
 * @created 26/08/2010
 * 
 */
public class Contratante implements Serializable {

	/**
	 * Identificador de la clase para serializaci�n.
	 */
	private static final long serialVersionUID = -7068553346602312440L;
	/**
	 * Id del contratante.
	 */
	private long idContratante;
	/**
	 * Rut del contratante.
	 */
	private long rut;
	/**
	 * Digito verificador del contratante.
	 */
	private String dv;
	/**
	 * Nombre del contratante.
	 */
	private String nombre;
	/**
	 * Apellido paterno del contratante.
	 */
	private String apellidoPaterno;
	/**
	 * Apellido materno del contratante.
	 */
	private String apellidoMaterno;
	/**
	 * Fecha de nacimiento del contratante.
	 */
	private Date fechaDeNacimiento;
	/**
	 * Sexo del contratante.
	 */
	private String sexo;
	/**
	 * Telefono 1 del contratante.
	 */
	private String telefono1;
	/**
	 * Telefono 2 del contratante.
	 */
	private String telefono2;
	/**
	 * Email del contratante.
	 */
	private String email;
	/**
	 * N�mero del departamento.
	 */
	private String numeroDepartamentoDireccion;
	/**
	 * N�mero de la direcci�n.
	 */
	private String numeroDireccion;
	/**
	 * Calle asociada a la direcci�n.
	 */
	private String calleDireccion;
	/**
	 * Identificador de la regi�n.
	 */
	private String idRegion;
	/**
	 * Identificador de la ciudad.
	 */
	private String idCiudad;
	/**
	 * Identificador de la comuna.
	 */
	private String idComuna;
	/**
	 * Edad del conductor.
	 */
	private int edadConductor;
	/**
	 * Estado civil del contratante.
	 */
	private String estadoCivil;
	/**
	 * Tipo de telefono asociado al telefono 1.
	 */
	private String tipoTelefono1;
	/**
	 * Tipo de telefono asociado al telefono 2.
	 */
	private String tipoTelefono2;
	
	/**
	 * @return the nombreCompleto
	 */
	public String getNombreCompleto() {
		return this.nombre + " " + this.apellidoPaterno + " "
				+ this.apellidoMaterno;
	}

	/**
	 * @return the idContratante
	 */
	public long getIdContratante() {
		return idContratante;
	}

	/**
	 * @param idContratante the idContratante to set
	 */
	public void setIdContratante(long idContratante) {
		this.idContratante = idContratante;
	}

	/**
	 * @return the rut
	 */
	public long getRut() {
		return rut;
	}

	/**
	 * @param rut the rut to set
	 */
	public void setRut(long rut) {
		this.rut = rut;
	}

	/**
	 * @return the dv
	 */
	public String getDv() {
		return dv;
	}
	
	/**
	 * @param dv the dv to set
	 */
	public void setDv(String dv) {
        this.dv = dv;
    }

	/**
	 * @param dv the dv to set
	 */
	public void setDv(char dv) {
		this.dv = String.valueOf(dv);
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apellidoPaterno
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	/**
	 * @param apellidoPaterno the apellidoPaterno to set
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	/**
	 * @return the fechaDeNacimiento
	 */
	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	/**
	 * @param fechaDeNacimiento the fechaDeNacimiento to set
	 */
	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	/**
	 * @return the telefono1
	 */
	public String getTelefono1() {
		return telefono1;
	}

	/**
	 * @param telefono1 the telefono1 to set
	 */
	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	/**
	 * @return the telefono2
	 */
	public String getTelefono2() {
		return telefono2;
	}

	/**
	 * @param telefono2 the telefono2 to set
	 */
	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the numeroDepartamentoDireccion
	 */
	public String getNumeroDepartamentoDireccion() {
		return numeroDepartamentoDireccion;
	}

	/**
	 * @param numeroDepartamentoDireccion the numeroDepartamentoDireccion to set
	 */
	public void setNumeroDepartamentoDireccion(String numeroDepartamentoDireccion) {
		this.numeroDepartamentoDireccion = numeroDepartamentoDireccion;
	}

	/**
	 * @return the numeroDireccion
	 */
	public String getNumeroDireccion() {
		return numeroDireccion;
	}

	/**
	 * @param numeroDireccion the numeroDireccion to set
	 */
	public void setNumeroDireccion(String numeroDireccion) {
		this.numeroDireccion = numeroDireccion;
	}

	/**
	 * @return the calleDireccion
	 */
	public String getCalleDireccion() {
		return calleDireccion;
	}

	/**
	 * @param calleDireccion the calleDireccion to set
	 */
	public void setCalleDireccion(String calleDireccion) {
		this.calleDireccion = calleDireccion;
	}

	/**
	 * @return the idRegion
	 */
	public String getIdRegion() {
		return idRegion;
	}

	/**
	 * @param idRegion the idRegion to set
	 */
	public void setIdRegion(String idRegion) {
		this.idRegion = idRegion;
	}

	/**
	 * @return the idCiudad
	 */
	public String getIdCiudad() {
		return idCiudad;
	}

	/**
	 * @param idCiudad the idCiudad to set
	 */
	public void setIdCiudad(String idCiudad) {
		this.idCiudad = idCiudad;
	}

	/**
	 * @return the idComuna
	 */
	public String getIdComuna() {
		return idComuna;
	}

	/**
	 * @param idComuna the idComuna to set
	 */
	public void setIdComuna(String idComuna) {
		this.idComuna = idComuna;
	}

	/**
	 * @return the edadConductor
	 */
	public int getEdadConductor() {
		return edadConductor;
	}

	/**
	 * @param edadConductor the edadConductor to set
	 */
	public void setEdadConductor(int edadConductor) {
		this.edadConductor = edadConductor;
	}

	/**
	 * @return the estadoCivil
	 */
	public String getEstadoCivil() {
		return estadoCivil;
	}

	/**
	 * @param estadoCivil the estadoCivil to set
	 */
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	/**
	 * @return the tipoTelefono1
	 */
	public String getTipoTelefono1() {
		return tipoTelefono1;
	}

	/**
	 * @param tipoTelefono1 the tipoTelefono1 to set
	 */
	public void setTipoTelefono1(String tipoTelefono1) {
		this.tipoTelefono1 = tipoTelefono1;
	}

	/**
	 * @return the tipoTelefono2
	 */
	public String getTipoTelefono2() {
		return tipoTelefono2;
	}

	/**
	 * @param tipoTelefono2 the tipoTelefono2 to set
	 */
	public void setTipoTelefono2(String tipoTelefono2) {
		this.tipoTelefono2 = tipoTelefono2;
	}

	
}
