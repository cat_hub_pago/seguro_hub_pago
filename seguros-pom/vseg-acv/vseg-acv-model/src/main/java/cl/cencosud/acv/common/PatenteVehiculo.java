package cl.cencosud.acv.common;

import java.io.Serializable;

public class PatenteVehiculo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String patente;
	
	private String tipoVehiculo;
	
	private String marca;
	
	private String modelo;
	
	private String anio;
	
	private String fechaRegistro;
	
	private String fechaModificacion;

	
	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Override
	public String toString() {
		return "PatenteVehiculo [patente=" + patente + ", tipoVehiculo="
				+ tipoVehiculo + ", marca=" + marca + ", modelo=" + modelo
				+ ", anio=" + anio + ", fechaRegistro=" + fechaRegistro
				+ ", fechaModificacion=" + fechaModificacion + "]";
	}
}
