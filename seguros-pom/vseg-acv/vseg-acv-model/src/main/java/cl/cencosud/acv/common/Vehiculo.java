package cl.cencosud.acv.common;

import java.io.Serializable;

public class Vehiculo implements Serializable {

	/**
	 * Identificador de la clase para serializaci�n.
	 */
	private static final long serialVersionUID = -6992515972673406254L;
	/**
	 * Identificador del tipo de veh�culo.
	 */
	private String idTipoVehiculo;
	/**
	 * Descripci�n del veh�culo.
	 */
	private String descripcionTipoVehiculo;
	/**
	 * Identificador de la marca del veh�culo.
	 */
	private String idMarcaVehiculo;
	/**
	 * Marca del veh�culo.
	 */
	private String marcaVehiculo;
	/**
	 * Identificador del modelo del veh�culo.
	 */
	private String idModeloVehiculo;
	/**
	 * Modelo del veh�culo.
	 */
	private String modeloVehiculo;
	/**
	 * Identificador del color del veh�culo.
	 */
	private String idColorVehiculo;
	/**
	 * Color del veh�culo.
	 */
	private String colorVehiculo;
	/**
	 * N�mero de puertas.
	 */
	private long numeroPuertas;
	/**
	 * Valor comercial del vehiculo.
	 */
	private double valorComercial;

	/**
	 * @return the idTipoVehiculo
	 */
	public String getIdTipoVehiculo() {
		return idTipoVehiculo;
	}

	/**
	 * @param idTipoVehiculo the idTipoVehiculo to set
	 */
	public void setIdTipoVehiculo(String idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}

	/**
	 * @return the descripcionTipoVehiculo
	 */
	public String getDescripcionTipoVehiculo() {
		return descripcionTipoVehiculo;
	}

	/**
	 * @param descripcionTipoVehiculo the descripcionTipoVehiculo to set
	 */
	public void setDescripcionTipoVehiculo(String descripcionTipoVehiculo) {
		this.descripcionTipoVehiculo = descripcionTipoVehiculo;
	}

	/**
	 * @return the idMarcaVehiculo
	 */
	public String getIdMarcaVehiculo() {
		return idMarcaVehiculo;
	}

	/**
	 * @param idMarcaVehiculo the idMarcaVehiculo to set
	 */
	public void setIdMarcaVehiculo(String idMarcaVehiculo) {
		this.idMarcaVehiculo = idMarcaVehiculo;
	}

	/**
	 * @return the marcaVehiculo
	 */
	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}

	/**
	 * @param marcaVehiculo the marcaVehiculo to set
	 */
	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}

	/**
	 * @return the idModeloVehiculo
	 */
	public String getIdModeloVehiculo() {
		return idModeloVehiculo;
	}

	/**
	 * @param idModeloVehiculo the idModeloVehiculo to set
	 */
	public void setIdModeloVehiculo(String idModeloVehiculo) {
		this.idModeloVehiculo = idModeloVehiculo;
	}

	/**
	 * @return the modeloVehiculo
	 */
	public String getModeloVehiculo() {
		return modeloVehiculo;
	}

	/**
	 * @param modeloVehiculo the modeloVehiculo to set
	 */
	public void setModeloVehiculo(String modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	/**
	 * @return the idColorVehiculo
	 */
	public String getIdColorVehiculo() {
		return idColorVehiculo;
	}

	/**
	 * @param idColorVehiculo the idColorVehiculo to set
	 */
	public void setIdColorVehiculo(String idColorVehiculo) {
		this.idColorVehiculo = idColorVehiculo;
	}

	/**
	 * @return the colorVehiculo
	 */
	public String getColorVehiculo() {
		return colorVehiculo;
	}

	/**
	 * @param colorVehiculo the colorVehiculo to set
	 */
	public void setColorVehiculo(String colorVehiculo) {
		this.colorVehiculo = colorVehiculo;
	}

	/**
	 * @return the numeroPuertas
	 */
	public long getNumeroPuertas() {
		return numeroPuertas;
	}

	/**
	 * @param numeroPuertas the numeroPuertas to set
	 */
	public void setNumeroPuertas(long numeroPuertas) {
		this.numeroPuertas = numeroPuertas;
	}

	/**
	 * @return the valorComercial
	 */
	public double getValorComercial() {
		return valorComercial;
	}

	/**
	 * @param valorComercial the valorComercial to set
	 */
	public void setValorComercial(double valorComercial) {
		this.valorComercial = valorComercial;
	}
}
