package cl.cencosud.acv.common.valorizacion;

import java.util.Date;

/**
 * Contiene los datos de una cotizaci�n de tipo Vehiculo. <br/>
 * 
 * @author miguelgarcia
 * @version 1.0
 * @created 14/10/2010
 */
public class CotizacionSeguroVehiculo extends CotizacionSeguro {

	/**
	 * Identificador de la Clase para Serializaci�n.
	 */
	private static final long serialVersionUID = 6848625302670560913L;

	/**
	 * Codigo de la Marca del Vehiculo.
	 */
	private int codigoMarca;

	/**
	 * Codigo Tipo de Vehiculo.
	 */
	private int codigoTipoVehiculo;

	/**
	 * Codigo Modelo de Vehiculo.
	 */
	private int codigoModelo;

	/**
	 * A�o del Vehiculo.
	 */
	private int anyoVehiculo;

	/**
	 * Monto asegurado del Vehiculo.
	 */
	private float montoAsegurado;

	/**
	 * Numero de Puertas.
	 */
	private int numeroPuertas;

	private Date fechaFactura;

	private String patente;

	private String numeroMotor;

	private String numeroChasis;

	private long idColor;

	private int esParticular;

	private String nombreContacto;

	private String tipoTelefono1Contacto;

	private String tipoTelefono2Contacto;

	private long numeroTelefono1Contacto;

	private long numeroTelefono2Contacto;

	private long idMateriaVehiculo;
	
	private String tipoInspeccion;
	
	private int edadConductor;
	
	private long idProducto;
	
	/**
	 * Variable que indica si se requiere o
	 * Inspeccion Vehicular
	 */
	private int inspeccionBSP;

	private byte[] factura;
	
	private String hiddNumeroFono;
	
	private String claveVitrineo;
	
	private String hiddPrecio;
    
    private String hiddCompannia;
    
    private String hiddProducto;
    
    private String hiddProducto2;
    
    private String hiddProducto3;
    
    private String hiddProducto4;
    
    private String hiddProducto5;
    
    private String hiddProducto6;
    
    private String hiddProducto7;
    
    private String hiddCaptcha;

    private String hiddHide2;
    
    private String hiddHide3;
    
    private String hiddHide4;
    
    private String hiddHide5;
    
    private String hiddHide6;
    
    private String hiddHide7;
    
    private String checkId00;
    
    private String checkId03;
    
    private String checkId04;
    
    private String checkId05;
    
    private String checkId08;
    
    private String checkId10;
	
	public int getEdadConductor() {
            return edadConductor;
        }

        public void setEdadConductor(int edadConductor) {
            this.edadConductor = edadConductor;
        }

        /**
	 * @return the codigoMarca
	 */
	public int getCodigoMarca() {
		return codigoMarca;
	}

	/**
	 * @param codigoMarca
	 *            the codigoMarca to set
	 */
	public void setCodigoMarca(int codigoMarca) {
		this.codigoMarca = codigoMarca;
	}

	/**
	 * @return the codigoTipoVehiculo
	 */
	public int getCodigoTipoVehiculo() {
		return codigoTipoVehiculo;
	}

	/**
	 * @param codigoTipoVehiculo
	 *            the codigoTipoVehiculo to set
	 */
	public void setCodigoTipoVehiculo(int codigoTipoVehiculo) {
		this.codigoTipoVehiculo = codigoTipoVehiculo;
	}

	/**
	 * @return the codigoModelo
	 */
	public int getCodigoModelo() {
		return codigoModelo;
	}

	/**
	 * @param codigoModelo
	 *            the codigoModelo to set
	 */
	public void setCodigoModelo(int codigoModelo) {
		this.codigoModelo = codigoModelo;
	}

	/**
	 * @return the anyoVehiculo
	 */
	public int getAnyoVehiculo() {
		return anyoVehiculo;
	}

	/**
	 * @param anyoVehiculo
	 *            the anyoVehiculo to set
	 */
	public void setAnyoVehiculo(int anyoVehiculo) {
		this.anyoVehiculo = anyoVehiculo;
	}

	/**
	 * @return the montoAsegurado
	 */
	public float getMontoAsegurado() {
		return montoAsegurado;
	}

	/**
	 * @param montoAsegurado
	 *            the montoAsegurado to set
	 */
	public void setMontoAsegurado(float montoAsegurado) {
		this.montoAsegurado = montoAsegurado;
	}

	/**
	 * @return the numeroPuertas
	 */
	public int getNumeroPuertas() {
		return numeroPuertas;
	}

	/**
	 * @param numeroPuertas
	 *            the numeroPuertas to set
	 */
	public void setNumeroPuertas(int numeroPuertas) {
		this.numeroPuertas = numeroPuertas;
	}

	/**
	 * @return the fechaFactura
	 */
	public Date getFechaFactura() {
		return fechaFactura;
	}

	/**
	 * @param fechaFactura
	 *            the fechaFactura to set
	 */
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	/**
	 * @return the patente
	 */
	public String getPatente() {
		return patente;
	}

	/**
	 * @param patente
	 *            the patente to set
	 */
	public void setPatente(String patente) {
		this.patente = patente;
	}

	/**
	 * @return the numeroMotor
	 */
	public String getNumeroMotor() {
		return numeroMotor;
	}

	/**
	 * @param numeroMotor
	 *            the numeroMotor to set
	 */
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	/**
	 * @return the numeroChasis
	 */
	public String getNumeroChasis() {
		return numeroChasis;
	}

	/**
	 * @param numeroChasis
	 *            the numeroChasis to set
	 */
	public void setNumeroChasis(String numeroChasis) {
		this.numeroChasis = numeroChasis;
	}

	/**
	 * @return the idColor
	 */
	public long getIdColor() {
		return idColor;
	}

	/**
	 * @param idColor
	 *            the idColor to set
	 */
	public void setIdColor(long idColor) {
		this.idColor = idColor;
	}

	/**
	 * @return the esParticular
	 */
	public int getEsParticular() {
		return esParticular;
	}

	/**
	 * @param esParticular
	 *            the esParticular to set
	 */
	public void setEsParticular(int esParticular) {
		this.esParticular = esParticular;
	}

	/**
	 * @return the nombreContacto
	 */
	public String getNombreContacto() {
		return nombreContacto;
	}

	/**
	 * @param nombreContacto
	 *            the nombreContacto to set
	 */
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	/**
	 * @return the tipoTelefono1Contacto
	 */
	public String getTipoTelefono1Contacto() {
		return tipoTelefono1Contacto;
	}

	/**
	 * @param tipoTelefono1Contacto
	 *            the tipoTelefono1Contacto to set
	 */
	public void setTipoTelefono1Contacto(String tipoTelefono1Contacto) {
		this.tipoTelefono1Contacto = tipoTelefono1Contacto;
	}

	/**
	 * @return the tipoTelefono2Contacto
	 */
	public String getTipoTelefono2Contacto() {
		return tipoTelefono2Contacto;
	}

	/**
	 * @param tipoTelefono2Contacto
	 *            the tipoTelefono2Contacto to set
	 */
	public void setTipoTelefono2Contacto(String tipoTelefono2Contacto) {
		this.tipoTelefono2Contacto = tipoTelefono2Contacto;
	}

	/**
	 * @return the numeroTelefono1Contacto
	 */
	public long getNumeroTelefono1Contacto() {
		return numeroTelefono1Contacto;
	}

	/**
	 * @param numeroTelefono1Contacto
	 *            the numeroTelefono1Contacto to set
	 */
	public void setNumeroTelefono1Contacto(long numeroTelefono1Contacto) {
		this.numeroTelefono1Contacto = numeroTelefono1Contacto;
	}

	/**
	 * @return the numeroTelefono2Contacto
	 */
	public long getNumeroTelefono2Contacto() {
		return numeroTelefono2Contacto;
	}

	/**
	 * @param numeroTelefono2Contacto
	 *            the numeroTelefono2Contacto to set
	 */
	public void setNumeroTelefono2Contacto(long numeroTelefono2Contacto) {
		this.numeroTelefono2Contacto = numeroTelefono2Contacto;
	}

	/**
	 * @return the idMateriaVehiculo
	 */
	public long getIdMateriaVehiculo() {
		return idMateriaVehiculo;
	}

	/**
	 * @param idMateriaVehiculo
	 *            the idMateriaVehiculo to set
	 */
	public void setIdMateriaVehiculo(long idMateriaVehiculo) {
		this.idMateriaVehiculo = idMateriaVehiculo;
	}

	/**
	 * @return the factura
	 */
	public byte[] getFactura() {
		return factura;
	}

	/**
	 * @param factura
	 *            the factura to set
	 */
	public void setFactura(byte[] factura) {
		this.factura = factura;
	}

	/**
	 * @return the tipoInspeccion
	 */
	public String getTipoInspeccion() {
		return tipoInspeccion;
	}
	
	/**
	 * @param tipoInspeccion
	 *            the tipoInspeccion to set
	 */
	public void setTipoInspeccion(String tipoInspeccion) {
		this.tipoInspeccion = tipoInspeccion;
	}

	/**
	 * @return the inspeccionBSP
	 */
	public int getInspeccionBSP() {
		return inspeccionBSP;
	}

	/**
	 * @param inspeccionBSP
	 *            the inspeccionBSP to set
	 */
	public void setInspeccionBSP(int inspeccionBSP) {
		this.inspeccionBSP = inspeccionBSP;
	}

	public void setHiddNumeroFono(String hiddNumeroFono) {
		this.hiddNumeroFono = hiddNumeroFono;
	}

	public String getHiddNumeroFono() {
		return hiddNumeroFono;
	}

	public void setClaveVitrineo(String claveVitrineo) {
		this.claveVitrineo = claveVitrineo;
	}

	public String getClaveVitrineo() {
		return claveVitrineo;
	}

	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	public long getIdProducto() {
		return idProducto;
	}

	public void setHiddPrecio(String hiddPrecio) {
		this.hiddPrecio = hiddPrecio;
	}

	public String getHiddPrecio() {
		return hiddPrecio;
	}

	public void setHiddCompannia(String hiddCompannia) {
		this.hiddCompannia = hiddCompannia;
	}

	public String getHiddCompannia() {
		return hiddCompannia;
	}

	public void setHiddProducto(String hiddProducto) {
		this.hiddProducto = hiddProducto;
	}

	public String getHiddProducto() {
		return hiddProducto;
	}

	public void setHiddProducto2(String hiddProducto2) {
		this.hiddProducto2 = hiddProducto2;
	}

	public String getHiddProducto2() {
		return hiddProducto2;
	}

	public void setHiddProducto3(String hiddProducto3) {
		this.hiddProducto3 = hiddProducto3;
	}

	public String getHiddProducto3() {
		return hiddProducto3;
	}

	public void setHiddProducto4(String hiddProducto4) {
		this.hiddProducto4 = hiddProducto4;
	}

	public String getHiddProducto4() {
		return hiddProducto4;
	}

	public void setHiddProducto5(String hiddProducto5) {
		this.hiddProducto5 = hiddProducto5;
	}

	public String getHiddProducto5() {
		return hiddProducto5;
	}

	public void setHiddProducto6(String hiddProducto6) {
		this.hiddProducto6 = hiddProducto6;
	}

	public String getHiddProducto6() {
		return hiddProducto6;
	}

	public void setHiddProducto7(String hiddProducto7) {
		this.hiddProducto7 = hiddProducto7;
	}

	public String getHiddProducto7() {
		return hiddProducto7;
	}

	public void setHiddCaptcha(String hiddCaptcha) {
		this.hiddCaptcha = hiddCaptcha;
	}

	public String getHiddCaptcha() {
		return hiddCaptcha;
	}

	public void setHiddHide2(String hiddHide2) {
		this.hiddHide2 = hiddHide2;
	}

	public String getHiddHide2() {
		return hiddHide2;
	}

	public void setHiddHide4(String hiddHide4) {
		this.hiddHide4 = hiddHide4;
	}

	public String getHiddHide4() {
		return hiddHide4;
	}

	public void setHiddHide5(String hiddHide5) {
		this.hiddHide5 = hiddHide5;
	}

	public String getHiddHide5() {
		return hiddHide5;
	}

	public void setHiddHide3(String hiddHide3) {
		this.hiddHide3 = hiddHide3;
	}

	public String getHiddHide3() {
		return hiddHide3;
	}

	public void setHiddHide6(String hiddHide6) {
		this.hiddHide6 = hiddHide6;
	}

	public String getHiddHide6() {
		return hiddHide6;
	}

	public void setHiddHide7(String hiddHide7) {
		this.hiddHide7 = hiddHide7;
	}

	public String getHiddHide7() {
		return hiddHide7;
	}

	public void setCheckId00(String checkId00) {
		this.checkId00 = checkId00;
	}

	public String getCheckId00() {
		return checkId00;
	}

	public void setCheckId03(String checkId03) {
		this.checkId03 = checkId03;
	}

	public String getCheckId03() {
		return checkId03;
	}

	public void setCheckId04(String checkId04) {
		this.checkId04 = checkId04;
	}

	public String getCheckId04() {
		return checkId04;
	}

	public void setCheckId05(String checkId05) {
		this.checkId05 = checkId05;
	}

	public String getCheckId05() {
		return checkId05;
	}

	public void setCheckId08(String checkId08) {
		this.checkId08 = checkId08;
	}

	public String getCheckId08() {
		return checkId08;
	}

	public void setCheckId10(String checkId10) {
		this.checkId10 = checkId10;
	}

	public String getCheckId10() {
		return checkId10;
	}


	
}
