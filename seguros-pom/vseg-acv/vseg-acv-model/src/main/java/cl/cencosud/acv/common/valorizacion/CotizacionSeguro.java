package cl.cencosud.acv.common.valorizacion;

import java.io.Serializable;
import java.util.Date;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivilEnum;

/**
 * Contiene los datos de una cotización. <br/>
 * 
 * @author miguelgarcia
 * @version 1.0
 * @created 14/10/2010
 */
public class CotizacionSeguro implements Serializable {

    private static final long serialVersionUID = 4766002450723389621L;
    
    private long codigoPlan;
    private long rutCliente;
    private char dv;
    private long codigoProducto;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String numeroTelefono1;
    private String email;
    private Date fechaNacimiento;
    private String estadoCivil;
    private String actividad;
    private String sexo;
    private String region;
    private String ciudad;
    private String comuna;
    private String direccion;
    private boolean clienteEsAsegurado;
    private String antiguedadVivienda;
    private String numeroTelefono2;
    private String tipoTelefono1;
    private String tipoTelefono2;
    private String numeroDireccion;
    private String numeroDepto;
    private Contratante asegurado;   
    private Contratante contratante;
    private int numDiaMaxIniVig;
    private String fecIniVig;
    private String fecTerVig;
    private String tramoDias;
    private String tipoMascota;
	private String razaMascota;
    private String sexoMascota;
    private String edadMascota;
    private String numSubcategoria;
    private String integrante;
	private String contratanteEsAsegurado; 
        
    public String getNumSubcategoria() {
		return numSubcategoria;
	}

	public void setNumSubcategoria(String numSubcategoria) {
		this.numSubcategoria = numSubcategoria;
	}

	public String getTipoMascota() {
		return tipoMascota;
	}

	public void setTipoMascota(String tipoMascota) {
		this.tipoMascota = tipoMascota;
	}

	public String getRazaMascota() {
		return razaMascota;
	}

	public void setRazaMascota(String razaMascota) {
		this.razaMascota = razaMascota;
	}

	public String getSexoMascota() {
		return sexoMascota;
	}

	public void setSexoMascota(String sexoMascota) {
		this.sexoMascota = sexoMascota;
	}

	public String getEdadMascota() {
		return edadMascota;
	}

	public void setEdadMascota(String edadMascota) {
		this.edadMascota = edadMascota;
	}
    
	public String getTramoDias() {
		return tramoDias;
	}

	public void setTramoDias(String tramoDias) {
		this.tramoDias = tramoDias;
	}

	public Contratante getContratante() {
        return contratante;
    }

    public void setContratante(Contratante contratante) {
        this.contratante = contratante;
    }

    public long getCodigoPlan() {
        return codigoPlan;
    }

    public void setCodigoPlan(long codigoPlan) {
        this.codigoPlan = codigoPlan;
    }

    public long getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(long rutCliente) {
        this.rutCliente = rutCliente;
    }

    public char getDv() {
        return dv;
    }

    public void setDv(char dv) {
        this.dv = dv;
    }

    public long getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(long codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNumeroTelefono1() {
        return numeroTelefono1;
    }

    public void setNumeroTelefono1(String numeroTelefono1) {
        this.numeroTelefono1 = numeroTelefono1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isClienteEsAsegurado() {
        return clienteEsAsegurado;
    }

    public void setClienteEsAsegurado(boolean clienteEsAsegurado) {
        this.clienteEsAsegurado = clienteEsAsegurado;
    }

    public String getAntiguedadVivienda() {
        return antiguedadVivienda;
    }

    public void setAntiguedadVivienda(String antiguedadVivienda) {
        this.antiguedadVivienda = antiguedadVivienda;
    }

    public String getNumeroTelefono2() {
        return numeroTelefono2;
    }

    public void setNumeroTelefono2(String numeroTelefono2) {
        this.numeroTelefono2 = numeroTelefono2;
    }

    public String getTipoTelefono1() {
        return tipoTelefono1;
    }

    public void setTipoTelefono1(String tipoTelefono1) {
        this.tipoTelefono1 = tipoTelefono1;
    }

    public String getTipoTelefono2() {
        return tipoTelefono2;
    }

    public void setTipoTelefono2(String tipoTelefono2) {
        this.tipoTelefono2 = tipoTelefono2;
    }

    public String getNumeroDireccion() {
        return numeroDireccion;
    }

    public void setNumeroDireccion(String numeroDireccion) {
        this.numeroDireccion = numeroDireccion;
    }

    public String getNumeroDepto() {
        return numeroDepto;
    }

    public void setNumeroDepto(String numeroDepto) {
        this.numeroDepto = numeroDepto;
    }

    public Contratante getAsegurado() {
        return asegurado;
    }

    public void setAsegurado(Contratante asegurado) {
        this.asegurado = asegurado;
    }
    
    public String getEstadoCivilDesc() {
        return EstadoCivilEnum.valueOf("_" + this.estadoCivil).getDescripcion();
    }

    public int getNumDiaMaxIniVig() {
		return numDiaMaxIniVig;
	}

	public void setNumDiaMaxIniVig(int numDiaMaxIniVig) {
		this.numDiaMaxIniVig = numDiaMaxIniVig;
	}

	public String getFecIniVig() {
		return fecIniVig;
	}

	public void setFecIniVig(String fecIniVig) {
		this.fecIniVig = fecIniVig;
	}

	public String getFecTerVig() {
		return fecTerVig;
	}

	public void setFecTerVig(String fecTerVig) {
		this.fecTerVig = fecTerVig;
	}

	public void setAsegurado(int i) {
	}

	public String getIntegrante() {
		return integrante;
	}

	public void setIntegrante(String integrante) {
		this.integrante = integrante;
	}

	public String getContratanteEsAsegurado() {
		return contratanteEsAsegurado;
	}

	public void setContratanteEsAsegurado(String contratanteEsAsegurado) {
		this.contratanteEsAsegurado = contratanteEsAsegurado;
	}


}
