package cl.cencosud.acv.common;

import java.io.Serializable;

public class ColorVehiculo implements Serializable {

    /**
     * serialVersionUID 
     */
    private static final long serialVersionUID = -6842892257343895363L;

    private long idColor;

    private String idColorLeg;

    private String descripcion;

    public long getIdColor() {
        return idColor;
    }

    public void setIdColor(long idColor) {
        this.idColor = idColor;
    }

    public String getIdColorLeg() {
        return idColorLeg;
    }

    public void setIdColorLeg(String idColorLeg) {
        this.idColorLeg = idColorLeg;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
