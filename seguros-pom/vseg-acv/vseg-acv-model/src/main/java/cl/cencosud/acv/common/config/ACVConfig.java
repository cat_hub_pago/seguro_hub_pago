package cl.cencosud.acv.common.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * Clase ACVConfig, permite obetner parametros desde el archivo propertie 
 * ACVConfig<br/>
 * 
 * @author chespi81
 * @version 1.0
 * @created Aug 20, 2010
 */
public class ACVConfig extends AbstractConfigurator {

	/**
	 * Atributo CONFIG_BUNDLE_NAME.
	 */
	public static final String CONFIG_BUNDLE_NAME = 
		"cl.cencosud.acv.common.resource.ACVConfig";

	private static ACVConfig instance = new ACVConfig();

	public static ACVConfig getInstance() {
		return instance;
	}

	public ACVConfig() {

	}

	/**
	 * M�todo loadBundle.
	 * 
	 * @param locale
	 * @return
	 */
	@Override
	public ResourceBundle loadBundle(Locale locale) {
		return ResourceBundle.getBundle(CONFIG_BUNDLE_NAME, locale);
	}

}
