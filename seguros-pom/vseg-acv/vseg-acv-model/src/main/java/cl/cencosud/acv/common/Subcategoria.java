package cl.cencosud.acv.common;

public class Subcategoria extends ParametroCotizacion {

	/**
	 * Identificador de la clase para serialización.
	 */
	private static final long serialVersionUID = -1352471612329224918L;

	/**
	 * Titulo de la subcategoria.
	 */
	private String tituloSubcategoria;
	
	/**
         * id del Tipo de Subcategoria.
         */
	private int idTipo; 

	/**
	 * @return the tituloSubcategoria
	 */
	public String getTituloSubcategoria() {
		return tituloSubcategoria;
	}

	/**
	 * @param tituloSubcategoria
	 *            the tituloSubcategoria to set
	 */
	public void setTituloSubcategoria(String tituloSubcategoria) {
		this.tituloSubcategoria = tituloSubcategoria;
	}

	/**
         * @return the idTipo
         */
        public int getIdTipo() {
            return idTipo;
        }
    
        /**
         * @param idTipo
         *            the idTipo to set
         */
        public void setIdTipo(int idTipo) {
            this.idTipo = idTipo;
        }
	
}
