package cl.cencosud.acv.common;

import java.io.Serializable;

public class CoberturaSeg implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6194976635276586783L;
	private java.lang.Integer codigoPlan;
    private Integer codigoCobertura;
    private String descripcion;
    private String explicacion;
    private String iva;
    private String descripDeducible;
    
     public CoberturaSeg() {
	}
	public java.lang.Integer getCodigoPlan() {
		return codigoPlan;
	}
	public void setCodigoPlan(java.lang.Integer codigoPlan) {
		this.codigoPlan = codigoPlan;
	}
	public Integer getCodigoCobertura() {
		return codigoCobertura;
	}
	public void setCodigoCobertura(Integer codigoCobertura) {
		this.codigoCobertura = codigoCobertura;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getExplicacion() {
		return explicacion;
	}
	public void setExplicacion(String explicacion) {
		this.explicacion = explicacion;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public String getDescripDeducible() {
		return descripDeducible;
	}
	public void setDescripDeducible(String descripDeducible) {
		this.descripDeducible = descripDeducible;
	}

    
    
}
