package cl.cencosud.asesorcotizador.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.Stub;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.cencosud.acv.common.Beneficiario;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivilEnum;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.RegistrarCotizacionDAO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.CotizacionHogarVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.CotizacionVehiculoVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.CotizacionVidaVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.DatosAseguradoVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.DatosBenficiarioVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.ErrorInternoException;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.GrabarCotizacionSeguroHogarGraba;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.GrabarCotizacionSeguroVehiculoGraba;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.GrabarCotizacionSeguroVidaGraba;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.GrabarResponseVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.RespuestaScoringVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.SeguroHogarVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.SeguroVehiculoVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.SeguroVidaVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.TipoEstadoCivil;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.TipoPersonaVO;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.WsBigsaCargaCotizacionImplDelegate;
import cl.cencosud.bigsa.cotizacion.carga.webservice.client.WsBigsaCargaCotizacion_Impl;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.logger.Logging;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class RegistrarCotizacionDAOWS extends BaseConfigurable implements
    RegistrarCotizacionDAO {

    private static Log logger =
        LogFactory.getLog(RegistrarCotizacionDAOWS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String WSDL_REGISTRAR_COTIZACION =
        "cl.cencosud.asesorcotizador.dao.ws.RegistrarCotizacionDAOWS.wsdl.registrar.cotizacion";

    private static final String USER_WS =
        "cl.cencosud.asesorcotizador.cotizador.registro.bigsa.usuario";

    private static final String SCORING_VEHICULO_ESTADO_CIVIL =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.estadoCivil";

    private static final String SCORING_VEHICULO_MEDIO_INFORMATIVO =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.medioInformativo";

    private static final String SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO_CODE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.medioInformativoCode";

    private static final String SCORING_VEHICULO_NUMERO_PUERTAS =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.nroPuertas";

    private static final String SCORING_VEHICULO_RESPUESTA_NUMERO_PUERTAS_DE_2_A_3 =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.nroPuertas.de2a3";

    private static final String SCORING_VEHICULO_EDAD_CONTRATANTE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadContratante";

    private static final String SCORING_VEHICULO_SEXO_CONTRATANTE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.sexoContratante";

    private static final String SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO_CODE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.masculinoCode";

    private static final String SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO_CODE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.femeninoCode";

    private static final String SCORING_VEHICULO_CODIGO_COMUNA =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.codigoComuna";

    private static final String SCORING_VEHICULO_EDAD_CONDUCTOR =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadConductor";

    private static final String CODIGO_SCORING_ANTIGUEDAD =
        "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.antiguedad";

    private static final String CODIGO_SCORING_VERANEO =
        "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.veraneo";

    private static final String VALOR_SCORING_VERANEO =
        "cl.cencosud.asesorcotizador.scoring.hogar.valor.veraneo";

    /**
     * TODO Describir m�todo registrarCotizacionHogar.
     * @param cotHogar
     * @param solicitud
     * @return
     */
    public int registrarCotizacionHogar(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
        
        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_REGISTRAR_COTIZACION));

        WsBigsaCargaCotizacion_Impl svc = new WsBigsaCargaCotizacion_Impl();
        WsBigsaCargaCotizacionImplDelegate ws =
            this.inicializarWS(svc.getWsBigsaCargaCotizacionImplPort(), params);

        int nroBigsa = 0;
        GrabarResponseVO result = null;

        try {
            CotizacionHogarVO cotizacion = new CotizacionHogarVO();
            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));
            cotizacion.setCodigoPlan(new Long(cotHogar.getCodigoPlan())
                .intValue());

            Contratante datos = cotHogar.getContratante();
            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue());
            contratante.setDigitoPersona(datos.getDv());
            contratante.setApePatPersona(datos.getApellidoPaterno());
            contratante.setApeMatPersona(datos.getApellidoMaterno());
            contratante.setNombresPersona(datos.getNombre());
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                .format(datos.getFechaDeNacimiento()));

            String estadoCivil =
                EstadoCivilEnum.valueOf("_" + cotHogar.getEstadoCivil())
                    .getDescripcion();
            contratante.setEstCivilPersona(TipoEstadoCivil
                .fromString(estadoCivil));

            contratante.setSexoPersona(datos.getSexo());
            contratante.setDirCallePersona(datos.getCalleDireccion());
            contratante.setDirNroPersona(datos.getNumeroDireccion());
            contratante.setDirDeptoPersona(datos
                .getNumeroDepartamentoDireccion());
            contratante.setCodigoRegionPersona(Integer.parseInt(datos
                .getIdRegion()));
            contratante.setCodigoComunaPersona(Integer.parseInt(datos
                .getIdComuna()));
            contratante.setCodigoCiudadPersona(Integer.parseInt(datos
                .getIdCiudad()));
            contratante.setFono1Persona(datos.getTelefono1());
            contratante.setEmailPersona(datos.getEmail());
            cotizacion.setContratante(contratante);

            if (cotHogar.isClienteEsAsegurado()) {
                cotizacion.setAsegurado(contratante);
            } else {

                Contratante asegurado = cotHogar.getAsegurado();
                TipoPersonaVO aseguradoLeg = new TipoPersonaVO();

                aseguradoLeg.setRutPersona(new Long(asegurado.getRut())
                    .intValue());
                aseguradoLeg.setDigitoPersona(asegurado.getDv());
                aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
                aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
                aseguradoLeg.setNombresPersona(asegurado.getNombre());
                aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                    .format(asegurado.getFechaDeNacimiento()));

                String estadoCivilAseg =
                    EstadoCivilEnum.valueOf("_" + cotHogar.getEstadoCivil())
                        .getDescripcion();
                aseguradoLeg.setEstCivilPersona(TipoEstadoCivil
                    .fromString(estadoCivilAseg));

                aseguradoLeg.setSexoPersona(asegurado.getSexo());
                aseguradoLeg.setDirCallePersona(asegurado.getCalleDireccion());
                aseguradoLeg.setDirNroPersona(asegurado.getNumeroDireccion());
                aseguradoLeg.setDirDeptoPersona(asegurado
                    .getNumeroDepartamentoDireccion());
                aseguradoLeg.setCodigoRegionPersona(Integer.parseInt(asegurado
                    .getIdRegion()));
                aseguradoLeg.setCodigoComunaPersona(Integer.parseInt(asegurado
                    .getIdComuna()));
                aseguradoLeg.setCodigoCiudadPersona(Integer.parseInt(asegurado
                    .getIdCiudad()));
                aseguradoLeg.setFono1Persona(asegurado.getTelefono1());

                cotizacion.setAsegurado(aseguradoLeg);

            }

            cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
                .format(solicitud.getFecha_creacion()));

            Calendar cal = Calendar.getInstance();
            cal.setTime(solicitud.getFecha_creacion());
            cal.add(Calendar.MONTH, 1);
            cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal
                .getTime()));

            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());

            SeguroHogarVO hogar = new SeguroHogarVO();
            String direccionRiesgo = cotHogar.getDireccion();
            if (cotHogar.getNumeroDireccion() != null
                && !cotHogar.getNumeroDireccion().equals("")) {
                direccionRiesgo += " " + cotHogar.getNumeroDireccion();
            }
            if (cotHogar.getNumeroDepto() != null
                && !cotHogar.getNumeroDepto().equals("")) {
                direccionRiesgo += " " + cotHogar.getNumeroDepto();
            }
            hogar.setDireccionRiesgo(direccionRiesgo);
            hogar.setCodigoRegionRiesgo(Integer.valueOf(cotHogar.getRegion()));
            hogar.setCodigoComunaRiesgo(Integer.valueOf(cotHogar.getComuna()));
            hogar.setCodigoCiudadRiesgo(Integer.valueOf(cotHogar.getCiudad()));

            hogar.setMontoAseguradoContenido(new Float(cotHogar
                .getMontoAseguradoContenido()));
            hogar.setMontoAseguradoEdificio(new Float(cotHogar
                .getMontoAseguradoEdificio()));
            hogar.setMontoAseguradoRobo(new Float(cotHogar
                .getMontoAseguradoRobo()));

            cotizacion.setHogar(hogar);

            //Respuestas de scoring
            //SCORING ANTIGUEDAD
            RespuestaScoringVO rptaAntiguedad = new RespuestaScoringVO();
            int codigoAntiguedad =
                Integer.valueOf(ACVConfig.getInstance().getString(
                    CODIGO_SCORING_ANTIGUEDAD));
            rptaAntiguedad.setCodigoPregunta(codigoAntiguedad);
            rptaAntiguedad.setValorNumeral(Float.valueOf(cotHogar
                .getAntiguedadVivienda()));
            rptaAntiguedad.setValorSN("N");

            //SCORING VERANEO
            RespuestaScoringVO rptaVeraneo = new RespuestaScoringVO();
            int codigoVeraneo =
                Integer.valueOf(ACVConfig.getInstance().getString(
                    CODIGO_SCORING_VERANEO));
            rptaVeraneo.setCodigoPregunta(codigoVeraneo);
            rptaVeraneo.setValorSN(ACVConfig.getInstance().getString(
                VALOR_SCORING_VERANEO));

            RespuestaScoringVO[] scoring =
                new RespuestaScoringVO[] { rptaAntiguedad, rptaVeraneo };
            cotizacion.setScoring(scoring);

            logger.info("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
            logger.info("GRABAR COTIZACION");
            Logging.Logger("COTIZACION", cotizacion, getClass());
            Logging.Logger("CONTRATANTE", cotizacion.getContratante(),
                getClass());
            Logging.Logger("ASEGURADO", cotizacion.getAsegurado(), getClass());
            Logging.Logger("HOGAR", cotizacion.getHogar(), getClass());
            logger.info("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");

            result =
                ws.grabarCotizacionSeguroHogar(
                    GrabarCotizacionSeguroHogarGraba.S, cotizacion);

            nroBigsa = result.getNumeroCotizacion();

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            try {
                this.obtenerExcepcionWS(e);
                e.printStackTrace();
                throw new SystemException(e);
            } catch (ValorizacionException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                throw new SystemException(e1);
            }
        } catch (ErrorInternoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return nroBigsa;

    }

    public int registrarCotizacionVehiculo(CotizacionSeguroVehiculo vehiculo,
        Solicitud solicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_REGISTRAR_COTIZACION));

        int resultNroBigsa = 0;

        try {

            WsBigsaCargaCotizacion_Impl svc = new WsBigsaCargaCotizacion_Impl();
            WsBigsaCargaCotizacionImplDelegate ws =
                this.inicializarWS(svc.getWsBigsaCargaCotizacionImplPort(),
                    params);

            GrabarResponseVO result = null;

            CotizacionVehiculoVO cotizacion = new CotizacionVehiculoVO();

            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));

            String codigoPlan = String.valueOf(vehiculo.getCodigoPlan());
            cotizacion.setCodigoPlan(new Integer(codigoPlan));

            Contratante datos = vehiculo.getContratante();
            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue());
            contratante.setDigitoPersona(datos.getDv());
            contratante.setApePatPersona(datos.getApellidoPaterno());
            contratante.setApeMatPersona(datos.getApellidoMaterno());
            contratante.setNombresPersona(datos.getNombre());
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                .format(datos.getFechaDeNacimiento()));

            String estadoCivil =
                EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil())
                    .getDescripcion();
            contratante.setEstCivilPersona(TipoEstadoCivil
                .fromString(estadoCivil));

            contratante.setSexoPersona(datos.getSexo());
            contratante.setDirCallePersona(datos.getCalleDireccion());
            contratante.setDirNroPersona(datos.getNumeroDireccion());
            contratante.setDirDeptoPersona(datos
                .getNumeroDepartamentoDireccion());
            contratante.setCodigoRegionPersona(Integer.parseInt(datos
                .getIdRegion()));
            contratante.setCodigoComunaPersona(Integer.parseInt(datos
                .getIdComuna()));
            contratante.setCodigoCiudadPersona(Integer.parseInt(datos
                .getIdCiudad()));
            contratante.setFono1Persona(datos.getTelefono1());

            cotizacion.setContratante(contratante);

            Contratante asegurado = vehiculo.getAsegurado();
            TipoPersonaVO aseguradoLeg = new TipoPersonaVO();

            aseguradoLeg.setRutPersona(new Long(asegurado.getRut()).intValue());
            aseguradoLeg.setDigitoPersona(asegurado.getDv());
            aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
            aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
            aseguradoLeg.setNombresPersona(asegurado.getNombre());
            aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                .format(asegurado.getFechaDeNacimiento()));

            estadoCivil =
                EstadoCivilEnum.valueOf("_" + asegurado.getEstadoCivil())
                    .getDescripcion();
            aseguradoLeg.setEstCivilPersona(TipoEstadoCivil
                .fromString(estadoCivil));

            aseguradoLeg.setSexoPersona(asegurado.getSexo());
            aseguradoLeg.setDirCallePersona(asegurado.getCalleDireccion());
            aseguradoLeg.setDirNroPersona(asegurado.getNumeroDireccion());
            aseguradoLeg.setDirDeptoPersona(asegurado
                .getNumeroDepartamentoDireccion());
            aseguradoLeg.setCodigoRegionPersona(Integer.parseInt(asegurado
                .getIdRegion()));
            aseguradoLeg.setCodigoComunaPersona(Integer.parseInt(asegurado
                .getIdComuna()));
            aseguradoLeg.setCodigoCiudadPersona(Integer.parseInt(asegurado
                .getIdCiudad()));
            aseguradoLeg.setFono1Persona(asegurado.getTelefono1());
            aseguradoLeg.setEmailPersona(asegurado.getEmail());
            cotizacion.setAsegurado(aseguradoLeg);

            //Fecha vigencia.
            if(vehiculo.getNumDiaMaxIniVig() != 0){
                cotizacion.setFecIniVig(vehiculo.getFecIniVig());
                cotizacion.setFecTerVig(vehiculo.getFecTerVig());
                	
            }else{
            	cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
                .format(solicitud.getFecha_creacion()));

                Calendar cal = Calendar.getInstance();
                cal.setTime(solicitud.getFecha_creacion());
                cal.add(Calendar.MONTH, 1);
                cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal
                    .getTime()));        	
            }

            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());

            SeguroVehiculoVO vehiculoLeg = new SeguroVehiculoVO();
            vehiculoLeg.setCodigoMarca(vehiculo.getCodigoMarca());
            vehiculoLeg.setCodigoModelo(vehiculo.getCodigoModelo());
            vehiculoLeg.setCodigoTipoVehiculo(vehiculo.getCodigoTipoVehiculo());
            vehiculoLeg.setAnoVehiculo(vehiculo.getAnyoVehiculo());
            vehiculoLeg.setPatente(vehiculo.getPatente());
            vehiculoLeg.setNumeroMotor(vehiculo.getNumeroMotor());
            vehiculoLeg.setNumeroChassis(vehiculo.getNumeroChasis());
            String sIdColor = String.valueOf(vehiculo.getIdColor());
            vehiculoLeg.setCodigoColor(Integer.valueOf(sIdColor));
            vehiculoLeg.setMontoAsegurado(vehiculo.getMontoAsegurado());
            
            String usoVehiculo = vehiculo.getEsParticular() == 1 ? "P" : "";
            vehiculoLeg.setUsoVehiculo(usoVehiculo);

            cotizacion.setVehiculo(vehiculoLeg);

            String estadoCivilAsegurado = "";
            int edadAsegurado = -1;
            int edadAseguradoDuenyo = -1;
            String sexoAsegurado = "";
            String idComunaAsegurado = "";

            if (vehiculo.isClienteEsAsegurado()) {
                estadoCivilAsegurado = datos.getEstadoCivil();
//                edadAsegurado =
//                    calcularEdadContratante(datos.getFechaDeNacimiento());
                sexoAsegurado = datos.getSexo();
                idComunaAsegurado = datos.getIdComuna();
            } else {

                estadoCivilAsegurado = asegurado.getEstadoCivil();
                edadAseguradoDuenyo =
                    calcularEdadContratante(asegurado.getFechaDeNacimiento());
                sexoAsegurado = asegurado.getSexo();
                idComunaAsegurado = asegurado.getIdComuna();
            }
            
            edadAsegurado = (int) vehiculo.getEdadConductor();
            Date fechaNacimiento = vehiculo.getFechaNacimiento();

            //PREGUNTAS DE SCORING
            //42: -> (Valor alternativa) C:CASADO, S:SOLTERO, V:VIUDO, O:OTRO.
            RespuestaScoringVO respuestaEstadoCivil = new RespuestaScoringVO();
            int codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_ESTADO_CIVIL);
            respuestaEstadoCivil.setCodigoPregunta(codigoPregunta);
            respuestaEstadoCivil
                .setValorAlternativa(obtenerEstadoCivilCode(estadoCivilAsegurado));

            //106: -> (Valor alternativa) Fijo O
            RespuestaScoringVO respuestaMedioInformativo =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(
                    SCORING_VEHICULO_MEDIO_INFORMATIVO);
            respuestaMedioInformativo.setCodigoPregunta(codigoPregunta);
            String valorMedioInformativo =
                ACVConfig.getInstance().getString(
                    SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO_CODE);
            respuestaMedioInformativo
                .setValorAlternativa(valorMedioInformativo); //FIJO

            //100: -> (Valor alternativa) 2, 3, 4, 5
            RespuestaScoringVO respuestaNumeroPuertas =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_NUMERO_PUERTAS);
            respuestaNumeroPuertas.setCodigoPregunta(codigoPregunta);
            String nroPuertas =
                ACVConfig.getInstance().getString(
                    SCORING_VEHICULO_RESPUESTA_NUMERO_PUERTAS_DE_2_A_3);
            if (vehiculo.getNumeroPuertas() > 3) {
                nroPuertas =
                    String.valueOf(vehiculo.getNumeroPuertas());
            }
            respuestaNumeroPuertas.setValorAlternativa(nroPuertas);

            //101: -> (Valor numeral) Edad
            RespuestaScoringVO respuestaEdadAsegurado =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(
                    SCORING_VEHICULO_EDAD_CONTRATANTE);
            respuestaEdadAsegurado.setCodigoPregunta(codigoPregunta);
            respuestaEdadAsegurado
                .setValorAlternativa(obtenerRangoEdadAsegurado(edadAsegurado));

            //59: -> (Valor alternativa) M: Masculino, F: Femenino
            RespuestaScoringVO respuestaSexoAsegurado =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(
                    SCORING_VEHICULO_SEXO_CONTRATANTE);
            respuestaSexoAsegurado.setCodigoPregunta(codigoPregunta);
            String descSexoAsegurado =
                ACVConfig.getInstance().getString(
                    SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO_CODE);
            if (sexoAsegurado.equals("F")) {
                descSexoAsegurado =
                    ACVConfig.getInstance().getString(
                        SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO_CODE);
            }
            respuestaSexoAsegurado.setValorAlternativa(descSexoAsegurado);

            //79: -> (Codigo Comuna) Codigo de comuna
            RespuestaScoringVO respuestaCodigoComuna = new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_CODIGO_COMUNA);
            respuestaCodigoComuna.setCodigoPregunta(codigoPregunta);
            int codigoComuna = Integer.valueOf(idComunaAsegurado);
            respuestaCodigoComuna.setCodigoComuna(codigoComuna);

            RespuestaScoringVO respuestaEdad = null;
            //54: -> (Valor numeral) Edad conductores del vehiculo 
            if (vehiculo.isClienteEsAsegurado() == false) {
            	respuestaEdad =
                    new RespuestaScoringVO();
                codigoPregunta =
                    ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
                respuestaEdad.setCodigoPregunta(codigoPregunta);
                respuestaEdad.setValorNumeral(Float.valueOf(edadAseguradoDuenyo));
                }
            
            else {
            respuestaEdad =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
            respuestaEdad.setCodigoPregunta(codigoPregunta);
            respuestaEdad.setValorNumeral(Float.valueOf(calcularEdadContratante(fechaNacimiento)));
            }
            
            RespuestaScoringVO[] scoring =
                new RespuestaScoringVO[] { respuestaEstadoCivil,
                    respuestaMedioInformativo, respuestaNumeroPuertas,
                    respuestaEdadAsegurado, respuestaSexoAsegurado,
                    respuestaCodigoComuna, respuestaEdad };

            cotizacion.setScoring(scoring);

            // LOG datos Entrada
            logger.info("REGISTRAR COTIZACION VEHICULO");
            logger.info("Datos de Entrada");
            logger.info("----------------");
            logger.info("Codigo Plan: " + codigoPlan);
            logger.info("Marca Vehiculo: " + vehiculoLeg.getCodigoMarca());
            logger
                .info("Tipo Vehiculo: " + vehiculoLeg.getCodigoTipoVehiculo());
            logger.info("Modelo Vehiculo: " + vehiculoLeg.getCodigoModelo());
            logger.info("A�o Vehiculo: " + vehiculoLeg.getAnoVehiculo());
            logger.info("Monto Asegurado: " + vehiculoLeg.getMontoAsegurado());
            logger.info("Rut: " + cotizacion.getAsegurado().getRutPersona());
            logger.info("****************");
            logger.info("SCORING:");
            logger.info("codigo pregunta: "
                + respuestaEstadoCivil.getCodigoPregunta());
            logger.info("valor: " + respuestaEstadoCivil.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaMedioInformativo.getCodigoPregunta());
            logger.info("valor: "
                + respuestaMedioInformativo.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaNumeroPuertas.getCodigoPregunta());
            logger.info("valor: "
                + respuestaNumeroPuertas.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaEdadAsegurado.getCodigoPregunta());
            logger.info("valor: "
                + respuestaEdadAsegurado.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaSexoAsegurado.getCodigoPregunta());
            logger.info("valor: "
                + respuestaSexoAsegurado.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaCodigoComuna.getCodigoPregunta());
            logger.info("valor: " + respuestaCodigoComuna.getCodigoComuna());
            logger.info("codigo pregunta: "
                + respuestaEdad.getCodigoPregunta());
            logger.info("valor: " + respuestaEdad.getValorNumeral());
            logger.info("****************");

            logger.info("----------------");

            result =
                ws.grabarCotizacionSeguroVehiculo(
                    GrabarCotizacionSeguroVehiculoGraba.S, cotizacion);

            resultNroBigsa = result.getNumeroCotizacion();

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            try {
                logger.error("Error al guardar cotizacion.", e);
                this.obtenerExcepcionWS(e);
                throw new SystemException(e);
            } catch (ValorizacionException e1) {
                throw new SystemException(e1);
            }
        } catch (ErrorInternoException e) {
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return resultNroBigsa;
    }

    /**
     * TODO Describir m�todo obtenerRangoEdadAsegurado.
     * @param edadAsegurado
     * @return
     */
    private String obtenerRangoEdadAsegurado(int edadAsegurado) {
        String rangoEdadAsegurado = "";
        if (edadAsegurado >= 18 && edadAsegurado < 23) {
            rangoEdadAsegurado = "Mayores a 18";
        } else if (edadAsegurado >= 23 && edadAsegurado < 28) {
            rangoEdadAsegurado = "Mayores a 23";
        } else if (edadAsegurado >= 28) {
            rangoEdadAsegurado = "Mayores a 28";
        }
        return rangoEdadAsegurado;
    }

    /**
     * TODO Describir m�todo calcularEdad.
     * @param cotizacionVehiculo
     * @return
     */
    private int calcularEdadContratante(Date fechaNacimiento) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(fechaNacimiento);
        GregorianCalendar now = new GregorianCalendar();
        int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
        if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
            || (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal
                .get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
            res--;
        }
        return res;
    }

    /**
     * TODO Describir m�todo obtenerEstadoCivil.
     * @param cotizacionVehiculo
     * @return
     */
    private String obtenerEstadoCivilCode(String sEstadoCivil) {
        String estadoCivil = "O";
        if (sEstadoCivil.equals("1")) {
            estadoCivil = "S";
        } else if (sEstadoCivil.equals("2")) {
            estadoCivil = "C";
        } else if (sEstadoCivil.equals("3")) {
            estadoCivil = "V";
        }
        return estadoCivil;
    }

    public int registrarCotizacionVida(CotizacionSeguroVida cotVida,
        Solicitud solicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_REGISTRAR_COTIZACION));

        WsBigsaCargaCotizacion_Impl svc = new WsBigsaCargaCotizacion_Impl();
        WsBigsaCargaCotizacionImplDelegate ws =
            this.inicializarWS(svc.getWsBigsaCargaCotizacionImplPort(), params);

        int nroBigsa = 0;
        GrabarResponseVO result = null;

        try {
            CotizacionVidaVO cotizacion = new CotizacionVidaVO();
            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));
            cotizacion.setCodigoPlan(new Long(cotVida.getCodigoPlan())
                .intValue());

            Contratante datos = cotVida.getContratante();
            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue());
            contratante.setDigitoPersona(datos.getDv());
            contratante.setApePatPersona(datos.getApellidoPaterno());
            contratante.setApeMatPersona(datos.getApellidoMaterno());
            contratante.setNombresPersona(datos.getNombre());
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                .format(datos.getFechaDeNacimiento()));

            String estadoCivil =
                EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil())
                    .getDescripcion();
            contratante.setEstCivilPersona(TipoEstadoCivil
                .fromString(estadoCivil));

            contratante.setSexoPersona(datos.getSexo());
            contratante.setDirCallePersona(datos.getCalleDireccion());
            contratante.setDirNroPersona(datos.getNumeroDireccion());
            contratante.setDirDeptoPersona(datos
                .getNumeroDepartamentoDireccion());
            contratante.setCodigoRegionPersona(Integer.parseInt(datos
                .getIdRegion()));
            contratante.setCodigoComunaPersona(Integer.parseInt(datos
                .getIdComuna()));
            contratante.setCodigoCiudadPersona(Integer.parseInt(datos
                .getIdCiudad()));
            contratante.setFono1Persona(datos.getTelefono1());
            contratante.setFono2Persona(datos.getTelefono2());

            //contratante.setCodigoProfesion(new Integer(cotVida.getActividad()));

            contratante.setEmailPersona(datos.getEmail());

            cotizacion.setContratante(contratante);

            cotizacion.setAsegurado(contratante);

            cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
                .format(solicitud.getFecha_creacion()));

            Calendar cal = Calendar.getInstance();
            cal.setTime(solicitud.getFecha_creacion());
            cal.add(Calendar.MONTH, 1);
            cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal
                .getTime()));

            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());

            SeguroVidaVO vida = new SeguroVidaVO();

            DatosAseguradoVO oAseguradoVida = new DatosAseguradoVO();

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

            Beneficiario[] beneficiarios = cotVida.getBeneficiarios();

            DatosBenficiarioVO[] beneficiariosAux = null;
            if (beneficiarios != null) {

                beneficiariosAux = new DatosBenficiarioVO[beneficiarios.length];
                int i = 0;
                for (Beneficiario beneficiario : beneficiarios) {

                    beneficiariosAux[i] = new DatosBenficiarioVO();

                    beneficiariosAux[i].setApeMaternoBene(beneficiario
                        .getApellidoMaterno());
                    beneficiariosAux[i].setApePaternoBene(beneficiario
                        .getApellidoPaterno());
                    beneficiariosAux[i].setDigitoBene(String
                        .valueOf(beneficiario.getDv()));
                    beneficiariosAux[i].setFecNacBene(sdf.format(beneficiario
                        .getFechaNacimiento()));
                    beneficiariosAux[i]
                        .setNombresBene(beneficiario.getNombre());

                    String descParentesco = "";
                    String parentesco = beneficiario.getParentesco();

                    if (parentesco.equalsIgnoreCase("H")) {
                        descParentesco =
                            GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg._value5String;
                    } else if (parentesco.equalsIgnoreCase("P")) {
                        descParentesco =
                            GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg._value3String;
                    } else if (parentesco.equalsIgnoreCase("M")) {
                        descParentesco =
                            GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg._value4String;
                    } else if (parentesco.equalsIgnoreCase("C")) {
                        descParentesco =
                            GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg._value2String;
                    } else if (parentesco.equalsIgnoreCase("O")) {
                        descParentesco =
                            GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg._value6String;
                    } else if (parentesco.equalsIgnoreCase("E")) {
                        descParentesco =
                            GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg._value1String;
                    }

                    beneficiariosAux[i].setParentBene(descParentesco);
                    beneficiariosAux[i].setPorcBene(beneficiario
                        .getDistribucion());
                    beneficiariosAux[i].setRutBene(new Long(beneficiario
                        .getRut()).intValue());
                    beneficiariosAux[i].setSexoBene(beneficiario.getSexo());
                    beneficiariosAux[i].setDigitoBene(null);
                    beneficiariosAux[i].setRutBene(null);

                    i++;
                }

                oAseguradoVida.setBeneficiarioVida(beneficiariosAux);
            }

            oAseguradoVida.setApeMaternoAseg(contratante.getApeMatPersona());
            oAseguradoVida.setApePaternoAseg(contratante.getApePatPersona());
            oAseguradoVida.setDigitoAseg(contratante.getDigitoPersona());
            oAseguradoVida.setFecNacAseg(contratante.getFecNacPersona());
            oAseguradoVida.setNombresAseg(contratante.getNombresPersona());
            oAseguradoVida
                .setParentAseg(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg.value1);
            oAseguradoVida.setRutAseg(contratante.getRutPersona());
            oAseguradoVida.setSexoAseg(contratante.getSexoPersona());

            Beneficiario[] adicionales = cotVida.getAdicionales();

            int asegurados = 0;

            if (adicionales != null) {
                asegurados = adicionales.length;
            }

            if (oAseguradoVida.getBeneficiarioVida() == null) {
                DatosBenficiarioVO[] beneficiarioVida =
                    new DatosBenficiarioVO[1];
                DatosBenficiarioVO beneBenficiarioElMismo =
                    new DatosBenficiarioVO();
                beneBenficiarioElMismo.setApeMaternoBene(contratante
                    .getApeMatPersona());
                beneBenficiarioElMismo.setApePaternoBene(contratante
                    .getApePatPersona());
                beneBenficiarioElMismo.setDigitoBene(contratante
                    .getDigitoPersona());
                beneBenficiarioElMismo.setFecNacBene(contratante
                    .getFecNacPersona());
                beneBenficiarioElMismo.setNombresBene(contratante
                    .getNombresPersona());
                beneBenficiarioElMismo
                    .setParentBene(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg._value1);
                beneBenficiarioElMismo.setRutBene(contratante.getRutPersona());
                beneBenficiarioElMismo
                    .setSexoBene(contratante.getSexoPersona());
                beneBenficiarioElMismo.setPorcBene(100f);

                beneficiarioVida[0] = beneBenficiarioElMismo;
                oAseguradoVida.setBeneficiarioVida(beneficiarioVida);
            }

            DatosAseguradoVO[] aseguradoVida =
                new DatosAseguradoVO[asegurados + 1];

            aseguradoVida[0] = oAseguradoVida;

            if (adicionales != null) {
                int i = 1;

                for (Beneficiario adicional : adicionales) {

                    aseguradoVida[i] = new DatosAseguradoVO();

                    aseguradoVida[i].setApeMaternoAseg(adicional
                        .getApellidoMaterno());
                    aseguradoVida[i].setApePaternoAseg(adicional
                        .getApellidoPaterno());
                    String dv = "";
                    if (adicional.getDv() != '\u0000') {
                        dv = String.valueOf(adicional.getDv());
                    }
                    aseguradoVida[i].setDigitoAseg(dv);
                    aseguradoVida[i].setFecNacAseg(sdf.format(adicional
                        .getFechaNacimiento()));
                    aseguradoVida[i].setNombresAseg(adicional.getNombre());

                    String parentesco = adicional.getParentesco();

                    if (parentesco != null) {
                        if (parentesco.equalsIgnoreCase("H")) {
                            aseguradoVida[i]
                                .setParentAseg(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg.value5);
                        } else if (parentesco.equalsIgnoreCase("P")) {
                            aseguradoVida[i]
                                .setParentAseg(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg.value3);
                        } else if (parentesco.equalsIgnoreCase("M")) {
                            aseguradoVida[i]
                                .setParentAseg(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg.value4);
                        } else if (parentesco.equalsIgnoreCase("C")) {
                            aseguradoVida[i]
                                .setParentAseg(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg.value2);
                        } else if (parentesco.equalsIgnoreCase("O")) {
                            aseguradoVida[i]
                                .setParentAseg(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg.value6);
                        } else if (parentesco.equalsIgnoreCase("E")) {
                            aseguradoVida[i]
                                .setParentAseg(GrabarCotizacionSeguroVidaCotizacionVidaAseguradoVidaParentAseg.value1);
                        }

                    }
                    aseguradoVida[i].setRutAseg(new Long(adicional.getRut())
                        .intValue());
                    aseguradoVida[i].setSexoAseg(adicional.getSexo());
                    aseguradoVida[i].setBeneficiarioVida(beneficiariosAux);

                    i++;
                }
            }

            cl.cencosud.bigsa.cotizacion.carga.webservice.client.RespuestaScoringVO respuesta =
                new cl.cencosud.bigsa.cotizacion.carga.webservice.client.RespuestaScoringVO();
            int codigoPregunta =
                Integer
                    .valueOf(ACVConfig
                        .getInstance()
                        .getString(
                            "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil"));
            respuesta.setCodigoPregunta(codigoPregunta);

            String estadoCivilScoring = "";
            estadoCivilScoring =
                EstadoCivilEnum.valueOf("_" + cotVida.getEstadoCivil())
                    .getDescripcion();

            respuesta.setValorAlternativa(estadoCivilScoring);

            cl.cencosud.bigsa.cotizacion.carga.webservice.client.RespuestaScoringVO[] oRespuestas =
                new cl.cencosud.bigsa.cotizacion.carga.webservice.client.RespuestaScoringVO[] { respuesta };

            cotizacion.setScoring(oRespuestas);

            vida.setValorBaseCalculo(cotVida.getBaseCalculo());
            vida.setAseguradoVida(aseguradoVida);
            cotizacion.setVida(vida);

            Logging.Logger("COTIZACION", cotizacion, getClass());

            Logging.Logger("VIDA", cotizacion.getVida(), getClass());

            for (int j = 0; j < cotizacion.getVida().getAseguradoVida().length; j++) {
                Logging.Logger("ASEGURADOVIDA", cotizacion.getVida()
                    .getAseguradoVida()[j], getClass());

                for (int k = 0; k < cotizacion.getVida().getAseguradoVida()[j]
                    .getBeneficiarioVida().length; k++) {
                    Logging.Logger("BENEFICIARIOVIDA", cotizacion.getVida()
                        .getAseguradoVida()[j].getBeneficiarioVida()[k],
                        getClass());
                }
            }

            Logging.Logger("ASEGURADO", cotizacion.getAsegurado(), getClass());

            Logging.Logger("CONTRATANTE", cotizacion.getContratante(),
                getClass());

            for (int j = 0; j < cotizacion.getScoring().length; j++) {
                Logging.Logger("SCORING", cotizacion.getScoring()[j],
                    getClass());
            }

            result =
                ws.grabarCotizacionSeguroVida(
                    GrabarCotizacionSeguroVidaGraba.S, cotizacion);

            nroBigsa = result.getNumeroCotizacion();

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            try {
                this.obtenerExcepcionWS(e);
            } catch (ValorizacionException e1) {
                // TODO Auto-generated catch block 
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch (ErrorInternoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            ResourceLeakUtil.close(dao);
        }

        return nroBigsa;
    }

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub

    }

    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws ExtraccionDatosException 
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    /**
     * TODO Describir m�todo obtenerExcepcionWS.
     * @param e
     * @throws ValorizacionException
     */
    private void obtenerExcepcionWS(RemoteException e)
        throws ValorizacionException {
        if (e.detail instanceof SOAPFaultException) {
            SOAPFaultException sfe = (SOAPFaultException) e.detail;
            Detail detail = sfe.getDetail();
            if (detail != null) {
                ValorizacionException vex =
                    obtenerErrorValorizacion(detail.getChildNodes());
                if (vex != null) {
                    logger.debug("Error de negocio durante invocacion a WS.",
                        vex);
                    throw vex;
                }
            }
        }
    }

    /**
     * TODO Describir m�todo obtenerErrorValorizacion.
     * @param nodes
     * @return
     */
    private ValorizacionException obtenerErrorValorizacion(NodeList nodes) {
        if (nodes == null) {
            return null;
        }
        ValorizacionException vex = null;
        String message = null;
        String codigo = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node item = nodes.item(i);
            vex = obtenerErrorValorizacion(item.getChildNodes());
            if (logger.isDebugEnabled()) {
                logger.debug(i + ") item : " + item);
                logger.debug(i + ") name : " + item.getNodeName());
                logger.debug(i + ") value: " + item.getNodeValue());
                logger.debug(i + ") eie  : " + vex);
            }
            if (vex == null) {
                if ("mensaje".equals(item.getNodeName())) {
                    message = item.getFirstChild().getNodeValue();
                } else if ("codigo".equals(item.getNodeName())) {
                    codigo = item.getFirstChild().getNodeValue();
                }
            } else {
                return vex;
            }
        }
        if ((message != null) && (codigo != null)) {
            logger.debug("Descripcion encontrada. Se crea excepcion.");
            vex = new ValorizacionException(message, Integer.valueOf(codigo));
        }
        return vex;
    }

}
