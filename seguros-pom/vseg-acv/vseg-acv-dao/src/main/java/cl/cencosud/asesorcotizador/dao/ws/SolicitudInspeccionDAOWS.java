package cl.cencosud.asesorcotizador.dao.ws;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.SolicitudInspeccionDAO;
import cl.cencosud.bsp.inspeccion.webservice.client.WS_Inspeccion;
import cl.cencosud.bsp.inspeccion.webservice.client.WS_InspeccionSoap;
import cl.cencosud.bsp.inspeccion.webservice.client.WS_Inspeccion_Impl;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class SolicitudInspeccionDAOWS extends BaseConfigurable implements
    SolicitudInspeccionDAO {

    private static Log logger =
        LogFactory.getLog(SolicitudInspeccionDAOWS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String WSDL_INSPECCION_BSP =
        "cl.cencosud.asesorcotizador.dao.ws.SolicitudInspeccionDAOWS.wsdl.solicitud.inspeccion";

    public String solicitarInspeccion(SolicitudInspeccion solicitudInspeccion) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
        
        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_INSPECCION_BSP));

        logger.debug("WS: Inspeccion BSP");
        logger.debug("Antes de intanciar IMPL.");

        WS_Inspeccion srv = new WS_Inspeccion_Impl();

        logger.debug("Antes de Ininicalizar el servicio.");
        WS_InspeccionSoap ws;
        try {

            ws = this.inicializarWS(srv.getWS_InspeccionSoap(), params);

            System.out.println("EN DAO JBOSS");

            String inspeccionXml = "";

            inspeccionXml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            inspeccionXml += "<INSPECCION>";
            inspeccionXml += "  <COMPA�IA>" + solicitudInspeccion.getCod_compannia_inspeccion() + "</COMPA�IA>";
            inspeccionXml += "  <NOMBRE_ASEGURADO>" + solicitudInspeccion.getNombre_asegurado() + "</NOMBRE_ASEGURADO>";
            inspeccionXml += "  <RUT_ASEGURADO>" + solicitudInspeccion.getRut_asegurado() + "</RUT_ASEGURADO>";
            inspeccionXml += "  <NUMERO_DE_PATENTE>" + solicitudInspeccion.getNumero_patente() + "</NUMERO_DE_PATENTE>";
            // inspeccionXml += "       <MARCA>MARCA01</MARCA>";
            // inspeccionXml += "       <MODELO>MODELO01</MODELO>";
            inspeccionXml += "  <NOMBRE_CORREDOR>" + solicitudInspeccion.getNombre_corredor() + "</NOMBRE_CORREDOR>";
            inspeccionXml += "  <REFERENCIA_INTERNA>" + solicitudInspeccion.getReferencia_interna() + "</REFERENCIA_INTERNA>";
            inspeccionXml += "  <NOMBRE_CONTACTO>" + solicitudInspeccion.getNombre_contacto() + "</NOMBRE_CONTACTO>";
            inspeccionXml += "  <DIRECCION>" + solicitudInspeccion.getDireccion() + "</DIRECCION>";
            inspeccionXml += "  <COMUNA>" + solicitudInspeccion.getComuna() + "</COMUNA>";
            inspeccionXml += "  <TELEFONO_CONTACTO>" + solicitudInspeccion.getTelefono_contacto() + "</TELEFONO_CONTACTO>";
            if(solicitudInspeccion.getCelular() != null) {
                inspeccionXml += "      <CELULAR>"+solicitudInspeccion.getCelular()+"</CELULAR>";
            }
            inspeccionXml += "  <USUARIO>" + solicitudInspeccion.getUsuario_interno_bsp() + "</USUARIO>";
            inspeccionXml += "  <TIPO_INSPECCION>" + solicitudInspeccion.getTipo_inspeccion() + "</TIPO_INSPECCION>";
            inspeccionXml += "  <OBSERVACIONES>" + solicitudInspeccion.getObservaciones() + "</OBSERVACIONES>";
            inspeccionXml += "  <RAMO TIPO=\"" + solicitudInspeccion.getRamo() + "\"/>";
            inspeccionXml += "</INSPECCION>";


            String result = ws.insertarInspeccion(inspeccionXml);

            if (result != null) {
                String idSolicitud = null;
                DocumentBuilderFactory dbf =
                    DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document document =
                    db.parse(new ByteArrayInputStream(result.getBytes()));
                NodeList elements =
                    document.getElementsByTagName("NUMERO_SOLICITUD");
                if (elements.getLength() > 0) {
                    Node node = elements.item(0);
                    idSolicitud = node.getTextContent();

                    if (idSolicitud.equals("0")) {
                        NodeList elementos =
                            document.getElementsByTagName("ERROR");
                        if (elementos.getLength() > 0) {
                            Node nodeError = elementos.item(0);
                            logger.error("ERROR: Respuesta WS BSP: "
                                + nodeError.getTextContent());
                        }
                    }

                    return idSolicitud;
                }
            }

            //Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(result.getBytes()));

        } catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return null;
    }

    /**
    * TODO Describir m�todo inicializarServicio.
    * @param stub
    * @param param
    * @throws ExtraccionDatosException 
    */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub

    }
}
