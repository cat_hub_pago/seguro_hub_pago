package cl.cencosud.asesorcotizador.dao;

import java.util.Locale;

import cl.cencosud.acv.common.config.ACVConfig;
import cl.tinet.common.dao.AbstractDAOFactory;

import com.tinet.exceptions.system.SystemException;

/**
 * @author tinet
 * @version 1.0
 * @created 26-Jul-2010 15:32:02
 */
public class ACVDAOFactory extends AbstractDAOFactory {

	/**
	 * Atributo DEFAULT_KEY.
	 */
	public static final String DEFAULT_KEY = 
		"cl.cencosud.asesorcotizador.DAOFACTORY";

	/**
	 * M�todo getInstance.
	 * 
	 * @param locale
	 * @return
	 */
	public static ACVDAOFactory getInstance(Locale locale) {
		return (ACVDAOFactory) loadFactory(ACVDAOFactory.class, DEFAULT_KEY,
				ACVConfig.getInstance(), locale);  
	}

	/**
	 * M�todo getInstance.
	 * 
	 * @return
	 */
	public static ACVDAOFactory getInstance() {
		return (ACVDAOFactory) loadFactory(ACVDAOFactory.class, DEFAULT_KEY,
				ACVConfig.getInstance());
	}

	/**
	 * M�todo getVentaSegurosDAO.
	 * 
	 * @param <T>
	 * @param interfazDAO
	 * @return
	 */
	public <T> T getVentaSegurosDAO(Class<T> interfazDAO) {
		String className = this.getConfigurator().getString(
				interfazDAO.getName() + ".IMPL");
		try {
			Class<?> clase = Class.forName(className);
			return (T) this.getDAO(interfazDAO, clase);
		} catch (ClassNotFoundException cnfe) {
			throw new SystemException(cnfe);
		}
	}
}