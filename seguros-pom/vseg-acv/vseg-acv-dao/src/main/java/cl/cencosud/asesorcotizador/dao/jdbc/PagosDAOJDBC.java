package cl.cencosud.asesorcotizador.dao.jdbc;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.acv.common.RespuestaTBK;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.asesorcotizador.dao.PagosDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

import com.tinet.exceptions.system.SystemException;

/**
 * TODO Falta descripcion de clase PagosDAOJDBC.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 30/10/2010
 */
public class PagosDAOJDBC extends ManagedBaseDAOJDBC implements PagosDAO {

    /**
     * Logger de la clase.
     */
    private static final Log logger = LogFactory.getLog(PagosDAOJDBC.class);

    public static final String SQL_GENERAR_TRANSACCION =
        "INSERT INTO TRANSACCION (ID_TRANSACCION, ID_SOLICITUD, "
            + "NUMERO_ORDEN_COMPRA, TIPO_MEDIO_PAGO, TIPO_CARGO, MONTO, "
            + "ESTADO_TRANSACCION, NRO_TARJETA_ENTRADA, NRO_TARJETA_SALIDA, "
            + "RESPUESTA_MEDIO, FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS, "
            + "RUT_CLIENTE) VALUES  (TRANSACCION_SEQ.nextval,? , ?, ?, ?, ?, "
            + "?, ?, null, null, sysdate, sysdate, 1, ?)";

    /**
     * TODO Describir atributo SQL_OBTENER_DATOS_TRANSACCION.
     */
    public static final String SQL_OBTENER_DATOS_TRANSACCION =
        "SELECT ID_TRANSACCION, ID_SOLICITUD, NUMERO_ORDEN_COMPRA, "
            + "TIPO_MEDIO_PAGO, TIPO_CARGO, MONTO, ESTADO_TRANSACCION, "
            + "NRO_TARJETA_ENTRADA, NRO_TARJETA_SALIDA, RESPUESTA_MEDIO, "
            + "FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS, RUT_CLIENTE "
            + "FROM TRANSACCION WHERE NUMERO_ORDEN_COMPRA = ? ";

    private static final String SQL_OBTENER_DATOS_EMAIL =
        "SELECT ID_PARAMETRO, GRUPO, GLOBAL, NOMBRE, DESCRIPCION, VALOR, "
            + "ES_EXTERNO, ID_PAIS, FECHA_CREACION, FECHA_MODIFICACION, "
            + "ORDEN_LISTADO FROM PARAMETRO_SISTEMA WHERE GRUPO = ? "
            + "AND NOMBRE= ?";

    private static final String SQL_UPDATE_SOLICITUD =
        "UPDATE SOLICITUD SET FECHA_MODIFICACION = {fn CURDATE()},"
            + "CODIGO_INSPECCION_VEHICULO = ? WHERE ID_SOLICITUD = ?";

    private static final String SQL_OBTENER_CUERPO_EMAIL =
        "SELECT ARCHIVO FROM ARCHIVO_EMAIL WHERE TIPO_EMAIL = ?";

    private static final String SQL_INSERT_TRANSACCION =
        "INSERT INTO TRANSACCION (ID_TRANSACCION, ID_SOLICITUD, NUMERO_ORDEN_COMPRA, "
            + "TIPO_MEDIO_PAGO, TIPO_CARGO, MONTO, ESTADO_TRANSACCION, NRO_TARJETA_ENTRADA, "
            + "NRO_TARJETA_SALIDA, RESPUESTA_MEDIO, FECHA_CREACION, FECHA_MODIFICACION, "
            + "ID_PAIS, RUT_CLIENTE) VALUES  (TRANSACCION_SEQ.nextval, ?, ?, "
            + "null, null, ?, ?, null, null, null, sysdate, sysdate, 1, ?)";

    private static final String SQL_ACTUALIZAR_ESTADO_TRANSACCION =
        "UPDATE TRANSACCION set ESTADO_TRANSACCION=? where ID_SOLICITUD=? "
            + "AND NUMERO_ORDEN_COMPRA=? ";

    private static final String SQL_ACTUALIZAR_TRANSACCION =
        "UPDATE TRANSACCION SET ESTADO_TRANSACCION=?, NRO_TARJETA_ENTRADA=?, TIPO_CARGO=?, "
            + "TIPO_MEDIO_PAGO=?, MONTO=? WHERE ID_SOLICITUD=? AND NUMERO_ORDEN_COMPRA=? ";

    private static final String SQL_OBTENER_TRANSACCION =
        "SELECT ID_TRANSACCION, ID_SOLICITUD, NUMERO_ORDEN_COMPRA, "
            + "TIPO_MEDIO_PAGO, TIPO_CARGO, MONTO, ESTADO_TRANSACCION, NRO_TARJETA_ENTRADA, "
            + "NRO_TARJETA_SALIDA, RESPUESTA_MEDIO, FECHA_CREACION, FECHA_MODIFICACION, "
            + "ID_PAIS, RUT_CLIENTE FROM TRANSACCION WHERE  ID_SOLICITUD=? "
            + "AND NUMERO_ORDEN_COMPRA=?";

    private static final String SQL_GRABAR_DATOS_RESPUESTA =
        "INSERT INTO RESPUESTA_TBK (TBK_ORDEN_COMPRA, TBK_TIPO_TRANSACCION, "
            + "TBK_RESPUESTA, TBK_MONTO, TBK_CODIGO_AUTORIZACION, "
            + "TBK_FINAL_NUMERO_TARJETA, TBK_FECHA_CONTABLE, TBK_FECHA_TRANSACCION, "
            + "TBK_HORA_TRANSACCION, TBK_ID_SESION, TBK_ID_TRANSACCION, TBK_TIPO_PAGO, "
            + "TBK_NUMERO_CUOTAS, TBK_TASA_INTERES_MAX, TBK_VCI, TBK_MAC, FECHA) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,{fn CURDATE()})";

    private static final String SQL_OBTENER_DATOS_RESPUESTA =
        "SELECT TBK_ORDEN_COMPRA, TBK_TIPO_TRANSACCION, "
            + "TBK_RESPUESTA, TBK_MONTO, TBK_CODIGO_AUTORIZACION, "
            + "TBK_FINAL_NUMERO_TARJETA, TBK_FECHA_CONTABLE, TBK_FECHA_TRANSACCION, "
            + "TBK_HORA_TRANSACCION, TBK_ID_SESION, TBK_ID_TRANSACCION, TBK_TIPO_PAGO, "
            + "TBK_NUMERO_CUOTAS, TBK_TASA_INTERES_MAX, TBK_VCI, TBK_MAC, FECHA FROM "
            + "RESPUESTA_TBK WHERE TBK_ID_SESION = ?";
    
    private static final String SQL_INSERTAR_CLIENTE_NO_REGISTRADO = 
    		"INSERT INTO CLIENTE_NO_REGISTRADO(ID_NO_REGISTRADO, RUT_CLIENTE, NOMBRE_CLIENTE, EMAIL_CLIENTE, TIPO_SEGURO, FECHA_CREACION) " 
    				+"VALUES(SQ_ID_NO_REGISTRADO.nextval, ?, ?, ?, ?,{fn CURDATE()})";

    /**
     * TODO Describir m�todo obtenerDatosTransaccion.
     * @param idSolicitud
     * @return
     */
    public Transaccion obtenerDatosTransaccion(int idSolicitud) {
        Object[] params = new Object[] { String.valueOf(idSolicitud) };

        Transaccion res = new Transaccion();

        List < Transaccion > lRes =
            this.query(Transaccion.class, SQL_OBTENER_DATOS_TRANSACCION, params);

        if (lRes.size() > 0) {
            res = lRes.get(0);
        }

        return res;
    }

    /**
     * TODO Describir m�todo obtenerDatosEmail.
     * @param nombre
     * @return
     */
    public String obtenerDatosEmail(String nombre) {
        String res = "";
        Object[] params = new Object[] { "CONFIG_EMAIL", nombre };
        List < Map < String, String >> lres =
            this.query(Map.class, SQL_OBTENER_DATOS_EMAIL, params);
        if (lres.size() > 0) {
            res = lres.get(0).get("VALOR");
        }
        return res;
    }

    /**
     * TODO Describir m�todo obtenerCuerpoEmail.
     * @param nombre
     * @return
     */
    public InputStream obtenerCuerpoEmail(String nombre) {

        InputStream is = null;

        Object[] params = new Object[] { nombre };
        Map < String, Object > mRes =
            (Map < String, Object >) this.find(Map.class,
                SQL_OBTENER_CUERPO_EMAIL, params);

        if (mRes != null && mRes.get("ARCHIVO") != null) {
            try {
                Blob archivo = (Blob) mRes.get("ARCHIVO");

                is = archivo.getBinaryStream();

            } catch (SQLException e) {
                throw new SystemException(e);
            }

        }
        return is;
    }

    /**
     * TODO Describir metodo actualizaSolitud.
     * @param idSolicitud
     * @param codigoSolicitudInspeccion
     */
    public void actualizaSolitud(long idSolicitud,
        String codigoSolicitudInspeccion) {
        Object[] params =
            new Object[] { codigoSolicitudInspeccion, idSolicitud };
        int opt = this.update(SQL_UPDATE_SOLICITUD, params);
    }

    /**
     * TODO Describir m�todo generarTransaccion.
     * @param transaccion
     */
    public void generarTransaccion(Transaccion transaccion) {
        Object[] params =
            new Object[] { transaccion.getId_solicitud(),
                transaccion.getNumero_orden_compra(), transaccion.getMonto(),
                transaccion.getEstado_transaccion(),
                transaccion.getRut_cliente() };
        try {
            this.insert(SQL_INSERT_TRANSACCION, params);
        } catch (SystemException e) {

            if (e.getException() instanceof SQLException) {
                if (e.getException().getMessage().contains("ORA-00001")) {
                    logger.debug("Controlando clave duplicada.");
                    logger.debug("error: " + e);
                } else {
                    throw e;
                }
            } else{
                throw e;
            }
        }
    }

    /**
     * TODO Describir m�todo actualizarEstadoTransaccion.
     * @param transaccion
     */
    public void actualizarEstadoTransaccion(Transaccion transaccion) {
        Object[] params =
            new Object[] { transaccion.getEstado_transaccion(),
                transaccion.getId_solicitud(),
                transaccion.getNumero_orden_compra() };
        this.update(SQL_ACTUALIZAR_ESTADO_TRANSACCION, params);
    }

    /**
     * TODO Describir m�todo actualizarTransaccion.
     * @param transaccion
     */
    public void actualizarTransaccion(Transaccion transaccion) {
        Object[] params =
            new Object[] { transaccion.getEstado_transaccion(),
                transaccion.getNro_tarjeta_entrada(),
                transaccion.getTipo_cargo(), transaccion.getTipo_medio_pago(),
                transaccion.getMonto(),
                transaccion.getId_solicitud(),
                transaccion.getNumero_orden_compra() };
        this.update(SQL_ACTUALIZAR_TRANSACCION, params);
    }

    /**
     * TODO Describir m�todo obtenerDatosTransaccion.
     * @param transaccion
     * @return
     */
    public Transaccion obtenerDatosTransaccion(Transaccion transaccion) {
        Object[] params =
            new Object[] { transaccion.getId_solicitud(),
                transaccion.getNumero_orden_compra() };
        return (Transaccion) this.find(Transaccion.class,
            SQL_OBTENER_TRANSACCION, params);
    }

    /**
     * TODO Describir m�todo grabarDatosTransaccion.
     * @param parametros
     */
    public long grabarDatosRespuesta(RespuestaTBK respuesta) {

        Object[] params =
            new Object[] { respuesta.getTBK_ORDEN_COMPRA(),
                respuesta.getTBK_TIPO_TRANSACCION(),
                respuesta.getTBK_RESPUESTA(), respuesta.getTBK_MONTO(),
                respuesta.getTBK_CODIGO_AUTORIZACION(),
                respuesta.getTBK_FINAL_NUMERO_TARJETA(),
                respuesta.getTBK_FECHA_CONTABLE(),
                respuesta.getTBK_FECHA_TRANSACCION(),
                respuesta.getTBK_HORA_TRANSACCION(),
                respuesta.getTBK_ID_SESION(),
                respuesta.getTBK_ID_TRANSACCION(),
                respuesta.getTBK_TIPO_PAGO(), respuesta.getTBK_NUMERO_CUOTAS(),
                respuesta.getTBK_TASA_INTERES_MAX(), respuesta.getTBK_VCI(),
                respuesta.getTBK_MAC() };

        this.insertWithoutGeneratedKey(SQL_GRABAR_DATOS_RESPUESTA, params);

        return 1;
    }

    /**
     * TODO Describir m�todo obtenerDatosRespuesta.
     * @param idSolicitud
     * @return
     */
    public RespuestaTBK obtenerDatosRespuesta(long idSolicitud) {
        Object[] params = new Object[] { idSolicitud };
        return (RespuestaTBK) this.find(RespuestaTBK.class,
            SQL_OBTENER_DATOS_RESPUESTA, params);
    }
    
    /**
     * TODO Describir m�todo guardarNoRegistrado.
     * @param rutCliente, nombreCliente, mailCliente, tipoSeguro
     */
    public void guardarNoRegistrado(String rutCliente, String nombreCliente, String mailCliente, String tipoSeguro) {
        Object[] params = new Object[] { rutCliente, nombreCliente, mailCliente, tipoSeguro };
        this.insertWithoutGeneratedKey(SQL_INSERTAR_CLIENTE_NO_REGISTRADO, params);
    }
}
