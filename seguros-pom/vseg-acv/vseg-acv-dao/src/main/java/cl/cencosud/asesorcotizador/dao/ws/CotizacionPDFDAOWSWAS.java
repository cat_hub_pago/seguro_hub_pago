package cl.cencosud.asesorcotizador.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivilEnum;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.CotizacionPDFDAO;

import com.bigsa.ws.consulta.cotizacion.impl.*;

import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

/**
 * TODO Falta descripcion de clase CotizacionPDFDAOWS.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 05/11/2010
 */
public class CotizacionPDFDAOWSWAS extends BaseConfigurable implements CotizacionPDFDAO {

    private static Log logger = LogFactory.getLog(CotizacionPDFDAOWSWAS.class);
    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";
    private static final String USER_WS = "cl.cencosud.asesorcotizador.cotizador.registro.bigsa.usuario";
    private static final String WSDL_COTIZACION_PDF = "cl.cencosud.asesorcotizador.dao.CotizacionPDFDAOWS.wsdl.cotizacion.pdf";
	private static final String CODIGO_SCORING_TIPO = "codigo.pregunta.tipo.mascota"; 
	private static final String CODIGO_SCORING_RAZA = "codigo.pregunta.raza.mascota"; 
	private static final String CODIGO_SCORING_SEXO = "codigo.pregunta.sexo.mascota"; 
	private static final String CODIGO_SCORING_EDAD= "codigo.pregunta.edad.mascota"; 

    public void close() {}

    public Class getDAOInterface() {
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {}

    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws ExtraccionDatosException 
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method = stub.getClass().getMethod("_setProperty", String.class, Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param.get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    /**
     * TODO Describir m�todo obtenerExcepcionWS.
     * @param e
     * @throws ValorizacionException
     */
    private void obtenerExcepcionWS(RemoteException e)
        throws ValorizacionException {
        if (e.detail instanceof SOAPFaultException) {
            SOAPFaultException sfe = (SOAPFaultException) e.detail;
            Detail detail = sfe.getDetail();
            if (detail != null) {
                ValorizacionException vex =
                    obtenerErrorValorizacion(detail.getChildNodes());
                if (vex != null) {
                    logger.debug("Error de negocio durante invocacion a WS.",
                        vex);
                    throw vex;
                }
            }
        }
    }

    /**
     * TODO Describir m�todo obtenerErrorValorizacion.
     * @param nodes
     * @return
     */
    private ValorizacionException obtenerErrorValorizacion(NodeList nodes) {
        if (nodes == null) {
            return null;
        }
        ValorizacionException vex = null;
        String message = null;
        String codigo = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node item = nodes.item(i);
            vex = obtenerErrorValorizacion(item.getChildNodes());
            if (logger.isDebugEnabled()) {
                logger.debug(i + ") item : " + item);
                logger.debug(i + ") name : " + item.getNodeName());
                logger.debug(i + ") value: " + item.getNodeValue());
                logger.debug(i + ") eie  : " + vex);
            }
            if (vex == null) {
                if ("mensaje".equals(item.getNodeName())) {
                    message = item.getFirstChild().getNodeValue();
                } else if ("codigo".equals(item.getNodeName())) {
                    codigo = item.getFirstChild().getNodeValue();
                }
            } else {
                return vex;
            }
        }
        if ((message != null) && (codigo != null)) {
            logger.debug("Descripcion encontrada. Se crea excepcion.");
            vex = new ValorizacionException(message, Integer.valueOf(codigo));
        }
        return vex;
    }

    /**
     * Obtiene PDF con cotizacion de hogar.
     * @param cotHogar Datos de cotizacion.
     * @param solicitud Datos de la solicitud.
     * @return PDF obtenido.
     */
    public byte[] obtenerCotizacionHogarPDF(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        byte[] response = null;
        Context context = null;

        try {
            context = new InitialContext();
            WsBigsaCotizacionPDF svc = (WsBigsaCotizacionPDF) context.lookup("java:comp/env/service/WsBigsaCotizacionPDF");
            WsBigsaCotizacionPDFDelegate ws = svc.getWsBigsaCotizacionPDFPort(new URL(dao.getString(WSDL_COTIZACION_PDF)));

            String sRutCliente = String.valueOf(cotHogar.getRutCliente());
            int rutCliente = Integer.parseInt(sRutCliente);

            CotizacionHogarVO cotizacion = new CotizacionHogarVO();
            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));
            cotizacion.setCodigoPlan(new Long(cotHogar.getCodigoPlan()).intValue());

	        Contratante datos = cotHogar.getContratante();
	        TipoPersonaVO contratante = new TipoPersonaVO();
	        contratante.setRutPersona(new Long(datos.getRut()).intValue());
	        contratante.setDigitoPersona(datos.getDv());
	        contratante.setApePatPersona(datos.getApellidoPaterno());
	        contratante.setApeMatPersona(datos.getApellidoMaterno());
	        contratante.setNombresPersona(datos.getNombre());
	        contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(datos.getFechaDeNacimiento()));
	        String estadoCivil = EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil()).getDescripcion();
	        contratante.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivil));
	        contratante.setSexoPersona(datos.getSexo());
	        contratante.setFono1Persona(datos.getTelefono1());
	        contratante.setFono2Persona(datos.getTelefono2());
	        
	        if(datos.getCalleDireccion() != null && datos.getNumeroDireccion() != null || datos.getNumeroDepartamentoDireccion() != null){
	        	logger.info("Direccion contratante ingresada");
		        contratante.setDirCallePersona(datos.getCalleDireccion());
		        contratante.setDirNroPersona(datos.getNumeroDireccion());
		        contratante.setDirDeptoPersona(datos.getNumeroDepartamentoDireccion());
	        }
	        
	        cotizacion.setContratante(contratante);
	
	        if(cotHogar.isClienteEsAsegurado()){
	            cotizacion.setAsegurado(contratante);      
	        }else{
	            Contratante asegurado = cotHogar.getAsegurado();
	            TipoPersonaVO aseguradoLeg = new TipoPersonaVO();
	            aseguradoLeg.setRutPersona(new Long(asegurado.getRut()).intValue());
	            aseguradoLeg.setDigitoPersona(asegurado.getDv());
	            aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
	            aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
	            aseguradoLeg.setNombresPersona(asegurado.getNombre());
	            
	            if(asegurado.getSexo() !=null && asegurado.getTelefono1() !=null && asegurado.getFechaDeNacimiento() !=null){
	                aseguradoLeg.setSexoPersona(asegurado.getSexo());       
		            aseguradoLeg.setFono1Persona(asegurado.getTelefono1());
		            aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(asegurado.getFechaDeNacimiento()));
	            }
	           	            
	            // Se deja por defecto ya que no se tiene estado civil asegurado.
	            estadoCivil = EstadoCivilEnum.valueOf("_" + 1).getDescripcion();
	            contratante.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivil));
	            
	            if(asegurado.getCalleDireccion() != null && asegurado.getNumeroDireccion() != null || 
	            		asegurado.getNumeroDepartamentoDireccion() != null){
	            	logger.info("Direccion asegurado ingresada");
		            aseguradoLeg.setDirCallePersona(asegurado.getCalleDireccion());
		            aseguradoLeg.setDirNroPersona(asegurado.getNumeroDireccion());
		            aseguradoLeg.setDirDeptoPersona(asegurado.getNumeroDepartamentoDireccion());
	            }
	            
	            cotizacion.setAsegurado(aseguradoLeg);
	        }

	        cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy").format(solicitud.getFecha_creacion()));
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(solicitud.getFecha_creacion());
	        cal.add(Calendar.MONTH, 1);
	        cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
	        cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());
	
	        SeguroHogarVO hogar = new SeguroHogarVO();
	        hogar.setDireccionRiesgo(cotHogar.getDireccion());
	        hogar.setCodigoRegionRiesgo(Integer.valueOf(cotHogar.getRegion()));
	        hogar.setCodigoComunaRiesgo(Integer.valueOf(cotHogar.getComuna()));
	        hogar.setCodigoCiudadRiesgo(Integer.valueOf(cotHogar.getCiudad()));
	        cotizacion.setHogar(hogar);
	        
	        response = ws.obtenerCotizacionHogarPDF(rutCliente, cotizacion);
        } catch (RemoteException e) {
			try {
				logger.error("Remote Exception: ", e);
				this.obtenerExcepcionWS(e);
			} catch (ValorizacionException e1) {
				e1.printStackTrace();
			}
				e.printStackTrace();
			} catch (ErrorInternoException e) {
				e.printStackTrace();
			} catch (NamingException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (ServiceException e) {
				e.printStackTrace();
			} finally {
				ResourceLeakUtil.close(context);
				ResourceLeakUtil.close(dao);
			}	
        return response;
    }

    /**
     * TODO Describir m�todo obtenerCotizacionVehiculoPDF.
     * @param vehiculo
     * @param solicitud
     * @return
     */
    public byte[] obtenerCotizacionVehiculoPDF(CotizacionSeguroVehiculo vehiculo, Solicitud solicitud) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        byte[] response = null;
        Context context = null;

        try {
            context = new InitialContext();
            WsBigsaCotizacionPDF svc = (WsBigsaCotizacionPDF) context.lookup("java:comp/env/service/WsBigsaCotizacionPDF");
            WsBigsaCotizacionPDFDelegate ws = svc.getWsBigsaCotizacionPDFPort(new URL(dao.getString(WSDL_COTIZACION_PDF)));

            String sRutCliente = String.valueOf(vehiculo.getRutCliente());
            int rutCliente = Integer.parseInt(sRutCliente);

            CotizacionVehiculoVO cotizacion = new CotizacionVehiculoVO();
            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS)); 
            cotizacion.setCodigoPlan(new Long(vehiculo.getCodigoPlan()).intValue());

            Contratante datos = vehiculo.getContratante();
            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue()); 
            contratante.setDigitoPersona(datos.getDv()); 
            contratante.setNombresPersona(datos.getNombre()); 
            
            if(datos.getApellidoPaterno() != null && datos.getApellidoMaterno() != null){
                contratante.setApePatPersona(datos.getApellidoPaterno()); 
                contratante.setApeMatPersona(datos.getApellidoMaterno()); 
            }
            
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(datos.getFechaDeNacimiento()));
            String estadoCivil = EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil()).getDescripcion();
            contratante.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivil));
            contratante.setSexoPersona(datos.getSexo());
            contratante.setFono1Persona(vehiculo.getNumeroTelefono1()); 
            contratante.setEmailPersona(datos.getEmail()); 
            
            if(datos.getIdRegion() != null && datos.getIdComuna() != null){
            	logger.info("Reg y Com del contratante ingresada");
                contratante.setCodigoRegionPersona(Integer.parseInt(datos.getIdRegion()));
                contratante.setCodigoComunaPersona(Integer.parseInt(datos.getIdComuna()));
            }
            
            cotizacion.setContratante(contratante);

            if (vehiculo.isClienteEsAsegurado()) {
                cotizacion.setAsegurado(contratante);
            } else {
                Contratante asegurado = vehiculo.getAsegurado();
                TipoPersonaVO aseguradoLeg = new TipoPersonaVO();
                aseguradoLeg.setRutPersona(new Long(asegurado.getRut()).intValue());
                aseguradoLeg.setDigitoPersona(asegurado.getDv());
                aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
                aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
                aseguradoLeg.setNombresPersona(asegurado.getNombre());
                aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(asegurado.getFechaDeNacimiento()));
                estadoCivil = EstadoCivilEnum.valueOf("_" + asegurado.getEstadoCivil()).getDescripcion();
                aseguradoLeg.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivil));
                aseguradoLeg.setSexoPersona(asegurado.getSexo());
                aseguradoLeg.setFono1Persona(asegurado.getTelefono1());
                
                if(asegurado.getIdRegion() != null && asegurado.getIdComuna() != null){
                	logger.info("Reg y Com del asegurado ingresada");
                    aseguradoLeg.setCodigoRegionPersona(Integer.parseInt(asegurado.getIdRegion()));
                    aseguradoLeg.setCodigoComunaPersona(Integer.parseInt(asegurado.getIdComuna()));
                }
                
                cotizacion.setAsegurado(aseguradoLeg);
            }
            
            cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy").format(solicitud.getFecha_creacion())); 
            Calendar cal = Calendar.getInstance();
            cal.setTime(solicitud.getFecha_creacion());
            cal.add(Calendar.MONTH, 1);
            cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime())); 
            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf()); 

            SeguroVehiculoVO vehiculoLeg = new SeguroVehiculoVO();
            vehiculoLeg.setCodigoMarca(vehiculo.getCodigoMarca()); 
            vehiculoLeg.setCodigoModelo(vehiculo.getCodigoModelo()); 
            vehiculoLeg.setCodigoTipoVehiculo(vehiculo.getCodigoTipoVehiculo());
            vehiculoLeg.setAnoVehiculo(vehiculo.getAnyoVehiculo()); 
            vehiculoLeg.setPatente(vehiculo.getPatente()); 
            cotizacion.setVehiculo(vehiculoLeg);

            response = ws.obtenerCotizacionVehiculoPDF(rutCliente, cotizacion);

        } catch (RemoteException e) {
            try {
                logger.error("Remote Exception: ", e);
                this.obtenerExcepcionWS(e);
            } catch (ValorizacionException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch (ErrorInternoException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        } finally {
            ResourceLeakUtil.close(context);
            ResourceLeakUtil.close(dao);
        }
        
        return response;
    }

    /**
     * TODO Describir m�todo obtenerCotizacionVidaPDF.
     * @param cotVida
     * @param solicitud
     * @return
     */
    public byte[] obtenerCotizacionVidaPDF(CotizacionSeguroVida cotVida, Solicitud solicitud) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        byte[] response = null;
        Context context = null;

        try {
            context = new InitialContext();
            WsBigsaCotizacionPDF svc = (WsBigsaCotizacionPDF) context.lookup("java:comp/env/service/WsBigsaCotizacionPDF");
            WsBigsaCotizacionPDFDelegate ws = svc.getWsBigsaCotizacionPDFPort(new URL(dao.getString(WSDL_COTIZACION_PDF)));

            String sRutCliente = String.valueOf(cotVida.getRutCliente());
            int rutCliente = Integer.parseInt(sRutCliente);
            
            CotizacionVidaVO cotizacion = new CotizacionVidaVO();
            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));
            cotizacion.setCodigoPlan(new Long(cotVida.getCodigoPlan()).intValue());

            Contratante datos = cotVida.getContratante();
            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue());
            contratante.setDigitoPersona(datos.getDv());
            contratante.setApePatPersona(datos.getApellidoPaterno());
            contratante.setApeMatPersona(datos.getApellidoMaterno());
            contratante.setNombresPersona(datos.getNombre());
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(datos.getFechaDeNacimiento()));
            String estadoCivil = EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil()).getDescripcion();            
            contratante.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivil));
            contratante.setSexoPersona(datos.getSexo());
            contratante.setFono1Persona(cotVida.getNumeroTelefono1());
            
            if(datos.getCalleDireccion() != null && datos.getNumeroDireccion() != null || datos.getNumeroDepartamentoDireccion() != null){
            	logger.info("Direccion contratante ingresada");
                contratante.setDirCallePersona(datos.getCalleDireccion());
                contratante.setDirNroPersona(datos.getNumeroDireccion());
                contratante.setDirDeptoPersona(datos.getNumeroDepartamentoDireccion());
            }
            
            cotizacion.setContratante(contratante);
            
            //INICIO CONTRATACION TERCEROS
        	Contratante registro = cotVida.getAsegurado();
        	TipoPersonaVO asegurado = new TipoPersonaVO();
        	
            if (cotVida.isClienteEsAsegurado()) {
            	cotizacion.setAsegurado(contratante);
            }else{
            	asegurado.setRutPersona(new Long(registro.getRut()).intValue());
            	asegurado.setDigitoPersona(registro.getDv());
            	asegurado.setApePatPersona(registro.getApellidoPaterno());
            	asegurado.setApeMatPersona(registro.getApellidoMaterno());
            	asegurado.setNombresPersona(registro.getNombre());
            	asegurado.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(registro.getFechaDeNacimiento()));
                String estadoCivilAsegurado = EstadoCivilEnum.valueOf("_" + registro.getEstadoCivil()).getDescripcion();            
                asegurado.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivilAsegurado));
                asegurado.setSexoPersona(registro.getSexo());
            	cotizacion.setAsegurado(asegurado);
            }
            //FIN CONTRATACION TERCEROS
            
            cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy").format(solicitud.getFecha_creacion()));
            Calendar cal = Calendar.getInstance();
            cal.setTime(solicitud.getFecha_creacion());
            cal.add(Calendar.MONTH, 1);
            cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());
            
            SeguroVidaVO vida = new SeguroVidaVO();    
            vida.setValorBaseCalculo(365f);
            DatosAseguradoVO aseguradoVida = new DatosAseguradoVO();
            
            //INICIO CONTRATACION TERCEROS
            if (cotVida.isClienteEsAsegurado()) {
                aseguradoVida.setRutAseg(contratante.getRutPersona());
                aseguradoVida.setDigitoAseg(contratante.getDigitoPersona());
                aseguradoVida.setApePaternoAseg(contratante.getApePatPersona());
                aseguradoVida.setApeMaternoAseg(contratante.getApeMatPersona());
                aseguradoVida.setNombresAseg(contratante.getNombresPersona());
                aseguradoVida.setFecNacAseg(contratante.getFecNacPersona());         
                aseguradoVida.setParentAseg(ParentAseg.value1);
                aseguradoVida.setSexoAseg(contratante.getSexoPersona());            	
            }else{
                aseguradoVida.setRutAseg(asegurado.getRutPersona());
                aseguradoVida.setDigitoAseg(asegurado.getDigitoPersona());
                aseguradoVida.setApePaternoAseg(asegurado.getApePatPersona());
                aseguradoVida.setApeMaternoAseg(asegurado.getApeMatPersona());
                aseguradoVida.setNombresAseg(asegurado.getNombresPersona());
                aseguradoVida.setFecNacAseg(asegurado.getFecNacPersona());         
                aseguradoVida.setParentAseg(ParentAseg.value1);
                aseguradoVida.setSexoAseg(asegurado.getSexoPersona());       
            }
            //FIN CONTRATACION TERCEROS
            
            DatosBenficiarioVO oBeneficiario = new DatosBenficiarioVO();
            
            //INICIO CONTRATACION TERCEROS
            if (cotVida.isClienteEsAsegurado()) {
                oBeneficiario.setRutBene(contratante.getRutPersona());
                oBeneficiario.setDigitoBene(contratante.getDigitoPersona());
                oBeneficiario.setApePaternoBene(contratante.getApePatPersona());
                oBeneficiario.setApeMaternoBene(contratante.getApeMatPersona());
                oBeneficiario.setNombresBene(contratante.getNombresPersona());
                oBeneficiario.setFecNacBene(contratante.getFecNacPersona());
                oBeneficiario.setParentBene(aseguradoVida.getParentAseg().toString());
                oBeneficiario.setSexoBene(contratante.getSexoPersona());
                oBeneficiario.setPorcBene(100f);
            }else{
                oBeneficiario.setRutBene(asegurado.getRutPersona());
                oBeneficiario.setDigitoBene(asegurado.getDigitoPersona());
                oBeneficiario.setApePaternoBene(asegurado.getApePatPersona());
                oBeneficiario.setApeMaternoBene(asegurado.getApeMatPersona());
                oBeneficiario.setNombresBene(asegurado.getNombresPersona());
                oBeneficiario.setFecNacBene(asegurado.getFecNacPersona());
                oBeneficiario.setParentBene(aseguradoVida.getParentAseg().toString());
                oBeneficiario.setSexoBene(asegurado.getSexoPersona());
                oBeneficiario.setPorcBene(100f);
            }
            //FIN CONTRATACION TERCEROS
            
			// INICIO MASCOTA
			RespuestaScoringVO respuestaTipo = new RespuestaScoringVO();
			RespuestaScoringVO respuestaRaza = new RespuestaScoringVO();
			RespuestaScoringVO respuestaSexo = new RespuestaScoringVO();
			RespuestaScoringVO respuestaEdad = new RespuestaScoringVO();
			
			String numSubcategoria = cotVida.getNumSubcategoria() == null ? ""
					: cotVida.getNumSubcategoria();
			
			if(numSubcategoria.equals("140")){
				logger.info("Scoring Mascota");
				
				//TIPO MASCOTA
				int codigoTipo = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_TIPO));
				respuestaTipo.setCodigoPregunta(codigoTipo);
				respuestaTipo.setValorAlternativa(cotVida.getTipoMascota());
							
				//RAZA MASCOTA
				int codigoRaza = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_RAZA));
				respuestaRaza.setCodigoPregunta(codigoRaza);
				respuestaRaza.setTexto(cotVida.getRazaMascota());
				
				//SEXO MASCOTA
				int codigoSexo = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_SEXO));
				respuestaSexo.setCodigoPregunta(codigoSexo);
				respuestaSexo.setTexto(cotVida.getSexoMascota());
				
				//EDAD MASCOTA
				int codigoEdad = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_EDAD));
				respuestaEdad.setCodigoPregunta(codigoEdad);
				respuestaEdad.setValorNumeral(Float.parseFloat(cotVida.getEdadMascota()));		
				
				RespuestaScoringVO[] scoring = new RespuestaScoringVO[] {
						respuestaTipo, respuestaRaza, 
						respuestaSexo, respuestaEdad };
				
				cotizacion.setScoring(scoring);
			}	
			// FIN MASCOTA
				
            aseguradoVida.setBeneficiarioVida(new DatosBenficiarioVO[]{oBeneficiario});
            vida.setAseguradoVida(new DatosAseguradoVO[]{aseguradoVida});       
            cotizacion.setVida(vida);    
           
            response = ws.obtenerCotizacionVidaPDF(rutCliente, cotizacion);

        } catch (RemoteException e) {
            try {
                logger.error("Remote Exception: ", e);
                logger.info("Remote Exception: ", e);
                this.obtenerExcepcionWS(e);
            } catch (ValorizacionException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch (ErrorInternoException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        } finally {
            ResourceLeakUtil.close(context);
            ResourceLeakUtil.close(dao);
        }

        return response;
    }
}
