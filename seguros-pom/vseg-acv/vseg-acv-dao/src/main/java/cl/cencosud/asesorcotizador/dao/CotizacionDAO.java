package cl.cencosud.asesorcotizador.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.Beneficiario;
import cl.cencosud.acv.common.Cobertura;
import cl.cencosud.acv.common.ColorVehiculo;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.ParentescoAsegurado;
import cl.cencosud.acv.common.PatenteVehiculo;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Rama;
import cl.cencosud.acv.common.RestriccionVida;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.Subcategoria;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * Interfaz que contiene servicios para el m�dulo de Cotizaci�n.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 30/09/2010
 */
public interface CotizacionDAO extends BaseDAO {

    /**
     * Obtiene los Datos de un Contratante.
     * @param rut rut del cliente
     * @param dv digito verificador.
     * @return datos del contratante.
     */
    public Contratante obtenerDatosContratante(long idSolicitud);

    /**
     * Obtiene los Datos de un Asegurado.
     * @param rut rut del cliente
     * @param dv digito verificador.
     * @return datos del asegurado.
     */
    public Contratante obtenerDatosAsegurado(long idSolicitud);

    /**
     * Obtiene los Tipos de Vehiculo.
     * @return tipos de vehiculo.
     */
    public Vehiculo[] obtenerTiposVehiculos();
    
    /**
     * Obtiene los Tipos de Vehiculo seg�n los tipos de Rama.
     * @return tipos de vehiculo.
     */
    public Vehiculo[] obtenerTiposVehiculos( int idTipo );

    /**
     * Obtiene las Marcas de Vehiculos.
     * @return marcas de vehiculo.
     */
    public Vehiculo[] obtenerMarcasVehiculos();

    /**
     * Obtiene los Modelos de Vehiculos.
     * @param idTipo tipo de vehiculo.
     * @param idMarca marca de vehiculo.
     * @return marcas de vehiculo.
     */
    public Vehiculo[] obtenerModelosVehiculos(String idTipo, String idMarca);

    /**
     * Obtiene los Planes asociados a un Prodcuto de una Subcategor�a.
     * @param idProducto id de producto
     * @return planes asociados.
     */
    public Plan[] obtenerPlanesCotizacion(String idProducto,
        String idPlanPromocion);

    /**
     * Obtiene un Plan en Promoci�n determinado.
     * @param idProducto id del producto.
     * @param idPlan id del plan.
     * @return planes en promoci�n.
     */
    public Plan[] obtenerPlanPromocion(String idProducto, String idPlan);

    /**
     * Obtiene listado de Productos seg�n Subcategor�a de una Rama.
     * @param idSubcategoria identificador de subcategoria.
     * @param idRama identificador de rama
     * @return productos.
     */
    public Producto[] obtenerProductos(String idSubcategoria, String idRama,
        String idPlan);

    /**
     * Obtiene listado de Coberturas seg�n un id Plan.
     * @param idPlan identificador de plan.
     * @return coberturas asociadas.
     */
    public Cobertura[] obtenerCoberturas(String idPlan);

    /**
     * Obtiene listado de Actividades disponibles.
     * @return actividades
     */
    public Actividad[] obtenerActividades();

    /**
     * Obtiene listado de Estado Civiles disponibles. 
     * @return estados civiles.
     */
    public EstadoCivil[] obtenerEstadosCiviles();

    /**
     * Obtiene listado de Regiones.
     * @return listado de regiones.
     */
    public ParametroCotizacion[] obtenerRegiones();

    /**
     * Obtiene listado de Comunas.
     * @param idRegion id de region.
     * @return listado de comunas.
     */
    public ParametroCotizacion[] obtenerComunas(String idRegion);

    /**
     * Obtiene listado de Ciudades.
     * @param idComuna id de comuna.
     * @return listado de ciudades.
     */
    public ParametroCotizacion[] obtenerCiudades(String idComuna);

    /**
     * Un listado de seg�n parametro de sistema.
     * @param idGrupo identificador del grupo de parametros.
     * @return obtiene un parametro en particular.
     */
    public List < Map < String, ? > > obtenerParametro(String idGrupo);

    /**
     * Obtiene un parametro de sistema en particular.
     * @param idGrupo identificador de grupo de parametros.
     * @param idParametro identificador del parametro.
     * @return parametro de sistema.
     */
    public Map < String, ? > obtenerParametro(String idGrupo, String idParametro);

    /**
     * Obtiene informaci�n de un plan en particular.
     * @param idProducto identificador de producto.
     * @param idPlan identificador de plan.
     * @return Plan
     */
    public Plan[] obtenerPlanCotizacion(String idProducto, String idPlan);

    /**
     * Obtiene codigos legacy de un vehiculo.
     * @param idTipo tipo de vehiculo
     * @param idMarca marca de vehiculo
     * @param idModelo modelo de vehiculo
     * @return C�digos legacy de un vechiculo
     */
    public Vehiculo obtenerVehiculoLegacy(String idTipo, String idMarca,
        String idModelo);

    /**
     * Almacena los datos en la tabla Solicitud.
     * @param 
     * @return identificador del registro.
     */
    public long agregarDatosSolicitud(Plan plan, int clienteEsAsegurado,
        String idRama, String idProducto, String estadoSolicitud,
        String nroTarjeta, long rut, String idVitrineo);

    /**
     * Agrega un registro en la tabla Contratante.
     * @return identificador del registro.
     */
    public long agregarDatosContratante(long idSolicitud,
        CotizacionSeguro cotizacion);

    /**
     * Agrega un registro en la tabla Asegurado.
     * @return indentificador del registro.
     */
    public long agregarDatosAsegurado(long idSolicitud, Contratante asegurado);

    /**
     * Agrega un registro en la tabla Materia Hogar
     * @return indentificador del registro.
     */
    public long agregarDatosMateriaHogar(CotizacionSeguroHogar datos,
        long idSolicitud);

    /**
     * Agrega un registro en la tabla Materia Vida.
     * @return identificador del registro.
     */
    public long agregarDatosMateriaVida(CotizacionSeguroVida cotizacion,
        long idSolicitud);

    /**
     * Agrega un registro en la tabla Beneficiario.
     * @return identificador del registro.
     */
    public long agregarDatosVidaBeneficiario(long idMateria,
        Beneficiario beneficiario);

    /**
     * Agrega un registro en la tabla Asegurado Adicional.
     * @return identificador del registro.
     */
    public long agregarDatosVidaAdicional(long idMateria, Beneficiario adicional);

    /**
     * Agrega un registro en la tabla Materia Vehiculo.
     * @return identificador del registro.
     */
    public long agregarDatosMateriaVehiculo(CotizacionSeguroVehiculo datos,
        long idSolicitud);

    /**
     * Agrega un registro en la tabla Factura.
     * @return identificador del registro.
     */
    public long agregarDatosVehiculoFactura();

    public void actualizarEstadoSolicitud(long idSolicitud, String idEstado);

    public void actualizarContratante(long idSolicitud, CotizacionSeguro datos);

    public void actualizarAsegurado(long idSolicitud, CotizacionSeguro datos);
    
    public void actualizarAsegurado(long idSolicitud, Contratante datos);

    public void actualizarMateriaVehiculo(long idSolicitud,
        CotizacionSeguroVehiculo datos);

    public void actualizarFactura(long idSolicitud, CotizacionSeguro datos);

    public void actualizarMateriaVida(long idSolicitud,
        CotizacionSeguroVida datos);

    public void actualizarMateriaHogar(long idSolicitud,
        CotizacionSeguroHogar datos);

    public ColorVehiculo[] obtenerColoresVehiculo();

    public ParentescoAsegurado[] obtenerParentescoRestriccion(long idRestriccion);

    public RestriccionVida obtenerRestriccionVida(long idPlan);

    public Solicitud obtenerDatosSolicitud(long idSolicitud);

    Rama obtenerRamaPorId(long idRama);
    
    Subcategoria obtenerSubcategoriaPorId(long idSubcategoria);

    CotizacionSeguroVehiculo obtenerDatosMateriaVehiculo(long idSolicitud);

    CotizacionSeguroVida obtenerDatosMateriaVida(long idSolicitud);

    CotizacionSeguroHogar obtenerDatosMateriaHogar(long idSolicitud);

    long ingresarFacturaVehiculo(long idMateriaVehiculo, byte[] factura);

    Plan obtenerDatosPlan(long idPlan);

    boolean existePrimaUnica(long idPlan);

    Map < String, Object > obtenerRegionPorId(String idRegion);

    Map < String, Object > obtenerComunaPorId(String idComuna);

    Map < String, Object > obtenerCiudadPorId(String idCiudad);
    
    long obtenerSubcategoriaAsociada(String idProducto, String idRama);

    Vehiculo[] obtenerMarcasPorTipoVehiculos(String idTipo);

	public int obtenerRutSinCaptcha(long rutsincap);   

	
	
	public List < HashMap < String, String > > obtenerVehiculos(String rut);
	
	public Map<String, Object> obtenerVehiculosCompleto(String rut,String modelo);
	
	public Map<String, Object> obtenerDatosVehiculosCompleto(String tipo,String marca, String modelo);

	

	public long grabarVitrineoGeneral(String pswd, String rutVitrineo,
			String dvVitrineo, String nombre, String apellidoPaterno,
			String apellidoMaterno, String tipoTelefono, String codTelefono,
			String telefono, String email, int region, int comuna,
			String diaNac, String mesNac, String anyoNac, String estadoCivil,
			String sexo, long edadConductor, int tipoVehiculo, int idMarcaVitrineo,
			int idModeloVitrineo, String idAnyoVitrineo, int numPuertas,
			String esDuenyo, String ciudadHogar, String direccionHogar,
			String numeroDireccionHogar, String numeroDeptoHogar,
			String anyoHogar, int rama, int subcategoria, int productoVitrineo,
			String rutDuenyo, String dvDuenyo, String nombreDuenyo,
			String apellidoPaternoDuenyo, String apellidoMaternoDuenyo,
			String diaNacimientoDuenyo, String mesNacimientoDuenyo,
			String anyoNacimientoDuenyo, String estadoCivilDuenyo,
			String regionResidenciaDuenyo, String comunaResidenciaDuenyo,
			String valorComercial);

	public Map<String, Object> obtenerDatosVitrineo(
			String claveVitrineo);

	public Plan[] obtenerPlanesDeducibles(String idProducto2,
			String idProducto3, String idProducto4, String idProducto5,
			String idProducto6, String idProducto7, String idPlanPromocion);

	public Plan[] obtenerPlanCotizacionDeducibles(String idProducto2,
			String idProducto3, String idProducto4, String idProducto5,
			String idProducto6, String idProducto7, String idPlan);
	
    public void guardarMascota(String rutDuenoMascota, String nombreMascota, String tipoMascota,
    		String razaMascota, String colorMascota, String edadMascota, String sexoMascota, String direccionMascota);
    
    public String obtenerIdFicha(int idSubcategoria);
    
    //Pleyasoft - Patente Vehiculo
    public PatenteVehiculo obtenerDatosPatente(String idPatente);
    
    public long agregarDatosPatente(PatenteVehiculo p);
    
    public long actualizarDatosPatente(PatenteVehiculo p);
    //Pleyasoft - Patente Vehiculo
}
