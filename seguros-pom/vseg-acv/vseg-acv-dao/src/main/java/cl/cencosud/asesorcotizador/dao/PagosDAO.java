package cl.cencosud.asesorcotizador.dao;

import java.io.InputStream;

import cl.cencosud.acv.common.RespuestaTBK;
import cl.cencosud.acv.common.Transaccion;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * TODO Falta descripcion de clase PagosDAO.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 30/10/2010
 */
public interface PagosDAO extends BaseDAO {

    /**
     * TODO Describir m�todo obtenerDatosTransaccion.
     * @param idSolicitud
     * @return
     */
    Transaccion obtenerDatosTransaccion(int idSolicitud);

    /**
     * TODO Describir m�todo obtenerDatosEmail.
     * @param nombre
     * @return
     */
    String obtenerDatosEmail(String nombre);

    /**
     * TODO Describir m�todo obtenerCuerpoEmail.
     * @param nombre
     * @return
     */
    InputStream obtenerCuerpoEmail(String nombre);

    /**
     * TODO Describir metodo actualizaSolitud.
     * @param idSolicitud
     * @param codigoSolicitudInspeccion
     */
    void actualizaSolitud(long idSolicitud, String codigoSolicitudInspeccion);

    void generarTransaccion(Transaccion transaccion);

    void actualizarEstadoTransaccion(Transaccion transaccion);

    void actualizarTransaccion(Transaccion transaccion);

    Transaccion obtenerDatosTransaccion(Transaccion transaccion);

    long grabarDatosRespuesta(RespuestaTBK respuesta);

    RespuestaTBK obtenerDatosRespuesta(long idSolicitud);
    
    void guardarNoRegistrado(String rutCliente, String nombreCliente, String mailCliente, String tipoSeguro);
}
