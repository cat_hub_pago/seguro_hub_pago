package cl.cencosud.asesorcotizador.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.acv.common.Beneficiario;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivilEnum;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.RegistrarCotizacionDAO;
import com.bigsa.ws.gestion.grabar.cotizacion.impl.*;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.logger.Logging;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class RegistrarCotizacionDAOWSWAS extends BaseConfigurable implements
		RegistrarCotizacionDAO {

	private static Log logger = LogFactory
			.getLog(RegistrarCotizacionDAOWSWAS.class);

	private static final String WSDL_REGISTRAR_COTIZACION = "cl.cencosud.asesorcotizador.dao.ws.RegistrarCotizacionDAOWS.wsdl.registrar.cotizacion";
	private static final String USER_WS = "cl.cencosud.asesorcotizador.cotizador.registro.bigsa.usuario";
	private static final String SCORING_VEHICULO_ESTADO_CIVIL = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.estadoCivil";
	private static final String SCORING_VEHICULO_MEDIO_INFORMATIVO = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.medioInformativo";
	private static final String SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO_CODE = "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.medioInformativoCode";
	private static final String SCORING_VEHICULO_RESPUESTA_NUMERO_PUERTAS_DE_2_A_3 = "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.nroPuertas.de2a3";
	private static final String SCORING_VEHICULO_NUMERO_PUERTAS = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.nroPuertas";
	private static final String SCORING_VEHICULO_EDAD_CONTRATANTE = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadContratante";
	private static final String SCORING_VEHICULO_SEXO_CONTRATANTE = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.sexoContratante";
	private static final String SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO_CODE = "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.masculinoCode";
	private static final String SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO_CODE = "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.femeninoCode";
	private static final String SCORING_VEHICULO_CODIGO_COMUNA = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.codigoComuna";
	private static final String SCORING_VEHICULO_EDAD_CONDUCTOR = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadConductor";
	private static final String CODIGO_SCORING_ANTIGUEDAD = "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.antiguedad";
	private static final String CODIGO_SCORING_VERANEO = "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.veraneo";
	private static final String VALOR_SCORING_VERANEO = "cl.cencosud.asesorcotizador.scoring.hogar.valor.veraneo";
	private static final String CODIGO_SCORING_TIPO = "codigo.pregunta.tipo.mascota"; 
	private static final String CODIGO_SCORING_RAZA = "codigo.pregunta.raza.mascota"; 
	private static final String CODIGO_SCORING_SEXO = "codigo.pregunta.sexo.mascota"; 
	private static final String CODIGO_SCORING_EDAD= "codigo.pregunta.edad.mascota"; 

	public int registrarCotizacionHogar(CotizacionSeguroHogar cotHogar,
			Solicitud solicitud) {

		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
		RespuestaScoringVO[] scoring;
		Context context = null;

		int nroBigsa = 0;
		GrabarResponseVO result = null;

		try {
			context = new InitialContext();
			WsBigsaCargaCotizacion svc = (WsBigsaCargaCotizacion) context
					.lookup("java:comp/env/service/WsBigsaCargaCotizacion");
			WsBigsaCargaCotizacionImplDelegate ws = svc
					.getWsBigsaCargaCotizacionImplPort(new URL(dao
							.getString(WSDL_REGISTRAR_COTIZACION)));

			CotizacionHogarVO cotizacion = new CotizacionHogarVO();
			cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));
			cotizacion.setCodigoPlan(new Long(cotHogar.getCodigoPlan())
					.intValue());

			Contratante datos = cotHogar.getContratante();
			TipoPersonaVO contratante = new TipoPersonaVO();
			contratante.setRutPersona(new Long(datos.getRut()).intValue());
			contratante.setDigitoPersona(datos.getDv());
			contratante.setApePatPersona(datos.getApellidoPaterno());
			contratante.setApeMatPersona(datos.getApellidoMaterno());
			contratante.setNombresPersona(datos.getNombre());
			contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
					.format(datos.getFechaDeNacimiento()));

			String estadoCivil = EstadoCivilEnum.valueOf(
					"_" + cotHogar.getEstadoCivil()).getDescripcion();
			contratante.setEstCivilPersona(TipoEstadoCivil
					.fromString(estadoCivil));

			contratante.setSexoPersona(datos.getSexo());
			contratante.setDirCallePersona(datos.getCalleDireccion());
			contratante.setDirNroPersona(datos.getNumeroDireccion());
			contratante.setDirDeptoPersona(datos
					.getNumeroDepartamentoDireccion());
			contratante.setCodigoRegionPersona(Integer.parseInt(datos
					.getIdRegion()));
			contratante.setCodigoComunaPersona(Integer.parseInt(datos
					.getIdComuna()));
			contratante.setCodigoCiudadPersona(Integer.parseInt(datos
					.getIdCiudad()));
			contratante.setFono1Persona(datos.getTelefono1());
			contratante.setEmailPersona(datos.getEmail());
			cotizacion.setContratante(contratante);

			if (cotHogar.isClienteEsAsegurado()) {
				cotizacion.setAsegurado(contratante);
			} else {

				Contratante asegurado = cotHogar.getAsegurado();
				TipoPersonaVO aseguradoLeg = new TipoPersonaVO();

				aseguradoLeg.setRutPersona(new Long(asegurado.getRut())
						.intValue());
				aseguradoLeg.setDigitoPersona(asegurado.getDv());
				aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
				aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
				aseguradoLeg.setNombresPersona(asegurado.getNombre());
				aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
						.format(asegurado.getFechaDeNacimiento()));

				String estadoCivilAseg = EstadoCivilEnum.valueOf(
						"_" + cotHogar.getEstadoCivil()).getDescripcion();
				aseguradoLeg.setEstCivilPersona(TipoEstadoCivil
						.fromString(estadoCivilAseg));

				aseguradoLeg.setSexoPersona(asegurado.getSexo());
				aseguradoLeg.setDirCallePersona(asegurado.getCalleDireccion());
				aseguradoLeg.setDirNroPersona(asegurado.getNumeroDireccion());
				aseguradoLeg.setDirDeptoPersona(asegurado
						.getNumeroDepartamentoDireccion());
				aseguradoLeg.setCodigoRegionPersona(Integer.parseInt(asegurado
						.getIdRegion()));
				aseguradoLeg.setCodigoComunaPersona(Integer.parseInt(asegurado
						.getIdComuna()));
				aseguradoLeg.setCodigoCiudadPersona(Integer.parseInt(asegurado
						.getIdCiudad()));
				aseguradoLeg.setFono1Persona(asegurado.getTelefono1());

				cotizacion.setAsegurado(aseguradoLeg);

			}

			Calendar cal = Calendar.getInstance();
			
			String fechaInicio = cotHogar.getFecIniVig();
			
			if (fechaInicio == null) {
			
			cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
					.format(solicitud.getFecha_creacion()));
			cal.setTime(solicitud.getFecha_creacion());
			cal.add(Calendar.MONTH, 1);
			cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal
					.getTime()));

			} else {
				// INICIO HOGAR VACACIONES
				fechaInicio = fechaInicio.replace("/", "");
				cotizacion.setFecIniVig(fechaInicio);

				SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				Date fechaViaje;
				try {
					fechaViaje = sdf.parse(fechaInicio);
					cal.setTime(fechaViaje);
					if (cotHogar.getTramoDias().equals("DE 1 A 8 DIAS")) {
						cal.add(Calendar.DATE, 7);
						cotizacion
								.setFecTerVig(new SimpleDateFormat("ddMMyyyy")
										.format(cal.getTime()));
					} else if (cotHogar.getTramoDias().equals("DE 9 A 15 DIAS")) {
						cal.add(Calendar.DATE, 14);
						cotizacion
								.setFecTerVig(new SimpleDateFormat("ddMMyyyy")
										.format(cal.getTime()));
					} else if (cotHogar.getTramoDias().equals("DE 16 A 22 DIAS")) {
						cal.add(Calendar.DATE, 21);
						cotizacion
								.setFecTerVig(new SimpleDateFormat("ddMMyyyy")
										.format(cal.getTime()));
					} else if (cotHogar.getTramoDias().equals("DE 23 A 30 DIAS")) {
						cal.add(Calendar.DATE, 29);
						cotizacion
								.setFecTerVig(new SimpleDateFormat("ddMMyyyy")
										.format(cal.getTime()));
					} else if (cotHogar.getTramoDias().equals("DE 31 A 45 DIAS")) {
						cal.add(Calendar.DATE, 44);
						cotizacion
								.setFecTerVig(new SimpleDateFormat("ddMMyyyy")
										.format(cal.getTime()));
					} else if (cotHogar.getTramoDias().equals("DE 46 A 60 DIAS")) {
						cal.add(Calendar.DATE, 59);
						cotizacion
								.setFecTerVig(new SimpleDateFormat("ddMMyyyy")
										.format(cal.getTime()));
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				// FIN  HOGAR VACACIONES
			}
			
			
			cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());

			SeguroHogarVO hogar = new SeguroHogarVO();
			String direccionRiesgo = cotHogar.getDireccion();
			if (cotHogar.getNumeroDireccion() != null
					&& !cotHogar.getNumeroDireccion().equals("")) {
				direccionRiesgo += " " + cotHogar.getNumeroDireccion();
			}
			if (cotHogar.getNumeroDepto() != null
					&& !cotHogar.getNumeroDepto().equals("")) {
				direccionRiesgo += " " + cotHogar.getNumeroDepto();
			}
			hogar.setDireccionRiesgo(direccionRiesgo);
			hogar.setCodigoRegionRiesgo(Integer.valueOf(cotHogar.getRegion()));
			hogar.setCodigoComunaRiesgo(Integer.valueOf(cotHogar.getComuna()));
			hogar.setCodigoCiudadRiesgo(Integer.valueOf(cotHogar.getCiudad()));

			hogar.setMontoAseguradoContenido(new Float(cotHogar
					.getMontoAseguradoContenido()));
			hogar.setMontoAseguradoEdificio(new Float(cotHogar
					.getMontoAseguradoEdificio()));
			hogar.setMontoAseguradoRobo(new Float(cotHogar
					.getMontoAseguradoRobo()));

			cotizacion.setHogar(hogar);

			// Respuestas de scoring
			// SCORING ANTIGUEDAD
			RespuestaScoringVO rptaAntiguedad = new RespuestaScoringVO();
			int codigoAntiguedad = Integer.valueOf(ACVConfig.getInstance()
					.getString(CODIGO_SCORING_ANTIGUEDAD));
			rptaAntiguedad.setCodigoPregunta(codigoAntiguedad);
			rptaAntiguedad.setValorNumeral(Float.valueOf(cotHogar
					.getAntiguedadVivienda()));
			rptaAntiguedad.setValorSN("N");

			// SCORING VERANEO
			RespuestaScoringVO rptaVeraneo = new RespuestaScoringVO();
			int codigoVeraneo = Integer.valueOf(ACVConfig.getInstance()
					.getString(CODIGO_SCORING_VERANEO));
			rptaVeraneo.setCodigoPregunta(codigoVeraneo);
			rptaVeraneo.setValorSN(ACVConfig.getInstance().getString(
					VALOR_SCORING_VERANEO));

			// INICIO CODIGO DE COMERCIO
			// 134 : �Su vivienda se encuentra en una zona rural o urbana?
			RespuestaScoringVO respuestaZona = new RespuestaScoringVO();
			respuestaZona.setCodigoPregunta(134);
			respuestaZona.setValorAlternativa("URBANA");

			// 135 : �Su vivienda est� construida parcial o totalmente de adobe?
			RespuestaScoringVO respuestaConstruccion = new RespuestaScoringVO();
			respuestaConstruccion.setCodigoPregunta(135);
			respuestaConstruccion.setValorAlternativa("NO");

			// 136 : �Su vivienda corresponde a su lugar permanente de
			// residencia?
			RespuestaScoringVO respuestaResidencia = new RespuestaScoringVO();
			respuestaResidencia.setCodigoPregunta(136);
			respuestaResidencia.setValorAlternativa("SI");

			// 137 : �Cuenta con rejas en ventanas o alarma o guardia / Conserje
			// 24 hrs?
			RespuestaScoringVO respuestaSeguridad = new RespuestaScoringVO();
			respuestaSeguridad.setCodigoPregunta(137);
			respuestaSeguridad.setValorAlternativa("SI");
			// FIN CODIGO DE COMERCIO
			
			
			// INICIO Hogar Vacaciones
			RespuestaScoringVO respuestaTramo = new RespuestaScoringVO();
			String tramoDias = cotHogar.getTramoDias();
			respuestaTramo.setCodigoPregunta(151);
			respuestaTramo.setValorAlternativa(tramoDias);
			// FIN Hogar vacaciones
			

			scoring = new RespuestaScoringVO[] {
					rptaAntiguedad, rptaVeraneo, respuestaZona,
					respuestaConstruccion, respuestaResidencia,
					respuestaSeguridad, respuestaTramo };
			
			cotizacion.setScoring(scoring);

			logger.info("Cod. prgunta zona: " + respuestaZona.getCodigoPregunta());
			logger.info("Val. respuesta zona: "
					+ respuestaZona.getValorAlternativa());
			logger.info("Cod. prgunta construccion: "
					+ respuestaConstruccion.getCodigoPregunta());
			logger.info("Val. respuesta construccion: "
					+ respuestaConstruccion.getValorAlternativa());
			logger.info("Cod. prgunta residencia: "
					+ respuestaResidencia.getCodigoPregunta());
			logger.info("Val. respuesta residencia: "
					+ respuestaResidencia.getValorAlternativa());
			logger.info("Cod. prgunta seguridad: "
					+ respuestaSeguridad.getCodigoPregunta());
			logger.info("Val. respuesta seguridad: "
					+ respuestaSeguridad.getValorAlternativa());
			logger.info("Cod. prgunta seguridad: "
					+ respuestaTramo.getCodigoPregunta());
			logger.info("Val. respuesta seguridad: "
					+ respuestaTramo.getValorAlternativa());
			
			logger.info("GRABAR COTIZACION");
			Logging.Logger("COTIZACION", cotizacion, getClass());
			Logging.Logger("CONTRATANTE", cotizacion.getContratante(),
					getClass());
			Logging.Logger("ASEGURADO", cotizacion.getAsegurado(), getClass());
			Logging.Logger("HOGAR", cotizacion.getHogar(), getClass());
			
			result = ws.grabarCotizacionSeguroHogar(
					_grabarCotizacionSeguroHogar_graba.S, cotizacion);

			nroBigsa = result.getNumeroCotizacion();

		} catch (NamingException e) {
			logger.error("Error obteniendo WS de registro.", e);
			throw new SystemException(e);
		} catch (RemoteException e) {
			logger.debug("Error durante invocando WS de valorizacion.", e);
			throw new SystemException(e.detail);
		} catch (ErrorInternoException e) {
			e.printStackTrace();
			logger.error("Error interno invocando WS de registro.", e);
			throw new SystemException(e);
		} catch (ServiceException e) {
			logger.error("Error durante invocando WS de registro.", e);
			throw new SystemException(e);
		} catch (MalformedURLException e) {
			logger.error("URL mal construida.", e);
			throw new SystemException(e);
		} finally {
			ResourceLeakUtil.close(context);
			ResourceLeakUtil.close(dao);
		}

		return nroBigsa;

	}

	public int registrarCotizacionVehiculo(CotizacionSeguroVehiculo vehiculo,
			Solicitud solicitud) {

		logger.debug("Iniciando registro cotizacion vehiculo.");
		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

		Context context = null;
		int resultNroBigsa = 0;

		try {
			logger.debug("Instanciando contexto JNDI.");
			context = new InitialContext();
			logger.debug("Obteniendo referencia al servicio.");
			WsBigsaCargaCotizacion svc = (WsBigsaCargaCotizacion) context
					.lookup("java:comp/env/service/WsBigsaCargaCotizacion");

			AbstractConfigurator config = this.getConfigurator();
			String url = dao.getString(WSDL_REGISTRAR_COTIZACION);

			WsBigsaCargaCotizacionImplDelegate ws = svc
					.getWsBigsaCargaCotizacionImplPort(new URL(url));

			logger.debug("Referencia al servicio obtenida. Preparando...");
			CotizacionVehiculoVO cotizacion = new CotizacionVehiculoVO();

			cotizacion.setUsuario(config.getString(USER_WS));

			String codigoPlan = String.valueOf(vehiculo.getCodigoPlan());
			cotizacion.setCodigoPlan(new Integer(codigoPlan));

			Contratante datos = vehiculo.getContratante();
			TipoPersonaVO contratante = new TipoPersonaVO();
			contratante.setRutPersona(new Long(datos.getRut()).intValue());
			contratante.setDigitoPersona(datos.getDv());
			contratante.setApePatPersona(datos.getApellidoPaterno());
			contratante.setApeMatPersona(datos.getApellidoMaterno());
			contratante.setNombresPersona(datos.getNombre());
			contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
					.format(datos.getFechaDeNacimiento()));

			String estadoCivil = EstadoCivilEnum.valueOf(
					"_" + datos.getEstadoCivil()).getDescripcion();
			contratante.setEstCivilPersona(TipoEstadoCivil
					.fromString(estadoCivil));

			contratante.setSexoPersona(datos.getSexo());
			contratante.setDirCallePersona(datos.getCalleDireccion());
			contratante.setDirNroPersona(datos.getNumeroDireccion());
			contratante.setDirDeptoPersona(datos
					.getNumeroDepartamentoDireccion());
			contratante.setCodigoRegionPersona(Integer.parseInt(datos
					.getIdRegion()));
			contratante.setCodigoComunaPersona(Integer.parseInt(datos
					.getIdComuna()));
			contratante.setCodigoCiudadPersona(Integer.parseInt(datos
					.getIdCiudad()));
			contratante.setFono1Persona(datos.getTelefono1());
			contratante.setEmailPersona(datos.getEmail());

			cotizacion.setContratante(contratante);

			Contratante asegurado = vehiculo.getAsegurado();
			TipoPersonaVO aseguradoLeg = new TipoPersonaVO();

			aseguradoLeg.setRutPersona(new Long(asegurado.getRut()).intValue());
			aseguradoLeg.setDigitoPersona(asegurado.getDv());
			aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
			aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
			aseguradoLeg.setNombresPersona(asegurado.getNombre());
			aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
					.format(asegurado.getFechaDeNacimiento()));

			estadoCivil = EstadoCivilEnum.valueOf(
					"_" + asegurado.getEstadoCivil()).getDescripcion();
			aseguradoLeg.setEstCivilPersona(TipoEstadoCivil
					.fromString(estadoCivil));

			aseguradoLeg.setSexoPersona(asegurado.getSexo());
			aseguradoLeg.setDirCallePersona(asegurado.getCalleDireccion());
			aseguradoLeg.setDirNroPersona(asegurado.getNumeroDireccion());
			aseguradoLeg.setDirDeptoPersona(asegurado
					.getNumeroDepartamentoDireccion());
			aseguradoLeg.setCodigoRegionPersona(Integer.parseInt(asegurado
					.getIdRegion()));
			aseguradoLeg.setCodigoComunaPersona(Integer.parseInt(asegurado
					.getIdComuna()));
			aseguradoLeg.setCodigoCiudadPersona(Integer.parseInt(asegurado
					.getIdCiudad()));
			aseguradoLeg.setFono1Persona(asegurado.getTelefono1());
			aseguradoLeg.setEmailPersona(asegurado.getEmail());
			cotizacion.setAsegurado(aseguradoLeg);

			if (vehiculo.getNumDiaMaxIniVig() != 0) {
				cotizacion.setFecIniVig(vehiculo.getFecIniVig());
				cotizacion.setFecTerVig(vehiculo.getFecTerVig());

			} else {
				cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
						.format(solicitud.getFecha_creacion()));

				Calendar cal = Calendar.getInstance();
				cal.setTime(solicitud.getFecha_creacion());
				cal.add(Calendar.MONTH, 1);
				cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy")
						.format(cal.getTime()));
			}

			cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());

			SeguroVehiculoVO vehiculoLeg = new SeguroVehiculoVO();
			vehiculoLeg.setCodigoMarca(vehiculo.getCodigoMarca());
			vehiculoLeg.setCodigoModelo(vehiculo.getCodigoModelo());
			vehiculoLeg.setCodigoTipoVehiculo(vehiculo.getCodigoTipoVehiculo());
			vehiculoLeg.setAnoVehiculo(vehiculo.getAnyoVehiculo());
			vehiculoLeg.setPatente(vehiculo.getPatente());
			vehiculoLeg.setNumeroMotor(vehiculo.getNumeroMotor());
			vehiculoLeg.setNumeroChassis(vehiculo.getNumeroChasis());
			String sIdColor = String.valueOf(vehiculo.getIdColor());
			vehiculoLeg.setCodigoColor(Integer.valueOf(sIdColor));
			vehiculoLeg.setMontoAsegurado(vehiculo.getMontoAsegurado());

			String usoVehiculo = vehiculo.getEsParticular() == 1 ? "P" : "";
			vehiculoLeg.setUsoVehiculo(usoVehiculo);

			cotizacion.setVehiculo(vehiculoLeg);

			String estadoCivilAsegurado = "";
			int edadAsegurado = -1;
			int edadAseguradoDuenyo = -1;
			String sexoAsegurado = "";
			String idComunaAsegurado = "";

			if (vehiculo.isClienteEsAsegurado()) {
				estadoCivilAsegurado = datos.getEstadoCivil();
				edadAsegurado = calcularEdadContratante(datos
						.getFechaDeNacimiento());
				sexoAsegurado = datos.getSexo();
				idComunaAsegurado = datos.getIdComuna();
			} else {
				estadoCivilAsegurado = asegurado.getEstadoCivil();
				edadAseguradoDuenyo = calcularEdadContratante(asegurado
						.getFechaDeNacimiento());
				sexoAsegurado = asegurado.getSexo();
				idComunaAsegurado = asegurado.getIdComuna();
			}
			Date fechaNacimiento = vehiculo.getFechaNacimiento();

			// PREGUNTAS DE SCORING
			// 42: -> (Valor alternativa) C:CASADO, S:SOLTERO, V:VIUDO, O:OTRO.
			RespuestaScoringVO respuestaEstadoCivil = new RespuestaScoringVO();
			int codigoPregunta = ACVConfig.getInstance().getInt(
					SCORING_VEHICULO_ESTADO_CIVIL);
			respuestaEstadoCivil.setCodigoPregunta(codigoPregunta);
			respuestaEstadoCivil
					.setValorAlternativa(obtenerEstadoCivilCode(estadoCivilAsegurado));

			// 106: -> (Valor alternativa) Fijo O
			RespuestaScoringVO respuestaMedioInformativo = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(
					SCORING_VEHICULO_MEDIO_INFORMATIVO);
			respuestaMedioInformativo.setCodigoPregunta(codigoPregunta);
			String valorMedioInformativo = ACVConfig.getInstance().getString(
					SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO_CODE);
			respuestaMedioInformativo
					.setValorAlternativa(valorMedioInformativo); // FIJO

			// 100: -> (Valor alternativa) 2, 3, 4, 5
			RespuestaScoringVO respuestaNumeroPuertas = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(
					SCORING_VEHICULO_NUMERO_PUERTAS);
			respuestaNumeroPuertas.setCodigoPregunta(codigoPregunta);
			String nroPuertas = ACVConfig.getInstance().getString(
					SCORING_VEHICULO_RESPUESTA_NUMERO_PUERTAS_DE_2_A_3);
			if (vehiculo.getNumeroPuertas() > 3) {
				nroPuertas = String.valueOf(vehiculo.getNumeroPuertas());
			}
			respuestaNumeroPuertas.setValorAlternativa(nroPuertas);

			// 101: -> (Valor numeral) Edad
			RespuestaScoringVO respuestaEdadAsegurado = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(
					SCORING_VEHICULO_EDAD_CONTRATANTE);
			respuestaEdadAsegurado.setCodigoPregunta(codigoPregunta);
			int rangoEdad = -1;
			if (vehiculo.isClienteEsAsegurado()) {
				rangoEdad = edadAsegurado;
			} else {
				rangoEdad = edadAseguradoDuenyo;
			}
			respuestaEdadAsegurado
					.setValorAlternativa(obtenerRangoEdadAsegurado(rangoEdad));

			// 59: -> (Valor alternativa) M: Masculino, F: Femenino
			RespuestaScoringVO respuestaSexoAsegurado = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(
					SCORING_VEHICULO_SEXO_CONTRATANTE);
			respuestaSexoAsegurado.setCodigoPregunta(codigoPregunta);
			String descSexoAsegurado = ACVConfig.getInstance().getString(
					SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO_CODE);
			if (sexoAsegurado.equals("F")) {
				descSexoAsegurado = ACVConfig.getInstance().getString(
						SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO_CODE);
			}
			respuestaSexoAsegurado.setValorAlternativa(descSexoAsegurado);

			// 79: -> (Codigo Comuna) Codigo de comuna
			RespuestaScoringVO respuestaCodigoComuna = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(
					SCORING_VEHICULO_CODIGO_COMUNA);
			respuestaCodigoComuna.setCodigoPregunta(codigoPregunta);
			int codigoComuna = Integer.valueOf(idComunaAsegurado);
			respuestaCodigoComuna.setCodigoComuna(codigoComuna);

			RespuestaScoringVO respuestaEdad = null;
			// 54: -> (Valor numeral) Edad conductores del vehiculo
			if (vehiculo.isClienteEsAsegurado() == false) {
				respuestaEdad = new RespuestaScoringVO();
				codigoPregunta = ACVConfig.getInstance().getInt(
						SCORING_VEHICULO_EDAD_CONDUCTOR);
				respuestaEdad.setCodigoPregunta(codigoPregunta);
				respuestaEdad.setValorNumeral(Float
						.valueOf(edadAseguradoDuenyo));
			}

			else {
				respuestaEdad = new RespuestaScoringVO();
				codigoPregunta = ACVConfig.getInstance().getInt(
						SCORING_VEHICULO_EDAD_CONDUCTOR);
				respuestaEdad.setCodigoPregunta(codigoPregunta);
				respuestaEdad.setValorNumeral(Float
						.valueOf(calcularEdadContratante(fechaNacimiento)));
			}

			// INICIO CODIGO DE COMERCIO
			// 130 : �El veh�culo es de uso particular?
			RespuestaScoringVO respuestaVehiculoParicular = new RespuestaScoringVO();
			respuestaVehiculoParicular.setCodigoPregunta(130);
			respuestaVehiculoParicular.setValorAlternativa("SI");

			// 131 : �El veh�culo fue internado a Chile con franquicia aduanera?
			RespuestaScoringVO respuestaFranquicia = new RespuestaScoringVO();
			respuestaFranquicia.setCodigoPregunta(131);
			respuestaFranquicia.setValorAlternativa("NO");

			// 132 : �El veh�culo presenta da�os que impiden el normal
			// funcionamiento de �ste?
			RespuestaScoringVO respuestaFuncionamiento = new RespuestaScoringVO();
			respuestaFuncionamiento.setCodigoPregunta(132);
			respuestaFuncionamiento.setValorAlternativa("NO");

			// 133 : �El veh�culo presenta da�os en chapas, cerraduras u otra
			// pieza que afecte su seguridad?
			RespuestaScoringVO respuestaSeguridad = new RespuestaScoringVO();
			respuestaSeguridad.setCodigoPregunta(133);
			respuestaSeguridad.setValorAlternativa("NO");
			// FIN CODIGO DE COMERCIO
			
			//Inicio Regalo Full Servicio - IG
			RespuestaScoringVO respuestaRegalo = new RespuestaScoringVO();
			respuestaRegalo.setCodigoPregunta(152);
			respuestaRegalo.setValorAlternativa("NO APLICA");
			//Fin Regalo Full Servicio - IG

			RespuestaScoringVO[] scoring;
			if(vehiculoLeg.getCodigoTipoVehiculo() == 6){
				logger.info("SCORING MOTO");
				scoring = new RespuestaScoringVO[] {
						respuestaEstadoCivil, respuestaMedioInformativo,
						respuestaEdadAsegurado, respuestaSexoAsegurado, 
						respuestaCodigoComuna, respuestaEdad };
			}else{
				logger.info("SCORING AUTO");
				//Inicio Regalo Full Servicio - IG
				if(vehiculo.getNumSubcategoria().equals("22")){
					logger.info("SCORING FULL SERVICIO");
					scoring = new RespuestaScoringVO[] {
							respuestaEstadoCivil, respuestaMedioInformativo,
							respuestaNumeroPuertas, respuestaEdadAsegurado,
							respuestaSexoAsegurado, respuestaCodigoComuna,
							respuestaEdad, respuestaVehiculoParicular,
							respuestaFranquicia, respuestaFuncionamiento,
							respuestaSeguridad,  respuestaRegalo};
				}else{
					scoring = new RespuestaScoringVO[] {
							respuestaEstadoCivil, respuestaMedioInformativo,
							respuestaNumeroPuertas, respuestaEdadAsegurado,
							respuestaSexoAsegurado, respuestaCodigoComuna,
							respuestaEdad, respuestaVehiculoParicular,
							respuestaFranquicia, respuestaFuncionamiento,
							respuestaSeguridad };
				}
				//Fin Regalo Full Servicio - IG
			}

			cotizacion.setScoring(scoring);
			
			// LOG datos Entrada
			logger.info("REGISTRAR COTIZACION VEHICULO");
			logger.info("Datos de Entrada");
			logger.info("----------------");
			logger.info("Codigo Plan: " + codigoPlan);
			logger.info("Marca Vehiculo: " + vehiculoLeg.getCodigoMarca());
			logger.info("Tipo Vehiculo: " + vehiculoLeg.getCodigoTipoVehiculo());
			logger.info("Modelo Vehiculo: " + vehiculoLeg.getCodigoModelo());
			logger.info("Anio Vehiculo: " + vehiculoLeg.getAnoVehiculo());
			logger.info("Monto Asegurado: " + vehiculoLeg.getMontoAsegurado());
			logger.info("Rut: " + cotizacion.getAsegurado().getRutPersona());
			logger.info("****************");
			logger.info("SCORING:");
			logger.info("cod_preg estadoCivil: "
					+ respuestaEstadoCivil.getCodigoPregunta());
			logger.info("valor estadoCivil: "
					+ respuestaEstadoCivil.getValorAlternativa());
			logger.info("cod_preg medioInformativo: "
					+ respuestaMedioInformativo.getCodigoPregunta());
			logger.info("valor medioInformativo: "
					+ respuestaMedioInformativo.getValorAlternativa());
			logger.info("cod_preg rangoEdad: "
					+ respuestaEdadAsegurado.getCodigoPregunta());
			logger.info("valor rangoEdad: "
					+ respuestaEdadAsegurado.getValorAlternativa());
			logger.info("cod_preg sexoAsegurado: "
					+ respuestaSexoAsegurado.getCodigoPregunta());
			logger.info("valor sexoAsegurado: "
					+ respuestaSexoAsegurado.getValorAlternativa());
			logger.info("cod_preg comuna: "
					+ respuestaCodigoComuna.getCodigoPregunta());
			logger.info("valor comuna: "
					+ respuestaCodigoComuna.getCodigoComuna());
			logger.info("cod_preg Edad: " + respuestaEdad.getCodigoPregunta());
			logger.info("valor Edad: " + respuestaEdad.getValorNumeral());
			
			if(vehiculoLeg.getCodigoTipoVehiculo() != 6){
				logger.info("cod_preg numeroPuertas: "
						+ respuestaNumeroPuertas.getCodigoPregunta());
				logger.info("valor numeroPuertas: "
						+ respuestaNumeroPuertas.getValorAlternativa());
				logger.info("cod_preg 1: "
						+ respuestaVehiculoParicular.getCodigoPregunta());
				logger.info("valor 1: "
						+ respuestaVehiculoParicular.getValorAlternativa());
				logger.info("cod_preg 2: "
						+ respuestaFranquicia.getCodigoPregunta());
				logger.info("valor 2: " + respuestaFranquicia.getValorAlternativa());
				logger.info("cod_preg 3: "
						+ respuestaFuncionamiento.getCodigoPregunta());
				logger.info("valor 3: "
						+ respuestaFuncionamiento.getValorAlternativa());
				logger.info("cod_preg 4: " + respuestaSeguridad.getCodigoPregunta());
				logger.info("valor 4: " 
						+ respuestaSeguridad.getValorAlternativa());
				logger.info("cod_preg promo: " + respuestaRegalo.getCodigoPregunta());
				logger.info("valor promo: " + respuestaRegalo.getValorAlternativa());
			}

			logger.info("----------------");
			
			// cotizacion.setTipoInspeccion(Integer.parseInt(vehiculo.getTipoInspeccion()));
			// logger.info("RegistrarCotizacionDAOWSWAS: Tipo de Inspeccion: "+cotizacion.getTipoInspeccion()+"");
			try {
				logger.info("COTIZACION");
				logger.info(BeanUtils.describe(cotizacion));
				logger.info("ASEGURADO");
				logger.info(BeanUtils.describe(cotizacion.getAsegurado()));
				logger.info("CONTRATANTE");
				logger.info(BeanUtils.describe(cotizacion.getContratante()));
				logger.info("VEHICULO");
				logger.info(BeanUtils.describe(cotizacion.getVehiculo()));
				// logger.info("SCORING");
				// for(int k=0; k<=cotizacion.getScoring().length; k++) {
				// logger.info(BeanUtils.describe(cotizacion.getScoring()[k]));
				// }
				logger.info("****************");
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			logger.info("Invocando al servicio de registro...");
			GrabarResponseVO result = ws.grabarCotizacionSeguroVehiculo(
					_grabarCotizacionSeguroVehiculo_graba.S, cotizacion);

			logger.info("Respuesta WS  : " + result.getRespuesta());
			logger.info("Num cotizacion: " + result.getNumeroCotizacion());

			resultNroBigsa = result.getNumeroCotizacion();
		} catch (NamingException ne) {
			logger.error("Error obteniendo referencia al servicio.", ne);
			throw new SystemException(ne);
		} catch (ServiceException se) {
			logger.error("Error invocando al WS Bigsa de registro.", se);
			throw new SystemException(se);
		} catch (MalformedURLException mue) {
			logger.error("URL de WS mal configurada.", mue);
			throw new SystemException(mue);
		} catch (RemoteException re) {
			logger.error("Error invocando al WS de registro.", re);
			throw new SystemException(re);
		} catch (ErrorInternoException eie) {
			logger.error("Error interno invocando WS de registro.", eie);
			throw new SystemException(eie);
		} finally {
			ResourceLeakUtil.close(context);
			ResourceLeakUtil.close(dao);
		}
		return resultNroBigsa;
	}

	/**
	 * TODO Describir m�todo obtenerRangoEdadAsegurado.
	 * 
	 * @param edadAsegurado
	 * @return
	 */
	private String obtenerRangoEdadAsegurado(int edadAsegurado) {
		String rangoEdadAsegurado = "";
		if (edadAsegurado >= 18 && edadAsegurado < 23) {
			rangoEdadAsegurado = "Mayores a 18";
		} else if (edadAsegurado >= 23 && edadAsegurado < 28) {
			rangoEdadAsegurado = "Mayores a 23";
		} else if (edadAsegurado >= 28) {
			rangoEdadAsegurado = "Mayores a 28";
		}
		return rangoEdadAsegurado;
	}

	/**
	 * TODO Describir m�todo calcularEdad.
	 * 
	 * @param cotizacionVehiculo
	 * @return
	 */
	private int calcularEdadContratante(Date fechaNacimiento) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fechaNacimiento);
		GregorianCalendar now = new GregorianCalendar();
		int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
				|| (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal
						.get(Calendar.DAY_OF_MONTH) > now
						.get(Calendar.DAY_OF_MONTH))) {
			res--;
		}
		return res;
	}

	/**
	 * TODO Describir m�todo obtenerEstadoCivilCode.
	 * 
	 * @param cotizacionVehiculo
	 * @return
	 */
	private String obtenerEstadoCivilCode(String sEstadoCivil) {
		String estadoCivil = "O";
		if (sEstadoCivil.equals("1")) {
			estadoCivil = "S";
		} else if (sEstadoCivil.equals("2")) {
			estadoCivil = "C";
		} else if (sEstadoCivil.equals("3")) {
			estadoCivil = "V";
		}
		return estadoCivil;
	}

	public int registrarCotizacionVida(CotizacionSeguroVida cotVida,
			Solicitud solicitud) {

		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
		Context context = null;
		int nroBigsa = 0;
		RespuestaScoringVO[] oRespuestas;
		
		logger.info("Iniciando registro de cotizacion vida");
		try {
			context = new InitialContext();
			AbstractConfigurator config = this.getConfigurator();

			logger.debug("Obteniendo referencia al servicio.");
			WsBigsaCargaCotizacion svc = (WsBigsaCargaCotizacion) context
					.lookup("java:comp/env/service/WsBigsaCargaCotizacion");

			String url = dao.getString(WSDL_REGISTRAR_COTIZACION);
			WsBigsaCargaCotizacionImplDelegate ws = svc
					.getWsBigsaCargaCotizacionImplPort(new URL(url));

			//Datos Plan
			CotizacionVidaVO cotizacion = new CotizacionVidaVO();
			cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));
			cotizacion.setCodigoPlan(new Long(cotVida.getCodigoPlan()).intValue());

			//Datos Contratante
			Contratante datos = cotVida.getContratante();
			TipoPersonaVO contratante = new TipoPersonaVO();
			contratante.setRutPersona(new Long(datos.getRut()).intValue());
			contratante.setDigitoPersona(datos.getDv());
			contratante.setApePatPersona(datos.getApellidoPaterno());
			contratante.setApeMatPersona(datos.getApellidoMaterno());
			contratante.setNombresPersona(datos.getNombre());
			contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(datos.getFechaDeNacimiento()));
			String estadoCivil = EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil()).getDescripcion();
			contratante.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivil));
			contratante.setSexoPersona(datos.getSexo());
			contratante.setDirCallePersona(datos.getCalleDireccion());
			contratante.setDirNroPersona(datos.getNumeroDireccion());
			contratante.setDirDeptoPersona(datos.getNumeroDepartamentoDireccion());
			contratante.setCodigoRegionPersona(Integer.parseInt(datos.getIdRegion()));
			contratante.setCodigoComunaPersona(Integer.parseInt(datos.getIdComuna()));
			contratante.setCodigoCiudadPersona(Integer.parseInt(datos.getIdCiudad()));
			contratante.setFono1Persona(datos.getTelefono1());
			contratante.setFono2Persona(datos.getTelefono2());
			contratante.setEmailPersona(datos.getEmail());
			// contratante.setCodigoProfesion(new Integer(cotVida.getActividad()));
			cotizacion.setContratante(contratante);

			//INICIO CONTRATACION TERCEROS
			Contratante registro = cotVida.getAsegurado();
			TipoPersonaVO asegurado = new TipoPersonaVO();

			if (cotVida.isClienteEsAsegurado()) {
				cotizacion.setAsegurado(contratante);
			} else {
				//Datos Asegurado
				asegurado.setRutPersona(new Long(registro.getRut()).intValue());
				asegurado.setDigitoPersona(registro.getDv());
				asegurado.setApePatPersona(registro.getApellidoPaterno());
				asegurado.setApeMatPersona(registro.getApellidoMaterno());
				asegurado.setNombresPersona(registro.getNombre());
				asegurado.setFecNacPersona(new SimpleDateFormat("ddMMyyyy").format(registro.getFechaDeNacimiento()));
				String estadoCivilAsegurado = EstadoCivilEnum.valueOf("_" + registro.getEstadoCivil()).getDescripcion();
				asegurado.setEstCivilPersona(TipoEstadoCivil.fromString(estadoCivilAsegurado));
				asegurado.setSexoPersona(registro.getSexo());
				asegurado.setDirCallePersona(registro.getCalleDireccion());
				asegurado.setDirNroPersona(registro.getNumeroDireccion());
				asegurado.setDirDeptoPersona(registro.getNumeroDepartamentoDireccion());
				asegurado.setCodigoRegionPersona(Integer.parseInt(registro.getIdRegion()));
				asegurado.setCodigoComunaPersona(Integer.parseInt(registro.getIdComuna()));
				asegurado.setCodigoCiudadPersona(Integer.parseInt(registro.getIdCiudad()));
				asegurado.setFono1Persona(registro.getTelefono1());
				asegurado.setFono2Persona(registro.getTelefono2());
				cotizacion.setAsegurado(asegurado);
			}
			// FIN CONTRATACION TERCEROS

			Calendar cal = Calendar.getInstance();
			String fechaInicio = cotVida.getFecIniVig();

			if (fechaInicio == null) {
				cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy").format(solicitud.getFecha_creacion()));
				cal.setTime(solicitud.getFecha_creacion());
				cal.add(Calendar.MONTH, 1);
				cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
			} else {
				// INICIO ASISTENCIA EN VIAJE
				fechaInicio = fechaInicio.replace("/", "");
				cotizacion.setFecIniVig(fechaInicio);

				SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				Date fechaViaje;
				try {
					fechaViaje = sdf.parse(fechaInicio);
					cal.setTime(fechaViaje);
					if (cotVida.getTramoDias().equals("DE 1 A 8 DIAS")) {
						cal.add(Calendar.DATE, 7);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("DE 9 A 15 DIAS")) {
						cal.add(Calendar.DATE, 14);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("DE 16 A 22 DIAS")) {
						cal.add(Calendar.DATE, 21);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("DE 23 A 30 DIAS")) {
						cal.add(Calendar.DATE, 29);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("DE 31 A 45 DIAS")) {
						cal.add(Calendar.DATE, 44);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("46 A 60 DIAS")) {
						cal.add(Calendar.DATE, 59);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("61 A 90 DIAS")) {
						cal.add(Calendar.DATE, 89);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("91 A 120 DIAS")) {
						cal.add(Calendar.DATE, 119);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("121 A 180 DIAS")) {
						cal.add(Calendar.DATE, 179);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					} else if (cotVida.getTramoDias().equals("181 A 365 DIAS")) {
						cal.add(Calendar.DATE, 364);
						cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal.getTime()));
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				// FIN ASISTENCIA EN VIAJE
			}

			cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());
			String numSubcategoria = cotVida.getNumSubcategoria() == null ? "": cotVida.getNumSubcategoria();
			
			SeguroVidaVO vida = new SeguroVidaVO();
			DatosAseguradoVO oAseguradoVida = new DatosAseguradoVO();
			DatosAseguradoVO[] aseguradosVida = null;
			DatosAseguradoVO[] aseguradoVida = null;
			int ibenef = 0;
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						
			// INICIO CONTRATACION TERCEROS
			if (cotVida.isClienteEsAsegurado()) {
				oAseguradoVida.setApeMaternoAseg(contratante.getApeMatPersona());
				oAseguradoVida.setApePaternoAseg(contratante.getApePatPersona());
				oAseguradoVida.setDigitoAseg(contratante.getDigitoPersona());
				oAseguradoVida.setFecNacAseg(contratante.getFecNacPersona());
				oAseguradoVida.setNombresAseg(contratante.getNombresPersona());
				oAseguradoVida.setParentAseg(ParentAseg.value1);
				oAseguradoVida.setRutAseg(contratante.getRutPersona());
				oAseguradoVida.setSexoAseg(contratante.getSexoPersona());
			} else {
				Contratante contrataAseguradoVida = cotVida.getAsegurado();
				oAseguradoVida.setApeMaternoAseg(contrataAseguradoVida.getApellidoMaterno());
				oAseguradoVida.setApePaternoAseg(contrataAseguradoVida.getApellidoPaterno());
				oAseguradoVida.setDigitoAseg(contrataAseguradoVida.getDv());
				oAseguradoVida.setFecNacAseg(new SimpleDateFormat("ddMMyyyy").format(contrataAseguradoVida.getFechaDeNacimiento()));
				oAseguradoVida.setNombresAseg(contrataAseguradoVida.getNombre());
				oAseguradoVida.setParentAseg(ParentAseg.value1);
				oAseguradoVida.setRutAseg(new Long(contrataAseguradoVida.getRut()).intValue());
				oAseguradoVida.setSexoAseg(contrataAseguradoVida.getSexo());
			}
			// FIN CONTRATACION TERCEROS
			
			//Datos Beneficiarios
			Beneficiario[] beneficiarios = cotVida.getBeneficiarios();
			
			if(!numSubcategoria.equals("221")){
				DatosBenficiarioVO[] beneficiariosAux = null;
				if (beneficiarios != null) {
					beneficiariosAux = new DatosBenficiarioVO[beneficiarios.length];
					int i = 0;
					for (Beneficiario beneficiario : beneficiarios) {
						beneficiariosAux[i] = new DatosBenficiarioVO();
						beneficiariosAux[i].setApeMaternoBene(beneficiario.getApellidoMaterno());
						beneficiariosAux[i].setApePaternoBene(beneficiario.getApellidoPaterno());
						beneficiariosAux[i].setDigitoBene(String.valueOf(beneficiario.getDv()));
						beneficiariosAux[i].setFecNacBene(sdf.format(beneficiario.getFechaNacimiento()));
						beneficiariosAux[i].setNombresBene(beneficiario.getNombre());
						beneficiariosAux[i].setPorcBene(beneficiario.getDistribucion());
						beneficiariosAux[i].setRutBene(new Long(beneficiario.getRut()).intValue());
						beneficiariosAux[i].setSexoBene(beneficiario.getSexo());
						
						String descParentesco = "";
						String parentesco = beneficiario.getParentesco();
						if (parentesco.equalsIgnoreCase("H")) {
							descParentesco = ParentAseg._value5;
						} else if (parentesco.equalsIgnoreCase("P")) {
							descParentesco = ParentAseg._value3;
						} else if (parentesco.equalsIgnoreCase("M")) {
							descParentesco = ParentAseg._value4;
						} else if (parentesco.equalsIgnoreCase("C")) {
							descParentesco = ParentAseg._value2;
						} else if (parentesco.equalsIgnoreCase("O")) {
							descParentesco = ParentAseg._value6;
						} else if (parentesco.equalsIgnoreCase("E")) {
							descParentesco = "�l Mismo";
						}
						beneficiariosAux[i].setParentBene(descParentesco);

						i++;
					}
					oAseguradoVida.setBeneficiarioVida(beneficiariosAux);
				}
			}else{
				//VIAJE GRUPAL
				logger.info("Viaje Grupal");
				if (beneficiarios != null) {
					aseguradosVida = new DatosAseguradoVO[beneficiarios.length+1];
					aseguradosVida[ibenef] = new DatosAseguradoVO();
					aseguradosVida[ibenef] = oAseguradoVida;
					ibenef = ibenef+1;
					
					for (Beneficiario beneficiario : beneficiarios) {
						aseguradosVida[ibenef] = new DatosAseguradoVO();
						aseguradosVida[ibenef].setApeMaternoAseg(beneficiario.getApellidoMaterno());
						aseguradosVida[ibenef].setApePaternoAseg(beneficiario.getApellidoPaterno());
						aseguradosVida[ibenef].setDigitoAseg(String.valueOf(beneficiario.getDv()));
						aseguradosVida[ibenef].setFecNacAseg(sdf.format(beneficiario.getFechaNacimiento()));
						aseguradosVida[ibenef].setNombresAseg(beneficiario.getNombre());
						aseguradosVida[ibenef].setRutAseg(new Long(beneficiario.getRut()).intValue());
						aseguradosVida[ibenef].setSexoAseg(beneficiario.getSexo());
						
						ParentAseg descParentesco = ParentAseg.value6;
						String parentesco = beneficiario.getParentesco();
						
						if (parentesco.equalsIgnoreCase("E")) {
							descParentesco = ParentAseg.value1;
						} else if (parentesco.equalsIgnoreCase("C")) {
							descParentesco = ParentAseg.value2;
						} else if (parentesco.equalsIgnoreCase("P")) {
							descParentesco = ParentAseg.value3;
						} else if (parentesco.equalsIgnoreCase("M")) {
							descParentesco = ParentAseg.value4;
						}else if (parentesco.equalsIgnoreCase("H")) {
							descParentesco = ParentAseg.value5;
						} else if (parentesco.equalsIgnoreCase("O")) {
							descParentesco = ParentAseg.value6;
						}
						aseguradosVida[ibenef].setParentAseg(descParentesco);
						
						ibenef++;
					}
					vida.setAseguradoVida(aseguradosVida);
				}else{
					int asegurados = 0;
					aseguradoVida = new DatosAseguradoVO[asegurados + 1];
					aseguradoVida[0] = oAseguradoVida;
					vida.setAseguradoVida(aseguradoVida);
				}
			}
						
			if(!numSubcategoria.equals("221")){
				int asegurados = 0;
				aseguradoVida = new DatosAseguradoVO[asegurados + 1];
				aseguradoVida[0] = oAseguradoVida;
				vida.setAseguradoVida(aseguradoVida);
			}
			
			vida.setValorBaseCalculo(cotVida.getBaseCalculo());
			
			RespuestaScoringVO respuesta = new RespuestaScoringVO();
			int codigoPregunta = config
					.getInt("cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil");
			respuesta.setCodigoPregunta(codigoPregunta);
			String estadoCivilScoring = "";
			estadoCivilScoring = EstadoCivilEnum.valueOf(
					"_" + cotVida.getEstadoCivil()).getDescripcion();
			respuesta.setValorAlternativa(estadoCivilScoring);

			// INICIO ASISTENCIA EN VIAJE
			RespuestaScoringVO respuestaTramo = new RespuestaScoringVO();
			String tramoDias = cotVida.getTramoDias();
			
			if(tramoDias != null && !("").equals(tramoDias)){
				if(("DE 1 A 8 DIAS").equals(tramoDias.trim()) || ("DE 9 A 15 DIAS").equals(tramoDias.trim()) 
						|| ("DE 16 A 22 DIAS").equals(tramoDias.trim()) || ("DE 23 A 30 DIAS").equals(tramoDias.trim())
						|| ("DE 31 A 45 DIAS").equals(tramoDias.trim())){
					respuestaTramo.setCodigoPregunta(120);
					respuestaTramo.setValorAlternativa(tramoDias);
				} else {
					respuestaTramo.setCodigoPregunta(150);
					respuestaTramo.setValorAlternativa(tramoDias);
				}
			} 
			// FIN ASISTENCIA EN VIAJE
			
			// INICIO MASCOTA
			RespuestaScoringVO respuestaTipo = new RespuestaScoringVO();
			RespuestaScoringVO respuestaRaza = new RespuestaScoringVO();
			RespuestaScoringVO respuestaSexo = new RespuestaScoringVO();
			RespuestaScoringVO respuestaEdad = new RespuestaScoringVO();
						
			if(numSubcategoria.equals("140")){
				logger.info("Scoring Mascota");
				
				//TIPO MASCOTA
				int codigoTipo = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_TIPO));
				respuestaTipo.setCodigoPregunta(codigoTipo);
				respuestaTipo.setValorAlternativa(cotVida.getTipoMascota());
							
				//RAZA MASCOTA
				int codigoRaza = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_RAZA));
				respuestaRaza.setCodigoPregunta(codigoRaza);
				respuestaRaza.setTexto(cotVida.getRazaMascota());
				
				//SEXO MASCOTA
				int codigoSexo = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_SEXO));
				respuestaSexo.setCodigoPregunta(codigoSexo);
				respuestaSexo.setTexto(cotVida.getSexoMascota());
				
				//EDAD MASCOTA
				int codigoEdad = Integer.valueOf(ACVConfig.getInstance()
						.getString(CODIGO_SCORING_EDAD));
				respuestaEdad.setCodigoPregunta(codigoEdad);
				respuestaEdad.setValorNumeral(Float.parseFloat(cotVida.getEdadMascota()));		
				
				oRespuestas = new RespuestaScoringVO[] {
						respuestaTipo, respuestaRaza, 
						respuestaSexo, respuestaEdad };
			}else{
				
				//INICIO ASISTENCIA VIAJE GRUPAL
				RespuestaScoringVO  respuestaViajeGrupal = new RespuestaScoringVO();				
				respuestaViajeGrupal.setCodigoPregunta(148);
				respuestaViajeGrupal.setValorAlternativa(cotVida.getIntegrante());
				//FIN ASISTENCIA EN VIAJE GRUPAL
				
				oRespuestas = new RespuestaScoringVO[] {
						respuesta, respuestaTramo, respuestaViajeGrupal };
			}
			// FIN MASCOTA		
			
			cotizacion.setScoring(oRespuestas);
			cotizacion.setVida(vida);
			
			try {
				Logging.Logger("COTIZACION", cotizacion, getClass());

				Logging.Logger("VIDA", cotizacion.getVida(), getClass());

				for (int j = 0; j < cotizacion.getVida().getAseguradoVida().length; j++) {
					Logging.Logger("ASEGURADOVIDA", cotizacion.getVida().getAseguradoVida()[j], getClass());

					if (cotizacion.getVida().getAseguradoVida()[j].getBeneficiarioVida() != null) {
						for (int k = 0; k < cotizacion.getVida().getAseguradoVida()[j].getBeneficiarioVida().length; k++) {
							Logging.Logger("BENEFICIARIOVIDA", cotizacion.getVida().getAseguradoVida()[j].getBeneficiarioVida()[k], getClass());
						}
					}
				}

				Logging.Logger("ASEGURADO", cotizacion.getAsegurado(), getClass());
				Logging.Logger("CONTRATANTE", cotizacion.getContratante(), getClass());

				for (int j = 0; j < cotizacion.getScoring().length; j++) {
					Logging.Logger("SCORING", cotizacion.getScoring()[j], getClass());
				}
			} catch (Exception e) {
				logger.error("Exception: ", e);
			}

			GrabarResponseVO result = ws.grabarCotizacionSeguroVida(
					_grabarCotizacionSeguroVida_graba.S, cotizacion);

			nroBigsa = result.getNumeroCotizacion();

		} catch (NamingException ne) {
			logger.error("Error obteniendo referencia al servicio.", ne);
			throw new SystemException(ne);
		} catch (ServiceException se) {
			logger.error("Error invocando al WS Bigsa de registro.", se);
			throw new SystemException(se);
		} catch (MalformedURLException mue) {
			logger.error("URL de WS mal configurada.", mue);
			throw new SystemException(mue);
		} catch (RemoteException re) {
			logger.error("Error invocando al WS de registro.", re);
			throw new SystemException(re);
		} catch (ErrorInternoException eie) {
			logger.error("Error interno en WS de registro.", eie);
			throw new SystemException(eie);
		} finally {
			ResourceLeakUtil.close(context);
			ResourceLeakUtil.close(dao);
		}
		return nroBigsa;
	}

	public void close() {
	}

	public Class<?> getDAOInterface() {
		return RegistrarCotizacionDAO.class;
	}

	@SuppressWarnings("unchecked")
	public void setDAOInterface(Class interfazDAO) {
	}
}
