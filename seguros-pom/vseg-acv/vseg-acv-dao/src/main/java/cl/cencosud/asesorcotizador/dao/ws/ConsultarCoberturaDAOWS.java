package cl.cencosud.asesorcotizador.dao.ws;

import cl.cencosud.asesorcotizador.dao.ConsultarCoberturasDAO;

public interface ConsultarCoberturaDAOWS extends ConsultarCoberturasDAO{

	static final String WSDL_CONSULTA_COBERTURAS =  "cl.cencosud.asesorcotizador.dao.ws.ConsultarCoberturasDAOWS.wsdl.consultar.coberturas";

}
