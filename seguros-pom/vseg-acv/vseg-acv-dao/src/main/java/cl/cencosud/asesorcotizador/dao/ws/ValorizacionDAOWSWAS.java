package cl.cencosud.asesorcotizador.dao.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.acv.common.valorizacion.ValorizacionSeguro;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.ValorizacionDAO;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.AseguradoVO;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.ErrorInternoException;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.PrimaHogarVO;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.PrimaVehiculoVO;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.PrimaVidaVO;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.RespuestaScoringVO;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaHogar;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaHogarImplDelegate;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaVehiculo;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaVehiculoImplDelegate;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaVida;
import cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaVidaDelegate;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.bigsa.ws.gestion.grabar.cotizacion.impl.ParentAseg;
/*import cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.ParentAseg;*/

public class ValorizacionDAOWSWAS extends BaseConfigurable implements
		ValorizacionDAO {

	private static final String CODIGO_SCORING_HOGAR = "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.comuna";

	private static final String CODIGO_SCORING_ANTIGUEDAD = "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.antiguedad";

	private static final String CODIGO_SCORING_VERANEO = "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.veraneo";

	private static final String VALOR_SCORING_VERANEO = "cl.cencosud.asesorcotizador.scoring.hogar.valor.veraneo";

	private static final String CODIGO_SCORING_VIDA = "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil";

	private static final String SCR_ESTADO_CIVIL_OTRO = "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil.otro";

	private static final String SCR_ESTADO_CIVIL_CASADO = "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil.casado";

	private static final String SCR_ESTADO_CIVIL_SOLTERO = "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil.soltero";

	private static final String SCR_ESTADO_CIVIL_VIUDO = "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil.viudo";

	private static final String SCORING_VEHICULO_ESTADO_CIVIL = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.estadoCivil";

	private static final String SCORING_VEHICULO_MEDIO_INFORMATIVO = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.medioInformativo";

	private static final String SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO = "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.medioInformativo";

	private static final String SCORING_VEHICULO_NUMERO_PUERTAS = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.nroPuertas";

	private static final String SCORING_VEHICULO_EDAD_CONTRATANTE = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadContratante";

	private static final String SCORING_VEHICULO_SEXO_CONTRATANTE = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.sexoContratante";

	private static final String SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO = "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.masculino";

	private static final String SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO = "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.femenino";

	private static final String SCORING_VEHICULO_CODIGO_COMUNA = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.codigoComuna";

	private static final String SCORING_VEHICULO_EDAD_CONDUCTOR = "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadConductor";

	private static final String WSDL_VALORIZAR_PLAN_HOGAR = "cl.cencosud.asesorcotizador.dao.ValorizacionDAOWS.wsdl.valorizar.hogar";

	private static final String WSDL_VALORIZAR_PLAN_VEHICULO = "cl.cencosud.asesorcotizador.dao.ValorizacionDAOWS.wsdl.valorizar.vehiculo";

	private static final String WSDL_VALORIZAR_PLAN_VIDA = "cl.cencosud.asesorcotizador.dao.ValorizacionDAOWS.wsdl.valorizar.vida";

	private static Log logger = LogFactory.getLog(ValorizacionDAOWSWAS.class);

	/**
	 * @throws ValorizacionException
	 * @see ValorizacionDAO#valorizarPlanHogar(CotizacionSeguroHogar)
	 */
	public ValorizacionSeguro valorizarPlanHogar(
			CotizacionSeguroHogar cotizacionHogar) throws ValorizacionException {
		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

		Context context = null;
		PrimaHogarVO result = null;
		ValorizacionSeguro valorizacion = new ValorizacionSeguro();
		try {
			context = new InitialContext();
			WsBigsaCalculoPrimaHogar svc = (WsBigsaCalculoPrimaHogar) context
					.lookup("java:comp/env/service/WsBigsaCalculoPrimaHogar");

			WsBigsaCalculoPrimaHogarImplDelegate ws = svc
					.getWsBigsaCalculoPrimaHogarImplPort(new URL(dao
							.getString(WSDL_VALORIZAR_PLAN_HOGAR)));

			int codigoPlan = (int) cotizacionHogar.getCodigoPlan();
			int rutCliente = (int) cotizacionHogar.getRutCliente();
			float montoAseguradoEdificio = cotizacionHogar
					.getMontoAseguradoEdificio();
			float montoAseguradoContenido = cotizacionHogar
					.getMontoAseguradoContenido();
			float montoAseguradoRobo = cotizacionHogar.getMontoAseguradoRobo();

			// Pregunta Scoring COMUNA
			RespuestaScoringVO rptaComuna = new RespuestaScoringVO();
			int codigoPregunta = Integer.valueOf(ACVConfig.getInstance()
					.getString(CODIGO_SCORING_HOGAR));
			rptaComuna.setCodigoPregunta(codigoPregunta);
			rptaComuna.setValorNumeral(Float.valueOf(cotizacionHogar
					.getComuna()));
			rptaComuna.setValorSN(cotizacionHogar.getComuna());

			// Pregunta scoring ANTIGUEDAD
			RespuestaScoringVO rptaAntiguedad = new RespuestaScoringVO();
			int codigoAntiguedad = Integer.valueOf(ACVConfig.getInstance()
					.getString(CODIGO_SCORING_ANTIGUEDAD));
			rptaAntiguedad.setCodigoPregunta(codigoAntiguedad);
			rptaAntiguedad.setValorNumeral(Float.valueOf(cotizacionHogar
					.getAntiguedadVivienda()));

			// Pregunta scoring VERANEO
			RespuestaScoringVO rptaVeraneo = new RespuestaScoringVO();
			int codigoVeraneo = Integer.valueOf(ACVConfig.getInstance()
					.getString(CODIGO_SCORING_VERANEO));
			rptaVeraneo.setCodigoPregunta(codigoVeraneo);
			rptaVeraneo.setValorSN(ACVConfig.getInstance().getString(
					VALOR_SCORING_VERANEO));
			
			//INICIO CODIGO DE COMERCIO
			// 134 : �Su vivienda se encuentra en una zona rural o urbana? 	
			RespuestaScoringVO respuestaZona = new RespuestaScoringVO();
			respuestaZona.setCodigoPregunta(134);
			respuestaZona.setValorAlternativa("URBANA");
			
			// 135 : �Su vivienda est� construida parcial o totalmente de adobe?	
			RespuestaScoringVO respuestaConstruccion = new RespuestaScoringVO();
			respuestaConstruccion.setCodigoPregunta(135);
			respuestaConstruccion.setValorAlternativa("NO");
			
			// 136 : �Su vivienda corresponde a su lugar permanente de residencia? 
			RespuestaScoringVO respuestaResidencia = new RespuestaScoringVO();
			respuestaResidencia.setCodigoPregunta(136);
			respuestaResidencia.setValorAlternativa("SI");
			
			// 137 : �Cuenta con rejas en ventanas o alarma o guardia / Conserje 24 hrs?
			RespuestaScoringVO respuestaSeguridad = new RespuestaScoringVO();
			respuestaSeguridad.setCodigoPregunta(137);
			respuestaSeguridad.setValorAlternativa("SI");
			//FIN CODIGO DE COMERCIO
			
			//INICIO Tramo hogar vacaciones
			
		
			
			RespuestaScoringVO respuestaTramo = new RespuestaScoringVO();
			String tramoDias = cotizacionHogar.getTramoDias();				
			respuestaTramo.setCodigoPregunta(151);
			respuestaTramo.setValorAlternativa(tramoDias);
			//FIN Tramo hogar vacaciones

			RespuestaScoringVO[] oRespuestaScoring = new RespuestaScoringVO[] {
					rptaComuna, rptaAntiguedad, rptaVeraneo, respuestaZona,
					respuestaConstruccion, respuestaResidencia,
					respuestaSeguridad, respuestaTramo };

			logger.debug("Antes de consultar WS Prima Hogar...");
			logger.debug("Parametros Enviados: " + cotizacionHogar.toString());

			// Log Datos de Entrada.
			logger.info("VALORIZACION HOGAR");
			logger.info("Datos de Entrada");
			logger.info("----------------");
			logger.info("Codigo Plan: " + codigoPlan);
			logger.info("Rut Cliente: " + rutCliente);
			logger.info("Monto Asegurado Edificio: " + montoAseguradoEdificio);
			logger.info("Monto Asegurado Contenido: " + montoAseguradoContenido);
			logger.info("Monto Asegurado Robo: " + montoAseguradoRobo);
			logger.info("Monto Asegurado Robo: " + montoAseguradoRobo);
			logger.info("Codigo Preg. comuna: " + codigoPregunta);
			logger.info("Val. Numeral Scoring: " + cotizacionHogar.getComuna());
			logger.info("Val. SN: " + cotizacionHogar.getComuna());

			logger.info("Codigo Preg. antiguedad: "
					+ rptaAntiguedad.getCodigoPregunta());
			logger.info("Val. Numeral antiguedad: "
					+ rptaAntiguedad.getValorNumeral());
			logger.info("Codigo Preg. veraneo: "
					+ rptaVeraneo.getCodigoPregunta());
			logger.info("Val. S/N veraneo: " + rptaVeraneo.getValorSN());
			logger.info("Codigo pregunta: " + respuestaZona.getCodigoPregunta());
			logger.info("Valor respuesta: " + respuestaZona.getValorAlternativa());
			logger.info("Codigo pregunta: " + respuestaConstruccion.getCodigoPregunta());
			logger.info("Valor respuesta: " + respuestaConstruccion.getValorAlternativa());
			logger.info("Codigo pregunta: " + respuestaResidencia.getCodigoPregunta());
			logger.info("Valor respuesta: " + respuestaResidencia.getValorAlternativa());
			logger.info("Codigo pregunta: " + respuestaSeguridad.getCodigoPregunta());
			logger.info("Valor respuesta: " + respuestaSeguridad.getValorAlternativa());
			logger.info("Codigo pregunta: " + respuestaTramo.getCodigoPregunta());
			logger.info("Valor respuesta: " + respuestaTramo.getValorAlternativa());
		
			
			logger.info("----------------");

			result = ws.calcularPrimaHogar(codigoPlan, rutCliente,
					montoAseguradoEdificio, montoAseguradoContenido,
					montoAseguradoRobo, oRespuestaScoring);

			logger.debug("Despues de consultar WS Prima Hogar...");

			logger.info("Resultado Prima Anual: " + result.getPrimaAnual());

			if (result != null) {

				// Log Datos de Salida.
				logger.info("Datos de Salida");
				logger.info("----------------");
				logger.info("Prima Anual: " + result.getPrimaAnual());
				logger.info("Prima Anual Pesos: " + result.getPrimaAnualPesos());
				logger.info("Prima Mensual: " + result.getPrimaMensual());
				logger.info("Prima Mensual Pesos: "
						+ result.getPrimaMensualPesos());
				logger.info("----------------");

				logger.debug("Valorizacion Obtenida.");
				valorizacion = new ValorizacionSeguro();
				valorizacion.setPrimaAnual(result.getPrimaAnual());
				valorizacion.setPrimaAnualPesos(Math.round(result
						.getPrimaAnualPesos()));
				valorizacion.setPrimaMensual(result.getPrimaMensual());
				valorizacion.setPrimaMensualPesos(Math.round(result
						.getPrimaMensualPesos()));
			}

		} catch (NamingException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(
					ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (RemoteException e) {
			logger.debug("Error durante invocando WS de valorizacion.", e);
			logger.debug("Detalle: " + e.detail);
			obtenerExcepcionWS(e);
			logger.error("Excepcion remota durante invocacion a WS.", e.detail);
			throw new ValorizacionException(
					ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (ErrorInternoException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(
					ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (ServiceException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(
					ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (MalformedURLException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(
					ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} finally {
			ResourceLeakUtil.close(context);
			ResourceLeakUtil.close(dao);
		}

		return valorizacion;
	}

	/**
	 * @throws ValorizacionException
	 * @see ValorizacionDAO#valorizarPlanVehiculo(CotizacionSeguroVehiculo)
	 */
	public ValorizacionSeguro valorizarPlanVehiculo(
			CotizacionSeguroVehiculo cotizacionVehiculo)
			throws ValorizacionException {

		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

		Context context = null;
		PrimaVehiculoVO result = null;
		ValorizacionSeguro valorizacion = new ValorizacionSeguro();
		try {
			context = new InitialContext();
			WsBigsaCalculoPrimaVehiculo svc = (WsBigsaCalculoPrimaVehiculo) context.lookup("java:comp/env/service/WsBigsaCalculoPrimaVehiculo");

			WsBigsaCalculoPrimaVehiculoImplDelegate ws = svc.getWsBigsaCalculoPrimaVehiculoImplPort(new URL(dao.getString(WSDL_VALORIZAR_PLAN_VEHICULO)));

			int codigoPlan = (int) cotizacionVehiculo.getCodigoPlan();
			int marca = (int) cotizacionVehiculo.getCodigoMarca();
			int tipoVehiculo = (int) cotizacionVehiculo.getCodigoTipoVehiculo();
			int codigoModelo = (int) cotizacionVehiculo.getCodigoModelo();
			int anyoVehiculo = (int) cotizacionVehiculo.getAnyoVehiculo();
			int montoAsegurado = (int) cotizacionVehiculo.getMontoAsegurado();
			int rutCliente = (int) cotizacionVehiculo.getRutCliente();
			String estadoCivilAsegurado = "";
			int edadContratante = -1;
			int edadAseguradoDuenyo = -1;
			String sexoAsegurado = "";

			if (cotizacionVehiculo.isClienteEsAsegurado()) {
				Contratante contratante = cotizacionVehiculo.getContratante();
				estadoCivilAsegurado = contratante.getEstadoCivil();
				edadContratante = calcularEdadContratante(contratante.getFechaDeNacimiento());
				sexoAsegurado = contratante.getSexo();
			} else {
				Contratante asegurado = cotizacionVehiculo.getAsegurado();
				estadoCivilAsegurado = asegurado.getEstadoCivil();
				edadAseguradoDuenyo = calcularEdadContratante(asegurado.getFechaDeNacimiento());
				sexoAsegurado = asegurado.getSexo();
				rutCliente = Integer.valueOf(String.valueOf(asegurado.getRut()));
			}

			// PREGUNTAS DE SCORING
			// 42: -> (Valor alternativa) CASADO, SOLTERO, VIUDO, OTRO.
			RespuestaScoringVO respuestaEstadoCivil = new RespuestaScoringVO();
			int codigoPregunta = ACVConfig.getInstance().getInt(SCORING_VEHICULO_ESTADO_CIVIL);
			respuestaEstadoCivil.setCodigoPregunta(codigoPregunta);
			respuestaEstadoCivil.setValorAlternativa(obtenerEstadoCivil(estadoCivilAsegurado));

			// 106: -> (Valor alternativa) Fijo 6
			RespuestaScoringVO respuestaMedioInformativo = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(SCORING_VEHICULO_MEDIO_INFORMATIVO);
			respuestaMedioInformativo.setCodigoPregunta(codigoPregunta);
			String valorMedioInformativo = ACVConfig.getInstance().getString(SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO);
			respuestaMedioInformativo.setValorAlternativa(valorMedioInformativo);

			// 100: -> (Valor alternativa) De 2 a 3, 4, 5
			RespuestaScoringVO respuestaNumeroPuertas = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(SCORING_VEHICULO_NUMERO_PUERTAS);
			respuestaNumeroPuertas.setCodigoPregunta(codigoPregunta);
			respuestaNumeroPuertas.setValorAlternativa("5");

			// 101: -> (Valor alternativa) Mayores a 28 a�os, Mayores a 23 a�os,
			// Mayores a 18 a�os
			RespuestaScoringVO respuestaEdadAsegurado = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONTRATANTE);
			respuestaEdadAsegurado.setCodigoPregunta(codigoPregunta);
			int rangoEdad = -1;
			if (cotizacionVehiculo.isClienteEsAsegurado()) {
            	rangoEdad = edadContratante;
            }else{
            	rangoEdad = edadAseguradoDuenyo;
            }
			respuestaEdadAsegurado.setValorAlternativa(obtenerRangoEdadAsegurado(rangoEdad));
			
			// 59: -> (Valor alternativa) Masculino, Femenino
			RespuestaScoringVO respuestaSexoAsegurado = new RespuestaScoringVO();
			codigoPregunta = ACVConfig.getInstance().getInt(SCORING_VEHICULO_SEXO_CONTRATANTE);
			respuestaSexoAsegurado.setCodigoPregunta(codigoPregunta);
			String descSexoAsegurado = ACVConfig.getInstance().getString(SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO);
			if (sexoAsegurado.equals("F")) {
				descSexoAsegurado = ACVConfig.getInstance().getString(SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO);
			}
			respuestaSexoAsegurado.setValorAlternativa(descSexoAsegurado);

			RespuestaScoringVO respuestaEdad = null;
			// 54: -> (Valor numeral) Edad conductores del vehiculo
			if (cotizacionVehiculo.isClienteEsAsegurado() == false) {
				respuestaEdad = new RespuestaScoringVO();
				codigoPregunta = ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
				respuestaEdad.setCodigoPregunta(codigoPregunta);
				respuestaEdad.setValorNumeral(Float.valueOf(edadAseguradoDuenyo));
			} else {
				respuestaEdad = new RespuestaScoringVO();
				codigoPregunta = ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
				respuestaEdad.setCodigoPregunta(codigoPregunta);
				respuestaEdad.setValorNumeral(Float.valueOf(edadContratante));
			}

			//INICIO CODIGO DE COMERCIO
			// 130 : �El veh�culo es de uso particular?		
			RespuestaScoringVO respuestaVehiculoParicular = new RespuestaScoringVO();
			respuestaVehiculoParicular.setCodigoPregunta(130);
			respuestaVehiculoParicular.setValorAlternativa("SI");
			
			// 131 : �El veh�culo fue internado a Chile con franquicia aduanera?	
			RespuestaScoringVO respuestaFranquicia = new RespuestaScoringVO();
			respuestaFranquicia.setCodigoPregunta(131);
			respuestaFranquicia.setValorAlternativa("NO");
			
			// 132 : �El veh�culo presenta da�os que impiden el normal funcionamiento de �ste?	
			RespuestaScoringVO respuestaFuncionamiento = new RespuestaScoringVO();
			respuestaFuncionamiento.setCodigoPregunta(132);
			respuestaFuncionamiento.setValorAlternativa("NO");
			
			// 133 : �El veh�culo presenta da�os en chapas, cerraduras u otra pieza que afecte su seguridad?
			RespuestaScoringVO respuestaSeguridad = new RespuestaScoringVO();
			respuestaSeguridad.setCodigoPregunta(133);
			respuestaSeguridad.setValorAlternativa("NO");
			//FIN CODIGO DE COMERCIO
			
			//Inicio Regalo Full Servicio - IG
			RespuestaScoringVO respuestaRegalo = new RespuestaScoringVO();
			respuestaRegalo.setCodigoPregunta(152);
			respuestaRegalo.setValorAlternativa("NO APLICA");
			//Fin Regalo Full Servicio - IG
						
			//INICIO SEGURO MOTO
			RespuestaScoringVO[] oRespuestas;
			if(tipoVehiculo == 6){
				logger.info("SCORING MOTO");
				oRespuestas = new RespuestaScoringVO[] {
						respuestaEstadoCivil, respuestaMedioInformativo,
						respuestaEdadAsegurado,
						respuestaSexoAsegurado,
						respuestaEdad };
			}else{
				logger.info("SCORING AUTO");
				//Inicio Regalo Full Servicio - IG
				if(cotizacionVehiculo.getNumSubcategoria().equals("22")){
					logger.info("SCORING FULL SERVICIO");
					oRespuestas = new RespuestaScoringVO[] {
							respuestaEstadoCivil, respuestaMedioInformativo,
							respuestaNumeroPuertas, respuestaEdadAsegurado,
							respuestaSexoAsegurado,
							respuestaEdad, respuestaVehiculoParicular,
							respuestaFranquicia, respuestaFuncionamiento,
							respuestaSeguridad, respuestaRegalo };
				}else{
					oRespuestas = new RespuestaScoringVO[] {
							respuestaEstadoCivil, respuestaMedioInformativo,
							respuestaNumeroPuertas, respuestaEdadAsegurado,
							respuestaSexoAsegurado,
							respuestaEdad, respuestaVehiculoParicular,
							respuestaFranquicia, respuestaFuncionamiento,
							respuestaSeguridad };
				}
				//Fin Regalo Full Servicio - IG
			}
			//FIN SEGURO MOTO

			logger.debug("Antes de consultar WS Prima Vehiculo...");
			logger.debug("Parametros Enviados: "+ cotizacionVehiculo.toString());

			// LOG datos Entrada
			logger.info("VALORIZACION VEHICULO");
			logger.info("----------------");
			logger.info("Datos de Entrada WS");
			logger.info("Codigo Plan: " + codigoPlan);
			logger.info("Marca Vehiculo: " + marca);
			logger.info("Tipo Vehiculo: " + tipoVehiculo);
			logger.info("Modelo Vehiculo: " + codigoModelo);
			logger.info("Ano Vehiculo: " + anyoVehiculo);
			logger.info("Monto Asegurado: " + montoAsegurado);
			logger.info("Rut: " + rutCliente);
			logger.info("----------------");
			logger.info("SCORING:");
			logger.info("cod_preg estado civil: "+ respuestaEstadoCivil.getCodigoPregunta());
			logger.info("valor estado civil: " + respuestaEstadoCivil.getValorAlternativa());
			logger.info("cod_preg medio informativo: "+ respuestaMedioInformativo.getCodigoPregunta());
			logger.info("valor medio informativo: "+ respuestaMedioInformativo.getValorAlternativa());
			logger.info("cod_preg edad 1: "+ respuestaEdadAsegurado.getCodigoPregunta());
			logger.info("valor edad 1: "+ respuestaEdadAsegurado.getValorAlternativa());
			logger.info("cod_preg sexo: "+ respuestaSexoAsegurado.getCodigoPregunta());
			logger.info("valor sexo: "+ respuestaSexoAsegurado.getValorAlternativa());
			logger.info("cod_preg edad 2: " + respuestaEdad.getCodigoPregunta());
			logger.info("valor edad 2: " + respuestaEdad.getValorNumeral());
			
			if(tipoVehiculo != 6){
				logger.info("cod_preg numero puertas: "+ respuestaNumeroPuertas.getCodigoPregunta());
				logger.info("valor numero puertas: "+ respuestaNumeroPuertas.getValorAlternativa());
				logger.info("cod_preg 1: " + respuestaVehiculoParicular.getCodigoPregunta());
				logger.info("valor 1: " + respuestaVehiculoParicular.getValorAlternativa());
				logger.info("cod_preg 2: " + respuestaFranquicia.getCodigoPregunta());
				logger.info("valor 2: " + respuestaFranquicia.getValorAlternativa());
				logger.info("cod_preg 3: " + respuestaFuncionamiento.getCodigoPregunta());
				logger.info("valor 3: " + respuestaFuncionamiento.getValorAlternativa());
				logger.info("cod_preg 4: " + respuestaSeguridad.getCodigoPregunta());
				logger.info("valor 4: " + respuestaSeguridad.getValorAlternativa());
				logger.info("cod_preg promo: " + respuestaRegalo.getCodigoPregunta());
				logger.info("valor promo: " + respuestaRegalo.getValorAlternativa());
				logger.info("----------------");
			}

			result = ws.calcularPrimaVehiculo(codigoPlan, marca, tipoVehiculo,
					codigoModelo, anyoVehiculo, montoAsegurado, rutCliente,
					oRespuestas);

			logger.debug("Despues de consultar WS Prima Vehiculo...");

			logger.info("Resultado Prima Anual: " + result.getPrimaAnual());

			if (result != null) {

				// Log Datos de Salida.
				logger.info("Datos de Salida");
				logger.info("----------------");
				logger.info("Prima Anual: " + result.getPrimaAnual());
				logger.info("Prima Anual Pesos: " + result.getPrimaAnualPesos());
				logger.info("Prima Mensual: " + result.getPrimaMensual());
				logger.info("Prima Mensual Pesos: "+ result.getPrimaMensualPesos());
				logger.info("----------------");

				valorizacion = new ValorizacionSeguro();
				valorizacion.setPrimaAnual(result.getPrimaAnual());
				valorizacion.setPrimaAnualPesos(Math.round(result.getPrimaAnualPesos()));
				valorizacion.setPrimaMensual(result.getPrimaMensual());
				valorizacion.setPrimaMensualPesos(Math.round(result.getPrimaMensualPesos()));
				valorizacion.setIdProducto(cotizacionVehiculo.getCodigoProducto());
			}

		} catch (NamingException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (RemoteException e) {
			logger.debug("Error durante invocando WS de valorizacion.", e);
			logger.debug("Detalle: " + e.detail);
			obtenerExcepcionWS(e);
			logger.error("Excepcion remota durante invocacion a WS.", e.detail);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (ErrorInternoException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (ServiceException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (MalformedURLException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} finally {
			ResourceLeakUtil.close(context);
			ResourceLeakUtil.close(dao);
		}

		return valorizacion;
	}

	/**
	 * TODO Describir m�todo obtenerRangoEdadAsegurado.
	 * 
	 * @param edadAsegurado
	 * @return
	 */
	private String obtenerRangoEdadAsegurado(int edadAsegurado) {
		String rangoEdadAsegurado = "";
		if (edadAsegurado >= 18 && edadAsegurado < 23) {
			rangoEdadAsegurado = "Mayores a 18";
		} else if (edadAsegurado >= 23 && edadAsegurado < 28) {
			rangoEdadAsegurado = "Mayores a 23";
		} else if (edadAsegurado >= 28) {
			rangoEdadAsegurado = "Mayores a 28";
		}
		return rangoEdadAsegurado;
	}

	/**
	 * TODO Describir m�todo calcularEdad.
	 * 
	 * @param cotizacionVehiculo
	 * @return
	 */
	private int calcularEdadContratante(Date fechaNacimiento) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fechaNacimiento);
		GregorianCalendar now = new GregorianCalendar();
		int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
				|| (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal
						.get(Calendar.DAY_OF_MONTH) > now
						.get(Calendar.DAY_OF_MONTH))) {
			res--;
		}
		return res;
	}

	/**
	 * TODO Describir m�todo obtenerEstadoCivil.
	 * 
	 * @param cotizacionVehiculo
	 * @return
	 */
	private String obtenerEstadoCivil(String sEstadoCivil) {
		String estadoCivil = "OTRO";
		if (sEstadoCivil.equals("1")) {
			estadoCivil = "SOLTERO";
		} else if (sEstadoCivil.equals("2")) {
			estadoCivil = "CASADO";
		} else if (sEstadoCivil.equals("3")) {
			estadoCivil = "VIUDO";
		}
		return estadoCivil;
	}

	/**
	 * @throws ValorizacionException
	 * @see ValorizacionDAO#valorizarPlanVida(CotizacionSeguroVida)
	 */
	public ValorizacionSeguro valorizarPlanVida(CotizacionSeguroVida cotizacionVida) throws ValorizacionException {

		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

		Context context = null;
		PrimaVidaVO result = null;
		ValorizacionSeguro valorizacion = new ValorizacionSeguro();
		try {
			context = new InitialContext();
			WsBigsaCalculoPrimaVida svc = (WsBigsaCalculoPrimaVida) context.lookup("java:comp/env/service/WsBigsaCalculoPrimaVida");

			WsBigsaCalculoPrimaVidaDelegate ws = svc.getWsBigsaCalculoPrimaVidaPort(new URL(dao.getString(WSDL_VALORIZAR_PLAN_VIDA)));
			
			int rutCliente;
			String fechaNacimiento;
			String valorEstadoCivil;
			int codigoPlan = (int) cotizacionVida.getCodigoPlan();
			float baseCalculo = cotizacionVida.getBaseCalculo();
			
			//INICIO CONTRATACION TERCEROS
			if (cotizacionVida.isClienteEsAsegurado()) {
				Contratante contratante = cotizacionVida.getContratante();
				rutCliente = (int)contratante.getRut();
				fechaNacimiento = new SimpleDateFormat("ddMMyyyy").format(contratante.getFechaDeNacimiento());
				valorEstadoCivil = contratante.getEstadoCivil();

			} else {
				Contratante asegurado = cotizacionVida.getAsegurado();
				rutCliente = (int) asegurado.getRut();
				fechaNacimiento = new SimpleDateFormat("ddMMyyyy").format(asegurado.getFechaDeNacimiento());
				valorEstadoCivil = asegurado.getEstadoCivil();
			}	
			//FIN CONTRATACION TERCEROS
			
			// Respuesta Scoring
			RespuestaScoringVO respuesta = new RespuestaScoringVO();
			int codigoPregunta = Integer.valueOf(ACVConfig.getInstance().getString(CODIGO_SCORING_VIDA));
			respuesta.setCodigoPregunta(codigoPregunta);

			String estadoCivil = SCR_ESTADO_CIVIL_OTRO;
			if (valorEstadoCivil.equals("1")) {
				estadoCivil = SCR_ESTADO_CIVIL_SOLTERO;
			} else if (valorEstadoCivil.equals("2")) {
				estadoCivil = SCR_ESTADO_CIVIL_CASADO;
			} else if (valorEstadoCivil.equals("3")) {
				estadoCivil = SCR_ESTADO_CIVIL_VIUDO;
			}

			estadoCivil = ACVConfig.getInstance().getString(estadoCivil);

			respuesta.setValorSN(estadoCivil);
			respuesta.setValorAlternativa(estadoCivil);
			
			//INICIO ASISTENCIA EN VIAJE
			RespuestaScoringVO respuestaTramo = new RespuestaScoringVO();
			String tramoDias = cotizacionVida.getTramoDias();
			if(tramoDias != null && !("").equals(tramoDias)){
				if(("DE 1 A 8 DIAS").equals(tramoDias.trim()) || ("DE 9 A 15 DIAS").equals(tramoDias.trim()) 
						|| ("DE 16 A 22 DIAS").equals(tramoDias.trim()) || ("DE 23 A 30 DIAS").equals(tramoDias.trim())
						|| ("DE 31 A 45 DIAS").equals(tramoDias.trim())){
					respuestaTramo.setCodigoPregunta(120);
				} else {
					respuestaTramo.setCodigoPregunta(150);
				}
			} 
			
			respuestaTramo.setValorAlternativa(tramoDias);
			//FIN ASISTENCIA EN VIAJE
			
			//INICIO ASISTENCIA VIAJE GRUPAL
			RespuestaScoringVO respuestaViajeGrupal = new RespuestaScoringVO();				
			respuestaViajeGrupal.setCodigoPregunta(148);
			respuestaViajeGrupal.setValorAlternativa(cotizacionVida.getIntegrante());
			//FIN ASISTENCIA EN VIAJE GRUPAL
			
			RespuestaScoringVO[] oRespuestas = new RespuestaScoringVO[] { respuesta, respuestaTramo, respuestaViajeGrupal };

			logger.debug("Antes de consultar WS Prima Vida...");
			logger.debug("Parametros Enviados: " + cotizacionVida.toString());

			// Log Datos de Entrada
			logger.info("VALORIZACION VIDA");
			logger.info("Datos de Entrada");
			logger.info("----------------");
			logger.info("Codigo Plan: " + codigoPlan);
			logger.info("Rut: " + rutCliente);
			logger.info("Fecha de Nacimiento: " + fechaNacimiento);
			logger.info("Base Calculo: " + baseCalculo);
			logger.info("Cod. Preg. Scoring: " + respuesta.getCodigoPregunta());
			logger.info("Val. SN Scoring: " + respuesta.getValorSN());
			logger.info("Val. Alternativa Scoring: " + respuesta.getValorAlternativa());
			logger.info("Cod. Preg. Tramo: " + respuestaTramo.getCodigoPregunta());
			logger.info("Val. Alternativa Tramo: " + respuestaTramo.getValorAlternativa());

			AseguradoVO asegurado = new AseguradoVO();
			asegurado.setFechaNacimiento(fechaNacimiento);
			asegurado.setParentesco(ParentAseg._value1);
			asegurado.setRutAsegurado(rutCliente);

			AseguradoVO[] listaAsegurados = new AseguradoVO[] { asegurado };

			result = ws.calcularPrimaVida(codigoPlan, rutCliente, fechaNacimiento, baseCalculo, listaAsegurados, oRespuestas);

			logger.debug("Despues de consultar WS Prima Vida...");

			logger.info("Resultado Prima Anual: " + result.getPrimaAnual());

			if (result != null) {

				// Log Datos de Salida.
				logger.info("Datos de Salida");
				logger.info("----------------");
				logger.info("Prima Anual: " + result.getPrimaAnual());
				logger.info("Prima Anual Pesos: " + result.getPrimaAnualPesos());
				logger.info("Prima Mensual: " + result.getPrimaMensual());
				logger.info("Prima Mensual Pesos: " + result.getPrimaMensualPesos());
				logger.info("----------------");

				logger.debug("Valorizacion Obtenida.");
				valorizacion = new ValorizacionSeguro();
				valorizacion.setPrimaAnual(result.getPrimaAnual());
				valorizacion.setPrimaAnualPesos(Math.round(result.getPrimaAnualPesos()));
				valorizacion.setPrimaMensual(result.getPrimaMensual());
				valorizacion.setPrimaMensualPesos(Math.round(result.getPrimaMensualPesos()));
			}
		} catch (NamingException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (RemoteException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			logger.debug("Detalle: " + e.detail);
			obtenerExcepcionWS(e);
			logger.error("Excepcion remota durante invocacion a WS.", e.detail);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (ErrorInternoException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (ServiceException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} catch (MalformedURLException e) {
			logger.error("Error durante invocando WS de valorizacion.", e);
			throw new ValorizacionException(ValorizacionException.ERROR_VALORIZACION_KEY, 0);
		} finally {
			ResourceLeakUtil.close(context);
			ResourceLeakUtil.close(dao);
		}

		return valorizacion;
	}

	public void close() {
		logger.debug("No existen recursos por cerrar en DAO.");
	}

	/**
	 * TODO Describir m�todo getDAOInterface.
	 * 
	 * @return
	 */
	public Class<?> getDAOInterface() {
		return ValorizacionDAO.class;
	}

	/**
	 * TODO Describir m�todo setDAOInterface.
	 * 
	 * @param interfazDAO
	 */
	public void setDAOInterface(Class interfazDAO) {
		if (logger.isDebugEnabled()) {
			logger.debug("No se establece interfaz DAO " + interfazDAO);
		}
	}

	/**
	 * TODO Describir m�todo obtenerExcepcionWS.
	 * 
	 * @param e
	 * @throws ValorizacionException
	 */
	private void obtenerExcepcionWS(RemoteException e)
			throws ValorizacionException {
		if (e.detail instanceof SOAPFaultException) {
			SOAPFaultException sfe = (SOAPFaultException) e.detail;
			Detail detail = sfe.getDetail();
			if (detail != null) {
				ValorizacionException vex = obtenerErrorValorizacion(detail
						.getChildNodes());
				if (vex != null) {
					logger.debug("Error de negocio durante invocacion a WS.",
							vex);
					throw vex;
				}
			}
		}
	}

	/**
	 * TODO Describir m�todo obtenerErrorValorizacion.
	 * 
	 * @param nodes
	 * @return
	 */
	private ValorizacionException obtenerErrorValorizacion(NodeList nodes) {
		if (nodes == null) {
			return null;
		}
		ValorizacionException vex = null;
		String message = null;
		String codigo = null;
		for (int i = 0; i < nodes.getLength(); i++) {
			Node item = nodes.item(i);
			vex = obtenerErrorValorizacion(item.getChildNodes());
			if (logger.isDebugEnabled()) {
				logger.debug(i + ") item : " + item);
				logger.debug(i + ") name : " + item.getNodeName());
				logger.debug(i + ") value: " + item.getNodeValue());
				logger.debug(i + ") eie  : " + vex);
			}
			if (vex == null) {
				if ("mensaje".equals(item.getNodeName())) {
					message = item.getFirstChild().getNodeValue();
				} else if ("codigo".equals(item.getNodeName())) {
					codigo = item.getFirstChild().getNodeValue();
				}
			} else {
				return vex;
			}
		}
		if ((message != null) && (codigo != null)) {
			logger.debug("Descripcion encontrada. Se crea excepcion.");
			vex = new ValorizacionException(message, Integer.valueOf(codigo));
		}
		return vex;
	}

}
