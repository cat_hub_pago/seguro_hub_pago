package cl.cencosud.asesorcotizador.dao;

import cl.tinet.common.dao.jdbc.BaseDAO;

public interface ClientePreferenteDAO extends BaseDAO{
	   /**
	    * Identifica si el cliente es preferente
	    * @param rut del cliente
	    * @return mensaje
	    */
	   public String consultarDescuento(int rut) throws java.rmi.RemoteException;
}
