package cl.cencosud.asesorcotizador.dao;

import cl.cencosud.acv.common.CoberturaSeg;
import cl.tinet.common.dao.jdbc.BaseDAO;

public interface ConsultarCoberturasDAO extends BaseDAO {
public CoberturaSeg[] obtenerCoberturas(Integer cod);
}
