package cl.cencosud.asesorcotizador.dao.ws;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.Stub;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.AceptacionDAO;
import cl.cencosud.bigsa.cotizacion.aceptar.webservice.client.AceptaCotizacionVO;
import cl.cencosud.bigsa.cotizacion.aceptar.webservice.client.ErrorInternoException;
import cl.cencosud.bigsa.cotizacion.aceptar.webservice.client.WsBigsaAceptaCotizacionImplDelegate;
import cl.cencosud.bigsa.cotizacion.aceptar.webservice.client.WsBigsaAceptaCotizacion_Impl;
import cl.tinet.common.config.BaseConfigurable;

import com.tinet.exceptions.system.SystemException;

public class AceptacionDAOWS extends BaseConfigurable implements AceptacionDAO {

    private static Log logger = LogFactory.getLog(AceptacionDAOWS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    public static final String LLAVE_USUARIO_REGISTRO =
        "cl.cencosud.asesorcotizador.cotizador.registro.bigsa.usuario";

    public static final String LLAVE_URL_WS =
        "cl.cencosud.asesorcotizador.dao.ws.RegistrarCotizacionDAOWS.wsdl.aceptar.cotizacion";

    public boolean aceptaTransaccion(Transaccion transaccion)
        throws ValorizacionException {

        Map < String, String > params = new HashMap < String, String >();

        ACVConfig config = ACVConfig.getInstance();

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        String url = dao.getString(LLAVE_URL_WS);

        params.put(SERVICE_ENDPOINT_KEY, url);

        logger.debug("WS: AceptaCotizacion");
        logger.debug("Antes de intanciar IMPL.");
        WsBigsaAceptaCotizacion_Impl svx = new WsBigsaAceptaCotizacion_Impl();

        // Inicializa el servicio (URL).
        logger.debug("Antes de Ininicalizar el servicio.");
        try {
            WsBigsaAceptaCotizacionImplDelegate ws =
                this.inicializarWS(svx.getWsBigsaAceptaCotizacionImplPort(),
                    params);

            String usuario = dao.getString(LLAVE_USUARIO_REGISTRO);

            Long lRut = transaccion.getRut_cliente();

            int rutCliente = Integer.parseInt(lRut.toString());

            int numeroCotizacion =
                Integer.parseInt(transaccion.getNumero_orden_compra());

            int codigoFormaPago = transaccion.getTipo_medio_pago();

            String numeroTarjeta = transaccion.getNro_tarjeta_entrada();

            AceptaCotizacionVO acepta =
                ws.aceptaCotizacion(usuario, rutCliente, numeroCotizacion,
                    codigoFormaPago, numeroTarjeta);

            return acepta.isCotizacionAceptada();
        } catch (RemoteException e) {
            logger.debug("Error durante invocando WS de valorizacion.", e);
            logger.debug("Detalle: " + e.detail);
            obtenerExcepcionWS(e);
            logger.error("Excepcion remota durante invocacion a WS.", e.detail);
            throw new SystemException(new Exception(e.detail.toString()));
        } catch (ErrorInternoException e) {
            logger.error("Error durante invocando WS de valorizacion.", e);
            throw new SystemException(e);
        }

    }

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub

    }

    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws ExtraccionDatosException 
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    /**
     * TODO Describir m�todo obtenerExcepcionWS.
     * @param e
     * @throws ValorizacionException
     */
    private void obtenerExcepcionWS(RemoteException e)
        throws ValorizacionException {
        if (e.detail instanceof SOAPFaultException) {
            SOAPFaultException sfe = (SOAPFaultException) e.detail;
            Detail detail = sfe.getDetail();
            if (detail != null) {
                ValorizacionException vex =
                    obtenerErrorValorizacion(detail.getChildNodes());
                if (vex != null) {
                    logger.debug("Error de negocio durante invocacion a WS.",
                        vex);
                    throw vex;
                }
            }
        }
    }

    /**
     * TODO Describir m�todo obtenerErrorValorizacion.
     * @param nodes
     * @return
     */
    private ValorizacionException obtenerErrorValorizacion(NodeList nodes) {
        if (nodes == null) {
            return null;
        }
        ValorizacionException vex = null;
        String message = null;
        String codigo = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node item = nodes.item(i);
            vex = obtenerErrorValorizacion(item.getChildNodes());
            if (logger.isDebugEnabled()) {
                logger.debug(i + ") item : " + item);
                logger.debug(i + ") name : " + item.getNodeName());
                logger.debug(i + ") value: " + item.getNodeValue());
                logger.debug(i + ") eie  : " + vex);
            }
            if (vex == null) {
                if ("mensaje".equals(item.getNodeName())) {
                    message = item.getFirstChild().getNodeValue();
                } else if ("codigo".equals(item.getNodeName())) {
                    codigo = item.getFirstChild().getNodeValue();
                }
            } else {
                return vex;
            }
        }
        if ((message != null) && (codigo != null)) {
            logger.debug("Descripcion encontrada. Se crea excepcion.");
            vex = new ValorizacionException(message, Integer.valueOf(codigo));
        }
        return vex;
    }

    public void actualizaSolitud(long idSolicitud,
        String codigoSolicitudInspeccion) {
        // TODO Auto-generated method stub

    }

    public void actualizarEstadoTransaccion(Transaccion transaccion) {
        // TODO Auto-generated method stub

    }

    public void generarTransaccion(Transaccion transaccion) {
        // TODO Auto-generated method stub

    }

    public InputStream obtenerCuerpoEmail(String nombre) {
        // TODO Auto-generated method stub
        return null;
    }

    public String obtenerDatosEmail(String nombre) {
        // TODO Auto-generated method stub
        return null;
    }

    public Transaccion obtenerDatosTransaccion(int idSolicitud) {
        // TODO Auto-generated method stub
        return null;
    }
}
