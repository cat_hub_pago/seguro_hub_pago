package cl.cencosud.asesorcotizador.dao.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.AceptacionDAO;
import cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.AceptaCotizacionVO;
import cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.ErrorInternoException;
import cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.WsBigsaAceptaCotizacion;
import cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.WsBigsaAceptaCotizacionImplDelegate;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

/**
 * DAO WebService para realizar la aceptación de la cotización en BIGSA.
 * <p>
 * Basado en implementación del servicio para JBOSS.
 * 
 * @see AceptacionDAOWS <br/>
 * 
 * @author Roberto San Martín
 * @version 1.0
 * @created Nov 10, 2010
 */
public class AceptacionDAOWSWAS extends BaseConfigurable implements
		AceptacionDAO {
	private static Logger logger = Logger.getLogger(AceptacionDAOWSWAS.class);
	public static final String LLAVE_USUARIO_REGISTRO = "cl.cencosud.asesorcotizador.cotizador.registro.bigsa.usuario";
	public static final String LLAVE_URL_WS = "cl.cencosud.asesorcotizador.dao.ws.RegistrarCotizacionDAOWS.wsdl.aceptar.cotizacion";

	public boolean aceptaTransaccion(Transaccion transaccion)
			throws ValorizacionException {

		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
		Context context = null;

		try {
			context = new InitialContext();
			WsBigsaAceptaCotizacion svc = (WsBigsaAceptaCotizacion) context
					.lookup("java:comp/env/service/WsBigsaAceptaCotizacion");

			AbstractConfigurator config = this.getConfigurator();
			String url = dao.getString(LLAVE_URL_WS);
			String usuario = config.getString(LLAVE_USUARIO_REGISTRO);
			WsBigsaAceptaCotizacionImplDelegate ws = svc
					.getWsBigsaAceptaCotizacionImplPort(new URL(url));

			Long lRut = transaccion.getRut_cliente();
			int rutCliente = Integer.parseInt(lRut.toString());
			int numeroCotizacion = Integer.parseInt(transaccion
					.getNumero_orden_compra());
			int codigoFormaPago = transaccion.getTipo_medio_pago();
			String numeroTarjeta = transaccion.getNro_tarjeta_entrada();

			logger.info("LLAMADO WS DE ACEPTACION");
			logger.info("usuario: " + usuario);
			logger.info("rutCliente: " + rutCliente);
			logger.info("numeroCotizacion: " + numeroCotizacion);
			logger.info("codigoFormaPago: " + codigoFormaPago);
			logger.info("numeroTarjeta: " + numeroTarjeta);

			AceptaCotizacionVO respuesta = ws.aceptaCotizacion(usuario,
					rutCliente, numeroCotizacion, codigoFormaPago,
					numeroTarjeta);

			logger.info("RESPUESTA WS: " + respuesta.isCotizacionAceptada());
			logger.info("EXPLICACION WS: " + respuesta.getExplicacion());
			return respuesta.isCotizacionAceptada();

		} catch (NamingException ne) {
			logger.error("Error obteniendo referencia al servicio", ne);
			throw new SystemException(ne);
		} catch (ServiceException se) {
			logger.error("Error invocando al WS Bigsa de aceptacion", se);
			throw new SystemException(se);
		} catch (MalformedURLException mue) {
			logger.error("URL de WS mal configurada", mue);
			throw new SystemException(mue);
		} catch (RemoteException re) {
			logger.error("Error invocando al WS de aceptacion", re);
			throw new SystemException(re);
		} catch (ErrorInternoException eie) {
			logger.error("Error invocando al WS de aceptacion", eie);
			throw new SystemException(eie);
		} finally {
			ResourceLeakUtil.close(context);
			ResourceLeakUtil.close(dao);
		}
	}

	public void close() {
	}

	public Class<?> getDAOInterface() {
		return AceptacionDAO.class;
	}

	@SuppressWarnings("unchecked")
	public void setDAOInterface(Class interfazDAO) {
	}
}
