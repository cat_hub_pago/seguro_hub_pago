package cl.cencosud.asesorcotizador.dao.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.ClientePreferenteVO;
import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.ErrorInternoException;
import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.WsBigsaClientePreferente;
import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.WsBigsaClientePreferenteImplDelegate;

import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.ClientePreferenteDAO;
import cl.tinet.common.config.BaseConfigurable;

public class ClientePreferenteDAOWSWAS extends BaseConfigurable implements ClientePreferenteDAO{
	private static Log logger = LogFactory.getLog(ClientePreferenteDAOWSWAS.class);
    private static final String WSDL_CLIENTE_PREFERENTE= "cl.cencosud.asesorcotizador.dao.ClientePreferenteDAOWS.wsdl.cliente.preferente";

	public String consultarDescuento(int rut) throws RemoteException {
		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
		Context context = null;
		ClientePreferenteVO result = null;
		String glosaRespuesta = null;
		int[] plan = {1};
		
		try {
			context = new InitialContext();
			WsBigsaClientePreferente wsCP = (WsBigsaClientePreferente)context.lookup("java:comp/env/service/WsBigsaClientePreferente");
			WsBigsaClientePreferenteImplDelegate delegate = wsCP.getWsBigsaClientePreferenteImplPort(new URL(dao.getString(WSDL_CLIENTE_PREFERENTE)));
			result = delegate.obtenerClientePreferente(rut, plan);
			
			if (result != null) {
				glosaRespuesta = result.getPopUp();
				logger.info("glosaRespuesta: "+result.getPopUp());
			}
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (ErrorInternoException e) {
			e.printStackTrace();
		}
		return glosaRespuesta;
	}
	
	public void close() {
	}

	public void setDAOInterface(Class interfazDAO) {
	}

	public Class getDAOInterface() {
		return null;
	}
}
