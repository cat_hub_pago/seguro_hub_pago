// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.ventaseguros.comparador.struts.action;

import cl.cencosud.acv.common.Plan;
import com.tinet.exceptions.system.SystemException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class ObtenerPlanesAction extends Action {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      String oIdPlanEliminar = request.getParameter("idPlan");

      try {
         LinkedHashMap e = (LinkedHashMap)request.getSession().getAttribute("mapResultComparador");
         if(oIdPlanEliminar != null && !oIdPlanEliminar.trim().equals("") && e != null && e.size() > 0 && e.containsKey("listPlanes")) {
            List result = (List)e.get("listPlanes");
            ArrayList pwritter = new ArrayList();
            Iterator mapper = result.iterator();

            while(mapper.hasNext()) {
               Plan json = (Plan)mapper.next();
               if(!json.getIdPlan().equals(oIdPlanEliminar)) {
                  pwritter.add(json);
               }
            }

            if(pwritter.size() >= 2) {
               e.put("listPlanes", pwritter);
               request.getSession().setAttribute("mapResultComparador", e);
            } else {
               e.put("mensaje", "No puede existir menos de 2 planes en el comparador.");
            }
         }

         LinkedHashMap result1 = new LinkedHashMap();
         result1.put("result", e);
         response.setHeader("pragma", "no-cache");
         response.setHeader("cache-control", "no-cache");
         response.setDateHeader("expires", -1L);
         response.setContentType("text/html");
         PrintWriter pwritter1 = response.getWriter();
         ObjectMapper mapper1 = new ObjectMapper();
         StringWriter json1 = new StringWriter();
         mapper1.writeValue(json1, result1);
         pwritter1.write(json1.toString());
         return null;
      } catch (IOException var11) {
         throw new SystemException(var11);
      }
   }
}
