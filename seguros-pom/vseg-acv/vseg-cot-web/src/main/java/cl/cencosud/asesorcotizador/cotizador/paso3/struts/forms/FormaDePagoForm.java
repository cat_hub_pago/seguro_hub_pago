// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.forms;

import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;

public class FormaDePagoForm extends BuilderActionFormBaseCOT {

   private static final long serialVersionUID = 5655687462471355971L;


   public InputStream getValidationRules(HttpServletRequest request) {
      return FormaDePagoForm.class.getResourceAsStream("resource/validation.xml");
   }
}
