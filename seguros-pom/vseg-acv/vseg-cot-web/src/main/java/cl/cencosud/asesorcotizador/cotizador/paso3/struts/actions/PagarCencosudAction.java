// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.exception.PagosException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.util.crypto.CryptoUtil;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class PagarCencosudAction extends Action {

   private static Log logger = LogFactory.getLog(PagarCencosudAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      HashMap res = new HashMap();

      try {
         CotizacionSeguro pwriter = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
         PagosDelegate mapper = new PagosDelegate();
         CotizacionDelegate json = new CotizacionDelegate();
         long lIdSolicitud = ((Long)request.getSession().getAttribute("idSolicitud")).longValue();
         int idSolicitud = Long.valueOf(lIdSolicitud).intValue();
         Integer numeroOrdenCompra = (Integer)request.getSession().getAttribute("resultNroBigsa");
         String nroTarjetaEntrada = request.getParameter("nroTarjeta");
         CryptoUtil cryptoUtil = new CryptoUtil(SeguridadConfig.getInstance());
         nroTarjetaEntrada = CryptoUtil.toBase64String(cryptoUtil.getEncrypted(nroTarjetaEntrada), true);
         String cargoRecurrente = request.getParameter("cargoRecurrente");
         int tipoCargo = cargoRecurrente != null?1:0;
         String tipoTarjeta = request.getParameter("tipoTarjeta");
         Map mTipoTarjeta = json.obtenerParametro("WEBPAY", tipoTarjeta);
         int tipoMedioPago = Integer.valueOf((String)mTipoTarjeta.get("valor")).intValue();
         Transaccion transaccion = new Transaccion();
         transaccion.setId_solicitud((long)idSolicitud);
         transaccion.setNro_tarjeta_entrada(nroTarjetaEntrada);
         transaccion.setNumero_orden_compra(numeroOrdenCompra.toString());
         transaccion.setRut_cliente(pwriter.getRutCliente());
         transaccion.setTipo_cargo(tipoCargo);
         transaccion.setTipo_medio_pago(tipoMedioPago);
         logger.info("ENTRANDO A PAGO CENCOSUD");
         
         boolean estado = mapper.pagarCencosud(transaccion);
         logger.info("FIN PAGO CENCOSUD");
         if(estado) {
            res.put("status", "ok");
         }
      } catch (Exception var22) {
    	  
    	  // agregar if de mensajes "exception".
    	   
    	String  mensaje =   var22.getMessage();
    	  logger.info("Mensaje : " + mensaje);

    	
    	String mensaje1= "No fue posible realizar el cargo en su tarjeta, favor comunicarse con el call center";
    	String mensaje2= "No fue posible realizar el cargo en su tarjeta, favor comunicarse con el call center";
    	String mensaje3= "El rut asociado a la tarjeta no es igual al rut del contratante";
    	String mensaje4= "La tarjeta se encuentra bloqueada";
    	String mensaje5= "No fue posible realizar el cargo en su tarjeta, favor comunicarse con el call center";
    	  
    	  if (mensaje.trim().equals(mensaje1.trim())){  
    		  res.put("status", "1");
    		  logger.info("status  1");  
    	  }else if (mensaje.trim().equals(mensaje2.trim())){
    		  res.put("status", "2");
    		  logger.info("status  2"); 
    	  }else if (mensaje.trim().equals(mensaje3.trim())){
    		  res.put("status", "3");
    		  logger.info("status  3"); 
    	  }else if (mensaje.trim().equals(mensaje4.trim())){  
    		  res.put("status", "4");
    		  logger.info("status  4"); 
    	  }else if (mensaje.trim().equals(mensaje5.trim())){
    		  res.put("status", "5");
    		  logger.info("status  5"); 
    	  }else{
    		  res.put("status", "error");
    		  logger.info("status  error");   
    	  }
         
      }

      response.setHeader("pragma", "no-cache");
      response.setHeader("cache-control", "no-cache");
      response.setDateHeader("expires", -1L);
      response.setContentType("text/html");
      PrintWriter pwriter1 = response.getWriter();
      ObjectMapper mapper1 = new ObjectMapper();
      StringWriter json1 = new StringWriter();
      mapper1.writeValue(json1, res);
      pwriter1.write(json1.toString());
      return null;
   }
}
