// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.asesorcotizador.cotizador.struts.actions.DesplegarCotizacionAction;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVehiculoForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class BuscarVehiculosAction extends DesplegarCotizacionAction {

   private static final Log logger = LogFactory.getLog(BuscarVehiculosAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      new CotizacionDelegate();
      HashMap clienteSession = this.obtenerDatosClienteSession(request);
      if(((CotizacionVehiculoForm)form).getDatos().isEmpty() && clienteSession != null) {
         ((CotizacionVehiculoForm)form).getDatos().putAll(clienteSession);
      }

      ArrayList vehiculosCliente = new ArrayList();
      logger.info("valor del rut en session:" + (String)((CotizacionVehiculoForm)form).getDatos().get("rut"));
      if(((CotizacionVehiculoForm)form).getDatos().get("rut") != null) {
         String rut = (String)((CotizacionVehiculoForm)form).getDatos().get("rut");
         request.setAttribute("arrVehiculo", vehiculosCliente);
         PrintWriter pwritter = response.getWriter();
         ObjectMapper mapper = new ObjectMapper();
         StringWriter json = new StringWriter();
         mapper.writeValue(json, vehiculosCliente);
         pwritter.write(json.toString());
      }

      response.setContentType("text/html");
      return null;
   }

}
