package cl.cencosud.asesorcotizador.cotizador.util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.PropertyConfigurator;

public class Log4JServlet extends HttpServlet {

	private static final long serialVersionUID = -9067858203664090118L;

	public void init() throws ServletException {
		String log4jfile = getInitParameter("log4j-init-file");
		
		if (log4jfile != null) {
			String propfile = getServletContext().getRealPath(log4jfile);
			PropertyConfigurator.configure(propfile);
		}
	}

}
