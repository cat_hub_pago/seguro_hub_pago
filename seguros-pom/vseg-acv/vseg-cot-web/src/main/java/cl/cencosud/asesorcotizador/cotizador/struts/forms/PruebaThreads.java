// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.cotizador.struts.actions.ThreadPrueba;

public class PruebaThreads {

   Plan plan;
   String idProducto2 = "";
   String idProducto3 = "";
   String idProducto4 = "";
   String idProducto5 = "";
   String idProducto6 = "";
   String idProducto7 = "";
   String idProducto = "";
   String idPlan = "";
   CotizacionSeguro cotizacion = new CotizacionSeguro();
   int numero = 0;
   Thread a = null;
   private Boolean estado = Boolean.valueOf(false);


   public Boolean getEstado() {
      return this.estado;
   }

   public void setEstado(Boolean estado) {
      this.estado = estado;
   }

   public void setData(Plan str) {
      this.plan = str;
   }

   public int getNumero() {
      return this.numero;
   }

   public void setNumero(int numero) {
      this.numero = numero;
   }

   public CotizacionSeguro getCotizacion() {
      return this.cotizacion;
   }

   public void setCotizacion(CotizacionSeguro cotizacion) {
      this.cotizacion = cotizacion;
   }

   public String getIdPlan() {
      return this.idPlan;
   }

   public void setIdPlan(String idPlan) {
      this.idPlan = idPlan;
   }

   public String getIdProducto() {
      return this.idProducto;
   }

   public void setIdProducto(String idProducto) {
      this.idProducto = idProducto;
   }

   public String getIdProducto2() {
      return this.idProducto2;
   }

   public void setIdProducto2(String idProducto2) {
      this.idProducto2 = idProducto2;
   }

   public String getIdProducto3() {
      return this.idProducto3;
   }

   public void setIdProducto3(String idProducto3) {
      this.idProducto3 = idProducto3;
   }

   public String getIdProducto4() {
      return this.idProducto4;
   }

   public void setIdProducto4(String idProducto4) {
      this.idProducto4 = idProducto4;
   }

   public String getIdProducto5() {
      return this.idProducto5;
   }

   public void setIdProducto5(String idProducto5) {
      this.idProducto5 = idProducto5;
   }

   public String getIdProducto6() {
      return this.idProducto6;
   }

   public void setIdProducto6(String idProducto6) {
      this.idProducto6 = idProducto6;
   }

   public String getIdProducto7() {
      return this.idProducto7;
   }

   public void setIdProducto7(String idProducto7) {
      this.idProducto7 = idProducto7;
   }

   public Plan getData() {
      return this.plan;
   }

   public Thread getA() {
      return this.a;
   }

   public void setA(Thread a) {
      this.a = a;
   }

   public void requestData() {
      try {
         ThreadPrueba asd = new ThreadPrueba(this);
         this.a = new Thread(asd, "Prueba Runnable" + this.numero);
         this.setA(this.a);
         this.a.start();
      } catch (Exception var2) {
         ;
      }

   }
}
