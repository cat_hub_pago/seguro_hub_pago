// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.ventaseguros.comparador.struts.action;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Subcategoria;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarComparadorAction extends Action {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      String oPlanes = request.getParameter("oPlanes");
      CotizacionDelegate oDelegate = new CotizacionDelegate();
      if(oPlanes != null && !oPlanes.trim().equals("")) {
         String[] arrPlanes = oPlanes.split(";");
         ArrayList listPlanes = new ArrayList();
         CotizacionSeguro oCotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
         String idProducto = String.valueOf(oCotizacionSeguro.getCodigoProducto());
         String idSubcategoria = (String)request.getSession().getAttribute("idSubcategoria");
         String idRama = (String)request.getSession().getAttribute("idRama");
         Subcategoria subcategoria = oDelegate.obtenerSubcategoriaPorId(Long.parseLong(idSubcategoria));
         if(subcategoria != null) {
            request.setAttribute("descSubcategoria", subcategoria.getTituloSubcategoria());
         }

         Producto[] productos = oDelegate.obtenerProductos(idSubcategoria, idRama, (String)null);
         String tmp;
         if(productos != null && productos.length > 0) {
            tmp = "";
            String idProd = "";
            Producto[] plan = productos;
            int arrObject = productos.length;

            for(int var18 = 0; var18 < arrObject; ++var18) {
               Producto producto = plan[var18];
               idProd = producto.getIdProducto();
               request.setAttribute("idProductoComp", idProd);
               if(producto.getIdProducto().trim().equals(idProducto.trim())) {
                  tmp = producto.getNombreProducto();
                  break;
               }
            }

            request.setAttribute("descProducto", tmp);
         }

         Map var23;
         if(oCotizacionSeguro.getRegion() != null) {
            var23 = oDelegate.obtenerRegionPorId(oCotizacionSeguro.getRegion());
            request.setAttribute("regionDescripcion", var23.get("descripcion_region"));
         }

         if(oCotizacionSeguro.getCiudad() != null) {
            var23 = oDelegate.obtenerCiudadPorId(oCotizacionSeguro.getCiudad());
            request.setAttribute("ciudadDescripcion", var23.get("descripcion_ciudad"));
         }

         if(oCotizacionSeguro.getComuna() != null) {
            var23 = oDelegate.obtenerComunaPorId(oCotizacionSeguro.getComuna());
            request.setAttribute("comunaDescripcion", var23.get("descripcion_comuna"));
         }

         this.getData(request, oDelegate);
         String[] var26 = arrPlanes;
         int var24 = arrPlanes.length;

         for(int var22 = 0; var22 < var24; ++var22) {
            tmp = var26[var22];
            String[] var25 = tmp.split(":");
            Plan var27 = new Plan();

            for(int i = 0; i < var25.length; ++i) {
               switch(i) {
               case 0:
                  var27.setIdPlan(var25[i]);
                  break;
               case 1:
                  var27.setNombrePlan(var25[i]);
                  break;
               case 2:
                  var27.setCompannia(var25[i]);
                  break;
               case 3:
                  var27.setPrimaMensualPesos(Long.valueOf(var25[i]).longValue());
                  break;
               case 4:
                  var27.setPrimaMensualUF(Float.valueOf(var25[i]).floatValue());
                  break;
               case 5:
                  var27.setPrimaAnualUF(Float.valueOf(var25[i]).floatValue());
                  break;
               case 6:
                  var27.setPrimaAnualPesos(Long.valueOf(var25[i]).longValue());
                  break;
               case 7:
                  var27.setHiddProducto(var25[i]);
               }
            }

            listPlanes.add(var27);
         }

         this.getData(request, listPlanes, idProducto);
      }

      return mapping.findForward("continuar");
   }

   private void getData(HttpServletRequest request, List listPlanes, String idProducto) {
      LinkedHashMap mapCarateristicas = new LinkedHashMap();
      List listTempCaractComp = null;
      if(listPlanes != null && listPlanes.size() > 0) {
         CotizacionDelegate mapResultComparador = new CotizacionDelegate();
         List listPivote = null;

         List list;
         for(Iterator var9 = listPlanes.iterator(); var9.hasNext(); listPivote = list) {
            Plan plan = (Plan)var9.next();
            list = mapResultComparador.getCaracteristicasComparadorPorPlan(Integer.valueOf(plan.getIdPlan()), Integer.valueOf(1));
            if(listPivote != null) {
               for(int i = 0; i < listPivote.size(); ++i) {
                  for(int j = 0; j < list.size(); ++j) {
                     if(((Caracteristica)listPivote.get(i)).getDescripcion().equals(((Caracteristica)list.get(j)).getDescripcion()) && ((Caracteristica)listPivote.get(i)).getTipo() == ((Caracteristica)list.get(j)).getTipo()) {
                        ((Caracteristica)list.get(j)).setId_caracteristica(((Caracteristica)listPivote.get(i)).getId_caracteristica());
                     }
                  }
               }
            }

            mapCarateristicas.put(plan.getIdPlan(), list);
         }

         listTempCaractComp = mapResultComparador.obtenerCaracteristicasCompartidas(Long.valueOf(idProducto), Integer.valueOf(1));
      }

      LinkedHashMap var13 = new LinkedHashMap();
      var13.put("listPlanes", listPlanes);
      var13.put("listCaracteristicas", this.getNormalizedRows(mapCarateristicas, listTempCaractComp));
      request.getSession().setAttribute("mapResultComparador", var13);
   }

   private LinkedHashMap getNormalizedRows(LinkedHashMap mapCarateristicas, List listCaractComp) {
      LinkedHashMap mapResult = new LinkedHashMap();
      ArrayList listCaract = new ArrayList();
      Collection c = mapCarateristicas.values();
      Iterator iter = c.iterator();

      while(iter.hasNext()) {
         List mapPorTipo = (List)iter.next();
         if(mapPorTipo != null && mapPorTipo.size() > 0) {
            listCaract.addAll(mapPorTipo);
         }
      }

      LinkedHashMap mapPorTipo1 = new LinkedHashMap();
      Iterator listTemp = listCaract.iterator();

      Integer tipoCaract;
      while(listTemp.hasNext()) {
         Caracteristica mapCaractComp = (Caracteristica)listTemp.next();
         tipoCaract = Integer.valueOf(mapCaractComp.getTipo());
         if(mapPorTipo1.containsKey(tipoCaract)) {
            List mapPorCaract = (List)mapPorTipo1.get(tipoCaract);
            mapPorCaract.add(mapCaractComp);
         } else {
            ArrayList mapPorCaract1 = new ArrayList();
            mapPorCaract1.add(mapCaractComp);
            mapPorTipo1.put(tipoCaract, mapPorCaract1);
         }
      }

      iter = mapPorTipo1.entrySet().iterator();

      while(iter.hasNext()) {
         Entry mapCaractComp2 = (Entry)iter.next();
         List listTemp1 = (List)mapCaractComp2.getValue();
         tipoCaract = (Integer)mapCaractComp2.getKey();
         LinkedHashMap mapPorCaract2 = new LinkedHashMap();
         if(listTemp1 != null && listTemp1.size() > 0) {
            Iterator var13 = listTemp1.iterator();

            while(var13.hasNext()) {
               Caracteristica ca = (Caracteristica)var13.next();
               String idCaract = String.valueOf(ca.getId_caracteristica());
               if(mapPorCaract2.containsKey(idCaract)) {
                  List list = (List)mapPorCaract2.get(idCaract);
                  list.add(ca);
               } else {
                  ArrayList list1 = new ArrayList();
                  list1.add(ca);
                  mapPorCaract2.put(idCaract, list1);
               }
            }
         }

         if(mapPorCaract2.size() > 0) {
            mapResult.put(tipoCaract, mapPorCaract2);
         }
      }

      if(listCaractComp != null && listCaractComp.size() > 0) {
         LinkedHashMap mapCaractComp1 = new LinkedHashMap();
         mapCaractComp1.put("caractComp", listCaractComp);
         mapResult.put(Integer.valueOf(3), mapCaractComp1);
      }

      return mapResult;
   }

   private void getData(HttpServletRequest request, CotizacionDelegate oDelegate) {
      if(request.getSession().getAttribute("datosCotizacion") instanceof CotizacionSeguroVehiculo) {
         CotizacionSeguroVehiculo datosCot = (CotizacionSeguroVehiculo)request.getSession().getAttribute("datosCotizacion");
         Vehiculo[] tipos = oDelegate.obtenerTiposVehiculos();
         Vehiculo[] marcas = oDelegate.obtenerMarcasPorTipoVehiculos(String.valueOf(datosCot.getCodigoTipoVehiculo()));
         Vehiculo[] modelos = oDelegate.obtenerModelosVehiculos(String.valueOf(datosCot.getCodigoTipoVehiculo()), String.valueOf(datosCot.getCodigoMarca()));

         int i;
         for(i = 0; i < tipos.length; ++i) {
            if(tipos[i].getIdTipoVehiculo().equals(String.valueOf(datosCot.getCodigoTipoVehiculo()))) {
               request.setAttribute("tipoDesc", tipos[i].getDescripcionTipoVehiculo());
            }
         }

         for(i = 0; i < marcas.length; ++i) {
            if(marcas[i].getIdMarcaVehiculo().equals(String.valueOf(datosCot.getCodigoMarca()))) {
               request.setAttribute("marcaDesc", marcas[i].getMarcaVehiculo());
            }
         }

         for(i = 0; i < modelos.length; ++i) {
            if(modelos[i].getIdModeloVehiculo().equals(String.valueOf(datosCot.getCodigoModelo()))) {
               request.setAttribute("modeloDes", modelos[i].getModeloVehiculo());
            }
         }
      }

   }
}
