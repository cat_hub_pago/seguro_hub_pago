// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionSaludForm;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import cl.tinet.common.util.validate.ValidacionUtil;

import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class IngresarDatosVidaForm extends BuilderActionFormBaseCOT {

   private static final long serialVersionUID = -3765873996264509432L;
   String[] idFilaBeneficiario;
   String[] rutBeneficiario;
   String[] dvBeneficiario;
   String[] nombreBeneficiario;
   String[] apellidoPaternoBeneficiario;
   String[] apellidoMaternoBeneficiario;
   String[] diaFechaNacimientoBeneficiario;
   String[] mesFechaNacimientoBeneficiario;
   String[] anyoFechaNacimientoBeneficiario;
   String[] parentescoBeneficiario;
   String[] sexoBeneficiario;
   String[] porcentajeBeneficiario;
   String[] idFilaAdicional;
   String[] rutAdicional;
   String[] dvAdicional;
   String[] nombreAdicional;
   String[] apellidoPaternoAdicional;
   String[] apellidoMaternoAdicional;
   String[] diaFechaNacimientoAdicional;
   String[] mesFechaNacimientoAdicional;
   String[] anyoFechaNacimientoAdicional;
   String[] sexoAdicional;
   String[] parentescoAdicional;


   public InputStream getValidationRules(HttpServletRequest request) {
      return CotizacionSaludForm.class.getResourceAsStream("resource/validation-ingresaDatosVida.xml");
   }

   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
      Usuario usuario = SeguridadUtil.getUsuario(request);
      ActionErrors errors = new ActionErrors();
      ActionErrors errorsAux = new ActionErrors();
      ActionErrors errorsReq = new ActionErrors();
      ActionErrors errorsReqFrmMultBenef = new ActionErrors();
      ActionErrors errorsOtros = new ActionErrors();
      ActionErrors errorsOtrosMultBenef = new ActionErrors();
      boolean datosFaltantesBenef = false;
      boolean hayBeneficiarios = false;
      request.getSession().setAttribute("fechaMaxima", null);
      String idSubcategoria = (String)request.getSession().getAttribute("idSubcategoria");
      
      if(((String)this.getDatos().get("tipoTelefono1")).equals("")) {
         errorsReq.add("datos.tipoTelefono1", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("codigoTelefono1")).equals("")) {
         errorsReq.add("datos.codigoTelefono1", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("numeroTelefono1")).equals("")) {
         errorsReq.add("datos.numeroTelefono1", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("region")).equals("")) {
         errorsReq.add("datos_region", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("comuna")).equals("")) {
         errorsReq.add("datos_comuna", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("ciudad")).equals("")) {
         errorsReq.add("datos_ciudad", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("direccionNumero")).equals("")) {
         errorsReq.add("datos.direccionNumero", new ActionMessage("errors.required"));
      } else {
         try {
            Long.parseLong((String)this.getDatos().get("direccionNumero"));
         } catch (NumberFormatException var23) {
            errorsReq.add("datos.direccionNumero", new ActionMessage("errors.validacion"));
         }
      }

      if(((String)this.getDatos().get("direccion")).equals("")) {
         errorsReq.add("datos.direccion", new ActionMessage("errors.required"));
      }

      // Validar si corresponde a compra sin registro
      if(request.getParameter("sinRegistro") == null) {
    	  System.out.println("Compra con registro");
          if(usuario == null && request.getParameter("password") == null 
        		  || String.valueOf(request.getParameter("password")).equals("")) {
              errors.add("clavepaso2", new ActionMessage("errors.required"));
           }
      }

      int porcTotal = 0;
      int totBeneficiarios = 0;
      int e;
      if(this.idFilaBeneficiario != null && this.idFilaBeneficiario.length > 0) {
    	 System.out.println("Producto con beneficiarios");
         hayBeneficiarios = true;

         for(e = 0; e < this.idFilaBeneficiario.length; ++e) {
        	 System.out.println("Cantidad de Beneficiarios "+this.idFilaBeneficiario.length);
        	 
            ++totBeneficiarios;
            if(totBeneficiarios <= 1 || !this.rutBeneficiario[e].equals("") || !this.dvBeneficiario[e].equals("") 
            		|| !this.nombreBeneficiario[e].equals("") || !this.apellidoPaternoBeneficiario[e].equals("") 
            		|| !this.apellidoMaternoBeneficiario[e].equals("") || !this.diaFechaNacimientoBeneficiario[e].equals("") 
            		|| !this.mesFechaNacimientoBeneficiario[e].equals("") || !this.anyoFechaNacimientoBeneficiario[e].equals("") 
            		|| !this.parentescoBeneficiario[e].equals("") || !this.sexoBeneficiario[e].equals("") 
            		|| !this.porcentajeBeneficiario[e].equals("")) {
				long ex;            
				if(this.rutBeneficiario[e].equals("") || this.dvBeneficiario[e].equals("")) {
					errorsReq.add(this.idFilaBeneficiario[e] + "rutBeneficiario", new ActionMessage("errors.required"));
					errorsReq.add(this.idFilaBeneficiario[e] + "dvBeneficiario", new ActionMessage("errors.required"));
					
				}else{
					ex = Long.parseLong(this.rutBeneficiario[e]);
					char digitoVerificador = (this.dvBeneficiario[e]).length() == 0?32:(this.dvBeneficiario[e]).charAt(0);
					try{
						if(!ValidacionUtil.isValidoRUT(ex, digitoVerificador)) {		
							errorsAux.add(this.idFilaBeneficiario[e] + "rutBeneficiario", new ActionMessage("errors.validacion"));
							errorsAux.add(this.idFilaBeneficiario[e] + "dvBeneficiario", new ActionMessage("errors.validacion"));
						}
					}catch(IllegalArgumentException var10) {						   
						errorsAux.add(this.idFilaBeneficiario[e] + "rutBeneficiario", new ActionMessage("errors.validacion"));
						errorsAux.add(this.idFilaBeneficiario[e] + "dvBeneficiario", new ActionMessage("errors.validacion"));
					}
				}
                             
               if(this.nombreBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "nombreBeneficiario", new ActionMessage("errors.validacion"));
               }

               if(this.apellidoPaternoBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "apellidoPaternoBeneficiario", new ActionMessage("errors.validacion"));
               }

               if(this.apellidoMaternoBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "apellidoMaternoBeneficiario", new ActionMessage("errors.validacion"));
               }

               if(this.diaFechaNacimientoBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "diaFechaNacimientoBeneficiario", new ActionMessage("errors.validacion"));
               }

               if(this.mesFechaNacimientoBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "mesFechaNacimientoBeneficiario", new ActionMessage("errors.validacion"));
               }

               if(this.anyoFechaNacimientoBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "anyoFechaNacimientoBeneficiario", new ActionMessage("errors.validacion"));
               }

               if(this.parentescoBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "parentescoBeneficiario", new ActionMessage("errors.validacion"));
               }

               if(this.sexoBeneficiario[e].equals("")) {
                  errorsAux.add(this.idFilaBeneficiario[e] + "sexoBeneficiario", new ActionMessage("errors.validacion"));
               }

               //NO VALIDAR EN VIAJE GRUPAL
               if(!idSubcategoria.equals("221")){
                   if(this.porcentajeBeneficiario[e].equals("")) {
                	   errorsAux.add(this.idFilaBeneficiario[e] + "porcentajeBeneficiario", new ActionMessage("errors.validacion"));
                   } else {
                      boolean porc = false;

                      int intPorcentaje;
                      try {
                    	  intPorcentaje = Integer.parseInt(this.porcentajeBeneficiario[e]);
                         if(intPorcentaje < 0) {
                        	 intPorcentaje = 0;
                        	 errorsAux.add(this.idFilaBeneficiario[e] + "porcentajeBeneficiario", new ActionMessage("errors.validacion"));
                         } else if(intPorcentaje > 100) {
                            errorsOtrosMultBenef.add("porcentajeIncompleto", new ActionMessage("errors.validacion.distribucion.sobre"));
                         }
                      } catch (NumberFormatException var22) {
                    	  intPorcentaje = 0;
                      }

                      porcTotal += intPorcentaje;
                   }
               }               
            }
         }

         if(errorsAux.size() > 0) {
            errorsReqFrmMultBenef.add(errorsAux);
            errorsAux.clear();
            datosFaltantesBenef = true;
         }
      }

      if(hayBeneficiarios && datosFaltantesBenef) {
         errorsReqFrmMultBenef.add("camposSolicitadosBenef", new ActionMessage("errors.validacion.solicitado"));
      }

      if(errorsReq.size() == 0) {
         if(!((String)this.getDatos().get("numeroTelefono1")).equals("")) {
            try {
               if(Long.parseLong((String)this.getDatos().get("numeroTelefono1")) <= 0L) {
                  errorsOtros.add("datos.numeroTelefono1", new ActionMessage("errors.required"));
               }
            } catch (NumberFormatException var21) {
               errorsOtros.add("datos.numeroTelefono1", new ActionMessage("errors.required"));
            }
         }

         if(!((String)this.getDatos().get("numeroTelefono2")).equals("")) {
            try {
               if(Long.parseLong((String)this.getDatos().get("numeroTelefono2")) <= 0L) {
                  errorsOtros.add("datos.numeroTelefono2", new ActionMessage("errors.required"));
               }
            } catch (NumberFormatException var20) {
               errorsOtros.add("datos.numeroTelefono2", new ActionMessage("errors.required"));
            }
         }
      }

      if(errorsReqFrmMultBenef.size() == 0 && hayBeneficiarios && errorsOtrosMultBenef.size("porcentajeIncompleto") == 0) {
    	  if(!idSubcategoria.equals("221")){
    		  if(porcTotal < 100) {
    			  System.out.println("porcTotal < 100");
    			  errorsOtrosMultBenef.add("porcentajeIncompleto", new ActionMessage("errors.validacion.distribucion"));
    		  } else if(porcTotal > 100) {
    			  System.out.println("porcTotal > 100");
    			  errorsOtrosMultBenef.add("porcentajeIncompleto", new ActionMessage("errors.validacion.distribucion.sobre"));
    		  }  
    	  }
      }
      
      //INICIO ASISTENCIA EN VIAJE
      GregorianCalendar fechaHoy;
      CotizacionSeguro datosCotizacion1;
      Date fecha;
      if(this.getDatos().get("fechaInicio") != null) {
         if(((String)this.getDatos().get("fechaInicio")).equals("")) {
             errorsReq.add("inputIniVig", new ActionMessage("errors.required"));
         } else {
            datosCotizacion1 = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
            SimpleDateFormat ne1 = new SimpleDateFormat("dd/MM/yyyy");
            GregorianCalendar fechaInicio1 = new GregorianCalendar();
            GregorianCalendar fechaTermino2 = new GregorianCalendar();
            GregorianCalendar fechaMaxima1 = new GregorianCalendar();
            fechaHoy = new GregorianCalendar();

            try {
               fecha = ne1.parse((String)this.getDatos().get("fechaInicio"));
               int fechaMax = Integer.parseInt(request.getSession().getAttribute("maxFechaVig").toString());
               String fechaPresente1 = ne1.format(fechaHoy.getTime());
               String fechaIni = (String)this.getDatos().get("fechaInicio");
               fechaInicio1.setTime(fecha);
               fechaTermino2.setTime(fecha);
               datosCotizacion1.setNumDiaMaxIniVig(fechaMax);
               fechaMaxima1.add(5, datosCotizacion1.getNumDiaMaxIniVig());
               if(!fechaHoy.before(fechaInicio1) && !fechaPresente1.equals(fechaIni)) {
            	   errorsOtros.add("inputIniVig", new ActionMessage("errors.fechaAnterior"));
               } else if(!fechaInicio1.before(fechaMaxima1)) {
            	   request.getSession().setAttribute("fechaMaxima", "true");
//            	   errorsOtros.add("inputIniVig", new ActionMessage("errors.fechaSuperaMax"));
               } else {
                  ne1 = new SimpleDateFormat("ddMMyyyy");
                  fechaTermino2.add(2, 1);
                  request.getSession().setAttribute("fechaInicioVig", ne1.format(fechaInicio1.getTime()));
                  request.getSession().setAttribute("fechaTerminoVig", ne1.format(fechaTermino2.getTime()));
               }
            } catch (ParseException var18) {
            	errorsOtros.add("inputIniVig", new ActionMessage("errors.fechaErronea"));
            }
         }
      }
      
      datosCotizacion1 = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      if(datosCotizacion1 != null && !datosCotizacion1.isClienteEsAsegurado()) {
         if(!((String)this.getDatos().get("dueTipoTelefono1")).equals("") && !((String)this.getDatos().get("dueCodigoTelefono1")).equals("") && !((String)this.getDatos().get("dueNumeroTelefono1")).equals("")) {
            try {
               if(Long.parseLong((String)this.getDatos().get("dueNumeroTelefono1")) <= 0L) {
            	   errorsReq.add("dueTipoTelefono1", new ActionMessage("errors.required"));
            	   errorsReq.add("dueCodigoTelefono1", new ActionMessage("errors.required"));
            	   errorsReq.add("dueNumeroTelefono1", new ActionMessage("errors.required"));
               }
            } catch (NumberFormatException var15) {
            	errorsReq.add("dueTipoTelefono1", new ActionMessage("errors.required"));
            	errorsReq.add("dueCodigoTelefono1", new ActionMessage("errors.required"));
            	errorsReq.add("dueNumeroTelefono1", new ActionMessage("errors.required"));
            }
         } else {
        	 errorsReq.add("dueTipoTelefono1", new ActionMessage("errors.required"));
        	 errorsReq.add("dueCodigoTelefono1", new ActionMessage("errors.required"));
        	 errorsReq.add("dueNumeroTelefono1", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueRegion")).equals("")) {
        	 errorsReq.add("dueRegion", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueComuna")).equals("")) {
        	 errorsReq.add("dueComuna", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueCiudad")).equals("")) {
        	 errorsReq.add("dueCiudad", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueCalle")).equals("") || ((String)this.getDatos().get("dueNumeroCalle")).equals("")) {
        	 errorsReq.add("dueCalle", new ActionMessage("errors.required"));
        	 errorsReq.add("dueNumeroCalle", new ActionMessage("errors.required"));
        	 errorsReq.add("dueNumeroDepto", new ActionMessage("errors.required"));
         }

         if(!((String)this.getDatos().get("dueNumeroCalle")).equals("")) {
            try {
               Long.parseLong((String)this.getDatos().get("dueNumeroCalle"));
            } catch (NumberFormatException var14) {
            	errorsOtros.add("dueCalle", new ActionMessage("errors.validacion"));
            	errorsOtros.add("dueNumeroCalle", new ActionMessage("errors.validacion"));
            	errorsOtros.add("dueNumeroDepto", new ActionMessage("errors.validacion"));
            }
         }
      }
      //FIN ASISTENCIA EN VIAJE

      request.setAttribute("erroresRequerido", errorsReq.properties());
      request.setAttribute("erroresRequeridoMultBenef", errorsReqFrmMultBenef.properties());
      request.setAttribute("erroresOtros", errorsOtros.properties());
      request.setAttribute("erroresOtrosMultBenef", errorsOtrosMultBenef.properties());
      errors.add(errorsReq);
      errors.add(errorsReqFrmMultBenef);
      errors.add(errorsOtros);
      errors.add(errorsOtrosMultBenef);
      return errors;
   }

   public String[] getRutBeneficiario() {
	   return this.rutBeneficiario;
   }
   
   public void setRutBeneficiario(String[] rutBeneficiario) {
	   this.rutBeneficiario = rutBeneficiario;
   }
   
   public String[] getDvBeneficiario() {
	   return this.dvBeneficiario;
   }
   
   public void setDvBeneficiario(String[] dvBeneficiario) {
	   this.dvBeneficiario = dvBeneficiario;
   }
   
   public String[] getNombreBeneficiario() {
      return this.nombreBeneficiario;
   }

   public void setNombreBeneficiario(String[] nombreBeneficiario) {
      this.nombreBeneficiario = nombreBeneficiario;
   }

   public String[] getApellidoPaternoBeneficiario() {
      return this.apellidoPaternoBeneficiario;
   }

   public void setApellidoPaternoBeneficiario(String[] apellidoPaternoBeneficiario) {
      this.apellidoPaternoBeneficiario = apellidoPaternoBeneficiario;
   }

   public String[] getApellidoMaternoBeneficiario() {
      return this.apellidoMaternoBeneficiario;
   }

   public void setApellidoMaternoBeneficiario(String[] apellidoMaternoBeneficiario) {
      this.apellidoMaternoBeneficiario = apellidoMaternoBeneficiario;
   }

   public String[] getDiaFechaNacimientoBeneficiario() {
      return this.diaFechaNacimientoBeneficiario;
   }

   public void setDiaFechaNacimientoBeneficiario(String[] diaFechaNacimientoBeneficiario) {
      this.diaFechaNacimientoBeneficiario = diaFechaNacimientoBeneficiario;
   }

   public String[] getMesFechaNacimientoBeneficiario() {
      return this.mesFechaNacimientoBeneficiario;
   }

   public void setMesFechaNacimientoBeneficiario(String[] mesFechaNacimientoBeneficiario) {
      this.mesFechaNacimientoBeneficiario = mesFechaNacimientoBeneficiario;
   }

   public String[] getAnyoFechaNacimientoBeneficiario() {
      return this.anyoFechaNacimientoBeneficiario;
   }

   public void setAnyoFechaNacimientoBeneficiario(String[] anyoFechaNacimientoBeneficiario) {
      this.anyoFechaNacimientoBeneficiario = anyoFechaNacimientoBeneficiario;
   }

   public String[] getParentescoBeneficiario() {
      return this.parentescoBeneficiario;
   }

   public void setParentescoBeneficiario(String[] parentescoBeneficiario) {
      this.parentescoBeneficiario = parentescoBeneficiario;
   }

   public String[] getSexoBeneficiario() {
      return this.sexoBeneficiario;
   }

   public void setSexoBeneficiario(String[] sexoBeneficiario) {
      this.sexoBeneficiario = sexoBeneficiario;
   }

   public String[] getPorcentajeBeneficiario() {
      return this.porcentajeBeneficiario;
   }

   public void setPorcentajeBeneficiario(String[] porcentajeBeneficiario) {
      this.porcentajeBeneficiario = porcentajeBeneficiario;
   }

   public String[] getRutAdicional() {
      return this.rutAdicional;
   }

   public void setRutAdicional(String[] rutAdicional) {
      this.rutAdicional = rutAdicional;
   }
	   
   public String[] getDvAdicional() {
      return this.dvAdicional;
   }

   public void setDvAdicional(String[] dvAdicional) {
      this.dvAdicional = dvAdicional;
   }
   
   public String[] getNombreAdicional() {
      return this.nombreAdicional;
   }

   public void setNombreAdicional(String[] nombreAdicional) {
      this.nombreAdicional = nombreAdicional;
   }

   public String[] getApellidoPaternoAdicional() {
      return this.apellidoPaternoAdicional;
   }

   public void setApellidoPaternoAdicional(String[] apellidoPaternoAdicional) {
      this.apellidoPaternoAdicional = apellidoPaternoAdicional;
   }

   public String[] getApellidoMaternoAdicional() {
      return this.apellidoMaternoAdicional;
   }

   public void setApellidoMaternoAdicional(String[] apellidoMaternoAdicional) {
      this.apellidoMaternoAdicional = apellidoMaternoAdicional;
   }

   public String[] getDiaFechaNacimientoAdicional() {
      return this.diaFechaNacimientoAdicional;
   }

   public void setDiaFechaNacimientoAdicional(String[] diaFechaNacimientoAdicional) {
      this.diaFechaNacimientoAdicional = diaFechaNacimientoAdicional;
   }

   public String[] getMesFechaNacimientoAdicional() {
      return this.mesFechaNacimientoAdicional;
   }

   public void setMesFechaNacimientoAdicional(String[] mesFechaNacimientoAdicional) {
      this.mesFechaNacimientoAdicional = mesFechaNacimientoAdicional;
   }

   public String[] getAnyoFechaNacimientoAdicional() {
      return this.anyoFechaNacimientoAdicional;
   }

   public void setAnyoFechaNacimientoAdicional(String[] anyoFechaNacimientoAdicional) {
      this.anyoFechaNacimientoAdicional = anyoFechaNacimientoAdicional;
   }

   public String[] getSexoAdicional() {
      return this.sexoAdicional;
   }

   public void setSexoAdicional(String[] sexoAdicional) {
      this.sexoAdicional = sexoAdicional;
   }

   public String[] getParentescoAdicional() {
      return this.parentescoAdicional;
   }

   public void setParentescoAdicional(String[] parentescoAdicional) {
      this.parentescoAdicional = parentescoAdicional;
   }

   public String[] getIdFilaBeneficiario() {
      return this.idFilaBeneficiario;
   }

   public void setIdFilaBeneficiario(String[] idFilaBeneficiario) {
      this.idFilaBeneficiario = idFilaBeneficiario;
   }

   public String[] getIdFilaAdicional() {
      return this.idFilaAdicional;
   }

   public void setIdFilaAdicional(String[] idFilaAdicional) {
      this.idFilaAdicional = idFilaAdicional;
   }
}
