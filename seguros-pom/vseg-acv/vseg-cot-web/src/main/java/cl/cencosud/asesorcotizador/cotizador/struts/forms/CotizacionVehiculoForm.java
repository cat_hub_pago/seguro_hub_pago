// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:21
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import cl.tinet.common.util.validate.ValidacionUtil;
import java.io.InputStream;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CotizacionVehiculoForm extends BuilderActionFormBaseCOT {

   private static final long serialVersionUID = -1656721547994569635L;
   private static final String VALIDATOR_VEHICULO = "resource/validation-vehiculo.xml";
   private static final String VALIDATOR_CONTRATANTE_DUENYO = "resource/validation-vehiculo-duenyo.xml";
   private String vehiculos;
   private String arrVehiculo;
   private String vehiculosCliente;


   public InputStream getValidationRules(HttpServletRequest request) {
      HashMap datos = (HashMap)this.getDatos();
      String esDuenyo = datos.get("contratanteEsDuenyo") == null?"":(String)datos.get("contratanteEsDuenyo");
      String validatorFile = VALIDATOR_CONTRATANTE_DUENYO;
      if(esDuenyo.equals("true")) {
         validatorFile = VALIDATOR_VEHICULO;
      }

      return CotizacionVehiculoForm.class.getResourceAsStream(validatorFile);
   }

   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
      ActionErrors errors = super.validate(mapping, request);
      long ex;
      if(!errors.get("datos.dv").hasNext() && !errors.get("datos.rut").hasNext() && !((String)this.getDatos().get("rut")).equals("") && !((String)this.getDatos().get("dv")).equals("")) {
         ex = Long.parseLong((String)this.getDatos().get("rut"));
         char digitoVerificador = ((String)this.getDatos().get("dv")).length() == 0?32:((String)this.getDatos().get("dv")).charAt(0);

         try {
            if(!ValidacionUtil.isValidoRUT(ex, digitoVerificador)) {
               errors.add("datos.rutdv", new ActionMessage("errors.validacion"));
               errors.add("datos.dv", new ActionMessage("errors.validacion"));
               errors.add("datos.rut", new ActionMessage("errors.validacion", true));
            }
         } catch (IllegalArgumentException var10) {
            errors.add("datos.rutdv", new ActionMessage("errors.validacion"));
            errors.add("datos.dv", new ActionMessage("errors.validacion"));
            errors.add("datos.rut", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.rutdv", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("nombre")).equals("")) {
         if(errors.get("datos.nombre").hasNext()) {
            errors.add("datos.nombres", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.nombres", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("tipoTelefono")).equals("") && !((String)this.getDatos().get("codigoTelefono")).equals("") && !((String)this.getDatos().get("numeroTelefono")).equals("")) {
         try {
            ex = Long.parseLong((String)this.getDatos().get("numeroTelefono"));
            if(ex <= 0L) {
               errors.add("datos.telefono1", new ActionMessage("errors.validacion"));
               errors.add("datos.telefono2", new ActionMessage("errors.validacion"));
               errors.add("datos.telefono3", new ActionMessage("errors.validacion"));
            }
         } catch (NumberFormatException var9) {
            errors.add("datos.telefono1", new ActionMessage("errors.validacion"));
            errors.add("datos.telefono2", new ActionMessage("errors.validacion"));
            errors.add("datos.telefono3", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.telefono", new ActionMessage("errors.required"));
         errors.add("datos.telefono1", new ActionMessage("errors.required"));
         errors.add("datos.telefono2", new ActionMessage("errors.required"));
         errors.add("datos.telefono3", new ActionMessage("errors.required"));
      }

      String ex1;
      if(!((String)this.getDatos().get("diaFechaNacimiento")).equals("") && !((String)this.getDatos().get("mesFechaNacimiento")).equals("") && !((String)this.getDatos().get("anyoFechaNacimiento")).equals("")) {
         ex1 = ((String)this.getDatos().get("diaFechaNacimiento")).length() < 2?"0" + (String)this.getDatos().get("diaFechaNacimiento"):(String)this.getDatos().get("diaFechaNacimiento");
         ex1 = ex1 + (((String)this.getDatos().get("mesFechaNacimiento")).length() < 2?"0" + (String)this.getDatos().get("mesFechaNacimiento"):(String)this.getDatos().get("mesFechaNacimiento"));
         ex1 = ex1 + (String)this.getDatos().get("anyoFechaNacimiento");
         if(!ValidacionUtil.isFechaValida(ex1, "ddMMyyyy")) {
            errors.add("datos.fechaNacimiento", new ActionMessage("errors.validacion"));
            errors.add("diasFecha", new ActionMessage("errors.validacion"));
            errors.add("mesFecha", new ActionMessage("errors.validacion"));
            errors.add("anyosFecha", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.fechaNacimiento", new ActionMessage("errors.required"));
         errors.add("diasFecha", new ActionMessage("errors.required"));
         errors.add("mesFecha", new ActionMessage("errors.required"));
         errors.add("anyosFecha", new ActionMessage("errors.required"));
      }

      if(this.getDatos().get("contratanteEsDuenyo") != null && ((String)this.getDatos().get("contratanteEsDuenyo")).equals("false")) {
         if(((String)this.getDatos().get("rutDuenyo")).equals("") || ((String)this.getDatos().get("dvDuenyo")).equals("")) {
            errors.add("datos.rutDuenyo", new ActionMessage("errors.required"));
            errors.add("datos.dvDuenyo", new ActionMessage("errors.required"));
         }

         if(!((String)this.getDatos().get("nombreDuenyo")).equals("") && !((String)this.getDatos().get("apellidoPaternoDuenyo")).equals("") && !((String)this.getDatos().get("apellidoMaternoDuenyo")).equals("")) {
            if(errors.get("datos.nombreDuenyo").hasNext() || errors.get("datos.apellidoPaternoDuenyo").hasNext() || errors.get("datos.apellidoPaternoDuenyo").hasNext()) {
               errors.add("datos.nombreDuenyo", new ActionMessage("errors.validacion"));
               errors.add("datos.apellidoPaternoDuenyo", new ActionMessage("errors.validacion"));
               errors.add("datos.apellidoMaternoDuenyo", new ActionMessage("errors.validacion"));
               errors.add("datos.grupoNombreDuenyo", new ActionMessage("errors.validacion"));
            }
         } else {
            errors.add("datos.nombreDuenyo", new ActionMessage("errors.required"));
            errors.add("datos.apellidoPaternoDuenyo", new ActionMessage("errors.required"));
            errors.add("datos.apellidoMaternoDuenyo", new ActionMessage("errors.required"));
            errors.add("datos.grupoNombreDuenyo", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("estadoCivilDuenyo")).equals("")) {
            errors.add("datos.estadoCivilDuenyo", new ActionMessage("errors.required"));
         }

         if(this.getDatos().get("sexoDuenyo") == null || ((String)this.getDatos().get("sexoDuenyo")).equals("")) {
            errors.add("datos.sexoDuenyo", new ActionMessage("errors.required"));
         }

         if(!((String)this.getDatos().get("diaFechaNacimientoDuenyo")).equals("") && !((String)this.getDatos().get("mesFechaNacimientoDuenyo")).equals("") && !((String)this.getDatos().get("anyoFechaNacimientoDuenyo")).equals("")) {
            ex1 = ((String)this.getDatos().get("diaFechaNacimientoDuenyo")).length() < 2?"0" + (String)this.getDatos().get("diaFechaNacimientoDuenyo"):(String)this.getDatos().get("diaFechaNacimientoDuenyo");
            ex1 = ex1 + (((String)this.getDatos().get("mesFechaNacimientoDuenyo")).length() < 2?"0" + (String)this.getDatos().get("mesFechaNacimientoDuenyo"):(String)this.getDatos().get("mesFechaNacimientoDuenyo"));
            ex1 = ex1 + (String)this.getDatos().get("anyoFechaNacimientoDuenyo");
            if(!ValidacionUtil.isFechaValida(ex1, "ddMMyyyy")) {
               errors.add("datos.fechaNacimientoDuenyo", new ActionMessage("errors.validacion"));
               errors.add("diasFechaDuenyo", new ActionMessage("errors.validacion"));
               errors.add("mesFechaDuenyo", new ActionMessage("errors.validacion"));
               errors.add("anyosFechaDuenyo", new ActionMessage("errors.validacion"));
            }
         } else {
            errors.add("datos.fechaNacimientoDuenyo", new ActionMessage("errors.required"));
            errors.add("diasFechaDuenyo", new ActionMessage("errors.required"));
            errors.add("mesFechaDuenyo", new ActionMessage("errors.required"));
            errors.add("anyosFechaDuenyo", new ActionMessage("errors.required"));
         }
      }

      if(((String)this.getDatos().get("tipoVehiculo")).equals("")) {
         errors.add("tipoVehiculo", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("marcaVehiculo")).equals("")) {
         errors.add("marcaVehiculo", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("modeloVehiculo")).equals("")) {
         errors.add("modeloVehiculo", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("anyoVehiculo")).equals("")) {
         errors.add("anyoVehiculo", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("producto")).equals("")) {
         errors.add("producto", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("valorComercial")).equals("")) {
         errors.add("valorComercial", new ActionMessage("errors.required"));
      } else {
         try {
            if(Long.parseLong(((String)this.getDatos().get("valorComercial")).toString()) < 0L) {
               errors.add("valorComercial", new ActionMessage("errors.outofrange.down"));
            } else if(Long.parseLong(((String)this.getDatos().get("valorComercial")).toString()) > 100000000L) {
               errors.add("valorComercial", new ActionMessage("errors.outofrange.up"));
            }
         } catch (NumberFormatException var8) {
            errors.add("valorComercial", new ActionMessage("errors.validacion"));
         }
      }

      request.setAttribute("cl.tinet.common.struts.validator.PROPERTY_NAMES", errors.properties());
      return errors;
   }

   public void setArrVehiculo(String arrVehiculo) {
      this.arrVehiculo = arrVehiculo;
   }

   public String getArrVehiculo() {
      return this.arrVehiculo;
   }

   public void setVehiculosCliente(String vehiculosCliente) {
      this.vehiculosCliente = vehiculosCliente;
   }

   public String getVehiculosCliente() {
      return this.vehiculosCliente;
   }

   public void setVehiculos(String vehiculos) {
      this.vehiculos = vehiculos;
   }

   public String getVehiculos() {
      return this.vehiculos;
   }
}
