// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Beneficiario;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.IngresarDatosVidaForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class GuardarDatosAdicionalesCotizacionVidaAction extends Action {

   private static Log logger = LogFactory.getLog(GuardarDatosAdicionalesCotizacionVidaAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		   throws BusinessException, ParseException {
      request.getSession().removeAttribute("error-login");
      
      if(request.getSession().getAttribute("idSolicitud") != null) {
         try {
          	// Validar si corresponde a compra sin registro
          	if(request.getParameter("sinRegistro") == null) {
         		request.getSession().removeAttribute("sinRegistro");
         		request.getSession().removeAttribute("rutSinRegistro");
         		
                this.verificarAutenticacion(request, response);
                Usuario forward = SeguridadUtil.getUsuario(request);
                if(forward.isCambioClaveRequerido()) {
                    request.getSession().setAttribute("frameClaveTemporal", "true");
                    request.setAttribute("reload", "true");
                }
         	}else{
         		logger.info("Compra sin registro");
            	String sinRegistro = request.getParameter("sinRegistro");
            	String rutSinRegistro = request.getParameter("rutSinRegistro");
        		request.getSession().setAttribute("sinRegistro", sinRegistro);
        		request.getSession().setAttribute("rutSinRegistro", rutSinRegistro);
        	}
       		String numSubcategoria = (String)request.getSession().getAttribute("idSubcategoria");
       		
           long idSolicitud = Long.parseLong(String.valueOf(request.getSession().getAttribute("idSolicitud")));
           logger.info("Solicitud -> " + idSolicitud + "");
           BuilderActionFormBaseCOT oForm = (BuilderActionFormBaseCOT)form;
           CotizacionSeguroVida oCotizacionSeguro = this.obtenerDatosAdicionalesVida(oForm, numSubcategoria);
           CotizacionSeguroVida cotizacion = (CotizacionSeguroVida)request.getSession().getAttribute("datosCotizacion");
  		   oCotizacionSeguro.setNumSubcategoria(numSubcategoria);
           oCotizacionSeguro.setActividad(cotizacion.getActividad());
           oCotizacionSeguro.setApellidoPaterno(cotizacion.getApellidoPaterno());
           oCotizacionSeguro.setApellidoMaterno(cotizacion.getApellidoMaterno());
           oCotizacionSeguro.setAsegurado(cotizacion.getAsegurado());
           oCotizacionSeguro.setBaseCalculo(cotizacion.getBaseCalculo());
           oCotizacionSeguro.setClienteEsAsegurado(cotizacion.isClienteEsAsegurado());
           oCotizacionSeguro.setCodigoPlan(cotizacion.getCodigoPlan());
           oCotizacionSeguro.setCodigoProducto(cotizacion.getCodigoProducto());
           oCotizacionSeguro.setContratante(cotizacion.getContratante());
           oCotizacionSeguro.setDv(cotizacion.getDv());
           oCotizacionSeguro.setEmail(cotizacion.getEmail());
           oCotizacionSeguro.setEstadoCivil(cotizacion.getEstadoCivil());
           oCotizacionSeguro.setRutCliente(cotizacion.getRutCliente());
           oCotizacionSeguro.setSexo(cotizacion.getSexo());
           oCotizacionSeguro.setFechaNacimiento(cotizacion.getFechaNacimiento());
           oCotizacionSeguro.setFechaNacimiento(cotizacion.getFechaNacimiento());
           oCotizacionSeguro.setNombre(cotizacion.getNombre());
           // INICIO ASISTENCIA EN VIAJE
           oCotizacionSeguro.setTramoDias(cotizacion.getTramoDias());
           oCotizacionSeguro.setIntegrante(cotizacion.getIntegrante());
           // FIN ASISTENCIA EN VIAJE
           
	   		// INICIO MASCOTA
	   		if(numSubcategoria.equals("140")){
	   			logger.info("Datos adicionales mascota");
	   			oCotizacionSeguro.setTipoMascota(cotizacion.getTipoMascota());
	   			oCotizacionSeguro.setRazaMascota(cotizacion.getRazaMascota());
	   			oCotizacionSeguro.setSexoMascota(cotizacion.getSexoMascota());
	   			oCotizacionSeguro.setEdadMascota(cotizacion.getEdadMascota());
	   		}
	   		// FIN MASCOTA
           
           if(oCotizacionSeguro.isClienteEsAsegurado()){
               Contratante asegurado = oCotizacionSeguro.getAsegurado();
               asegurado.setCalleDireccion(oCotizacionSeguro.getDireccion());
               asegurado.setNumeroDireccion(oCotizacionSeguro.getNumeroDireccion());
               asegurado.setNumeroDepartamentoDireccion(oCotizacionSeguro.getNumeroDepto());
               asegurado.setTelefono1(oCotizacionSeguro.getNumeroTelefono1());
               asegurado.setTipoTelefono1(oCotizacionSeguro.getTipoTelefono1());
               asegurado.setTelefono2(oCotizacionSeguro.getNumeroTelefono2());
               asegurado.setTipoTelefono2(oCotizacionSeguro.getTipoTelefono2());
               asegurado.setIdRegion(oCotizacionSeguro.getRegion());
               asegurado.setIdComuna(oCotizacionSeguro.getComuna());
               asegurado.setIdCiudad(oCotizacionSeguro.getCiudad());
               oCotizacionSeguro.setAsegurado(asegurado);
           }else{
        	   //INICIO CONTRATACION TERCEROS
        	   logger.info("Contratacion Terceros");
               Contratante asegurado = oCotizacionSeguro.getAsegurado();
               CotizacionSeguroVida cotVidaPaso2 = this.obtenerDatosAdicionalesVida(oForm, numSubcategoria);
               asegurado.setCalleDireccion(cotVidaPaso2.getAsegurado().getCalleDireccion());
               asegurado.setNumeroDireccion(cotVidaPaso2.getAsegurado().getNumeroDireccion());
               asegurado.setNumeroDepartamentoDireccion(cotVidaPaso2.getAsegurado().getNumeroDepartamentoDireccion());
               asegurado.setTelefono1(cotVidaPaso2.getAsegurado().getTelefono1());
               asegurado.setTipoTelefono1(cotVidaPaso2.getAsegurado().getTipoTelefono1());
               asegurado.setTelefono2(cotVidaPaso2.getAsegurado().getTelefono2());
               asegurado.setTipoTelefono2(cotVidaPaso2.getAsegurado().getTipoTelefono2());
               asegurado.setIdRegion(cotVidaPaso2.getAsegurado().getIdRegion());
               asegurado.setIdComuna(cotVidaPaso2.getAsegurado().getIdComuna());
               asegurado.setIdCiudad(cotVidaPaso2.getAsegurado().getIdCiudad());
               oCotizacionSeguro.setAsegurado(asegurado); 
               //FIN CONTRATACION TERCEROS
           }
           
           int largoAsegurados = 0;
           if (oCotizacionSeguro.getBeneficiarios() != null && numSubcategoria.equals("221")){
        	   largoAsegurados = oCotizacionSeguro.getBeneficiarios().length;
        	   Beneficiario[] beneficiarios = new Beneficiario[largoAsegurados-1];
        	   int x = 0;
        	   for (Beneficiario beneficiario : oCotizacionSeguro.getBeneficiarios()) {
        		   if (!String.valueOf(beneficiario.getRut()).equals(String.valueOf(oCotizacionSeguro.getAsegurado().getRut()))){
        			   beneficiarios[x]=beneficiario;
        			   x++;
        		   }
        	   }
        	   oCotizacionSeguro.setBeneficiarios(beneficiarios);
           }
           
           
           
           Contratante contratante = oCotizacionSeguro.getContratante();
           contratante.setCalleDireccion(oCotizacionSeguro.getDireccion());
           contratante.setNumeroDireccion(oCotizacionSeguro.getNumeroDireccion());
           contratante.setNumeroDepartamentoDireccion(oCotizacionSeguro.getNumeroDepto());
           contratante.setTelefono1(oCotizacionSeguro.getNumeroTelefono1());
           contratante.setTipoTelefono1(oCotizacionSeguro.getTipoTelefono1());
           contratante.setTelefono2(oCotizacionSeguro.getNumeroTelefono2());
           contratante.setTipoTelefono2(oCotizacionSeguro.getTipoTelefono2());
           contratante.setIdRegion(oCotizacionSeguro.getRegion());
           contratante.setIdComuna(oCotizacionSeguro.getComuna());
           contratante.setIdCiudad(oCotizacionSeguro.getCiudad());
           oCotizacionSeguro.setContratante(contratante);
           
           CotizacionDelegate oDelegate = new CotizacionDelegate();
           logger.info("INICIO GUARDAR DATOS BIGSA");
           int resultNroBigsa = oDelegate.ingresarDatosAdicionalesVida(idSolicitud, oCotizacionSeguro);
           logger.info("TERMINO GUARDAR DATOS BIGSA, NUMERO GENERADO: " + resultNroBigsa + "");
           request.getSession().setAttribute("resultNroBigsa", Integer.valueOf(resultNroBigsa));
           request.getSession().setAttribute("datosCotizacion", oCotizacionSeguro);
           request.removeAttribute("reload");
           
         } catch (AutenticacionException var15) {
            logger.debug("Error autenticando usuario.", var15);
            request.getSession().setAttribute("error-login", var15.getMessage(request.getLocale()));
            request.setAttribute("reload", "true");
         }

         String forward1 = "continuar";
         return mapping.findForward(forward1);
      } else {
         throw new ProcesoCotizacionException("cl.cencosud.ventaseguros.cotizacion.exception.NO_SOLICITUD", new Object[0]);
      }
   }

   private CotizacionSeguroVida obtenerDatosAdicionalesVida(BuilderActionFormBaseCOT form,
		   String idSubcategoria) throws ParseException {
      CotizacionSeguroVida oCotizacion = new CotizacionSeguroVida();
      CotizacionDelegate oDelegate = new CotizacionDelegate();
      oCotizacion.setTipoTelefono1((String)form.getDatos().get("tipoTelefono1"));
      String numeroTelefono1 = (String)form.getDatos().get("codigoTelefono1") + (String)form.getDatos().get("numeroTelefono1");
      oCotizacion.setNumeroTelefono1(numeroTelefono1);
      oCotizacion.setTipoTelefono2((String)form.getDatos().get("tipoTelefono2"));
      String numeroTelefono2 = (String)form.getDatos().get("codigoTelefono2") + (String)form.getDatos().get("numeroTelefono2");
      oCotizacion.setNumeroTelefono2(numeroTelefono2);
      oCotizacion.setRegion((String)form.getDatos().get("region"));
      oCotizacion.setComuna((String)form.getDatos().get("comuna"));
      oCotizacion.setCiudad((String)form.getDatos().get("ciudad"));
      oCotizacion.setDireccion((String)form.getDatos().get("direccion"));
      oCotizacion.setNumeroDireccion((String)form.getDatos().get("direccionNumero"));
      oCotizacion.setNumeroDepto((String)form.getDatos().get("direccionNroDepto"));
      
      //INICIO ASISTENCIA EN VIAJE
      oCotizacion.setFecIniVig((String)form.getDatos().get("fechaInicio"));
      Contratante asegurado = new Contratante();      
      asegurado.setTipoTelefono1((String)form.getDatos().get("dueTipoTelefono1"));
      numeroTelefono1 = (String)form.getDatos().get("dueCodigoTelefono1") + (String)form.getDatos().get("dueNumeroTelefono1");
      asegurado.setTelefono1(numeroTelefono1);
      asegurado.setTipoTelefono2((String)form.getDatos().get("dueTipoTelefono2"));
      numeroTelefono2 = (String)form.getDatos().get("dueCodigoTelefono2") + (String)form.getDatos().get("dueNumeroTelefono2");
      asegurado.setTelefono2(numeroTelefono2);
      asegurado.setIdRegion((String)form.getDatos().get("dueRegion"));
      asegurado.setIdComuna((String)form.getDatos().get("dueComuna"));
      asegurado.setIdCiudad((String)form.getDatos().get("dueCiudad"));
      asegurado.setCalleDireccion((String)form.getDatos().get("dueCalle"));
      asegurado.setNumeroDireccion((String)form.getDatos().get("dueNumeroCalle"));
      asegurado.setNumeroDepartamentoDireccion((String)form.getDatos().get("dueNumeroDepto"));
      oCotizacion.setAsegurado(asegurado);  
      //FIN ASISTENCIA EN VIAJE
      
      SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
      if(form instanceof IngresarDatosVidaForm) {
         IngresarDatosVidaForm vidaForm = (IngresarDatosVidaForm)form;
         Beneficiario[] beneficiarios = null;
         Beneficiario[] beneficiarios2 = null;
         int j;
         int x;
         String dia;
         String mes;
         String strFecha;
         String anyo;
         Map parentescoAux;
         Date fecha;
         String descParentesco;

         if(!idSubcategoria.equals("221")){
             if(vidaForm.getIdFilaBeneficiario() != null) {
                 j = 0;
                 beneficiarios = new Beneficiario[vidaForm.getIdFilaBeneficiario().length];

                 for(x = 0; x < vidaForm.getIdFilaBeneficiario().length; ++x) { 	 
                    if(vidaForm.getApellidoPaternoBeneficiario()[x] != null && !vidaForm.getApellidoPaternoBeneficiario()[x].equals("")) {
                       beneficiarios[j] = new Beneficiario();
                       beneficiarios[j].setApellidoMaterno(vidaForm.getApellidoMaternoBeneficiario()[x]);
                       beneficiarios[j].setApellidoPaterno(vidaForm.getApellidoPaternoBeneficiario()[x]);
                       beneficiarios[j].setDistribucion(Float.parseFloat(vidaForm.getPorcentajeBeneficiario()[x]));
                       dia = vidaForm.getDiaFechaNacimientoBeneficiario()[x];
                       mes = vidaForm.getMesFechaNacimientoBeneficiario()[x];
                       anyo = vidaForm.getAnyoFechaNacimientoBeneficiario()[x];
                       strFecha = anyo + "-" + mes + "-" + dia;
                       fecha = null;
                       fecha = formatoDelTexto.parse(strFecha);
                       beneficiarios[j].setFechaNacimiento(fecha);
                       beneficiarios[j].setNombre(vidaForm.getNombreBeneficiario()[x]);
                       beneficiarios[j].setRut(Long.parseLong(vidaForm.getRutBeneficiario()[x]));
                       beneficiarios[j].setDv(vidaForm.getDvBeneficiario()[x].charAt(0));
                       parentescoAux = oDelegate.obtenerParametro("PARENTESCO", vidaForm.getParentescoBeneficiario()[x]);
                       descParentesco = (String)parentescoAux.get("descripcion");
                       beneficiarios[j].setParentesco(vidaForm.getParentescoBeneficiario()[x]);
                       beneficiarios[j].setDescParentesco(descParentesco);
                       beneficiarios[j].setSexo(vidaForm.getSexoBeneficiario()[x]);
                       
                       ++j;
                    }
                 }
                 beneficiarios2 = new Beneficiario[j];
                 
                 for(x = 0; x < j; ++x) {
                    beneficiarios2[x] = beneficiarios[x];
                 }
              }
              oCotizacion.setBeneficiarios(beneficiarios2);
         }else{
        	 //VIAJE GRUPAL
        	 if(vidaForm.getIdFilaBeneficiario() != null) {
                 j = 0;
                 int largoArrayBeneficiarios = vidaForm.getIdFilaBeneficiario().length;
                 beneficiarios = new Beneficiario[largoArrayBeneficiarios];

                 for(x = 0; x < vidaForm.getIdFilaBeneficiario().length; ++x) { 	 
                    if(vidaForm.getApellidoPaternoBeneficiario()[x] != null && !vidaForm.getApellidoPaternoBeneficiario()[x].equals("")) {
                       beneficiarios[j] = new Beneficiario();
                       beneficiarios[j].setApellidoMaterno(vidaForm.getApellidoMaternoBeneficiario()[x]);
                       beneficiarios[j].setApellidoPaterno(vidaForm.getApellidoPaternoBeneficiario()[x]);
                       beneficiarios[j].setDistribucion(100);
                       dia = vidaForm.getDiaFechaNacimientoBeneficiario()[x];
                       mes = vidaForm.getMesFechaNacimientoBeneficiario()[x];
                       anyo = vidaForm.getAnyoFechaNacimientoBeneficiario()[x];
                       strFecha = anyo + "-" + mes + "-" + dia;
                       fecha = null;
                       fecha = formatoDelTexto.parse(strFecha);
                       beneficiarios[j].setFechaNacimiento(fecha);
                       beneficiarios[j].setNombre(vidaForm.getNombreBeneficiario()[x]);
                       beneficiarios[j].setRut(Long.parseLong(vidaForm.getRutBeneficiario()[x]));
                       beneficiarios[j].setDv(vidaForm.getDvBeneficiario()[x].charAt(0));
                       beneficiarios[j].setSexo(vidaForm.getSexoBeneficiario()[x]);
                       parentescoAux = oDelegate.obtenerParametro("PARENTESCO", vidaForm.getParentescoBeneficiario()[x]);
                       descParentesco = (String)parentescoAux.get("descripcion");
                       beneficiarios[j].setParentesco(vidaForm.getParentescoBeneficiario()[x]);
                       beneficiarios[j].setDescParentesco(descParentesco);
                       
                       ++j;
                    }
                 }
                 beneficiarios2 = new Beneficiario[j];
                 
                 for(x = 0; x < j; ++x) {
                    beneficiarios2[x] = beneficiarios[x];
                 }
              }
              oCotizacion.setBeneficiarios(beneficiarios2);
         }

      }
      return oCotizacion;
   }

   protected void verificarAutenticacion(HttpServletRequest request, HttpServletResponse response) throws AutenticacionException {
      if(!SeguridadUtil.isAutenticado(request)) {
         logger.debug("Usuario no autenticado. Autenticando.");
         SeguridadUtil.login(request, response, 1);
      } else {
         logger.debug("Usuario autenticado, se continua normalmente.");
      }

   }

}
