// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import java.util.Calendar;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.PruebaThreads;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;

public class ThreadPrueba implements Runnable {

   private String idProducto = "";
   private String idPlan = "";
   private CotizacionSeguro cotizacion = new CotizacionSeguro();
   private CotizacionDelegate delegate = new CotizacionDelegate();
   private String idProducto2 = "";
   private String idProducto3 = "";
   private String idProducto4 = "";
   private String idProducto5 = "";
   private String idProducto6 = "";
   private String idProducto7 = "";
   private Plan plan = new Plan();
   private int value;
   private int valorSuma;
   private PruebaThreads object;
   

   public ThreadPrueba() {}

   public ThreadPrueba(PruebaThreads object) {
      this.object = object;
   }

   public void run() {
	   try {    	
	         this.idProducto = this.object.getIdProducto();
	         this.idPlan = this.object.getIdPlan();
	         this.cotizacion = this.object.getCotizacion();
	         this.idProducto2 = this.object.getIdProducto2();
	         this.idProducto3 = this.object.getIdProducto3();
	         this.idProducto4 = this.object.getIdProducto4();
	         this.idProducto5 = this.object.getIdProducto5();
	         this.idProducto6 = this.object.getIdProducto6();
	         this.idProducto7 = this.object.getIdProducto7();
	         
	         System.out.println("Plan:"+this.idPlan);
	         System.out.println("ID:"+this.idProducto);
	         System.out.println("Plan:"+this.idProducto2);
	         if(!this.idProducto.equals("188") && !this.idProducto.equals("189") && !this.idProducto.equals("190") && !this.idProducto.equals("191") && !this.idProducto.equals("192") && !this.idProducto.equals("88")) {
	        	 System.out.println("No vehiculo");
	        	 this.plan = this.delegate.valorizarPlan(this.idProducto, this.cotizacion, this.idPlan);
	         } else{ 
	        	 System.out.println("Si vehiculo");
	            //this.plan = this.delegate.valorizarPlanDeducibles(this.idProducto2, this.idProducto3, this.idProducto4, this.idProducto5, this.idProducto6, this.idProducto7, this.cotizacion, this.idPlan);
	            CotizacionDelegate delegate = new CotizacionDelegate();
	            String[] pDeducibles;
	            pDeducibles = String.valueOf(delegate.obtenerParametro("PRODUCTOS", "DEDUCIBLES").get("VALOR")).split(",");
	            this.idProducto2 = pDeducibles[0];
	            this.idProducto3 = pDeducibles[1];
	            this.idProducto4 = pDeducibles[2];
	            this.idProducto5 = pDeducibles[3];
	            this.idProducto6 = pDeducibles[4];
	            this.idProducto7 = pDeducibles[5];
	         	this.plan = this.delegate.valorizarPlanDeducibles(this.idProducto2, this.idProducto3, this.idProducto4, this.idProducto5, this.idProducto6, this.idProducto7, this.cotizacion, this.idPlan);
	         }
	         
	         this.object.setData(this.plan);
	         this.object.setEstado(Boolean.valueOf(true));
	         } catch (ProcesoCotizacionException var2) {
	         this.object.setEstado(Boolean.valueOf(true));
	         var2.printStackTrace();
	      } catch (ValorizacionException var3) {
	         this.object.setEstado(Boolean.valueOf(true));
	         var3.printStackTrace();
	      }
   }
}
