// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivilEnum;
import cl.cencosud.acv.common.Rama;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.Subcategoria;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCotizacionAction extends Action {

	private static final String TIPO_VEHICULO = "cl.cencosud.asesorcotizador.tipo.codigo.vehiculo";
	private static final String TIPO_SALUD = "cl.cencosud.asesorcotizador.tipo.codigo.salud";
	private static final String TIPO_VIDA = "cl.cencosud.asesorcotizador.tipo.codigo.vida";
	private static final String TIPO_HOGAR = "cl.cencosud.asesorcotizador.tipo.codigo.hogar";
	private static final String TIPO_FRAUDE = "cl.cencosud.asesorcotizador.tipo.codigo.fraude";
	private static final String TIPO_OTROS = "cl.cencosud.asesorcotizador.tipo.codigo.otros";
	private static final String TIPO_DEVOLUCION = "cl.cencosud.asesorcotizador.tipo.codigo.devolucion";
	private static final String TIPO_MOTO = "cl.cencosud.asesorcotizador.tipo.codigo.moto";
	private static final String LOGIN_USUARIO = "cl.tinet.common.seguridad.USUARIO_CONECTADO";
	private static Log logger = LogFactory
			.getLog(DesplegarCotizacionAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String forward = "";
		String tipoRama = "";
		String idSubcategoria = "";
		String idTipoRama = "";
		String token = null;
		String idSolicitud = request.getParameter("id_solicitud");
		CotizacionDelegate delegate = new CotizacionDelegate();
		logger.info("idSolicitud: " + idSolicitud);
		if (idSolicitud != null && !"".equals(idSolicitud)) {
			Solicitud subcategoriaGetIdTipo = delegate
					.obtenerDatosSolicitud(Long.parseLong(idSolicitud));
			tipoRama = subcategoriaGetIdTipo.getId_rama();
			long session = delegate.obtenerSubcategoriaAsociada(
					subcategoriaGetIdTipo.getId_producto(), tipoRama);
			idSubcategoria = String.valueOf(session);
			request.setAttribute("retomaCotizacion", subcategoriaGetIdTipo);
			logger.info("Requiere Vitrineo: NO");
			request.setAttribute("vitrineo", "no");
		} else {
			tipoRama = request.getParameter("idRama");
			idSubcategoria = request.getParameter("idSubcategoria");
			token = request.getParameter("token");
			logger.info("No Requiere Vitrineo");
			request.setAttribute("vitrineo", "no");
			if (request.getParameter("idTipo") != null) {
				idTipoRama = request.getParameter("idTipo").toString();
			} else {
				idTipoRama = "0";
			}
		}

		if (idTipoRama.equalsIgnoreCase("0")
				&& delegate.obtenerSubcategoriaPorId(
						Long.parseLong(idSubcategoria)).getIdTipo() != 0) {
			idTipoRama = String.valueOf(delegate.obtenerSubcategoriaPorId(
					Long.parseLong(idSubcategoria)).getIdTipo());
		}

		delegate.obtenerSubcategoriaPorId(Long.parseLong(idSubcategoria));
		HttpSession session1 = request.getSession();
		session1.setAttribute("idRama", tipoRama);
		session1.setAttribute("idSubcategoria", idSubcategoria);
		session1.setAttribute("idTipo", idTipoRama);
		session1.setAttribute("claveVit", token);
		CotizacionDelegate oDelegate = new CotizacionDelegate();
		Subcategoria subcategoria = oDelegate.obtenerSubcategoriaPorId(Long
				.parseLong(idSubcategoria));
		if (subcategoria != null) {
			session1.setAttribute("subcategoriaDesc",
					subcategoria.getTituloSubcategoria());
		}

		Rama ramaSeleccionada = oDelegate.obtenerRamaPorId(Long
				.parseLong(tipoRama));
		if (ramaSeleccionada != null) {
			session1.setAttribute("ramaDescripcion",
					ramaSeleccionada.getTitulo_rama());
		}

		if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_VEHICULO))) {
			forward = "cotizadorVehiculo";
			request.getSession().setAttribute("hiddCaptcha", 1);
		} else if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_SALUD))) {
			forward = "cotizadorSalud";
		} else if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_VIDA))) {
			forward = "cotizadorVida";
		} else if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_HOGAR))) {
			forward = "cotizadorHogar";
		} else if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_FRAUDE))) {
			forward = "cotizadorFraude";
		} else if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_OTROS))) {
			forward = "cotizadorOtros";
		} else if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_DEVOLUCION))) {
			forward = "cotizadorDevolucion";
		}else if (tipoRama.equals(ACVConfig.getInstance().getString(
				TIPO_MOTO))) {
			forward = "cotizadorMoto";
		}

		if (request.getSession().getAttribute("datosCotizacion") != null) {
			request.getSession().removeAttribute("datosCotizacion");
		}

		if (request.getSession().getAttribute("datosPlan") != null) {
			request.getSession().removeAttribute("datosPlan");
		}

		return mapping.findForward(forward);
	}

	protected HashMap setearFormularioDatosContratante(Contratante contratante,
			String token) {
		HashMap datos = new HashMap();
		datos.put("rut", String.valueOf(contratante.getRut()));
		datos.put("dv", String.valueOf(contratante.getDv()));
		datos.put("nombre", contratante.getNombre());
		datos.put("apellidoPaterno", contratante.getApellidoPaterno());
		datos.put("apellidoMaterno", contratante.getApellidoMaterno());
		datos.put("telefono1", contratante.getTelefono1());
		datos.put("email", contratante.getEmail());
		datos.put("sexo", contratante.getSexo());
		return datos;
	}

	protected HashMap obtenerDatosClienteSession(HttpServletRequest request)
			throws NumberFormatException, RemoteException {
		HashMap datos = null;
		HttpSession session = request.getSession();
		logger.info("la clave es: "
				+ (String) request.getSession().getAttribute("claveVitrineo"));
		Parametro[] regionDuenyo;
		Parametro[] comunaDuenyo1;
		if (session != null
				&& session
						.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO") != null) {
			datos = new HashMap();
			UsuarioExterno claveVitrineo1 = (UsuarioExterno) session
					.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO");
			datos.put("rut", claveVitrineo1.getRut_cliente());
			datos.put("dv", claveVitrineo1.getDv_cliente());
			datos.put("nombre", claveVitrineo1.getNombre());
			datos.put("apellidoPaterno", claveVitrineo1.getApellido_paterno());
			datos.put("apellidoMaterno", claveVitrineo1.getApellido_materno());
			logger.info("rut de cliente: " + claveVitrineo1.getRut_cliente());
			CotizacionDelegate ramaVitrineo1 = new CotizacionDelegate();
			logger.info("valor de consulta: "
					+ ramaVitrineo1.obtenerRutSinCaptcha(Long
							.parseLong(claveVitrineo1.getRut_cliente())));
			if (1 == ramaVitrineo1.obtenerRutSinCaptcha(Long
					.parseLong(claveVitrineo1.getRut_cliente()))) {
				byte delegateVitrineo2 = 0;
				session.setAttribute("vaCaptcha",
						Integer.valueOf(delegateVitrineo2));
				logger.error("valor de vacaptcha: " + delegateVitrineo2);
			}

			String delegateVitrineo1 = claveVitrineo1.getTelefono_1();
			if (delegateVitrineo1 != null) {
				delegateVitrineo1 = delegateVitrineo1.trim();
				String datosVitrineo1 = delegateVitrineo1.substring(0, 1);
				if (!datosVitrineo1.equals("2") && !datosVitrineo1.equals("9")
						&& !datosVitrineo1.equals("8")
						&& !datosVitrineo1.equals("7")) {
					datos.put("codigoTelefono",
							delegateVitrineo1.substring(0, 2));
					datos.put(
							"numeroTelefono",
							delegateVitrineo1.substring(2,
									delegateVitrineo1.length()));
				} else {
					datos.put("codigoTelefono", "0" + datosVitrineo1);
					datos.put(
							"numeroTelefono",
							delegateVitrineo1.substring(1,
									delegateVitrineo1.length()));
				}
			}

			datos.put("tipoTelefono", claveVitrineo1.getTipo_telefono_1());
			datos.put("email", claveVitrineo1.getEmail());
			datos.put("estadoCivil", claveVitrineo1.getEstado_civil());
			datos.put(
					"estadoCivilDesc",
					EstadoCivilEnum.valueOf(
							"_" + claveVitrineo1.getEstado_civil())
							.getDescripcion());
			datos.put("sexo", claveVitrineo1.getSexo());
			Calendar datosVitrineo2 = Calendar.getInstance();
			Date parametro1 = claveVitrineo1.getFecha_nacimiento();
			datosVitrineo2.setTime(parametro1);
			logger.info(claveVitrineo1.getFecha_nacimiento());
			datos.put("diaFechaNacimiento",
					String.valueOf(datosVitrineo2.get(5)));
			logger.info("dia-nacimiento:" + datosVitrineo2.get(5));
			datos.put("mesFechaNacimiento",
					String.valueOf(datosVitrineo2.get(2) + 1));
			logger.info("mes-nacimiento:" + datosVitrineo2.get(3));
			datos.put("anyoFechaNacimiento",
					String.valueOf(datosVitrineo2.get(1)));
			logger.info("años:" + datosVitrineo2.get(1));
			datos.put("maxDias",
					String.valueOf(datosVitrineo2.getActualMaximum(5)));
			logger.info("max-dias:" + datosVitrineo2.getActualMaximum(5));
			CotizacionDelegate comunaVitrineoDuenyo1 = new CotizacionDelegate();
			Map comunaDuenyo2 = comunaVitrineoDuenyo1.obtenerParametro("SEXO",
					claveVitrineo1.getSexo());
			datos.put("sexoDesc", comunaDuenyo2.get("NOMBRE").toString());
			regionDuenyo = comunaVitrineoDuenyo1
					.obtenerComunaPorCiudad(claveVitrineo1.getId_ciudad());
			if (regionDuenyo != null && regionDuenyo.length > 0) {
				comunaDuenyo1 = comunaVitrineoDuenyo1
						.obtenerRegionPorComuna(regionDuenyo[0].getId());
				datos.put("comunaResidenciaCte", regionDuenyo[0].getId());
				if (comunaDuenyo1 != null && comunaDuenyo1.length > 0) {
					datos.put("regionResidenciaCte", comunaDuenyo1[0].getId());
				}
			}
		} else if (session != null
				&& session
						.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO") == null) {
			datos = new HashMap();
			String claveVitrineo = (String) request.getSession().getAttribute(
					"claveVit");
			String ramaVitrineo = String.valueOf(request.getSession()
					.getAttribute("idRama"));
			logger.info("rama es: " + ramaVitrineo);
			CotizacionDelegate delegateVitrineo = new CotizacionDelegate();
			if (claveVitrineo != null) {
				Map datosVitrineo = delegateVitrineo
						.obtenerDatosVitrineo(claveVitrineo);
				Map parametro;
				String comunaVitrineoDuenyo;
				Parametro[] comunaDuenyo;
				if (ramaVitrineo.equalsIgnoreCase("1")) {
					datos.put("rut", (String) datosVitrineo.get("rutVitrineo"));
					datos.put("dv", (String) datosVitrineo.get("dvVitrineo"));
					datos.put("nombre",
							(String) datosVitrineo.get("nombreCliente"));
					datos.put("apellidoPaterno",
							(String) datosVitrineo.get("apellidoPaterno"));
					datos.put("apellidoMaterno",
							(String) datosVitrineo.get("apellidoMaterno"));
					datos.put("codigoTelefono",
							(String) datosVitrineo.get("codTelefono"));
					datos.put("numeroTelefono",
							(String) datosVitrineo.get("telefono"));
					datos.put("tipoTelefono",
							(String) datosVitrineo.get("tipoTelefono"));
					datos.put("email", (String) datosVitrineo.get("mail"));
					datos.put("estadoCivil",
							(String) datosVitrineo.get("estadoCivil"));
					datos.put(
							"estadoCivilDesc",
							EstadoCivilEnum.valueOf(
									"_"
											+ (String) datosVitrineo
													.get("estadoCivil"))
									.getDescripcion());
					datos.put("sexo", (String) datosVitrineo.get("sexo"));
					datos.put("diaFechaNacimiento",
							(String) datosVitrineo.get("diaNac"));
					datos.put("mesFechaNacimiento",
							(String) datosVitrineo.get("mesNac"));
					datos.put("anyoFechaNacimiento",
							(String) datosVitrineo.get("anyoNac"));
					parametro = delegateVitrineo.obtenerParametro("SEXO",
							(String) datosVitrineo.get("sexo"));
					datos.put("sexoDesc", parametro.get("NOMBRE").toString());
					comunaVitrineoDuenyo = datosVitrineo.get("comuna")
							.toString();
					comunaDuenyo = delegateVitrineo
							.obtenerComunaPorCiudad(comunaVitrineoDuenyo);
					if (comunaDuenyo != null && comunaDuenyo.length > 0) {
						regionDuenyo = delegateVitrineo
								.obtenerRegionPorComuna(comunaDuenyo[0].getId());
						datos.put("comunaResidenciaCte",
								comunaDuenyo[0].getId());
						if (regionDuenyo != null && regionDuenyo.length > 0) {
							datos.put("regionResidenciaCte",
									regionDuenyo[0].getId());
						}
					}

					datos.put("edadConductor",
							(String) datosVitrineo.get("edadConductor"));
					if (Boolean.parseBoolean((String) datosVitrineo
							.get("esDuenyo"))) {
						datos.put("contratanteEsDuenyo",
								(String) datosVitrineo.get("esDuenyo"));
					} else {
						datos.put("contratanteEsDuenyo",
								(String) datosVitrineo.get("esDuenyo"));
						datos.put("rutDuenyo",
								(String) datosVitrineo.get("rutDuenyo"));
						datos.put("dvDuenyo",
								(String) datosVitrineo.get("dvDuenyo"));
						datos.put("nombreDuenyo",
								(String) datosVitrineo.get("nombreDuenyo"));
						datos.put("apellidoPaternoDuenyo",
								(String) datosVitrineo
										.get("apellidoPaternoDuenyo"));
						datos.put("apellidoMaternoDuenyo",
								(String) datosVitrineo
										.get("apellidoMaternoDuenyo"));
						String regionDuenyo2 = datosVitrineo.get(
								"comunaResidenciaDuenyo").toString();
						comunaDuenyo1 = delegateVitrineo
								.obtenerComunaPorCiudad(regionDuenyo2);
						if (comunaDuenyo1 != null && comunaDuenyo1.length > 0) {
							Parametro[] regionDuenyo1 = delegateVitrineo
									.obtenerRegionPorComuna(comunaDuenyo1[0]
											.getId());
							datos.put("comunaResidenciaDuenyo",
									comunaDuenyo1[0].getId());
							if (regionDuenyo1 != null
									&& regionDuenyo1.length > 0) {
								datos.put("regionResidenciaDuenyo",
										regionDuenyo1[0].getId());
							}
						}

						datos.put("diaNacimientoDuenyo",
								(String) datosVitrineo.get("diaNacDuenyo"));
						datos.put("mesNacimientoDuenyo",
								(String) datosVitrineo.get("mesNacDuenyo"));
						datos.put("anyoNacimientoDuenyo",
								(String) datosVitrineo.get("anyoNacDuenyo"));
						datos.put("estadoCivilDuenyo",
								(String) datosVitrineo.get("estadoCivilDuenyo"));
						datos.put("sexoDuenyo",
								(String) datosVitrineo.get("sexoDuenyo"));
					}

					datos.put("tipoVehiculo", datosVitrineo.get("tipoVehiculo")
							.toString());
					datos.put("marcaVehiculo",
							datosVitrineo.get("marcaVehiculo").toString());
					datos.put("modeloVehiculo",
							datosVitrineo.get("modeloVehiculo").toString());
					datos.put("anyoVehiculo", datosVitrineo.get("anyoVehiculo")
							.toString());
					datos.put("numeroPuertas", datosVitrineo.get("numPuertas")
							.toString());
				} else if (ramaVitrineo.equalsIgnoreCase("2")) {
					datos.put("rut", (String) datosVitrineo.get("rutVitrineo"));
					datos.put("dv", (String) datosVitrineo.get("dvVitrineo"));
					datos.put("nombre",
							(String) datosVitrineo.get("nombreCliente"));
					datos.put("apellidoPaterno",
							(String) datosVitrineo.get("apellidoPaterno"));
					datos.put("apellidoMaterno",
							(String) datosVitrineo.get("apellidoMaterno"));
					datos.put("codigoTelefono",
							(String) datosVitrineo.get("codTelefono"));
					datos.put("numeroTelefono",
							(String) datosVitrineo.get("telefono"));
					datos.put("tipoTelefono",
							(String) datosVitrineo.get("tipoTelefono"));
					datos.put("email", (String) datosVitrineo.get("mail"));
					datos.put("diaFechaNacimiento",
							(String) datosVitrineo.get("diaNac"));
					datos.put("mesFechaNacimiento",
							(String) datosVitrineo.get("mesNac"));
					datos.put("anyoFechaNacimiento",
							(String) datosVitrineo.get("anyoNac"));
					datos.put("estadoCivil",
							(String) datosVitrineo.get("estadoCivil"));
					datos.put(
							"estadoCivilDesc",
							EstadoCivilEnum.valueOf(
									"_"
											+ (String) datosVitrineo
													.get("estadoCivil"))
									.getDescripcion());
					datos.put("sexo", (String) datosVitrineo.get("sexo"));
					parametro = delegateVitrineo.obtenerParametro("SEXO",
							(String) datosVitrineo.get("sexo"));
					datos.put("sexoDesc", parametro.get("NOMBRE").toString());
					if (Boolean.parseBoolean((String) datosVitrineo
							.get("esDuenyo"))) {
						datos.put("contratanteEsDuenyo",
								(String) datosVitrineo.get("esDuenyo"));
					} else {
						datos.put("contratanteEsDuenyo",
								(String) datosVitrineo.get("esDuenyo"));
						datos.put("rutDuenyo",
								(String) datosVitrineo.get("rutDuenyo"));
						datos.put("dvDuenyo",
								(String) datosVitrineo.get("dvDuenyo"));
						datos.put("nombreDuenyo",
								(String) datosVitrineo.get("nombreDuenyo"));
						datos.put("apellidoPaternoDuenyo",
								(String) datosVitrineo
										.get("apellidoPaternoDuenyo"));
						datos.put("apellidoMaternoDuenyo",
								(String) datosVitrineo
										.get("apellidoMaternoDuenyo"));
					}

					comunaVitrineoDuenyo = datosVitrineo.get(
							"comunaResidenciaDuenyo").toString();
					comunaDuenyo = delegateVitrineo
							.obtenerComunaPorCiudad(comunaVitrineoDuenyo);
					if (comunaDuenyo != null && comunaDuenyo.length > 0) {
						regionDuenyo = delegateVitrineo
								.obtenerRegionPorComuna(comunaDuenyo[0].getId());
						datos.put("comunaResidenciaDuenyo",
								comunaDuenyo[0].getId());
						if (regionDuenyo != null && regionDuenyo.length > 0) {
							datos.put("regionResidenciaDuenyo",
									regionDuenyo[0].getId());
						}
					}

					datos.put("ciudadResidenciaDuenyo",
							(String) datosVitrineo.get("ciudadHog"));
					datos.put("direccion",
							(String) datosVitrineo.get("direccionHog"));
					datos.put("numeroDireccion",
							(String) datosVitrineo.get("numDirHog"));
					datos.put("numeroDepto",
							(String) datosVitrineo.get("numDeptoHog"));
					datos.put("antiguedadVivienda",
							(String) datosVitrineo.get("anyoHog"));
				} else if (ramaVitrineo.equalsIgnoreCase("3")) {
					datos.put("rut", (String) datosVitrineo.get("rutVitrineo"));
					datos.put("dv", (String) datosVitrineo.get("dvVitrineo"));
					datos.put("nombre",
							(String) datosVitrineo.get("nombreCliente"));
					datos.put("apellidoPaterno",
							(String) datosVitrineo.get("apellidoPaterno"));
					datos.put("apellidoMaterno",
							(String) datosVitrineo.get("apellidoMaterno"));
					datos.put("codigoTelefono",
							(String) datosVitrineo.get("codTelefono"));
					datos.put("numeroTelefono",
							(String) datosVitrineo.get("telefono"));
					datos.put("tipoTelefono",
							(String) datosVitrineo.get("tipoTelefono"));
					datos.put("email", (String) datosVitrineo.get("mail"));
					datos.put("estadoCivil",
							(String) datosVitrineo.get("estadoCivil"));
					datos.put(
							"estadoCivilDesc",
							EstadoCivilEnum.valueOf(
									"_"
											+ (String) datosVitrineo
													.get("estadoCivil"))
									.getDescripcion());
					datos.put("sexo", (String) datosVitrineo.get("sexo"));
					parametro = delegateVitrineo.obtenerParametro("SEXO",
							(String) datosVitrineo.get("sexo"));
					datos.put("sexoDesc", parametro.get("NOMBRE").toString());
					datos.put("diaFechaNacimiento",
							(String) datosVitrineo.get("diaNac"));
					datos.put("mesFechaNacimiento",
							(String) datosVitrineo.get("mesNac"));
					datos.put("anyoFechaNacimiento",
							(String) datosVitrineo.get("anyoNac"));
				} else if (ramaVitrineo.equalsIgnoreCase("4")) {
					datos.put("rut", (String) datosVitrineo.get("rutVitrineo"));
					datos.put("dv", (String) datosVitrineo.get("dvVitrineo"));
					datos.put("nombre",
							(String) datosVitrineo.get("nombreCliente"));
					datos.put("apellidoPaterno",
							(String) datosVitrineo.get("apellidoPaterno"));
					datos.put("apellidoMaterno",
							(String) datosVitrineo.get("apellidoMaterno"));
					datos.put("codigoTelefono",
							(String) datosVitrineo.get("codTelefono"));
					datos.put("numeroTelefono",
							(String) datosVitrineo.get("telefono"));
					datos.put("tipoTelefono",
							(String) datosVitrineo.get("tipoTelefono"));
					datos.put("email", (String) datosVitrineo.get("mail"));
					datos.put("estadoCivil",
							(String) datosVitrineo.get("estadoCivil"));
					datos.put(
							"estadoCivilDesc",
							EstadoCivilEnum.valueOf(
									"_"
											+ (String) datosVitrineo
													.get("estadoCivil"))
									.getDescripcion());
					datos.put("sexo", (String) datosVitrineo.get("sexo"));
					parametro = delegateVitrineo.obtenerParametro("SEXO",
							(String) datosVitrineo.get("sexo"));
					datos.put("sexoDesc", parametro.get("NOMBRE").toString());
					datos.put("diaFechaNacimiento",
							(String) datosVitrineo.get("diaNac"));
					datos.put("mesFechaNacimiento",
							(String) datosVitrineo.get("mesNac"));
					datos.put("anyoFechaNacimiento",
							(String) datosVitrineo.get("anyoNac"));
				} else if (ramaVitrineo.equalsIgnoreCase("5")) {
					datos.put("rut", (String) datosVitrineo.get("rutVitrineo"));
					datos.put("dv", (String) datosVitrineo.get("dvVitrineo"));
					datos.put("nombre",
							(String) datosVitrineo.get("nombreCliente"));
					datos.put("apellidoPaterno",
							(String) datosVitrineo.get("apellidoPaterno"));
					datos.put("apellidoMaterno",
							(String) datosVitrineo.get("apellidoMaterno"));
					datos.put("codigoTelefono",
							(String) datosVitrineo.get("codTelefono"));
					datos.put("numeroTelefono",
							(String) datosVitrineo.get("telefono"));
					datos.put("tipoTelefono",
							(String) datosVitrineo.get("tipoTelefono"));
					datos.put("email", (String) datosVitrineo.get("mail"));
					datos.put("estadoCivil",
							(String) datosVitrineo.get("estadoCivil"));
					datos.put(
							"estadoCivilDesc",
							EstadoCivilEnum.valueOf(
									"_"
											+ (String) datosVitrineo
													.get("estadoCivil"))
									.getDescripcion());
					datos.put("sexo", (String) datosVitrineo.get("sexo"));
					parametro = delegateVitrineo.obtenerParametro("SEXO",
							(String) datosVitrineo.get("sexo"));
					datos.put("sexoDesc", parametro.get("NOMBRE").toString());
					datos.put("diaFechaNacimiento",
							(String) datosVitrineo.get("diaNac"));
					datos.put("mesFechaNacimiento",
							(String) datosVitrineo.get("mesNac"));
					datos.put("anyoFechaNacimiento",
							(String) datosVitrineo.get("anyoNac"));
				} else if (ramaVitrineo.equalsIgnoreCase("6")) {
					datos.put("rut", (String) datosVitrineo.get("rutVitrineo"));
					datos.put("dv", (String) datosVitrineo.get("dvVitrineo"));
					datos.put("nombre",
							(String) datosVitrineo.get("nombreCliente"));
					datos.put("apellidoPaterno",
							(String) datosVitrineo.get("apellidoPaterno"));
					datos.put("apellidoMaterno",
							(String) datosVitrineo.get("apellidoMaterno"));
					datos.put("codigoTelefono",
							(String) datosVitrineo.get("codTelefono"));
					datos.put("numeroTelefono",
							(String) datosVitrineo.get("telefono"));
					datos.put("tipoTelefono",
							(String) datosVitrineo.get("tipoTelefono"));
					datos.put("email", (String) datosVitrineo.get("mail"));
					datos.put("estadoCivil",
							(String) datosVitrineo.get("estadoCivil"));
					datos.put(
							"estadoCivilDesc",
							EstadoCivilEnum.valueOf(
									"_"
											+ (String) datosVitrineo
													.get("estadoCivil"))
									.getDescripcion());
					datos.put("sexo", (String) datosVitrineo.get("sexo"));
					parametro = delegateVitrineo.obtenerParametro("SEXO",
							(String) datosVitrineo.get("sexo"));
					datos.put("sexoDesc", parametro.get("NOMBRE").toString());
					datos.put("diaFechaNacimiento",
							(String) datosVitrineo.get("diaNac"));
					datos.put("mesFechaNacimiento",
							(String) datosVitrineo.get("mesNac"));
					datos.put("anyoFechaNacimiento",
							(String) datosVitrineo.get("anyoNac"));
				}
			}
		}

		return datos;
	}

}
