// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:21
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionSaludForm;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import cl.tinet.common.util.validate.ValidacionUtil;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class IngresarDatosHogarForm extends BuilderActionFormBaseCOT {

   private static final long serialVersionUID = -9041007713815428966L;


   public InputStream getValidationRules(HttpServletRequest request) {
      return CotizacionSaludForm.class.getResourceAsStream("resource/validation-ingresaDatosVida.xml");
   }

   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
      Usuario usuario = SeguridadUtil.getUsuario(request);
      ActionErrors errores = new ActionErrors();
      if(!((String)this.getDatos().get("cteTipoTelefono1")).equals("") && !((String)this.getDatos().get("cteCodigoTelefono1")).equals("") && !((String)this.getDatos().get("cteNumeroTelefono1")).equals("")) {
         try {
            if(Long.parseLong((String)this.getDatos().get("cteNumeroTelefono1")) <= 0L) {
               errores.add("datos.telefono1", new ActionMessage("errors.validacion"));
               errores.add("datos.telefono2", new ActionMessage("errors.validacion"));
               errores.add("datos.telefono3", new ActionMessage("errors.validacion"));
            }
         } catch (NumberFormatException var10) {
            errores.add("datos.telefono1", new ActionMessage("errors.validacion"));
            errores.add("datos.telefono2", new ActionMessage("errors.validacion"));
            errores.add("datos.telefono3", new ActionMessage("errors.validacion"));
         }
      } else {
         errores.add("datos.telefono1_1", new ActionMessage("errors.required"));
         errores.add("datos.telefono1_2", new ActionMessage("errors.required"));
         errores.add("datos.telefono1_3", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteRegion")).equals("")) {
         errores.add("cteRegion", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteComuna")).equals("")) {
         errores.add("cteComuna", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteCiudad")).equals("")) {
         errores.add("cteCiudad", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteNumeroCalle")).equals("") || ((String)this.getDatos().get("cteCalle")).equals("")) {
         errores.add("datos.calle1", new ActionMessage("errors.required"));
         errores.add("datos.nro1", new ActionMessage("errors.required"));
         errores.add("datos.depto1", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("cteNumeroCalle")).equals("")) {
         try {
            Long.parseLong((String)this.getDatos().get("cteNumeroCalle"));
         } catch (NumberFormatException var9) {
            errores.add("datos.calle1", new ActionMessage("errors.validacion"));
            errores.add("datos.nro1", new ActionMessage("errors.validacion"));
            errores.add("datos.depto1", new ActionMessage("errors.validacion"));
         }
      }

      // Validar si corresponde a compra sin registro
      if(request.getParameter("sinRegistro") == null) {
          if(usuario == null && request.getParameter("password") == null || String.valueOf(request.getParameter("password")).equals("")) {
              errores.add("clavepaso2", new ActionMessage("errors.required"));
           }
      }

      CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      if(cotizacionSeguro != null && !cotizacionSeguro.isClienteEsAsegurado()) {
         if(!((String)this.getDatos().get("asegDiaNac")).equals("") && !((String)this.getDatos().get("asegMesNac")).equals("") && !((String)this.getDatos().get("asegAnyoNac")).equals("")) {
            String ne = ((String)this.getDatos().get("asegDiaNac")).length() < 2?"0" + (String)this.getDatos().get("asegDiaNac"):(String)this.getDatos().get("asegDiaNac");
            ne = ne + (((String)this.getDatos().get("asegMesNac")).length() < 2?"0" + (String)this.getDatos().get("asegMesNac"):(String)this.getDatos().get("asegMesNac"));
            ne = ne + (String)this.getDatos().get("asegAnyoNac");
            if(!ValidacionUtil.isFechaValida(ne, "ddMMyyyy")) {
               errores.add("diasFecha", new ActionMessage("errors.validacion"));
               errores.add("mesFecha", new ActionMessage("errors.validacion"));
               errores.add("anyoFecha", new ActionMessage("errors.validacion"));
            }
         } else {
            errores.add("diasFecha", new ActionMessage("errors.required"));
            errores.add("mesFecha", new ActionMessage("errors.required"));
            errores.add("anyoFecha", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("estadoCivil")).equals("")) {
            errores.add("datos.estadoCivil", new ActionMessage("errors.required"));
         }

         if(this.getDatos().get("asegSexo") == null) {
            errores.add("sexo1", new ActionMessage("errors.required"));
            errores.add("sexo2", new ActionMessage("errors.required"));
         }

         if(!((String)this.getDatos().get("asegTipoTelefono1")).equals("") && !((String)this.getDatos().get("asegCodigoTelefono1")).equals("") && !((String)this.getDatos().get("asegNumeroTelefono1")).equals("")) {
            try {
               if(Long.parseLong((String)this.getDatos().get("cteNumeroTelefono1")) <= 0L) {
                  errores.add("datos.asegFormatTel", new ActionMessage("errors.validacion"));
                  errores.add("datos.asegTelefono1_1", new ActionMessage("errors.validacion"));
                  errores.add("datos.asegTelefono1_2", new ActionMessage("errors.validacion"));
                  errores.add("datos.asegTelefono1_3", new ActionMessage("errors.validacion"));
               }
            } catch (NumberFormatException var8) {
               errores.add("datos.asegFormatTel", new ActionMessage("errors.validacion"));
               errores.add("datos.asegTelefono1_1", new ActionMessage("errors.validacion"));
               errores.add("datos.asegTelefono1_2", new ActionMessage("errors.validacion"));
               errores.add("datos.asegTelefono1_3", new ActionMessage("errors.validacion"));
            }
         } else {
            errores.add("datos.asegTelefono1_1", new ActionMessage("errors.required"));
            errores.add("datos.asegTelefono1_2", new ActionMessage("errors.required"));
            errores.add("datos.asegTelefono1_3", new ActionMessage("errors.required"));
            errores.add("datos.asegNoTel", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("asegRegion")).equals("")) {
            errores.add("asegRegion", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("asegComuna")).equals("")) {
            errores.add("asegComuna", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("asegCiudad")).equals("")) {
            errores.add("asegCiudad", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("asegCalle")).equals("") || ((String)this.getDatos().get("asegNumeroCalle")).equals("")) {
            errores.add("asegCalle", new ActionMessage("errors.required"));
            errores.add("asegNumeroCalle", new ActionMessage("errors.required"));
            errores.add("asegNumeroDepto", new ActionMessage("errors.required"));
         }

         if(!((String)this.getDatos().get("asegNumeroCalle")).equals("")) {
            try {
               Long.parseLong((String)this.getDatos().get("asegNumeroCalle"));
            } catch (NumberFormatException var7) {
               errores.add("asegCalle", new ActionMessage("errors.validacion"));
               errores.add("asegNumeroCalle", new ActionMessage("errors.validacion"));
               errores.add("asegNumeroDepto", new ActionMessage("errors.validacion"));
            }
         }
      }
      
      
      //INICIO Hogar vacaciones
      
      GregorianCalendar fechaHoy;
      CotizacionSeguro datosCotizacion1;
      Date fecha;
      if(this.getDatos().get("fechaInicio") != null) {
         if(((String)this.getDatos().get("fechaInicio")).equals("")) {
        	 errores.add("inputIniVig", new ActionMessage("errors.required"));
         } else {
            datosCotizacion1 = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
            SimpleDateFormat ne1 = new SimpleDateFormat("dd/MM/yyyy");
            GregorianCalendar fechaInicio1 = new GregorianCalendar();
            GregorianCalendar fechaTermino2 = new GregorianCalendar();
            GregorianCalendar fechaMaxima1 = new GregorianCalendar();
            fechaHoy = new GregorianCalendar();

            try {
               fecha = ne1.parse((String)this.getDatos().get("fechaInicio"));
               String fechaPresente1 = ne1.format(fechaHoy.getTime());
               String fechaIni = (String)this.getDatos().get("fechaInicio");
               fechaInicio1.setTime(fecha);
               fechaTermino2.setTime(fecha);
               fechaMaxima1.add(5, datosCotizacion1.getNumDiaMaxIniVig());
               //if(fechaPresente1.equals(fechaIni)) {
               //errorsOtros.add("inputIniVig", new ActionMessage("errors.fechaActual"));
               if(!fechaHoy.before(fechaInicio1) && !fechaPresente1.equals(fechaIni)) {
            	   errores.add("inputIniVig", new ActionMessage("errors.fechaAnterior"));
               } else if(!fechaInicio1.before(fechaMaxima1)) {
            	   errores.add("inputIniVig", new ActionMessage("errors.fechaSuperaMax"));
               } else {
                  ne1 = new SimpleDateFormat("ddMMyyyy");
                  fechaTermino2.add(2, 1);
                  request.getSession().setAttribute("fechaInicioVig", ne1.format(fechaInicio1.getTime()));
                  request.getSession().setAttribute("fechaTerminoVig", ne1.format(fechaTermino2.getTime()));
               }
            } catch (ParseException var18) {
            	errores.add("inputIniVig", new ActionMessage("errors.fechaErronea"));
            }
         }
      }
      
 
      //FIN Hogar Vacaciones
      
      

      request.setAttribute("cl.tinet.common.struts.validator.PROPERTY_NAMES", errores.properties());
      return errores;
   }
}
