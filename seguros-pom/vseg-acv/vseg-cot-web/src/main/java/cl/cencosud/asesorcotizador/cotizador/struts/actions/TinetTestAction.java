// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TinetTestAction extends Action {

   private static Log logger = LogFactory.getLog(TinetTestAction.class);
   public static final String USUARIO_CONECTADO_KEY = "cl.tinet.common.seguridad.USUARIO_CONECTADO";


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
      logger.info("prueba");
      String forward = "";
      String val = request.getParameter("id");
      if("tmas".equals(val)) {
         forward = this.exitoTMAS(request);
      } else if("email".equals(val)) {
         forward = this.correoExito(request);
      }

      return mapping.findForward(forward);
   }

   private String exitoTMAS(HttpServletRequest request) {
      UsuarioExterno usuario = new UsuarioExterno();
      usuario.setNombre("Francisco");
      usuario.setApellido_paterno("Mendoza");
      usuario.setApellido_materno("Cabrera");
      request.setAttribute("HTML_CAMPANNA", "http://10.7.3.44:8080/cotizador/swf/230x160.swf");
      request.getSession().setAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO", usuario);
      return "tmas-exito";
   }

   private String correoExito(HttpServletRequest request) {
      PagosDelegate delegate = new PagosDelegate();
      CotizacionSeguroVehiculo cotizacionSeguro = new CotizacionSeguroVehiculo();
      String numeroOrdenCompra = "12334";
      cotizacionSeguro.setEmail("fmendoza@tinet.cl");
      cotizacionSeguro.setNombre("Francisco");
      cotizacionSeguro.setRutCliente(1L);
      cotizacionSeguro.setCodigoProducto(122L);
      cotizacionSeguro.setFactura(new byte[0]);
      cotizacionSeguro.setCodigoPlan(741L);
      String idRama = "1";
      delegate.enviarEmail(cotizacionSeguro, numeroOrdenCompra, idRama);
      return null;
   }

}
