// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.cotizador.struts.actions.DesplegarCotizacionAction;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVehiculoForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCotizacionVehiculoAction extends
		DesplegarCotizacionAction {

	private static final Log logger = LogFactory
			.getLog(DesplegarCotizacionVehiculoAction.class);
	private static final int DIAS_MES = 31;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws BusinessException, NumberFormatException, RemoteException {
		HttpSession session = request.getSession();

		CotizacionDelegate delegate = new CotizacionDelegate();
		String idSubcategoria = "";
		if (request.getParameter("idSubcategoria") != null) {
			request.getSession().setAttribute("idSubcategoria",
					request.getParameter("idSubcategoria"));
			idSubcategoria = request.getParameter("idSubcategoria");
		} else if (request.getSession() != null
				&& request.getSession().getAttribute("idSubcategoria") != null) {
			idSubcategoria = request.getSession()
					.getAttribute("idSubcategoria").toString();
		}

		request.getSession().setAttribute("subcategoria", idSubcategoria);
		String idRama = "";
		if (request.getParameter("idRama") != null) {
			request.getSession().setAttribute("idRama",
					request.getParameter("idRama"));
			idRama = request.getParameter("idRama");
		} else if (request.getSession() != null
				&& request.getSession().getAttribute("idRama") != null) {
			idRama = request.getSession().getAttribute("idRama").toString();
		}

		String idPlan = request.getParameter("idPlan");
		if (idPlan == null) {
			if (request.getSession().getAttribute("idPlan") != null) {
				idPlan = request.getSession().getAttribute("idPlan").toString();
			} else {
				idPlan = null;
			}
		} else {
			request.getSession().setAttribute("idPlan", idPlan);
		}

		try {
			Producto[] clienteSession = delegate.obtenerProductos(
					idSubcategoria, idRama, idPlan);
			if (clienteSession != null && clienteSession.length > 0) {
				request.setAttribute("productos", clienteSession);
			}
		} catch (BusinessException var57) {
			throw new ProcesoCotizacionException(
					"cl.cencosud.ventaseguros.cotizacion.exception.PRODUCTO_NO_ENCONTRADO",
					new Object[0]);
		}
		Producto[] clienteSession = (Producto[]) null;
		byte vaCaptcha = 1;
		session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
		HashMap clienteSession1;
		if (idSubcategoria.equals("22")) {
			int solicitud = (Integer) request.getSession().getAttribute(
					"hiddCaptcha");
			logger.info("VALOR hiddCaptcha " + solicitud);
			if (solicitud == 1) {
				logger.info("ENTRA IF VALOR hiddCaptcha " + solicitud);
				clienteSession1 = obtenerDatosClienteSession(request);
			} else {
				logger.info("ENTRA ELSE VALOR hiddCaptcha " + solicitud);
				vaCaptcha = 0;
				session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
				clienteSession1 = obtenerDatosClienteSession(request);
			}
		} else {
			clienteSession1 = obtenerDatosClienteSession(request);
		}

		if (((CotizacionVehiculoForm) form).getDatos().isEmpty()
				&& clienteSession1 != null) {
			((CotizacionVehiculoForm) form).getDatos().putAll(clienteSession1);
		}

		request.getSession().setAttribute("checkId00", "1");
		request.getSession().setAttribute("checkId03", "1");
		request.getSession().setAttribute("checkId04", "1");
		request.getSession().setAttribute("checkId05", "1");
		request.getSession().setAttribute("checkId08", "1");
		request.getSession().setAttribute("checkId10", "1");
		if (request.getAttribute("vitrineo") != null
				&& !"si".equals((String) request.getAttribute("vitrineo"))) {
			logger.info("Desplegar Cotizacion Vehiculo: No se Guarda Vitrineo ...");
		} else {
			// 25-10-13 Inicio Validar que los campos no esten vacios antes de
			// parsear
			String tipoVehiculoStr = (String) ((CotizacionVehiculoForm) form)
					.getDatos().get("tipoVehiculo");
			String numPuertasStr = (String) ((CotizacionVehiculoForm) form)
					.getDatos().get("numeroPuertas");
			String idModeloVitrineoStr = (String) ((CotizacionVehiculoForm) form)
					.getDatos().get("numeroPuertas");
			String idMarcaVitrineoStr = (String) ((CotizacionVehiculoForm) form)
					.getDatos().get("marcaVehiculo");
			String productoVitrineoStr = (String) ((CotizacionVehiculoForm) form)
					.getDatos().get("producto");
			if (!tipoVehiculoStr.equals("") && !numPuertasStr.equals("")
					&& !idModeloVitrineoStr.equals("")
					&& !idMarcaVitrineoStr.equals("")
					&& (!productoVitrineoStr.equals(""))) {
				// 25-10-13 Inicio Validar que los campos no esten vacios antes
				// de parsear
				logger.info("Guardando Vitrineo");
				String solicitud = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("claveVitrineo");
				session.setAttribute("claveVitrineo", solicitud);
				String cotVehiculo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("rut");
				String vehiculo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("dv");
				String tipoTelefono = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("tipoTelefono");
				String codTelefono = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("codigoTelefono");
				String estadoCivil = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("estadoCivil");
				String sexo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("sexo");

				int tipoVehiculo = Integer
						.parseInt((String) ((CotizacionVehiculoForm) form)
								.getDatos().get("tipoVehiculo"));
				String esDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("contratanteEsDuenyo");
				int numPuertas = Integer
						.parseInt((String) ((CotizacionVehiculoForm) form)
								.getDatos().get("numeroPuertas"));
				int idModeloVitrineo = Integer
						.parseInt((String) ((CotizacionVehiculoForm) form)
								.getDatos().get("modeloVehiculo"));
				String idAnyoVitrineo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("anyoVehiculo");
				int idMarcaVitrineo = Integer
						.parseInt((String) ((CotizacionVehiculoForm) form)
								.getDatos().get("marcaVehiculo"));
				String nombre = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("nombre");
				String diaNac = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("diaFechaNacimiento");
				String mesNac = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("mesFechaNacimiento");
				String anyoNac = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("anyoFechaNacimiento");
				String telefono = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("numeroTelefono");
				String email = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("email");
				int comuna = 0;
				int region = 0;
				int rama = Integer.parseInt(idRama);
				int subcategoria = Integer.parseInt(idSubcategoria);
				int productoVitrineo = Integer
						.parseInt((String) ((CotizacionVehiculoForm) form)
								.getDatos().get("producto"));
				Object ciudadHogar = null;
				Object direccionHogar = null;
				Object numeroDireccionHogar = null;
				Object numeroDeptoHogar = null;
				Object anyoHogar = null;
				String apellidoPaterno = null;
				String apellidoMaterno = null;
				int edadConductor = 0;
				String rutDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("rutDuenyo");
				String dvDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("dvDuenyo");
				String nombreDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("nombreDuenyo");
				String apellidoPaternoDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("apellidoPaternoDuenyo");
				String apellidoMaternoDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("apellidoMaternoDuenyo");
				String diaNacimientoDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("diaFechaNacimientoDuenyo");
				String mesNacimientoDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("mesFechaNacimientoDuenyo");
				String anyoNacimientoDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("anyoFechaNacimientoDuenyo");
				String estadoCivilDuenyo = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("estadoCivilDuenyo");
				String valorComercial = (String) ((CotizacionVehiculoForm) form)
						.getDatos().get("valorComercial");
				String regionResidenciaDuenyo = null;
				String comunaResidenciaDuenyo = null;
				delegate.grabarVitrineoGeneral(solicitud, cotVehiculo,
						vehiculo, nombre, apellidoPaterno, apellidoMaterno,
						tipoTelefono, codTelefono, telefono, email, region,
						comuna, diaNac, mesNac, anyoNac, estadoCivil, sexo,
						edadConductor, tipoVehiculo, idMarcaVitrineo,
						idModeloVitrineo, idAnyoVitrineo, numPuertas, esDuenyo,
						(String) ciudadHogar, (String) direccionHogar,
						(String) numeroDireccionHogar,
						(String) numeroDeptoHogar, (String) anyoHogar, rama,
						subcategoria, productoVitrineo, rutDuenyo, dvDuenyo,
						nombreDuenyo, apellidoPaternoDuenyo,
						apellidoMaternoDuenyo, diaNacimientoDuenyo,
						mesNacimientoDuenyo, anyoNacimientoDuenyo,
						estadoCivilDuenyo, regionResidenciaDuenyo,
						comunaResidenciaDuenyo, valorComercial);
				logger.info("Vitrineo guardado");
			}
		}

		if (request.getAttribute("retomaCotizacion") != null) {
			Solicitud solicitud1 = (Solicitud) request
					.getAttribute("retomaCotizacion");
			logger.info("Se retoma cotizacion: "+ solicitud1);
			CotizacionSeguroVehiculo cotVehiculo1 = delegate
					.obtenerDatosMateriaVehiculo(solicitud1.getId_solicitud());
			solicitud1.setDatosCotizacion(cotVehiculo1);
			if (solicitud1 != null) {
				CotizacionSeguroVehiculo vehiculo1 = (CotizacionSeguroVehiculo) solicitud1
						.getDatosCotizacion();
				((BuilderActionFormBaseCOT) form).getDatos().put(
						"tipoVehiculo",
						String.valueOf(vehiculo1.getCodigoTipoVehiculo()));
				((BuilderActionFormBaseCOT) form).getDatos().put(
						"marcaVehiculo",
						String.valueOf(vehiculo1.getCodigoMarca()));
				((BuilderActionFormBaseCOT) form).getDatos().put(
						"modeloVehiculo",
						String.valueOf(vehiculo1.getCodigoModelo()));
				((BuilderActionFormBaseCOT) form).getDatos().put(
						"anyoVehiculo",
						String.valueOf(vehiculo1.getAnyoVehiculo()));
				((BuilderActionFormBaseCOT) form).getDatos().put(
						"valorComercial",
						String.valueOf(vehiculo1.getMontoAsegurado()));
				((BuilderActionFormBaseCOT) form).getDatos().put(
						"numeroPuertas",
						String.valueOf(vehiculo1.getNumeroPuertas()));
				if (solicitud1.getCliente_es_asegurado() < 1L) {
					((BuilderActionFormBaseCOT) form).getDatos().put(
							"contratanteEsDuenyo", Boolean.FALSE.toString());
				}

				((BuilderActionFormBaseCOT) form).getDatos().put("producto",
						String.valueOf(solicitud1.getId_producto()));
			}
		}

		this.obtenerParametrosFormulario(request, delegate);
		if (((CotizacionVehiculoForm) form).getDatos().get(
				"contratanteEsDuenyo") == null) {
			((CotizacionVehiculoForm) form).getDatos().put(
					"contratanteEsDuenyo", Boolean.TRUE.toString());
		}

		if (request.getParameter("idProducto") != null
				&& !request.getParameter("idProducto").equals("")) {
			((BuilderActionFormBaseCOT) form).getDatos().put("producto",
					request.getParameter("idProducto"));
		}

		if (request.getParameter("volverPaso1") != null
				&& request.getSession().getAttribute("datosVolverPaso1") != null) {
			((BuilderActionFormBaseCOT) form).getDatos().putAll(
					(HashMap) request.getSession().getAttribute(
							"datosVolverPaso1"));
			logger.info("Desplegar Cotizacion Vehiculo: Volviendo al paso 1");
			request.getSession().removeAttribute("datosVolverPaso1");
		}

		if (request.getParameter("idPlan") != null) {
			request.getSession().setAttribute("isPromocion", "true");
		} else {
			request.getSession().removeAttribute("isPromocion");
		}

		logger.info("Fin EXECUTE");
		return mapping.findForward("desplegarFormularioVehiculo");
	}

	private void obtenerParametrosFormulario(HttpServletRequest request,
			CotizacionDelegate delegate) {
		logger.info("Inicio obtenerParametros");
		
		int idTipo = 0;
		if (request.getSession().getAttribute("idTipo") != null
				&& "".equals(request.getSession().getAttribute("idTipo"))) {
			idTipo = 1;
			request.getSession()
					.setAttribute("idTipo", Integer.valueOf(idTipo));
		} else if (request.getSession().getAttribute("idTipo") != null) {
			idTipo = Integer.parseInt(request.getSession()
					.getAttribute("idTipo").toString());
		}

		// 15072015 Se reemplaza rama por tipo
		int idRamaTipo = 0;
		if (request.getParameter("idRama") != null
				&& !request.getParameter("idRama").equals("")) {
			idRamaTipo = Integer.parseInt(request.getSession()
					.getAttribute("idRama").toString());
		} else {
			idRamaTipo = 0;
		}
		
		Vehiculo[] tiposVehiculos = idRamaTipo == 0 ? delegate
				.obtenerTiposVehiculos() : delegate
				.obtenerTiposVehiculos(idRamaTipo);
		int numTiposVehiculo = tiposVehiculos.length;
		request.setAttribute("tiposVehiculo", tiposVehiculos);
		request.setAttribute("numTiposVehiculo",
				Integer.valueOf(numTiposVehiculo));
		request.setAttribute("marcasVehiculos",
				delegate.obtenerMarcasPorTipoVehiculos("1"));
		request.setAttribute("modelosVehiculos",
				delegate.obtenerModelosVehiculos("1", "1"));
		
		Actividad[] actividades = delegate.obtenerActividades();
		request.setAttribute("actividades", actividades);
		EstadoCivil[] estadosCiviles = delegate.obtenerEstadosCiviles();
		request.setAttribute("estadosCiviles", estadosCiviles);
		ParametroCotizacion[] regiones = delegate.obtenerRegiones();
		request.setAttribute("regiones", regiones);
		List tipoTelefono = delegate.obtenerParametro("TIPO_TELEFONO");
		request.setAttribute("tipoTelefono", tipoTelefono.iterator());
		List codArea = delegate.obtenerParametro("TEL_CODIGO_AREA");
		request.setAttribute("codigoArea", codArea.iterator());
		List meses = delegate.obtenerParametro("MESES");
		request.setAttribute("meses", meses);
		List anyoVehiculo = delegate.obtenerParametro("ANYOVEHICULO");
		request.setAttribute("anyoVehiculo", anyoVehiculo.iterator());
		List puertas = delegate.obtenerParametro("NRO_PUERTAS");
		request.setAttribute("puertas", puertas.iterator());
		List anyos = delegate.obtenerParametro("ANYOS");
		request.setAttribute("anyos", anyos);
		List edades = delegate.obtenerParametro("EDAD_CONDUCTOR");
		request.setAttribute("edades", edades);
		List valoresComercial = delegate.obtenerParametro("VALOR_COMERCIAL");
		request.setAttribute("valoresComercial", valoresComercial);
		logger.info("Fin obtenerParametros");
	}
}
