package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.acv.common.PatenteVehiculo;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.BuscarPatenteForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;

public class GuardarPatenteVehiculoAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
			throws Exception {
		BuscarPatenteForm patenteForm = (BuscarPatenteForm) form;
		
		if(patenteForm.getIdPatente() != null
				&& patenteForm.getIdTipoVehiculo() != null
				&& patenteForm.getMarca() != null
				&& patenteForm.getModelo() != null
				&& patenteForm.getAnio() != null){
			
			String idPatente = patenteForm.getIdPatente().toUpperCase();
			
			request.getSession().setAttribute("patente", idPatente);
			
			CotizacionDelegate delegate = new CotizacionDelegate();
			PatenteVehiculo patente = new PatenteVehiculo();
			patente.setPatente(idPatente);
			patente.setTipoVehiculo(patenteForm.getIdTipoVehiculo()); 
			patente.setMarca(patenteForm.getMarca());
			patente.setModelo(patenteForm.getModelo());
			patente.setAnio(patenteForm.getAnio());
			delegate.guardarDatosPatente(patente);
		}
		
		return null;
	}
}
