// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Cobertura;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCoberturasAction extends Action {

   public static final Log logger = LogFactory.getLog(DesplegarCoberturasAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      String idPlan = request.getParameter("idPlan");
      logger.debug("Inicio obtener coberturas (idPlan=" + idPlan + ") ...");
      if(idPlan != null && idPlan.length() > 0) {
         CotizacionDelegate delegate = new CotizacionDelegate();
         Cobertura[] coberturas = delegate.obtenerCoberturas(idPlan);
         logger.debug("Coberturas Obtenidas ...");
         request.setAttribute("coberturas", coberturas);
      }

      return mapping.findForward("desplegarCoberturas");
   }

}
