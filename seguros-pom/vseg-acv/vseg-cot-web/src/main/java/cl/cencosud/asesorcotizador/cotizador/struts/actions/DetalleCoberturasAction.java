package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import java.util.List;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.acv.common.CoberturaSeg;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;

public class DetalleCoberturasAction extends Action {
	
	private static final Log logger = LogFactory
			.getLog(DetalleCoberturasAction.class);
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CotizacionDelegate delegate = new CotizacionDelegate();
		
		/* Obtener datos desde PCBS
		String plan = request.getParameter("idPlan");
		logger.info("idPlan: " + plan);
		CoberturaSeg[] cobertura = null;
		List <CoberturaSeg> coberturas = null;
		
		if(plan != null){
			cobertura = delegate.consultarrCoberturas(new Integer(plan).intValue());
		}		
		coberturas = Arrays.asList(cobertura);	
		request.setAttribute("coberturaPlanes", coberturas);
		*/
		
		String idSubcategoria = (String)request.getSession().getAttribute("idSubcategoria");
		logger.info("idSubcategoria "+idSubcategoria);
		String idFicha = delegate.obtenerIdFicha(Integer.parseInt(idSubcategoria));
		request.getSession().setAttribute("idFicha", idFicha);
		logger.info("idFicha "+idFicha);		
	
		return mapping.findForward("continuar");
	}
}
