// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.RespuestaTBK;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.util.crypto.CryptoUtil;
import cl.tinet.common.util.resource.ResourceLeakUtil;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CerrarWebPayAction extends Action{
	private static final ACVConfig config = ACVConfig.getInstance();
	private static String NOMBRE_CORREDOR = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.NOMBRE_CORREDOR";
	private static String TIPO_RAMO = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.TIPO_RAMO";
	private static String USER_BSP = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.USER_BSP";
	private static final String[] PARAMETROS_TBK = new String[] {
		"TBK_ORDEN_COMPRA", "TBK_TIPO_TRANSACCION", "TBK_RESPUESTA",
		"TBK_MONTO", "TBK_CODIGO_AUTORIZACION", "TBK_FINAL_NUMERO_TARJETA",
		"TBK_FECHA_CONTABLE", "TBK_FECHA_TRANSACCION",
		"TBK_HORA_TRANSACCION", "TBK_ID_SESION", "TBK_ID_TRANSACCION",
		"TBK_TIPO_PAGO", "TBK_NUMERO_CUOTAS", "TBK_TASA_INTERES_MAX",
		"TBK_VCI", "TBK_MAC" };
	private static final String WEBPAY_RUTA_TEMPORAL_KEY = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WEBPAY_RUTA_TEMPORAL";
	private static final String WEBPAY_EJECUTABLE_MAC_KEY = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WEBPAY_EJECUTABLE_MAC";
	private static final String WEBPAY_VALIDAR_MAC = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WEBPAY_VALIDAR_MAC";
	private static Logger logger = Logger.getLogger(CerrarWebPayAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
	HttpServletRequest request, HttpServletResponse response) {
		logger.info("Inicio CerrarWebPayAction");
		Transaccion transaccion = new Transaccion();
		PagosDelegate delegate = new PagosDelegate();
		Enumeration<?> paramNames = request.getParameterNames();
		
		while (paramNames.hasMoreElements()) {
			String parametros = (String) paramNames.nextElement();
			logger.info(parametros + ": " + request.getParameter(parametros));
		}
		transaccion.setEstado_transaccion("ESTADO_WEBPAY");
		int idSolicitud = Integer.valueOf(request.getParameter("TBK_ID_SESION")).intValue();
		transaccion.setId_solicitud((long) idSolicitud);
		transaccion.setNumero_orden_compra(request.getParameter("TBK_ORDEN_COMPRA"));
		transaccion.setNro_tarjeta_entrada(request.getParameter("TBK_FINAL_NUMERO_TARJETA"));
		transaccion.setRespuesta_medio(request.getParameter("TBK_RESPUESTA"));
		delegate.actualizarEstadoTransaccion(transaccion);
		String respuesta = "RECHAZADO";
		PrintWriter writer1 = null;

		try {
			if ("0".equals(request.getParameter("TBK_RESPUESTA"))) {
				logger.info("PRIMER ESTADO TRX: " + respuesta);
				if (this.esMACValido(request)
						&& this.esMontoCompraValido(request)
						&& this.esTarjetaValida(request)
						&& this.grabarDatosRespuesta(request)) {
					respuesta = "ACEPTADO";
				}else{
					respuesta = "RECHAZADO";
				}
				logger.info("SEGUNDO ESTADO TRX: " + respuesta);
			}else{
				respuesta = "RECHAZADO";
			}
			logger.info("TERCER ESTADO TRX: " + respuesta);
		
			if ("ACEPTADO".equals(respuesta)) {
				boolean aceptaTrx = this.aceptaTransaccion(transaccion, request);
				logger.info("COMPRA REGISTRADA EN PCBS: " + aceptaTrx);
				if (!aceptaTrx) {
					respuesta = "RECHAZADO";
					logger.info("RECHAZADO POR PCBS");
				}
			}else{
				respuesta = "RECHAZADO";
				logger.info("TRANSACCION RECHAZADA");
			}
		
			transaccion.setEstado_transaccion("RESPUESTA A WEBPAY: "+respuesta);
			delegate.actualizarEstadoTransaccion(transaccion);
			writer1 = response.getWriter();
			writer1.write(respuesta);
			writer1.flush();
		} catch (Exception e) {
			logger.error("Error enviando respuesta a transbank: ", e);
		} finally {
			ResourceLeakUtil.close(writer1);
			logger.info("FIN CerrarWebPayAction");
		}
		return null;
	}

	private boolean esMACValido(HttpServletRequest request) {
		boolean valido = false;
		StringBuilder builder = new StringBuilder();
		String union = "";
		String validarMac;
		String ejecutable;
		
		for (int archivo = 0; archivo < PARAMETROS_TBK.length; ++archivo) {
			validarMac = PARAMETROS_TBK[archivo];
			ejecutable = request.getParameter(validarMac);
			if (ejecutable != null) {
				builder.append(union).append(validarMac).append("=")
				.append(ejecutable);
				union = "&";
			}
		}

		String var21 = this.crearArchivo(request, builder);
		validarMac = config.getString(WEBPAY_VALIDAR_MAC);
		if (var21 != null && !validarMac.equals("N")) {
			ejecutable = config.getString(WEBPAY_EJECUTABLE_MAC_KEY);
			String llamada = ejecutable + " " + var21;
			InputStreamReader reader = null;
	
			try {
				logger.info("Archivo: " + llamada);
				Process ie = Runtime.getRuntime().exec(llamada);
				int exitCode = ie.waitFor();
				if (exitCode == 0) {
					logger.info("El proceso se ejecuto exitosamente");
					reader = new InputStreamReader(ie.getInputStream());
					int dato = reader.read();
			
					for (builder = new StringBuilder(); dato != -1; dato = reader
							.read()) {
						builder.append((char) dato);
					}
			
					String resultado = builder.toString().trim();
					valido = "CORRECTO".equals(resultado);
				}
			}catch(IOException var18){
				logger.error("Error durante validacion de MAC.", var18);
			} catch (InterruptedException var19){
				logger.error("Error durante validacion de MAC.", var19);
			}finally{
				ResourceLeakUtil.close(reader);
			}
		}else if(validarMac.equals("N")) {
			logger.error("Validacion de MAC inactiva. Por favor activar.");
			valido = true;
		}
		logger.info("RESULTADO VALIDACION MAC: " + valido);
		return valido;
	}

	private String crearArchivo(HttpServletRequest request, StringBuilder builder) {
		String nombre = null;
		PrintWriter pw = null;
		try {
			String fnfe = config.getString(WEBPAY_RUTA_TEMPORAL_KEY);
			String nombreTemp = fnfe + "/PARAMS_"
					+request.getParameter("TBK_ORDEN_COMPRA") + ".txt";
			FileOutputStream fos = new FileOutputStream(nombreTemp);
			pw = new PrintWriter(fos);
			pw.println(builder.toString());
			nombre = nombreTemp;
		}catch(FileNotFoundException var11){
			logger.warn("Error creando el archivo de parametros", var11);
		} finally {
			ResourceLeakUtil.close(pw);
		}
		return nombre;
	}

	private boolean esMontoCompraValido(HttpServletRequest request) {
		PagosDelegate delegate = new PagosDelegate();
		String montoTbk = request.getParameter("TBK_MONTO");
		String ordenCompra = request.getParameter("TBK_ORDEN_COMPRA");
		int idSolicitud = Integer.parseInt(ordenCompra);
		boolean valido = false;
		if (montoTbk != null) {
			try {
				Transaccion trx = delegate.obtenerDatosTransaccion(idSolicitud);
				long montoAutorizado = Long.parseLong(montoTbk) / 100L;
				logger.info("Monto autorizado: " + montoAutorizado);
				long monto = trx.getMonto();
				logger.info("Valor del Seguro: " + monto);
				if (montoAutorizado >= monto) {
					valido = true;
				}
			}catch(NumberFormatException var12){
				logger.warn("El monto especificado desde transbank no es valido", var12);
			}
		}
		logger.info("RESULTADO VALIDACION MONTO: " + valido);
		return valido;
	}

	private boolean esTarjetaValida(HttpServletRequest request) {
		boolean res = false;
		new CryptoUtil(SeguridadConfig.getInstance());
		int tbkOrdenCompra = Integer.valueOf(
		request.getParameter("TBK_ORDEN_COMPRA")).intValue();
		String tbkFinalNumeroTarjeta = request.getParameter("TBK_FINAL_NUMERO_TARJETA");
		logger.info("NUM TARJETA: " + tbkFinalNumeroTarjeta);
		PagosDelegate delegate = new PagosDelegate();
		res = delegate.validarDigitosTarjeta(tbkOrdenCompra, tbkFinalNumeroTarjeta);
		logger.info("RESULTADO VALIDACION TARJETA: " + res);
		return res;
	}

	private boolean aceptaTransaccion(Transaccion transaccion, HttpServletRequest request){
		boolean respuestaPCBS = false;
		CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
		long lIdSolicitud = Long.valueOf(request.getParameter("TBK_ID_SESION")).longValue();
	
		try {	
			PagosDelegate pagos = new PagosDelegate();
			respuestaPCBS = pagos.aceptaTransaccion(transaccion); // Grabar trx en PCBS WsBigsaAceptaCotizacion
			logger.info("Resultado de aceptacion PCBS: " + respuestaPCBS);
			
			if (respuestaPCBS) {
				logger.info("Marcar compra exitosa");
				this.marcarCompraExitosa(request);	
			}else{
				logger.info("Actualizar estado de solicitud: Rechazada"); 
				cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud, "CANCELADA"); 
				respuestaPCBS = false; 
			}
		}catch(Exception var5){
			logger.error("Error al aceptar transaccion", var5);
			respuestaPCBS = false;
			cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud, "CANCELADA");
		}
		return respuestaPCBS;
	}

	private boolean marcarCompraExitosa(HttpServletRequest request) {
		PagosDelegate delegate = new PagosDelegate();
		CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
		String numeroOrdenCompra = request.getParameter("TBK_ORDEN_COMPRA");
		long lIdSolicitud = Long.valueOf(request.getParameter("TBK_ID_SESION")).longValue();
		logger.info("Orden compra: " + numeroOrdenCompra);
		logger.info("Id solicitud: " + lIdSolicitud);
		Object cotizacionSeguro = null;
	
		try {		
			Solicitud solicitud = cotizacionDelegate.obtenerDatosSolicitud(lIdSolicitud);
			logger.info("Rama de la solicitud: " + solicitud.getId_rama());
			
			if (solicitud.getId_rama().equals("1")|| solicitud.getId_rama().equals("8")) {
				cotizacionSeguro = cotizacionDelegate.obtenerDatosMateriaVehiculo(lIdSolicitud);
			} else if (solicitud.getId_rama().equals("2")) {
				cotizacionSeguro = cotizacionDelegate.obtenerDatosMateriaHogar(lIdSolicitud);
			} else {
				cotizacionSeguro = cotizacionDelegate.obtenerDatosMateriaVida(lIdSolicitud);
			}
			
			logger.info("Obtener contratante");
			((CotizacionSeguro)cotizacionSeguro).
				setContratante(cotizacionDelegate.obtenerDatosContratante(lIdSolicitud));
			((CotizacionSeguro)cotizacionSeguro).
				setCodigoProducto(Long.parseLong(solicitud.getId_producto()));

			logger.info("Obtener datos del plan");
			Plan e = cotizacionDelegate
						.obtenerDatosPlan(solicitud.getId_plan());
			Contratante contratante = ((CotizacionSeguro)cotizacionSeguro).getContratante();
			((CotizacionSeguro)cotizacionSeguro).setCodigoPlan(solicitud.getId_plan());
			((CotizacionSeguro)cotizacionSeguro).setEmail(contratante.getEmail());
			((CotizacionSeguro)cotizacionSeguro).setNombre(contratante.getNombre());
			((CotizacionSeguro)cotizacionSeguro).setRutCliente(contratante.getRut());
			
			//Envia Mail
			logger.info("Enviar Mail");
			delegate.enviarEmail((CotizacionSeguro)cotizacionSeguro,
					numeroOrdenCompra, solicitud.getId_rama());
			
			logger.info("Actualizar estado de solicitud: COMPRADA");
			cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud, "COMPRADA");

			ACVConfig config = ACVConfig.getInstance();
			ParametroCotizacion[] comunas = cotizacionDelegate
												.obtenerComunas(contratante.getIdRegion());
			String descComuna = "";
			ParametroCotizacion[] direccion = comunas;
			int vehiculo = 0;
			for (int solicitudInspeccion = comunas.length; vehiculo < 
					solicitudInspeccion; ++vehiculo){
				ParametroCotizacion idSolicitud = direccion[vehiculo];
				if (idSolicitud.getId().trim().equals(contratante.getIdComuna())) {
					descComuna = idSolicitud.getDescripcion();
					break;
				}
			}

			String SidSolicitud = String.valueOf(lIdSolicitud);
			
			/* Se elimina solicitud de inspecci�n a trav�s de WS -- CBM 02-11 --*/
			if (cotizacionSeguro instanceof CotizacionSeguroVehiculo
					&& e.getInspeccionVehi() == 1) {
				logger.info("Es cotizacion de vehiculo");
				CotizacionSeguroVehiculo var22 = (CotizacionSeguroVehiculo) cotizacionSeguro;
				logger.info("No tiene factura, es auto usado");
				SolicitudInspeccion solicitudBSP = new SolicitudInspeccion();
				solicitudBSP.setId_solicitud(SidSolicitud);
				solicitudBSP.setNombre_corredor(config.getString(NOMBRE_CORREDOR));
				solicitudBSP.setRamo(config.getString(TIPO_RAMO));
				solicitudBSP.setCod_compannia_inspeccion(e.getCodigoCompanniaInspeccion());
				solicitudBSP.setNombre_asegurado(contratante.getNombreCompleto());
				solicitudBSP.setRut_asegurado(contratante.getRut()+"-"+contratante.getDv());
				solicitudBSP.setNombre_contacto(contratante.getNombreCompleto());
				String direccionCompleta = contratante.getCalleDireccion()
						+" "+contratante.getNumeroDireccion() + " "
						+contratante.getNumeroDepartamentoDireccion();
				solicitudBSP.setDireccion(direccionCompleta);
				solicitudBSP.setComuna(descComuna);
				solicitudBSP.setTelefono_contacto(contratante.getTelefono1());
				solicitudBSP.setUsuario_interno_bsp(config.getString(USER_BSP));
				solicitudBSP.setTipo_inspeccion(var22.getTipoInspeccion());
				solicitudBSP.setObservaciones(e.getNombrePlan());
				solicitudBSP.setNumero_patente(var22.getPatente());
				solicitudBSP.setReferencia_interna(numeroOrdenCompra);
				if (contratante.getTelefono2() != null) {
					solicitudBSP.setCelular(contratante.getTelefono2());
				}
				solicitudBSP.setFactura(var22.getFactura());
				logger.info("Solicitar inspeccion");
				String idSolicitudInspeccion = delegate.solicitarInspeccion(solicitudBSP);
				request.setAttribute("nroSolicitudInspeccion", idSolicitudInspeccion);
			}
			
			
			return true;
		}catch(Exception var19) {
			var19.printStackTrace();
			logger.error("traza:" + var19);
			logger.error("ERROR AL MARCAR COMPRA EXITOSA: " + numeroOrdenCompra);
			cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud, "ERROR_POST_WEBPAY");
			return false;
		}
	}

	private boolean grabarDatosRespuesta(HttpServletRequest request){
		boolean res = false;
		try {
			PagosDelegate e = new PagosDelegate();
			RespuestaTBK respuesta = new RespuestaTBK();
			respuesta.setTBK_CODIGO_AUTORIZACION(request
					.getParameter("TBK_CODIGO_AUTORIZACION"));
			respuesta.setTBK_FECHA_CONTABLE(Integer.parseInt(request
					.getParameter("TBK_FECHA_CONTABLE")));
			respuesta.setTBK_FECHA_TRANSACCION(Integer.parseInt(request
					.getParameter("TBK_FECHA_TRANSACCION")));
			respuesta.setTBK_FINAL_NUMERO_TARJETA(Integer.parseInt(request
					.getParameter("TBK_FINAL_NUMERO_TARJETA")));
			respuesta.setTBK_HORA_TRANSACCION(Integer.parseInt(request
					.getParameter("TBK_HORA_TRANSACCION")));
			respuesta.setTBK_ID_SESION(request.getParameter("TBK_ID_SESION"));
			respuesta.setTBK_ID_TRANSACCION(Long.parseLong(request
					.getParameter("TBK_ID_TRANSACCION")));
			respuesta.setTBK_MAC(request.getParameter("TBK_MAC"));
			respuesta.setTBK_MONTO(Integer.parseInt(request
					.getParameter("TBK_MONTO")));
			respuesta.setTBK_NUMERO_CUOTAS(Integer.parseInt(request
					.getParameter("TBK_NUMERO_CUOTAS")));
			respuesta.setTBK_ORDEN_COMPRA(request
					.getParameter("TBK_ORDEN_COMPRA"));
			respuesta.setTBK_RESPUESTA(Integer.parseInt(request
					.getParameter("TBK_RESPUESTA")));
			respuesta.setTBK_TASA_INTERES_MAX(request.getParameter("TBK_TASA_INTERES_MAX") != null ? 
					Integer.parseInt(request.getParameter("TBK_TASA_INTERES_MAX")) : 0);
			respuesta.setTBK_TIPO_PAGO(request.getParameter("TBK_TIPO_PAGO"));
			respuesta.setTBK_TIPO_TRANSACCION(request
					.getParameter("TBK_TIPO_TRANSACCION"));
			respuesta.setTBK_VCI(request.getParameter("TBK_VCI("));
			
			e.grabarDatosRespuesta(respuesta); //Guardar tabla RESPUESTA_TBK
			res = true;
		}catch(Exception var5) {
			logger.error("Error al grabar datos de transaccion en BD", var5);
		}
		logger.info("RESULTADO GRABAR BD: " + res);
		return res;
	}
}
