// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.RespuestaTBK;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class WebPayExitoAction extends Action {

   private static Log logger = LogFactory.getLog(WebPayExitoAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      logger.info("PAGINA DE EXITO ACCESADA.");
      PagosDelegate delegate = new PagosDelegate();
      CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
      Plan plan = (Plan)request.getSession().getAttribute("datosPlan");
      long lIdSolicitud = ((Long)request.getSession().getAttribute("idSolicitud")).longValue();
      RespuestaTBK respuesta = delegate.obtenerDatosRespuesta(lIdSolicitud);
      Integer inumeroOrdenCompra = (Integer)request.getSession().getAttribute("resultNroBigsa");
      String numeroOrdenCompra = String.valueOf(inumeroOrdenCompra);
      logger.info("orden compra: " + numeroOrdenCompra);
      CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      logger.debug("cotizacionSeguro Encontrado en sesion: " + cotizacionSeguro);
      respuesta.setTBK_MONTO(respuesta.getTBK_MONTO() / 100);
      request.setAttribute("respuesta", respuesta);
      //request.setAttribute("email", cotizacionSeguro.getEmail());
      logger.debug("VALIDAR CAMPANNA DEL PLAN: " + plan.getIdPlan());
      Map parametro = cotizacionDelegate.obtenerParametro("CAMPANNA", plan.getIdPlan());
      if(parametro != null) {
         logger.debug("PARAMETRO CAMPANNA OBTENIDO: " + parametro.get("VALOR"));
         String htmlCampanna = (String)parametro.get("VALOR");
         if(htmlCampanna != null && !"".equals(htmlCampanna)) {
            request.setAttribute("HTML_CAMPANNA", parametro.get("VALOR"));
         }
      }

      logger.info("LEVANTAR PAGINA DE EXITO PARA: " + numeroOrdenCompra);
      return mapping.findForward("continuar");
   }

}
