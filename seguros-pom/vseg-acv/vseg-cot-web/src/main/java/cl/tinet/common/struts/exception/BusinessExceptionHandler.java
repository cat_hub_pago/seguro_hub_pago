// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.tinet.common.struts.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ExceptionHandler;

public class BusinessExceptionHandler extends ExceptionHandler {

   private static final Log LOGGER = LogFactory.getLog(BusinessExceptionHandler.class);


   protected void logException(Exception e) {
      LOGGER.debug("Se ha encontrado una excepción de negocio.", e);
   }

}
