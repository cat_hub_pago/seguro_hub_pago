// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.tinet.common.struts.form;

import com.tinet.validator.struts.StrutsTinetValidator;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.RequestUtils;

public abstract class ValidableActionForm extends ActionForm {

   public static final String PROPERTY_NAMES = "cl.tinet.common.struts.validator.PROPERTY_NAMES";


   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
      InputStream validators = ValidableActionForm.class.getResourceAsStream("/com/tinet/validator/resource/validation-base.xml");
      InputStream formRules = this.getValidationRules(request);
      StrutsTinetValidator validator = new StrutsTinetValidator(new InputStream[]{validators, formRules}, RequestUtils.getUserLocale(request, (String)null));
      validator.validateTO(this);
      ActionErrors results = (ActionErrors)validator.getResults(this.getServlet());
      request.setAttribute("cl.tinet.common.struts.validator.PROPERTY_NAMES", results.properties());
      return results;
   }

   public abstract InputStream getValidationRules(HttpServletRequest var1);

   protected Class obtenerClase() {
      return this.getClass();
   }
}
