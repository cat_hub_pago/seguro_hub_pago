// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.tinet.common.struts.form;

import cl.tinet.common.bean.PopulateUtil;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.struts.form.ValidableActionForm;
import com.tinet.exceptions.system.SystemException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.RequestUtils;

public abstract class BuilderActionForm extends ValidableActionForm {

   private Map datos;


   public Map getDatos() {
      return this.datos;
   }

   public void setDatos(Map datos) {
      this.datos = datos;
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {
      this.datos = new HashMap();
   }

   public Object buildJavaBean(HttpServletRequest request, Class clase) {
      return this.buildJavaBean(request, clase, this.datos);
   }

   public Object buildJavaBean(HttpServletRequest request, Class clase, Map datos) {
      try {
         Object e = clase.newInstance();
         PopulateUtil.populate(e, datos, this.getConfigurator(request), RequestUtils.getUserLocale(request, (String)null));
         return e;
      } catch (InstantiationException var5) {
         throw new SystemException(var5);
      } catch (IllegalAccessException var6) {
         throw new SystemException(var6);
      }
   }

   public void buildForm(HttpServletRequest request, Object javabean) {
      this.buildForm(request, javabean, this.datos);
   }

   public void buildForm(HttpServletRequest request, Object javabean, Map datos) {
      PopulateUtil.describe(javabean, this.getConfigurator(request), RequestUtils.getUserLocale(request, (String)null));
   }

   public abstract AbstractConfigurator getConfigurator(HttpServletRequest var1);
}
