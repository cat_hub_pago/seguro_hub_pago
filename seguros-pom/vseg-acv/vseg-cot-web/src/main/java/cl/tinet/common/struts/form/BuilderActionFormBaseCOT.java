// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.tinet.common.struts.form;

import cl.cencosud.acv.common.config.ACVConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.struts.form.BuilderActionForm;
import javax.servlet.http.HttpServletRequest;

public abstract class BuilderActionFormBaseCOT extends BuilderActionForm {

   private static final long serialVersionUID = -1009855847539665071L;


   public AbstractConfigurator getConfigurator(HttpServletRequest request) {
      return ACVConfig.getInstance();
   }
}
