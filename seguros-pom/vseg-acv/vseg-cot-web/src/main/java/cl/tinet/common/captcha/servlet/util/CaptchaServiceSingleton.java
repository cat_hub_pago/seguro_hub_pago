// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.tinet.common.captcha.servlet.util;

import com.octo.captcha.CaptchaFactory;
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomListColorGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.SimpleTextPaster;
import com.octo.captcha.component.image.wordtoimage.ComposedWordToImage;
import com.octo.captcha.component.word.FileDictionary;
import com.octo.captcha.component.word.wordgenerator.DictionaryWordGenerator;
import com.octo.captcha.engine.GenericCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.octo.captcha.service.multitype.GenericManageableCaptchaService;
import java.awt.Color;
import java.awt.Font;

public class CaptchaServiceSingleton {

   private static ImageCaptchaService instance;


   public static ImageCaptchaService getInstance() {
      return instance;
   }

   static {
      short imageWidth = 133;
      byte imageHeight = 30;
      byte minWordLength = 4;
      byte maxWordLength = 8;
      short timeoutSegundos = 600;
      byte maxBuffer = 100;
      byte maxBufferBeforeGC = 100;
      Color[] coloresFondo = new Color[]{Color.WHITE};
      Color[] coloresTexto = new Color[]{Color.BLACK};
      Font[] fonts = new Font[]{Font.decode("Verdana"), Font.decode("Arial")};
      byte minFontSize = 20;
      byte maxFontSize = 20;
      RandomFontGenerator fg = new RandomFontGenerator(Integer.valueOf(minFontSize), Integer.valueOf(maxFontSize), fonts);
      UniColorBackgroundGenerator bg = new UniColorBackgroundGenerator(Integer.valueOf(imageWidth), Integer.valueOf(imageHeight), new RandomListColorGenerator(coloresFondo));
      SimpleTextPaster textpaster = new SimpleTextPaster(Integer.valueOf(minWordLength), Integer.valueOf(maxWordLength), new RandomListColorGenerator(coloresTexto), Boolean.valueOf(false));
      ComposedWordToImage word2image = new ComposedWordToImage(fg, bg, textpaster);
      DictionaryWordGenerator dic = new DictionaryWordGenerator(new FileDictionary("toddlist"));
      GimpyFactory factory = new GimpyFactory(dic, word2image);
      GenericCaptchaEngine engine = new GenericCaptchaEngine(new CaptchaFactory[]{factory});
      instance = new GenericManageableCaptchaService(engine, timeoutSegundos, maxBuffer, maxBufferBeforeGC);
   }
}
