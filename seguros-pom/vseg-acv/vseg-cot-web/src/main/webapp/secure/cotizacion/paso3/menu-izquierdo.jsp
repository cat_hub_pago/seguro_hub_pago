<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="java.text.DecimalFormat"%><%@ taglib
	uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="java.util.Locale"%>
<%
	session.setAttribute("currentLocale", new Locale("es", "CL"));
%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<!-- INICIO  RESUMEN -->

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3-fluid">
	<div class="row bg-resumen">
		<h4 class="hidden-xs">Resumen</h4>
		<div class="row">
			<div class="p-resumen">
				<div class="col-md-12">
<!-- 					<div class="col-md-3 col-sm-3 col-xs-3"> -->
<!-- 						<img src="/vseg-paris/img/cliente.jpg" class="img-cliente"> -->
<!-- 					</div> -->

					<logic:present name="usuario">

						<div class="col-md-9 col-sm-9 col-xs-9">
							<div class="col-md-4 col-xs-4 p0">
								<p class="m0">Bienvenido</p>
							</div>
							<div class="col-md-8 col-xs-8 pright">
								<p class="text-right m0">
									<a href="/vseg-paris/autenticacion/cerrarSession"><span class="glyphicon glyphicon-remove"
										aria-hidden="true"></span> Cerrar</a>
								</p>
							</div>
							<p>
								<strong><bean:write name="usuario" property="usuarioExterno.nombre" /> <bean:write name="usuario" property="usuarioExterno.apellido_paterno" /> <bean:write name="usuario" property="usuarioExterno.apellido_materno" /></strong>
							</p>
						</div>
			
				
					<div class="col-md-12 col-sm-12 col-xs-12 p0">
						<div class="separacion-hor-cont-lateral hidden-xs"></div>
					</div>
					</logic:present>
					<div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
						<label>Rut:</label>
						<bean:write name="datosCotizacion" property="rutCliente"
							format="#,###" locale="currentLocale" />
						-
						<bean:write name="datosCotizacion" property="dv" />
						<br />
						<logic:notEmpty name="datosCotizacion" property="nombre">
							<label>Nombre:</label>
							<bean:write name="datosCotizacion" property="nombre" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion" property="apellidoPaterno">
							<label>Apellido Paterno:</label>
							<bean:write name="datosCotizacion" property="apellidoPaterno" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion" property="apellidoMaterno">
							<label>Apellido Materno:</label>
							<bean:write name="datosCotizacion" property="apellidoMaterno" />
							<br />
						</logic:notEmpty>
						<logic:notEqual name="datosCotizacion" property="numeroTelefono1"
							value="0">
							<label>N�mero Tel�fono:</label>
							<bean:write name="datosCotizacion" property="numeroTelefono1" />
							<br />
						</logic:notEqual>
						<logic:notEmpty name="datosCotizacion" property="email">
							<label>Email:</label>
							<bean:write name="datosCotizacion" property="email" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion" property="fechaNacimiento">
							<label>Fecha Nacimiento:</label>
							<bean:write name="datosCotizacion" property="fechaNacimiento"
								format="dd/MM/yyyy" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion" property="estadoCivil">
							<label>Estado Civil:</label>
							<bean:write name="datosCotizacion" property="estadoCivilDesc" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion" property="actividad">
							<label>Actividad:</label>
							<bean:write name="datosCotizacion" property="actividad" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion" property="sexo">
							<label>Sexo:</label>
							<logic:equal value="M" name="datosCotizacion" property="sexo">Masculino</logic:equal>
							<logic:equal value="F" name="datosCotizacion" property="sexo">Femenino</logic:equal>
							<br />
						</logic:notEmpty>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 p0">
						<div class="separacion-hor-cont-lateral hidden-xs"></div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
						<logic:notEmpty name="datosPlan" property="nombrePlan">
							<label>Plan:</label>
							<bean:write name="datosPlan" property="nombrePlan" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualPesos">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualPesos"
								format="$ ##,##0" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualUF">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualUF"
								format="UF #0.0000" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion"
							property="antiguedadVivienda">
							<label>Antig�edad Vivienda:</label>
							<bean:write name="datosCotizacion" property="antiguedadVivienda" /> A�os.
							<br />
						</logic:notEmpty>
						<logic:present name="datosCotizacion" property="tipoDesc" scope="session">
							<label>Tipo:</label>
							<bean:write name="tipoDesc" property="tipoDesc" scope="request" />
							<br />
						</logic:present>
						<logic:present name="datosCotizacion" property="anyoVehiculo">
							<label>A�o:</label><bean:write name="datosCotizacion" property="anyoVehiculo"/>
						</logic:present>
						<br />
					
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 p0">
						<div class="separacion-hor-cont-lateral hidden-xs"></div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
						<logic:notEmpty name="datosCotizacion" property="numeroTelefono2">
							<label>Tel�fono Opcional:</label>
							<bean:write name="datosCotizacion" property="numeroTelefono2" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="regionDescripcion">
							<label>Regi�n:</label>
							<bean:write name="regionDescripcion" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="ciudadDescripcion">
							<label>Ciudad:</label>
							<bean:write name="ciudadDescripcion" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="comunaDescripcion">
							<label>Comuna:</label>
							<bean:write name="comunaDescripcion" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosCotizacion" property="direccion">
							<label>Direcci�n:</label>
							<bean:write name="datosCotizacion" property="direccion" />
							<bean:write name="datosCotizacion" property="numeroDireccion" />
							<bean:write name="datosCotizacion" property="numeroDepto" />
							<br />
						</logic:notEmpty>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- FIN  RESUMEN -->
