<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<link type="text/css" rel="stylesheet" href="/vseg-paris/style/estilo_footer.css" />
   <link type="text/css" rel="stylesheet" href="/vseg-paris/style/estilo_calugas.css" />
   <link type="text/css" rel="stylesheet" href="/vseg-paris/style/estilo_home.css" />


<div id='footer'>
<div id="sombra_header"></div>

<!-- EMPRESAS -->
<div class="body_contenido_empresas">
	<div id="contenido_empresas">
		<div id="cot_seguros">Compa&ntilde;ias que trabajan con nosostros</div>
		<div id="cotiza_sep_2"></div>
		<div id="cotiza_sep_empresas"></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Magallanes.png" width="122" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Consorcio.png" width="122" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Zenit.png" width="92" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/BNP.png" width="130" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Sura.png" width="72" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Metlife.png" width="78" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/BanChile.png" width="95" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Liberty.png" width="92" height="40" /></div>
		<div id="cotiza_sep_empresas_b"></div>
		<!--div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/PentaSecurity.png" width="139" height="40" /></div-->
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Mapfre.png" width="173" height="40" /></div>
		<div id="cot_seguros_empresas"><img border="0" display="block" src="/vseg-paris/img/seguros/bn/Bice_Vida.png" width="135" height="40" /></div>
		<div id="cotiza_sep"></div>
	</div>
</div>
<!-- FIN EMPRESAS -->

    <div class="body_footer">

        <div class="contenido_footer">

            <!-- FOOTER CONTENIDO -->
            <div class="footer">

                <div class="footer_contenido">

                    <div class="sep_footer"></div>

                    <div class="columnas_footer" style="width:140px;">
                        <ul>
                            <strong>Pol&iacute;ticas del Sitio</strong>
                            <li><a href="/vseg-paris/html/empresa.html">Nuestra Empresa</a></li>
                            <li><a href="/vseg-paris/html/politicas.html" >Pol&iacute;ticas de Seguridad</a></li>
                            <li><a href="/vseg-paris/html/terminos-condiciones.html">T&eacute;rminos y Condiciones</a></li>
                        </ul>
                    </div>

                    <div class="sep_footer"></div>

                    <div class="columnas_footer" style="width:151px;">
                        <ul>
                            <strong>Te Ayudamos</strong>
                            <li><a href="/vseg-paris/html/preguntas-frecuentes.html" >Preguntas Frecuentes</a></li>
                            <li><a href="/vseg-paris/html/caso-siniestro.html" >En Caso de Siniestro</a></li>
                            <li><a href="/vseg-paris/Sucursales" >Nuestras Sucursales</a></li>
                            <li><a href="/vseg-paris/html/polizas.html" >P&oacute;lizas</a></li>
                            <li><a href="/vseg-paris/html/glosario.html" >Glosario</a></li>
                            <li><a href="/vseg-paris/html/puntos.html" >Acumulas Puntos Cencosud</a></li>
                            <!--li><a href="/vseg-paris/formulario-contacto.do" >Contactanos</a></li-->
							<li><a href="/vseg-paris/html/contactanos.html" >Cont&aacute;ctanos</a></li>			
                        </ul>
                    </div>

                    <div class="sep_footer"></div>

                    <div class="columnas_footer" style="width:110px;">
                        <ul>
                            <strong>Cencosud</strong>
                            <li><a href="http://www.tarjetacencosud.cl/" target="_blank">Tarjeta Cencosud</a></li>
                            <li><a href="http://www.bancoparis.cl/" target="_blank">Banco Paris</a></li>
                            <li><a href="http://www.paris.cl/" target="_blank">Paris</a></li>
                            <li><a href="http://www.jumbo.cl/" target="_blank">Jumbo</a></li>
                            <li><a href="http://www.easy.cl/" target="_blank">Easy</a></li>
                            <li><a href="http://www.johnson.cl/" target="_blank">Johnson</a></li>
                            <li><a href="http://www.santaisabel.cl/" target="_blank">Santa Isabel</a></li>
                            <li><a href="http://www.puntoscencosud.cl/" target="_blank">Puntos Cencosud</a></li>
                        </ul>
                    </div>

                    <div class="sep_footer"></div>

                    <div class="columnas_footer" style="width:260px;">
                        <ul>
                                    <strong>Informaci&oacute;n</strong>
                            <li><a href="http://www.svs.cl/portal/principal/605/w3-channel.html" target="_blank">Superintendencia Valores y Seguros</a></li>
                            <li><a href="https://www.seguroscencosud.cl/vseg-paris/html/bases_Promocion.html">Bases de Promociones</a></li>
							<li><a href="/vseg-paris/html/credito_consumo.html" target="_blank">Seguros Cr&eacute;ditos de Consumo</a></li>
                        </ul>
                    </div>

                    <div class="sep_footer"></div>

                    <div class="columnas_footer" style="width:266px;">
                        <div id="img_preocupadosporti">
							<a href="http://www.vivetranquilovivefeliz.cl/" target="_blank">
								<img border="0" display="block" src="/vseg-paris/img/vivetranquilovivefeliz.png" width="266" height="60" />
							</a>
						</div>
                    </div>

                    <div class="sep_footer_final"></div>

                </div>

            </div>
            <!-- FIN FOOTER CONTENIDO -->

            <!-- MEDIO DE PAGO -->
            <div class="medio_pago">
                <div class="medio_pago_contenido">

                    <div class="medio_pago_sep"></div>

                    <div class="medio_pago_esp"></div>

                    <div class="medio_pago_info">
                        <strong>Medios de Pago </strong></div>
                        <div> <img border="0" display="block" src="/vseg-paris/img/medio_pago.png" width="650" height="55" /></div>

                        <div class="medio_pago_sep"></div>

                    </div>
                </div>
                <!-- FIN MEDIO DE PAGO -->

                <!-- COPYRIGHT -->

                <!--div class="copyright">
                    <div class="copyright_contenido">

                        <div class="copy_esp"></div>

                        <div class="copyright_info"><strong>Copyright 2013</strong>. Todos los derechos reservados.</div>

                        <div class="copy_sep"></div>

                        <div class="navegador_info">Sitio optimizado para los navegadores <strong>Mozilla Firefox</strong>, <strong>Google Chrome</strong> e <strong>Explorer 8</strong> &oacute; superior.</div>

                        <div class="copy_sep"></div>

                        <div class="secure_info"></div>

                    </div>
                </div-->
				<div class="col-12"> 
					<div class="txt_legal c-summary__subtitle" style="padding-bottom: 0;"> 
						<p>Copyright 2017. Todos los derechos reservados. Sitio optimizado para los navegadores Mozilla Firefox, Google Chrome e Explorer 10 � superior.</p>        
					</div> 
				</div>

                <!-- FIN COPYRIGHT -->

            </div>

        </div>

