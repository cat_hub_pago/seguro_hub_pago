<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="#">
		
		<link href="/vseg-paris/css/bootstrap.min.css" rel="stylesheet">
		<link href="/vseg-paris/css/bootstrap-theme.min.css" rel="stylesheet">
		<link href="/vseg-paris/css/estilos.css" rel="stylesheet">
		<link href="/vseg-paris/css/carousel.css" rel="stylesheet">
		<link href="/vseg-paris/css/base.css" rel="stylesheet">
		<link rel="stylesheet" href="/vseg-paris/css/owl.carousel.css">
		<link rel="stylesheet" href="/vseg-paris/css/owl.theme.css">
		<link href="/cotizador/css/jquery-ui.css" rel="stylesheet">
		
		<script src="/vseg-paris/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<script src="/vseg-paris/js/ie-emulation-modes-warning.js"></script>
		<script src="/vseg-paris/js/jquery-1.11.3.min.js"></script>
		<script src="/vseg-paris/js/vendor/bootstrap.min.js"></script>
		<script src="/vseg-paris/js/jPushMenu.js"></script>
		<script src="/vseg-paris/js/v2p.js"></script>
		<script src="/vseg-paris/js/owl.carousel.js"></script>
		<script src="/cotizador/js/jquery-ui.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {

				var owl = $("#owl-demo");

				owl.owlCarousel({

					items : 6, //10 items above 1000px browser width
					itemsDesktop : [ 1200, 8 ], //5 items between 1000px and 901px
					itemsDesktopSmall : [ 992, 3 ], // 3 items betweem 900px and 601px
					itemsTablet : [ 768, 2 ], //2 items between 600 and 0;
					itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option

				});
			});
		</script>
    <title>Exito</title>
  </head>
  
	<body>
		<%@ include file="../google-analytics/google-analytics.jsp" %>
	<!--BOX RESPONSIVE -->
	<div class="container">
		<div class="row bot30">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bot30">
				<h4 class="text-center center-block"><bean:message key="labels.usuario.registroCliente.titulo" bundle="labels-cliente"/></h4>
				<h4 class="text-center center-block"><bean:message bundle="labels-cliente" key="labels.usuario.registroCliente.msgExito" /></h4>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top30">
					<button type="button" class="btn btn-primary center-block top30" onclick="javascript:window.parent.location.href='/vseg-paris/index.jsp';">Volver</button>
				</div>
			</div>
		</div>
	</div>
	<!--BOX RESPONSIVE FIN-->
    </body>
</html:html>
