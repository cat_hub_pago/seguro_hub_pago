<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
	<head>
		<script src="js/jquery/jquery-1.4.2.js"></script>
		<link href="css/formulario-ver.css" rel="stylesheet" type="text/css" />
        <script src="js/jquery/jquery.fancybox/jquery.mousewheel-3.0.4.pack.js" type="text/javascript" ></script>
		<script src="js/jquery/jquery.fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript" ></script>
		<link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
				<script src="js/jquery/jquery.colorbox.js" type="text/javascript"></script>
				<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
		
	</head>
	<body>
		
		<div id="errorlogin">
			<div align="center" style="padding-top: 50px;">
				<div id="estamos_procesando">
					<div id="curba_top_proceso">

						<a onclick="javascript:history.back();"> </a>
					</div>
					<div class="pantalla-error_cuerpo">
						<div id="conte_estamos_procesando">
							<div class="icono_geeral">
								<img src="images/img/icono_error.png" alt=""
									width="46" height="46" border="0" />
							</div>
							<div id="texto_procesando">
								<h3>
									Lo sentimos, ha ocurrido un error. Por favor int�ntalo m�s tarde
									<br />
								</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--BOX PROCESO FIN-->
		</div>
		
		<script type="text/javascript">
		//$(document).ready(function(){
		//	$("#errorlogin").show();
		//	$.fn.colorbox({
		//			 inline: true, 
		//		     width:"100%", 
		//		     height:"100%", 
		//		     href: "#errorlogin"
		//	});
		//});
		
		$(document).ready(function(){
			$('<a href="#errorlogin" class="login_error"></a>').appendTo('body');
			
			$("a.login_error").fancybox({ 
			    'hideOnContentClick': false,
				'frameWidth' : 470,
				'frameHeight' : 185,
		    	'type'  : 'inline'
			  });
			$("#fancybox-close").click(function(){
				$("#errorlogin").css("display","none");
			});
			$("a.login_error").click();
		});
</script>
	</body>
</html>
