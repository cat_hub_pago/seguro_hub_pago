


/*
 * Muestra y Oculta un div determinado por su id.
 * 
 * @param {Object} valor {true, false}. true: muestra, false:oculta.
 * @param {Object} nombreDiv id del Elemento
 */
 
 
function autocompletarfechaguion(input) {
	if(input.value.length == 2) {
		var fecha = input.value;
		input.value = fecha + "-";
	}
	else{ 
		if(input.value.length == 5) {
		var diaMes = input.value;
		input.value = diaMes +"-";
		}
	}
}
   	
function validarFecha(campo)
{	
	var dia = campo.substring(0,2);
	var mes = campo.substring(3,5);

	if(dia > 31  ||  mes > 12 ){
		return false;
	}else{
		return true;
	}
}
 
	function calcularEdadFecha(fecha,minimo,maximo){
	console.log( 'INI validando edad, fecha ingresada: '+fecha);
	fecha = fecha.split('-');
	var dia = fecha[0];
	var mes = fecha[1];
	var ano = fecha[2];		

		// rescatamos los valores actuales
		var fecha_hoy = new Date();
		var ahora_ano = fecha_hoy.getYear();
		var ahora_mes = fecha_hoy.getMonth()+1;
		var ahora_dia = fecha_hoy.getDate();
		

		// realizamos el calculo
		var edad = (ahora_ano + 1900) - ano;
		if ( ahora_mes < mes ){
			edad--;
		}
		if ((mes == ahora_mes) && (ahora_dia < dia)){
			edad--;
		}
		if (edad > 1900){
			edad -= 1900;
		}

		// calculamos los meses
		var meses=0;
		if(ahora_mes>mes)
			meses=ahora_mes-mes;
		if(ahora_mes<mes)
			meses=12-(mes-ahora_mes);
		if(ahora_mes==mes && dia>ahora_dia)
			meses=11;

		// calculamos los dias
		var dias=0;
		if(ahora_dia>dia)
			dias=ahora_dia-dia;
		if(ahora_dia<dia){
			var ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
			dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
		}
		if(edad>=parseInt(minimo) && edad<parseInt(maximo)){
			if(ahora_dia == dia && edad == parseInt(minimo)) {
	            console.log("--> dia del cumplea�o no puede contratar aun");
	            return false;
			}else{
				console.log("--> 18+1");
				return true;
			}
		}else{
			console.log("menor de 18");
			return false;
		}	
	}
 
function mostrarDiv(valor, id) {
	var elemento = document.getElementById(id);
	if(valor) {
		elemento.style.display = "none";
	}
	else {
		elemento.style.display = "block";
	}
} 

function datosDuenyoVehiculo(valor){
	var idElemento = "formulario-datos-vehiculo";
	mostrarDiv(valor, idElemento);
}

function datosVivienda(valor) {
	var idElemento = "formulario-datos-vivienda";
	mostrarDiv(valor,idElemento);
}

function datosAseguradoViaje(valor) {
	var idElemento = "formulario-datos-viaje";
	mostrarDiv(valor,idElemento);
}

function resizeIframe(){
	//alert(document.getElementById("iframePlanes").height);
	//alert(document.getElementById('iframePlanes').contentWindow.document.body.scrollHeight + "px");
	document.getElementById("iframePlanes").height = document.getElementById('iframePlanes').contentWindow.document.body.scrollHeight + "px";
}

/**
 * Agrega el numero en romanos a un select que tenga numeros.
 * @param selectName Nombre del objeto select
 * @return objeto select con el texto con numero romano.
 */
function regionToRoman(selectName) {
	$("#"+selectName+" option").each(
		function(idx) {
			var value = $(this).attr('value');
			var text = $(this).text();
			//Agregar el numero romano al texto.
			
			text = roman(idx, 1) + " " + text;
			
			$(this).text(text);
		}
	);
}

function roman(n, s) {
	var r = '';
	var d;
	var rn = new Array('IIII', 'V', 'XXXX', 'L', 'CCCC', 'D', 'MMMM');
	for ( var i = 0; i < rn.length; i++) {
		var x = rn[i].length + 1;
		var d = n % x;
		r = rn[i].substr(0, d) + r;
		n = (n - d) / x;
	}
	if (s) {
		r = r.replace(/DCCCC/g, 'CM');
		r = r.replace(/CCCC/g, 'CD');
		r = r.replace(/LXXXX/g, 'XC');
		r = r.replace(/XXXX/g, 'XL');
		r = r.replace(/VIIII/g, 'IX');
		r = r.replace(/IIII/g, 'IV');
	}
	return r;
}

 function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	 
	return true;
}

function validar(e) {
	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla==8) return true;
	patron =/[A-Za-z\s]/;
	te = String.fromCharCode(tecla);
	return patron.test(te);
} 

function isCaracterEmail(evt) {
                var tecla = (evt.which) ? evt.which : evt.keyCode;
                var isCtrl = false;
                var isShift = false;
                var teclasPermitidas = new Array("a","n","c","x","v","j", "~", "�");
                var codigosNoPermitidos = new Array('!', '"', '#', '$', '%', '&', '/', '(', ')', '=', '\'', '�', '�', '?', ':', ';', ',', '�', '|', '�', '�', '+', '{', '}', '[', ']', '<', '>', '/', '*', '�', '�', '~');
                var noPermitidosSinShift = new Array("|", "/", "*",  "+", "<", "�", "{", "}", ",", "'", "�");
                if (window.event) {
                               key = window.event.keyCode;     //IE      
                               isCtrl = false;
                               if (window.event.ctrlKey) { isCtrl = true; }
                               if (window.event.shiftKey) { isShift = true; }
                } else {                                 
                               key = evt.which;     //firefox
                               isCtrl = false;
                               if (evt.ctrlKey) { isCtrl = true; }
                               if (evt.shiftKey) { isShift = true; }
                }
                if (isCtrl) {
                               for (i = 0; i < teclasPermitidas.length; i++) {
                                               if (teclasPermitidas[i].toLowerCase() == String.fromCharCode(key).toLowerCase()) {
                                                               return false;
                                                               break;
                                               }
                               }
                }
                if (isShift) {
                               for (j = 0; j < codigosNoPermitidos.length; j++) {
                                               if (codigosNoPermitidos[j] == String.fromCharCode(key).toLowerCase()) {
                                                               return false;
                                                               break;
                                               }
                               }                              
                } else {
                               for (k = 0; k < noPermitidosSinShift.length; k++) {
                                               if (noPermitidosSinShift[k] == String.fromCharCode(key).toLowerCase()) {
                                                               return false;
                                                               break;
                                               }
                               } 
                }                              
}

function mostrarDetalle(){
	$.fancybox({ 
		href		: '/cotizador/detalleCoberturas.do',
		maxWidth	: '100%',
		maxHeight	: '100%',
		fitToView	: false,
		width		: 650,
		height		: 400,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		type		: 'iframe'
	});		
}		