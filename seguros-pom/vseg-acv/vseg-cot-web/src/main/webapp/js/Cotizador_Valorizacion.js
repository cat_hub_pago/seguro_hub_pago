
$(document).ready(function(){
		//$.scrollTo("#para-continuar");
		
//		$("img[name=guardarEnviar]").colorbox({
//				iframe:true, 
//				width:"750", 
//				height:"630",
//				close: "",
//				href: "<%=request.getContextPath()%>/guardar-enviar.do?idPlan=" + $(this).attr('id')
//			});
	});

	function formatearSalida(opt){
	
		//CBM guardamos el valor del deducible seleccionado para saber cual mostrar.

		var check00 = $('#input00').is (':checked') ;
		var check03 = $('#input03').is (':checked') ;
		var check05 = $('#input05').is (':checked') ;
		var check10 = $('#input10').is (':checked') ;
		var ordenarCheck = $('#ordenar-por').val();
		
		frames.iframe_tabla.$('#d_valorizacion').empty();
		frames.iframe_tabla.formatearSalida(opt,'NO',check00,check03,check05,check10);
		frames.iframe_tabla.desplegarCoberturas();
		$('#ordenar-por').val(ordenarCheck);
		
	}
	
	function showFancyBox(url) {
				$.fancybox({
					'onStart'			: function() {
							var checkComp = $("input[type='checkbox'][name='chkPlanes']");
							var cont = 0;
							var value = '';
									
							for (i=0; i < checkComp.length; i++) {
								if (checkComp[i].checked) {
					  				cont = cont + 1;
					  				value += checkComp[i].value + '; ';
					 			}		
							}
							
							if (cont > 1 && cont < 5 ) {
								return true;
							} else {
								return false;
							}
					},
					'href'				: url ,
			        'width'				: 800,
					'height'			: '110%',
					'autoScale'			: false,
					'transitionIn'		: 'none',
					'transitionOut'		: 'none',
					'type'				: 'iframe'
				});
			}
			
			function habilitarComparador() {
				var checkComp = $("input[type='checkbox'][name='chkPlanes']");
				var cont = 0;
				var value = '';
				var url = '';
				
				for (i=0; i < checkComp.length; i++) {
					if (checkComp[i].checked) {
		  				cont = cont + 1;
		  				value += escape(checkComp[i].value) + ';';
		 			} 	
				}
				
				if (cont > 1 && cont < 5 ) {
					value=encodeURIComponent(value);
					url = '/cotizador/comparador-desplegar.do?oPlanes=' + value;
					$("#btn_comparador").attr("href", "javascript:showFancyBox('"+url+"')");
					$("#img_btn_comparador").attr("src","/vseg-paris/images/comparador/btn-azul-comparar.gif");
					$("#img_btn_comparador").attr("onmouseover","this.src='/vseg-paris/images/comparador/btn-azul-comparar-hover.gif'");
					$("#img_btn_comparador").attr("onmouseout","this.src='/vseg-paris/images/comparador/btn-azul-comparar.gif'");
				} else {
					$("#btn_comparador").attr("href", "javascript:void(0);");
					$("#img_btn_comparador").attr("src","/vseg-paris/images/comparador/btn-gris-comparar.gif");
					$("#img_btn_comparador").attr("onmouseover","this.src='/vseg-paris/images/comparador/btn-gris-comparar.gif'");
					$("#img_btn_comparador").attr("onmouseout","this.src='/vseg-paris/images/comparador/btn-gris-comparar.gif'");
				}
			}
	
	
	function contratar(idPlan, nombrePlan, primaPeso, primaUF, primaAnualUF, primaAnualPesos){
		$("#waitModal").modal('show');
		$("#idProducto").val($("#selectProducto").attr("value"));
		$("#idPlan").val(idPlan);
		$("#nombrePlan").val(nombrePlan);
		$("#primaPlanPesos").val(primaPeso);
		$("#primaPlanUF").val(primaUF);
		$("#primaAnualPesos").val(primaAnualPesos);
		$("#primaAnualUF").val(primaAnualUF);
		$("#enviar").val("");
				
		$("#guardarCotizacion").submit();
		
	}
	
	
	function envio(idPlan, nombrePlan, primaPeso, primaUF, primaAnualUF, primaAnualPesos, enviar){	
		$("#enviar").val(enviar);
		$("#idProducto").val($("#selectProducto").attr("value"));
		$("#idPlan").val(idPlan);
		$("#nombrePlan").val(nombrePlan);
		$("#primaPlanPesos").val(primaPeso);
		$("#primaPlanUF").val(primaUF);
		$("#primaAnualPesos").val(primaAnualPesos);
		$("#primaAnualUF").val(primaAnualUF);
		
		// Despliega Modal Enviar Cotizacion / HTML => cotizador-valorizacion.jsp
		$("#desplegarCotizacion").attr('src','cotizacion/includes/frame-guardar-enviar.jsp');
		$("#modalEnviarCotizacion").modal({
			backdrop: true,
			show: true
		});	
	}