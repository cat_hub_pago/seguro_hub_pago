$( '#paso2VehiculoForm' ).validate({
	rules: {
		tipoVehiculo: {
			required:true
		},
		marcaVehiculo: {
			required:true
		},
		modeloVehiculo: {
			required:true
		},
		anoVehiculo: {
			required:true
		},
		esNuevo: {
			required:true
		},
		color: {
			required:true
		},
		patente: {
			required:true
		},
		numeroMotor: {
			required:true
		},
		numeroChasis: {
			required:true
		},
		pregunta1: {
			required:true
		},
		pregunta2: {
			required:true
		},
		pregunta3: {
			required:true
		},
		pregunta4: {
			required:true
		}
	},
	groups: {
		phone: 'phone_type phone_number'
	},
	messages: {
		tipoVehiculo: {
			required: 'Este campo es requerido'
		},
		marcaVehiculo: {
			required: 'Este campo es requerido'
		},
		modeloVehiculo: {
			required: 'Este campo es requerido'
		},
		anoVehiculo: {
			required: 'Este campo es requerido'
		},
		esNuevo: {
			required: 'Este campo es requerido'
		},
		color: {
			required: 'Este campo es requerido'
		},
		patente: {
			required: 'Este campo es requerido'
		},
		numeroMotor: {
			required: 'Este campo es requerido'
		},
		numeroChasis: {
			required: 'Este campo es requerido'
		},
		pregunta1: {
			required: 'Este campo es requerido'
		},
		pregunta2: {
			required: 'Este campo es requerido'
		},
		pregunta3: {
			required: 'Este campo es requerido'
		},
		pregunta4: {
			required: 'Este campo es requerido'
		}
	},
	highlight: function(element) {
		$(element).parent().addClass("is-error");
		$(element).parent().addClass("o-form__field--error");
		$(element).parent().removeClass("is-ok");
		if($(element).attr('type')== 'radio'){
			$(element).parents('.o-form__field').addClass("is-error");
		}
	},
	unhighlight: function(element) {
		$(element).parent().removeClass("is-error");
		$(element).parent().removeClass("o-form__field--error");
		$(element).parent().addClass("is-ok");
	},
	errorPlacement: function ($error, $element) {
		$element.siblings('.o-form__message').append($error);

		if($element.attr('type')== 'radio'){
			$element.parents('.o-form__field').children('.o-form__message').append($error);
		}
	},
	submitHandler: function(form) {
		/*var inst = $('[data-remodal-id=continueModal]').remodal();
		inst.open();*/
		return false;
	}
});