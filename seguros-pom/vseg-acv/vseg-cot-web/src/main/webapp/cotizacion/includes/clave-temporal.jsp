<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>



<logic:present name="frameClaveTemporal">
<!--BOX PROCESO -->
<script type="text/javascript">

		$(document).ready(function(){
			/*var url = "/vseg-paris/secure/cambio-clave/cambio-clave-temporal.do";
			var params = "?desplegarClaveTemporal='true'";
			$.fn.colorbox({
			     iframe:true,
			     width:"400",
			     height:"300",
			     href: url + params,
			     overlayClose: false,
			     escKey: false,
			     close: ""
   			});*/
		
		
		$("#olvidasteClave").fancybox({ 
		    'onStart'   : function() {
					var url = "/vseg-paris/secure/cambio-clave/cambio-clave-temporal.do";
					var params = "?desplegarClaveTemporal='true'";
		     $("#olvidasteClave").attr("href",url+params);
		     
		    },
		    'width'    : 440,
		    'height'   : 240,
		    'autoScale'   : false,
		    'transitionIn'  : 'none',
		    'transitionOut'  : 'none',
		    'type'    : 'iframe',
		    'scrolling'  : 'no' 
		  });
   			});
		
</script>
</logic:present>
