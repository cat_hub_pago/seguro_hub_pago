<%@page language="java" pageEncoding="ISO-8859-1" %>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals" %>
				
				
				<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
				<!-- Desa  <div class="g-recaptcha" data-sitekey="6LdPQgoUAAAAAPeJOWHQqnDlHc7WovPIxUBmM33D"></div>
				<!-- Test <div class="g-recaptcha" data-sitekey="6LdOYScTAAAAAEG_L0pnI3r3JSJB6C3ASBifqyes"></div>-->
				<!-- Produccion <div class="g-recaptcha" data-sitekey="6Ld9ARUTAAAAAAkrHBRvrB_eTcMhJVDJBmDY5goB"></div>-->
				<div class="row u-mb50">
			  	<div class="col-lg-12">
                      <div class="o-form__field">
                       <div class="g-recaptcha" data-sitekey="6LdPQgoUAAAAAPeJOWHQqnDlHc7WovPIxUBmM33D"></div>
                      </div>
                </div>
                </div>
				<div id="globalExceptionDiv">
					<logic:present name="<%=Globals.EXCEPTION_KEY%>">
						<div id="globalExceptionDivIframe">
							<div class="alert alert-warning top15">
								<p align="center">
									<strong><bean:write
											name="<%=Globals.EXCEPTION_KEY%>" property="message"
											ignore="true" /> </strong>
								</p>
							</div>
						</div>
						<script type="text/javascript">
							if($("#paso2")) {
								$("#paso2").show("fast");
							} else {
								$("#paso1").show("fast");
							}							
							window.parent.createDivTable('globalExceptionDivIframe', 'globalExceptionDiv');
							window.parent.$("#captcha-container").hide();
						</script>
					</logic:present>
				</div>
				
				<logic:equal value="1" name="idRama">
				<logic:equal name="idSubcategoria" value="22">										
				<div class="row o-actions o-actions--flex">
									                    <div class="col-lg-3">
									                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="switchForm(false);"><i class="mx-arrow-left"></i> Volver</a></div>
									                    </div>
									                    <div class="col-lg-3 offset-lg-6">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitVehiculo();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
				</div>
				</logic:equal>
				<logic:notEqual name="idSubcategoria" value="22">		
				<div class="row o-actions o-actions--flex">
									                    <div class="col-lg-3">
									                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="switchForm(false);"><i class="mx-arrow-left"></i> Volver</a></div>
									                    </div>
									                    <div class="col-lg-3 offset-lg-6">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitVehiculo();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
				</div>
				</logic:notEqual>
				</logic:equal>
				<logic:equal value="8" name="idRama">
				<div class="row o-actions o-actions--flex">
									                    <div class="col-lg-3">
									                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="switchForm(false);"><i class="mx-arrow-left"></i> Volver</a></div>
									                    </div>
									                    <div class="col-lg-3 offset-lg-6">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitVehiculo();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
				</div>
				</logic:equal>
				<logic:equal value="7" name="idRama">
				<div class="row o-actions o-actions--flex">
									                    <div class="col-lg-3">
									                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="switchForm(false);"><i class="mx-arrow-left"></i> Volver</a></div>
									                    </div>
									                    <div class="col-lg-3 offset-lg-6">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitFraude();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
				</div>
				</logic:equal>
				<logic:equal value="5" name="idRama">
				<div class="row o-actions o-actions--flex">
									                  <!--  <div class="col-lg-3">
									                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="switchForm(false);"><i class="mx-arrow-left"></i> Volver</a></div>
									                    </div> -->
									                    <div class="col-lg-3 offset-lg-9 u-text-right">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitFraude();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
				</div>
				</logic:equal>
				<logic:equal value="6" name="idRama">
					<logic:equal name="idSubcategoria" value="140">	
				<div class="row o-actions o-actions--flex">
														<div class="col-lg-3">												
															<div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="switchForm(false);"><i class="mx-arrow-left"></i> Volver</a></div>      
														</div>
									                    <div class="col-lg-3 offset-lg-6">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitFraude();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
					</div> 
					</logic:equal>
					<logic:notEqual name="idSubcategoria" value="140">
							<div class="row o-actions o-actions--flex">
														
									                    <div class="col-lg-3 offset-lg-9 u-text-right">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitFraude();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
								</div> 
					</logic:notEqual>
				</logic:equal>
				<logic:equal value="2" name="idRama">
				<div class="row o-actions o-actions--flex">
									                    <div class="col-lg-3">
									                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="switchForm(false);"><i class="mx-arrow-left"></i> Volver</a></div>
									                    </div>
									                    <div class="col-lg-3 offset-lg-6">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="javascript:submitHogar();">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
				</div>
				</logic:equal>

	<!--[if lt IE 7]>
		<script type="text/javascript">recargar();</script>
	<![endif]-->
	
	