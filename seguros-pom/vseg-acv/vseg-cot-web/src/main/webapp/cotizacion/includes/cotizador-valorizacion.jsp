<%@page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.cencosud.acv.common.Plan"%>
<%@page import="java.util.Locale"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<logic:notEmpty name="planesCotizados">
	<script type="text/javascript">
		$(document).ready(function() {
			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push({
				"content" : "/cotizador/desplegar-cotizacion.do/resultado",
				"event" : "pageview"
			});
		});
	
	</script>
</logic:notEmpty>

<%session.setAttribute("currentLocale", new Locale("es", "CL"));%>



<div id="grupo_valorizacion">
			<html:form styleId="guardarCotizacion" action="/guardar-cotizacion"
				method="post" target="_self">
				<html:hidden property="datos(enviar)" styleId="enviar" />
				<html:hidden property="datos(producto)" styleId="idProducto" />
				<html:hidden property="datos(idPlan)" styleId="idPlan" />
				<html:hidden property="datos(nombrePlan)" styleId="nombrePlan" />
				<html:hidden property="datos(primaPlanPesos)" styleId="primaPlanPesos" />
				<html:hidden property="datos(primaPlanUF)" styleId="primaPlanUF" />
				<html:hidden property="datos(primaAnualPesos)"
					styleId="primaAnualPesos" />
				<html:hidden property="datos(primaAnualUF)" styleId="primaAnualUF" />
				<logic:notEmpty name="planesCotizados">
					
					<!-- nombre -->
             <h2 class="o-title o-title--subtitle u-mb10">Tenemos los siguientes seguros para ti<span class="o-title--subtitle o-title--secundary"></span></h2>
                <logic:equal name="idRama" value="1">
                <logic:equal value="22" name="idSubcategoria">
                <div class="o-collapse"><a class="o-collapse__button o-btn o-btn--outline o-btn--secundary o-btn--icon u-mb0" href="#" state="0" onclick="collapse(event);"> Filtros<i class="o-icon o-icon--triangle"><span class="u-triangle_down"></span></i></a>
                  <div id="filtro" class="o-collapse__content u-mb0">
                    <div class="c-filter">
                      <div class="row">
                        <!--<div class="col-lg-3 u-bdr_r"><span class="o-help">Valor cuota mensual<i class="o-help__icon" data-tooltip="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                              <svg width="11px" height="17px" viewbox="0 0 11 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="Cotizaci�n-Paso-3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <g id="Desktop" transform="translate(-355.000000, -434.000000)" fill="#FFFFFF">
                                    <g id="Valor-cuota-mensual" transform="translate(201.000000, 432.000000)">
                                      <g id="Group-2" transform="translate(154.000000, 2.000000)">
                                        <path id="Path" d="M5.49009823,16.4153497 C4.744,16.4153497 4.13949705,15.8108468 4.13949705,15.0647485 C4.13949705,14.3199705 4.744,13.7154676 5.49009823,13.7154676 C6.23487623,13.7154676 6.83937917,14.3199705 6.83937917,15.0647485 C6.83937917,15.8108468 6.23487623,16.4153497 5.49009823,16.4153497 Z"></path>
                                        <path id="Path" d="M7.87134971,9.96657564 C7.32060511,10.2694872 6.80133988,10.8687092 6.80133988,11.1126228 C6.80133988,11.836442 6.21387623,12.4239057 5.49009823,12.4239057 C4.76632024,12.4239057 4.17885658,11.836442 4.17885658,11.1126228 C4.17885658,9.44731041 5.70517289,8.16490766 6.60730648,7.66924165 C7.93430845,6.93754224 8.11262279,6.14032613 8.11262279,5.45846562 C8.11262279,3.53224165 6.46963065,3.24504912 5.49009823,3.24504912 C4.22733399,3.24504912 2.86757367,4.07378585 2.86757367,5.89509234 C2.86757367,6.61891159 2.28011002,7.20633399 1.55633202,7.20633399 C0.832554028,7.20633399 0.245090373,6.61887033 0.245090373,5.89509234 C0.245090373,2.47007073 2.94761297,0.622524558 5.49013949,0.622524558 C8.10086444,0.622524558 10.7351886,2.11736149 10.7351886,5.45846562 C10.7351473,7.40304912 9.77265422,8.92148527 7.87134971,9.96657564 Z"></path>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </svg></i></span>
                          <div class="row u-mt40">
                            <div class="col-12">
                            <div class="o-slider" id="slider-range-min"></div>
                            </div>
                          </div>
                        </div>-->
                        <div class="col-lg-3 u-bdr_r"><span class="o-help">Deducible (UF)<i class="o-help__icon" data-tooltip="Selecciona el deducible asociado a tu plan">
                              <svg width="11px" height="17px" viewbox="0 0 11 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="Cotizaci�n-Paso-3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <g id="Desktop" transform="translate(-355.000000, -434.000000)" fill="#FFFFFF">
                                    <g id="Valor-cuota-mensual" transform="translate(201.000000, 432.000000)">
                                      <g id="Group-2" transform="translate(154.000000, 2.000000)">
                                        <path id="Path" d="M5.49009823,16.4153497 C4.744,16.4153497 4.13949705,15.8108468 4.13949705,15.0647485 C4.13949705,14.3199705 4.744,13.7154676 5.49009823,13.7154676 C6.23487623,13.7154676 6.83937917,14.3199705 6.83937917,15.0647485 C6.83937917,15.8108468 6.23487623,16.4153497 5.49009823,16.4153497 Z"></path>
                                        <path id="Path" d="M7.87134971,9.96657564 C7.32060511,10.2694872 6.80133988,10.8687092 6.80133988,11.1126228 C6.80133988,11.836442 6.21387623,12.4239057 5.49009823,12.4239057 C4.76632024,12.4239057 4.17885658,11.836442 4.17885658,11.1126228 C4.17885658,9.44731041 5.70517289,8.16490766 6.60730648,7.66924165 C7.93430845,6.93754224 8.11262279,6.14032613 8.11262279,5.45846562 C8.11262279,3.53224165 6.46963065,3.24504912 5.49009823,3.24504912 C4.22733399,3.24504912 2.86757367,4.07378585 2.86757367,5.89509234 C2.86757367,6.61891159 2.28011002,7.20633399 1.55633202,7.20633399 C0.832554028,7.20633399 0.245090373,6.61887033 0.245090373,5.89509234 C0.245090373,2.47007073 2.94761297,0.622524558 5.49013949,0.622524558 C8.10086444,0.622524558 10.7351886,2.11736149 10.7351886,5.45846562 C10.7351473,7.40304912 9.77265422,8.92148527 7.87134971,9.96657564 Z"></path>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </svg></i></span>
                              	<div id="link_cotizador_deducible" class="row u-mt20">
                              	</div>
                        </div>
                        <!-- se comenta filtro que no tienen funcionalidad (aun) -->
                        <!--<div class="col-lg-6"><span class="o-help">Aseguradoras</span>
                          <div class="row u-mt20">
                        <div class="col-6 col-md-4 u-mb10"><span class="o-checkbox">
                                <input class="o-checkbox__input" type="checkbox" id="Zenit Seguros">
                                <label class="o-checkbox__label" for="Zenit Seguros">Zenit Seguros</label></span></div>
                            <div class="col-6 col-md-4 u-mb10"><span class="o-checkbox">
                                <input class="o-checkbox__input" type="checkbox" id="HDI Seguros">
                                <label class="o-checkbox__label" for="HDI Seguros">HDI Seguros</label></span></div>
                            <div class="col-6 col-md-4 u-mb10"><span class="o-checkbox">
                                <input class="o-checkbox__input" type="checkbox" id="PENTA">
                                <label class="o-checkbox__label" for="PENTA">PENTA</label></span></div>
                            <div class="col-6 col-md-4 u-mb10"><span class="o-checkbox">
                                <input class="o-checkbox__input" type="checkbox" id="Sura">
                                <label class="o-checkbox__label" for="Sura">Sura</label></span></div>
                            <div class="col-6 col-md-4 u-mb10"><span class="o-checkbox">
                                <input class="o-checkbox__input" type="checkbox" id="Consorcio">
                                <label class="o-checkbox__label" for="Consorcio">Consorcio</label></span></div>
                            <div class="col-6 col-md-4 u-mb10"><span class="o-checkbox">
                                <input class="o-checkbox__input" type="checkbox" id="MAPFRE">
                                <label class="o-checkbox__label" for="MAPFRE">MAPFRE</label></span></div>
                            <div class="col-6 col-md-4 u-mb10"><span class="o-checkbox">
                                <input class="o-checkbox__input" type="checkbox" id="MAPFRE2">
                                <label class="o-checkbox__label" for="MAPFRE2">MAPFRE</label></span></div> 
                          </div>
                        </div>-->
                      </div>
                    </div>
                  </div>
                </div>
                </logic:equal>
                </logic:equal>
	  			<logic:equal name="idRama" value="1">
                <logic:equal value="22" name="idSubcategoria">
					<section class="c-pagination">
	                  <div class="row">
	                    <div class="col-6 col-md-6 col-lg-9">
	                      <p class="c-pagination__total"></p>
	                    </div>
	                    <div class="col-6 col-lg-3">
	                      <div class="o-form o-form--filter o-form--border">
	                        <div class="o-form__field">
	                          <select class="o-form__select" name="ordenar-por" id="ordenar-por"
	                          onchange="if(this.value!=='0') {formatearSalida(this.value);}">
	                            <option value="0">Ordenar por...</option>
								<option value="1">Menor a Mayor</option>
								<option value="2">Compa&ntilde;&iacute;a</option>
								<option value="3">Deducible</option>
	                          </select>
	                        </div>
	                      </div>
	                    </div>
	                  </div>
	                </section>
				</logic:equal>
                </logic:equal>
                
                <section class="c-result">
                 
                  <div id="d_valorizacion"></div>
                    
                </section>
                
					<div class="row u-mt30 o-actions o-actions--flex">
						<logic:notEqual value="3" name="idRama">
							<logic:equal value="6" name="idRama">
									<div class="col-lg-3">
									<div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="$('#paso3').hide();$('#paso2').show();$('#captcha-container').show();"><i class="mx-arrow-left"></i> Volver</a></div>
								</div>
								
								
						</logic:equal>
						<logic:notEqual value="6" name="idRama">
								<div class="col-lg-3">
									<div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="" id="full_volver_paso2" onclick="$('#paso3').hide();$('#paso2').show();$('#captcha-container').show();"><i class="mx-arrow-left"></i> Volver</a></div>
								</div>
							
							
						</logic:notEqual>

						</logic:notEqual>
						<!--<div class="col-lg-3 offset-lg-6"><a class="c-pagination__link" href="#"><i class="o-icon o-icon--circle_line o-icon--primary">
                        <svg width="13px" height="10px" viewbox="0 0 13 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <g id="email" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Desktop" transform="translate(-225.000000, -590.000000)" fill-rule="nonzero" fill="#FB802F">
                              <g id="Envio-+-Resultados" transform="translate(220.000000, 573.000000)">
                                <g id="Env�o-cotizaci�n-+-Filtro" transform="translate(0.000000, 10.000000)">
                                  <g id="Group" transform="translate(0.000000, 1.000000)">
                                    <g id="mail-icon" transform="translate(5.600000, 6.644444)">
                                      <path id="Shape" d="M0.957649787,0.578589136 L5.31945549,4.03360525 C5.51724516,4.19009818 5.78980368,4.25965059 6.05453755,4.24660952 C6.31883672,4.25965059 6.59139524,4.19053288 6.78918492,4.03360525 L11.1509906,0.578589136 C11.5004915,0.303422402 11.4213756,0.0778117621 10.977979,0.0778117621 L6.05497226,0.0778117621 L1.13196552,0.0778117621 C0.688134186,0.0778117621 0.609018316,0.303422402 0.957649787,0.578589136 Z"></path>
                                      <path id="Shape" d="M11.46615,1.49016045 L6.69963618,5.10905944 C6.52140813,5.24294783 6.28797284,5.30815322 6.05540696,5.3051103 C5.82240637,5.30815322 5.58897109,5.24251313 5.41074303,5.10905944 L0.64335982,1.49016045 C0.289511919,1.22194896 0,1.36583551 0,1.80966685 L0,7.74726941 C0,8.19110074 0.363411358,8.5545121 0.807242694,8.5545121 L6.05453755,8.5545121 L11.3022671,8.5545121 C11.7460985,8.5545121 12.1095098,8.19110074 12.1095098,7.74726941 L12.1095098,1.80966685 C12.1095098,1.36583551 11.8199979,1.22194896 11.46615,1.49016045 Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </g>
                        </svg></i> Enviar cotizaciones</a></div>-->
					</div>
		
					<!-- INICIO Modal Enviar Cotizacion -->
					<div class="modal fade" id="modalEnviarCotizacion" role="dialog"
						style="padding-top: 20px;">
						<div class="modal-dialog" style="margin-top: 20px;">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4>Enviar Cotizacion</h4>
								</div>
								<div class="modal-body">
									<iframe class="" frameborder="0" id="desplegarCotizacion"
										width="98%" height="370px"> </iframe>
								</div>
		     				</div>   
						</div>
					</div>
		<!-- FIN Modal Enviar Cotizacion -->
					
					<logic:equal value="3" name="idRama">
						<div class="col-md-12 top15 hidden-xs">
							<button type="button" class="btn btn-primary"
								onclick="$('#paso3').hide();$('#paso1').show();$('#captcha-container').show();">Volver</button>
						</div>
						<div class="col-md-12 p0 hidden-lg text-center">
							<button type="button" class="btn btn-primary top15"
								onclick="$('#paso3').hide();$('#paso1').show();$('#captcha-container').show();">Volver</button>
						</div>
					</logic:equal>
				</logic:notEmpty>
			</html:form>
		</div>

	

            
<!--CBMCBMCBMCBMCBM -->
		
<br>
<logic:notEmpty name="planesCotizados">
	<script type="text/javascript">
	var logos = new Array();
	logos['LIBERTY GENERALES'] = 'brand-libertyseguros.png';
	logos['MAGALLANES'] = 'logo-magallanes.png';
	logos['AIG'] = 'logo-aig.png';
	logos['RSA SEGUROS (CHILE)'] = 'brand-segurossura.png';
	logos['CARDIF'] = 'brand-bnpparibascardif.png';
	logos['ZENIT'] = 'brand-zenitseguros.png';
	logos['INTERAMERICANA'] = 'logo-metlife.png';
	logos['CONSORCIO GENERALES'] = 'brand-consorcio.png';
	logos['ACE SEGUROS S.A'] = 'logo-ace.png';
	logos['Banchile Seguros de Vida S.A'] = 'logo-banchile.png';
	logos['ZENIT SEGUROS GENERALES S.A.'] = 'logo-zenit.png';
	logos['METLIFE'] = 'brand-metlife.png';
	logos['MAPFRE'] = 'brand-mapfreseguros.png';
	logos['PENTA'] = 'brand-pentaseguros.png';
	logos['BICE VIDA'] = 'brand-bicevida.png';
	logos['ITAU CHILE SEGUROS DE VIDA S.A'] = 'logo-itau.png';
	
	var mensajes = new Array();
	mensajes['DEFAULT'] = 'Los valores en pesos han sido calculados en base a la UF del d�a de hoy, considerar que �stos variar�n seg�n el comportamiento de la UF.<br /> Esta Cotizaci�n es v�lida s�lo para compras por internet y tiene vigencia de <span>24 horas</span>';
	mensajes['INTERAMERICANA'] = '<p>La compa��a que asume los riesgos es La Interamericana Compa��a de Seguros de Vida S.A., RUT N� 99.289.000-2, la cual es una sociedad an�nima especial constituida bajo las leyes de Chile y controlada indirectamente por MetLife Inc. </p>';
	
	var mensajesMostrar = new Array();
	mensajesMostrar['DEFAULT'] = 'true';
	mensajesMostrar['LIBERTY GENERALES'] = 'false';
	mensajesMostrar['MAGALLANES'] = 'false';
	mensajesMostrar['AIG'] = 'false';
	mensajesMostrar['RSA SEGUROS (CHILE)'] = 'false';
	mensajesMostrar['CARDIF'] = 'false';
	mensajesMostrar['ZENIT'] = 'false';
	mensajesMostrar['INTERAMERICANA'] = 'false';
	mensajesMostrar['CONSORCIO GENERALES'] = 'false';
	mensajesMostrar['ACE SEGUROS S.A'] = 'false';
	mensajesMostrar['Banchile Seguros de Vida S.A'] = 'false';
	mensajesMostrar['ZENIT SEGUROS GENERALES S.A.'] = 'false';
	mensajesMostrar['METLIFE'] = 'false';
	mensajesMostrar['MAPFRE'] = 'false';
	mensajesMostrar['PENTA'] = 'false';
	mensajesMostrar['BICE VIDA'] = 'false';
	mensajesMostrar['ITAU CHILE SEGUROS DE VIDA S.A'] = 'false';

	var contadorplanes = 0;
	var isHabilitadoComparador = false;
	var planes = new Array();
	var idPlanes = '';
	var tipo = '<bean:write name="idRama"/>';

	if (tipo == '1' || tipo== '8') {
		formname = 'formularioVehiculo';
		urlvaloriza = "/cotizador/valorizar-plan-vehiculo.do";
	} else if (tipo == '2') {
		formname = 'cotizarProductoForm';
		urlvaloriza = "/cotizador/valorizar-plan-hogar.do";
	} else if (tipo == '3') {
		formname = 'cotizarProducto';
		urlvaloriza = "/cotizador/valorizar-plan-vida.do";
	} else if (tipo == '4') {
		formname = 'cotizarProductoForm';
		urlvaloriza = "/cotizador/valorizar-plan-salud.do";
	} else if (tipo == '5' || tipo == '6' || tipo == '7') {
		formname = 'cotizarProductoForm';
		urlvaloriza = "/cotizador/valorizar-plan-fraude.do";
	}	
	
	$(document).ready(function(){
	/**
	* Obtiene todos los planes con el deducible 0UF = 188
	*/
		url = "/cotizador/obtener-planes.do";
		$.getJSON(url, window.parent.$("#" + formname).serialize(),
			function(e){
				contadorplanes = e.planesCotizados.length;
				//alert('Los planes son '+contadorplanes);
				isHabilitadoComparador = e.isComparadorHabilitado;
				$.each(e.planesCotizados, 
					function(a){
						//Valorizar cada plan obtenido
						//e[a].idPlan, e[a].idPlanLegacy
						//setTimeout('valorizarPlan(\'' + e.planesCotizados[a].idPlan + '\', \'' + a + '\')',25000);
						//valorizarPlan(e.planesCotizados,0);
						if(a == 0){
						//idPlanes = idPlanes.concat(e.planesCotizados[a].idPlan); 
						idPlanes=idPlanes+e.planesCotizados[a].idPlan;
						}else{
						//idPlanes = idPlanes.concat(","+e.planesCotizados[a].idPlan+""); 
						idPlanes=idPlanes+','+e.planesCotizados[a].idPlan+'';
						}
					}
				)
				valorizarPlan(idPlanes,0);
				//setTimeout('$(#input03).click()', 4000);
			}
		)		
	});

	function valorizarPlan(idPlan, pos) {
	/**
	* Obtiene los valores de cada plan
	*/
		window.parent.$("#idPlanValorizacion").val(idPlan);		
		$.ajax({
			url: urlvaloriza,
			type: "POST",
			dataType: "json",
			data: window.parent.$("#"+formname).serialize(),
			success: function(j) {
				//j contiene el plan valorizado.
				planes = j;
				//alert(planes);
				$.each(planes, 
					function(c){
						//alert(planes[c].idProducto);
					}
				);
				validarDespliegue();
			},
			error: function(req, status, error) {
				//En caso de error se debe descontar 1 del contador.
				validarDespliegue();
			}
		});
	}
	
	function validarDespliegue() {

		contadorplanes = contadorplanes - 1;
		
		//if(contadorplanes <= 0) {

			//Borrar planes con valorizacion igual a cero.
			borrarEnCero();
			$.session("planesValorizados", planes);
			$.session('testPlanes',planes);
			planes = null;
			//Formatear salida.
			formatearSalida(1,'SI',false,false,false,false);
						
			//Desplegar planes.
			desplegarCoberturas();
			
			
		//}
	}
	
	function borrarEnCero() {
		var cnt = 0;
		var planesSalida = new Array();
		$.each(planes, 
			function(idx, plan) {
				if(plan != null){
					var primaMensualPesos = plan.primaMensualPesos * 1;
					if(primaMensualPesos > 0) {
						planesSalida[cnt] = plan;
						cnt = cnt + 1;
					}
				}
			}
		)
		planes = planesSalida;
	}

	var sort_by = function(field, reverse, primer){
		reverse = (reverse) ? -1 : 1;
		
		return function(a,b){
			a = a[field];
			b = b[field];
			
			if (typeof(primer) != 'undefined'){
				a = primer(a);
				b = primer(b);
			}

			if (a<b) return reverse * -1;
			if (a>b) return reverse * 1;
			return 0;
		}
	}

	var sort_by_compannia = function(field,reverse){
		reverse = (reverse) ? -1 : 1;
		return function(a,b){
			a = a[field];
			b = b[field];
			
			if (a<b) return reverse * -1;
			if (a>b) return reverse * 1;
			return 0;
		}
	}
	
	function ordenarPlanes(e, planesSinOrdenar){
		if (e == 1){			
			planesSinOrdenar.sort(sort_by('primaMensualPesos', false, parseInt));
		} else if (e == 2){
			planesSinOrdenar.sort(sort_by_compannia('compannia',false));
		} else if (e == 3){
			planesSinOrdenar.sort(sort_by('idProducto', false, parseInt));
		}
	}
	
	function formatearSalida(optOrder,primeraVez,checkeado00,checkeado03,checkeado05,checkeado10) {
		$('#d_valorizacion').empty();
		
		var planesValorizados = $.session("planesValorizados");
		ordenarPlanes(optOrder, planesValorizados);

		console.debug("planes:" + planesValorizados.length);
		if(planesValorizados.length == 0) {
			
			$('#grupo_valorizacion').css("display", "none");
			if($("#paso2")) {
				console.debug("mostrando paso2");
				$("#paso2").show("fast");
			} else {
				console.debug("mostrando paso1");
				$("#paso1").show("fast");
			}
			window.parent.document.getElementById('globalExceptionDivPlanes').innerHTML = 
				'<div class="col-lg-12" id="globalExceptionDivIframePlanes">'+
					'<div class="row">'+
						'<div class="alert alert-warning top15">'+
							'<p align="center"><strong>No existen planes asociados.</strong></p>'+
						'</div>'+
					'</div>'+
				'</div>'+
			<logic:notEqual value="3" name="idRama">
				<logic:equal value="6" name="idRama">
					'<div class="volverSinPlanes" >'+
						'<div class="col-md-12 top15 hidden-xs "  >'+
							'<button type="button" class="btn btn-primary" onclick="switchForm(false);$(\'.volverSinPlanes\').hide();$(\'#globalExceptionDivIframePlanes\').hide();$(\'#paso3\').hide();$(\'#captcha-container\').show();">Volver</button>'+
						'</div>'+
						'<div class="col-md-12 p0 hidden-lg text-center" >'+
							'<button type="button" class="btn btn-primary top15" onclick="$(\'.volverSinPlanes\').hide();$(\'#globalExceptionDivIframePlanes\').hide();$(\'#paso3\').hide();$(\'#paso2\').show();$(\'#captcha-container\').show();">Volver</button>'+
						'</div>'+
					'</div>';
				</logic:equal>
				<logic:notEqual value="6" name="idRama">
					'<div class="volverSinPlanes" >'+
						'<div class="col-md-12 top15 hidden-xs "  >'+
							'<button type="button" class="btn btn-primary" onclick="$(\'.volverSinPlanes\').hide();$(\'#globalExceptionDivIframePlanes\').hide();$(\'#paso3\').hide();$(\'#paso2\').show();$(\'#captcha-container\').show();">Volver</button>'+
						'</div>'+
						'<div class="col-md-12 p0 hidden-lg text-center" >'+
							'<button type="button" class="btn btn-primary top15" onclick="$(\'.volverSinPlanes\').hide();$(\'#globalExceptionDivIframePlanes\').hide();$(\'#paso3\').hide();$(\'#paso2\').show();$(\'#captcha-container\').show();">Volver</button>'+
						'</div>'+
					'</div>';
				</logic:notEqual>
			</logic:notEqual>
			<logic:equal value="3" name="idRama">
				'<div class="volverSinPlanes" >'+
					'<div class="col-md-12 top15 hidden-xs" >'+
						'<button type="button" class="btn btn-primary" onclick="$(\'.volverSinPlanes\').hide();$(\'#globalExceptionDivIframePlanes\').hide();$(\'#paso3\').hide();$(\'#paso1\').show();$(\'#captcha-container\').show();">Volver</button>'+
					'</div>'+
					'<div class="col-md-12 p0 hidden-lg text-center">'+
						'<button type="button" class="btn btn-primary top15" onclick="$(\'.volverSinPlanes\').hide();$(\'#globalExceptionDivIframePlanes\').hide();$(\'#paso3\').hide();$(\'#paso1\').show();$(\'#captcha-container\').show();">Volver</button>'+
					'</div>'+
				'</div>';
			</logic:equal>
		}
		
		if (isHabilitadoComparador && typeof(planesValorizados) != 'undefined' && planesValorizados.length > 1) {
			isHabilitadoComparador = true;
		} else {
			isHabilitadoComparador = false;
		}
		
// 		$('#para-continuar').hide();
		
		/*if (isHabilitadoComparador) {
			$('#para-continuar').show();
		} else {
			$('#para-continuar').hide();
		}*/

		var idPlanAux = null;
		$.each(planesValorizados, 
			function(idx, itm) {
			  	var producto = itm.idProducto;
			  	//alert(producto);
			  	console.log(producto);
			
				if(itm != null) {
					if (idPlanAux != itm.idPlan){ 
					idPlanAux = itm.idPlan;
// 					window.parent.document.getElementById('botonCotizarDiv').style.display='none';
					cargarCheck(itm.idProducto);
					mensajesMostrar[itm.compannia] = 'true';
					var v_deducible;
					var html_contenido;
					switch(producto) {
						case 188: 
							v_deducible = '00';
							break;
						case 189:
							v_deducible = '03';
							break;
						case 190:
							v_deducible = '04';
							break;
						case 191:
							v_deducible = '05';
							break;
						case 192:
							v_deducible = '08';
							break;
						case 88:
							v_deducible = '10';
							break;
					}
					
					<!-- CBM , validacion para cargar s�lo los planes cuyo deducible se encuentre seleccionado -->
					var checkeado = false;
					
					if(v_deducible == '00'){if(checkeado00){checkeado=true}};
					if(v_deducible == '03'){if(checkeado03){checkeado=true}};
					if(v_deducible == '05'){if(checkeado05){checkeado=true}};
					if(v_deducible == '10'){if(checkeado10){checkeado=true}};
/*
*	Pleyasoft
*	Se comenta validacion if( (primeraVez == 'SI')  || (checkeado)) - para el desplegar seg�n el orden en Vehiculo
*/

/*
					if( (primeraVez == 'SI')  || (checkeado)){
*/
					<!-- Valorizaci�n -->

					if (itm.idPlan == 1872 || itm.idPlan == 1873 || itm.idPlan == 1874 || itm.idPlan == 1875 || itm.idPlanLegacy == 3028 || itm.idPlanLegacy == 3029 ||
					    itm.idPlanLegacy == 3030 ||itm.idPlanLegacy == 3031 ){
					    if(v_deducible){
					    	html_contenido = '<div class="c-sure c-sure--highlight visible'+v_deducible+'" id="deducible'+v_deducible+'">';
					    } else {
					    	html_contenido = '<div class="c-sure c-sure--highlight visible" id="deducible">';
					    }
					    	html_contenido += '<span class="c-sure__tag"><i class="c-sure__tag_icon">';
                        html_contenido += '<svg width="24px" height="24px" viewbox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">';
                          html_contenido += '<defs>';
                            html_contenido += '<ellipse id="path-1" cx="9.57130074" cy="9.68039092" rx="9.57130074" ry="9.59994429"></ellipse>';
                            html_contenido += '<filter id="filter-2" x="-20.9%" y="-10.4%" width="141.8%" height="141.7%" filterunits="objectBoundingBox">';
                              html_contenido += '<feoffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feoffset>';
                              html_contenido += '<fegaussianblur stddeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></fegaussianblur>';
                              html_contenido += '<fecolormatrix values="0 0 0 0 0.464126276   0 0 0 0 0.464126276   0 0 0 0 0.464126276  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></fecolormatrix>';
                            html_contenido += '</filter>';
                          html_contenido += '</defs>';
                          html_contenido += '<g id="icon_seccess" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">';
                            html_contenido += '<g id="Desktop" transform="translate(-188.000000, -659.000000)">';
                              html_contenido += '<g id="success-illustration" transform="translate(190.000000, 659.000000)">';
                                html_contenido += '<g id="Group-5" transform="translate(0.152373, 0.189265)">';
                                  html_contenido += '<g id="Group-4" transform="translate(0.000000, 0.136339)">';
                                    html_contenido += '<g id="Oval-2-Copy-19">';
                                      html_contenido += '<use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use>';
                                      html_contenido += '<use fill="#30CC7B" fill-rule="evenodd" xlink:href="#path-1"></use>';
                                    html_contenido += '</g>';
                                    html_contenido += '<path id="Shape-Copy" d="M14.1775823,6.68811508 L13.916536,6.4240777 C13.6299624,6.13326729 13.1604437,6.13326729 12.8729584,6.4240777 L8.44094485,10.9127133 L6.86828506,9.32079556 C6.5817115,9.03029288 6.11188888,9.03029288 5.82531532,9.32110329 L5.56457289,9.58483294 C5.27799933,9.87533562 5.27799933,10.3507876 5.56457289,10.6412902 L7.9179405,13.0268588 C8.20481796,13.3170537 8.67433667,13.3170537 8.96121413,13.0268588 L14.1775823,7.74457237 C14.463852,7.4540697 14.463852,6.97861776 14.1775823,6.68811508 Z" fill="#FFFFFF" fill-rule="nonzero"></path>';
                                  html_contenido += '</g>';
                                html_contenido += '</g>';
                              html_contenido += '</g>';
                            html_contenido += '</g>';
                          html_contenido += '</g>';
                        html_contenido += '</svg></i>';
                      	html_contenido += '<p class="c-sure__tag_text">Full Cobertura Auto Play</p></span>';
					    }else{
					    if(v_deducible){
					    	html_contenido = '<div class="c-sure visible'+v_deducible+'" id="deducible'+v_deducible+'">';
						    } else {
						    	html_contenido = '<div class="c-sure visible">';
						    }
					   }
					
				    if(v_deducible) {
						html_contenido += '<div class="c-sure__company"><img src="/cotizador/images/' + logos[itm.compannia] + '" alt="' + itm.compannia + '"></div>';
					} else {
						html_contenido += '<div class="c-sure__company"><img src="/cotizador/images/' + logos[itm.compannia] + '" alt="' + itm.compannia + '"></div>';
					}
                    html_contenido += '<div class="c-sure__data">';
                    html_contenido += '<div class="row">';  
                    html_contenido += '<div class="col-12 col-lg-7">';  
                    if(v_deducible){
                    	html_contenido += '<h2 class="c-sure__name">'+itm.nombrePlan+'</h2><span class="c-sure__deductible u-hidden_mobile">Deducible '+v_deducible+'UF';      
                    } else {
                    	html_contenido += '<h2 class="c-sure__name">'+itm.nombrePlan+'</h2><span class="c-sure__deductible u-hidden_mobile">UF '+itm.primaMensualUF+' al mes';                          	
                    }            
                    html_contenido += '</span>';          
                    html_contenido += '</div>';    
                    html_contenido += '<div class="col-12 col-lg-5 u-text-right">';    
                    html_contenido += '<h3 class="c-sure__price"><span class="precio">$ '+itm.primaMensualPesos+'</span><sub>/ mes*</sub><span class="o-help">';  
                    if(v_deducible){
                    html_contenido+= 'Deducible '+v_deducible+'UF';	
                    }               
                    html_contenido += '</span></h3>';     
                    if(v_deducible){
                    html_contenido += '<h4 class="monto_uf">UF '+itm.primaMensualUF+' <span> / mes*</span></h4>';
                    }       
                    html_contenido += '</div>';    
                    html_contenido += '</div>';  
                    html_contenido += '</div>';
                    html_contenido += '<div class="c-sure__actions"><a class="o-btn o-btn--primary o-btn" onclick="javascript:contratar(\''+itm.idPlan+'\', \''+itm.nombrePlan+'\', \''+itm.primaMensualPesos+'\', \''+itm.primaMensualUF+'\', \''+itm.primaAnualUF+'\', \''+itm.primaAnualPesos+'\');">Contratar</a><span><span class="o-checkbox">';
                    html_contenido += '</span></span></div>';
					html_contenido += '</div>';
					
					
					
					<!-- Vista Responsive -->
					
					<!-- fin -->
					$('#d_valorizacion').append(html_contenido);
/*					
					}else{
							
						<!-- CBM , validacion para cargar s�lo los planes cuyo deducible se encuentre seleccionado -->
							if(v_deducible == '00'){check00 = false};
							if(v_deducible == '03'){check03 = false};
							if(v_deducible == '05'){check05 = false};
							if(v_deducible == '10'){check10 = false};						
					}
*/
				}
			}
			}
		)
		var html3 = '';
		if (check00 == true){
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" checked="checked" id="input00" value="00" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input00">00 UF</label></span></div>';
		}else {
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" id="input00" value="00" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input00">00 UF</label></span></div>';
		} 
		if (check03 == true){
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" checked="checked" id="input03" value="03" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input03">03 UF</label></span></div>';
		}else {
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" id="input03" value="03" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input03">03 UF</label></span></div>';
		} 
		if (check05 == true){
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" checked="checked" id="input05" value="05" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input05">05 UF</label></span></div>';
		}else {
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" id="input05" value="05" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input05">05 UF</label></span></div>';
		} 
		if (check10 == true){
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" checked="checked" id="input10" value="10" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input10">10 UF</label></span></div>';
		}else {
			html3 += '<div class="col-6 u-mb10"><span class="o-checkbox"><input class="o-checkbox__input" type="checkbox" id="input10" value="10" onclick="javascript:cargarDeducible();"><label class="o-checkbox__label" for="input10">10 UF</label></span></div>';
		} 
		$("#link_cotizador_deducible").empty();
		$("#link_cotizador_deducible").append(html3);
		$(".precio").formatCurrency({digitGroupSymbol: '.', roundToDecimalPlace: -2, positiveFormat: '%s %n'});
	}
	
	function setTextoPiePagina(){
		var finalMessage = '';
		for(key in mensajesMostrar){
			if(mensajesMostrar[key] == 'true'){
				if(mensajes[key] != undefined && mensajes[key] != ''){
					finalMessage = finalMessage + '<br><br><br><br>' + mensajes[key];
				}
				mensajesMostrar[key] = 'false';
			}
		}
		window.parent.$('#texto-pie').html(finalMessage);
	}
	
	function desplegarCoberturas(){
		window.parent.$('#waitModal').modal('hide');
		window.parent.$('#loading').hide();
		window.parent.$('#paso1').hide();
		window.parent.$('#paso2').hide();
		window.parent.$("#captcha-container").hide();

		window.parent.createDivTable('grupo_valorizacion', 'grupo_valorizacion_iframe');
		window.parent.loadCotizadorValoracionScript();
		
		setTextoPiePagina();
		window.parent.$('#grupo_valorizacion_iframe').show();
		window.parent.$('#paso3').show();
		window.parent.redefineVerCoberturas('grupo_valorizacion_iframe');
	}

</script>
</logic:notEmpty>