<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.cencosud.acv.common.Plan"%>
<%@page import="java.util.Locale"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%session.setAttribute("currentLocale", new Locale("es", "CL"));%> 

<div id="grupo_valorizacion">
<!--  contenido COBERTURAS-->
<script type="text/javascript">
	$(document).ready(function(){
		$.scrollTo("#para-continuar");
		
		$("img[name=guardarEnviar]").colorbox({
				iframe:true, 
				width:"750", 
				height:"630",
				close: "",
				href: "<%=request.getContextPath()%>/guardar-enviar.do?idPlan=" + $(this).attr('id')
			});
		
	});	
	
	function contratar(idPlan, nombrePlan, primaPeso, primaUF, primaAnualUF, primaAnualPesos){
		$("#idProducto").val($("#selectProducto").attr("value"));
		$("#idPlan").val(idPlan);
		$("#nombrePlan").val(nombrePlan);
		$("#primaPlanPesos").val(primaPeso);
		$("#primaPlanUF").val(primaUF);
		$("#primaAnualPesos").val(primaAnualPesos);
		$("#primaAnualUF").val(primaAnualUF);
		$("#enviar").val("");
				
		$("#guardarCotizacion").submit();
		
	}
	
	
	function envio(idPlan, nombrePlan, primaPeso, primaUF, primaAnualUF, primaAnualPesos, enviar){
		
		$("#enviar").val(enviar);
		$("#idProducto").val($("#selectProducto").attr("value"));
		$("#idPlan").val(idPlan);
		$("#nombrePlan").val(nombrePlan);
		$("#primaPlanPesos").val(primaPeso);
		$("#primaPlanUF").val(primaUF);
		$("#primaAnualPesos").val(primaAnualPesos);
		$("#primaAnualUF").val(primaAnualUF);
		
		
		/*$.fn.colorbox({
			iframe: true,
			width:"750", 
			height:"630",
			close: "",
			href: "/cotizador/cotizacion/includes/frame-guardar-enviar.jsp"
		});*/
		$(document).ready(function(){
			$('<a href="/cotizador/cotizacion/includes/frame-guardar-enviar.jsp" class="login_error"></a>').appendTo('body');
			
			$("a.login_error").fancybox({ 
			    'hideOnContentClick': false,
				'frameWidth' : 470,
				'frameHeight' : 185,
		    	'type'  : 'inline' 
			  });
			$("#fancybox-close").click(function(){
				$("#errorlogin").css("display","none");
			});
			$("a.login_error").click();
		});
		}
	
</script>


<!-- descripcion -->
<html:form styleId="guardarCotizacion" action="/guardar-cotizacion" method="post" target="_self">

<html:hidden property="datos(enviar)" styleId="enviar"/>
<html:hidden property="datos(producto)" styleId="idProducto"/>
<html:hidden property="datos(idPlan)" styleId="idPlan"/>
<html:hidden property="datos(nombrePlan)" styleId="nombrePlan"/>
<html:hidden property="datos(primaPlanPesos)" styleId="primaPlanPesos"/>
<html:hidden property="datos(primaPlanUF)" styleId="primaPlanUF"/>
<html:hidden property="datos(primaAnualPesos)" styleId="primaAnualPesos"/>
<html:hidden property="datos(primaAnualUF)" styleId="primaAnualUF"/>


<logic:notEmpty name="planesCotizados">

<div id="para-continuar">
	<p>Para continuar con la cotizaci&oacute;n debes seleccionar uno de los siguientes planes.
		<br />
		<logic:present name="subcategoriaDesc">
			<logic:equal name="subcategoriaDesc" value="Cobertura Argentina">
				<b>Atenci&oacute;n:</b> El seguro te cubre desde el momento en que realizas la contrataci&oacute;n online y permanece vigente por la cantidad de d&iacute;as seleccionados. Dado lo anterior, te recomendamos <span><b>contratar tu seguro al momento de viajar.</b></span>
			</logic:equal>
		</logic:present>
		<logic:notPresent name="subcategoriaDesc">
			No se encuentra en sesion
		</logic:notPresent> 
		<!-- FIX IE6 -->
		<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	</p>
</div>

<div id="d_valorizacion"></div>
<script type="text/javascript">
	var contadorplanes = 0;
	var planes = new Array();
	var tipo = '<bean:write name="idRama"/>';

	if (tipo == '1') {
		formname = 'formularioVehiculo';
		urlvaloriza = "/cotizador/valorizar-plan-vehiculo.do";
	} else if (tipo == '2') {
		formname = 'cotizarProductoForm';
		urlvaloriza = "/cotizador/valorizar-plan-hogar.do";
	} else if (tipo == '3') {
		formname = 'cotizarProducto';
		urlvaloriza = "/cotizador/valorizar-plan-vida.do";
	} else if (tipo == '4') {
		formname = 'cotizarProductoForm';
		urlvaloriza = "/cotizador/valorizar-plan-salud.do";
	} else if (tipo == '5') {
		formname = 'cotizarProductoForm';
		urlvaloriza = "/cotizador/valorizar-plan-cesantia.do";
	} else if (tipo == '6') {
		formname = 'cotizarProductoForm';
		urlvaloriza = "/cotizador/valorizar-plan-fraude.do";
	}

	$(document).ready(function(){
		url = "/cotizador/obtener-planes.do";
	
		$.getJSON(url, $("#" + formname).serialize(),
			function(e){
				contadorplanes = e.length;
				$.each(e, 
					function(a){
						//Valorizar cada plan obtenido
						//e[a].idPlan, e[a].idPlanLegacy
						valorizarPlan(e[a].idPlan, a);
					}
				)
			}
		)
	});
	
	
	function valorizarPlan(idPlan, pos) {

		$("#idPlanValorizacion").val(idPlan);

		$.ajax({
			url: urlvaloriza,
			type: "POST",
			dataType: "json",
			data: $("#"+formname).serialize(),
			success: function(j) {
				//j contiene el plan valorizado.
				planes[pos] = j;
				validarDespliegue();
			},
			error: function(req, status, error) {
				//En caso de error se debe descontar 1 del contador.
				validarDespliegue();
			}
		});
	}
	
	function validarDespliegue() {

		contadorplanes = contadorplanes - 1;
		if(contadorplanes <= 0) {

			//Borrar planes con valorizacion igual a cero.
			borrarEnCero();

			//Formatear salida.
			formatearSalida();
						
			//Desplegar planes.
			desplegarCoberturas();
		}
	}
	
	function borrarEnCero() {
		var cnt = 0;
		var planesSalida = new Array();
		$.each(planes, 
			function(idx, plan) {
				if(plan != null){
					var primaMensualPesos = plan.primaMensualPesos * 1;
					if(primaMensualPesos > 0) {
						planesSalida[cnt] = plan;
						cnt = cnt + 1;
					}
				}
			}
		)
		planes = planesSalida;
	}

	var sort_by = function(field, reverse, primer){
		reverse = (reverse) ? -1 : 1;
		
		return function(a,b){
			a = a[field];
			b = b[field];
			
			if (typeof(primer) != 'undefined'){
				a = primer(a);
				b = primer(b);
			}

			if (a<b) return reverse * -1;
			if (a>b) return reverse * 1;

			return 0;
		}
	}

	function ordenarPlanes(){
		planes.sort(sort_by('primaMensualPesos', false, parseInt));
	}

	function formatearSalida() {
		ordenarPlanes();
		
		$.each(planes, 
			function(idx, itm) { 
				if(itm != null) {
					<!-- Inicio Div contenedor FIX valorizacion IE6 -->
					var d_cont = $('<div/>');
					var d_descripcion = $('<div/>', {class: 'descripcion-cobertura', id: 'contenido-cobertura'}).appendTo(d_cont);

					$('<div/>', {class: 'cont-logo-compania', text: itm.compannia, css: {width: '160px', color: '#2D8CC4', textAlign: 'left'}}).appendTo(d_descripcion);
					$('<div/>', {class: 'cont-seguro', text: itm.nombrePlan, css: {width: '160px', textAlign: 'left'}}).appendTo(d_descripcion);
					var d_coberturas = $('<div/>', {class: 'cont-ver-coberturas'}).appendTo(d_descripcion);
					$('<a/>', {href: '#cob' + idx, class: 'a1', text: 'ver coberturas'}).appendTo(d_coberturas);

					var d_precios = $('<div/>', {class: 'cont-precios-coberturas'}).appendTo(d_descripcion);
					var almes = $('al mes');
					$('<div/>', { class: 'cont-pesos-dos', text: itm.primaMensualPesos}).formatCurrency({digitGroupSymbol: '.', roundToDecimalPlace: -2, positiveFormat: '%s %n'}).appendTo(d_precios,almes);
					$('<div/>', { class: 'cont-uf', text: itm.primaMensualUF}).formatCurrency({digitGroupSymbol: '.', decimalSymbol: ',', roundToDecimalPlace: 4, positiveFormat: '%s %n', symbol: 'UF'}).appendTo(d_precios,almes);


					var d_descr_cobertura = $('<div/>', {class: 'bloque-descripcion-cobertura', 
							mouseover: function(){ $(this).css('cursor', 'pointer')}}
					).appendTo(d_descripcion);
		
					var a_contratar = $('<a/>', {
							click: function(){ contratar(itm.idPlan, itm.nombrePlan, itm.primaMensualPesos, itm.primaMensualUF, itm.primaAnualUF, itm.primaAnualPesos)}}
					).appendTo(d_descr_cobertura);
		
					$('<img/>', {class: 'dsR2', src: 'images/btn-img/btn-naranjo-contratar2.gif', border: '0', alt: '', width: '90', height: '32', 
							mouseover: function(){ $(this).attr('src', 'images/btn-img/btn-naranjo-contratar-hover2.gif')},
							mouseout: function(){ $(this).attr('src', 'images/btn-img/btn-naranjo-contratar2.gif')}}
					).appendTo(a_contratar);

					var d_guardar = $('<div/>', {class: 'cont-guardar-cotizacion'}).appendTo(d_descr_cobertura);
					$('<div/>', {class: 'icono-guardar'}).appendTo(d_guardar);
					var d_guardar_cotizacion = $('<div/>', {class: 'text-guardar-cotizacion', id: 'guardarEnviar' + 'idx'}).appendTo(d_guardar);
					$('<a/>', {text: 'Guardar Cotizacion',
							click: function(){envio(itm.idPlan, itm.nombrePlan, itm.primaMensualPesos, itm.primaMensualUF, itm.primaAnualUF, itm.primaAnualPesos, 'true')}}
					).appendTo(d_guardar_cotizacion);
					<!-- Fin Div contenedor FIX valorizacion IE6 -->
		
					var d_contver = $('<div/>', {css: {display: 'none'}, class: 'contver', id: 'cob' + idx});
					var d_contenedorver = $('<div/>', {id: 'centenedor_ver'}).appendTo(d_contver);
					var d_desplieguever = $('<div/>', {id: 'despliegue_ver'}).appendTo(d_contenedorver);
					var d_ver_titulos_cob = $('<div/>', {class: 'ver-titulos-cob'}).appendTo(d_desplieguever);
					var d_ver_cobertura = $('<div/>', {class: 'ver-cobertura'}).appendTo(d_ver_titulos_cob);
					$('<h1/>', {text: 'Coberturas'}).appendTo(d_ver_cobertura);
					var d_ver_monto = $('<div/>', {class: 'ver-monto'}).appendTo(d_ver_titulos_cob);
					$('<h1/>', {text: 'Montos'}).appendTo(d_ver_monto);
		
					if(itm.coberturas != null) {
						//Iterar por cobertura
						$.each(itm.coberturas, 
							function(idxcob, itmcob) {
								var d_ver_textos_cob = $('<div/>', {class: 'ver-textos-cob'}).appendTo(d_desplieguever);
								var d_ver_texto_cobertura = $('<div/>', {class: 'ver-texto-covertura'}).appendTo(d_ver_textos_cob);
								var p_cob = $('<p/>', {text: itmcob.descripcion}).appendTo(d_ver_texto_cobertura);
								$('<img/>', {src: 'images/img_light_box/ok_azul.gif', alt: '', width: '12', height: '11', border: '0'}).prependTo(p_cob);
								var d_ver_texto_monto = $('<div/>', {class: 'ver-texto-monto'}).appendTo(d_ver_textos_cob);
								var p_monto_cob = $('<p/>', {text: itmcob.monto}).appendTo(d_ver_texto_monto);
							}
						)
					} else {
						var c_center = $('<center/>').appendTo(d_desplieguever);
						var d_ver_textos = $('<div/>', {class: 'ver-textos'}).appendTo(c_center);
						var d_ver_texto_cobertura =  $('<div/>', {class: 'ver-texto-covertura'}).appendTo(d_ver_textos);
						$('<p/>', {text: 'No existe información sobre las coberturas de este plan.'}).appendTo(d_ver_texto_cobertura);
					}
					$('#d_valorizacion').append(d_cont).append(d_contver);
				}
			}
		)
	}
	
	function desplegarCoberturas(){
		$(".a1").fancybox({
			'hideOnContentClick': true,
			'frameWidth' : 440,
			'frameHeigh' : 320
		});
	}

</script>


</logic:notEmpty>
</html:form>

</div>
<!--  contenido COBERTURAS FIN -->

