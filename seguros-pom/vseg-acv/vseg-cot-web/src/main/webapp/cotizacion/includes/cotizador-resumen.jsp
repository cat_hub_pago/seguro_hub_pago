<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!-- INICIO  RESUMEN -->
<logic:notPresent name="usuario">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3-fluid">
		<div class="row bg-resumen hidden-xs">
			<h4>Resumen</h4>
			
			<div class="col-lg-12 col-xs-12 top15 text-center p0 hidden-xs">
				<div id="vacio" style="height: 193px; background-color: white;"></div><!--Se oculta Banner, se mantiene el alto-->
			</div>
			<div class="p-resumen">
			<div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
				<logic:equal name="idRama" value="1">
				<script>
					$('#vacio').hide();
				</script>
					<logic:present name="datosPlan">
						<logic:notEmpty name="datosPlan" property="nombrePlan">
							<label>Plan:</label>
							<bean:write name="datosPlan" property="nombrePlan" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualPesos">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualPesos"
								format="$ ##,##0" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualUF">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualUF"
								format="UF #0.0000" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:present name="datosCotizacion">
							<logic:present name="datosCotizacion" scope="session">
								<label>Tipo:</label><bean:write name="tipoDesc" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>Marca:</label><bean:write name="marcaDesc" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>Modelo:</label><bean:write name="modeloDes" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>A�o:</label><bean:write name="datosCotizacion" property="anyoVehiculo"/>
							</logic:present>
							<br />
						</logic:present>
					</logic:present>
				</logic:equal>
				<logic:equal name="idRama" value="8">
				<script>
					$('#vacio').hide();
				</script>
					<logic:present name="datosPlan">
						<logic:notEmpty name="datosPlan" property="nombrePlan">
							<label>Plan:</label>
							<bean:write name="datosPlan" property="nombrePlan" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualPesos">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualPesos"
								format="$ ##,##0" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualUF">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualUF"
								format="UF #0.0000" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:present name="datosCotizacion">
							<logic:present name="datosCotizacion" scope="session">
								<label>Tipo:</label><bean:write name="tipoDesc" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>Marca:</label><bean:write name="marcaDesc" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>Modelo:</label><bean:write name="modeloDes" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>A�o:</label><bean:write name="datosCotizacion" property="anyoVehiculo"/>
							</logic:present>
							<br />
						</logic:present>
					</logic:present>
				</logic:equal>
			</div>
		</div>
		</div>
	
		<div id="resumen-content">
		</div>
		<div class="row bg-resumen hidden-lg hidden-sm hidden-md">
			<div class="panel-group" id="resumen" role="tablist"
				aria-multiselectable="true">
				<div class="panel panel-default">
					<logic:equal name="idRama" value="1">
					<logic:present name="datosPlan">
						<logic:notEmpty name="datosPlan" property="nombrePlan">
							<label>Plan:</label>
							<bean:write name="datosPlan" property="nombrePlan" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualPesos">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualPesos"
								format="$ ##,##0" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:notEmpty name="datosPlan" property="primaMensualUF">
							<logic:notPresent name="existePrimaUnica">
								<label>Monto Mensual:</label>
							</logic:notPresent>
							<logic:present name="existePrimaUnica">
								<label>Prima �nica:</label>
							</logic:present>
							<bean:write name="datosPlan" property="primaMensualUF"
								format="UF #0.0000" locale="currentLocale" />
							<br />
						</logic:notEmpty>
						<logic:present name="datosCotizacion">
							<logic:present name="datosCotizacion" scope="session">
								<label>Tipo:</label><bean:write name="tipoDesc" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>Marca:</label><bean:write name="marcaDesc" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>Modelo:</label><bean:write name="modeloDes" scope="request"/>
							</logic:present>
							<br />
							<logic:present name="datosCotizacion" scope="session">
								<label>A�o:</label><bean:write name="datosCotizacion" property="anyoVehiculo"/>
							</logic:present>
							<br />
						</logic:present>
					</logic:present>
				</logic:equal>
				</div>
			</div>
		</div>
	</div>
</logic:notPresent>
<logic:present name="usuario">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3-fluid">
		<div class="row bg-resumen">
			<h4 class="hidden-xs">Resumen</h4>
			<div class="row">
				<div class="p-resumen">
					<div class="col-md-12">
						<div class="col-md-3 col-sm-3 col-xs-3">
							<!--img src="img/cliente.jpg" class="img-cliente"-->
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
							<div class="col-md-4 col-xs-4 p0">
								<p class="m0">Bienvenido</p>
							</div>
							<div class="col-md-8 col-xs-8 pright">
								<p class="text-right m0">
									<a href="/vseg-paris/autenticacion/cerrarSession"><span class="glyphicon glyphicon-remove"
										aria-hidden="true"></span> Cerrar</a>
								</p>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 hidden-xs">
								<logic:present name="usuario">
							<label>Rut:</label><bean:write name="usuario" property="usuarioExterno.rut_cliente" format="#,###" locale="currentLocale" />-<bean:write name="usuario" property="usuarioExterno.dv_cliente" /><br />
							<label>Nombre: </label><bean:write name="usuario" property="usuarioExterno.nombre" /><br />
							<label>Apellidos: </label><bean:write name="usuario" property="usuarioExterno.apellido_paterno" />
													 <bean:write name="usuario" property="usuarioExterno.apellido_materno" /><br />
							<label>Email: </label><bean:write name="usuario" property="email"/><br />
							<label>Fecha de Nacimiento: </label><bean:write name="usuario" property="usuarioExterno.fecha_nacimiento" format="dd-MM-yyyy" /><br />
							<label>Estado Civil: </label><logic:equal value="1" name="usuario" property="usuarioExterno.estado_civil">Soltero</logic:equal>
														<logic:equal value="2" name="usuario" property="usuarioExterno.estado_civil">Casado</logic:equal>
														<logic:equal value="3" name="usuario" property="usuarioExterno.estado_civil">Viudo</logic:equal><br />
							<label>Sexo:</label><logic:equal value="M" name="usuario" property="sexo">Masculino</logic:equal>
												<logic:equal value="F" name="usuario" property="sexo">Femenino</logic:equal><br />
							<label>Direcci&oacute;n:</label><bean:write name="usuario" property="usuarioExterno.calle" /> 
											  <bean:write name="usuario" property="usuarioExterno.numero"/>
											  <bean:write name="usuario" property="usuarioExterno.numero_departamento" /><br />
							
							</logic:present>
							<logic:equal name="idRama" value="1">
								<logic:present name="datosPlan">
									<logic:notEmpty name="datosPlan" property="nombrePlan">
										<label>Plan:</label>
										<bean:write name="datosPlan" property="nombrePlan" />
										<br />
									</logic:notEmpty>
									<logic:notEmpty name="datosPlan" property="primaMensualPesos">
										<logic:notPresent name="existePrimaUnica">
											<label>Monto Mensual:</label>
										</logic:notPresent>
										<logic:present name="existePrimaUnica">
											<label>Prima �nica:</label>
										</logic:present>
										<bean:write name="datosPlan" property="primaMensualPesos"
											format="$ ##,##0" locale="currentLocale" />
										<br />
									</logic:notEmpty>
									<logic:notEmpty name="datosPlan" property="primaMensualUF">
										<logic:notPresent name="existePrimaUnica">
											<label>Monto Mensual:</label>
										</logic:notPresent>
										<logic:present name="existePrimaUnica">
											<label>Prima �nica:</label>
										</logic:present>
										<bean:write name="datosPlan" property="primaMensualUF"
											format="UF #0.0000" locale="currentLocale" />
										<br />
									</logic:notEmpty>
									<logic:present name="datosCotizacion">
										<logic:present name="datosCotizacion" scope="session">
											<label>Tipo:</label><bean:write name="tipoDesc" scope="request"/>
										</logic:present>
										<br />
										<logic:present name="datosCotizacion" scope="session">
											<label>Marca:</label><bean:write name="marcaDesc" scope="request"/>
										</logic:present>
										<br />
										<logic:present name="datosCotizacion" scope="session">
											<label>Modelo:</label><bean:write name="modeloDes" scope="request"/>
										</logic:present>
										<br />
										<logic:present name="datosCotizacion" scope="session">
											<label>A�o:</label><bean:write name="datosCotizacion" property="anyoVehiculo"/>
										</logic:present>
										<br />
									</logic:present>
								</logic:present>
							</logic:equal>
								
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 p0">
							<!--div class="separacion-hor-cont-lateral hidden-xs"></div-->
						</div>
						<div class="contenedor-pasos-lateral hidden-xs">
							<div class="col-md-12 col-sm-12 col-xs-12 bg-lateral-on">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<!--img src="img/icono-dato-vehiculo-activo.png" width="49"
										height="49" class="img-iconos"-->
								</div>
								<div class="col-md-7 col-sm-7 col-xs-7">
									<!--p class="titulo-lateral-listado">DATOS DEL VEH�CULO</p-->
									<!--p>
										Autom�vil Mazda 6<br> 2014
									</p-->
								</div>
							</div>
							<div class="col-md-12 col-xs-12 bg-lateral-on">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<!--img src="img/icono-cotizar-seguro-activo.png" width="49"
										height="49" class="img-iconos"-->
								</div>
								<div class="col-md-7 col-sm-7 col-xs-7">
									<!--p class="titulo-lateral-listado">COTIZAR SEGURO</p-->
									<!--p>
										S�per Seguros<br> <strong>$20.000</strong> (Monto
										Mensual)<br> <strong>UF 0,85</strong> (Monto Mensual)
									</p-->
								</div>
							</div>
							<div class="col-md-12 col-xs-12 bg-lateral-off">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<!--img src="img/icono-pagar-seguro-off.png" width="49"
										height="49" class="img-iconos"-->
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<!--p class="titulo-lateral-listado">PAGAR SEGURO</p-->
								</div>
							</div>
							<div class="col-md-12">
								<div class="separacion-hor2"></div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<!--img src="img/icono-cotizar-listo-off.png" width="48"
										height="48" class="img-iconos"-->
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<!--p class="titulo-lateral-listado">�LISTO!</p-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</logic:present>

<!-- FIN  RESUMEN -->
