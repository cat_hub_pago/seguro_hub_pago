<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
<!--BOX PROCESO -->
<div id="errorlogin">
<div align="center" style="padding-top:25px;height:168px;" >
	
		
		<a onclick="javascript:parent.$.fn.colorbox.close();$('#errorlogin').hide();"></a>
		
		<div class="pantalla-error_cuerpo">
			<div id="conte_estamos_procesando">
				<div class="icono_geeral">
					<img src="/vseg-paris/images/img/icono_error.png" alt="" width="46" height="46" border="0" />
				</div>
				<div id="texto_procesando">
					<h3>
						<div id="errorClave2">
						</div>
					</h3>
				</div>
			</div>
		</div>
	
</div>
	<!--BOX PROCESO FIN-->
</div>
<script type="text/javascript">
			$(document).ready(function(){
				html = "";
				campo = "<bean:write name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>" ignore="true" />";
				cadena = "bloqueada";
				numIntentos = "3";
				numIntentos2 = "2";
				numIntentos3 = "1";
				numIntentos4 = "0";
				if (campo.indexOf(numIntentos) > 0){
					html += "La clave ingresada no es correcta. Le quedan 2 intentos";
					html += "<br />";
					html += "<a id=\"olvidasteClave3\" href=\"javascript:void(0);\" style=\"cursor: pointer; font-size: 11px; text-decoration: underline; color: blue;\">żOlvidaste tu clave?</a>";
					html += "<br />";
					$('#errorClave2').append(html);
				} else if (campo.indexOf(numIntentos2) > 0){
					html += "La clave ingresada no es correcta. Le queda 1 intentos";
					html += "<br />";
					html += "<a id=\"olvidasteClave3\" href=\"javascript:void(0);\" style=\"cursor: pointer; font-size: 11px; text-decoration: underline; color: blue;\">żOlvidaste tu clave?</a>";
					html += "<br />";
					$('#errorClave2').append(html);
				} else if (campo.indexOf(numIntentos3) > 0){
					html += "La clave ingresada no es correcta. Su clave ha sido bloqueada";
					html += "<br />";
					html += "<a id=\"olvidasteClave3\" href=\"javascript:void(0);\" style=\"cursor: pointer; font-size: 11px; text-decoration: underline; color: blue;\">żOlvidaste tu clave?</a>";
					html += "<br />";
					$('#errorClave2').append(html);
				} else if (campo.indexOf(numIntentos4) > 0){
					html += "Su clave ha sido bloqueada";
					html += "<br />";
					html += "Por favor haga click <a id=\"olvidasteClave3\" href=\"javascript:void(0);\" style=\"cursor: pointer; font-size: 11px; text-decoration: underline; color: blue;\">Aqui</a> para recuperar su clave";
					html += "<br />";
					$('#errorClave2').append(html);
				}
				
			});

			$(document).ready(function(){
			$("#olvidasteClave3").fancybox({ 
		    'onStart'   : function() {
					var url = "/vseg-paris/recuperar-clave.do";
					var params = "?rut=";

					params += "&dv=";

		     $("#olvidasteClave3").attr("href",url+params);
		     
		    },
		    'width'    : 430,

		    'height'   : 130,

		    'autoScale'   : false,
		    'transitionIn'  : 'none',
		    'transitionOut'  : 'none',

		    'type'    : 'iframe',

		    'scrolling'  : 'no'

		  });
			});
		</script>

<script type="text/javascript">
		
		$(document).ready(function(){
			$('<a href="#errorlogin" class="login_error"></a>').appendTo('body');
			
			$("a.login_error").fancybox({ 
			    'hideOnContentClick': false,				
				'width':750,
		    	'type':'inline',
			'scrolling'  : 'no' 
			  });
			$("#fancybox-close").click(function(){
				$("#errorlogin").css("display","none");
			});
			$("a.login_error").click();
		});
		
</script>
</logic:present>
