<!DOCTYPE html>
<%@page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="cl.cencosud.acv.common.Producto"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<html lang="es">
<head>

<!--  <script src="js/main.min.js"></script>
<script src="js/formularioVehiculo.js"></script> -->
<%@ include file="./includes/cotizador-head.jsp"%>
<script type="text/javascript">	
			$(document).ready(function(){
    		
    		$("#paso3").hide();
    		duenioVehiculo('true');
    		$("#paso2").hide();
    		$('.datos-nombre-input').attr('placeholder','Nombre');
    		$('.datos-paterno-input').attr('placeholder','Ingrese su apellido paterno');
    		$('.datos-materno-input').attr('placeholder','Ingrese su apellido materno');
    		$('.datos-rut-input').attr('placeholder','Ej: 1234567-9');
    		$('.datos-email-input').attr('placeholder','ejemplo@correo.cl');
    		$('.datos-telefono3-input').attr('placeholder','tel�fono');
    		$('.datos-fecha-input').attr('placeholder','01-01-1980');
    		$('.input-patente').attr('placeholder','Patente');
    		
    		<logic:present property="datos(codigoTelefono)" name="cotizarProductoVehiculo">
				var val_codigo_cel = "<bean:write name='cotizarProductoVehiculo' property='datos(codigoTelefono)'/>";
			$("select[name='datos(codigoTelefono)']").val(val_codigo_cel);
			</logic:present>
			
			<logic:present property="datos(contratanteEsDuenyo)" name="cotizarProductoVehiculo">
				var valor = "<bean:write property='datos(contratanteEsDuenyo)' name='cotizarProductoVehiculo'/>";
				if(valor == "false") {
					duenioVehiculo('false');
				}
			</logic:present>
			
			if( $('select#tipoVehiculo').val() != "" ) {
				var valMarca = '';
				<logic:present property="datos(marcaVehiculo)" name="cotizarProductoVehiculo">
					valMarca = "<bean:write name='cotizarProductoVehiculo' property='datos(marcaVehiculo)'/>";
				</logic:present>
				marcas($('select#tipoVehiculo').val(), valMarca);
			}
			
			var valModelo = '';
			<logic:present property="datos(modeloVehiculo)" name="cotizarProductoVehiculo">
				valModelo = "<bean:write name='cotizarProductoVehiculo' property='datos(modeloVehiculo)'/>";
			</logic:present>
			if( valModelo != '' ) {
				modelos($('select#tipoVehiculo').val(), valMarca, valModelo);
			}
			
			$("select#tipoVehiculo").change(function() {
				//Buscar marcas.
				marcas($(this).val(), '');
				//Limpiar modelo.
				$("select#modeloVehiculo option").remove();
				var options = '<option value="">Seleccione</option>';
				$("select#modeloVehiculo").html(options);
			});
			
			$("select#marcaVehiculo").change(function() {
				//Buscar Modelos
				modelos($('select#tipoVehiculo').val(), $(this).val(), '');
			});
		});

	    <logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
	    	<bean:define id="usuario" name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY%>" scope="session" type="UsuarioExterno" />
	    </logic:present>
    	
    	
    	function duenioVehiculo(val) {
    		if(val==='true') {
    			$(".form-vehiculo-duenio").hide();
    			$("#form-vehiculo-datos").addClass("col-md-8");
    			$("#form-vehiculo-datos").removeClass("col-md-12");
    		} else {
    			$(".form-vehiculo-duenio").show();
    			$("#form-vehiculo-datos").addClass("col-md-12");
    			$("#form-vehiculo-datos").removeClass("col-md-8");
    		}
    	}
    
		function obtenerDescuento() {
			personalInfo();
			document.getElementById('formularioVehiculo').action="/cotizador/preferente-vehiculo.do";
			document.getElementById('formularioVehiculo').method="post";
			document.getElementById('formularioVehiculo').submit();
		}
    
		//Envio de formuario y validaci�n de campos
		function submitVehiculo() {
			
			var result= validateInput("paso2");
			if(validacionInput == 0) {
				bar(66.7,1);
				personalInfo();
			$('html, body').animate({scrollTop: 0}, 0);
			$('#waitModal').modal({backdrop: 'static', keyboard: false});
			$('#grupo_valorizacion_iframe').hide();
			var rutCompleto = document.getElementById('datos.rutDuenyoCompleto').value.split('-');
			document.getElementById('datos.rutDuenyo').value = rutCompleto[0];
			if(rutCompleto.length > 1) {
				document.getElementById('datos.dvDuenyo').value = rutCompleto[1];
			}
			var fechaCompleta = document.getElementById('fechaCompletaDuenyo').value.split('-');
			document.getElementById('diasFechaDuenyo').value = fechaCompleta[0];
			document.getElementById('mesFechaDuenyo').value = fechaCompleta[1];
			document.getElementById('anyosFechaDuenyo').value = fechaCompleta[2];
			if(fechaCompleta[0] > 32 ){
			document.getElementById('diasFechaDuenyo').value = 30;
			}
			document.getElementById('mesFechaDuenyo').value = fechaCompleta[1];
			document.getElementById('anyosFechaDuenyo').value = fechaCompleta[2];
			var caracter="1234567890";
			caracter+="QWERTYUIOPASDFGHJKLZXCVBNM";
			caracter+="qwertyuioplkjhgfdsazxcvbnm";
			var numero_caracteres=10;
			var total=caracter.length;
			var clave="";
			for(a=0;a<numero_caracteres;a++){
				clave+=caracter.charAt(parseInt(total*Math.random(1)));
			}
			//Se guardan los datos de vehiculo
			submitDatosVehiculo();
			document.getElementById('claveVitrineo').value = clave;
			document.getElementById('formularioVehiculo').action="/cotizador/cotizar-producto-vehiculo.do";
			document.getElementById('formularioVehiculo').method="post";
			document.getElementById('formularioVehiculo').submit();	
			}
		}
		
		function marcas(idTipo, valMarca) {
			//			alert('buscar marcas ' + idTipo + ' - ' + valMarca);
			if (idTipo != '') {
				var url = '/cotizador/buscar-marcas-vehiculo.do';
				var params = '?idTipoVehiculo=' + idTipo;
				$.getJSON(url + params, function(data) {	
					$('select#marcaVehiculo option').remove();
					var options = '';
					options += '<option value="">Seleccione</option>';
					for ( var i = 0; i < data.length; i++) {
						if (valMarca != '' && valMarca == data[i].idMarcaVehiculo) {
							options += '<option value="' + data[i].idMarcaVehiculo
							+ '" selected="selected">' + data[i].marcaVehiculo
							+ '</option>';
						} else {
							options += '<option value="' + data[i].idMarcaVehiculo
							+ '">' + data[i].marcaVehiculo + '</option>';
						}
					}
					$('select#marcaVehiculo').html(options);
				});
			}
		}

			function modelos(idTipo, idMarca, valModelo) {
				//			alert('buscar modelos ' + idTipo + ' - ' + idMarca + ' - ' + valModelo);
				if ((idMarca != "") && (idTipo != "")) {
					var url = '/cotizador/buscar-modelos-vehiculo.do';
					var params = '?idTipoVehiculo=' + idTipo;
					params += '&idMarcaVehiculo=' + idMarca;
					$.getJSON(url + params, function(data) {
						$('select#modeloVehiculo option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';
						for ( var i = 0; i < data.length; i++) {
							if (valModelo != '' && valModelo == data[i].idModeloVehiculo) {
								options += '<option value="' + data[i].idModeloVehiculo
								+ '" selected="selected">' + data[i].modeloVehiculo
								+ '</option>';
							} else {
								options += '<option value="' + data[i].idModeloVehiculo
								+ '">' + data[i].modeloVehiculo + '</option>';
							}
						}
						$('select#modeloVehiculo').html(options);
					});
				}
			}
	</script>



<style type="text/css">
.ui-datepicker {
	color: #000000 width :   216px;
	height: auto;
	margin: 5px auto 0;
	font: 9pt Arial, sans-serif;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
}

.ui-datepicker-month {
	color: #000000
}

.ui-datepicker-year {
	color: #000000
}

.patente{	
	text-transform: uppercase;
}
#avisoPatente{
	display: none;
}
</style>


<script type="text/javascript">

$(document).ready(function(){
	cargaDia('idFechaDia');
	cargaMes('idFechaMes');
	cargaAnyo('idFechaAnyo');
	
	cargaDia('idFechaDiaDuenyo');
	cargaMes('idFechaMesDuenyo');
	cargaAnyo('idFechaAnyoDuenyo');
});
function validaPatente(idPatente) {
	idPatente = idPatente.toUpperCase();
	if(idPatente.length > 5){
		$('#patenteVehiculo').blur();
		buscarPatente(idPatente);
	}else{
		$('patenteVehiculo').value=idPatente;
	}
}

function buscarPatente(idPatente){
	window.parent.$("#idPatente").val(idPatente);		
	$.ajax({
		url: "/cotizador/buscar-patente-vehiculo.do",
		type: "POST",
		async:   true,
		dataType: "json",
		data: window.parent.$("#formularioVehiculo").serialize(),
		success: function(j) {
			cargarComboBox(j);
			console.debug(j);
		},
		error: function(req, status, error) {
			
		}
	});
}
function cargarComboBox(patente){
	
	if(patente == null){
		$("#avisoPatente").show();
		$('#tipoVehiculo').val('');
		$('#marcaVehiculo').val('');
		$('#modeloVehiculo').val('');
		$('#anyoVehiculo').val('');
		$("#tipoVehiculo").focus();
	}else{
		$("#avisoPatente").hide();
		
		console.debug(patente.tipoVehiculo);
		if($("#tipoVehiculo option[value='"+patente.tipoVehiculo+"']").length > 0){
			$('#tipoVehiculo').val(patente.tipoVehiculo);
			if(patente.marca != ''){
				marcas(patente.tipoVehiculo, patente.marca);
				if(patente.modelo != ''){
					modelos(patente.tipoVehiculo, patente.marca, patente.modelo);
				}
			}
		}
		if($("#anyoVehiculo option[value='"+patente.anio+"']").length > 0){
			$('#anyoVehiculo').val(patente.anio);
		}
		if($('#tipoVehiculo').val()==""){
			$("#tipoVehiculo").focus();
		}else{
			$('#marcaVehiculo').val()=="";
		}
	}
}

function switchFormVehiculo(bool) {
	    		if(bool===false) {
	    			$('html, body').animate({scrollTop: 0}, 0);
	    			$("#paso1").show();
	    			$("#paso2").hide();
	    		} else {
	    			
	    			var result = validateInput("paso1");
	    			if(validacionInput == 0) {
	    			
	    										var edadCorrecta = true;
						<logic:equal name="idSubcategoria" value="119">
							edadCorrecta =	calcularEdad();
						</logic:equal>
						
						if ( !edadCorrecta ){
							$("#incomplete-text-paso1").html("La edad requerida es mayor de 18 y menor de 75 a�os");
	    					$("#incomplete-paso1").show("fast");
	    					$("#fechaCompleta").css("border-color","red");
	    					$("#fechaCompleta").focus();
	    					
	    					validacionInput = 1;
						
						}else{						
	    				try {
		    				if(paso2bar && paso2bar !== 'undefined' && paso2bar >= 0 && paso2bar <= 100) {
		    					bar(paso2bar,2);
		    				} else {
		    					bar(33.35,1);
		    				}
	    				} catch(err) {
	    					bar(33.35,1);
	    				}
	    				$('html, body').animate({scrollTop: 0}, 0);
	    				$("#paso2").show();
	    				$("#paso1").hide();
						}
	    			}
	    			
	    		}
	    	}

function submitDatosVehiculo(){
	console.log("Patente:"+$("input#patenteVehiculo").val());
	console.log("TV:"+$('select#tipoVehiculo option:selected').val());

	$('#idPatente').val($("input#patenteVehiculo").val());
	$('#idTipoVehiculo').val($('select#tipoVehiculo option:selected').val());
	$('#marca').val($("#marcaVehiculo option:selected").val());
	$('#modelo').val($("#modeloVehiculo option:selected").val());
	$('#anio').val($("#anyoVehiculo option:selected").val());
	$.ajax({
		url: "/cotizador/guardar-patente-vehiculo.do",
		type: "POST",
		async:   true,
		dataType: "json",
		data: window.parent.$("#formularioVehiculo").serialize(),
		success: function(j) {
		
		},
		error: function(req, status, error) {
			
		}
	});
}
</script>
<script type="text/javascript">
$(document).ready(function(){
$('.datos-rut-input').blur(function(){
	var rut = $('.datos-rut-input').val(); 
	var cadena =rut.substr(0,1);
	if (cadena >=4 && cadena <= 9){
		if (rut != null){
			if (rut.length < 9){
				var cuerpo = rut.slice(0,-1);
				var dv = rut.slice(-1).toUpperCase(); 
				var completo = cuerpo + '-' + dv;
				
				$('.datos-rut-input').val("");
				$('.datos-rut-input').val(completo);
			}
		}
	}else if(cadena >= 1 && cadena <=3)
	if (rut != null){
		if (rut.length < 10){
		var cuerpo = rut.slice(0,-1);
		var dv = rut.slice(-1).toUpperCase(); 
		var completo = cuerpo + '-' + dv;
		
		$('.datos-rut-input').val("");
		$('.datos-rut-input').val(completo);
		}
	
	
	}else{
		$('.datos-rut-input').attr('placeholder','12345678-9');
	}
   });
	$('.datos-rut-input-duenho').attr('placeholder','12345678-9');
});
</script>
</head>

<body onload="desplegarErrores();">
	<%@ include file="/google-analytics/google-analytics.jsp"%>  
	<%@ include file="./includes/cotizador-header.jsp"%>

		<%@ include file="./includes/cotizador-breadcrumb.jsp"%>
		<!-- INICIO  CONTENIDOS -->
	<main role="main">
	  <div class="remodal-bg">
        <section class="container">
          <div class="row">
          <div class="col-12">
              		<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
            </div>
			<!--  include file="./includes/cotizador-resumen.jsp"  -->
			<!-- INICIO  COTIZACION -->
				<!--  include file="./includes/cotizador-percent-bar.jsp" -->
				<html:form styleId="formularioVehiculo" styleClass="o-form o-form--standard"
					action="/cotizar-producto-vehiculo.do" target="iframe_tabla"
					method="post">
					<%
		String idPlan = (String) request.getAttribute("idPlan");
		idPlan = (idPlan != null ? idPlan : "-1");
	%>
					<html:hidden property="datos(claveVitrineo)" value="1"
						styleId="claveVitrineo" />
					<html:hidden property="datos(rut)" value="" styleId="datos.rut" />
					<html:hidden property="datos(dv)" value="" styleId="datos.dv" />
					<html:hidden property="datos(rutDuenyo)" value=""
						styleId="datos.rutDuenyo" />
					<html:hidden property="datos(dvDuenyo)" value=""
						styleId="datos.dvDuenyo" />
					<html:hidden property="datos(hiddProducto2)" value="1"
						styleId="hiddProducto2" />
					<html:hidden property="datos(hiddProducto3)" value="1"
						styleId="hiddProducto3" />
					<html:hidden property="datos(hiddProducto4)" value="1"
						styleId="hiddProducto4" />
					<html:hidden property="datos(hiddProducto5)" value="1"
						styleId="hiddProducto5" />
					<html:hidden property="datos(hiddProducto6)" value="1"
						styleId="hiddProducto6" />
					<html:hidden property="datos(hiddProducto7)" value="1"
						styleId="hiddProducto7" />
					<html:hidden property="datos(hiddCaptcha)" value="1"
						styleId="hiddCaptcha" />
					<html:hidden property="datos(hiddHide2)" value="1"
						styleId="hiddHide2" />
					<html:hidden property="datos(hiddHide3)" value="1"
						styleId="hiddHide3" />
					<html:hidden property="datos(hiddHide4)" value="1"
						styleId="hiddHide4" />
					<html:hidden property="datos(hiddHide5)" value="1"
						styleId="hiddHide5" />
					<html:hidden property="datos(hiddHide6)" value="1"
						styleId="hiddHide6" />
					<html:hidden property="datos(hiddHide7)" value="1"
						styleId="hiddHide7" />
					<html:hidden property="datos(idPlan)" value="<%=idPlan %>"
						styleId="datos.idPlan" />
					<html:hidden property="datos(diaFechaNacimientoDuenyo)" value=""
						styleId="diasFechaDuenyo" />
					<html:hidden property="datos(mesFechaNacimientoDuenyo)" value=""
						styleId="mesFechaDuenyo" />
					<html:hidden property="datos(anyoFechaNacimientoDuenyo)" value=""
						styleId="anyosFechaDuenyo" />
					<html:hidden property="datos(diaFechaNacimiento)" value=""
						styleId="diasFecha" />
					<html:hidden property="datos(mesFechaNacimiento)" value=""
						styleId="mesFecha" />
					<html:hidden property="datos(anyoFechaNacimiento)" value=""
						styleId="anyosFecha" />
					<html:hidden property="datos(numeroPuertas)"
						styleId="numeroPuertas" value="5" />
					<html:hidden property="idPatente"
						styleId="idPatente" value="" />
					<html:hidden property="idTipoVehiculo"
						styleId="idTipoVehiculo" value="" />
					<html:hidden property="marca"
						styleId="marca" value="" />
					<html:hidden property="modelo"
						styleId="modelo" value="" />
					<html:hidden property="anio"
						styleId="anio" value="" />
						
						
					<input type="hidden" name="idPlanValorizacion"
						id="idPlanValorizacion" value="" />
					<logic:notEqual name="idTipo" value="2">
						<html:hidden property="datos(valorComercial)"
							styleId="valorComercial" value="0" />
					</logic:notEqual>
					<div id="contenido-desplegado-cotizador">
					<div id="globalExceptionDivPlanes" class="">
						</div>
						<div id="paso2">
	
	<div class="col-12">
              <section class="o-box o-box--quotation u-mt20 u-mb40">
                <h2 class="o-title o-title--subtitle">Queremos saber de t�</h2>
                  <div class="row u-mb50">
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Nombres</label>
                       <html:text property="datos(nombre)"
												styleClass="o-form__input datos-nombre-input" size="16"
												styleId="datos.nombre" maxlength="25"
												onkeypress="return validar(event)" />
												<span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">RUT</label>
                        <logic:present name="usuario">
												<input type="text" class="o-form__input datos-rut-input" disabled="disabled" id="datos.rutcompleto"
													value="<bean:write name='usuario' property='usuarioExterno.rut_cliente' />-<bean:write name='usuario' property='usuarioExterno.dv_cliente' />">
						</logic:present>
						<logic:notPresent name="usuario">
												<html:text styleClass="o-form__input datos-rut-input"
													property="datos(rutCompleto)" maxlength="10"
													styleId="datos.rutcompleto" onblur="obtenerDescuento();"
													onkeypress="return isRutKey(event)"
													/>
						</logic:notPresent>
						<span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                    <div class="o-form__field">
                        <label class="o-form__label">Fecha de Nacimiento</label>
                        <div class="row">
                          <div class="col-4">
	                         <div class="o-form__field o-form__field--select u-mb0">
	                            <select fecha='fecha' name='idFechaDia' id='idFechaDia' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
								</select> 
								<span class="o-form__line"></span>
	                         </div>
                         </div>
                          <div class="col-4">
                          <div class="o-form__field o-form__field--select u-mb0">
							<select name='idFechaMes' id='idFechaMes' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
							</select>
							<span class="o-form__line"></span>
						  </div>
                          </div>
                          <div class="col-4">
                          <div class="o-form__field o-form__field--select u-mb0">
							<select name='idFechaAnyo' id='idFechaAnyo' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
						    </select>
						    <span class="o-form__line"></span>
						  </div>
                          </div>
                        </div><span class="o-form__message" id="year_of_birth"></span>
                       </div>
                        <!-- Input hiden para guardar la fecha-->
                        <logic:notPresent name="usuario">
												<input type="hidden" value="" id="fechaCompleta"/>    
						</logic:notPresent>
						<logic:present name="usuario">
												<input type="hidden"  id="fechaCompleta"  value="<bean:write name='usuario' property='usuarioExterno.fecha_nacimiento' format='dd-MM-yyyy' />"/>
												<script>   
													$(document).ready(function(){
														cargarSelectFechaNacimiento('fechaCompleta', 'idFechaDia', 'idFechaMes', 'idFechaAnyo');
													});
												</script>
						</logic:present>
                      
                    </div>
                  </div>
                  <div class="row u-mb50">
                    <div class="col-lg-4">
                      <div class="o-form__field o-form__field--select">
                        <label class="o-form__label">Estado Civil</label>
                        <html:select property="datos(estadoCivil)"
												styleClass="o-form__select" 
												styleId="datos.estadoCivil">
												<html:option value="">Seleccione estado civil</html:option>
												<html:options collection="estadosCiviles" property="id"
													labelProperty="descripcion" />
						</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field o-form__field--select">
                        <label class="o-form__label">Genero</label>
                        <html:select styleClass="o-form__select"
												property="datos(sexo)" styleId="datos.sexo">
												<html:option value="">Seleccione Sexo</html:option>
												<html:option value="F">Femenino</html:option>
												<html:option value="M">Masculino</html:option>
						</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Email</label>
						<html:text property="datos(email)" styleId="datos.email" size="60" 
												styleClass="o-form__input datos-email-input" 
												onkeypress="return isCaracterEmail(event);"/>
												<span class="o-form__line"></span><span class="o-form__message"> </span>
                      </div>
                    </div>
                  </div>
                  <div class="row u-mb50">
                    
                    
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Tel�fono</label>
                        <div class="row">
                          <div class="col-4">
                          <div class="o-form__field o-form__field--select u-mb0">
                            <html:select property="datos(tipoTelefono)"
													styleClass="o-form__select is-children" styleId="datos.telefono">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="tipoTelefono"
														label="descripcion" value="valor" />
							</html:select><span class="o-form__line"></span>
							</div>
                          </div>
                          <div class="col-3">
                          <div class="o-form__field o-form__field--select u-mb0">
                            <html:select property="datos(codigoTelefono)"
													styleClass="o-form__select is-children" styleId="datos.codigoTelefono">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="codigoArea"
														label="descripcion" value="valor" />
							</html:select><span class="o-form__line"></span>
							</div>
                          </div>
                          <div class="col-5">
                            <div class="o-form__field o-form__field--select u-mb0">
                             <html:text property="datos(numeroTelefono)"
													styleClass="o-form__input datos-telefono3-input is-children"
													maxlength="8" styleId="datos.telefono3"
													onkeypress="return isNumberKey(event)"></html:text><span class="o-form__line"></span>
							</div>
                          </div>
                        </div><span class="o-form__message" id="phone"></span>
                      </div>
                    </div>
                    <div class="col-md-4">
														<div class="o-form__field o-form__field--select">
								                        <label class="o-form__label">Qui�n cotiza<span class="o-help">�Es el due�o del veh�culo?<i class="o-help__icon" data-tooltip="Si tu respuesta es NO, recuerda tener a mano los datos del due�o del veh�culo asegurado.">
								                              <svg width="3px" height="15px" viewbox="0 0 3 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								                                <g id="exclamation" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								                                  <g id="Desktop" transform="translate(-433.000000, -745.000000)" fill="#FFFFFF">
								                                    <g id="Group-4" transform="translate(424.000000, 742.000000)">
								                                      <g id="tooltip">
								                                        <g id="Group-3">
								                                          <g id="Group-2" transform="translate(9.000000, 3.000000)">
								                                            <ellipse id="Oval-3" cx="1.5" cy="1.47058824" rx="1.5" ry="1.47058824"></ellipse>
								                                            <rect id="Rectangle-2" x="0" y="3.82352941" width="3" height="11.1456409" rx="1.5"></rect>
								                                          </g>
								                                        </g>
								                                      </g>
								                                    </g>
								                                  </g>
								                                </g>
								                              </svg></i></span></label>
								                        <html:select styleClass="o-form__select"
															property="datos(contratanteEsDuenyo)"
															onchange="javascript:duenioVehiculo(this.value);">
															<html:option value="true" selected>Si</html:option>
															<html:option value="false">No</html:option>
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
														<%
											 				Producto[] productos = (Producto[]) request.getAttribute("productos");
															String cantProductos = String.valueOf(productos.length);
														%>
														<bean:define id="cantProd" value="<%=cantProductos%>"
															scope="page" />	
														</div>
													</div>
                  </div>

													<div class="row u-mb50 form-vehiculo-duenio datos">
													<div class=" col-sm-12">
										                  <div class="row u-mb50">
										                    <div class="col-lg-4">
										                      <div class="o-form__field">
										                        <label class="o-form__label">Nombres</label>
										                        <html:text property="datos(nombreDuenyo)"
																	styleClass="o-form__input datos-nombre-input" size="16"
																	styleId="datos.nombreDuenyo" maxlength="25"
																	onkeypress="return validar(event)" /><span class="o-form__line"></span><span class="o-form__message"></span>                  
										                      </div>
										                    </div>
										                    <div class="col-lg-4">
										                      <div class="o-form__field">
										                        <label class="o-form__label">Apellido paterno</label>
										                        <html:text property="datos(apellidoPaternoDuenyo)"
																	styleClass="o-form__input datos-paterno-input"
																	styleId="datos.apellidoPaternoDuenyo" size="16"
																	maxlength="25" onkeypress="return validar(event)" /><span class="o-form__line"></span><span class="o-form__message"></span>
										                      </div>
										                    </div>
										                    <div class="col-lg-4">
										                      <div class="o-form__field">
										                        <label class="o-form__label">Apellido materno</label>
										                        <html:text property="datos(apellidoMaternoDuenyo)"
																	styleClass="o-form__input datos-materno-input"
																	styleId="datos.apellidoMaternoDuenyo" size="16"
																	maxlength="25" onkeypress="return validar(event)" /><span class="o-form__line"></span><span class="o-form__message"></span>
										                      </div>
										                    </div>
										                   </div>
										                   <div class="row u-mb50">
										                   	
										                    <div class="col-lg-4">
										                      <div class="o-form__field">
										                        <label class="o-form__label">RUT</label>
										                        <html:text styleClass="o-form__input datos-rut-input-duenho"
																property="datos(rutDuenyoCompleto)" maxlength="10"
																styleId="datos.rutDuenyoCompleto"
																onblur="obtenerDescuento();"
																onkeypress="return isRutKey(event)"
																onkeyup="autocompletarrut(this)" /><span class="o-form__line"></span><span class="o-form__message"></span>
										                      </div>
										                    </div>
										                    
										                     <div class="col-lg-4">
																	<div class="o-form__field">
											                        <label class="o-form__label">Fecha de Nacimiento</label>
											                        <div class="row">
											                          <div class="col-4">
											                           <div class="o-form__field o-form__field--select">
											                             <select fecha="fecha" name='idFechaDiaDuenyo' id='idFechaDiaDuenyo' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDiaDuenyo', 'idFechaMesDuenyo', 'idFechaAnyoDuenyo', 'fechaCompletaDuenyo')">
																			</select><span class="o-form__line"></span>
																		</div>
											                          </div>
											                          <div class="col-4">
											                           <div class="o-form__field o-form__field--select">
											                            <select name='idFechaMesDuenyo' id='idFechaMesDuenyo' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDiaDuenyo', 'idFechaMesDuenyo', 'idFechaAnyoDuenyo', 'fechaCompletaDuenyo')">
																		</select><span class="o-form__line"></span>
																		</div>
											                          </div>
											                          <div class="col-4">
											                           <div class="o-form__field o-form__field--select">
											                            <select name='idFechaAnyoDuenyo' id='idFechaAnyoDuenyo' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDiaDuenyo', 'idFechaMesDuenyo', 'idFechaAnyoDuenyo', 'fechaCompletaDuenyo')">
																		</select><span class="o-form__line"></span>
																		</div>
											                          </div>
											                        </div><span class="o-form__message" id="year_of_birth"></span>
											                        <input type="hidden" value="" id="fechaCompletaDuenyo" fecha="fecha"/>
										                      		</div>
															 </div>
																
																<div class="col-lg-4">
											                      <div class="o-form__field o-form__field--select">
											                        <label class="o-form__label">Estado Civil</label>
											                        <html:select property="datos(estadoCivilDuenyo)"
																		styleClass="o-form__select" 
																		styleId="datos.estadoCivilDuenyo">
																		<html:option value="">Seleccione</html:option>
																		<html:options collection="estadosCiviles" property="id"
																			labelProperty="descripcion" />
																	</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
											                      </div>
											                    </div>
										                    
										                   </div>
										                  <div class="row u-mb50">
										                    
										                    
										                    <div class="col-lg-4">
										                      <div class="o-form__field o-form__field--select">
										                        <label class="o-form__label">Genero</label>
										                        <html:select styleClass="o-form__select"
																	property="datos(sexoDuenyo)" styleId="datos.sexoDuenyo">
																	<html:option value="">Seleccione Genero</html:option>
																	<html:option value="F">Femenino</html:option>
																	<html:option value="M">Masculino</html:option>
																</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
										                      </div>
										                    </div>
										                  </div>
										                 
												</div>
												</div>
                  <%@ include file="./includes/cotizador-captcha.jsp"%>  
              </section>
            </div>

						</div>
						<div id="paso1">
						<div class="col-lg-12">
								<div class="row">
									<main role="main">
								      <div class="remodal-bg">
								        <section class="container">
								          <div class="row">
								            <div class="col-12">
								              <section class="o-box o-box--quotation u-mt20 u-mb40">
								                <h2 class="o-title o-title--subtitle">Cu�ntanos sobre el veh�culo a asegurar</h2>
								                <form class="o-form o-form--standard" id="form_quotation_step1" action="">
								                  <div class="row u-mb50">
								                    <div class="col-lg-4">
								                      <div class="o-form__field o-form__field--select">
								                        <label class="o-form__label">Tipo de veh�culo</label>
									                        <html:select property="datos(tipoVehiculo)"
																styleClass="o-form__select" styleId="tipoVehiculo">
																<logic:notEqual name="numTiposVehiculo" value="1"
																	scope="request">
																	<html:option value="">Seleccione Tipo</html:option>
																</logic:notEqual>
																<html:options collection="tiposVehiculo"
																	property="idTipoVehiculo"
																	labelProperty="descripcionTipoVehiculo" />
															</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
								                      </div>
								                    </div>
								                    <div class="col-lg-4">
								                      <div class="o-form__field o-form__field--select">
								                        <label class="o-form__label">Marca del veh�culo</label>
								                        <html:select property="datos(marcaVehiculo)"
															styleClass="o-form__select" styleId="marcaVehiculo">
															<html:option value="">Seleccione Marca</html:option>
															<html:options collection="marcasVehiculos"
																property="idMarcaVehiculo" labelProperty="marcaVehiculo" />
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
								                      </div>
								                    </div>
								                    <div class="col-lg-4">
								                      <div class="o-form__field o-form__field--select">
								                        <label class="o-form__label">Modelo veh�culo</label>
								                        <html:select property="datos(modeloVehiculo)"
														styleClass="o-form__select" styleId="modeloVehiculo">
														<html:option value="">Seleccione Modelo</html:option>
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
								                      </div>
								                    </div>
								                    
								                    
								                  </div>
								                  <div class="row u-mb50">
								               		<div class="col-lg-4">
								                      <div class="o-form__field o-form__field--select">
								                        <label class="o-form__label">A�o del veh�culo</label>
								                        <html:select property="datos(anyoVehiculo)"
														styleClass="o-form__select" styleId="anyoVehiculo">
														<html:option value="">Seleccione A�o</html:option>
														<html:optionsCollection name="anyoVehiculo"
															label="descripcion" value="valor" />
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
								                      </div>
								                    </div>
								                    <div class="col-lg-4">
		                    
								                  		<logic:empty name="planesCotizados">
															<logic:greaterThan value="1" name="cantProd" scope="page">
																<logic:equal name="idSubcategoria" value="36">
																	 <div class="o-form__field o-form__field--select">
																	 <label class="o-form__label">Tramo</label>
																	<html:select property="datos(producto)"
																		styleClass="o-form__select" styleId="producto">
																		<option value="">Seleccione Tramo</option>
																		<html:options collection="productos"
																			property="idProducto" labelProperty="nombreProducto" />
																	</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
																	</div>
																</logic:equal>
																<logic:equal name="idSubcategoria" value="239">
																	<div class="o-form__field o-form__field--select">
																	 <label class="o-form__label">Tramo</label>
																	<html:select property="datos(producto)"
																		styleClass="o-form__select" styleId="producto">
																		<option value="">Seleccione Tramo</option>
																		<html:options collection="productos"
																			property="idProducto" labelProperty="nombreProducto" />
																	</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
																	</div>
																</logic:equal>
																
																<logic:equal name="idSubcategoria" value="22">
																	<html:hidden property="datos(producto)" styleId="producto"
																		value="88" />
																</logic:equal>
																 																
															</logic:greaterThan>
														</logic:empty>
														<logic:lessEqual value="1" name="cantProd" scope="page">
															<logic:iterate id="producto" name="productos">
																<%
																	String idProducto = ((Producto) producto).getIdProducto(); 
																%>
																<html:hidden property="datos(producto)"
																	styleId="producto" value="<%=idProducto%>" />
																
															</logic:iterate>
														</logic:lessEqual>
													</div>
								                  </div>
								                  <logic:equal name="idSubcategoria" value="22">
                    <div class="row o-actions o-actions--flex">
                  <!-- <div class="col-lg-3"> -->
                    <!--  <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>
                    &idSubcategoria=<bean:write name='idSubcategoria'/>"><i class="mx-arrow-left"></i> Volver</a></div> -->
                  <!--  </div> -->
                    <div class="col-lg-3 offset-lg-9 u-text-right">
                      <div class="o-form__field is-last">
                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="switchFormVehiculo(true)">Cotizar<i class="o-icon">
                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                  <g id="orangeButton+rightArrow">
                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </svg></i></button>
                          </div>
                       </div>
                  	  </div>
                    </logic:equal>
                    <logic:notEqual name="idSubcategoria" value="22">
                    <div class="row o-actions o-actions--flex">
                    <!--<div class="col-lg-3">
                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>
                     &idSubcategoria=<bean:write name='idSubcategoria'/>"><i class="mx-arrow-left"></i> Volver</a></div> 
                    </div> -->
                    <div class="col-lg-3 offset-lg-9 u-text-right">
                      <div class="o-form__field is-last">
                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="switchFormVehiculo(true)">Cotizar<i class="o-icon">
                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                  <g id="orangeButton+rightArrow">
                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </svg></i></button>
                          </div>
                       </div>
                  	  </div>
                    </logic:notEqual>
												  
								                
								              </section>
								            </div>
								          </div>
								        </section>
								      </div>
								    </main>
									
							</div>
						</div>
						</div>
					</div>
				</html:form>
				
				<div id="paso3">
    				<main role="main">
							<div class="remodal-bg">
								<section class="container">
								<div class="row">
									<div class="col-12">
							              <section class="o-box o-box--quotation u-mt20 u-mb40">
							               <div id="grupo_valorizacion_iframe">
												<logic:notEmpty name="planesCotizados">
												
													<%@ include file="./includes/cotizador-valorizacion.jsp"%>
												</logic:notEmpty>
											</div>
							              </section>
							            </div>
								</div>
								</section>
							</div>
						</main>
					
			  </div>
          </div>
        </section>
      </div>
    </main>
    
    
	<iframe src="/cotizador/cotizacion/blank.jsp" width="100%" height="200"
		name="iframe_tabla" style="display: none;"> </iframe>
	<!-- FIN CONTENIDOS -->
	<%@ include file="./includes/cotizador-footer.jsp"%>

</body>
</html>

