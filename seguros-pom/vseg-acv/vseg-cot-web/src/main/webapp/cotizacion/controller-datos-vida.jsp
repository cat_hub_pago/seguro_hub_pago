<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>



<html xmlns="http://www.w3.org/1999/xhtml">

<logic:empty name="reload" scope="request">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta name="generator" content="Cencosud" />
		<title>Cotizador</title>
		<script type="text/javascript" src="js/jquery/jquery-1.4.2.js"></script>
		<link href="css/estilos-general.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/nubes.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />

		<script src="js/cotizacion.js"></script>
		<script src="js/jquery/jquery.scrollTo.js"></script>
		
		<link rel="stylesheet" href="css/jquery/jqModal.css" type="text/css" />

		<!-- <link rel="stylesheet"  href="css/estilos-light-box.css" type="text/css" media="all"/>  -->
		
		<script type="text/javascript" src="js/jquery/jqModal.js"></script>
		<script type="text/javascript">
		
		    var enviar = true;
		    var erroresArr = new Array();
			setErroresOrden();
			
			function setErroresOrden(){
				erroresArr['datos.tipoTelefono1'] = 0;
				erroresArr['datos.codigoTelefono1'] = 0;
				erroresArr['datos.numeroTelefono1'] = 0;
				erroresArr['datos.tipoTelefono2'] = 0;
				erroresArr['datos.codigoTelefono2'] = 0;
				erroresArr['datos.numeroTelefono2'] = 0;
				erroresArr['datos_region'] = 0;
				erroresArr['datos_comuna'] = 0;
				erroresArr['datos_ciudad'] = 0;
				erroresArr['datos.direccion'] = 0;
				erroresArr['datos.direccionNumero'] = 0;
				erroresArr['datos.direccionNroDepto'] = 0;
				erroresArr['benef1rutBeneficiario'] = 0;
				erroresArr['benef1dvBeneficiario'] = 0;
				erroresArr['benef1nombreBeneficiario'] = 0;
				erroresArr['benef1apellidoPaternoBeneficiario'] = 0;
				erroresArr['benef1apellidoMaternoBeneficiario'] = 0;
				erroresArr['benef1diaFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef1mesFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef1anyoFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef1parentescoBeneficiario'] = 0;
				erroresArr['benef1sexoBeneficiario'] = 0;
				erroresArr['benef1porcentajeBeneficiario'] = 0;
				erroresArr['benef2rutBeneficiario'] = 0;
				erroresArr['benef2dvBeneficiario'] = 0;
				erroresArr['benef2nombreBeneficiario'] = 0;
				erroresArr['benef2apellidoPaternoBeneficiario'] = 0;
				erroresArr['benef2apellidoMaternoBeneficiario'] = 0;
				erroresArr['benef2diaFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef2mesFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef2anyoFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef2parentescoBeneficiario'] = 0;
				erroresArr['benef2sexoBeneficiario'] = 0;
				erroresArr['benef2porcentajeBeneficiario'] = 0;
				erroresArr['benef3rutBeneficiario'] = 0;
				erroresArr['benef3dvBeneficiario'] = 0;
				erroresArr['benef3nombreBeneficiario'] = 0;
				erroresArr['benef3apellidoPaternoBeneficiario'] = 0;
				erroresArr['benef3apellidoMaternoBeneficiario'] = 0;
				erroresArr['benef3diaFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef3mesFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef3anyoFechaNacimientoBeneficiario'] = 0;
				erroresArr['benef3parentescoBeneficiario'] = 0;
				erroresArr['benef3sexoBeneficiario'] = 0;
				erroresArr['benef3porcentajeBeneficiario'] = 0;
				//INICIO ASISTENCIA EN VIAJE
				erroresArr['inputIniVig'] = 0;
				erroresArr['dueTipoTelefono1'] = 0;		
				erroresArr['dueRegion'] = 0;
				erroresArr['dueComuna'] = 0;
				erroresArr['dueCiudad'] = 0;
				erroresArr['dueCalle'] = 0;
				erroresArr['dueNumeroCalle'] = 0;
				erroresArr['dueNumeroDepto'] = 0;
				//FIN ASISTENCIA EN VIAJE
			}
			
			function resetErroresOrdenValue(){
				for(key in erroresArr){
					erroresArr[key] = 0;
				}
			}
			
			function markErrorToFocus(errorId){
				erroresArr[errorId] = 1;
			}
			
			function setFocusOnFirstError(){
				for(key in erroresArr){
					if(erroresArr[key]){
						//alert('Key: ' + key + ' - Valor: ' + erroresArr[key]);
						parent.document.getElementById(key).focus();
						break;
					}
				}
				resetErroresOrdenValue();
			}
		        
			function desplegarErroresPadre(){
				var camposRequerido = "";
				var camposOtros = "";
				var camposRequeridoMultBenef = "";
				var camposOtrosMultBenef = "";
								
				parent.$(":input").attr('class','textbox');
				
				<logic:messagesPresent>
					<logic:iterate id="error" name="erroresRequerido">
						camposRequerido += "<bean:write name="error"/>" + ";";
					</logic:iterate>
				</logic:messagesPresent>
				<logic:messagesPresent>
					<logic:iterate id="error" name="erroresRequeridoMultBenef">
						camposRequeridoMultBenef += "<bean:write name="error"/>" + ";";
					</logic:iterate>
				</logic:messagesPresent>
				<logic:messagesPresent>
					<logic:iterate id="error" name="erroresOtros">
						camposOtros += "<bean:write name="error"/>" + ";";
					</logic:iterate>
				</logic:messagesPresent>
				<logic:messagesPresent>
					<logic:iterate id="error" name="erroresOtrosMultBenef">
						camposOtrosMultBenef += "<bean:write name="error"/>" + ";";
					</logic:iterate>
				</logic:messagesPresent>			
	
				if(enviar) {
					parent.location.href = "secure/cotizador-forma-de-pago.do";
				}
				setFocusOnFirstError();
			}
			parent.document.getElementById("error.datos.tipoTelefono1").innerHTML = "";
			parent.document.getElementById("error.datos.tipoTelefono2").innerHTML = "";
		    parent.document.getElementById("error.datos.region").innerHTML = "";
		    parent.document.getElementById("error.datos.comuna").innerHTML = "";
		    parent.document.getElementById("error.datos.ciudad").innerHTML = "";
		    parent.document.getElementById("error.datos.direccion").innerHTML = "";
		    parent.document.getElementById("error.datos.direccion").innerHTML = "";
		    parent.document.getElementById("error.datos.direccion").innerHTML = "";
			parent.document.getElementById("error.inputIniVig").innerHTML = "";
			parent.document.getElementById("error.dueTipoTelefono1").innerHTML = "";
			parent.document.getElementById("error.dueRegion").innerHTML = "";
			parent.document.getElementById("error.dueComuna").innerHTML = "";
			parent.document.getElementById("error.dueCiudad").innerHTML = "";
			parent.document.getElementById("error.dueCalle").innerHTML = "";
			parent.document.getElementById("error.dueCalle").innerHTML = "";
			parent.document.getElementById("error.dueCalle").innerHTML = "";
		    if(parent.document.getElementById('errorRequeridosBenef') != null){
				parent.document.getElementById('errorRequeridosBenef').innerHTML = "";
				parent.document.getElementById('errorOtrosBenef').innerHTML = "";
			}
				
			function pintarErrores(campos,sufijo){
				var errores = campos.split(";");
				for (var i = 0; i < errores.length; i++) {
					if(errores[i] != ''){
						if(parent.document.getElementById(errores[i]) != null) {
							parent.document.getElementById(errores[i]).className='novalido';
							markErrorToFocus(errores[i]);
							
							var telefono1Ok = true;
							var telefono2Ok = true;
							var telefono3Ok = true;
						    if(sufijo == 'Contacto'){

							    if('<html:errors property="datos.tipoTelefono1"/>' != ''){
							      parent.document.getElementById("error.datos.tipoTelefono1").innerHTML = '<html:errors property="datos.tipoTelefono1"/>';
							      telefono1Ok = false;
							    }
							    if('<html:errors property="datos.codigoTelefono1"/>' != ''){
							      parent.document.getElementById("error.datos.tipoTelefono1").innerHTML = '<html:errors property="datos.codigoTelefono1"/>';
							      telefono1Ok = false;
							    }
							    if('<html:errors property="datos.numeroTelefono1"/>' != ''){
							      parent.document.getElementById("error.datos.tipoTelefono1").innerHTML = '<html:errors property="datos.numeroTelefono1"/>';
							      telefono1Ok = false;
							    }
							    if(telefono1Ok){
							      parent.document.getElementById("error.datos.tipoTelefono1").innerHTML = '';
							    }
							    
							    if('<html:errors property="datos.tipoTelefono2"/>' != ''){
							      parent.document.getElementById("error.datos.tipoTelefono2").innerHTML = '<html:errors property="datos.tipoTelefono2"/>';
							      telefono2Ok = false;
							    }
							    if('<html:errors property="datos.codigoTelefono2"/>' != ''){
							      parent.document.getElementById("error.datos.tipoTelefono2").innerHTML = '<html:errors property="datos.codigoTelefono2"/>';
							      telefono2Ok = false;
							    }
							    if('<html:errors property="datos.numeroTelefono2"/>' != ''){
							      parent.document.getElementById("error.datos.tipoTelefono2").innerHTML = '<html:errors property="datos.numeroTelefono2"/>';
							      telefono2Ok = false;
							    }
							    if(telefono2Ok){
							      parent.document.getElementById("error.datos.tipoTelefono2").innerHTML = '';
							    }
								
								if('<html:errors property="dueTipoTelefono1"/>' != ''){
							      parent.document.getElementById("error.dueTipoTelefono1").innerHTML = '<html:errors property="dueTipoTelefono1"/>';
							      telefono3Ok = false;
							    }
							    if('<html:errors property="dueCodigoTelefono1"/>' != ''){
							      parent.document.getElementById("error.dueTipoTelefono1").innerHTML = '<html:errors property="dueCodigoTelefono1"/>';
							      telefono3Ok = false;
							    }
							    if('<html:errors property="dueNumeroTelefono1"/>' != ''){
							      parent.document.getElementById("error.dueTipoTelefono1").innerHTML = '<html:errors property="dueNumeroTelefono1"/>';
							      telefono3Ok = false;
							    }
							    if(telefono3Ok){
							      parent.document.getElementById("error.dueTipoTelefono1").innerHTML = '';
							    }
							    parent.document.getElementById("error.datos.region").innerHTML = '<html:errors property="datos_region"/>';
							    parent.document.getElementById("error.datos.comuna").innerHTML = '<html:errors property="datos_comuna"/>';
							    parent.document.getElementById("error.datos.ciudad").innerHTML = '<html:errors property="datos_ciudad"/>';
							    parent.document.getElementById("error.datos.direccion").innerHTML = '<html:errors property="datos.direccion"/>';
							    parent.document.getElementById("error.datos.direccion").innerHTML = '<html:errors property="datos.direccionNumero"/>';
							    parent.document.getElementById("error.datos.direccion").innerHTML = '<html:errors property="datos.direccionNroDepto"/>';
								parent.document.getElementById("error.inputIniVig").innerHTML = '<html:errors property="inputIniVig"/>';
								parent.document.getElementById("error.dueRegion").innerHTML = '<html:errors property="dueRegion"/>';
								parent.document.getElementById("error.dueComuna").innerHTML = '<html:errors property="dueComuna"/>';
								parent.document.getElementById("error.dueCiudad").innerHTML = '<html:errors property="dueCiudad"/>';
								parent.document.getElementById("error.dueCalle").innerHTML = '<html:errors property="dueCalle"/>';
								parent.document.getElementById("error.dueCalle").innerHTML = '<html:errors property="dueNumeroCalle"/>';
								parent.document.getElementById("error.dueCalle").innerHTML = '<html:errors property="dueNumeroDepto"/>';
						    }
							
						}else{
						    if(sufijo == 'Benef'){
							    parent.document.getElementById('errorRequeridos' + sufijo).innerHTML = '<html:errors property="camposSolicitadosBenef"/>';
							    parent.document.getElementById('errorOtros' + sufijo).innerHTML = '<html:errors property="porcentajeIncompleto"/>';
						    }
						}
				   }
				}
			}
				
		</script>
		
		<script type="text/javascript">
			$(document).ready(function(){
			
			parent.$('.grupo_direccion :input').each(function(index) {
	    	 	
	    	 	if($(this).val() == "") {
	    	 		
	    	 		if(index == 0 || (index % 3 == 0)) {
	    	 			$(this).val("calle");
	    	 		}
	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
	    	 			$(this).val("n�");
	    	 		}
	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
	    	 			$(this).val("n� depto/casa");
	    	 		}
	    	 		
	    	 	}
		  		});
			});
		</script>
    </head>
    <body onload="desplegarErroresPadre()">

	</body>
</logic:empty>

<logic:notEmpty name="reload" scope="request">
	<head>
		<script type="text/javascript">
			function reloadPage(){
				parent.location.href = "ingresar-datos-vida.do";
			}
		</script>
	</head>
	<body onload="reloadPage()">
	
	</body>
</logic:notEmpty>
</html>