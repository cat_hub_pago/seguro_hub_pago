<!DOCTYPE html>
<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="cl.cencosud.acv.common.Producto"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html lang="en">
<head>
<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache">
<%@ include file="./includes/cotizador-head.jsp"%>

<script type="text/javascript">

$(function() {
   $('.datos-calle-input').attr('placeholder','Direcci�n');
   $('.datos-direccion-input').attr('placeholder','N�mero');
   $('.datos-depto-input').attr('placeholder','Dpto.');
});

function registroRemoto(){
	var rut_cliente =  "<bean:write name='datosCotizacion' property='rutCliente'/>";
	var dv_cliente = "<bean:write name='datosCotizacion' property='dv'/>";
	var diaFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='dd'/>";
	var mesFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='MM'/>";
	var anoFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='yyyy'/>";
	var val_sexo = "<bean:write name='datosCotizacion' property='sexo'/>";
	var val_estadoCivil= "<bean:write name='datosCotizacion' property='estadoCivil'/>";
	var val_nombre = "<bean:write name='datosCotizacion' property='nombre'/>";
	var val_apellidoP = "";
	var val_apellidoM = "";
	var val_email = "";
	var val_codigo_cel = "";
	var val_tipo_cel = "";
	var val_numero_cel = "";

	<logic:notEmpty name="datosCotizacion" property="apellidoPaterno">
		val_apellidoP = "<bean:write name='datosCotizacion' property='apellidoPaterno'/>";
	</logic:notEmpty>
	
	<logic:notEmpty name="datosCotizacion" property="apellidoMaterno">
		val_apellidoM = "<bean:write name='datosCotizacion' property='apellidoMaterno'/>";
	</logic:notEmpty>
	
	<logic:present property="email" name="datosCotizacion">
		val_email = "<bean:write name='datosCotizacion' property='email'/>";
	</logic:present>			

	<logic:present property="tipoTelefono1" name="datosCotizacion">
		val_tipo_cel = "<bean:write name='datosCotizacion' property='tipoTelefono1'/>";
	</logic:present>		
	
	<logic:present property="datos(cteCodigoTelefono1)" name="ingresarDatosVehiculo">
		val_codigo_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(cteCodigoTelefono1)'/>";
	</logic:present>				
	
	<logic:present property="datos(numeroTelefono1)" name="ingresarDatosVehiculo">
		val_numero_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(numeroTelefono1)'/>";
	</logic:present>				
					
	var url = '/cotizador/registroClienteRemote.do';
	var params = "?rut_cliente=" + rut_cliente;
	params += "&dv_cliente=" + dv_cliente;
	params += "&diaFechaNacimiento=" + diaFechaNacimiento;
	params += "&mesFechaNacimiento=" + mesFechaNacimiento;
	params += "&anoFechaNacimiento=" + anoFechaNacimiento;
	params += "&val_sexo=" + val_sexo;
	params += "&val_estadoCivil=" + val_estadoCivil;
	params += "&val_nombre=" + val_nombre;
	params += "&val_apellidoP=" + val_apellidoP;
	params += "&val_apellidoM=" + val_apellidoM;
	params += "&val_email=" + val_email;
	params += "&val_codigo_cel=" + val_codigo_cel;
	params += "&val_tipo_cel=" + val_tipo_cel;
	params += "&val_numero_cel=" + val_numero_cel;		

	$("#cargaRegistrate").attr('src',url+params);
	$("#modalRegistrate").modal({
	backdrop: 'static',
	show: true
	});
}
</script>

<script type="text/javascript">
	$(document).ready(function(){
	<% if(request.getSession().getAttribute("fechaMaxima") != null){
		request.getSession().setAttribute("fechaMaxima",null);
	%>
		$("#incomplete-text-paso1").html("La contrataci�n del plan se puede realizar hasta con un a�o de anticipaci�n a �la fecha de inicio del viaje.");
		$("#incomplete-paso1").show("fast");
		$("#inputIniVig").css("border-color", "red");
		$("#inputIniVig").focus();
	
	<%}%>
		bar(0,2);   
		paso2bar = 100;
		$("#inputIniVig").datepicker({
			minDate: 0,
			dateFormat: 'dd/mm/yy',
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
			'Junio', 'Julio', 'Agosto', 'Septiembre',
			'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
			'May', 'Jun', 'Jul', 'Ago',
			'Sep', 'Oct', 'Nov', 'Dic']
		}); 
		
		// Calendario para fecha de beneficiario
		<% String a�oActual = (String)request.getAttribute("a�oActual");	%>
		
		cargaDia('idFechaDia');
		cargaMes('idFechaMes');
		cargaAnyo('idFechaAnyo', true);
		
		
		
		$('.datos-calle-input').attr('placeholder','calle');
		$('.datos-direccion-input').attr('placeholder','n�');
		$('.datos-depto-input').attr('placeholder','n� depto/casa');
		$('.datos-telefono-input').attr('placeholder','tel�fono');

		regionToRoman('cteRegion');		
		regionToRoman('dueRegion');
		
		// Filtro de Region
		$("select#cteRegion").change(function() {                 
			$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
				$("select#cteComuna option").remove();
					var options = '';
					options += '<option value="">Seleccione</option>';          
					for(var i = 0; i < data.length; i++) {
					options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
				}
				$('select#cteComuna').html(options);
				$("select#cteCiudad").html('<option value="">Seleccione</option>');
			});
		});
			
		// Filtro de la comuna
		$("select#cteComuna").change(function() {                     
			$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
				$('select#cteCiudad option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';

				for(var i = 0; i < data.length; i++) {
				options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
				}

				$('select#cteCiudad').html(options);
			});
		});
		
		//INICIO TERCEROS
		if($("select#dueRegion").val() != null && $("select#dueRegion").val() != "" ) {
			var valor = "";
			$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("select#dueRegion").val(), function(data){
				$('select#dueComuna option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';
				<logic:present property="datos(dueComuna)" name="ingresarDatosVida">
					valor = "<bean:write name='ingresarDatosVida' property='datos(dueComuna)'/>";
				</logic:present>
				for(var i = 0; i < data.length; i++) {
					if(valor!='' && valor==data[i].id){
						options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
					} else {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}
				}
				$('select#dueComuna').html(options);
				recargarCiudadesPaso2HogarDue(valor);
			});
		}
		
		$("select#dueRegion").change(function() {     		
			if($("select#dueRegion").val() != null && $("select#dueRegion").val() != "" ) {
				var valor = "";
				$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("select#dueRegion").val(), function(data){
					$('select#dueComuna option').remove();
					var options = '';
					options += '<option value="">Seleccione</option>';
					<logic:present property="datos(dueComuna)" name="ingresarDatosVida">
						valor = "<bean:write name='ingresarDatosVida' property='datos(dueComuna)'/>";
					</logic:present>
					for(var i = 0; i < data.length; i++) {
						if(valor!='' && valor==data[i].id){
							options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
						} else {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
					}
					$('select#dueComuna').html(options);
					recargarCiudadesPaso2HogarDue(valor);
				});
			}
		});
		
		$("select#dueComuna").change(function() {                     
			$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
				$('select#dueCiudad option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';

				for(var i = 0; i < data.length; i++) {
				options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
				}

				$('select#dueCiudad').html(options);
			});
		});

		function recargarCiudadesPaso2HogarDue(valor) {
			$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + valor, function(data){
				var valorciudad = '';
				$('select#dueCiudad option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';
				<logic:present property="datos(dueCiudad)" name="ingresarDatosVida">
					valorciudad = '<bean:write name='ingresarDatosVida' property='datos(dueCiudad)'/>';
				</logic:present>
				for(var i = 0; i < data.length; i++) {
					if(valorciudad!='' && valorciudad==data[i].id) {
						options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
					} else {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}
				}
				$('select#dueCiudad').html(options);
			});
		}
		//FIN TERCEROS
	});
</script>

<script type="text/javascript">				
	function sinRegistro(){
		validateInput("validaForm");
		closeModal();
		var rutSinRegistro = "<bean:write name='datosCotizacion' property='rutCliente'/>";
		document.getElementById('formulario').action="/cotizador/guardar-datos-adicionales-vida.do?sinRegistro=1&rutSinRegistro="+rutSinRegistro;
		submitPaso2Vida();
	}
	
	function showModal()
	{
			var result = validateInput("validaForm");
			validaSubmitPaso2Vida();
	    	if(validacionInput == 0){	    	
		    	var inst = $('[data-remodal-id=continueModal]').remodal();
				inst.open();
	   }
	}
	
	function conRegistro(){
		validateInput("formulario");
		if(validacionInput == 0) {
			submitPaso2Vida();
		}
	}
	function renameParentescoPost(parentesco){
		if(parentesco == '�l mismo'){
			return 'E';
		}else if(parentesco == 'C�nyuge'){
			return 'C';
		}else if(parentesco == 'Hijo'){
			return 'H';
		}else if(parentesco == 'Padre'){
			return 'P';
		}else if(parentesco == 'Madre'){
			return 'M';
		}else{
			return 'O';
		}
	}
	
	function renameSexoPost(sexo){
		if(sexo == 'Femenino'){
			return 'F';
		}else if(sexo == 'Masculino'){
			return 'M';
		}
	}
	
	var distribucionesBenef = new Array();
	
	function agregaFila() {}
	
	function sobreCienPorcentaje(){
		var suma = 0;
		var flag;
		for(key in distribucionesBenef){
			suma = suma + ((distribucionesBenef[key] == '')? 0 : parseInt(distribucionesBenef[key]));
		}
		if(suma > 100){
			return false;
		}else{
			return true;
		}
	}

	function calcular(){
		var suma = 0;
		for(var i=0;i<distribucionesBenef.length;i++){
			suma = suma + parseInt(distribucionesBenef[i]);
		}
		return suma;
	}

	function checkPercentValue(object){
		//Cuando hay valor, el evento onBlur se activa y viene ac�
		var value = 0;
		var error = false;
		(object.value == '')? value = 0: value = object.value;
		distribucionesBenef[object.id] = value;
		var check = sobreCienPorcentaje();
		if(value > 100){
			error = true;
			setTimeout(function(){$('#' + object.id).focus();},10);
			$("#incomplete-text-"+'paso1').html("El valor debe ser menor a 100%");
			$("#incomplete-"+'paso1').show("fast");
			$('#' + object.id).css("border-color","red");
			$('#' + object.id).focus();
			return false;
		}else if(!check){
			error = true;
			object.value = "";
			setTimeout(function(){$('#' + object.id).focus();},10);
			$("#incomplete-text-"+'paso1').html("El porcentaje de las sumas de los beneficiarios no puede ser mayor a 100%");
			$("#incomplete-"+'paso1').show("fast");
			$('#' + object.id).css("border-color","red");
			$('#' + object.id).focus();
			return false;
		}else{
			$('#' + object.id).attr('style','');
		}
		return true;
		if(error){
			$('#textoPorcentajeBenef').append('<p style="padding:2px; color:#FF8000; font-weight:bold">* Los beneficiarios que asignes deben sumar 100% en la distribuci�n de tu Patrimonio</p>');
		}else{
			$('#textoPorcentajeBenef').empty();
		}
	}
	
	function autocompletarfechaVida(input) {
		if(input && input.value && input.value.length && input.value.length >= 8) {	
			var value = replaceAll(input.value, "/", "" );
			var dia = value.substring(0,2);
			var mes = value.substring(2,4);
			var anio = value.substring(4,8);
			input.value = dia + "/" + mes + "/" + anio;
		}		  
    }
    
    function validaSubmitPaso2Vida(){
		var valor2 = document.getElementById("datos.numeroTelefono2").value;
		if(valor2 == "" || valor2 == "1"){
			document.getElementById("datos.numeroTelefono2").value = "111";
			document.getElementById("datos.codigoTelefono2").value = "";
		}
		
		<logic:present property="datos(cteCodigoTelefono1)" name="ingresarDatosVida">
			val_codigo_cel = "<bean:write name='ingresarDatosVida' property='datos(cteCodigoTelefono1)'/>";
		</logic:present>				
	
		<logic:present property="datos(numeroTelefono1)" name="ingresarDatosVida">
			val_numero_cel = "<bean:write name='ingresarDatosVida' property='datos(numeroTelefono1)'/>";
		</logic:present>
	
		validateInput("paso1");
		<logic:equal value="false" name="datosCotizacion" property="clienteEsAsegurado">
		validateInput("paso2");
		</logic:equal>

		if (validacionInput != 0) {
			return false;
		}
		
		<logic:present name="restricciones">
			<logic:equal property="requiereBeneficiarios" name="restricciones" value="true">	
				
				<logic:notEqual name="idSubcategoria" value="221">				
					var suma = calcular();
					if(suma<100){
						validacionInput = 1;
						$("#incomplete-text-"+'paso1').html("El porcentaje de las sumas de los beneficiarios no puede ser menor a 100%");
						$("#incomplete-"+'paso1').show("fast");
						return false;
					}	
				</logic:notEqual>
				
			</logic:equal>
		</logic:present>

	}
		
	function submitPaso2Vida(){
		var valor2 = document.getElementById("datos.numeroTelefono2").value;
		if(valor2 == "" || valor2 == "1"){
			document.getElementById("datos.numeroTelefono2").value = "111";
			document.getElementById("datos.codigoTelefono2").value = "";
		}
		
		<logic:present property="datos(cteCodigoTelefono1)" name="ingresarDatosVida">
			val_codigo_cel = "<bean:write name='ingresarDatosVida' property='datos(cteCodigoTelefono1)'/>";
		</logic:present>				
	
		<logic:present property="datos(numeroTelefono1)" name="ingresarDatosVida">
			val_numero_cel = "<bean:write name='ingresarDatosVida' property='datos(numeroTelefono1)'/>";
		</logic:present>
	
		validateInput("paso1");
		<logic:equal value="false" name="datosCotizacion" property="clienteEsAsegurado">
		validateInput("paso2");
		</logic:equal>

		if (validacionInput != 0) {
			return false;
		}
		
		<logic:present name="restricciones">
			<logic:equal property="requiereBeneficiarios" name="restricciones" value="true">	
				
				<logic:notEqual name="idSubcategoria" value="221">				
					var suma = calcular();
					if(suma<100){
						$("#incomplete-text-"+'paso1').html("El porcentaje de las sumas de los beneficiarios no puede ser menor a 100%");
						$("#incomplete-"+'paso1').show("fast");
						return false;
					}	
				</logic:notEqual>
				
				//Obtener valores tabla
				var cantidadFilas = 1;
				$('#tab_logic tr').each(function(){
					$('<input>').attr({type:'hidden', name:'idFilaBeneficiario[]', value:cantidadFilas}).appendTo('#formulario');	
										
					var tdRut = $(this).find(".tdRut").html();
					var rutBenef = tdRut.replace(/[-]/g, '');
					var dvBenef = rutBenef.substring(rutBenef.length-1,rutBenef.length);
					var rutBenefSinDv = rutBenef.substring(0,rutBenef.length-1);
					$('<input>').attr({type:'hidden', name:'rutBeneficiario[]', value:rutBenefSinDv}).appendTo('#formulario');	
					$('<input>').attr({type:'hidden', name:'dvBeneficiario[]', value:dvBenef}).appendTo('#formulario');	
					
					var tdNombre = $(this).find(".tdNombre").html();
					$('<input>').attr({type:'hidden', name:'nombreBeneficiario[]', value:tdNombre}).appendTo('#formulario');	
					var tdApePaterno = $(this).find(".tdApePaterno").html();
					$('<input>').attr({type:'hidden', name:'apellidoPaternoBeneficiario[]', value:tdApePaterno}).appendTo('#formulario');	
					var tdApMaterno = $(this).find(".tdApMaterno").html();
					$('<input>').attr({type:'hidden', name:'apellidoMaternoBeneficiario[]', value:tdApMaterno}).appendTo('#formulario');	
					
					var tdFechaNac = $(this).find(".tdFechaNac").html();
					var fechaNacBenef = tdFechaNac.replace(/[-]/g, '');
					var diaNacBenef = fechaNacBenef.substring(0,2);
					$('<input>').attr({type:'hidden', name:'diaFechaNacimientoBeneficiario[]', value:diaNacBenef}).appendTo('#formulario');	
					var mesNacBenef = fechaNacBenef.substring(2,4);
					$('<input>').attr({type:'hidden', name:'mesFechaNacimientoBeneficiario[]', value:mesNacBenef}).appendTo('#formulario');	
					var anyoNacBenef = fechaNacBenef.substring(4,8);
					$('<input>').attr({type:'hidden', name:'anyoFechaNacimientoBeneficiario[]', value:anyoNacBenef}).appendTo('#formulario');	
					
					var tdParentesco = $(this).find(".tdParentesco").html();
					tdParentesco = renameParentescoPost(tdParentesco);
					$('<input>').attr({type:'hidden', name:'parentescoBeneficiario[]', value:tdParentesco}).appendTo('#formulario');	
					
					var tdSexo = $(this).find(".tdSexo").html();
					tdSexo = renameSexoPost(tdSexo);
					$('<input>').attr({type:'hidden', name:'sexoBeneficiario[]', value:tdSexo}).appendTo('#formulario');	
					
					<logic:notEqual name="idSubcategoria" value="221">
						var tdDistri = $(this).find(".tdDistri").html();
						var distriSinPorcentaje = tdDistri.substring(0,tdDistri.length-1);
						$('<input>').attr({type:'hidden', name:'porcentajeBeneficiario[]', value:distriSinPorcentaje}).appendTo('#formulario');	
					</logic:notEqual>
					cantidadFilas++;
				});
			</logic:equal>
		</logic:present>
		
		$('#waitModal').modal({backdrop: 'static', keyboard: false});
		$('.grupo_direccion :input').each(function(){
			if($(this).val() == "calle" || $(this).val() == "n�" || $(this).val() == "n� depto/casa" ) {
				$(this).val("");
			}
		});
		$("#username").val($("#rutpaso2").val() + '-' + $("#dvpaso2").val());
		$("#password").val($("#clavepaso2").val());
				
		$("#formulario").submit();
	}

	function daysInMonth(month,year) {
		var m = [31,28,31,30,31,30,31,31,30,31,30,31];
		if (month != 2) return m[month - 1];
		if (year%4 != 0) return m[1];
		if (year%100 == 0 && year%400 != 0) return m[1];
		return m[1] + 1;
	}	
</script>

<script type="text/javascript">

$(document).ready(function(){
 	$("#fechaCompletaDuenyo").datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "both",
		yearRange: '1936:1998',
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
		'Junio', 'Julio', 'Agosto', 'Septiembre',
		'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
		'May', 'Jun', 'Jul', 'Ago',
		'Sep', 'Oct', 'Nov', 'Dic']
	}); 

 	$("#fechaCompleta").datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "both",
		yearRange: '1936:1998',
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
		'Junio', 'Julio', 'Agosto', 'Septiembre',
		'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
		'May', 'Jun', 'Jul', 'Ago',
		'Sep', 'Oct', 'Nov', 'Dic']
	}); 
});
</script>

<script type="text/javascript">
	function renameParentescoTabla(parentesco){
		if(parentesco == 'E'){
			return '�l mismo';
		}else if(parentesco == 'C'){
			return 'C�nyuge';
		}else if(parentesco == 'H'){
			return 'Hijo';
		}else if(parentesco == 'P'){
			return 'Padre';
		}else if(parentesco == 'M'){
			return 'Madre';
		}else{
			return 'Otro';
		}
	}
	
	function renameSexoTabla(sexo){
		if(sexo == 'F'){
			return 'Femenino';
		}else if(sexo == 'M'){
			return 'Masculino';
		}
	}
	
	function clearInputs(){
		$("#inputRutBenef").val('');
		$("#inputNombreBenef").val('');
		$("#inputApePaternoBenef").val('');
		$("#inputApeMaternoBenef").val('');
		$("#inputFechaNacBenef").val('');
		$("#inputParentescoBenef").val('');
		$("#inputSexoBenef").val('');
		$("#inputPorcentajeBenef").val('');
	}
	
	//valida la fecha nacimiento contr al fecha del viaje no puede ser menor a 8 d�as
	function validarFechaNacimiento(){
		if($("#inputIniVig").val() != ""){
			if($("#inputFechaNacBenef").val() != ""){
				var fechaFin = $("#inputFechaNacBenef").val().split("-");
				var fechaIni = $("#inputIniVig").val().split("/"); 
				
				var fechaViaje = new Date (parseInt(fechaIni[2]),parseInt(fechaIni[1]-1),parseInt(fechaIni[0]));
				 
				var fechaNacimiento = new Date (fechaFin[2],fechaFin[1]-1,fechaFin[0]); 
				var dif = fechaViaje - fechaNacimiento;
 				var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
				if(dias < 8){
					$("#incomplete-text-paso1").html("La fecha de nacimiento no debe ser menor a 8 d�as");
					$("#incomplete-paso1").show("fast");
					$("#inputFechaNacBenef").css("border-color", "red");
					$("#inputFechaNacBenef").focus();
					return false;
				}else{
					return true;
				}
			}else{
				$("#incomplete-text-paso1").html("Por favor ingrese una fecha de nacimiento para el asegurado adicional");
				$("#incomplete-paso1").show("fast");
				$("#idFechaDia").css("border-color", "red");
				$("#idFechaMes").css("border-color", "red");
				$("#idFechaAnyo").css("border-color", "red");
				return false;	
			}
		}else{
			$("#incomplete-text-paso1").html("Por favor ingrese una fecha para el inicio del viaje");
			$("#incomplete-paso1").show("fast");
			$("#inputIniVig").css("border-color", "red");
			$("#inputIniVig").focus();
			return false;
		}
			
	}
	
	function validateInputBeneficiarios(id){
		validacionInputBenef = 0;
		$('.datos-rut-benef').attr('rut','rut');
		$('.datos-fecha-benef').attr('fecha','fecha');
			
		console.log('in validate input: '+id);
		$('#'+id+'').find(":input:not(:hidden,:button,:submit)").each(function(){
			console.log('testing ' + this.id);
			var optional = $(this).hasClass("optional beneficiario");
			var validaEdad = $(this).hasClass("validaEdad");
			
			if(optional == true){
				$(this).css("border-color", "#cecece");
				if(!this.value){
					$("#incomplete-text-paso1").html("Por favor complete el formulario correctamente");
					$("#incomplete-paso1").show("fast");
					$(this).css("border-color", "red");
					$(this).focus();
					console.log(this.id+ ' est� vac�o');
					validacionInputBenef = 1;
					return false;
				}
			}
			
			if(validaEdad == true){
				$(this).css("border-color", "#cecece");
				if(!calcularEdadFecha(this.value,18,75)){
					$("#incomplete-text-paso1").html("La edad requerida es mayor de 18 y menor de 75 a�os");
					$("#incomplete-paso1").show("fast");
					$(this).css("border-color", "red");
					$(this).focus();
					console.log(this.id+ 'Edad incorrecta');
					validacionInputBenef = 1;
					return false;
				}
			}
			
			if($(this).attr("fecha")) {
				$(this).css("border-color", "#cecece");
				console.log("testing fecha Benef");
				var result = validarFecha(this.value);
				if(result == false) {
					$("#incomplete-text-paso1").html("La fecha tiene formato incorrecto");
					$("#incomplete-paso1").show("fast");
					$(this).css("border-color","red");
					$(this).focus();
					console.log(this.id + ' formato incorrecto');
					validacionInputBenef = 1;
					return false;
					} 
				}
				
			if($(this).attr("rut")) {
				$(this).css("border-color", "#cecece");
				if( (this.value)){
					console.log("testing rut Benef");
					var result = validaRut(this.value);
					if(result==false){
						$("#incomplete-text-paso1").html("El rut tiene el formato incorrecto");
						$("#incomplete-paso1").show("fast");
						$(this).css("border-color","red");
						$(this).focus();
						console.log(this.id + ' formato incorrecto');
						validacionInputBenef = 1;
						return false;
					} 
				}
	    	}
			
		});
		
		if (validacionInputBenef == 0) {
			$("#incomplete-text-paso1").html("");
			$("#incomplete-paso1" + id).hide("fast");
			return true;
		}else{
			return false;
		}
	}
	
	var filas = [];
	<% String contratanteAsegurado = (String)request.getAttribute("contratanteAsegurado");	%>
	var contratanteAsegurado = '<%=contratanteAsegurado %>';
	var integrantes = 0;
	
    $(document).ready(function(){
	<% String integrantes = (String)request.getAttribute("integrantes");	%>
	integrantes = '<%=integrantes %>';
	
	var idSubcategoriaViajes = 0;
	<logic:equal name="idSubcategoria" value="221">			
		idSubcategoriaViajes = 221;
	</logic:equal>
	
	
	
	/**
	* Funcion para agregar a la tabla.
	*/
    $("#add_row").click(function(){
    		console.log("----> integrantes en validacion "+integrantes);
    		if(idSubcategoriaViajes != 221){
    			agregarFilas();
    			return false;
    		}else if(filas.length < integrantes && idSubcategoriaViajes == 221){
    			agregarFilas();
    			return false;
			}else{
				$("#incomplete-text-paso1").html("No se pueden agregar m�s integrantes");
				$("#incomplete-paso1").show("fast");
				return false;
			}
        });
 
        /**
         * Funcion para eliminar la ultima columna de la tabla.
         * Si unicamente queda una columna, esta no sera eliminada
         */
        $("#del_row").click(function(){
            // Obtenemos el total de columnas (tr) del id "tabla"
            var trs=$("#tab_logic tr").length;
            if(trs>=1){
                // Eliminamos la ultima columna
                $("#tab_logic tr:last").remove();
				filas.pop();
				<logic:notEqual name="idSubcategoria" value="221">
					distribucionesBenef.pop();	
				</logic:notEqual>
            }
        });
        <logic:equal name="idSubcategoria" value="221">
	        $("#add_row").click();
	        validarRegionComuna();
	        
	      	$("a[name='datosContratante']").click();
			scroll(0,0);
		</logic:equal>
    });
    
    function validarRegionComuna(){
    
    	var cteRegionVal = "";
    	var cteComunaVal = "";
    	var cteCiudadVal = "";
	    <logic:notEmpty name="ingresarDatosVida" property="datos(cteRegion)">
			cteRegionVal = "<bean:write name='ingresarDatosVida' property='datos(cteRegion)'/>";
			$("#cteRegion").val(cteRegionVal);
			$("#cteRegion").change();
			console.log("-->Region registro "+cteRegionVal);
			<logic:notEmpty name="ingresarDatosVida" property="datos(cteComuna)">
				cteComunaVal = "<bean:write name='ingresarDatosVida' property='datos(cteComuna)'/>";
				setTimeout(
					  function(){
					   	$("#cteComuna").val(cteComunaVal);
						$("#cteComuna").change();
						console.log("-->Comuna registro "+cteComunaVal);
							<logic:notEmpty name="ingresarDatosVida" property="datos(cteCiudad)">
								cteCiudadVal = "<bean:write name='ingresarDatosVida' property='datos(cteCiudad)'/>";
								setTimeout(
									  function(){
									   	$("#cteCiudad").val(cteCiudadVal);
										$("#cteCiudad").change();
										console.log("-->Ciudad registro "+cteCiudadVal);
									 }, 2000);
							</logic:notEmpty>
					 }, 2000);
			</logic:notEmpty>
		</logic:notEmpty>
    }
    
    var numerodeBenficiarios = 5;
    
					
	function closeModal(){
						$('[data-remodal-id=continueModal]').remodal().close();
					}
    
    function agregarFilas(){
    	<logic:equal name="idSubcategoria" value="221">
	    	if(contratanteAsegurado == "0"){
		    	<logic:equal name="idSubcategoria" value="221">	
					if (validarFechaNacimiento()== false) {
						return false;
					}	
				</logic:equal>
		   
				if (!validateInputBeneficiarios("formularioBeneficiarios")) {
					console.log(" el contratante no es asegurado --->"+contratanteAsegurado);
					return false;
				}
			}else{ 
				<logic:equal name="idSubcategoria" value="221">
					console.log(" el contratante es asegurado --->"+contratanteAsegurado);
					<% String aseguradoRut = (String)request.getAttribute("aseguradoRut");	%>
					var aseguradoRut = '<%=aseguradoRut %>';
					
					var rut_cliente =  "<bean:write name='datosCotizacion' property='rutCliente'/>";
					$("#inputRutBenef").val(aseguradoRut);
					$("#inputRutBenef").keyup();
					
					<% String aseguradoNombre = (String)request.getAttribute("aseguradoNombre");	%>
					var aseguradoNombre = '<%=aseguradoNombre %>';
					$("#inputNombreBenef").val(aseguradoNombre);
					
					<% String aseguradoApellidoP = (String)request.getAttribute("aseguradoApellidoP");	%>
					var aseguradoApellidoP = '<%=aseguradoApellidoP %>';
					$("#inputApePaternoBenef").val(aseguradoApellidoP);
					
					<% String aseguradoApellidoM = (String)request.getAttribute("aseguradoApellidoM");	%>
					var aseguradoApellidoM = '<%=aseguradoApellidoM %>';
					$("#inputApeMaternoBenef").val(aseguradoApellidoM);
					
					
					<% String aseguradoFechaNacimiento = (String)request.getAttribute("aseguradoFechaNacimiento");	%>
					var aseguradoFechaNacimiento = '<%=aseguradoFechaNacimiento %>';
					
					$("#inputFechaNacBenef").val(aseguradoFechaNacimiento);
					$("#inputFechaNacBenef").focus();
					$("#inputFechaNacBenef").keyup();
					
					$("#inputParentescoBenef").val("E");
					
					<% String aseguradoSexo = (String)request.getAttribute("aseguradoSexo");	%>
					var aseguradoSexo = '<%=aseguradoSexo %>';
					$("#inputSexoBenef").val(aseguradoSexo);
					
					console.log("----> numerodeBenficiarios antes"+numerodeBenficiarios);
					console.log("----> integrantes antes"+integrantes);
				</logic:equal>
				<logic:notEqual name="idSubcategoria" value="221">
					console.log("---> no es viaje grupal");
					return false;
				</logic:notEqual>
			}
		</logic:equal>
		if (!validateInputBeneficiarios("formularioBeneficiarios")) {
				console.log(" el contratante no es asegurado --->"+contratanteAsegurado);
				return false;
			}
						
		var rut = $("#inputRutBenef").val();
		var nombre = $("#inputNombreBenef").val();
		var aPaterno = $("#inputApePaternoBenef").val();
		var aMaterno = $("#inputApeMaternoBenef").val();
		var fechaNac = $("#inputFechaNacBenef").val();
		var parentesco = $("#inputParentescoBenef").val();
		var cambioParentesco = renameParentescoTabla(parentesco);
		var sexo = $("#inputSexoBenef").val();
		var cambioSexo = renameSexoTabla(sexo);
		<logic:notEqual name="idSubcategoria" value="221">			
			var distribucion = $("#inputPorcentajeBenef").val();
		</logic:notEqual>
		var tds=$("#tab_logic tr:first td").length;
		var trs=$("#tab_logic tr").length;
		var nuevaFila="<tr>";
		var validacion=true;
		if(contratanteAsegurado == "0"){
			validacion= switchForm3(true);
		}else{
			contratanteAsegurado = "0";
		}
		
	
		<logic:equal name="idSubcategoria" value="221">	
			var validarDistribucion = true;
			var validacionMayor = true;
		</logic:equal>
		<logic:notEqual name="idSubcategoria" value="221">	
			var validarDistribucion = checkPercentValue(document.getElementById("inputPorcentajeBenef"));
			var validacionMayor = sobreCienPorcentaje();
		</logic:notEqual>
		
		var beneficiarioNoExiste = true;
		$('#tab_logic tr').each(function(){								
			var tdRutExistente = $(this).find(".tdRut").html();
			if(tdRutExistente==rut){
				$("#incomplete-text-"+'paso1').html("El beneficiario ya se encuentra registrado");
				$("#incomplete-"+'paso1').show("fast");
				beneficiarioNoExiste = false;
			}
		});
		
		
		if(validarDistribucion && validacion && validacionMayor && beneficiarioNoExiste){
			var numFilaBenef = filas.length+1;
			console.log("Numero filas:"+numFilaBenef);
			//VALIDACION VIAJE GRUPAL
			console.log("----> numerodeBenficiarios en validacion"+numerodeBenficiarios);
			if(numFilaBenef>numerodeBenficiarios){
				$("#incomplete-text-"+'paso1').html("Este producto tiene un limite de "+numerodeBenficiarios+" beneficiarios");
				$("#incomplete-"+'paso1').show("fast");
				return false;
			}else{
				nuevaFila+="<td class='tdRut'>"+rut+"<td class='tdNombre'>"+nombre+"<td class='tdApePaterno'>"+aPaterno+
					"<td class='tdApMaterno'>"+aMaterno+"<td class='tdFechaNac'>"+fechaNac+"<td class='tdParentesco'>"+cambioParentesco+
					"<td class='tdSexo'>"+cambioSexo<logic:notEqual name="idSubcategoria" value="221">+"<td class='tdDistri'>"+distribucion+"%"</logic:notEqual>;
			
				var fila = rut+","+nombre+","+aPaterno+","+aMaterno+","+fechaNac+","+cambioParentesco+","+cambioSexo
						<logic:notEqual name="idSubcategoria" value="221">+","+distribucion</logic:notEqual>;
				
				//Filas posee los beneficiarios
				filas.push(fila);
				console.log("Beneficiarios: "+filas);
				
				//Posee las distribuciones para realizar calculos 
				<logic:notEqual name="idSubcategoria" value="221">
					distribucionesBenef.push(distribucion);
				</logic:notEqual>
				nuevaFila+="</tr>";
				$("#tab_logic").append(nuevaFila);
				clearInputs();
			}	
		}
    }
	

	
    </script>
</head>
<body onload="desplegarErrores();">
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="./includes/cotizador-header.jsp"%>

	
		<%@ include file="./includes/cotizador-breadcrumb.jsp"%>
		<!-- INICIO  CONTENIDOS -->
		<main role="main">
		  <div class="remodal-bg">
			<section class="container">
			  <div class="row">
				<div class="col-12">
              		<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
            </div>
		<!-- include file="./includes/cotizador-resumen.jsp" -->
			<html:form styleId="formulario"
				action="/guardar-datos-adicionales-vida.do" method="post"
				target="ifrEnvio">
				
					<!-- include file="./includes/cotizador-percent-bar.jsp" -->
					<div id="contenido-desplegado-cotizador">
						<div id="paso1">
						<div id="validaForm">
							<!-- Titulo -->
							<div class="col-12">
							<section class="o-box o-box--recruiting u-mtb50">
							<div class="o-steps">
							  <div class="o-steps__item o-steps__item--current"><span class="o-steps__milestone"><span class="o-steps__value">1</span></span>
								<h4 class="o-steps__title">Informaci�n contratante</h4>
							  </div>
							  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">2</span></span>
								<h4 class="o-steps__title">Datos del seguro</h4>
							  </div>
							  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">3</span></span>
								<h4 class="o-steps__title">Confirmaci�n y pago</h4>
							  </div>
							</div>
							<!--  fin col-lg-12 -->
							<div class="u-pad20">
								<div class="row o-form o-form--standard o-form--linear o-form--small">
								<div class="col-lg-8 u-mobile_second">
									<h2 class="o-title o-title--subtitle u-mb10">Informaci�n contratante</h2>
									<p class="o text"></p>
									<form class="o-form o-form--standard o-form--linear o-form--small" id="form_recruiting_step1" action="">
										<div class="row u-mt40">
									<!-- TELEFONO1 -->
									<div class="col-lg-6">
										<div class="o-form__field">
											<label class="o-form__label">Tel�fono 1</label>
											<div class="row">
												<div class="col-4">
												<div class="o-form__field o-form__field--select">
												<html:select property="datos(tipoTelefono1)"
													styleClass="o-form__select" styleId="datos.tipoTelefono1">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="tipoTelefono"
														label="descripcion" value="valor" />
												</html:select><span class="o-form__line"></span>
												</div>
												</div>
												<div class="col-3">
												<div class="o-form__field o-form__field--select">
												<html:select property="datos(codigoTelefono1)"
													styleClass="o-form__select" styleId="datos.codigoTelefono1">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="codigoArea"
														label="descripcion" value="valor" />
												</html:select><span class="o-form__line"></span>
												</div>
												</div>
												<div class="col-5">
												<div class="o-form__field o-form__field--select">
												<html:text property="datos(numeroTelefono1)"
													styleClass="o-form__input datos-telefono-input"
													maxlength="8" styleId="datos.numeroTelefono1"
													onkeypress="return isNumberKey(event)"></html:text><span class="o-form__line"></span>
													</div>
												</div>
											</div><span class="o-form__message" id="phone--recruiting"></span>
										</div>
										<!-- fin form-group -->
									</div>
									<!-- fin de TELEFONO -->

									<!-- TELEFONO2 -->
									<div class="col-lg-6">
										<div class="o-form__field">
											<label class="o-form__label">Tel�fono 2</label>
											<div class="row">
											<div class="col-4">
												<div class="o-form__field o-form__field--select">
												<html:select property="datos(tipoTelefono2)"
													styleClass="o-form__select optional" styleId="datos.tipoTelefono2">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="tipoTelefono"
														label="descripcion" value="valor" />
												</html:select>
												</div>
											</div><span class="o-form__line"></span>
											<div class="col-3">
											<div class="o-form__field o-form__field--select">
												<html:select property="datos(codigoTelefono2)"
													styleClass="o-form__select optional"
													styleId="datos.codigoTelefono2">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="codigoArea"
														label="descripcion" value="valor" />
												</html:select>
												</div>
											</div><span class="o-form__line"></span>
											<script type="text/javascript">
												$(document).ready(function(){
												<logic:present property="datos(codigoTelefono2)" name="ingresarDatosVida">
														var val_codigo_cel = "<bean:write name='ingresarDatosVida' property='datos(codigoTelefono2)'/>";
														$("select[name='datos(codigoTelefono2)']").val(val_codigo_cel);
												</logic:present>
											});	
											</script>
											<div class="col-5">
											<div class="o-form__field o-form__field--select">
												<html:text property="datos(numeroTelefono2)"
													styleClass="o-form__input optional datos-telefono3-input"
													maxlength="8" styleId="datos.numeroTelefono2"
													onkeypress="return isNumberKey(event)" ></html:text>
													</div>
											</div><span class="o-form__line"></span>
											</div><span class="o-form__message" id="phone--recruiting"></span>
										</div>
										<!-- fin form-group -->
									</div>
									<!-- fin de TELEFONO -->
								</div>
								<!-- fin de row -->
								<!-- FILA 2 -->
								<div class="row">
									<div class="col-lg-6">
										<div class="o-form__field o-form__field--select">
											<label class="o-form__label">Regi�n</label>
											<html:select property="datos(region)"
												styleClass="o-form__select" styleId="cteRegion">
												<html:option value="">Seleccione regi�n</html:option>
												<html:options collection="regiones" property="id"
													labelProperty="descripcion"/>
											</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="o-form__field o-form__field--select">
											<label class="o-form__label">Comuna</label>
											<html:select property="datos(comuna)"
												styleClass="o-form__select" styleId="cteComuna">
												<html:option value="">Seleccione comuna</html:option>
											</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="o-form__field o-form__field--select">
											<label class="o-form__label">Ciudad</label>
											<html:select property="datos(ciudad)"
												styleClass="o-form__select" styleId="cteCiudad">
												<html:option value="">Seleccione ciudad</html:option>
											</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
										</div>
									</div>
									<div class="col-lg-6">
			                            <div class="o-form__field">
			                              <label class="o-form__label">Direcci�n</label>
			                              <div class="row">
			                                <div class="col-6">
											 <html:text property="datos(direccion)"
															styleClass="o-form__input datos-calle-input" size="20"
															styleId="datos.calle1" maxlength="25" onkeypress="return validar(event)">
											</html:text><span class="o-form__line"></span>
			                                </div>
			                                <div class="col-3">
											 <html:text property="datos(direccionNumero)"
															styleClass="o-form__input datos-direccion-input" size="5"
															styleId="datos.nro1" maxlength="5"
															onkeypress="return isNumberKey(event)">
														</html:text><span class="o-form__line"></span>
			                                </div>
			                                <div class="col-3">
											   <html:text property="datos(direccionNroDepto)"
															styleClass="o-form__input datos-depto-input optional" size="10"
															styleId="datos.depto1" maxlength="5"
															onkeypress="return isNumberKey(event)">
												</html:text><span class="o-form__line"></span>
			                                </div>
			                              </div><span class="o-form__message" id="direction--recruiting"></span>
			                            </div>
			                          </div>
								</div>
								<!-- nuevo input direccion -->
								<!-- fin div row -->
								<!-- FILA 3 -->
									<div class="row u-hidden_desktop">
									  <div class="col-12">
										<!--<div class="c-assistance u-mb30">
										  <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
										  <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
										</div>-->
									  </div>
									</div>
<!-- Logic Viaje -->							
								<div class="row u-mt40">	
									<!--INICIO VIAJE-->
									<logic:notEqual name="datosPlan" property="maxDiaIniVig" value="0">
									
											<!-- INICIO CONTRATACION TERCEROS -->
												<div id="paso2">
													<logic:equal value="false" name="datosCotizacion"
														property="clienteEsAsegurado">
														<!-- Titulo -->
														<h2 class="o-title o-title--subtitle u-mb10">Datos del asegurado</h2>
														<p class="o text"></p>
														<form class="o-form o-form--standard o-form--linear o-form--small" id="form_recruiting_step2" action="">
														<div class="row u-mt40">
														
															<!-- PRIMERA FILA -->
															
																<!-- TELEFONO1 -->
																<div class="col-lg-6">
																<div class="o-form__field">
																	<label class="o-form__label">Tel�fono 1</label>
																	<div class="row">
																		<div class="col-4">
																		<div class="o-form__field o-form__field--select">
																			<html:select property="datos(dueTipoTelefono1)"
																				styleClass="o-form__select" styleId="dueTipoTelefono1">
																				<html:optionsCollection name="tipoTelefono"
																					label="descripcion" value="valor" />
																			</html:select><span class="o-form__line"></span>
																			</div>
																		</div>
																		<div class="col-3">
																		<div class="o-form__field o-form__field--select">
																			<html:select property="datos(dueCodigoTelefono1)"
																				styleClass="o-form__select" styleId="dueCodigoTelefono1">
																				<html:optionsCollection name="codigoArea"
																					label="descripcion" value="valor" />
																			</html:select><span class="o-form__line"></span>
																			</div>
																		</div>
																		<div class="col-5">
																		<div class="o-form__field o-form__field--select">
																			<html:text property="datos(dueNumeroTelefono1)"
																				styleClass="o-form__input datos-telefono3-input"
																				maxlength="8" styleId="dueNumeroTelefono1"
																				onkeypress="return isNumberKey(event)"></html:text>
																				<span class="o-form__line"></span>
																				</div>
																		</div>
																	</div><span class="o-form__message" id="phone--recruiting"></span>
																	<!-- fin form-group -->
																</div>
																</div>
																<!-- fin de TELEFONO -->
																<!-- TELEFONO2 -->
																<div class="col-lg-6">
																	<div class="o-form__field">
																	<label class="o-form__label">Tel�fono 2</label>
																		<div class="row">
																		<div class="col-4">
																		<div class="o-form__field o-form__field--select">
																			<html:select property="datos(dueTipoTelefono2)"
																				styleClass="o-form__select optional" styleId="dueTipoTelefono2">
																				<html:optionsCollection name="tipoTelefono"
																					label="descripcion" value="valor" />
																			</html:select><span class="o-form__line"></span>
																			</div>
																		</div>
																		<div class="col-3">
																		<div class="o-form__field o-form__field--select">
																			<html:select property="datos(dueCodigoTelefono2)"
																				styleClass="o-form__select optional" styleId="dueCodigoTelefono2">
																				<html:optionsCollection name="codigoArea"
																					label="descripcion" value="valor" />
																			</html:select><span class="o-form__line"></span>
																			</div>
																		</div>
																		<div class="col-5">
																		<div class="o-form__field o-form__field--select">
																			<html:text property="datos(dueNumeroTelefono2)"
																				styleClass="o-form__input datos-telefono-input optional"
																				maxlength="8" styleId="dueNumeroTelefono2"
																				onkeypress="return isNumberKey(event)"></html:text>
																				<span class="o-form__line"></span>
																				</div>
																		</div>
																		</div><span class="o-form__message" id="phone--recruiting"></span>
																		</div>
																	<!-- fin form-group -->
																</div>
																<!-- fin de TELEFONO -->
															</div>
															<!-- fin de row -->
															<!-- SEGUNDA FILA -->
															<div class="row">
																<div class="col-lg-6">
																	<div class="o-form__field o-form__field--select">
																		<label class="o-form__label">Regi�n</label>
																		<html:select property="datos(dueRegion)"
																			styleClass="o-form__select" styleId="dueRegion">
																			<html:option value="">Seleccione regi�n</html:option>
																			<html:options collection="regiones" property="id"
																				labelProperty="descripcion" />
																		</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="o-form__field o-form__field--select">
																		<label class="o-form__label">Comuna</label>
																		<html:select property="datos(dueComuna)"
																			styleClass="o-form__select" styleId="dueComuna">
																			<html:option value="">Seleccione ciudad</html:option>
																		</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
																	</div>
																</div>
															</div>
																<div class="row">
																	<div class="col-lg-6">
																		<div class="o-form__field o-form__field--select">
																		<label class="o-form__label">Ciudad</label>
																		<html:select property="datos(dueCiudad)"
																			styleClass="o-form__select" styleId="dueCiudad">
																			<html:option value="">Seleccione comuna</html:option>
																		</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
																		</div>
																	</div>
																	<!-- sadsdsasd	 -->
																	
																	
																	<div class="col-lg-6">
											                            <div class="o-form__field">
											                              <label class="o-form__label">Direcci�n</label>
											                              <div class="row">
											                                <div class="col-6">
																			 <html:text property="datos(dueCalle)"
																							styleClass="o-form__input datos-calle-input" size="20"
																							styleId="dueCalle" maxlength="25" onkeypress="return validar(event)">
																			</html:text><span class="o-form__line"></span>
											                                </div>
											                                <div class="col-3">
																			 <html:text property="datos(dueNumeroCalle)"
																							styleClass="o-form__input datos-direccion-input" size="5"
																							styleId="dueNumeroCalle" maxlength="5"
																							onkeypress="return isNumberKey(event)">
																						</html:text><span class="o-form__line"></span>
											                                </div>
											                                <div class="col-3">
																			   <html:text property="datos(dueNumeroDepto)"
																							styleClass="o-form__input datos-depto-input optional" size="10"
																							styleId="dueNumeroDepto" maxlength="5"
																							onkeypress="return isNumberKey(event)">
																				</html:text><span class="o-form__line"></span>
											                                </div>
											                              </div><span class="o-form__message" id="direction--recruiting"></span>
											                            </div>
											                          </div>
																	
																	<!-- fin sdsadsda -->
																</div>
															<!-- TERCERA FILA -->
														<!-- fin col-lg-12 -->
														
														<div class="col-lg-12 col-xs-12">
															<div class="alert alert-warning" id="incomplete-paso1"
																style="display: none">
																<p align="center" id="incomplete-text-paso1"></p>
															</div>
														</div>
													</form>	
													</logic:equal>
												</div>
											<!-- FIN CONTRATACION TERCEROS -->
												
												
										
									<div class="col-xs-12 col-sm-8 col-lg-4" >
											<div id="datos-del-contratante">
												<div id="datos-del-contratante-titulo">
													<h2 class="o-title o-title--subtitle u-mb10">Datos del viaje</h2>
												<p class="o text">Fecha de inicio de viaje.</p>
													
												</div>
												<div id="casilla-datos-cotizador">
													<div id="bloque-casilla">
														<div id="bloque-dato-usuario" style="padding-top: 4px;">
															<div id="bloque-dato-usuario"
																style="padding-top: 2.5px;">
																<p>
																	<input name="datos(fechaInicio)"
																		id="inputIniVig" class="form-control"
																		size="12" onBlur="autocompletarfechaVida(this);"
																		onFocus="autocompletarfechaVida(this);"
																		maxlength="10"></input>
																</p>
															</div>
															<div id="error.inputIniVig"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										
										
									</logic:notEqual>
									<!--FIN VIAJE-->
									
									
								</div>
<!-- Fin Logic Viaje -->		
<!-- test listado -->
		
								
<!-- fin test listado -->									
								</form>
                    			</div>
								<div class="col-lg-4">
								  <section class="c-summary" id="summary_sure">
									<h2 class="c-summary__title">Resumen de tu seguro</h2>
									<div class="c-summary__company"></div>
									<h3 class="c-summary__subtitle"><bean:write name="datosPlan" property="nombrePlan" /></h3>
									<div class="row">
									  <div class="col-12">
										<div class="c-summary__cost">
										  <div class="c-summary__label">Monto Total:</div>
										  <div class="c-summary__price"><sup>$</sup><bean:write name="datosPlan" property="primaMensualPesos"
								format=" ##,##0" locale="currentLocale" /><sub>/Mes*</sub></div>
										</div>
										<p class="c-summary__legal"></p>
									  </div>			
									</div>
								 <!--<div class="c-assistance u-mb30 u-hidden_mobile">
									  <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
									  <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
									</div>-->
								  </section>
								</div>
								</div>
								</div>
							</section>
								<!-- fin row -->
								<!-- FILA 4 -->
								
									
									
                                    <!--INICIO CAJA BENEFICIARIO-->
                                    <section class="o-box o-box--recruiting u-mtb50">
									
									<logic:present name="restricciones">
										<logic:equal property="requiereBeneficiarios" name="restricciones" value="true">
										<div class="u-pad20">
      										<div class="row o-form o-form--standard o-form--linear o-form--small">
											<div class="col-lg-8 u-mobile_second">
											<logic:equal name="idSubcategoria" value="221">
												<h2 class="o-title o-title--subtitle u-mb10">Datos de asegurados</h2>
												<p class="o text"></p>
											</logic:equal>
											<logic:notEqual name="idSubcategoria" value="221">
												<h2 class="o-title o-title--subtitle u-mb10">Datos del beneficiario</h2>
												<p class="o text"></p>
											</logic:notEqual>
											
											<form class="o-form o-form--standard o-form--linear o-form--small" id="formularioBeneficiarios" action=""> 
												<div class="row u-mt40">
													<div class="col-lg-6">
														<div class="o-form__field">
															<label class="o-form__label">RUT</label>
															<input type="text" maxlength="10" class="o-form__input optional beneficiario datos-rut-benef" 
															id="inputRutBenef"
															onkeypress="return isRutKey(event)"
															onkeyup="autocompletarrut(this)"  placeholder="12345678-9" ></input>
															</div>		
													</div>

													<div class="col-lg-6">
														<div class="o-form__field">
															<label class="o-form__label">Nombres</label>
															<input type="text" id="inputNombreBenef" placeholder="Nombre" class="o-form__input  optional beneficiario" maxlength="25" 
															size="16" onkeypress="return validar(event)"/>
														</div>
													</div>
								
												</div>
												<div class="row">
													<div class="col-lg-6">
														<div class="o-form__field">
															<label class="o-form__label">Apellido paterno</label>
															<input type="text" id="inputApePaternoBenef" placeholder="Apellido Paterno" 
															class="o-form__input optional beneficiario" maxlength="25" size="16" value="" onkeypress="return validar(event)" />
														</div>
													</div>
													<div class="col-lg-6">
														<div class="o-form__field">
															<label class="o-form__label">Apellido materno</label>
															<input type="text" id="inputApeMaternoBenef" placeholder="Apellido Materno" 
															class="o-form__input optional beneficiario" maxlength="25" size="16" value="" onkeypress="return validar(event)"/>
														</div>
													</div>
								
												</div>
												</form>
												</div>
												<div class="col-lg-8 u-mobile_second">
												<form class="o-form o-form--standard o-form--linear o-form--small" id="formularioBeneficiarios2" action=""> 
												<div class="row">
													<div class="col-lg-6">
														<div class="o-form__field">
															<label class="o-form__label">Fecha de Nacimiento</label>
															<div class="row">
										                          <div class="col-4">
										                          <div class="o-form__field o-form__field--select">
										                            <select name='idFechaDia' id='idFechaDia' class='o-form__select optional' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'inputFechaNacBenef')">
																	</select>  
																	<span class="o-form__line"></span>
																	</div>
										                          </div>
										                          <div class="col-4">
										                          <div class="o-form__field o-form__field--select">
																	<select name='idFechaMes' id='idFechaMes' class='o-form__select optional' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'inputFechaNacBenef')">
																	</select>
																	<span class="o-form__line"></span>
																	</div>
										                          </div>
										                          <div class="col-4">
										                          <div class="o-form__field o-form__field--select">
																	<select name='idFechaAnyo' id='idFechaAnyo' class='o-form__select optional' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'inputFechaNacBenef')">
																	 </select>
																	<span class="o-form__line"></span>
																	</div>
										                          </div>
										                        </div><span class="o-form__message" id="year_of_birth"></span>
															<input id="inputFechaNacBenef" type="hidden">
														</div>
													</div>
													<div class="col-lg-6">
														<div class="o-form__field o-form__field--select">
															<label class="o-form__label">Parentesco</label>
															<select class="o-form__select optional beneficiario" id="inputParentescoBenef">
																<option value="" selected="selected">Parentesco</option>
																<option value="E">�l Mismo</option>
																<option value="C">C�nyuge</option>
																<option value="H">Hijo</option>
																<option value="P">Padre</option>
																<option value="M">Madre</option>
																<option value="O">Otro</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-6">
														<div class="o-form__field o-form__field--select">
															<label class="o-form__label">Genero</label>
															<select class="o-form__select optional beneficiario" id="inputSexoBenef" >
																<option value="" selected="selected">Genero</option>
																<option value="M" >Masculino</option>
																<option value="F" >Femenino</option>
															<select>
														</div>
													</div>
													<logic:notEqual name="idSubcategoria" value="221">
														<div class="col-lg-6">
															<div class="form-group">
																<label class="o-form__label">% Distribuici�n</label>
																<input type="text" id='inputPorcentajeBenef' placeholder="% Distribuici�n" 
																class="o-form__input optional beneficiario" maxlength="3" size="16" value="" onkeypress="return isNumberKey(event)" />
															</div>
														</div>
													</logic:notEqual>
												</div>
												</form>
												
												<div class="row u-mt40">
													<div class="col-lg-3">
														<div class="o-form__field is-last">
														  <button class="o-btn o-btn--primary o-btn o-btn--icon" id="add_row">Agregar</button>
														</div>
													  </div>
													<div class="col-lg-3">
														<div class="o-form__field is-last">
														<a id="del_row" class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon">Borrar</a>
														</div>
													 </div>
													  
												</div>
												<div class="row">
															<logic:equal name="idSubcategoria" value="221">
															<div class="col-lg-12">
																<h2 class="o-title o-title--subtitle u-mb10">Asegurados Adicionales</h2>
															</div>
														</logic:equal>
														<logic:notEqual name="idSubcategoria" value="221">
															<div class="col-lg-12">
															<h2 class="o-title o-title--subtitle u-mb10">Beneficiarios</h2>
															</div>
														</logic:notEqual>
												</div>
												<div class="row">
													<div class="col-md-12 column table-responsive">
													<table class="table table-bordered table-hover" id="tab_logic">
													</table>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 col-sm-4">
														<div class="alert alert-warning" id="incomplete-paso1" style="display: none">
															<p align="center" id="incomplete-text-paso1"></p>
														</div>
													</div>
												</div>
												<div class="clearfix"></div>
												
												
												</div>
												
												</div>
												
												
												
												
											
									
										</logic:equal>
									</logic:present>
									<!--FIN CAJA BENEFICIARIO-->
								
								<!-- fin row -->
							
							<!-- fin col-lg-12 -->
							<div class="row">
							<logic:notPresent name="usuario">
						<div class="col-lg-8 o-actions o-actions--flex">
                          <div class="col-lg-3"> 
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true"><i class="mx-arrow-left"></i> Volver </a></div>
                          </div>
                          <div class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step2" type="button" onclick="showModal()">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
                        </div>
						</logic:notPresent>
							<div class="col-lg-8 u-mobile_second">
							<div class="col-lg-12 col-xs-12">
								<div class="alert alert-warning" id="incomplete-paso1" style="display: none">
									<p align="center" id="incomplete-text-paso1"></p>
								</div>
							</div>
					<logic:equal value="true" name="datosCotizacion" property="clienteEsAsegurado">
						<!--div class="top15">
							<div class="hidden-xs">
								<div class="col-md-3">
										<a href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true" type="button"
										
										class="btn btn-primary">Volver</a>
								</div>
							</div>
							<div class="col-md-3 col-md-offset-6">
								<button type="button"
									class="btn btn-primary btn-block pull-right"
									onclick="javascript:sinRegistro();">CONTINUAR</button>
							</div>
							<div class="hidden-lg">
								<br></br>
							</div>
							<div class="col-md-3 hidden-lg">
								<br> <br>
								<a href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true" type="button"
								
								class="btn btn-primary center-block">Volver</a>
							</div>
						</div-->
					</logic:equal>
							<!-- fin row -->
					
						</div>
						</div>
														
							
							
						<!-- fin paso1 -->
						
						<div class="row">
							<div class="col-lg-12 u-mobile_second">
						<div class="col-lg-12">
				<logic:notPresent name="usuario">					
				<!-- BOTONES COLAPSABLES -->
				
					<div class="form-group">
					
						<div class="row">
						
						  <div class="panel-group" id="accordion-botones" role="tablist" aria-multiselectable="true">
							<!--<div class="panel panel-default col-md-4" style="margin-top:0">
							  <div class="panel-heading" role="tab" id="1">
								<div class="panel-title2"> <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
								data-parent="#accordion-botones" aria-expanded="true" href="#collapsebot1"> 
								COMPRA SIN REGISTRARTE </a> </div>
							  </div>
							  <div id="collapsebot1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="1">
								<div class="panel-body">
								  <p class="text-center">Puedes realizar tus compras sin necesidad de estar registrado.</p>
								  <a class="btn btn-primary btn-block" href="javascript:sinRegistro();" role="button">CONTINUAR COMPRA</a> </div>
							  </div>
							</div>-->
							<!--<div class="top15 hidden-lg"></div>-->
							<div class="panel">
							  <div class="panel-heading" role="tab" id="2">
								<!--<div class="panel-title2"> <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
								data-parent="#accordion-botones" aria-expanded="false" href="#collapsebot2"> 
								USUARIO REGISTRADO </a> </div>-->
								<input type="hidden" name="username" id="username" value="" /> 
								<input type="hidden" name="password" id="password" value="" /> 
								<input type="hidden" name="rutpaso2" id="rutpaso2" value="" /> 
								<input type="hidden" name="dvpaso2" id="dvpaso2" value="" />
							  </div>
							  <div id="collapsebot2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="2">
								<div class="panel-body">
								<!-- REMODAL NUEVO -->
								
								<div class="remodal c-modal c-modal--modal-options" data-remodal-id="continueModal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">   
        <button class="remodal-close" data-remodal-action="close" aria-label="Close">  </button>
        <div class="c-modal__wrap">
          <div class="c-modal__modalHead">
            <div class="c-modal__modalHead-inner"> 
              <h2>Selecciona como deseas continuar tu compra</h2>
            </div>
          </div>
          <div class="c-modal__modalBody">
            <div class="row">
              <div class="col-md-12"> 
                <div class="item guest">
                  <h3>Continuar como invitado</h3>
                  <button class="o-btn o-btn--primary o-btn--medium" id="enter" type="button" onclick="sinRegistro()">Comprar</button>
                  <p>Podr�s realizar tus compras sin estar registrado y crear tu contrase�a despu�s. </p>
                </div>
              </div>
              <!--<div class="col-md-6"> 
                <div class="item login">
                  <h3>O como usuario registrado</h3>
                  <form class="o-form--loginForm" id="login">
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="o-form__field">
                          <label class="o-form__label">RUT</label>
                          <input class="o-form__input" id="rutpaso2completo" maxlength="10" required size="30" type="text"><span class="o-form__message"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="o-form__field">
                          <label class="o-form__label">Contrase�a</label>
                          <html:password property="datos(clavepaso2)" styleClass="o-form__input" maxlength="6" styleId="clavepaso2"/><span class="o-form__message"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="o-form__field">
                          <button class="o-btn o-btn--primary o-btn--medium" type="button" id="submitPaso2Vehiculo" name="submitPaso2Vehiculo" 
								  onclick="javascript:conRegistro();">Ingresar y continuar  </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>-->
            </div>
          </div>
        </div>
      </div>
								
								<!-- FIN REMODAL NUEVO -->
								  <!--<p>
									<input class="form-control" id="rutpaso2completo" maxlength="10" required size="30" type="text">
									<br>
									<html:password property="datos(clavepaso2)" styleClass="form-control" maxlength="6" styleId="clavepaso2"/>
									<br>
								  </p>
								  <button type="button" class="btn btn-primary btn-block" id="submitPaso2Vehiculo" name="submitPaso2Vehiculo" 
								  onclick="javascript:conRegistro();">INGRESAR</button>-->
								  
								  <a class="small" href="javascript:olvidasteClave();">�Olvidaste tu clave?</a> 
								  <script type="text/javascript">
									<logic:present name="datosCotizacion">
										$("#rutpaso2").val("<bean:write name='datosCotizacion' property='rutCliente'/>");	
										$("#dvpaso2").val("<bean:write name='datosCotizacion' property='dv'/>");
										$("#rutpaso2completo").val($("#rutpaso2").val()+'-'+$("#dvpaso2").val());
										$("#rutpaso2completo").attr("readonly", "true");
									</logic:present>
								  </script>
								</div>
							  </div>
							</div>
							<div class="top15 hidden-lg"></div>
							<!--<div class="panel panel-default col-md-4" style="margin-top:0">
							  <div class="panel-heading" role="tab" id="3">
								<div class="panel-title2"> 
									<a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
									data-parent="#accordion-botones" aria-expanded="false" href="#collapsebot3"> �ERES NUEVO? </a> 
								</div>
							  </div>
							  <div id="collapsebot3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="3">
								<div class="panel-body">
								  <p class="text-center">
									Reg�strate aqu� y podr�s revisar tus p�lizas contratadas, cotizaciones y cotizar de manera m�s r�pida
								  </p>
								  <a class="btn btn-primary btn-block" onclick="javascript:registroRemoto();" role="button">AQU�</a> </div>
							  </div>
							</div>-->
						  </div>
						</div>
					</div>
					<!-- 
					<div class="col-md-3 hidden-xs">
						<a type="button" class="btn btn-primary"
							href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
					</div>
					<div class="col-md-12 hidden-lg text-center">
						<a type="button" class="btn btn-primary top15"
							href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
					</div> -->
							<!--  <div class="col-md-3 hidden-xs">
								<div class="o-form__field is-last">
									<a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
								</div>
							</div>
							<div class="col-md-12 hidden-lg text-center">
								<div class="o-form__field is-last">
									<a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
								</div>
							</div>-->
					
					
				<!-- FIN BOTONES COLAPSABLES --> 
				</logic:notPresent>

				<logic:present name="usuario">
				<div class="col-lg-8 o-actions o-actions--flex">
                          <div class="col-lg-3"> 
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true"><i class="mx-arrow-left"></i> Volver </a></div>
                          </div>
                          <div class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step2" type="button" onclick="conRegistro()">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
                        </div>
					<!--<div class="row top15">
						<div class="col-md-12">
							<div class="col-md-4 hidden-xs pull-right">
								<button type="button" class="btn btn-primary btn-block"
									onclick="javascript:sinRegistro();">CONTINUAR</button>
							</div>
							<div class="col-md-4 hidden-lg bot15">
								<button type="button" class="btn btn-primary btn-block"
									onclick="javascript:sinRegistro();">CONTINUAR</button>
							</div>
							
							<div class="col-md-3 hidden-xs">
								<a type="button" class="btn btn-primary"
									href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
							</div>
							<div class="col-md-12 hidden-lg text-center">
								<a type="button" class="btn btn-primary top15"
									href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
							</div> 
							
							<div class="col-md-3 hidden-xs">
								<div class="o-form__field is-last">
									<a type="button" class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
								</div>
							</div>
							<div class="col-md-12 hidden-lg text-center">
								<div class="o-form__field is-last">
									<a type="button" class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true">Volver</a>
								</div>
							</div>
							
						</div>
					</div>-->
				</logic:present>
				
				
				</div>
			</div>
			</div>	
			
				
				</div>
				</section>
				
				
					</div>
					<!-- fin contenido-desplegado-cotizador -->
				</div>
				<!-- div col-xx-x -->
			</html:form>
		</div>
		<!-- fin de row -->
	
	<!-- fin de container -->
	<!-- INICIO ModalRegistroRemoto -->
	<div class="modal fade" id="modalRegistrate" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Registrate</h4>
				</div>
				<div class="modal-body">
					<iframe class="" frameborder="0" id="cargaRegistrate" width="100%" height="500px">
					</iframe>	
				</div>
			</div>
		</div>
	</div> 
	
	
	
		</section>
		</div>		
    </main>
	<!-- FIN ModalRegistroRemoto -->	
	<iframe name="ifrEnvio" id="ifrEnvio"
		src="/cotizador/cotizacion/blank.jsp" width="0" height="0"
		frameborder="0"> </iframe>
	<!-- FIN CONTENIDOS -->
	<%@ include file="./includes/cotizador-footer.jsp"%>
</body>


</html>
<!-- fin de html -->