<!DOCTYPE html>
<%@page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="cl.cencosud.acv.common.Producto"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html lang="es">
<head>



<%@ include file="./includes/cotizador-head.jsp"%>


<style type="text/css">
.ui-datepicker {
	color: #000000 width :     216px;
	height: auto;
	margin: 5px auto 0;
	font: 9pt Arial, sans-serif;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
}

.ui-datepicker-month {
	color: #000000
}

.ui-datepicker-year {
	color: #000000
}
</style>


<script type="text/javascript">

$(document).ready(function(){
	cargaDia('idFechaDia');
	cargaMes('idFechaMes');
	cargaAnyo('idFechaAnyo');
});
</script>

<script type="text/javascript">
			function obtenerDescuento() {
				personalInfo();
				document.getElementById('cotizarProductoForm').action="/cotizador/preferente-hogar.do";
				document.getElementById('cotizarProductoForm').method="post";
				document.getElementById('cotizarProductoForm').submit();
			}
			
			$(document).ready(function(){
				$("#paso3").hide();
				duenioVivienda('true');
				$("#paso2").hide();
				$('.datos-nombre-input').attr('placeholder','Nombre');
				$('.datos-paterno-input').attr('placeholder','Ingrese su apellido paterno');
				$('.datos-materno-input').attr('placeholder','Ingrese su apellido materno');
				$('.datos-rut-input').attr('placeholder','Ej: 1234567-9');
				$('.datos-rut-input-duenho').attr('placeholder','Ej: 1234567-9');
				$('.datos-email-input').attr('placeholder','ejemplo@correo.cl');
				$('.datos-telefono-input').attr('placeholder','tel�fono');
				$('.datos-calle-input').attr('placeholder','calle');
				$('.datos-direccion-input').attr('placeholder','n�');
				$('.datos-depto-input').attr('placeholder','n� depto/casa');
				regionToRoman('regionResidencia');
				
				<logic:present property="datos(codigoTelefono)" name="cotizarProductoHogar">
					var val_codigo_cel = "<bean:write name='cotizarProductoHogar' property='datos(codigoTelefono)'/>";
					$("select[name='datos(codigoTelefono)']").val(val_codigo_cel);
				</logic:present>
				
				<logic:present property="datos(contratanteEsDuenyo)" name="cotizarProductoHogar">
					var valorEsDuenyo = "<bean:write property="datos(contratanteEsDuenyo)" name="cotizarProductoHogar"/>";
					if(valorEsDuenyo == "false") {
						duenioVivienda('false');
					}
				</logic:present>
				
				$("select#regionResidencia").change(function() {
					$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
						$('select#comunaResidencia option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';

						for(var i = 0; i < data.length; i++) {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}

						$('select#comunaResidencia').html(options);
						$("select#ciudadResidencia").html('<option value="">Seleccione</option>');
					});
				});

				$("select#comunaResidencia").change(function() {
					$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
						$('select#ciudadResidencia option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';
						
						for(var i = 0; i < data.length; i++) {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
						
						$('select#ciudadResidencia').html(options);
					});
				});
			});
			
			function duenioVivienda(val) {
				if(val==='true') {
					$(".form-vivienda-duenio").hide();
				} else {
					$(".form-vivienda-duenio").show();
				}
			}	
									
			function submitHogar(){		
				validateInput("paso2");			
				if(validacionInput == 0) {
					bar(66.7,1);
					$('html, body').animate({scrollTop: 0}, 0);
					$('#waitModal').modal('show');
					$('#grupo_valorizacion_iframe').hide();
					personalInfo();				
					var rutCompleto = document.getElementById('datos.rutDuenyoCompleto').value.split('-');
					document.getElementById('datos.rutDuenyo').value = rutCompleto[0];
					if(rutCompleto.length > 1) {
						document.getElementById('datos.dvRutDuenyo').value = rutCompleto[1];
					}
					
					var caracter="1234567890";
					caracter+="QWERTYUIOPASDFGHJKLZXCVBNM";
					caracter+="qwertyuioplkjhgfdsazxcvbnm";
					var numero_caracteres=10;
						 
					var total=caracter.length;
					var clave="";
					for(a=0;a<numero_caracteres;a++){
						clave+=caracter.charAt(parseInt(total*Math.random(1)));
					}
					document.getElementById('claveVitrineo').value = clave;	
					document.getElementById('hiddCaptcha').value = "0";
					
					document.getElementById('cotizarProductoForm').action="/cotizador/cotizar-producto-hogar.do";
					document.getElementById('cotizarProductoForm').method="post";
					document.getElementById('cotizarProductoForm').submit();	
				}
			}
		</script>
<%
	String glosaDescuento = (String) request.getSession().getAttribute(
			"glosaDescuento");
	if (glosaDescuento != null) {
%>

	<%}%>
<script type="text/javascript">
$(document).ready(function(){
$('.datos-rut-input').blur(function(){
		var obtenerRut = document.getElementsByName('datos(rutCompleto)')[0].value;
		var cadena =obtenerRut.substr(0,1);
		if (cadena >=4 && cadena <= 9){
			if (obtenerRut != null){
				if (obtenerRut.length < 9){
					var rut = obtenerRut.substr(0,obtenerRut.length-1);
					var dv = obtenerRut.substr(obtenerRut.length-1,1);
					var completo = rut + '-' + dv;
				 	$('.datos-rut-input').val("");
					$('.datos-rut-input').val(completo);
				}
			}
		}else if(cadena >= 1 && cadena <=3){
			if (obtenerRut != null){
				if (obtenerRut.length < 10){
					var rut = obtenerRut.substr(0,obtenerRut.length-1);
					var dv = obtenerRut.substr(obtenerRut.length-1,1);
					var completo = rut + '-' + dv;
				 	$('.datos-rut-input').val("");
					$('.datos-rut-input').val(completo);
				}
			}
		}
});
});
</script>
</head>
<body onload="desplegarErrores()">
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="./includes/cotizador-header.jsp"%>

	<!--  <div class="container contenedor-sitio"> -->
		<%@ include file="./includes/cotizador-breadcrumb.jsp"%>
	<!-- INICIO  CONTENIDOS -->						 
	<main role="main">
      <div class="remodal-bg o-form o-form--standard o-form--linear o-form--small">	
		<section class="container">	
		
		<div class="row">
			<div class="col-12">
				<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			</div>					
			<!-- include file="./includes/cotizador-resumen.jsp" -->
			<html:form action="cotizar-producto-hogar.do" method="post"
				styleId="cotizarProductoForm" styleClass="o-form" target="iframe_tabla">
																			   
				<html:hidden property="datos(rut)" value="" styleId="datos.rut" />
				<html:hidden property="datos(dv)" value="" styleId="datos.dv" />
				<html:hidden property="datos(rutDuenyo)" value=""
																					
																					   
					styleId="datos.rutDuenyo" />
				<html:hidden property="datos(dvRutDuenyo)" value=""
					styleId="datos.dvRutDuenyo" />
				<html:hidden property="datos(diaFechaNacimiento)" value=""
					styleId="diasFecha" />
				<html:hidden property="datos(mesFechaNacimiento)" value=""
					styleId="mesFecha" />
				<html:hidden property="datos(anyoFechaNacimiento)" value=""
					styleId="anyosFecha" />
				<html:hidden property="datos(claveVitrineo)" value="1"
					styleId="claveVitrineo" />
				<html:hidden property="datos(hiddCaptcha)" value="1"
					styleId="hiddCaptcha" />
					
					
					
				<input type="hidden" name="idPlanValorizacion"
					id="idPlanValorizacion" value="" />

				<%
					String idPlan = (String) request.getAttribute("idPlan");
						idPlan = (idPlan != null ? idPlan : "-1");
				%>
				<html:hidden property="datos(idPlan)" value="<%=idPlan %>"
					styleId="datos(idPlan)" />

			<!--	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"> -->
				<!--	<%@ include file="./includes/cotizador-percent-bar.jsp"%> -->
													  
															   
	  
			<div id="contenido-desplegado-cotizador">
					<div id="globalExceptionDivPlanes" class="">
						</div>
	 
			<div id="paso1">
				<div class="col-12">	
              <section class="o-box o-box--quotation u-mt20 u-mb40">
                <h2 class="o-title o-title--subtitle">Queremos saber de t�</h2>
							<div class="row u-mb50">
								<div class="col-lg-4">
										<div class='o-form__field'>
											<label class="o-form__label">Nombres</label>
											<html:text property="datos(nombre)"
												styleClass="o-form__input datos-nombre-input" size="16"
												styleId="datos.nombre" maxlength="25"
												onkeypress="return validar(event)" />
												<span class="o-form__line"></span><span class="o-form__message"> </span>
							
										</div>
									</div>	
						  

									<div class='col-lg-4'>
										<div class="o-form__field">
											<label class="o-form__label">Apellido paterno</label>
											
											<html:text property="datos(apellidoPaterno)"
												styleClass="o-form__input datos-paterno-input"
												styleId="datos.apellidoPaterno" size="16" maxlength="25"
												onkeypress="return validar(event)" />
												<span class="o-form__line"></span><span class="o-form__message"> </span>
			
										</div>
									</div>
									<div class='col-lg-4'>
										<div class="o-form__field">
											<label class="o-form__label">Apellido materno</label>
											
											<html:text property="datos(apellidoMaterno)"
												styleClass="o-form__input datos-materno-input"
												styleId="datos.apellidoMaterno" size="16" maxlength="25"
												onkeypress="return validar(event)" />
												<span class="o-form__line"></span><span class="o-form__message"> </span>																		
										</div>
									</div>
								</div>
								<div class="row u-mb50">
									<div class='col-lg-4'>
										<div class='o-form__field'>
											 <label class="o-form__label">RUT</label>
											<logic:present name="usuario">
												<input type="text" class="o-form__input rut-completo" 
													placeholder="Ej: 1234567-9" disabled="disabled" id="datos.rutcompleto"
													value="<bean:write name='usuario' property='usuarioExterno.rut_cliente' />-<bean:write name='usuario' property='usuarioExterno.dv_cliente' />">
											</logic:present>
											<logic:notPresent name="usuario">
												<html:text styleClass="o-form__input datos-rut-input"
													property="datos(rutCompleto)" maxlength="10"
													styleId="datos.rutcompleto" onblur="obtenerDescuento();"
													onkeypress="return isRutKey(event)"
													/>
											</logic:notPresent>
										</div>
									</div>
									<!--INI  Fecha de nacimiento-->  
									<div class='col-lg-4'>
										<div class='o-form__field'>
											<label class="o-form__label">Fecha de Nacimiento</label>
											<div class="row">
											<div class="col-4">
											<div class="o-form__field o-form__field--select">
					                            <select fecha='fecha' name='idFechaDia' id='idFechaDia' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select> 
												<span class="o-form__line"></span>
												</div>
					                          </div>
					                          <div class="col-4">
					                          <div class="o-form__field o-form__field--select">
												<select name='idFechaMes' id='idFechaMes' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select>
												<span class="o-form__line"></span>
												</div>
					                          </div>
					                          <div class="col-4">
					                          <div class="o-form__field o-form__field--select">
												<select name='idFechaAnyo' id='idFechaAnyo' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select><span class="o-form__line"></span>
												</div>
					                          </div>
											</div><span class="o-form__message" id="year_of_birth"></span>
											
											
											<!-- Input hiden para guardar la fecha-->
											<logic:notPresent name="usuario">
												<input type="hidden" value="" id="fechaCompleta" fecha="fecha"/>
											</logic:notPresent>
											<logic:present name="usuario">
												<input type="hidden"  id="fechaCompleta" fecha="fecha" value="<bean:write name='usuario' property='usuarioExterno.fecha_nacimiento' format='dd-MM-yyyy' />"/>
												<script>
													$(document).ready(function(){
														cargarSelectFechaNacimiento('fechaCompleta', 'idFechaDia', 'idFechaMes', 'idFechaAnyo');
													});
												</script>
											</logic:present>
										</div>
									</div>
									<!--FIN  Fecha de nacimiento-->
									<div class='col-lg-4'>
										<div class='o-form__field o-form__field--select'>
											<label class="o-form__label">Estado Civil</label>
											<html:select property="datos(estadoCivil)"
												styleClass="o-form__select"
												styleId="datos.estadoCivil">
												<html:option value="">Seleccione estado civil</html:option>
												<html:options collection="estadosCiviles" property="id"
													labelProperty="descripcion" />
											</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
										</div>
									</div>
									</div>
									<div class="row u-mb50">
									<div class='col-lg-4'>
										<div class="o-form__field o-form__field--select">
											<label class="o-form__label">Genero</label>
											<html:select styleClass="o-form__select"
												property="datos(sexo)" styleId="datos.sexo">
												<html:option value="">Seleccione Genero</html:option>
												<html:option value="F">Femenino</html:option>
												<html:option value="M">Masculino</html:option>
											</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
										</div>
									</div>
									<div class='col-lg-4'>
										<div class="o-form__field">
											<label class="o-form__label">Email</label>
											<html:text property="datos(email)"
												styleClass="o-form__input datos-email-input" 
												styleId="datos.email" maxlength="50"
												onkeypress="return isCaracterEmail(event);"></html:text>
												<span class="o-form__line"></span><span class="o-form__message"></span>
										</div>
									</div>
									<div class='col-lg-4'>
										<div class="o-form__field">							   
											<label class="o-form__label">Tel�fono</label>
										<div class="row">
											<div class="col-4">
											<div class="o-form__field o-form__field--select">
												<html:select property="datos(tipoTelefono)"
													styleClass="o-form__select is-children" styleId="datos.tipoTelefono">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="tipoTelefono"
														label="descripcion" value="valor" />
												</html:select><span class="o-form__line"></span>
												</div>
											</div>
											<div class="col-3">
											<div class="o-form__field o-form__field--select">
												<html:select property="datos(codigoTelefono)"
													styleClass="o-form__select is-children" styleId="datos.codigoTelefono">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="codigoArea"
														label="descripcion" value="valor" />
												</html:select>
												<span class="o-form__line"></span><span class="o-form__message"></span>
												</div>			
											</div>
											<div class="col-5">
											<div class="o-form__field o-form__field--select">
												<html:text property="datos(numeroTelefono)"
													styleClass="o-form__input datos-telefono-input is-children"
													maxlength="9" styleId="datos.numeroTelefono"
													onkeypress="return isNumberKey(event)">
												</html:text><span class="o-form__line"></span><span class="o-form__message"></span>
												</div>
											</div>
										</div><span class="o-form__message" id="phone"></span>
									</div>
									
								</div>
								</div>
								<div class="row">
									
									
									
									<div class="row">
										<div class="col-lg-12 col-xs-12">
											<div class="alert alert-warning" id="incomplete-paso1"
												style="display: none">
												<p align="center" id="incomplete-text-paso1"></p>
											</div>
										</div>
									</div>
								</div>
								<div class="row o-actions o-actions--flex">
									                     <div class="col-lg-3 offset-lg-9 u-text-right">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="switchForm(true);">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									                       </div>
								</div>
								<div class="clearfix"></div>

					</section>
					</div>
				</div>      
         	 
   		   		<div id="paso2">
							<div class="col-lg-12">
								<div class="row">
									<main role="main">
										<div class="remodal-bg">
											<section class="container">
												<div class="row">
													<div class="col-12">
														<section class="o-box o-box--quotation u-mt20 u-mb40">
									
													<logic:equal name="subcategoriaDesc" value="Hogar Vacaciones">
														<h2 class="o-title o-title--subtitle">Due�o</h2>
													</logic:equal>
													<logic:notEqual name="subcategoriaDesc" value="Hogar Vacaciones">
														<h2 class="o-title o-title--subtitle">Due�o de la vivienda</h2>
													</logic:notEqual>
										<form class="o-form o-form--standard" id="form_quotation_step1" action="">
										<div class="row u-mb50">
											<div class="col-md-5">
												<div class="o-form__field o-form__field--select">
												<label class="o-form__label">Qui�n contrata el seguro<span class="o-help">�Es el due�o de la vivienda?<i class="o-help__icon" data-tooltip="Si tu respuesta es NO, recuerda tener a mano los datos del due�o de la vivienda.">
								                              <svg width="3px" height="15px" viewbox="0 0 3 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								                                <g id="exclamation" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								                                  <g id="Desktop" transform="translate(-433.000000, -745.000000)" fill="#FFFFFF">
								                                    <g id="Group-4" transform="translate(424.000000, 742.000000)">
								                                      <g id="tooltip">
								                                        <g id="Group-3">
								                                          <g id="Group-2" transform="translate(9.000000, 3.000000)">
								                                            <ellipse id="Oval-3" cx="1.5" cy="1.47058824" rx="1.5" ry="1.47058824"></ellipse>
								                                            <rect id="Rectangle-2" x="0" y="3.82352941" width="3" height="11.1456409" rx="1.5"></rect>
								                                          </g>
								                                        </g>
								                                      </g>
								                                    </g>
								                                  </g>
								                                </g>
								                              </svg></i></span></label>
												
													<html:select styleClass="o-form__select"
														property="datos(contratanteEsDuenyo)"
														onchange="javascript:duenioVivienda(this.value);">
														<option value="true" selected>Si</option>
														<option value="false">No</option>
													</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
											</div>
										</div>
														<%
															Producto[] productos = (Producto[]) request
																		.getAttribute("productos");
																String cantProductos = String.valueOf(productos.length);
														%>
														<bean:define id="cantProd" value="<%=cantProductos%>"
															scope="page" />

														<!--INICIO Hogar vacaciones-->
														<logic:equal name="subcategoriaDesc"
															value="Hogar Vacaciones">
															<div class="col-md-4">
																<div class="o-form__field o-form__field--select">
															<label class="o-form__label">Tramo d�as de viaje</label>
															<logic:iterate id="producto" name="productos">
																<%
																	String idProducto = ((Producto) producto)
																						.getIdProducto();
																%>
																<html:hidden property="datos(producto)"
																	styleId="producto" value="<%=idProducto%>" />
															</logic:iterate>

															<html:select property="datos(tramo)"
																styleClass="o-form__select" styleId="tramo">
																<option value="">Seleccione tramo</option>
																<option value="DE 1 A 8 DIAS">De 1 a 8 d�as</option>
																<option value="DE 9 A 15 DIAS">De 9 a 15 d�as</option>
																<option value="DE 16 A 22 DIAS">De 16 a 22 d�as</option>
																<option value="DE 23 A 30 DIAS">De 23 a 30 d�as</option>
																<option value="DE 31 A 45 DIAS">De 31 a 45 d�as</option>
																<option value="DE 46 A 60 DIAS">De 46 a 60 d�as</option>
															</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
															</div>
															</div>
														</logic:equal>

														<!--FIN Hogar vacaciones-->

														<logic:notEqual name="subcategoriaDesc"
															value="Hogar Vacaciones">

															<logic:empty name="planesCotizados">
																<logic:greaterThan value="1" name="cantProd"
																	scope="page">
																	<label class="o-form__label">Elige tu deducible</label>
																	<html:select property="datos(producto)"
																		styleClass="o-form__select" styleId="producto">
																		<option value="">seleccione</option>
																		<html:options collection="productos"
																			property="idProducto" labelProperty="nombreProducto" />
																	</html:select>
																</logic:greaterThan>
															</logic:empty>
															<logic:lessEqual value="1" name="cantProd" scope="page">
																<!--label>Tu Seguro:</label-->
																<logic:iterate id="producto" name="productos">
																	<%
																		String idProducto = ((Producto) producto)
																								.getIdProducto();
																	%>
																	<html:hidden property="datos(producto)"
																		styleId="producto" value="<%=idProducto%>" />
																	<!--label><bean:write name="producto" property="nombreProducto" /></label-->
																</logic:iterate>
															</logic:lessEqual>
														</logic:notEqual>
													
												
											
										
										</div>
										
									<div class="form-vivienda-duenio col-sm-12 datos">
										<div class="row u-mb50">
										<div class="col-lg-4">		
												<div class="o-form__field">
													<label class="o-form__label">Nombres</label>
													<html:text property="datos(nombreDuenyo)"
														styleClass="o-form__input datos-nombre-input" size="16"
														styleId="datos.nombreDuenyo" maxlength="25"
														onkeypress="return validar(event)" />
														<span class="o-form__line"></span><span class="o-form__message"></span>
												</div>
										</div>
										<div class="col-lg-4">		
												<div class="o-form__field">
													<label class="o-form__label">Apellido paterno</label>
													<html:text property="datos(apellidoPaternoDuenyo)"
														styleClass="o-form__input datos-paterno-input"
														styleId="datos.apellidoPaternoDuenyo" size="16"
														maxlength="25" onkeypress="return validar(event)" />
												</div>
											</div>
											<div class="col-lg-4">		
												<div class="o-form__field">
													<label class="o-form__label">Apellido materno</label>
													<html:text property="datos(apellidoMaternoDuenyo)"
														styleClass="o-form__input datos-materno-input"
														styleId="datos.apellidoMaternoDuenyo" size="16"
														maxlength="25" onkeypress="return validar(event)" />																											
												</div>
											</div>
										</div>
										<div class="row u-mb50">
											<div class="col-lg-4">
												<div class="o-form__field">
													<label class="o-form__label">RUT</label>
													<html:text styleClass="o-form__input datos-rut-input-duenho"
															property="datos(rutDuenyoCompleto)" maxlength="10"
															styleId="datos.rutDuenyoCompleto" onblur="obtenerDescuento();"
															onkeypress="return isRutKey(event)"
															onkeyup="autocompletarrut(this)" />
															<span class="o-form__line"></span><span class="o-form__message"></span>
												</div>
											</div>
										</div>
									</div>
									
								   
										<h2 class="o-title o-title--subtitle">Datos vivienda asegurada</h2>
									
										<div class="row u-mb50">
											<div class="col-lg-4">
													<div class="o-form__field o-form__field--select">
														<label class="o-form__label">Regi�n</label>
														<html:select property="datos(regionResidenciaDuenyo)"
															styleClass="o-form__select" styleId="regionResidencia">
															<html:option value="">Seleccione regi�n</html:option>
															<html:options collection="regiones" property="id"
																labelProperty="descripcion" />
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
											</div>
											<div class="col-lg-4">
													<div class="o-form__field o-form__field--select">
														<label class="o-form__label">Comuna</label>
														<html:select property="datos(comunaResidenciaDuenyo)"
															styleClass="o-form__select" styleId="comunaResidencia">
															<html:option value="">Seleccione comuna</html:option>
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
											</div>
											<div class="col-lg-4">
													<div class="o-form__field o-form__field--select">
													<label class="o-form__label">Ciudad</label>
	  
													<html:select property="datos(ciudadResidenciaDuenyo)"
														styleClass="o-form__select" styleId="ciudadResidencia">
														<html:option value="">Seleccione Ciudad</html:option>
													</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
		 					
												</div>
											</div>
											
										</div>
	  
										<div class="row u-mb50">
		   
											<div class="col-lg-4">
												<div class="o-form__field o-form__field--select">
													<label class="o-form__label">Antig�edad vivienda</label>
													<html:select property="datos(antiguedadVivienda)"
														styleClass="o-form__select" styleId="antiguedadVivienda">
														<html:option value="">Seleccione Antig�edad</html:option>
														<html:optionsCollection name="antiguedadVivienda"
															label="descripcion" value="valor" />
													</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="o-form__field">
													<label class="o-form__label">Direcci�n</label>
													<logic:present name="usuario">
													<input type="text" class="o-form__input datos-calle-input" name="datos(direccion)"
													placeholder="calle" id="direccion"
													value="<bean:write name='usuario' property='usuarioExterno.calle' />">
													</logic:present>
													<logic:notPresent name="usuario">
													<html:text property="datos(direccion)"
														styleClass="o-form__input datos-calle-input" maxlength="25"
														styleId="datos.direccion" onkeypress="return validar(event)" />
												    </logic:notPresent>
													<span class="o-form__line"></span><span class="o-form__message"></span>
												</div>
											</div>
											<div class='col-lg-4'>	   
												<div class='o-form__field'>
													<label class="o-form__label">N�mero</label>
													<logic:present name="usuario">
													<input type="text" class="o-form__input datos-direccion-input" name="datos(numeroDireccion)"
													placeholder="n�" id="direccionNumero"
													value="<bean:write name='usuario' property='usuarioExterno.numero' />">
													</logic:present>
													<logic:notPresent name="usuario">
													<html:text property="datos(numeroDireccion)"
														styleClass="o-form__input datos-direccion-input"
														maxlength="5" size="5" styleId="datos.direccionNumero"
														onkeypress="return isNumberKey(event)" />
													</logic:notPresent>
													<span class="o-form__line"></span><span class="o-form__message"></span>
												</div>
											</div>
											
										</div>
										
										<div class="row u-mb50">
											
											<div class='col-lg-4'>	   
												<div class='o-form__field'>
													<label class="o-form__label">N�mero depto</label>		   
													<logic:present name="usuario">
													<input type="text" class="o-form__input datos-depto-input optional" name="datos(numeroDepto)"
													placeholder="n� depto/casa" id="direccionNroDepto"
													value="<bean:write name='usuario' property='usuarioExterno.numero_departamento' />">
													</logic:present>
													<logic:notPresent name="usuario">
													<html:text property="datos(numeroDepto)"
														styleClass="o-form__input datos-depto-input optional"
														maxlength="5" size="10" styleId="datos.direccionNroDepto"
														onkeypress="return isNumberKey(event)" />
													</logic:notPresent>
												</div>
											</div>
										</div>
									
			   
									<logic:notEqual name="vaCaptcha" value="0">
										<%@ include file="./includes/cotizador-captcha.jsp"%>
									</logic:notEqual>
									
											</form>
										</section>
												</div>
											</div>
										</section>
											
											
										</div>
									</main>
								</div>
								
								<div class="row">
									<div class="col-lg-12 col-xs-12">
										<div class="alert alert-warning" id="incomplete-paso2"
											style="display: none">
											<p align="center" id="incomplete-text-paso2"></p>
										</div>
									</div>
								</div>
								<div class="row top15" id="botonCotizarDiv">
									
																	 
									<logic:equal name="vaCaptcha" value="0">
									<div class="col-md-3 col-md-offset-6">									 
										<button type="button"
											class="btn btn-primary btn-block pull-right"
											onclick="javascript:submitHogar();">COTIZAR</button> 
									</div>					
									</logic:equal>
																	  
									<div class="col-md-3 top15 hidden-lg">
										<br> <br>
										<button type="button" class="btn btn-primary center-block"
											onclick="switchForm(false);">Volver</button>																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						
									</div>
								</div>
								
								
								
							</div>									  
						</div>
					</div>					  
						 
			</html:form>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													 
				<div id="paso3">
    				<main role="main">
							<div class="remodal-bg">
								<section class="container">
								<div class="row">
									<div class="col-12">
							              <section class="o-box o-box--quotation u-mt20 u-mb40">
							               <div id="grupo_valorizacion_iframe">
												<logic:notEmpty name="planesCotizados">
												
													<%@ include file="./includes/cotizador-valorizacion.jsp"%>
												</logic:notEmpty>
											</div>
							              </section>
							            </div>
								</div>
								</section>
							</div>
						</main>					
			  	</div>
				<!-- Modal -->
				<div class="modal fade" id="waitModal" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body">
								<p align="center">Por favor espere...</p>
							</div>
						</div>

					</div>
				</div>
				</section>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													
		</div>																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																											
	</main>
	<iframe src="/cotizador/cotizacion/blank.jsp" width="100%" height="200"
		name="iframe_tabla" style="display: none;"> </iframe>
	<!-- FIN CONTENIDOS -->
	<%@ include file="./includes/cotizador-footer.jsp"%>
</body>

</html>