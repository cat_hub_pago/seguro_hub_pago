<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta name="generator" content="Cencosud" />
		<title>Cotizador</title>
		<link href="css/formulario-r.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

		<script src="js/cotizacion.js"></script>
		<script src="js/jquery-1.4.2.js"></script>
		<script src="js/jquery.scrollTo.js"></script>
		<script src="js/jquery.hoverIntent.minified.js" charset="utf-8"></script>
		<script src="js/jquery.colorbox.js"></script>
		
		<!--[if lt IE 7]>
<!--
			<script type="text/javascript" src="js/IE7.js" ></script>
-->
		<![endif]-->
		
	</head>

	<body>
	
	<div id="estimado_senor_contenedor">
   <div id="contenido_senor">
   <div id="contenido_senor_texto">
   <p>Estimado <logic:notEmpty property="contratante.nombre" name="datosCotizacion"> <bean:write property="contratante.nombre" name="datosCotizacion"/> </logic:notEmpty>, Gracias por cotizar con nosotros. Tu cotizaci�n ha sido enviada a <span> <logic:notEmpty name="datosCotizacion" property="email"> <bean:write name="datosCotizacion" property="email"/> </logic:notEmpty> </span></p>

   <p>Tambi�n puedes retomar tu cotizaci�n por hasta 24 horas desde la secci�n <span>"Mis Seguros Online"</span> en la p�gina de inicio de nuestro Sitio Web.</p>
   </div>
   
   <div id="logos_senor"><img src="images/img_light_box/logos.jpg" alt="" width="626" height="49" border="0" /></div>
   <div id="botones_senor"><a href="/vseg-paris"><img src="images/img_light_box/casa_inicio.jpg" alt="" width="122" height="47" border="0" /></a><a href="#"><img src="images/img_light_box/descargar-pdf.jpg"onmouseover="this.src = 'images/img_light_box/descargar-pdf-hover.jpg'" border="0" onmouseout="this.src = 'images/img_light_box/descargar-pdf.jpg'" border="0" alt="" width="223" height="47" border="0" /></a><a href="#"><img src="images/img_light_box/cantratacion.jpg"onmouseover="this.src = 'images/img_light_box/cantratacion-hover.jpg'" border="0" onmouseout="this.src = 'images/img_light_box/cantratacion.jpg'" border="0" alt="" width="257" height="47" border="0" /></a></div>
   <div id="bajada_de_texto"><p>Para visualizar este archivo se requiere Adobe Acrobat, si no lo tienes, desc�rgalo<a href="http://www.adobe.com/products/acrobat/readstep2.html"> Aqu&iacute;</a></p></div>

   <div id="senor_compartir"><p>�Te pareci� �til esta informaci�n? <img src="images/img_light_box/btn-mas-compartir.jpg" alt="" width="14" height="14" border="0" />COMPARTIR</p></div>
   </div>
   </div>
		
	
	</body>

</html>
