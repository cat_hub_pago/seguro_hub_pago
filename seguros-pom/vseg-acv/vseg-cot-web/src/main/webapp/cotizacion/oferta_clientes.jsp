<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta name="generator" content="cencosud" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<link href="<bean:write name="contextpath"/>/css/estilo_ofclientes.css" rel="stylesheet" type="text/css" />
		<link href="<bean:write name="contextpath"/>/css/colorbox.css" rel="stylesheet" type="text/css" />
		
		<script src="<bean:write name="contextpath"/>/js/jquery/jquery-1.4.2.js"></script>
		<script src="<bean:write name="contextpath"/>/js/jquery/jquery.colorbox.js" type="text/javascript" ></script>
		<script src="<bean:write name="contextpath"/>/js/jquery/jquery.tools.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="css/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

	</head>

	<body>
		<%@ include file="../google-analytics/google-analytics.jsp" %>
	
		<div class="contenido_ofclientes">
			<!--div id="estimado_ofclientes">
			Beneficio Exclusivo Colaboradores
			</div-->

			<div id="txt_ofclientes">
			Por haber contratado tu <span>SOAP en www.seguroscencosud.cl<br /> 
			</span> obt&eacute;n un <span><bean:write name="glosaDescuento" /></span> <br /> 
			de tu Seguro Full Servicio y disfruta de 15 SERVICIOS INCLUIDOS.<br /><br />  
			
			V&aacute;lido para contrataciones con el  
			<br />
			rut <font><bean:write name="rutDescuento" />-<bean:write name="dvDescuento" /></font>
			</div>
		
			<div id="cotizar_ofclientes">
			Cotizar
			</div>

			<div id="btn_ofclientes">
			<a href="/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=22" target="_parent"></a>
			</div>
		</div>
	</body>
</html>

