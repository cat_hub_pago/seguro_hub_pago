<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="cencosud" />
	<title>Monitoreo</title>
	<script src="js/jquery/jquery-1.4.2.js"></script>
	<script type="text/javascript">
	
	function moverDatos(origen, destino) {
		var html = $("#" + origen).text();
		$("#" + destino).html(html);		
	}

	function copiarDatos(origen, destino) {
		var html = $("#" + origen).text();
		$("#" + destino).text(html);		
	}
	
	$(document).ready( function() {
		moverDatos('html', 'div_poliza');
		copiarDatos('paginabsp', 'div_paginabsp');
		copiarDatos('paginaequifax', 'div_paginaequifax');
	});
</script>
</head>
<body
	style="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: small;">

	<div style="text-align: right;">
		Resultados obtenidos en:
		<bean:write name="horas" format="00" />
		:
		<bean:write name="minutos" format="00" />
		:
		<bean:write name="segundos" format="00" />
		.
		<bean:write name="milis" format="00" />
	</div>

	<div style="font-weight: bold;">
		Base de Datos:
	</div>
	<table cellpadding="0" cellspacing="0" style="width: 400px;">
		<logic:present name="regiones">
			<thead>
				<tr>
					<th style="border: 1px solid; width: 100px;">
						ID
					</th>
					<th style="border: 1px solid; width: 300px;">
						Descripci&oacute;n
					</th>
				</tr>
			</thead>
			<tbody>
				<logic:iterate id="region" name="regiones">
					<tr>
						<td style="text-align: center;">
							<bean:write name="region" property="id" />
						</td>
						<td style="text-align: center;">
							<bean:write name="region" property="descripcion" />
						</td>
					</tr>
				</logic:iterate>
			</tbody>
		</logic:present>
		<logic:notPresent name="regiones">
			<tr>
				<td style="color: red;">
					Error al obtener regiones desde la base de datos
				</td>
			</tr>
		</logic:notPresent>
	</table>

	<br />
	<br />
	<div style="font-weight: bold;">
		WebService BIGSA
	</div>
	<table cellpadding="0" cellspacing="0" style="width: 400px;">
		<logic:present name="poliza">
			<div id="div_poliza"
				style="width: 650px; height: 150px; overflow: auto;">
			</div>
			<div id="html" style="display: none;">
				<bean:write name="poliza" filter="true" />
			</div>
		</logic:present>
		<logic:notPresent name="poliza">
			<tr>
				<td style="color: red;">
					Error al obtener poliza desde WebService
				</td>
			</tr>
		</logic:notPresent>
	</table>


	<br />
	<br />
	<div style="font-weight: bold;">
		WebService BSP
	</div>
	<table cellpadding="0" cellspacing="0" style="width: 400px;">
		<logic:present name="paginaBSP">
			<div id="div_paginabsp"
				style="width: 650px; height: 150px; overflow: auto;">
			</div>
			<div id="paginabsp" style="display: none;">
				<bean:write name="paginaBSP" filter="true" />
			</div>
		</logic:present>
		<logic:notPresent name="paginaBSP">
			<tr>
				<td style="color: red;">
					Error al validar BSP.
				</td>
			</tr>
			<tr>
				<td>
					<bean:write name="errorBSP" />
				</td>
			</tr>
		</logic:notPresent>
	</table>


	<br />
	<br />
	  
	<div style="font-weight: bold;">
		WebService SIISA
	</div>
	<table cellpadding="0" cellspacing="0" style="width: 400px;">
		<logic:present name="paginaEQUIFAX">
			<div id="div_paginaequifax"
				style="width: 650px; height: 150px; overflow: auto;">
			</div>
			<div id="paginaequifax" style="display: none;">
				<bean:write name="paginaEQUIFAX" filter="true" />
			</div>
		</logic:present>
		<logic:notPresent name="paginaEQUIFAX">
			<tr>
				<td style="color: red;">
					Error al validar EQUIFAX.
				</td>
			</tr>
			<tr>
				<td>
					<bean:write name="errorEQUIFAX" />
				</td>
			</tr>
		</logic:notPresent>
	</table>
	
	<br />
	<br />
</body>
</html:html>