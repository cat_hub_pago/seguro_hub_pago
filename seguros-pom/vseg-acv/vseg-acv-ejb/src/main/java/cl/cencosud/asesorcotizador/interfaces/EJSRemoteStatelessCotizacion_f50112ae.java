package cl.cencosud.asesorcotizador.interfaces;

import java.rmi.RemoteException;

import cl.cencosud.acv.common.CoberturaSeg;
import cl.cencosud.acv.common.PatenteVehiculo;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessCotizacion_f50112ae
 */
public class EJSRemoteStatelessCotizacion_f50112ae extends EJSWrapper implements
		Cotizacion {
	/**
	 * EJSRemoteStatelessCotizacion_f50112ae
	 */
	public EJSRemoteStatelessCotizacion_f50112ae()
			throws java.rmi.RemoteException {
		super();
	}

	/**
	 * existePrimaUnica
	 */
	public boolean existePrimaUnica(long idPlan)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idPlan);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.existePrimaUnica(idPlan);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerCotizacionHogarPDF
	 */
	public byte[] obtenerCotizacionHogarPDF(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar cotHogar,
			cl.cencosud.acv.common.Solicitud solicitud,
			cl.tinet.common.seguridad.model.Usuario usuario)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		byte[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = cotHogar;
				_jacc_parms[1] = solicitud;
				_jacc_parms[2] = usuario;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCotizacionHogarPDF(cotHogar,
					solicitud, usuario);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerCotizacionVehiculoPDF
	 */
	public byte[] obtenerCotizacionVehiculoPDF(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo vehiculo,
			cl.cencosud.acv.common.Solicitud solicitud,
			cl.tinet.common.seguridad.model.Usuario usuario)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		byte[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = vehiculo;
				_jacc_parms[1] = solicitud;
				_jacc_parms[2] = usuario;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCotizacionVehiculoPDF(vehiculo,
					solicitud, usuario);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerCotizacionVidaPDF
	 */
	public byte[] obtenerCotizacionVidaPDF(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida cotVida,
			cl.cencosud.acv.common.Solicitud solicitud,
			cl.tinet.common.seguridad.model.Usuario usuario)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		byte[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = cotVida;
				_jacc_parms[1] = solicitud;
				_jacc_parms[2] = usuario;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCotizacionVidaPDF(cotVida, solicitud,
					usuario);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerPolizaHtml
	 */
	public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		byte[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Integer(rutCliente);
				_jacc_parms[1] = new java.lang.Integer(numeroSolicitud);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef
					.obtenerPolizaHtml(rutCliente, numeroSolicitud);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerActividades
	 */
	public cl.cencosud.acv.common.Actividad[] obtenerActividades()
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Actividad[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerActividades();
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerCoberturas
	 */
	public cl.cencosud.acv.common.Cobertura[] obtenerCoberturas(
			java.lang.String idPlan) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Cobertura[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idPlan;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCoberturas(idPlan);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerColoresVehiculo
	 */
	public cl.cencosud.acv.common.ColorVehiculo[] obtenerColoresVehiculo()
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.ColorVehiculo[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 7, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerColoresVehiculo();
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosContratante
	 */
	public cl.cencosud.acv.common.Contratante obtenerDatosContratante(
			long idSolicitud) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Contratante _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 8, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosContratante(idSolicitud);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerEstadosCiviles
	 */
	public cl.cencosud.acv.common.EstadoCivil[] obtenerEstadosCiviles()
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.EstadoCivil[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 9, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerEstadosCiviles();
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerCiudades
	 */
	public cl.cencosud.acv.common.ParametroCotizacion[] obtenerCiudades(
			java.lang.String idComuna) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.ParametroCotizacion[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idComuna;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 10, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCiudades(idComuna);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 10, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerComunas
	 */
	public cl.cencosud.acv.common.ParametroCotizacion[] obtenerComunas(
			java.lang.String idRegion) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.ParametroCotizacion[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idRegion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 11, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerComunas(idRegion);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 11, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerRegiones
	 */
	public cl.cencosud.acv.common.ParametroCotizacion[] obtenerRegiones()
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.ParametroCotizacion[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 12, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRegiones();
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 12, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosPlan
	 */
	public cl.cencosud.acv.common.Plan obtenerDatosPlan(long idPlan)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Plan _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idPlan);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 13, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosPlan(idPlan);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 13, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * valorizarPlan
	 */
	public cl.cencosud.acv.common.Plan valorizarPlan(
			java.lang.String idProducto,
			cl.cencosud.acv.common.valorizacion.CotizacionSeguro cotizacionSeguro,
			java.lang.String idPlan)
			throws cl.cencosud.acv.common.exception.ProcesoCotizacionException,
			cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Plan _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = idProducto;
				_jacc_parms[1] = cotizacionSeguro;
				_jacc_parms[2] = idPlan;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 14, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.valorizarPlan(idProducto, cotizacionSeguro,
					idPlan);
		} catch (cl.cencosud.acv.common.exception.ProcesoCotizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 14, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * valorizarPlanDeducibles
	 */
	public cl.cencosud.acv.common.Plan valorizarPlanDeducibles(
			java.lang.String idProducto2,
			java.lang.String idProducto3,
			java.lang.String idProducto4,
			java.lang.String idProducto5,
			java.lang.String idProducto6,
			java.lang.String idProducto7,
			cl.cencosud.acv.common.valorizacion.CotizacionSeguro cotizacionSeguro,
			java.lang.String idPlan)
			throws cl.cencosud.acv.common.exception.ProcesoCotizacionException,
			cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Plan _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[8];
				_jacc_parms[0] = idProducto2;
				_jacc_parms[1] = idProducto3;
				_jacc_parms[2] = idProducto4;
				_jacc_parms[3] = idProducto5;
				_jacc_parms[4] = idProducto6;
				_jacc_parms[5] = idProducto7;
				_jacc_parms[6] = cotizacionSeguro;
				_jacc_parms[7] = idPlan;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 15, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.valorizarPlanDeducibles(idProducto2,
					idProducto3, idProducto4, idProducto5, idProducto6,
					idProducto7, cotizacionSeguro, idPlan);
		} catch (cl.cencosud.acv.common.exception.ProcesoCotizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 15, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerPlanes
	 */
	public cl.cencosud.acv.common.Plan[] obtenerPlanes(
			java.lang.String idProducto, java.lang.String idPlanPromocion)
			throws cl.cencosud.acv.common.exception.ProcesoCotizacionException,
			cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Plan[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idProducto;
				_jacc_parms[1] = idPlanPromocion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 16, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPlanes(idProducto, idPlanPromocion);
		} catch (cl.cencosud.acv.common.exception.ProcesoCotizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 16, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerPlanesCotizacion
	 */
	public cl.cencosud.acv.common.Plan[] obtenerPlanesCotizacion(
			java.lang.String idProducto,
			cl.cencosud.acv.common.valorizacion.CotizacionSeguro cotizacionSeguro,
			java.lang.String idPlanPromocion)
			throws cl.cencosud.acv.common.exception.ProcesoCotizacionException,
			cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Plan[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = idProducto;
				_jacc_parms[1] = cotizacionSeguro;
				_jacc_parms[2] = idPlanPromocion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 17, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPlanesCotizacion(idProducto,
					cotizacionSeguro, idPlanPromocion);
		} catch (cl.cencosud.acv.common.exception.ProcesoCotizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 17, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerPlanesDeducibles
	 */
	public cl.cencosud.acv.common.Plan[] obtenerPlanesDeducibles(
			java.lang.String idProducto2, java.lang.String idProducto3,
			java.lang.String idProducto4, java.lang.String idProducto5,
			java.lang.String idProducto6, java.lang.String idProducto7,
			java.lang.String idPlanPromocion)
			throws cl.cencosud.acv.common.exception.ProcesoCotizacionException,
			cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Plan[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[7];
				_jacc_parms[0] = idProducto2;
				_jacc_parms[1] = idProducto3;
				_jacc_parms[2] = idProducto4;
				_jacc_parms[3] = idProducto5;
				_jacc_parms[4] = idProducto6;
				_jacc_parms[5] = idProducto7;
				_jacc_parms[6] = idPlanPromocion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 18, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPlanesDeducibles(idProducto2,
					idProducto3, idProducto4, idProducto5, idProducto6,
					idProducto7, idPlanPromocion);
		} catch (cl.cencosud.acv.common.exception.ProcesoCotizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 18, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerProductos
	 */
	public cl.cencosud.acv.common.Producto[] obtenerProductos(
			java.lang.String idSubcategoria, java.lang.String idRama,
			java.lang.String idPlan)
			throws cl.cencosud.acv.common.exception.ProcesoCotizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Producto[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = idSubcategoria;
				_jacc_parms[1] = idRama;
				_jacc_parms[2] = idPlan;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 19, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerProductos(idSubcategoria, idRama,
					idPlan);
		} catch (cl.cencosud.acv.common.exception.ProcesoCotizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 19, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerRamaPorId
	 */
	public cl.cencosud.acv.common.Rama obtenerRamaPorId(long idRama)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Rama _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idRama);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 20, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRamaPorId(idRama);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 20, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerRestriccionVida
	 */
	public cl.cencosud.acv.common.RestriccionVida obtenerRestriccionVida(
			long idPlan) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.RestriccionVida _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idPlan);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 21, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRestriccionVida(idPlan);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 21, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosSolicitud
	 */
	public cl.cencosud.acv.common.Solicitud obtenerDatosSolicitud(
			long idSolicitud) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Solicitud _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 22, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosSolicitud(idSolicitud);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 22, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerSubcategoriaPorId
	 */
	public cl.cencosud.acv.common.Subcategoria obtenerSubcategoriaPorId(
			long idSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Subcategoria _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idSubcategoria);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 23, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerSubcategoriaPorId(idSubcategoria);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 23, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerMarcasPorTipoVehiculos
	 */
	public cl.cencosud.acv.common.Vehiculo[] obtenerMarcasPorTipoVehiculos(
			java.lang.String idTipo) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Vehiculo[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idTipo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 24, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerMarcasPorTipoVehiculos(idTipo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 24, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerMarcasVehiculos
	 */
	public cl.cencosud.acv.common.Vehiculo[] obtenerMarcasVehiculos()
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Vehiculo[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 25, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerMarcasVehiculos();
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 25, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerModelosVehiculo
	 */
	public cl.cencosud.acv.common.Vehiculo[] obtenerModelosVehiculo(
			java.lang.String idTipo, java.lang.String idMarca)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Vehiculo[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idTipo;
				_jacc_parms[1] = idMarca;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 26, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerModelosVehiculo(idTipo, idMarca);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 26, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerTiposVehiculo
	 */
	public cl.cencosud.acv.common.Vehiculo[] obtenerTiposVehiculo()
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Vehiculo[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 27, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerTiposVehiculo();
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 27, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerTiposVehiculo
	 */
	public cl.cencosud.acv.common.Vehiculo[] obtenerTiposVehiculo(int idTipo)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Vehiculo[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Integer(idTipo);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 28, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerTiposVehiculo(idTipo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 28, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosMateriaHogar
	 */
	public cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar obtenerDatosMateriaHogar(
			long idSolicitud) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 29, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosMateriaHogar(idSolicitud);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 29, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosMateriaVehiculo
	 */
	public cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo obtenerDatosMateriaVehiculo(
			long idSolicitud) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 30, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosMateriaVehiculo(idSolicitud);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 30, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosMateriaVida
	 */
	public cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida obtenerDatosMateriaVida(
			long idSolicitud) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 31, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosMateriaVida(idSolicitud);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 31, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerValorizacionHogar
	 */
	public cl.cencosud.acv.common.valorizacion.ValorizacionSeguro obtenerValorizacionHogar(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar primaHogar)
			throws cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.valorizacion.ValorizacionSeguro _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = primaHogar;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 32, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerValorizacionHogar(primaHogar);
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 32, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerValorizacionVehiculo
	 */
	public cl.cencosud.acv.common.valorizacion.ValorizacionSeguro obtenerValorizacionVehiculo(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo cotizacionVehiculo)
			throws cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.valorizacion.ValorizacionSeguro _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = cotizacionVehiculo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 33, _EJS_s, _jacc_parms);
			_EJS_result = beanRef
					.obtenerValorizacionVehiculo(cotizacionVehiculo);
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 33, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerValorizacionVida
	 */
	public cl.cencosud.acv.common.valorizacion.ValorizacionSeguro obtenerValorizacionVida(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida cotizacionVida)
			throws cl.cencosud.acv.common.exception.ValorizacionException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.valorizacion.ValorizacionSeguro _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = cotizacionVida;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 34, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerValorizacionVida(cotizacionVida);
		} catch (cl.cencosud.acv.common.exception.ValorizacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 34, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerComunaPorCiudad
	 */
	public cl.cencosud.ventaseguros.common.Parametro[] obtenerComunaPorCiudad(
			java.lang.String idCiudad)
			throws com.tinet.exceptions.system.SystemException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Parametro[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idCiudad;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 35, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerComunaPorCiudad(idCiudad);
		} catch (com.tinet.exceptions.system.SystemException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 35, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerRegionPorComuna
	 */
	public cl.cencosud.ventaseguros.common.Parametro[] obtenerRegionPorComuna(
			java.lang.String idComuna)
			throws com.tinet.exceptions.system.SystemException,
			java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Parametro[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idComuna;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 36, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRegionPorComuna(idComuna);
		} catch (com.tinet.exceptions.system.SystemException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 36, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * ingresarDatosAdicionalesHogar
	 */
	public int ingresarDatosAdicionalesHogar(long idSolicitud,
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar cotizacion)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		int _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
				_jacc_parms[1] = cotizacion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 37, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarDatosAdicionalesHogar(idSolicitud,
					cotizacion);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 37, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * ingresarDatosAdicionalesVehiculo
	 */
	public int ingresarDatosAdicionalesVehiculo(
			long idSolicitud,
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo cotizacion)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		int _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
				_jacc_parms[1] = cotizacion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 38, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarDatosAdicionalesVehiculo(idSolicitud,
					cotizacion);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 38, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * ingresarDatosAdicionalesVida
	 */
	public int ingresarDatosAdicionalesVida(long idSolicitud,
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida cotizacion)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		int _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
				_jacc_parms[1] = cotizacion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 39, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarDatosAdicionalesVida(idSolicitud,
					cotizacion);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 39, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerRutSinCaptcha
	 */
	public int obtenerRutSinCaptcha(long rutsincap)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		int _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(rutsincap);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 40, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRutSinCaptcha(rutsincap);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 40, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerParametro
	 */
	public java.util.List obtenerParametro(java.lang.String idGrupo)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idGrupo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 41, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerParametro(idGrupo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 41, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerVehiculos
	 */
	public java.util.List obtenerVehiculos(java.lang.String rut)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = rut;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 42, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerVehiculos(rut);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 42, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerCiudadPorId
	 */
	public java.util.Map obtenerCiudadPorId(java.lang.String idCiudad)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idCiudad;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 43, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCiudadPorId(idCiudad);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 43, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerComunaPorId
	 */
	public java.util.Map obtenerComunaPorId(java.lang.String idComuna)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idComuna;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 44, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerComunaPorId(idComuna);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 44, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosVehiculosCompleto
	 */
	public java.util.Map obtenerDatosVehiculosCompleto(java.lang.String tipo,
			java.lang.String marca, java.lang.String modelo)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = tipo;
				_jacc_parms[1] = marca;
				_jacc_parms[2] = modelo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 45, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosVehiculosCompleto(tipo, marca,
					modelo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 45, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerDatosVitrineo
	 */
	public java.util.Map obtenerDatosVitrineo(java.lang.String claveVitrineo)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = claveVitrineo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 46, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosVitrineo(claveVitrineo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 46, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerParametro
	 */
	public java.util.Map obtenerParametro(java.lang.String idGrupo,
			java.lang.String idParametro) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idGrupo;
				_jacc_parms[1] = idParametro;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 47, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerParametro(idGrupo, idParametro);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 47, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerRegionPorId
	 */
	public java.util.Map obtenerRegionPorId(java.lang.String idRegion)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idRegion;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 48, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRegionPorId(idRegion);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 48, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerVehiculosCompleto
	 */
	public java.util.Map obtenerVehiculosCompleto(java.lang.String rut,
			java.lang.String modelo) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = rut;
				_jacc_parms[1] = modelo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 49, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerVehiculosCompleto(rut, modelo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 49, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * grabarVitrineoGeneral
	 */
	public long grabarVitrineoGeneral(java.lang.String pswd,
			java.lang.String rutVitrineo, java.lang.String dvVitrineo,
			java.lang.String nombre, java.lang.String apellidoPaterno,
			java.lang.String apellidoMaterno, java.lang.String tipoTelefono,
			java.lang.String codTelefono, java.lang.String telefono,
			java.lang.String email, int region, int comuna,
			java.lang.String diaNac, java.lang.String mesNac,
			java.lang.String anyoNac, java.lang.String estadoCivil,
			java.lang.String sexo, long edadConductor, int tipoVehiculo,
			int idMarcaVitrineo, int idModeloVitrineo,
			java.lang.String idAnyoVitrineo, int numPuertas,
			java.lang.String esDuenyo, java.lang.String ciudadHogar,
			java.lang.String direccionHogar,
			java.lang.String numeroDireccionHogar,
			java.lang.String numeroDeptoHogar, java.lang.String anyoHogar,
			int rama, int subcategoria, int productoVitrineo,
			java.lang.String rutDuenyo, java.lang.String dvDuenyo,
			java.lang.String nombreDuenyo,
			java.lang.String apellidoPaternoDuenyo,
			java.lang.String apellidoMaternoDuenyo,
			java.lang.String diaNacimientoDuenyo,
			java.lang.String mesNacimientoDuenyo,
			java.lang.String anyoNacimientoDuenyo,
			java.lang.String estadoCivilDuenyo,
			java.lang.String regionResidenciaDuenyo,
			java.lang.String comunaResidenciaDuenyo,
			java.lang.String valorComercial) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[44];
				_jacc_parms[0] = pswd;
				_jacc_parms[1] = rutVitrineo;
				_jacc_parms[2] = dvVitrineo;
				_jacc_parms[3] = nombre;
				_jacc_parms[4] = apellidoPaterno;
				_jacc_parms[5] = apellidoMaterno;
				_jacc_parms[6] = tipoTelefono;
				_jacc_parms[7] = codTelefono;
				_jacc_parms[8] = telefono;
				_jacc_parms[9] = email;
				_jacc_parms[10] = new java.lang.Integer(region);
				_jacc_parms[11] = new java.lang.Integer(comuna);
				_jacc_parms[12] = diaNac;
				_jacc_parms[13] = mesNac;
				_jacc_parms[14] = anyoNac;
				_jacc_parms[15] = estadoCivil;
				_jacc_parms[16] = sexo;
				_jacc_parms[17] = new java.lang.Long(edadConductor);
				_jacc_parms[18] = new java.lang.Integer(tipoVehiculo);
				_jacc_parms[19] = new java.lang.Integer(idMarcaVitrineo);
				_jacc_parms[20] = new java.lang.Integer(idModeloVitrineo);
				_jacc_parms[21] = idAnyoVitrineo;
				_jacc_parms[22] = new java.lang.Integer(numPuertas);
				_jacc_parms[23] = esDuenyo;
				_jacc_parms[24] = ciudadHogar;
				_jacc_parms[25] = direccionHogar;
				_jacc_parms[26] = numeroDireccionHogar;
				_jacc_parms[27] = numeroDeptoHogar;
				_jacc_parms[28] = anyoHogar;
				_jacc_parms[29] = new java.lang.Integer(rama);
				_jacc_parms[30] = new java.lang.Integer(subcategoria);
				_jacc_parms[31] = new java.lang.Integer(productoVitrineo);
				_jacc_parms[32] = rutDuenyo;
				_jacc_parms[33] = dvDuenyo;
				_jacc_parms[34] = nombreDuenyo;
				_jacc_parms[35] = apellidoPaternoDuenyo;
				_jacc_parms[36] = apellidoMaternoDuenyo;
				_jacc_parms[37] = diaNacimientoDuenyo;
				_jacc_parms[38] = mesNacimientoDuenyo;
				_jacc_parms[39] = anyoNacimientoDuenyo;
				_jacc_parms[40] = estadoCivilDuenyo;
				_jacc_parms[41] = regionResidenciaDuenyo;
				_jacc_parms[42] = comunaResidenciaDuenyo;
				_jacc_parms[43] = valorComercial;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 50, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.grabarVitrineoGeneral(pswd, rutVitrineo,
					dvVitrineo, nombre, apellidoPaterno, apellidoMaterno,
					tipoTelefono, codTelefono, telefono, email, region, comuna,
					diaNac, mesNac, anyoNac, estadoCivil, sexo, edadConductor,
					tipoVehiculo, idMarcaVitrineo, idModeloVitrineo,
					idAnyoVitrineo, numPuertas, esDuenyo, ciudadHogar,
					direccionHogar, numeroDireccionHogar, numeroDeptoHogar,
					anyoHogar, rama, subcategoria, productoVitrineo, rutDuenyo,
					dvDuenyo, nombreDuenyo, apellidoPaternoDuenyo,
					apellidoMaternoDuenyo, diaNacimientoDuenyo,
					mesNacimientoDuenyo, anyoNacimientoDuenyo,
					estadoCivilDuenyo, regionResidenciaDuenyo,
					comunaResidenciaDuenyo, valorComercial);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 50, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * ingresarCotizacionHogar
	 */
	public long ingresarCotizacionHogar(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar cotizacion,
			java.lang.String idRama, java.lang.String idProducto,
			cl.cencosud.acv.common.Plan plan, java.lang.String idVitrineo)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[5];
				_jacc_parms[0] = cotizacion;
				_jacc_parms[1] = idRama;
				_jacc_parms[2] = idProducto;
				_jacc_parms[3] = plan;
				_jacc_parms[4] = idVitrineo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 51, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarCotizacionHogar(cotizacion, idRama,
					idProducto, plan, idVitrineo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 51, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * ingresarCotizacionVehiculo
	 */
	public long ingresarCotizacionVehiculo(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo cotizacion,
			java.lang.String idRama, java.lang.String idProducto,
			cl.cencosud.acv.common.Plan plan, java.lang.String idVitrineo)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[5];
				_jacc_parms[0] = cotizacion;
				_jacc_parms[1] = idRama;
				_jacc_parms[2] = idProducto;
				_jacc_parms[3] = plan;
				_jacc_parms[4] = idVitrineo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 52, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarCotizacionVehiculo(cotizacion,
					idRama, idProducto, plan, idVitrineo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 52, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * ingresarCotizacionVida
	 */
	public long ingresarCotizacionVida(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida cotizacion,
			java.lang.String idRama, java.lang.String idProducto,
			cl.cencosud.acv.common.Plan plan, java.lang.String idVitrineo)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[5];
				_jacc_parms[0] = cotizacion;
				_jacc_parms[1] = idRama;
				_jacc_parms[2] = idProducto;
				_jacc_parms[3] = plan;
				_jacc_parms[3] = idVitrineo;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 53, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarCotizacionVida(cotizacion, idRama,
					idProducto, plan, idVitrineo);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 53, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * obtenerSubcategoriaAsociada
	 */
	public long obtenerSubcategoriaAsociada(java.lang.String idProducto,
			java.lang.String idRama) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idProducto;
				_jacc_parms[1] = idRama;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 54, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerSubcategoriaAsociada(idProducto,
					idRama);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 54, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * actualizarEstadoSolicitud
	 */
	public void actualizarEstadoSolicitud(long idSolicitud,
			java.lang.String idEstado) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;

		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
				_jacc_parms[1] = idEstado;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 55, _EJS_s, _jacc_parms);
			beanRef.actualizarEstadoSolicitud(idSolicitud, idEstado);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 55, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}

	/**
	 * cotizarProducto
	 */
	public void cotizarProducto(java.util.HashMap datosContratante)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;

		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = datosContratante;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 56, _EJS_s, _jacc_parms);
			beanRef.cotizarProducto(datosContratante);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 56, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}

	/**
	 * enviarCotizacionHogarPDF
	 */
	public void enviarCotizacionHogarPDF(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar cotHogar,
			cl.cencosud.acv.common.Solicitud solicitud,
			cl.tinet.common.seguridad.model.Usuario usuario)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;

		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = cotHogar;
				_jacc_parms[1] = solicitud;
				_jacc_parms[2] = usuario;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 57, _EJS_s, _jacc_parms);
			beanRef.enviarCotizacionHogarPDF(cotHogar, solicitud, usuario);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 57, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}

	/**
	 * enviarCotizacionVehiculoPDF
	 */
	public void enviarCotizacionVehiculoPDF(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo vehiculo,
			cl.cencosud.acv.common.Solicitud solicitud,
			cl.tinet.common.seguridad.model.Usuario usuario)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;

		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = vehiculo;
				_jacc_parms[1] = solicitud;
				_jacc_parms[2] = usuario;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 58, _EJS_s, _jacc_parms);
			beanRef.enviarCotizacionVehiculoPDF(vehiculo, solicitud, usuario);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 58, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}

	/**
	 * enviarCotizacionVidaPDF
	 */
	public void enviarCotizacionVidaPDF(
			cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida cotVida,
			cl.cencosud.acv.common.Solicitud solicitud,
			cl.tinet.common.seguridad.model.Usuario usuario)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;

		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[3];
				_jacc_parms[0] = cotVida;
				_jacc_parms[1] = solicitud;
				_jacc_parms[2] = usuario;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 59, _EJS_s, _jacc_parms);
			beanRef.enviarCotizacionVidaPDF(cotVida, solicitud, usuario);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 59, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}

	/**
	 * enviarCotizacionVidaPDF
	 */
	public String consultarDescuento(int rut) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		String _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
				_jacc_parms[0] = new java.lang.Integer(rut);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 60, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.consultarDescuento(rut);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 60, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	/**
	 * guardarMascota
	 */
	public void guardarMascota(java.lang.String rutDuenoMascota,
			java.lang.String nombreMascota, java.lang.String tipoMascota,
			java.lang.String razaMascota, java.lang.String colorMascota,
			java.lang.String edadMascota, java.lang.String sexoMascota, java.lang.String direccionMascota) 
					throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;

		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[5];
				_jacc_parms[0] = rutDuenoMascota;
				_jacc_parms[1] = nombreMascota;
				_jacc_parms[2] = tipoMascota;
				_jacc_parms[3] = razaMascota;
				_jacc_parms[4] = colorMascota;
				_jacc_parms[5] = edadMascota;
				_jacc_parms[6] = sexoMascota;
				_jacc_parms[7] = direccionMascota;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 61, _EJS_s, _jacc_parms);
			beanRef.guardarMascota(rutDuenoMascota, nombreMascota, tipoMascota,
		    		razaMascota, colorMascota, edadMascota, sexoMascota, direccionMascota);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 61, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}
	
	public String obtenerIdFicha(int idSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		String _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
				_jacc_parms[0] = new java.lang.Integer(idSubcategoria);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 62, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerIdFicha(idSubcategoria);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 62, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}

	public CoberturaSeg[] consultarCoberturas(int idPlan)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.CoberturaSeg[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idPlan;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 63, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.consultarCoberturas(idPlan);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 63, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}   
		return _EJS_result;	
	}
	//Pleyasoft - Patente Vehiculo
	public PatenteVehiculo obtenerDatosPatente(String idPatente)
			throws RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		PatenteVehiculo _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
				_jacc_parms[0] = new java.lang.String(idPatente);
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 64, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosPatente(idPatente);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 64, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	
	public long guardarDatosPatente(
			cl.cencosud.acv.common.PatenteVehiculo patente
			)
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments(this)) {
				_jacc_parms = new Object[0];
				_jacc_parms[0] = patente;
			}
			cl.cencosud.asesorcotizador.ejb.CotizacionBean beanRef = (cl.cencosud.asesorcotizador.ejb.CotizacionBean) container
					.preInvoke(this, 65, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.guardarDatosPatente(patente);
		} catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		} catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException(
					"bean method raised unchecked exception", ex);
		}

		finally {
			try {
				container.postInvoke(this, 65, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	
	//Pleyasoft - Patente Vehiculo
}
