package cl.cencosud.asesorcotizador.ejb;

import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.acl.frameworkjms.exceptions.InitException;
import cl.cencosud.acv.common.RespuestaTBK;
import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.exception.PagosException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.AceptacionDAO;
import cl.cencosud.asesorcotizador.dao.PagosDAO;
import cl.cencosud.asesorcotizador.dao.SolicitudInspeccionDAO;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.tarjetamas.exception.ApplicationException;
import cl.cencosud.tarjetamas.pojo.ConsultarSaldoFullRSP;
import cl.cencosud.tarjetamas.service.ConsultarSaldoFullService;
import cl.cencosud.tarjetamas.service.ConsultarSaldoFullServiceImpl;
import cl.cencosud.ventaseguros.interfaces.MantenedorClientes;
import cl.cencosud.ventaseguros.interfaces.MantenedorClientesHome;
import cl.tinet.common.mail.Mail;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.util.crypto.CryptoUtil;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * @author Francisco Mendoza.(TInet Soluciones Inform�ticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="Pagos" display-name="Pagos"
 *           description="Servicios relacionados al proceso de pago."
 *           jndi-name="ejb/ventaseguros/Pagos" type="Stateless"
 *           view-type="remote"
 * 
 * @ejb.transaction type="Supports"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/AsesorCotizadorDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/AsesorCotizadorDS"
 * 
 * @ejb.resource-ref res-ref-name="TMAS"
 *                   res-type="javax.jms.ConnectionFactory"
 *                   res-auth="Application"
 *                   res-sharing-scope="Shareable"
 *                   jndi-name="jms/TMAS"
 * 
 * @ejb.resource-ref res-ref-name="QL_RF_TMAS_RSP"
 *                   res-type="javax.jms.Queue"
 *                   res-auth="Application"
 *                   res-sharing-scope="Shareable"
 *                   jndi-name="jms/QL_RF_TMAS_RSP"
 * 
 * @ejb.resource-ref res-ref-name="QL_RF_TMAS_REQ"
 *                   res-type="javax.jms.Queue"
 *                   res-auth="Application"
 *                   res-sharing-scope="Shareable"
 *                   jndi-name="jms/QL_RF_TMAS_REQ"
 *
 * @ejb.ejb-service-ref name="service/WsBigsaAceptaCotizacion" interface="cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.WsBigsaAceptaCotizacion"
 *                      jaxrpc-mapping-file=
 *                      "META-INF/WsBigsaAceptaCotizacionImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WS_Inspeccion"
 *                      interface="cl.cencosud.bsp.inspeccion.webservice.client.was.WS_Inspeccion"
 *                      jaxrpc
 *                      -mapping-file="META-INF/WS_Inspeccion.asmx_mapping.xml"
 */
public class PagosBean implements SessionBean {

    /**
     * Identificador de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 5897425626164398877L;

    /**
     * Logger de la clase.
     */
    private static final Log logger = LogFactory.getLog(PagosBean.class);

    private SessionContext sessionContext;

    public void ejbActivate() throws EJBException, RemoteException {
        logger.info("Activando instancia del EJB Cotizacion");

    }

    public void ejbPassivate() throws EJBException, RemoteException {
        logger.info("Pasivando instancia del EJB Cotizacion");

    }

    public void ejbRemove() throws EJBException, RemoteException {
        logger.info("Eliminando instancia del EJB Cotizacion");

    }

    public void setSessionContext(SessionContext context) throws EJBException,
        RemoteException {
        logger.info("Estableciendo contexto de sesi�n.");
        this.sessionContext = context;

    }

    public void ejbCreate() throws CreateException {
        logger.info("Creando instancia del EJB Cotizacion");
    }

    /**
     * Obtiene los datos b�sicos de una transaccion.
     * 
     * @ejb.interface-method "remote"
     */
    public Transaccion obtenerDatosTransaccion(int idSolicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            logger.info("Consultar datos del pago...");
            return dao.obtenerDatosTransaccion(idSolicitud);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo validarDigitosTarjeta.
     * 
     * @param idSolicitud
     * @param tarjeta
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean validarDigitosTarjeta(int idSolicitud, String tarjeta) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);

        boolean res = false;
        try {

            Transaccion solicitud = dao.obtenerDatosTransaccion(idSolicitud);

            //Solo se usa la validaci�n si el pago es recurrente.
            //Ya que si no es recurrente, no se pide el n�mero de tarjeta.
            if (solicitud.getTipo_cargo() == 1) {
                CryptoUtil cryptoUtil =
                    new CryptoUtil(SeguridadConfig.getInstance());
                byte[] bTarjeta =
                    cryptoUtil.fromBase64String(solicitud
                        .getNro_tarjeta_entrada());
                String nroTarjetaEntrada = cryptoUtil.getDecrypted(bTarjeta);

                //Obtener los ultimos 4 digitos.
                nroTarjetaEntrada =
                    nroTarjetaEntrada.substring(
                        (nroTarjetaEntrada.length() - 4), nroTarjetaEntrada
                            .length());
                res = nroTarjetaEntrada.equals(tarjeta);
            } else {
                res = true;
            }

            return res;

        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo enviarEmail.
     * 
     * @param datosSeguro
     * @ejb.interface-method "remote"
     */
    public void enviarEmail(CotizacionSeguro datosSeguro,
        String numeroOrdenCompra, String idRama) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);

        //Solo se pueden enviar imagenes en el correo.
        String htmlBanner =
            "<table width=\"100%\" border=\"0\" align=\"center\">" +
            "<tr><td align=\"center\">" +
            "<div class=\"banner\" style=\"margin-bottom:20px;"
                + "margin-top:20px;\">"
                //TODO: Quitar esto de aca y dejar en base de datos.
                + "<a href=\"https://www.segurosparis.cl/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=30&utm_source=Mail_Seguros&utm_medium={medium}&utm_content=21-Abr&utm_campaign=Inter_LCD_Reembolso\">"
                + "<img src=\"{banner}\""
                + "name=\"campanna\" id=\"campanna\" border=\"0\"/></a></div>"
                + "</td></tr></table>";

        try {
            
            ServiceLocator locator = ServiceLocator.singleton();
            MantenedorClientesHome clientesHome =
                (MantenedorClientesHome) locator.getRemoteHome(
                    MantenedorClientesHome.JNDI_NAME,
                    MantenedorClientesHome.class);
            MantenedorClientes mantenedorClientes = clientesHome.create();

            String mensaje = "";
            String email = datosSeguro.getEmail();
            String nombre = datosSeguro.getNombre();
            String server = dao.obtenerDatosEmail("SERVER_CORREO");
            String from = dao.obtenerDatosEmail("MAIL_FROM");
            String asunto = dao.obtenerDatosEmail("MAIL_SUBJECT_COMPRA");

            String sRutCliente = String.valueOf(datosSeguro.getRutCliente());

            int rutCliente = Integer.parseInt(sRutCliente);
            int numeroSolicitud = Integer.parseInt(numeroOrdenCompra);
            // Enviar mail.

            String medium = "";
            
            String cuerpoEmail = "HTML_COMPRA_GENERAL";
            if (datosSeguro instanceof CotizacionSeguroVehiculo) {
                CotizacionDelegate cotizacionDelegate =
                    new CotizacionDelegate();
                long idSubcagegoriaSeguro =
                    cotizacionDelegate.obtenerSubcategoriaAsociada(String
                        .valueOf(datosSeguro.getCodigoProducto()), idRama);

                boolean esProvisorio = true;
                
                // Discriminar tipo de subcategoria para texto provisorio.
                List < Map < String, ? > > grupoSubcategoria =
                    cotizacionDelegate.obtenerParametro("SUBCAT_SINTEXTO_PROV");
                for (Iterator < Map < String, ? >> it =
                    grupoSubcategoria.iterator(); it.hasNext();) {
                    String id_subcategoria = it.next().get("VALOR").toString();
                    if (idSubcagegoriaSeguro == Long.parseLong(id_subcategoria)) {
                        esProvisorio = false;
                    }
                }

                if(idRama.equals("8")){
                	logger.info("EMAIL MOTO");
                	esProvisorio = false; 	
                }
                
                logger.info("EMAIL: " + esProvisorio);

                if (esProvisorio) {
                    cuerpoEmail = "HTML_COMPRA_AUTOUSADO";
                    medium = "mail_auto_usado";

                    CotizacionSeguroVehiculo vehiculo =
                        (CotizacionSeguroVehiculo) datosSeguro;
                    if (vehiculo.getFactura() != null) {
                        logger.info("VALOR FACTURA: "
                            + String.valueOf(vehiculo.getFactura()));
                        cuerpoEmail = "HTML_COMPRA_AUTONUEVO";
                        medium = "mail_auto_nuevo";
                    }
                }

            }
            
            logger.info("CUERPO DE EMAIL: " + cuerpoEmail);

            Mail mail = new Mail(from, email, asunto, "", server);

            try {
                logger.info(cuerpoEmail);
                InputStream is = dao.obtenerCuerpoEmail(cuerpoEmail);

                String archivo = mail.convertStreamToString(is);
                //logger.info(archivo);

                // Reemplazar nombre y clave.
                mensaje = archivo.replace("{0}", nombre);

                //Campa�a.
                CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
                String codigoPlan = String.valueOf(datosSeguro.getCodigoPlan());
                logger.info("Obtener CAMPANNA PARA PLAN: " + codigoPlan);
                Map < String, ? > parametro =
                    cotizacionDelegate.obtenerParametro("CAMPANNA", codigoPlan);

                if (parametro != null) {

                    //Para el banner del correo se usa un swf distinto.
                    String htmlCampanna = (String) parametro.get("DESCRIPCION");
                    logger.info("URL CAMPANNA OBTENIDA : " + htmlCampanna);
                    if (htmlCampanna != null && !("").equals(htmlCampanna)) {

                        mensaje = mensaje.replace("{1}", htmlBanner);
                        mensaje = mensaje.replace("{banner}", htmlCampanna);
                        mensaje = mensaje.replace("{medium}", medium);
                    }
                } else {
                    mensaje = mensaje.replace("{1}", "");                    
                }
                logger.info(mensaje);

                logger.info("obtener poliza pdf");
                byte[] poliza =
                    mantenedorClientes.obtenerPolizaPdf(rutCliente,
                        numeroSolicitud);

                mail.addContenido(mensaje);
                mail.addContenido(poliza, "application/pdf",
                    "certificado-poliza.pdf");

                logger.info("enviar email");
                mail.sendMail();
            } catch (Exception e) {
                logger.error("Excepcion general al generar email", e);
            }
        } catch (RemoteException e) {
            logger.error("Remote Exception al enviar correo", e);
        } catch (CreateException e) {
            logger.error("Create Exception al enviar correo", e);
        } catch (Exception e) {
            logger.error("Excepcion general al enviar correo", e);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir metodo solicitarInspeccion.
     * 
     * @param String
     * @ejb.interface-method "remote"
     */
    public String solicitarInspeccion(SolicitudInspeccion solicitudInspeccion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        SolicitudInspeccionDAO dao =
            factory.getVentaSegurosDAO(SolicitudInspeccionDAO.class);
        PagosDAO pagosDAO = factory.getVentaSegurosDAO(PagosDAO.class);
        try {

            logger.info("SOLICITAR INSPECCION BEAN");

            String idSolicitudInspeccion =
                dao.solicitarInspeccion(solicitudInspeccion);

            logger.info("FIN SOLICITAR INSPECCION BEAN");

            if (idSolicitudInspeccion != null
                && !idSolicitudInspeccion.equals("")) {
                pagosDAO.actualizaSolitud(new Long(solicitudInspeccion
                    .getId_solicitud()), idSolicitudInspeccion);
            }

            return idSolicitudInspeccion;
        } finally {
            dao.close();
            pagosDAO.close();
        }
    }

    /**
     * TODO Describir m�todo generarTransaccion.
     * @param transaccion
     * @ejb.interface-method "remote"
     */
    public void generarTransaccion(Transaccion transaccion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            dao.generarTransaccion(transaccion);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo actualizarEstadoTransaccion.
     * @param transaccion
     * @ejb.interface-method "remote"
     */
    public void actualizarEstadoTransaccion(Transaccion transaccion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            dao.actualizarEstadoTransaccion(transaccion);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo actualizarTransaccion.
     * @param transaccion
     * @ejb.interface-method "remote"
     */
    public void actualizarTransaccion(Transaccion transaccion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            dao.actualizarTransaccion(transaccion);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerDatosTransaccion.
     * @param transaccion
     * @return
     * @ejb.interface-method "remote"
     */
    public Transaccion obtenerDatosTransaccion(Transaccion transaccion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            return dao.obtenerDatosTransaccion(transaccion);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo aceptaTransaccion.
     * @param transaccion
     * @throws BusinessException
     * @ejb.interface-method "remote"
     */
    public boolean aceptaTransaccion(Transaccion transaccion)
        throws BusinessException {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        AceptacionDAO daoAceptacion = factory.getVentaSegurosDAO(AceptacionDAO.class);
        try {
            Transaccion dataTransaccion = dao.obtenerDatosTransaccion(transaccion);
            CryptoUtil cryptoUtil = new CryptoUtil(SeguridadConfig.getInstance());
            byte[] tarjeta = cryptoUtil.fromBase64String(dataTransaccion.getNro_tarjeta_entrada());
            String tarjetadec = cryptoUtil.getDecrypted(tarjeta);
            dataTransaccion.setNro_tarjeta_entrada(tarjetadec);

            logger.info("Llamar DAO Aceptacion...");
            return daoAceptacion.aceptaTransaccion(dataTransaccion);

        } finally {
            dao.close();
            logger.info("FIN DAO Aceptacion...");
        }
    }

    /**
     * TODO Describir m�todo pagarCencosud.
     * @param transaccion
     * @return
     * @throws BusinessException
     * @ejb.interface-method "remote"
     */
    public boolean pagarCencosud(Transaccion transaccion)
        throws BusinessException {

        logger.info("PAGOSBEAN: ENTRANDO A PAGAR CENCOSUD!!!!!");
        //Validar tarjeta mas.
        ConsultarSaldoFullService consulta;
        try {
            String nroTarjeta = transaccion.getNro_tarjeta_entrada();

            CryptoUtil cryptoUtil =
                new CryptoUtil(SeguridadConfig.getInstance());
            byte[] tarjeta = cryptoUtil.fromBase64String(nroTarjeta);
            String tarjetadec = cryptoUtil.getDecrypted(tarjeta);

            consulta = new ConsultarSaldoFullServiceImpl();
            logger.info("PAGOSBEAN -> pagarCencosud:Tarjeta: "+tarjetadec);
            ConsultarSaldoFullRSP saldo =
                consulta.consultarSaldoFull("000" + tarjetadec);
            logger.info("PAGOSBEAN -> pagarCencosud:RESPUESTA RECIBIDA!!!");

            logger.info("PAGOSBEAN -> pagarCencosud:Saldo : " + saldo);

            if (saldo == null) {
                throw new PagosException(PagosException.ERROR_OBTENER_SALDO);
            } else {
                //Validaciones.
                String indicadorError = saldo.getIndicadorError();
                if (!"00000".equals(indicadorError)) {
                    logger.error("Indicador de error: " + indicadorError);
                    throw new PagosException(PagosException.ERROR_DE_VALIDACION);
                }

                String rutTitular = saldo.getRutTitular();

                Long lRutTitular = Long.parseLong(rutTitular);
                logger.info("PAGOSBEAN -> pagarCencosud:rut titular pcbs: " + lRutTitular);
                logger.info("PAGOSBEAN -> pagarCencosud:rut titular: " + transaccion.getRut_cliente());
                //Long rutAux = Long.parseLong("7794");
                if (lRutTitular != /*lRutTitular*/transaccion.getRut_cliente()) {
                    logger.error("RUT DE TITULAR NO COINCIDE");
                    throw new PagosException(
                        PagosException.ERROR_RUT_VALIDACION);
                }

                String bloqueado1 = saldo.getCodigoBloqueo1();
                logger.info("bloqueado1 -> pagarCencosud: " + bloqueado1);
                String bloqueado2 = saldo.getCodigoBloqueo2();
                logger.info("bloqueado2 -> pagarCencosud: " + bloqueado1);

                if (!" ".equals(bloqueado1) && !" ".equals(bloqueado2)) {
                    logger.error("TARJETA BLOQUEADA");
                    throw new PagosException(
                        PagosException.ERROR_TARJETA_BLOQUEADA);
                }

                String saldoDisponible =
                    saldo.getDisponibleCompraPesosAutorizador();
                String lastchar = ""+saldoDisponible.charAt(saldoDisponible.length() - 1)+"";
                try {
                	Integer.parseInt(lastchar);
                	} catch (NumberFormatException nfe){
                	saldoDisponible = "0";
                	}

                logger.info("saldo -> pagarCencosud: " + saldoDisponible);

                Long lSaldoDisponible = Long.parseLong(saldoDisponible);
                //logger.info("lSaldo -> pagarCencosud: " + lSaldoDisponible);

                if (lSaldoDisponible < transaccion.getMonto()) {
                    throw new PagosException(
                        PagosException.ERROR_SALDO_NO_DISPONIBLE);
                }
            }
            logger.info("FIN DE VALIDACIONES!!!!!");

            return this.aceptaTransaccion(transaccion);

        } catch (InitException e) {
            logger.error("Error", e);
            throw new SystemException(e);
        } catch (ApplicationException e) {
            logger.error("Error", e);
            throw new SystemException(e);
        } catch (cl.cencosud.tarjetamas.exception.BusinessException e) {
            logger.error("Error", e);
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo grabarDatosRespuesta.
     * @param parametros
     * @return
     * @ejb.interface-method "remote"
     */
    public long grabarDatosRespuesta(RespuestaTBK respuesta) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            return dao.grabarDatosRespuesta(respuesta);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerDatosRespuesta.
     * @param idSolicitud
     * @return RespuestaTBK
     * @ejb.interface-method "remote"
     */
    public RespuestaTBK obtenerDatosRespuesta(long idSolicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            return dao.obtenerDatosRespuesta(idSolicitud);
        } finally {
            dao.close();
        }
    }
    
    /**
     * TODO Describir m�todo guardarNoRegistrado.
     * @param rutCliente, nombreCliente, mailCliente, tipoSeguro
     * @ejb.interface-method "remote"
     */
    public void guardarNoRegistrado(String rutCliente, String nombreCliente, String mailCliente, String tipoSeguro) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        PagosDAO dao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            dao.guardarNoRegistrado(rutCliente, nombreCliente, mailCliente, tipoSeguro);
        } finally {
            dao.close();
        }
    }
    
    
    /**
     * TODO Describir m�todo obtenerParametroSistema.
     * @param  grupo,  nombre
     * @ejb.interface-method "remote"
     */
	public String obtenerParametroSistema(String grupo, String nombre) {
		ACVDAOFactory factory = ACVDAOFactory.getInstance();
		ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
		String valor = "";
		try {
			valor = dao.getString(grupo, nombre);
		} finally {
			dao.close();
		}

		return valor;
	}
}
