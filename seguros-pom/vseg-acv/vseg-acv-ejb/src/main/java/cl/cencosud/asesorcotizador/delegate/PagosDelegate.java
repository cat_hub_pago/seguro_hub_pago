package cl.cencosud.asesorcotizador.delegate;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import cl.cencosud.acv.common.RespuestaTBK;
import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.interfaces.Pagos;
import cl.cencosud.asesorcotizador.interfaces.PagosHome;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Delegate para los servicios del m�dulo de pagos.
 * <br/>
 * @author Francisco Mendoza C.
 * @version 1.0
 * @created 30/09/2010
 */
public class PagosDelegate {
	
	private static Logger logger = Logger.getLogger(PagosDelegate.class);
    private Pagos pagos;

    private PagosHome pagosHome;

    /**
     * Constructor de la clase.
     */
    public PagosDelegate() {
        try {
            this.pagosHome =
                (PagosHome) ServiceLocator.singleton().getRemoteHome(
                    PagosHome.JNDI_NAME, PagosHome.class);

            this.pagos = this.pagosHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene los datos b�sicos de un Contratante.
     * @param rut rut del cliente
     * @param dv digito verificador.
     * @return datos del contratante.
     */
    public Transaccion obtenerDatosTransaccion(int idSolicitud) {
        Transaccion resultado = new Transaccion();
        try {
            resultado = this.pagos.obtenerDatosTransaccion(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * TODO Describir m�todo validarDigitosTarjeta.
     * @param idSolicitud
     * @param tarjeta
     * @return
     */
    public boolean validarDigitosTarjeta(int idSolicitud, String tarjeta) {
        try {
            return this.pagos.validarDigitosTarjeta(idSolicitud, tarjeta);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo enviarEmail.
     * @param datosSeguro
     */
    public void enviarEmail(CotizacionSeguro datosSeguro,
        String numeroOrdenCompra, String idRama) {
        try {
            this.pagos.enviarEmail(datosSeguro, numeroOrdenCompra, idRama);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir metodo solicitarInspeccion.
     * @param String
     */
    public String solicitarInspeccion(SolicitudInspeccion solicitudInspeccion) {
        try {
            return this.pagos.solicitarInspeccion(solicitudInspeccion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo generarTransaccion.
     * @param transaccion
     */
    public void generarTransaccion(Transaccion transaccion) {
        try {
            this.pagos.generarTransaccion(transaccion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo actualizarEstadoTransaccion.
     * @param transaccion
     */
    public void actualizarEstadoTransaccion(Transaccion transaccion) {
        try {
            this.pagos.actualizarEstadoTransaccion(transaccion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo actualizarTransaccion.
     * @param transaccion
     */
    public void actualizarTransaccion(Transaccion transaccion) {
        try {
            this.pagos.actualizarTransaccion(transaccion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo obtenerDatosTransaccion.
     * @param transaccion
     * @return
     */
    public Transaccion obtenerDatosTransaccion(Transaccion transaccion) {
        try {
            return this.pagos.obtenerDatosTransaccion(transaccion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo aceptaTransaccion.
     * @throws BusinessException 
     */
    public boolean aceptaTransaccion(Transaccion transaccion)
        throws BusinessException {
        try {
            return this.pagos.aceptaTransaccion(transaccion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo pagarCencosud.
     * @param transaccion
     * @return
     * @throws BusinessException
     */
    public boolean pagarCencosud(Transaccion transaccion)
        throws BusinessException {
        try {
            return this.pagos.pagarCencosud(transaccion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo grabarDatosTransaccion.
     * @param parametros
     * @return
     */
    public long grabarDatosRespuesta(RespuestaTBK respuesta) {
        try {
            return this.pagos.grabarDatosRespuesta(respuesta);
        } catch (RemoteException e) {
        	logger.error("Error al grabar datos Pago", e);
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo obtenerDatosRespuesta.
     * @param idSolicitud
     * @return
     */
    public RespuestaTBK obtenerDatosRespuesta(long idSolicitud) {
        try {
            return this.pagos.obtenerDatosRespuesta(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
    /**
     * TODO Describir m�todo guardarNoRegistrado.
     * @param rutCliente, nombreCliente, mailCliente, tipoSeguro
     */
    public void guardarNoRegistrado(String rutCliente, String nombreCliente, String mailCliente, String tipoSeguro) {
        try {
            this.pagos.guardarNoRegistrado(rutCliente, nombreCliente, mailCliente, tipoSeguro);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
    /**
     * 
     * @param arg0
     * @param arg1
     * @return
     */
    public String obtenerParametroSistema(String arg0, String arg1){
        try {
            return this.pagos.obtenerParametroSistema(arg0, arg1);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
}
