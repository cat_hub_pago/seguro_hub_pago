package cl.tinet.common.config;

import java.util.Locale;

/**
 * Interfaz que deben cumplir las clases que deseen aprovecharse del esquema de
 * configuración y propagacion de configuracion.
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:05
 */
public interface Configurable {

    /**
     * Inicializador de los datos de configuracion de la instancia.
     * 
     * @param config    utilitario de configuracion.
     * @param locale    locale de configuracion.
     */
    public void configure(AbstractConfigurator config, Locale locale);

    /**
     * Inicializa los datos de configuración de la instancia.
     * 
     * @param configurable Objeto configurable desde donde se copia la
     * información de configuración.
     */
    public void configure(Configurable configurable);

    /**
     * Retorna la instancia de utilitario de configuracion utilizada.
     * @return utilitario de configuracion.
     */
    public AbstractConfigurator getConfigurator();

    /**
     * Retorna el locale de configuracion.
     * @return locale de configuracion.
     */
    public Locale getLocale();

    /**
     * Permite establecer el utilitario de configuracion de la instancia.
     * 
     * @param config    utilitario de configuracion.
     */
    public void setConfigurator(AbstractConfigurator config);

    /**
     * Permite establecer el locale de configuracion de la instancia.
     * 
     * @param locale    Locale de configuracion.
     */
    public void setLocale(Locale locale);
}
