package cl.tinet.common.dao.jdbc.connection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tinet.exceptions.system.SystemException;

/**
 * Connection pool for JDBC access.
 *
 * @author ?
 * @version 1.0
 * @created 27-Jul-2010 0:49:05
 */
public final class ConnectionPoolSynchronizer {

    /**
     * Referencia de acceso al log de la aplicaci�n.
     */
    private static Log logger =
        LogFactory.getLog(ConnectionPoolSynchronizer.class);

    /**
     * List of connections.
     */
    private List connectionPool = new ArrayList();

    /**
     * List de conexiones a eliminar.
     */
    private List toDelete = new Vector();

    /**
     * Indica si es que cada conexi�n debe ser testeada antes de ser retornada
     * desde el pool.
     */
    private boolean testConnections;

    /**
     * Indica la consulta SQL que se debe utilizar para testear las conexiones.
     */
    private String testSQL;

    /**
     * URL de conexi�n a la base de datos.
     */
    private String url;

    /**
     * propiedades de conexi�n a la base de datos.
     */
    private Properties properties;

    /**
     * Factory method for creating a new ConnectionPool instance.
     *
     * @param jdbcUrl JDBC Driver URL
     * @param jdbcUser Database USER
     * @param jdbcPw Database Password
     * @param size Number of connections to share
     * @return New ConnectionPool instance.
     * @throws SQLException en caso de un error creando el pool.
     */
    public static ConnectionPoolSynchronizer newInstance(final String jdbcUrl,
        final String jdbcUser, final String jdbcPw, final int size)
        throws SQLException {
        return newInstance(jdbcUrl, jdbcUser, jdbcPw, Collections.EMPTY_MAP,
            size);
    }

    /**
     * Factory method for creating a new ConnectionPool instance.
     *
     * @param jdbcUrl JDBC Driver URL
     * @param jdbcUser Database USER
     * @param jdbcPw Database Password
     * @param data aditional parameters.
     * @param size Number of connections to share
     * @return New ConnectionPool instance.
     * @throws SQLException en caso de un error creando el pool.
     */
    public static ConnectionPoolSynchronizer newInstance(final String jdbcUrl,
        final String jdbcUser, final String jdbcPw, final Map data,
        final int size) throws SQLException {
        return new ConnectionPoolSynchronizer(jdbcUrl, jdbcUser, jdbcPw, data,
            size);
    }

    /**
     * Private constructor for creating new ConnectionPool instances.
     *
     * @param jdbcUrl JDBC Driver URL
     * @param jdbcUser Database USER
     * @param jdbcPw Database Password
     * @param data aditional parameters.
     * @param size Number of connections to share
     * @throws SQLException en caso de un error creando el pool.
     */
    private ConnectionPoolSynchronizer(final String jdbcUrl,
        final String jdbcUser, final String jdbcPw, final Map data,
        final int size) throws SQLException {

        logger.debug("Inicializando pool de conexiones.");
        Properties prop = new Properties();
        prop.put("user", jdbcUser);
        prop.put("password", jdbcPw);

        // Se incorporan los datos especificados por argumentos.
        prop.putAll(data);

        // Se inicializan atributos de conexiones.
        this.url = jdbcUrl;
        this.properties = prop;
        logger.debug("Inicializando conexiones.");
        for (int i = 0; i < size; i++) {
            if (logger.isDebugEnabled()) {
                logger.debug("Creando conexi�n: " + (i + 1));
            }
            Connection connection = DriverManager.getConnection(jdbcUrl, prop);
            // create a dynamic proxy for the connection

            logger.debug("Creando proxy.");

            Object connectionProxy =
                Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                    new Class[] { Connection.class },
                    new ConnectionInvocationHandler(connection));
            connectionPool.add(connectionProxy);
        }
        logger.debug("Conexiones inicializadas.");
    }

    /**
     * Indica si se deben o no testear las conexiones antes de ser retornadas.
     * @return <code>true</code> en caso de que se deben testear las conexiones.
     */
    public boolean getTestConnections() {
        return testConnections;
    }

    /**
     * Permite establecer el flag de test de conexiones.
     * @param testConnections flag de testeo de conexiones.
     */
    public void setTestConnections(boolean testConnections) {
        this.testConnections = testConnections;
    }

    /**
     * Sentencia SQL de testeo de conexi�n.
     * @return Sentencia SQL.
     */
    public String getTestSQL() {
        return testSQL;
    }

    /**
     * Permite establecer la sentencia de testeo.
     * @param testSQL sentencia de testeo.
     */
    public void setTestSQL(String testSQL) {
        this.testSQL = testSQL;
    }

    /**
     * Blocking method to get a Connection object The Connection object that is
     * returned is a proxy object.
     *
     * @param waitTime
     *            how long to wait for a connection to become available
     * @return A proxy to a Connection object
     */
    public synchronized Connection getConnection(final long waitTime) {

        while (connectionPool.size() == 0) {
            logger.info("Waiting for a connection to become available");
            try {
                long start = System.currentTimeMillis();
                if (logger.isDebugEnabled()) {
                    logger.debug("Wait por una conexi�n: " + waitTime);
                }
                wait(waitTime);
                logger.debug("Wait finalizado.");

                long now = System.currentTimeMillis();

                if (start + waitTime <= now) {
                    logger.info("Wait timed out");
                    return null;
                }
            } catch (InterruptedException ex) {
                logger.info("Interrupted in wait() - returning null");
                return null;
            }
        }
        Connection connection = (Connection) connectionPool.remove(0);
        if (this.getTestConnections()) {
            connection = this.testConnection(connection);
        }
        if (logger.isInfoEnabled()) {
            logger.info("Returning connection - available connections="
                + connectionPool.size());
        }
        return connection;
    }

    /**
     * Permite realizar el testeo de la conexi�n especificada intentando
     * ejecutar la sentencia SQL de testeo especificada en el constructor.
     * Retorna la conexi�n original si el testeo fue exitoso y una nueva
     * conexi�n en caso contrario.
     *
     * @param connection conexi�n a testear.
     * @return conexi�n testeada si resulto exitoso el test. Nueva conexi�n
     *          en caso contrario.
     */
    private Connection testConnection(Connection connection) {
        Connection con = connection;
        logger.debug("La conexi�n sera testeada.");
        if (this.testSQL != null) {
            Statement statement = null;
            ResultSet rs = null;
            try {
                try {
                    logger.debug("Creando sentencia SQL.");
                    statement = connection.createStatement();
                    if (logger.isDebugEnabled()) {
                        logger.debug("Sentencia SQL creada. Ejecutando: "
                            + this.testSQL);
                    }
                    rs = statement.executeQuery(this.testSQL);
                    logger.debug("Sentencia ejecutada. conexi�n v�lida.");
                } finally {
                    this.close(statement, rs);
                    statement = null;
                }
            } catch (SQLException sqle) {
                con = handleError(connection, sqle);
            }
        } else {
            logger.warn("No se establecio SQL de testeo. Se omite test.");
        }
        logger.debug("testeo completado. Se continua con el flujo.");
        return con;
    }

    /**
     * Se encarga de manejar un error durante el testeo de la conexi�n.
     * Se elimina y crea una nueva conexi�n a la base de datos. En caso de
     * no lograr crear una neuva conexi�n, se lanza una excepcion.
     *
     * @param connection conexi�n que se desea eliminar.
     * @param throwable causa del problema.
     * @return nueva conexi�n a la base de datos.
     */
    private Connection handleError(Connection connection, Throwable throwable) {
        logger
            .warn("Error testeando conexi�n. Se creara una nueva.", throwable);
        try {
            try {
                Connection con = recreateConnection();
                this.close(connection);
                return con;
            } catch (SystemException se) {
                connection.close();
            }
        } catch (SQLException sqle) {
            logger.warn("Error cerrando conexi�n.", sqle);
        }
        throw new SystemException(throwable);
    }

    /**
     * Permite cerrar la conexi�n especificada. En caso de error, registra el
     * motivo en el log.
     *
     * @param connection conexi�n que se desea cerrar.
     */
    private void close(Connection connection) {
        if (connection != null) {
            this.toDelete.add(String.valueOf(connection.hashCode()));
            try {
                connection.close();
            } catch (SQLException sqle) {
                logger.warn("Problemas cerrando conexi�n.", sqle);
            } catch (SystemException se) {
                logger.warn("Problemas cerrando conexi�n.", se);
            } catch (UndeclaredThrowableException re) {
                logger.warn("Problemas cerrando conexi�n.", re);
            } finally {
                this.toDelete.remove(String.valueOf(connection.hashCode()));
            }

        }
    }

    /**
     * Permite cerrar el statement y el resultset especificado.
     * @param statement sentencia que se desea cerrar.
     * @param rs resultset a cerrar.
     */
    private void close(Statement statement, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqle) {
                logger.warn("Problemas cerrando resultset", sqle);
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException sqle) {
                logger.warn("Problemas cerrando sentencia.", sqle);
            }
        }
    }

    /**
     * Permite recrear la conexi�n a la base de datos en caso de detectar que
     * la conexi�n anterior ya no es v�lida.
     *
     * @return nueva conexi�n a la base de datos.
     */
    private Connection recreateConnection() {
        Connection connection = null;
        try {
            logger.debug("Recreando conexi�n a la base de datos.");
            connection = DriverManager.getConnection(this.url, this.properties);
            logger.debug("Creando proxy de conexi�n.");
            Object connectionProxy =
                Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                    new Class[] { Connection.class },
                    new ConnectionInvocationHandler(connection));
            logger.debug("Retornando nueva conexi�n.");
            connection = (Connection) connectionProxy;
        } catch (SQLException sqle) {
            logger.error("No se logr� recrear conexi�n a base de datos.", sqle);
            throw new SystemException(sqle);
        }
        return connection;
    }

    /**
     * Invoked from ConnectionInvocationHandler when close() is invoked
     * on the Connection proxy.
     * @param connection Connection proxy to put back into the connection pool
     */
    private synchronized void releaseConnection(final Connection connection) {
        connectionPool.add(connection);
        logger.info("Connection closed, adding to pool "
            + " - available connections=" + connectionPool.size());
        notify();
    }

    /**
     * Proxy para la clase {@link Connection} que permite realizar tareas
     * administrativas sobre el pool de conexiones.
     *
     * @author ?
     * @version 1.0
     * @created ?
     */
    private class ConnectionInvocationHandler implements InvocationHandler {

        /**
         * 'Real' connection object.
         */
        private Connection impl;

        /**
         * Create an invocation handler for the Connection object proxy.
         * @param con the 'real' Connection object
         */
        ConnectionInvocationHandler(final Connection con) {
            impl = con;
        }

        /**
         * Invoked when methods are called on the Connection proxy
         * All method invocation except close() are delegated to
         * the 'real' Connection object _impl.
         *
         * @param proxy the proxy the method was invoked on
         * @param method the method object being invoked
         * @param params parameters to be passed to the method object
         * @throws Throwable problems during proxy invocation.
         * @return  value from Method invocation
         */
        public Object invoke(final Object proxy, final Method method,
            final Object[] params) throws Throwable {

            // delegate all calls to the connection except close()
            // when close() is called on the connection it gets put back in the
            // pool
            if (method.getName().equals("close")) {
                if (!toDelete.contains(String.valueOf(proxy.hashCode()))) {
                    releaseConnection((Connection) proxy);
                } else {
                    method.invoke(impl, params);
                    logger.debug("Conexi�n invalida cerrada.");
                }
                return null;
            } else {
                return method.invoke(impl, params);
            }
        }
    }
}
