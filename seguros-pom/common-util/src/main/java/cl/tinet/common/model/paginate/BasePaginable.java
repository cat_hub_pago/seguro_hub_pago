package cl.tinet.common.model.paginate;

import java.io.Serializable;
import java.util.List;

/**
 * Clase base de todo objeto de dominio que necesita mantener informaci�n de
 * paginaci�n al ser listado.
 *
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:04
 */
public class BasePaginable implements Paginable, Serializable {

    /**
     * Versi�n de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 2L;

    /**
     * Datos asociados a la paginaci�n del listado.
     */
    private InfoPaginacion infoPaginacion;
    
    /**
     * Datos que se est�n paginando.
     */
    private List datosPaginados;

    /**
     * @return retorna el valor del atributo infoPaginacion
     */
    public InfoPaginacion getInfoPaginacion() {
        return infoPaginacion;
    }

    /**
     * @param infoPaginacion a establecer en el atributo infoPaginacion.
     */
    public void setInfoPaginacion(InfoPaginacion infoPaginacion) {
        this.infoPaginacion = infoPaginacion;
    }

    /**
     * @return datos que se est�n paginando. P�gina actual.
     */
    public List getDatosPaginados() {
        return datosPaginados;
    }

    /**
     * Permite establecer los datos de la pagina actual.
     * @param datosPaginados de la p�gina actual.
     */
    public void setDatosPaginados(List datosPaginados) {
        this.datosPaginados = datosPaginados;
    }
}
