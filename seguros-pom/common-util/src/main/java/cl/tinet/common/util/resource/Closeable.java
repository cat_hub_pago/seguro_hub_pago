package cl.tinet.common.util.resource;

/**
 * Interfaz com�n para clases que requieren que sus recursos internos sean
 * eliminados una vez la instancia deja de ser utilizada.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 19, 2010
 */
public interface Closeable {

    /**
     * M�todo que permite a la implementaci�n concreta cerrar los recursos
     * previamente utilizados.
     */
    void close();
}
