package cl.tinet.common.ssh;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

public class FileUploader {

    private String user = "";
    private String host = "";
    private String password = "";

    public FileUploader(String host, String user, String password) {
        this.user = user;
        this.host = host;
        this.password = password;
    }

    public boolean sendFile(String sourceFilename, String destFilename) {
        FileInputStream fis = null;
        Session session = null;

        try {
            JSch jsch = new JSch();
            //jsch.setKnownHosts("D:/sftpRoot/test");

            //String knownHostsFilename = "/home/sschulte/.ssh/known_hosts";
            //jsch.setKnownHosts(knownHostsFilename);

            // Create session
            session = jsch.getSession(this.user, this.host);
            session.setConfig("StrictHostKeyChecking", "no");

            UserInfo userInfo = new MyUserInfo();
            ((MyUserInfo) userInfo).setPassword(this.password);

            session.setUserInfo(userInfo);
            session.setPassword(this.password);

            // connect
            session.connect();

            String command = "scp -p -t " + destFilename;
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);

            OutputStream out = channel.getOutputStream();
            //InputStream in = channel.getInputStream();

            channel.connect();

            //send "C0644 filesize filename" where filename doesn't contain a /
            long filesize = (new File(sourceFilename)).length();

            System.out.println("tamanno de archivo: " + filesize);
           
            command = "C0644 " + filesize + " ";
            if (sourceFilename.lastIndexOf('/') > 0) {
                command +=
                    sourceFilename
                        .substring(sourceFilename.lastIndexOf('/') + 1);
            } else {
                command += sourceFilename;
            }

            command += "\n";

            out.write(command.getBytes());
            out.flush();

            //send the contents of the source file
            fis = new FileInputStream(sourceFilename);
            byte[] buf = new byte[1024];
            while (true) {
                int len = fis.read(buf, 0, buf.length);

                if (len <= 0) {
                    break;
                }

                out.write(buf, 0, len);
            }

            fis.close();
            fis = null;

            //send '\0' to end it
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();

            out.close();

            channel.disconnect();
            session.disconnect();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        return false;
    }

    public static class MyUserInfo implements UserInfo {
        public String password = null;

        /**
        *
        * @param password
        */
        public void setPassword(String password) {
            this.password = password;
        }

        public String getPassword() {
            return this.password;
        }

        public boolean promptYesNo(String str) {
            return true;
        }

        public String getPassphrase() {
            return null;
        }

        public boolean promptPassphrase(String message) {
            return true;
        }

        public void showMessage(String message) {

        }

        public boolean promptPassword(String message) {
            return true;
        }
    }
}
