package cl.tinet.common.model.sort;

import java.io.Serializable;
import java.text.MessageFormat;

/**
 * Encapsula la información relacionada con el ordenamiento de listados.
 *
 * @author Roberto San Martín
 * @version 1.0
 * @created 27-Jul-2010 0:49:06
 */
public class InfoOrden implements Serializable {

    /**
     * Valor predeterminado del atributo campo cuando no has sido definido
     * explícitamente con otro valor.
     */
    public static final String CAMPO_SIN_DEFINIR = "SIN_DEFINIR";

    /**
     * Valor del orden ascendente de elementos.
     */
    public static final int ORDEN_ASCENDENTE = 1;

    /**
     * Valor del orden descendente de elementos.
     */
    public static final int ORDEN_DESCENDENTE = 2;

    /**
     * Orden indefinido.
     */
    public static final int ORDEN_INDEFINIDO = 0;

    private static final long serialVersionUID = -2165843626444751830L;

    private static final String TO_STRING_PATTERN =
        "{0}:[campoOrden=''{1}'',tipoOrden=''{2}'']";

    private String campoOrden;

    private int tipoOrden;

    /**
     * Construye la instancia del objeto estableciendo el valor del atributo
     * campoOrden como {@link InfoOrden#CAMPO_SIN_DEFINIR} y el atributo
     * tipoOrden como {@link InfoOrden#ORDEN_INDEFINIDO}. 
     */
    public InfoOrden() {
        this(CAMPO_SIN_DEFINIR, ORDEN_INDEFINIDO);
    }

    /**
     * Construye la instancia del objeto con los valores de campo y tipo
     * especificados.
     *
     * @param campoOrden campo por el cual se desea ordenar.
     * @param tipoOrden tipo de orden a establecer.
     */
    public InfoOrden(final String campoOrden, final int tipoOrden) {
        super();
        this.campoOrden = campoOrden;
        this.tipoOrden = tipoOrden;
    }

    /**
     * @return retorna el valor del atributo campoOrden
     */
    public String getCampoOrden() {
        return campoOrden;
    }

    /**
     * @return retorna el valor del atributo tipoOrden
     */
    public int getTipoOrden() {
        return tipoOrden;
    }

    /**
     * Permite cambiar el tipo de ordenamiento. El tipo de ordenamiento va
     * rotando entre las opciones {@link InfoOrden#ORDEN_INDEFINIDO},
     * {@link InfoOrden#ORDEN_ASCENDENTE} y {@link InfoOrden#ORDEN_DESCENDENTE}
     * en ese mismo orden.
     */
    public void rotateTipoOrden() {
        this.tipoOrden++;
        if (this.tipoOrden > ORDEN_DESCENDENTE) {
            this.tipoOrden = ORDEN_INDEFINIDO;
        }
    }

    /**
     * @param campoOrden a establecer en el atributo campoOrden.
     */
    public void setCampoOrden(String campoOrden) {
        if (this.campoOrden == null) {
            throw new NullPointerException(
                "El capo de ordeamiento no debe ser null.");
        }
        this.campoOrden = campoOrden;
    }

    /**
     * @param tipoOrden a establecer en el atributo tipoOrden.
     */
    public void setTipoOrden(int tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    /**
     * Retorna la representación String del objeto.
     * @return representación String de la información de ordenamiento.
     */
    public String toString() {
        return MessageFormat.format(TO_STRING_PATTERN, new Object[] {
            InfoOrden.class.getName(), this.campoOrden,
            String.valueOf(this.tipoOrden) });
    }
}
