package cl.tinet.common.config;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Clase abstracta que implementa los m�todos necesarios para obtener la
 * configuraci�n de una aplicaci�n desde un resource bundle. La decisi�n de qu�
 * resource bundle utilizar queda en las subclases concretas.
 * 
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Jul-2010 0:49:02
 */
public abstract class AbstractConfigurator {

    /**
     * Atributo de log de la aplicaci�n.
     */
    private static Log logger = LogFactory.getLog(AbstractConfigurator.class);

    /**
     * Resource bundle asociado al configurador abstracto.
     */
    private Map bundleMap;

    /**
     * Locale predeterminado del configurador.
     */
    private Locale defaultLocale;

    /**
     * Constructor del configurador. Asume como locale predeterminado el locale
     * por defecto de la m�quina virtual.
     */
    protected AbstractConfigurator() {
        this(Locale.getDefault());
    }

    /**
     * Constructor del configurador. Recibe como argumento el locale
     * predeterminado del configurador.
     * 
     * @param locale informaci�n de localizaci�n predeterminada.
     */
    protected AbstractConfigurator(Locale locale) {
        this.defaultLocale = locale;
        this.bundleMap = new HashMap();
    }

    /**
     * M�todo abstracto que tiene la responsabilidad de decidir el resource
     * bundle que ser� utilzado por el configurador. DEBE SER IMPLEMENTADO POR
     * LA CLASE CONCRETA DE CONFIGURACION.
     * 
     * @return {@link ResourceBundle} asociado al locale especificado.
     * 
     * @param locale
     *            para el cual se desea obtener la configuracion.
     */
    public abstract ResourceBundle loadBundle(Locale locale);

    /**
     * Retorna el bundle correspondiente al locale especificado. Este m�todo
     * mantiene un cach� de resource bundles con el fin de evitar la recarga
     * innecesaria, sin embargo puede ser sobreescrito con el fin de modificar
     * este comportamiento.
     * 
     * @return {@link ResourceBundle} asociado al locale especificado.
     * 
     * @param locale
     *            para el cual se desea obtener la configuraci�n.
     */
    public ResourceBundle getBundle(final Locale locale) {
        if (!this.bundleMap.containsKey(locale)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Bundle no cargado previamente para locale "
                    + locale + ". Cargandolo.");
            }
            this.bundleMap.put(locale, loadBundle(locale));
        }
        return (ResourceBundle) this.bundleMap.get(locale);
    }

    /**
     * Retorna el String asociado a la llave especificada en el resource bundle
     * asociado al locale especificado.
     * 
     * @return Valor asociado a la llave especificada.
     * 
     * @param llave
     *            del string buscado.
     * @param locale
     *            asociado al bundle.
     */
    public String getString(final String llave, final Locale locale) {
        try {
            return this.getBundle(locale).getString(llave);
        } catch (MissingResourceException mre) {
            if (logger.isDebugEnabled()) {
                logger.trace("Llave no encontrada (" + llave
                    + "), se retorna null.");
            }
            return null;
        }
    }

    /**
     * Retorna el String asociado a la llave especificada en el resource bundle
     * asociado al locale por defecto.
     * 
     * @return Valor asociado a la llave especificada.
     * 
     * @param llave
     *            del string buscado.
     */
    public String getString(final String llave) {
        return this.getString(llave, this.defaultLocale);
    }

    /**
     * Retorna el byte asociado a la llave especificada en el resource bundle
     * asociado al locale especificado.
     * 
     * @return Valor byte asociado a la llave especificada.
     * 
     * @param llave
     *            del byte buscado.
     * @param locale
     *            asociado al bundle.
     */
    public byte getByte(final String llave, final Locale locale) {
        return Byte.parseByte(this.getString(llave, locale));
    }

    /**
     * Retorna el byte asociado a la llave especificada en el resource bundle
     * asociado al locale por defecto.
     * 
     * @return Valor byte asociado a la llave especificada.
     * 
     * @param llave
     *            del byte buscado.
     */
    public byte getByte(final String llave) {
        return Byte.parseByte(this.getString(llave));
    }

    /**
     * Retorna el int asociado a la llave especificada en el resource bundle
     * asociado al locale especificado.
     * 
     * @return Valor int asociado a la llave especificada.
     * 
     * @param llave
     *            del int buscado.
     * @param locale
     *            asociado al bundle.
     */
    public int getInt(final String llave, final Locale locale) {
        return Integer.parseInt(this.getString(llave, locale));
    }

    /**
     * Retorna el int asociado a la llave especificada en el resource bundle
     * asociado al locale por defecto.
     * 
     * @return Valor int asociado a la llave especificada.
     * 
     * @param llave
     *            del int buscado.
     */
    public int getInt(final String llave) {
        return Integer.parseInt(this.getString(llave));
    }

    /**
     * Retorna el long asociado a la llave especificada en el resource bundle
     * asociado al locale especificado.
     * 
     * @return Valor long asociado a la llave especificada.
     * 
     * @param llave
     *            del long buscado.
     * @param locale
     *            asociado al bundle.
     */
    public long getLong(final String llave, final Locale locale) {
        return Long.parseLong(this.getString(llave, locale));
    }

    /**
     * Retorna el long asociado a la llave especificada en el resource bundle
     * asociado al locale por defecto.
     * 
     * @return Valor long asociado a la llave especificada.
     * 
     * @param llave
     *            del long buscado.
     */
    public long getLong(final String llave) {
        return Long.parseLong(this.getString(llave));
    }

    /**
     * Retorna el float asociado a la llave especificada en el resource bundle
     * asociado al locale especificado.
     * 
     * @return Valor float asociado a la llave especificada.
     * 
     * @param llave
     *            del float buscado.
     * @param locale
     *            asociado al bundle.
     */
    public float getFloat(final String llave, final Locale locale) {
        return Float.parseFloat(this.getString(llave, locale));
    }

    /**
     * Retorna el float asociado a la llave especificada en el resource bundle
     * asociado al locale por defecto.
     * 
     * @return Valor float asociado a la llave especificada.
     * 
     * @param llave
     *            del float buscado.
     */
    public float getFloat(final String llave) {
        return Float.parseFloat(this.getString(llave));
    }

    /**
     * Retorna el double asociado a la llave especificada en el resource bundle
     * asociado al locale especificado.
     * 
     * @return Valor double asociado a la llave especificada.
     * 
     * @param llave
     *            del double buscado.
     * @param locale
     *            asociado al bundle.
     */
    public double getDouble(final String llave, final Locale locale) {
        return Double.parseDouble(this.getString(llave, locale));
    }

    /**
     * Retorna el double asociado a la llave especificada en el resource bundle
     * asociado al locale por defecto.
     * 
     * @return Valor double asociado a la llave especificada.
     * 
     * @param llave
     *            del double buscado.
     */
    public double getDouble(final String llave) {
        return Double.parseDouble(this.getString(llave));
    }

    /**
     * Retorna el boolean asociado a la llave especificada en el resource bundle
     * que corresponde al locale especificado.
     * 
     * @return el boolean asociado a la llave especificada.
     * 
     * @param llave
     *            del boolean buscado.
     * @param locale
     *            al cual est� asociado el bundle del configurador.
     */
    public boolean isTrue(final String llave, final Locale locale) {
        return Boolean.valueOf(this.getString(llave, locale)).booleanValue();
    }

    /**
     * Retorna el boolean asociado a la llave especificada en el resource bundle
     * que corresponde al locale por defecto.
     * 
     * @return el boolean asociado a la llave especificada.
     * 
     * @param llave
     *            del boolean buscado.
     */
    public boolean isTrue(final String llave) {
        return this.isTrue(llave, this.defaultLocale);
    }
}
