package cl.tinet.common.dao.jdbc.oracle;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import oracle.sql.TIMESTAMP;

import org.apache.commons.dbutils.BasicRowProcessor;

/**
 * Procesador de filas de {@link ResultSet} especializado para Oracle.
 * <p>
 * Permite manipular tipos de dato especializados de la base de
 * datos Oracle.
 * </p>
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 17, 2010
 */
public class OracleRowProcessor extends BasicRowProcessor {

    /**
     * Retorna un arreglo de objetos con los datos obtenidos desde el
     * {@link ResultSet} especificado.
     * <p>
     * Para obtener cada dato desde el {@link ResultSet} se utiliza el
     * m�todo {@link #extractObject(ResultSet, int)}.
     * </p>
     * @param rs origen de los datos del arreglo a retornar.
     * @return arreglo de objetos generado desde el {@link ResultSet}.
     * @throws SQLException en caso de ocurrir un error durante la
     * generaci�n del arreglo desde el {@link ResultSet}.
     */
    public Object[] toArray(ResultSet rs) throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        int cols = meta.getColumnCount();
        Object[] result = new Object[cols];
        for (int i = 0; i < cols; ++i) {
            result[i] = extractObject(rs, i);
        }
        return result;
    }

    /**
     * Retorna la representaci�n Map de la fila actual del {@link ResultSet}
     * especificado como argumento.
     * <p>
     * Para la obtenci�n del dato desde el {@link ResultSet} se utiliza el
     * m�todo {@link #extractObject(ResultSet, int)}.
     * </p>
     * @param rs
     * @return
     * @throws SQLException
     */
    public Map toMap(ResultSet rs) throws SQLException {
        Map result = new HashMap();
        ResultSetMetaData rsmd = rs.getMetaData();
        int cols = rsmd.getColumnCount();
        for (int i = 1; i <= cols; ++i) {
            String key = rsmd.getColumnName(i);
            Object value = extractObject(rs, i);
            result.put(key, value);
            result.put(key.toUpperCase(), value);
            result.put(key.toLowerCase(), value);
        }
        return result;
    }

    /**
     * Extrae el objeto en la columna i de la posici�n actual del
     * {@link ResultSet} especificado.
     * <p>En caso de que el tipo de dato corresponda a un dato no est�ndar de
     * la API JDBC de Oracle, se transforma a un dato est�ndar o transformable.
     * </p>
     * @param rs origen desde donde se obtendr� el dato.
     * @param i columna del dato a extraer.
     * @return dato extra�do.
     * @throws SQLException en caso de ocurrir un error durante la extracci�n.
     */
    protected Object extractObject(ResultSet rs, int i) throws SQLException {
        Object object = rs.getObject(i);
        if (object instanceof TIMESTAMP) {
            TIMESTAMP ts = (TIMESTAMP) object;
            object = ts.timestampValue();
        }
        return object;
    }
}