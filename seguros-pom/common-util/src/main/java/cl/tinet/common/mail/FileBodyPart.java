package cl.tinet.common.mail;

import java.io.File;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

public class FileBodyPart extends MimeBodyPart
{
  public FileBodyPart(String fileName)
    throws MessagingException
  {
    this(new File(fileName));
  }

  public FileBodyPart(File file) throws MessagingException {
    this(file, file.getName(), null);
  }

  public FileBodyPart(File file, String contentId) throws MessagingException {
    this(file, file.getName(), contentId);
  }

  public FileBodyPart(File file, String fileName, String contentId) throws MessagingException {
    super.setDataHandler(new DataHandler(new FileDataSource(file)));
    if (fileName != null)
      super.setFileName(fileName);
    if (contentId != null)
      super.setHeader("Content-ID", contentId);
  }
}
