package cl.tinet.common.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

public class HtmlBodyPart extends MimeBodyPart
{
  public HtmlBodyPart(String content)
    throws MessagingException
  {
    super.setContent(content, "text/html");
  }
}
