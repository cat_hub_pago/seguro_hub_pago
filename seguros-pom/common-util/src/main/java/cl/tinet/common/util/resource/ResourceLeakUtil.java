package cl.tinet.common.util.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tinet.exceptions.system.SystemException;

/**
 * Utilitario que centraliza las llamadas a cierres de recurso en
 * la aplicaci�n.
 * <p>
 * Permite cerrar recursos de diferentes tipos entre los que se cuentan:
 * <ul>
 * <li>{@link Closeable}</li>
 * <li>{@link InputStream}</li>
 * <li>{@link Reader}</li>
 * <li>{@link OutputStream}</li>
 * <li>{@link Writer}</li>
 * <li>{@link Context}</li>
 * <li>{@link Connection}</li>
 * <li>{@link Statement}</li>
 * <li>{@link ResultSet}</li>
 * </ul>
 * Tambi�n implementa la invocaci�n de un m�todo <code>close</code> sin
 * argumentos en un objeto de clase desconocida utilizando reflexi�n. En caso
 * de que el m�todo no exista en la instancia especificada o no pueda ser
 * invocado se lanza una {@link SystemException} indicando el motivo del
 * error en su interior.
 * </p>
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 12, 2010
 */
public class ResourceLeakUtil {

    /**
     * Listado de argumentos vac�o del m�todo close. Utilizado por
     * el m�todo {@link #close(Object)}.
     */
    private static final Class[] SIN_ARGUMENTOS = {};

    /**
     * Variable de acceso al log de la aplicaci�n.
     */
    private static Log logger = LogFactory.getLog(ResourceLeakUtil.class);

    /**
     * Constructor sin argumentos.
     * <p>
     * Se define privado para evitar instanciaci�n dado que los m�todos de
     * esta clase utilitaria son definidos como est�ticos.
     * </p>
     */
    private ResourceLeakUtil() {
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param closeable recurso que se desea cerrar.
     */
    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (SystemException se) {
                logger.error("Error cerrando recurso.", se);
            }
        } else {
            logger.debug("Recurso nulo. No se requiere cerrar.");
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param stream recurso que se desea cerrar.
     */
    public static void close(InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException ioe) {
                logger.error("Error cerrando recurso.", ioe);
            }
        } else {
            logger.debug("Recurso nulo. No se requiere cerrar.");
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param reader recurso que se desea cerrar.
     */
    public static void close(Reader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException ioe) {
                logger.error("Error cerrando recurso.", ioe);
            }
        } else {
            logger.debug("Recurso nulo. No se requiere cerrar.");
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param stream recurso que se desea cerrar.
     */
    public static void close(OutputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException ioe) {
                logger.error("Error cerrando recurso.", ioe);
            }
        } else {
            logger.debug("Recurso nulo. No se requiere cerrar.");
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param writer recurso que se desea cerrar.
     */
    public static void close(Writer writer) {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException ioe) {
                logger.error("Error cerrando recurso.", ioe);
            }
        } else {
            logger.debug("Recurso nulo. No se requiere cerrar.");
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param context recurso que se desea cerrar.
     */
    public static void close(Context context) {
        if (context != null) {
            try {
                context.close();
            } catch (NamingException ne) {
                logger.error("Error cerrando recurso.", ne);
            }
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param connection recurso que se desea cerrar.
     */
    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException sqle) {
                logger.error("Error cerrando recurso.", sqle);
            }
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param statement recurso que se desea cerrar.
     */
    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException sqle) {
                logger.error("Error cerrando recurso.", sqle);
            }
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * @param resultSet recurso que se desea cerrar.
     */
    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException sqle) {
                logger.error("Error cerrando recurso.", sqle);
            }
        }
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * <p>
     * El orden de cierre es:
     * <ol>
     * <li>{@link Statement}</li>
     * <li>{@link Connection}</li>
     * </ol>
     * </p>
     * @param statement recurso que se desea cerrar.
     * @param connection recurso que se desea cerrar.
     */
    public static void close(Statement statement, Connection connection) {
        close(statement);
        close(connection);
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * <p>
     * El orden de cierre es:
     * <ol>
     * <li>{@link ResultSet}</li>
     * <li>{@link Statement}</li>
     * <li>{@link Connection}</li>
     * </ol>
     * </p>
     * @param resultSet recurso que se desea cerrar.
     * @param statement recurso que se desea cerrar.
     * @param connection recurso que se desea cerrar.
     */
    public static void close(ResultSet resultSet, Statement statement,
        Connection connection) {
        close(resultSet);
        close(statement);
        close(connection);
    }

    /**
     * Permite cerrar el recurso especificado.
     * <p>
     * En caso de que el recurso especificado sea <code>null</code> no se
     * intenta realizar la operaci�n. En caso de que ocurra un error durante
     * el cierre del recurso, el problema se reporta al log del sistema.
     * </p>
     * <p>
     * El m�todo close se invoca mediante reflexi�n y en caso de cualquier
     * problema durante la invocaci�n, la ejecuci�n se aborta y se lanza
     * una {@link SystemException} con el motivo real del problema en su
     * interior.
     * </p>
     * @param closeable recurso que se desea cerrar.
     */
    public static void close(Object closeable) {
        if (closeable != null) {
            Class clase = closeable.getClass();
            try {
                Method method = clase.getMethod("close", SIN_ARGUMENTOS);
                method.invoke(closeable, SIN_ARGUMENTOS);
            } catch (SecurityException e) {
                logger.error("Error de seguridad cerrando recurso.", e);
                throw new SystemException(e);
            } catch (NoSuchMethodException e) {
                logger.error("Error cerrando recurso.", e);
                throw new SystemException(e);
            } catch (IllegalArgumentException e) {
                logger.error("Error cerrando recurso.", e);
                throw new SystemException(e);
            } catch (IllegalAccessException e) {
                logger.error("Error de acceso cerrando recurso.", e);
                throw new SystemException(e);
            } catch (InvocationTargetException e) {
                logger.error("Error cerrando recurso.", e);
                throw new SystemException(e);
            }
        } else {
            logger.debug("El objeto especificado es nulo.");
        }
    }
}
