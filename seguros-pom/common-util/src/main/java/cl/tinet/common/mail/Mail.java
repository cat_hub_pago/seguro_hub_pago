package cl.tinet.common.mail;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.mail.ByteArrayDataSource;

import sun.misc.BASE64Encoder;

/**
 * Clase Mail enagarda del env�o de correo. <br/>
 * 
 * @author ghost23
 * @version 1.0
 * @created 02/11/2010
 */
public class Mail {
	private String from;
	private String to;
	private String asunto;
	private String host;
	private String pass;
	private Properties configuraciones;
	private Session session;
	private MimeMultipart multiParte;

	/**
	 * Constructor de Mail.
	 * 
	 * @param from Direcci�n del originardor del correo
	 * @param to Direcci�n del destinatario del correo
	 * @param asunto Asunto del correo
	 * @param pass Password del usuario
	 */
	public Mail(String from, String to, String asunto, String pass, 
			String host) {
		this.from = from;
		this.to = to;
		this.host = host;
		this.pass = pass;

		Properties props = new Properties();
		props.put("mail.smtp.host", this.host);
		props.setProperty("mail.smtp.starttls.enable", "false");
		props.setProperty("mail.smtp.port", "25");
		props.setProperty("mail.smtp.user", this.to);
		props.setProperty("mail.smtp.auth", "true");
		this.configuraciones = props;
		session = Session.getDefaultInstance(configuraciones, null);
		multiParte = new MimeMultipart();
		this.asunto = asunto;
	}

	/**
	 * Constructor de Mail.
	 * 
	 * @param to Direcci�n del destinatario del correo
	 * @param pass Password del usuario
	 */
	public Mail(String to, String pass) {
		Properties props = new Properties();

		this.to = to;
		this.pass = pass;
		props.put("mail.smtp.host", this.host);
		props.setProperty("mail.smtp.starttls.enable", "false");
		props.setProperty("mail.smtp.port", "25");
		props.setProperty("mail.smtp.user", this.from);
		props.setProperty("mail.smtp.auth", "true");
		this.configuraciones = props;
		session = Session.getDefaultInstance(configuraciones, null);
		multiParte = new MimeMultipart();
	}

	/**
	 * Constructor de Mail.
	 * 
	 * @param to Direcci�n del destinatario del correo
	 */
	public Mail(String to) {
		Properties props = new Properties();

		this.to = to;
		this.pass = "";
		props.put("mail.smtp.host", this.host);
		props.setProperty("mail.smtp.starttls.enable", "false");
		props.setProperty("mail.smtp.port", "25");
		props.setProperty("mail.smtp.user", this.from);
		props.setProperty("mail.smtp.auth", "true");
		this.configuraciones = props;
		session = Session.getDefaultInstance(configuraciones, null);
		multiParte = new MimeMultipart();
	}

	/**
	 * M�todo addContenido, permite agregar un texto con formato 
	 * HTML al correo.
	 * 
	 * @param texto Descripci�n del correo
	 * @throws MessagingException 
	 * @throws UnsupportedEncodingException
	 */
	public void addContenido(String texto) throws MessagingException,
			UnsupportedEncodingException {
		BodyPart adj = new MimeBodyPart();
		adj.setText(texto);
		adj.addHeader("Content-Type", "text/html; charset=ISO-8859-1");
		multiParte.addBodyPart(adj);
	}

	/**
	 * M�todo addContenido.
	 * 
	 * @param archivo
	 * @param mimeType
	 * @param nombreArchivo
	 * @throws Exception
	 */
	public void addContenido(byte[] archivo, String mimeType,
			String nombreArchivo) throws Exception {
		BodyPart adj = new MimeBodyPart();
		ByteArrayDataSource ds = new ByteArrayDataSource(archivo, mimeType);
		adj.setDataHandler(new DataHandler(ds));
		adj.addHeader("Content-Transfer-Encoding", "base64");
		adj.addHeader("Content-Type", mimeType);
		adj.addHeader("Content-Disposition:", BodyPart.INLINE);
		adj.setFileName(nombreArchivo);
		multiParte.addBodyPart(adj);

	}

	/**
	 * M�todo sendMail.
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean sendMail() throws Exception {

		// Validar que todos los campos esten completos.
		if (this.asunto == null) {
			throw new Exception("Falta asunto");
		}
		if (this.host == null) {
			throw new Exception("Falta servidor");
		}
		if (this.to == null) {
			throw new Exception("Falta destinatario");
		}
		if (this.from == null) {
			throw new Exception("Falta origen");
		}

		MimeMessage message = new MimeMessage(session);
		message.addHeader("Content-Transfer-Encoding", "base64");
		message.setFrom(new InternetAddress(this.from));
		InternetAddress[] theAdresses = InternetAddress.parse(this.to);
		message.addRecipients(Message.RecipientType.TO, theAdresses);
		message.setSubject(asunto);
		message.setContent(multiParte);

		Transport t = session.getTransport("smtp");
		t.connect(this.host, "", "");
		t.sendMessage(message, message.getAllRecipients());
		t.close();

		return false;
	}

	/**
	 * M�todo encode.
	 * 
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public byte[] encode(byte[] b) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		OutputStream b64os = MimeUtility.encode(baos, "base64");
		BASE64Encoder b64 = new BASE64Encoder();

		b64os.write(b);
		b64os.close();
		return b64.encode(b).getBytes();
	}

	/**
	 * M�todo getBytesFromFile.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		long length = file.length();
		byte[] bytes = new byte[(int) length];
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = is.read(bytes, offset, 
						bytes.length - offset)) >= 0) {
			offset += numRead;
		}
		is.close();
		return bytes;
	}

	/**
	 * M�todo convertStreamToString.
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public String convertStreamToString(InputStream is) throws IOException {
		/*
		 * To convert the InputStream to String we use the Reader.read(char[]
		 * buffer) method. We iterate until the Reader return -1 which means
		 * there's no more data to read. We use the StringWriter class to
		 * produce the string.
		 */
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	/**
	 * @return retorna el valor del atributo from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from
	 *            a establecer en el atributo from.
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return retorna el valor del atributo to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to
	 *            a establecer en el atributo to.
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return retorna el valor del atributo host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host
	 *            a establecer en el atributo host.
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return retorna el valor del atributo asunto
	 */
	public String getAsunto() {
		return asunto;
	}

	/**
	 * @param asunto
	 *            a establecer en el atributo asunto.
	 */
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

}
