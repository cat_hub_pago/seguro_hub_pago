package cl.tinet.common.util.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.config.Configurable;

import com.tinet.exceptions.system.SystemException;

/**
 * Encapsula rutinas �tiles para cifrar y descifrar datos.
 *
 * @author Roberto San Mart�n.
 * @version 1.0
 * @created 27-Jul-2010 0:49:05
 */
public class CryptoUtil extends BaseConfigurable implements Configurable {

    /**
     * Llave de configuraci�n para b�squeda de la semilla de inicializaci�n
     * del algoritmo que se utilizar� en caso de no especificar una semilla.
     */
    public static final String DEFAULT_HASH_SALT_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.DEFAULT_HASH_SALT";

    /**
     * Llave de configuraci�n para b�squeda de base (decimal, hexadecimal,
     * octal, etc) de semilla de algoritmo de hash.
     */
    public static final String DEFAULT_HASH_SALT_RADIX_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.DEFAULT_HASH_SALT_RADIX";

    /**
     * Llave de configuraci�n para b�squeda del nombre del algoritmo de Hash
     * utilizado por el utilitario.
     */
    public static final String HASH_ALGORITHM_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.HASH_ALGORITHM";

    /**
     * Llave de configuraci�n para b�squeda de cantidad de iteraciones del
     * algoritmo de cifrado sobre el texto de entrada. 
     */
    public static final String HASH_ITER_NUMBER_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.HASH_ITER_NUMBER";

    /**
     * Llave de configuraci�n para b�squeda del set de caracteres
     * correspondiente al texto que ser� procesado.
     */
    public static final String HASH_TEXT_CHARSET_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.HASH_TEXT_CHARSET";

    /**
     * Llave de configuraci�n para b�squeda de la llave de encriptaci�n
     * predeterminada para la aplicaci�n.
     */
    public static final String DEFAULT_ENCRYPTION_SALT_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.DEFAULT_ENCRYPTION_SALT";

    /**
     * Llave de configuraci�n para b�squeda de la base en que se encuentran los
     * valores de la llave de encriptaci�n (8=octal, 10=decimal,
     * 16=hexadecimal, etc).
     */
    public static final String DEFAULT_ENCRYPTION_SALT_RADIX_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.DEFAULT_ENCRYPTION_SALT_RADIX";

    /**
     * Llave de configuraci�n para b�squeda del algoritmo de encriptaci�n.
     * Utilizada por los m�todos:
     * <ul>
     * <li>{@link #getEncrypted(String)}
     * <li>{@link #getEncrypted(String, byte[])}
     * <li>{@link #getDecrypted(byte[])}
     * <li>{@link #getDecrypted(byte[], byte[])}
     * </ul>
     */
    public static final String ENCRYPTION_ALGORITHM_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.ENCRYPTION_ALGORITHM";

    /**
     * TODO Describir atributo ENCRYPTION_TEXT_CHARSET_KEY.
     */
    private static final String ENCRYPTION_TEXT_CHARSET_KEY =
        "cl.tinet.common.util.crypto.CryptoUtil.ENCRYPTION_TEXT_CHARSET";

    /**
     * Constructor del utilitario de cifrado. Recibe un objeto
     * configuraci�n a trav�s del cual el utilitario se inicializa. Como objeto
     * {@link Locale} de configuraci�n se utiliza la localizaci�n predeterminada
     * de la m�quina virtual {@link Locale#getDefault()}.
     * 
     * @see CryptoUtil#CryptoUtil(AbstractConfigurator, Locale);
     *
     * @param config utilitario de configuraci�n para inicializaci�n.
     */
    public CryptoUtil(AbstractConfigurator config) {
        this(config, Locale.getDefault());
    }

    /**
     * Constructor del utilitario de cifrado. Recibe como argumentos el
     * utilitario de configuraci�n y la localizaci�n correspondiente.
     *
     * @param config utilitario de configuraci�n.
     * @param locale de configuraci�n establecida.
     */
    public CryptoUtil(AbstractConfigurator config, Locale locale) {
        this.configure(config, locale);
    }

    /**
     * Constructor del utilitario de cifrado. Recibe un objeto
     * configurable a trav�s del cual el utilitario se inicializa.
     *
     * @see Configurable#configure(Configurable);
     *
     * @param configurable utilizado para inicializar los utilitarios de
     *          configuraci�n.
     */
    public CryptoUtil(Configurable configurable) {
        this.configure(configurable);
    }

    /**
     * M�todo encargado de generar una semilla particular en base al texto
     * especificado.
     *
     * @param textSalt texto desde el que se obtendr� la nueva semilla.
     * @return valor de la nueva semilla para ese valor.
     */
    public byte[] generateHashSalt(String textSalt) {
        try {
            byte[] defaultSalt = this.getDefaultHashSalt();
            return mixDefaultSalt(textSalt, defaultSalt, getHashCharset());
        } catch (UnsupportedEncodingException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo generateEncryptionSalt.
     * @param textSalt
     * @return
     */
    public byte[] generateEncryptionSalt(String textSalt) {
        try {
            byte[] defaultSalt = this.getDefaultEncryptionSalt();
            return mixDefaultSalt(textSalt, defaultSalt, getEncryptionCharset());
        } catch (UnsupportedEncodingException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo mixDefaultSalt.
     * @param textSalt
     * @param defaultSalt
     * @return
     * @throws UnsupportedEncodingException
     */
    protected byte[] mixDefaultSalt(String textSalt, byte[] defaultSalt, String charset)
        throws UnsupportedEncodingException {
        byte[] saltTemp = textSalt.getBytes(charset);
        byte[] salt = new byte[defaultSalt.length];
        for (int i = 0; i < defaultSalt.length; i++) {
            salt[i] = (byte) (saltTemp[i % saltTemp.length] ^ defaultSalt[i]);
        }
        return salt;
    }

    /**
     * Retorna el CHARSET en base a la configuraci�n de la aplicaci�n. Busca
     * bajo la llave {@link CryptoUtil#HASH_TEXT_CHARSET_KEY} el valor de la
     * configuraci�n.
     *
     * @return String nombre del set de caracteres a utilizar.
     */
    public String getHashCharset() {
        return this.getConfigurator().getString(HASH_TEXT_CHARSET_KEY,
            this.getLocale());
    }

    /**
     * TODO Describir m�todo getEncryptionCharset.
     * @return
     */
    public String getEncryptionCharset() {
        return this.getConfigurator().getString(ENCRYPTION_TEXT_CHARSET_KEY,
            this.getLocale());
    }

    /**
     * Retorna la semilla de inicializaci�n de algoritmo predeterminada para la
     * aplicaci�n. El valor de la semilla es obtenida desde la configuraci�n de
     * la aplicaci�n.
     *
     * @return semilla de cifrado predeterminada.
     */
    protected byte[] getDefaultHashSalt() {
        Locale locale = this.getLocale();
        AbstractConfigurator config = this.getConfigurator();
        String[] data =
            config.getString(DEFAULT_HASH_SALT_KEY, locale).split(",\\s*");
        int radix = config.getInt(DEFAULT_HASH_SALT_RADIX_KEY, locale);
        byte[] salt = new byte[data.length];
        for (int i = 0; i < salt.length; i++) {
            salt[i] = Byte.parseByte(data[i], radix);
        }
        return salt;
    }

    /**
     * Retorna la semilla de cifrado/descifrado predeterminada para el
     * algoritmo sim�trico configurado del sistema.
     *
     * @return semilla de encriptaci�n predeterminada obtenida desde
     *          la configuraci�n del sistema.
     */
    protected byte[] getDefaultEncryptionSalt() {
        Locale locale = this.getLocale();
        AbstractConfigurator config = this.getConfigurator();
        String[] data =
            config.getString(DEFAULT_ENCRYPTION_SALT_KEY, locale)
                .split(",\\s*");
        int radix = config.getInt(DEFAULT_ENCRYPTION_SALT_RADIX_KEY, locale);
        byte[] salt = new byte[data.length];
        for (int i = 0; i < salt.length; i++) {
            salt[i] = (byte) Integer.parseInt(data[i], radix);
        }
        return salt;
    }

    /**
     * Retorna el resultado de la ejecuci�n del algoritmo de hashing sobre el
     * texto especificado y utilizando la semilla de inicializaci�n
     * predeterminada.
     *
     * @param text valor que se desea cifrar.
     * @return resultado de la operaci�n de hashing sobre el texto especificado.
     */
    public byte[] getHash(String text) {
        return this.getHash(text, this.getDefaultHashSalt());
    }

    /**
     * Realiza el proceso HASH configurado para la aplicaci�n sobre el texto
     * especificado y utilizando la semilla especificada.
     *
     * @param text Texto sobre el que se ejecutar� el algoritmo de hash.
     * @param salt Semilla de cifrado para proceso hash.
     * @return byte[] Resultado de la operaci�n de hash.
     */
    public byte[] getHash(String text, byte[] salt) {
        try {
            String hashAlgorithm =
                this.getConfigurator().getString(HASH_ALGORITHM_KEY,
                    this.getLocale());
            int itNumber =
                this.getConfigurator().getInt(HASH_ITER_NUMBER_KEY,
                    this.getLocale());
            MessageDigest digest = MessageDigest.getInstance(hashAlgorithm);
            digest.reset();
            digest.update(salt);
            byte[] input = digest.digest(text.getBytes(getHashCharset()));
            for (int i = 0; i < itNumber; i++) {
                digest.reset();
                input = digest.digest(input);
            }
            return input;
        } catch (NoSuchAlgorithmException e) {
            throw new SystemException(e);
        } catch (UnsupportedEncodingException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Se encarga de transformar a su representaci�n String el arreglo de bytes
     * especificado. Utiliza la tabla de conversi�n a valores hexadecimales para
     * cada byte del arreglo.
     * 
     * @param bytes arreglo de bytes que se desea representar en formato String.
     * @return Valor String representativo del valor del arreglo de bytes
     *          especificado.
     */
    public String toString(byte[] bytes) {
        if (bytes == null) {
            throw new NullPointerException(
                "Debe especificar el arreglo de bytes.");
        }
        return Hex.encodeHexString(bytes);
    }

    /**
     * Retorna el texto especificado cifrada utilizando el algoritmo
     * predeterminado para la aplicaci�n y la semilla de encriptaci�n
     * especificada como argumento.
     * 
     * @param text datos a cifrar.
     * @param salt semilla de cifrado a utilizar. Su longitud depende del
     *          algoritmo a utilizar.
     * @return datos cifrados.
     */
    public byte[] getEncrypted(String text, byte[] salt) {
        String algorithm =
            getConfigurator().getString(ENCRYPTION_ALGORITHM_KEY, getLocale());
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(salt, algorithm);
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            return cipher.doFinal(text.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new SystemException(e);
        } catch (NoSuchPaddingException e) {
            throw new SystemException(e);
        } catch (InvalidKeyException e) {
            throw new SystemException(e);
        } catch (IllegalBlockSizeException e) {
            throw new SystemException(e);
        } catch (BadPaddingException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Retorna el texto especificado cifrada utilizando el algoritmo
     * predeterminado para la aplicaci�n y la semilla de encriptaci�n
     * predeterminada como argumento.
     * 
     * @param text datos a cifrar.
     * @return datos cifrados.
     */
    public byte[] getEncrypted(String text) {
        return getEncrypted(text, getDefaultEncryptionSalt());
    }

    /**
     * Se encarga de descifrar los datos cifrados especificados utilizando el
     * algoritmo de cifrado predeterminado para la aplicaci�n y la semilla de
     * cifrado especificada por argumentos.
     * 
     * @param data datos cifrados que se desea descifrar.
     * @param salt semilla de cifrado a utilizar. La longitud de la semilla
     *          depende del algoritmo utilizado.
     * @return datos descifrados.
     */
    public String getDecrypted(byte[] data, byte[] salt) {
        String algorithm =
            getConfigurator().getString(ENCRYPTION_ALGORITHM_KEY, getLocale());
        SecretKeySpec skeySpec = new SecretKeySpec(salt, algorithm);
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            return new String(cipher.doFinal(data));
        } catch (NoSuchAlgorithmException e) {
            throw new SystemException(e);
        } catch (NoSuchPaddingException e) {
            throw new SystemException(e);
        } catch (InvalidKeyException e) {
            throw new SystemException(e);
        } catch (IllegalBlockSizeException e) {
            throw new SystemException(e);
        } catch (BadPaddingException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Se encarga de descifrar los datos cifrados especificados utilizando el
     * algoritmo de cifrado predeterminado para la aplicaci�n y la semilla de
     * cifrado predeterminada del sistema.
     * 
     * @param data datos cifrados que se desea descifrar.
     * @return datos descifrados.
     */
    public String getDecrypted(byte[] data) {
        return getDecrypted(data, getDefaultEncryptionSalt());
    }

    /**
     * Se encarga de transformar a su representaci�n String el arreglo de bytes
     * especificado. Utiliza una tabla de conversi�n a valores hexadecimales
     * para cada byte del arreglo.
     * 
     * @see Hex#encodeHex(byte[], boolean)
     * 
     * @param bytes
     *            arreglo de bytes que se desea representar en formato String.
     * @return Valor String hexadecimal representativo del valor del arreglo de
     *         bytes especificado.
     */
    public static String toHexString(byte[] bytes) {
        if (bytes == null) {
            throw new NullPointerException(
                "Debe especificar el arreglo de bytes.");
        }
        return String.valueOf(Hex.encodeHex(bytes, true));
    }

    /**
     * Retorna los datos binarios que corresponden a la cadena en hexadecimal
     * especificada.
     * <p>
     * En caso de que la cadena especificada sea <code>null</code> se lanza una
     * {@link NullPointerException} indicando el error.
     * </p>
     * 
     * @see Hex#decodeHex(char[])
     * 
     * @param hexString cadena en hexadecimal a decodificar.
     * @return datos decodificados desde cadena en hexadecimal.
     */
    public static byte[] fromHexString(String hexString) {
        if (hexString == null) {
            throw new NullPointerException(
                "Debe especificar el string en hexadecimal.");
        }
        try {
            return Hex.decodeHex(hexString.toCharArray());
        } catch (DecoderException de) {
            throw new SystemException(de);
        }
    }

    /**
     * Retorna los datos especificados codificados en Base64. Id�ntico a una
     * llamada a:
     * <pre>
     * toBase64String(datos, true);
     * </pre>
     * 
     * @see Base64#encodeBase64String(byte[])
     * @see http://es.wikipedia.org/wiki/Base64
     * 
     * @param bytes datos que se desea codificar.
     * @return datos codificados en base64 como cadena de texto.
     */
    public static String toBase64String(byte[] bytes) {
        return toBase64String(bytes, false);
    }

    /**
     * Retorna los datos especificados codificados en Base64.
     * 
     * @see Base64#encodeBase64String(byte[])
     * @see http://es.wikipedia.org/wiki/Base64
     * 
     * @param bytes datos que se desea codificar.
     * @param trim indicador que permite especificar si es que a la cadena
     *          generada se le deben eliminar los espacios y saltos de
     *          l�nea finales o no.
     * @return datos codificados en base64 como cadena de texto.
     */
    public static String toBase64String(byte[] bytes, boolean trim) {
        if (bytes == null) {
            throw new NullPointerException(
                "Debe especificar el arreglo de bytes.");
        }
        String code = Base64.encodeBase64String(bytes);
        if (trim) {
            code = code.trim();
        }
        return code;
    }

    /**
     * Decodifica el String especificado desde una cadena de texto
     * en codificaci�n base64.
     * <p>
     * En caso de que la cadena especificada sea <code>null</code> se
     * lanza una {@link NullPointerException}.
     * </p>
     * 
     * @see Base64#decodeBase64(String)
     * @see http://es.wikipedia.org/wiki/Base64
     * 
     * @param base64String texto en base64 a decodificar.
     * @return valor de bytes decodificados.
     */
    public static byte[] fromBase64String(String base64String) {
        if (base64String == null) {
            throw new NullPointerException(
                "Debe especificar el texto en base64.");
        }
        return Base64.decodeBase64(base64String);
    }
}
