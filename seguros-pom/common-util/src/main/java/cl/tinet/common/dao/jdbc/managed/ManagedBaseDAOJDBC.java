package cl.tinet.common.dao.jdbc.managed;

import cl.tinet.common.dao.jdbc.BaseDAOJDBC;
import cl.tinet.common.dao.jdbc.connection.ConnectionFactory;

/**
 * Clase base de cualquier clase DAO que acceda a la base de datos dentro de
 * un ambiente gestionado.
 *
 * @author Roberto San Martín
 * @version 1.0
 * @created Nov 4, 2008
 */
public class ManagedBaseDAOJDBC extends BaseDAOJDBC {

    /**
     * Connection factory del DAO JDBC.
     */
    private ConnectionFactory cf;

    /**
     * Retorna una instancia concreta de {@link ConnectionFactory} que permite
     * obtener conexión a la base de datos en un ambiente gestionado. Esta
     * implementación retorna una instancia concreta de
     * {@link ManagedConnectionFactory}.
     *
     * @return {@link ConnectionFactory} concreto para le ambiente gestionado.
     */
    public ConnectionFactory getConnectionFactory() {
        if (cf == null) {
            cf = new ManagedConnectionFactory(this);
        }
        return cf;
    }
}
