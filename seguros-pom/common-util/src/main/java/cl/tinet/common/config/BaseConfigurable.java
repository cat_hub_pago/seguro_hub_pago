package cl.tinet.common.config;

import java.util.Locale;

/**
 * Clase base de cualquier clase que desee implementar fácilmente la interfaz
 * {@link Configurable}.
 * 
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:03
 */
public abstract class BaseConfigurable implements Configurable {

    /**
     * Configurador abstracto del elemento configurable.
     */
    private AbstractConfigurator configurator;

    /**
     * Datos de localización del elemento configurable.
     */
    private Locale locale;

    /**
     * Inicializador de los datos de configuración de la instancia.
     * 
     * @param config
     *            utilitario de configuración.
     * @param locale
     *            detos de localización de configuración.
     */
    public void configure(AbstractConfigurator config, Locale locale) {
        this.configurator = config;
        this.locale = locale;
    }

    /**
     * Inicializador de los datos de configuración de la instancia.
     * 
     * @param configurable
     *            Objeto configurable desde donde se copia la información de
     *            configuración.
     */
    public void configure(Configurable configurable) {
        this.configurator = configurable.getConfigurator();
        this.locale = configurable.getLocale();
    }

    /**
     * Retorna la instancia de utilitario de configuración utilizada.
     * 
     * @return utilitario de configuración.
     */
    public AbstractConfigurator getConfigurator() {
        return this.configurator;
    }

    /**
     * Retorna el locale de configuración.
     * 
     * @return locale de configuración.
     */
    public Locale getLocale() {
        return this.locale;
    }

    /**
     * Permite establecer el utilitario de configuración de la instancia.
     * 
     * @param config
     *            utilitario de configuración.
     */
    public void setConfigurator(AbstractConfigurator config) {
        this.configurator = config;
    }

    /**
     * Permite establecer el locale de configuración de la instancia.
     * 
     * @param locale
     *            Locale de configuración.
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
