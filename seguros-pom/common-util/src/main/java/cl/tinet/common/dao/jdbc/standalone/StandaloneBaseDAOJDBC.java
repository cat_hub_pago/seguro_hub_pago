package cl.tinet.common.dao.jdbc.standalone;

import java.util.HashMap;
import java.util.Map;

import cl.tinet.common.dao.jdbc.BaseDAOJDBC;
import cl.tinet.common.dao.jdbc.connection.ConnectionFactory;

/**
 * Clase encargada de representar cualquier DAO concreto que requiera conectarse
 * a la base de datos en un ambiente no gestionado.
 * 
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Jul-2010 0:49:08
 */
public abstract class StandaloneBaseDAOJDBC extends BaseDAOJDBC {

    /**
     * Map de objetos {@link ConnectionFactory}, asociados bajo el nombre del
     * objeto de configuraci�n y el locale.
     */
    private static Map cfMap = new HashMap();

    /**
     * Se encarga de entragar la instancia concreta de connection factory que
     * debe utilizar el DAO para conectarse a la base de datos en una ambiente
     * no gestionado.
     *
     * @return {@link ConnectionFactory} a utilizar.
     */
    public ConnectionFactory getConnectionFactory() {
        String key = createKey();
        if (!cfMap.containsKey(key)) {
            inicializar(key);
        }
        return (ConnectionFactory) cfMap.get(key);
    }

    /**
     * Se encarga de crear la llave bajo la que se encuetra la instancia de
     * {@link ConnectionFactory} que le corresponde a la instancia concreta
     * de DAO que se desea conectar.
     *
     * @return Llave generadad en base a la clase de configuraci�n y al locale
     *          que le corresponde.
     */
    private String createKey() {
        return this.getConfigurator().getClass().getName() + "_"
            + this.getLocale();
    }

    /**
     * M�todo encargado de instanciar el objeto {@link ConnectionFactory} que
     * corresponde a ambiente no gestionados {@link StandaloneConnectionFactory}
     * e ingresarlo al Map de fabricas de conexiones.
     *
     * @param key llave de b�squeda en el map de {@link ConnectionFactory}.
     */
    private void inicializar(String key) {
        synchronized (cfMap) {
            if (!cfMap.containsKey(key)) {
                ConnectionFactory cf = new StandaloneConnectionFactory(this);
                cfMap.put(key, cf);
            }
        }
    }
}
