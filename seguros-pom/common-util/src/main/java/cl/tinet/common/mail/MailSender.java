package cl.tinet.common.mail;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class MailSender {
    private String host;

    private String emailFrom;

    private String emailTo;

    private String displayNameFrom;

    private String subject;

    private String textMsg;

    private String htmlMsg;

    private String content;

    private HashMap headers;

    public void setHeaders(HashMap headers) {
        this.headers = headers;
    }

    public void setServer(String server) {
        this.host = server;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMsg(String msg) {
        this.textMsg = msg;
    }

    public void setHtmlMsg(String htmlMsg) {
        this.htmlMsg = htmlMsg;
    }

    public void setFrom(String email, String displayName) {
        this.emailFrom = email;
        this.displayNameFrom = displayName;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public static MailSender getInstance() {
        return new MailSender();
    }

    public void send() throws MessagingException, UnsupportedEncodingException {

        this.setServer(host);
        this.setFrom(emailFrom, displayNameFrom);
        this.setSubject(subject);
        content = textMsg;
        this.setHeaders(headers);

        this.setHtmlMsg(content);
        // El subtipo multipart/alternative indica que cada parte es una versi�n
        // "alternativa" del mismo contenido (o similar), cada una en formatos
        // diferentes denotados por su encabezado "Content-Type". Los formatos
        // son ordenados atendiendo a cuan fieles son al original, con el menos
        // fiel al inicio. Los sistemas pueden escoger la "mejor" representaci�n
        // que ellos son capaces de procesar; en general esta ser� la �ltima
        // parte que el sistema entiende, a menos que otros factores puedan
        // afectar este comportamiento.
        Multipart mainMultipart = new MimeMultipart("alternative");
        BodyPart opcionalTextPart = new TextBodyPart(this.textMsg);
        BodyPart htmlPart = new HtmlBodyPart(this.htmlMsg);

        BodyPart compoundPart = new MimeBodyPart();
        Multipart relatedMultiPart = new MimeMultipart("related");
        relatedMultiPart.addBodyPart(htmlPart);

        compoundPart.setContent(relatedMultiPart);
        mainMultipart.addBodyPart(opcionalTextPart);
        mainMultipart.addBodyPart(compoundPart);
        Properties props = new Properties();

        Session session = Session.getDefaultInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        message.setSubject(this.subject);
        message.setFrom(new InternetAddress(this.emailFrom, this.displayNameFrom));

        String recipients[] = emailTo.split(";");

        InternetAddress[] addressTo = new InternetAddress[recipients.length];
        for (int i = 0; i < recipients.length; i++) {
            addressTo[i] = new InternetAddress(recipients[i]);
        }

        message.addRecipients(Message.RecipientType.TO, addressTo);
        message.setContent(mainMultipart);

        if (this.headers != null) {
            for (Iterator it = this.headers.keySet().iterator(); it.hasNext();) {
                String key = (String) it.next();
                String value = (String) this.headers.get(key);
                message.addHeader(key, value);
            }
        }
        message.saveChanges();
        Transport transport = session.getTransport("smtp");
        transport.connect(this.host, "", "");
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
    
    /*public void send() throws MessagingException, UnsupportedEncodingException {
        
       /* Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "SMTP.tinet.cl");
        props.setProperty("mail.user", "ricardo.treumun@tinet.cl");
        props.setProperty("mail.password", "hola123");
                */
       /*
        Properties props = new Properties();
        Session mailSession = Session.getDefaultInstance(props, null);
        mailSession.setDebug(true);
        

        MimeMessage message = new MimeMessage(mailSession);
        message.setSubject("HTML  mail with images");
        message.setFrom(new InternetAddress("tinet@tinet.cl"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress("ricardo.treumun@tinet.cl"));
        
        //
        // This HTML mail have to 2 part, the BODY and the embedded image
        //
        MimeMultipart multipart = new MimeMultipart("related");

        // first part  (the html)
        BodyPart messageBodyPart = new MimeBodyPart();
        String htmlText = "
     
            
       //String htmlText =this.textMsg;
        messageBodyPart.setContent(htmlText, "text/html");

     // add it
        multipart.addBodyPart(messageBodyPart);
        
        // second part (the image)
        messageBodyPart = new MimeBodyPart();
        DataSource fds = new FileDataSource ("C:\\mail\\mi-cotizacion.jpg");
        messageBodyPart.setDataHandler(new DataHandler(fds));
        messageBodyPart.setHeader("Content-ID","<image>");


     // add it
        multipart.addBodyPart(messageBodyPart);
 
        // put everything together
        message.setContent(multipart);
        message.saveChanges();
        Transport transport = mailSession.getTransport("smtp");
        transport.connect(this.host, "", "");
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();


    }*/
    
}
