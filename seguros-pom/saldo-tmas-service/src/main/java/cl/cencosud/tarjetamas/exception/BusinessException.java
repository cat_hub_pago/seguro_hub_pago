package cl.cencosud.tarjetamas.exception;
import java.util.HashMap;
import java.util.Map;

/**
 * Exception de Negocio
 * @author Reinaldo Alvarez
 * @version 1.0
 *
 */
public class BusinessException extends Exception{

	private static final long serialVersionUID = -6089295690637584275L;
	Map messageException;
	public String mensaje  = "error.generico";
	public String descripcion;

    public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public BusinessException()
    {
        messageException = new HashMap();
    }

    public BusinessException(String string) {
		mensaje = string;
	}

	public Map getMessageException()
    {
        return messageException;
    }

    public void setMessageException(Map messageException)
    {
        this.messageException = messageException;
    }

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
    
	public String toString(){
		return mensaje + " : " + descripcion;
	}
}
