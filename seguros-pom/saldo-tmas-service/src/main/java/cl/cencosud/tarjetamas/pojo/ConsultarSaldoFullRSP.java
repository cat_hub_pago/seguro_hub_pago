package cl.cencosud.tarjetamas.pojo;

import java.io.Serializable;

public class ConsultarSaldoFullRSP implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6250557431886782974L;
	
	private String indicadorError;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private String direccionParte1;
	private String direccionParte2;
	private String comuna;
	private String region;
	private String telefono;
	private String fechamodificacion1;
	
	private String direccionPart1EECC;
	private String direccionPart2EECC;
	private String ciudadEECC;
	private String comunaEECC;
	private String regionEECC;
	private String telefonoEECC;
	private String zipCodeEECC;
	private String rutaEnvioEECC;
	private String fechaModificacion1EECC;
	private String direccionEmailEECC;
	private String envioEECCEmail;
	
	private String direccionLaboral;
	private String comunaLaboral;
	private String regionLaboral;
	private String telefonoLaboral;
	private String fechaModificacion1Laboral;
	private String statusDireccionLaboral;
	
	private String pct;
	private String codigoBancoPac;
	private String descripcionBancoPac;
	private String tipoPac;
	private String numeroCuentaCorriente;
	
	private String fechaPac;
	private String fechaActivacionPac;
	private String fechaModificacionPac;
	private String fechaRenunciaPac;
	
	private String codigoRebajaCobroAdm;
	private String codigoBloqueo1;
	private String codigoBloqueo2;
	private String statusAutorizador;
	private String codigo3CPCRevoling;
	private String cicloDeFacturacion;
	private String numeroDeCliente;
	private String numeroDeRelacion;
	
	private String cupoComprasPesos;
	private String cupoComprasDolares;
	private String cupoPesosAutorizador;
	private String cupoDolarAutorizador;
	private String fechaNacimiento;
	private String sexo;
	private String estadoCivil;
	private String codigoVip;
	private String claseDeRiesgo;
	private String diasDeMora;
	private String fechaDeInicioMora;
	private String tramoMora1;
	private String tramoMora2;
	private String tramoMora3;
	private String tramoMora4;
	private String tramoMora5;
	private String tramoMora6;
	private String tramoMora7;
	private String tramoMora8;
	private String codigoClaseRiesgo;
	private String codigoDeCobranza;
	private String porcentajePagoMinimo;
	private String numeroDeCuenta;
	private String organizacion;
	private String logo;
	private String descripcionLogo;
	private String tipoDeCuenta;
	private String cupoDiferenciado;
	private String relacionCuenta;
	
	private String cupoAvancePesos;
	private String cupoAvanceDolar;
	private String cupoAvancePesosAutorizador;
	private String cupoAvanceDolarAutorizador;
	private String deudaPesos;
	private String deudaDolar;
	private String disponibleCompraPesos;
	private String disponibleAvancePesos;
	private String disponibleCompraDolar;
	private String disponibleAvanceDolar;
	private String disponibleCompraPesosAutorizador;
	private String disponibleAvancePesosAutorizador;
	private String disponibleCompraDolarAutorizador;
	private String disponibleAvanceDolarAutorizador;
	
	private String fechaUltimaFacturacionPesos;
	private String fechaUltimaFacturacionDolar;
	private String fechaVencimientoFacturacionPesos;
	private String fechaVencimientoFacturacionDolar;
	
	private String pagoMinimo;
	private String deudaFacturadaPesos;
	private String deudaFacturadaDolar;
	private String fechaUltimoPagoPesos;
	private String fechaUltimoPagoDolar;
	private String montoUltimoPagoPesos;
	private String montoUltimoPagoDolar;
	private String sucursal;
	private String codigoCobroDeAdministracion;
	private String montoCobroDeAdministracion;
	private String consumoPesos;
	private String consumoDolar;
	private String pctDolar;
	private String rutTitular;
	private String dvRutTitular;
	private String fechaBloqueo1;
	private String fechaBloqueo2;
	private String fechaApertura;
	private String cantidadDeTarjetas;
	private String fechaDeModificacionCuenta;
	private String pagoPesosDelCiclo;
	private String pagoDolarDelCiclo;
	private String creditosPesosDelCiclo;
	private String creditosDolarDelCiclo;
	private String debitosPesosDelCiclo;
	private String debitosDolarDelCiclo;
	private String creditosMemoPesosDelCiclo;
	private String creditosMemoDolarDelCiclo;
	private String debitosMemoPesosDelCiclo;
	private String debitosMemoDolarDelCiclo;
	private String cuentaTraspasoDeSaldo;
	private String cuentaCarteraCastigo;
	private String montoCarteraCastigo;
	private String celular;
	private String numeroDeAdicionales;
	private String horaDeBloqueo;
	private String diasDeMoraVisionPlus;
	private String diasDeMoraPermitidos;
	private String numeroInterno1;
	private String numeroInterno2;
	private String pctAlfaPesos;
	private String pctAlfaDolar;
	private String filler1;
	
	/* Datos Tarjeta */
	private String numeroTarjeta;
	private String statusTarjeta;
	private String codigoBloqueoTarjeta;
	private String statusAutorizadorTarjeta;
	private String pinoffSetTarjeta;
	private String codigoDocioTarjeta;
	private String codigoAfinidadTarjeta;
	
	private String cupoLimiteCreditoPesosTarjeta;
	private String cupoLimiteCreditoDolarTarjeta;
	private String cupoLimiteAvancesPesosTarjeta;
	private String cupoLimiteAvancesDolarTarjeta;
	
	private String cupoLimiteCreditoPesosAutorizadorTarjeta;
	private String cupoLimiteCreditoDolarAutorizadorTarjeta;
	private String cupoLimiteAvancesPesosAutorizadorTarjeta;
	private String cupoLimiteAvancesDolarAutorizadorTarjeta;
	
	private String fechaDeVencimientoActual;
	private String fechaDeVencimientoAnterior;
	
	private String flagDeActivacionTarjetaActual;
	private String flagDeActivacionTarjetaAnterior;
	
	private String accionTarjeta;
	private String tarjetasRequeridasTarjeta;
	
	private String fechaDeBloqueoTarjeta;
	private String fechaDeActivacionTarjeta;
	private String fechaDeAperturaTarjeta;
	private String fechaDeModificacionTarjeta;
	
	private String disponibleCompraPesosTarjeta;
	private String disponibleAvancePesosTarjeta;
	private String disponibleCompraDolarTarjeta;
	private String disponibleAvanceDolarTarjeta;
	private String disponibleCompraPesosAutorizadorTarjeta;
	private String disponibleAvancePesosAutorizadorTarjeta;
	private String disponibleCompraDolarAutorizadorTarjeta;
	private String disponibleAvanceDolarAutorizadorTarjeta;
	
	private String nombreTarjeta;
	private String nombreEmpresaTarjeta;
	private String memberSinceTarjeta;
	private String rutTarjeta;
	private String dvRutTarjeta;
	private String tipoClienteTarjeta;
	
	private String deudaPesosTarjeta;
	private String deudaDolarTarjeta;
	private String puntajeTarjeta;
	private String parentescoTarjeta;
	
	private String numeroTarjetaParis;
	private String numeroSerieRut;
	private String filler2;
	
	
	
	public String getAccionTarjeta() {
		return accionTarjeta;
	}
	public void setAccionTarjeta(String accionTarjeta) {
		this.accionTarjeta = accionTarjeta;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getCantidadDeTarjetas() {
		return cantidadDeTarjetas;
	}
	public void setCantidadDeTarjetas(String cantidadDeTarjetas) {
		this.cantidadDeTarjetas = cantidadDeTarjetas;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getCicloDeFacturacion() {
		return cicloDeFacturacion;
	}
	public void setCicloDeFacturacion(String cicloDeFacturacion) {
		this.cicloDeFacturacion = cicloDeFacturacion;
	}
	public String getCiudadEECC() {
		return ciudadEECC;
	}
	public void setCiudadEECC(String ciudadEECC) {
		this.ciudadEECC = ciudadEECC;
	}
	public String getClaseDeRiesgo() {
		return claseDeRiesgo;
	}
	public void setClaseDeRiesgo(String claseDeRiesgo) {
		this.claseDeRiesgo = claseDeRiesgo;
	}
	public String getCodigo3CPCRevoling() {
		return codigo3CPCRevoling;
	}
	public void setCodigo3CPCRevoling(String codigo3CPCRevoling) {
		this.codigo3CPCRevoling = codigo3CPCRevoling;
	}
	public String getCodigoAfinidadTarjeta() {
		return codigoAfinidadTarjeta;
	}
	public void setCodigoAfinidadTarjeta(String codigoAfinidadTarjeta) {
		this.codigoAfinidadTarjeta = codigoAfinidadTarjeta;
	}
	public String getCodigoBancoPac() {
		return codigoBancoPac;
	}
	public void setCodigoBancoPac(String codigoBancoPac) {
		this.codigoBancoPac = codigoBancoPac;
	}
	public String getCodigoBloqueo1() {
		return codigoBloqueo1;
	}
	public void setCodigoBloqueo1(String codigoBloqueo1) {
		this.codigoBloqueo1 = codigoBloqueo1;
	}
	public String getCodigoBloqueo2() {
		return codigoBloqueo2;
	}
	public void setCodigoBloqueo2(String codigoBloqueo2) {
		this.codigoBloqueo2 = codigoBloqueo2;
	}
	public String getCodigoBloqueoTarjeta() {
		return codigoBloqueoTarjeta;
	}
	public void setCodigoBloqueoTarjeta(String codigoBloqueoTarjeta) {
		this.codigoBloqueoTarjeta = codigoBloqueoTarjeta;
	}
	public String getCodigoClaseRiesgo() {
		return codigoClaseRiesgo;
	}
	public void setCodigoClaseRiesgo(String codigoClaseRiesgo) {
		this.codigoClaseRiesgo = codigoClaseRiesgo;
	}
	public String getCodigoCobroDeAdministracion() {
		return codigoCobroDeAdministracion;
	}
	public void setCodigoCobroDeAdministracion(String codigoCobroDeAdministracion) {
		this.codigoCobroDeAdministracion = codigoCobroDeAdministracion;
	}
	public String getCodigoDeCobranza() {
		return codigoDeCobranza;
	}
	public void setCodigoDeCobranza(String codigoDeCobranza) {
		this.codigoDeCobranza = codigoDeCobranza;
	}
	public String getCodigoDocioTarjeta() {
		return codigoDocioTarjeta;
	}
	public void setCodigoDocioTarjeta(String codigoDocioTarjeta) {
		this.codigoDocioTarjeta = codigoDocioTarjeta;
	}
	public String getCodigoRebajaCobroAdm() {
		return codigoRebajaCobroAdm;
	}
	public void setCodigoRebajaCobroAdm(String codigoRebajaCobroAdm) {
		this.codigoRebajaCobroAdm = codigoRebajaCobroAdm;
	}
	public String getCodigoVip() {
		return codigoVip;
	}
	public void setCodigoVip(String codigoVip) {
		this.codigoVip = codigoVip;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getComunaEECC() {
		return comunaEECC;
	}
	public void setComunaEECC(String comunaEECC) {
		this.comunaEECC = comunaEECC;
	}
	public String getComunaLaboral() {
		return comunaLaboral;
	}
	public void setComunaLaboral(String comunaLaboral) {
		this.comunaLaboral = comunaLaboral;
	}
	public String getConsumoDolar() {
		return consumoDolar;
	}
	public void setConsumoDolar(String consumoDolar) {
		this.consumoDolar = consumoDolar;
	}
	public String getConsumoPesos() {
		return consumoPesos;
	}
	public void setConsumoPesos(String consumoPesos) {
		this.consumoPesos = consumoPesos;
	}
	public String getCreditosDolarDelCiclo() {
		return creditosDolarDelCiclo;
	}
	public void setCreditosDolarDelCiclo(String creditosDolarDelCiclo) {
		this.creditosDolarDelCiclo = creditosDolarDelCiclo;
	}
	public String getCreditosMemoDolarDelCiclo() {
		return creditosMemoDolarDelCiclo;
	}
	public void setCreditosMemoDolarDelCiclo(String creditosMemoDolarDelCiclo) {
		this.creditosMemoDolarDelCiclo = creditosMemoDolarDelCiclo;
	}
	public String getCreditosMemoPesosDelCiclo() {
		return creditosMemoPesosDelCiclo;
	}
	public void setCreditosMemoPesosDelCiclo(String creditosMemoPesosDelCiclo) {
		this.creditosMemoPesosDelCiclo = creditosMemoPesosDelCiclo;
	}
	public String getCreditosPesosDelCiclo() {
		return creditosPesosDelCiclo;
	}
	public void setCreditosPesosDelCiclo(String creditosPesosDelCiclo) {
		this.creditosPesosDelCiclo = creditosPesosDelCiclo;
	}
	public String getCuentaCarteraCastigo() {
		return cuentaCarteraCastigo;
	}
	public void setCuentaCarteraCastigo(String cuentaCarteraCastigo) {
		this.cuentaCarteraCastigo = cuentaCarteraCastigo;
	}
	public String getCuentaTraspasoDeSaldo() {
		return cuentaTraspasoDeSaldo;
	}
	public void setCuentaTraspasoDeSaldo(String cuentaTraspasoDeSaldo) {
		this.cuentaTraspasoDeSaldo = cuentaTraspasoDeSaldo;
	}
	public String getCupoAvanceDolar() {
		return cupoAvanceDolar;
	}
	public void setCupoAvanceDolar(String cupoAvanceDolar) {
		this.cupoAvanceDolar = cupoAvanceDolar;
	}
	public String getCupoAvanceDolarAutorizador() {
		return cupoAvanceDolarAutorizador;
	}
	public void setCupoAvanceDolarAutorizador(String cupoAvanceDolarAutorizador) {
		this.cupoAvanceDolarAutorizador = cupoAvanceDolarAutorizador;
	}
	public String getCupoAvancePesos() {
		return cupoAvancePesos;
	}
	public void setCupoAvancePesos(String cupoAvancePesos) {
		this.cupoAvancePesos = cupoAvancePesos;
	}
	public String getCupoAvancePesosAutorizador() {
		return cupoAvancePesosAutorizador;
	}
	public void setCupoAvancePesosAutorizador(String cupoAvancePesosAutorizador) {
		this.cupoAvancePesosAutorizador = cupoAvancePesosAutorizador;
	}
	public String getCupoComprasDolares() {
		return cupoComprasDolares;
	}
	public void setCupoComprasDolares(String cupoComprasDolares) {
		this.cupoComprasDolares = cupoComprasDolares;
	}
	public String getCupoComprasPesos() {
		return cupoComprasPesos;
	}
	public void setCupoComprasPesos(String cupoComprasPesos) {
		this.cupoComprasPesos = cupoComprasPesos;
	}
	public String getCupoDiferenciado() {
		return cupoDiferenciado;
	}
	public void setCupoDiferenciado(String cupoDiferenciado) {
		this.cupoDiferenciado = cupoDiferenciado;
	}
	public String getCupoDolarAutorizador() {
		return cupoDolarAutorizador;
	}
	public void setCupoDolarAutorizador(String cupoDolarAutorizador) {
		this.cupoDolarAutorizador = cupoDolarAutorizador;
	}
	public String getCupoLimiteAvancesDolarAutorizadorTarjeta() {
		return cupoLimiteAvancesDolarAutorizadorTarjeta;
	}
	public void setCupoLimiteAvancesDolarAutorizadorTarjeta(
			String cupoLimiteAvancesDolarAutorizadorTarjeta) {
		this.cupoLimiteAvancesDolarAutorizadorTarjeta = cupoLimiteAvancesDolarAutorizadorTarjeta;
	}
	public String getCupoLimiteAvancesDolarTarjeta() {
		return cupoLimiteAvancesDolarTarjeta;
	}
	public void setCupoLimiteAvancesDolarTarjeta(
			String cupoLimiteAvancesDolarTarjeta) {
		this.cupoLimiteAvancesDolarTarjeta = cupoLimiteAvancesDolarTarjeta;
	}
	public String getCupoLimiteAvancesPesosAutorizadorTarjeta() {
		return cupoLimiteAvancesPesosAutorizadorTarjeta;
	}
	public void setCupoLimiteAvancesPesosAutorizadorTarjeta(
			String cupoLimiteAvancesPesosAutorizadorTarjeta) {
		this.cupoLimiteAvancesPesosAutorizadorTarjeta = cupoLimiteAvancesPesosAutorizadorTarjeta;
	}
	public String getCupoLimiteAvancesPesosTarjeta() {
		return cupoLimiteAvancesPesosTarjeta;
	}
	public void setCupoLimiteAvancesPesosTarjeta(
			String cupoLimiteAvancesPesosTarjeta) {
		this.cupoLimiteAvancesPesosTarjeta = cupoLimiteAvancesPesosTarjeta;
	}
	public String getCupoLimiteCreditoDolarAutorizadorTarjeta() {
		return cupoLimiteCreditoDolarAutorizadorTarjeta;
	}
	public void setCupoLimiteCreditoDolarAutorizadorTarjeta(
			String cupoLimiteCreditoDolarAutorizadorTarjeta) {
		this.cupoLimiteCreditoDolarAutorizadorTarjeta = cupoLimiteCreditoDolarAutorizadorTarjeta;
	}
	public String getCupoLimiteCreditoDolarTarjeta() {
		return cupoLimiteCreditoDolarTarjeta;
	}
	public void setCupoLimiteCreditoDolarTarjeta(
			String cupoLimiteCreditoDolarTarjeta) {
		this.cupoLimiteCreditoDolarTarjeta = cupoLimiteCreditoDolarTarjeta;
	}
	public String getCupoLimiteCreditoPesosAutorizadorTarjeta() {
		return cupoLimiteCreditoPesosAutorizadorTarjeta;
	}
	public void setCupoLimiteCreditoPesosAutorizadorTarjeta(
			String cupoLimiteCreditoPesosAutorizadorTarjeta) {
		this.cupoLimiteCreditoPesosAutorizadorTarjeta = cupoLimiteCreditoPesosAutorizadorTarjeta;
	}
	public String getCupoLimiteCreditoPesosTarjeta() {
		return cupoLimiteCreditoPesosTarjeta;
	}
	public void setCupoLimiteCreditoPesosTarjeta(
			String cupoLimiteCreditoPesosTarjeta) {
		this.cupoLimiteCreditoPesosTarjeta = cupoLimiteCreditoPesosTarjeta;
	}
	public String getCupoPesosAutorizador() {
		return cupoPesosAutorizador;
	}
	public void setCupoPesosAutorizador(String cupoPesosAutorizador) {
		this.cupoPesosAutorizador = cupoPesosAutorizador;
	}
	public String getDebitosDolarDelCiclo() {
		return debitosDolarDelCiclo;
	}
	public void setDebitosDolarDelCiclo(String debitosDolarDelCiclo) {
		this.debitosDolarDelCiclo = debitosDolarDelCiclo;
	}
	public String getDebitosMemoDolarDelCiclo() {
		return debitosMemoDolarDelCiclo;
	}
	public void setDebitosMemoDolarDelCiclo(String debitosMemoDolarDelCiclo) {
		this.debitosMemoDolarDelCiclo = debitosMemoDolarDelCiclo;
	}
	public String getDebitosMemoPesosDelCiclo() {
		return debitosMemoPesosDelCiclo;
	}
	public void setDebitosMemoPesosDelCiclo(String debitosMemoPesosDelCiclo) {
		this.debitosMemoPesosDelCiclo = debitosMemoPesosDelCiclo;
	}
	public String getDebitosPesosDelCiclo() {
		return debitosPesosDelCiclo;
	}
	public void setDebitosPesosDelCiclo(String debitosPesosDelCiclo) {
		this.debitosPesosDelCiclo = debitosPesosDelCiclo;
	}
	public String getDescripcionBancoPac() {
		return descripcionBancoPac;
	}
	public void setDescripcionBancoPac(String descripcionBancoPac) {
		this.descripcionBancoPac = descripcionBancoPac;
	}
	public String getDescripcionLogo() {
		return descripcionLogo;
	}
	public void setDescripcionLogo(String descripcionLogo) {
		this.descripcionLogo = descripcionLogo;
	}
	public String getDeudaDolar() {
		return deudaDolar;
	}
	public void setDeudaDolar(String deudaDolar) {
		this.deudaDolar = deudaDolar;
	}
	public String getDeudaDolarTarjeta() {
		return deudaDolarTarjeta;
	}
	public void setDeudaDolarTarjeta(String deudaDolarTarjeta) {
		this.deudaDolarTarjeta = deudaDolarTarjeta;
	}
	public String getDeudaFacturadaDolar() {
		return deudaFacturadaDolar;
	}
	public void setDeudaFacturadaDolar(String deudaFacturadaDolar) {
		this.deudaFacturadaDolar = deudaFacturadaDolar;
	}
	public String getDeudaFacturadaPesos() {
		return deudaFacturadaPesos;
	}
	public void setDeudaFacturadaPesos(String deudaFacturadaPesos) {
		this.deudaFacturadaPesos = deudaFacturadaPesos;
	}
	public String getDeudaPesos() {
		return deudaPesos;
	}
	public void setDeudaPesos(String deudaPesos) {
		this.deudaPesos = deudaPesos;
	}
	public String getDeudaPesosTarjeta() {
		return deudaPesosTarjeta;
	}
	public void setDeudaPesosTarjeta(String deudaPesosTarjeta) {
		this.deudaPesosTarjeta = deudaPesosTarjeta;
	}
	public String getDiasDeMora() {
		return diasDeMora;
	}
	public void setDiasDeMora(String diasDeMora) {
		this.diasDeMora = diasDeMora;
	}
	public String getDiasDeMoraPermitidos() {
		return diasDeMoraPermitidos;
	}
	public void setDiasDeMoraPermitidos(String diasDeMoraPermitidos) {
		this.diasDeMoraPermitidos = diasDeMoraPermitidos;
	}
	public String getDiasDeMoraVisionPlus() {
		return diasDeMoraVisionPlus;
	}
	public void setDiasDeMoraVisionPlus(String diasDeMoraVisionPlus) {
		this.diasDeMoraVisionPlus = diasDeMoraVisionPlus;
	}
	public String getDireccionEmailEECC() {
		return direccionEmailEECC;
	}
	public void setDireccionEmailEECC(String direccionEmailEECC) {
		this.direccionEmailEECC = direccionEmailEECC;
	}
	public String getDireccionLaboral() {
		return direccionLaboral;
	}
	public void setDireccionLaboral(String direccionLaboral) {
		this.direccionLaboral = direccionLaboral;
	}
	public String getDireccionPart1EECC() {
		return direccionPart1EECC;
	}
	public void setDireccionPart1EECC(String direccionPart1EECC) {
		this.direccionPart1EECC = direccionPart1EECC;
	}
	public String getDireccionPart2EECC() {
		return direccionPart2EECC;
	}
	public void setDireccionPart2EECC(String direccionPart2EECC) {
		this.direccionPart2EECC = direccionPart2EECC;
	}
	public String getDireccionParte1() {
		return direccionParte1;
	}
	public void setDireccionParte1(String direccionParte1) {
		this.direccionParte1 = direccionParte1;
	}
	public String getDireccionParte2() {
		return direccionParte2;
	}
	public void setDireccionParte2(String direccionParte2) {
		this.direccionParte2 = direccionParte2;
	}
	public String getDisponibleAvanceDolar() {
		return disponibleAvanceDolar;
	}
	public void setDisponibleAvanceDolar(String disponibleAvanceDolar) {
		this.disponibleAvanceDolar = disponibleAvanceDolar;
	}
	public String getDisponibleAvanceDolarAutorizador() {
		return disponibleAvanceDolarAutorizador;
	}
	public void setDisponibleAvanceDolarAutorizador(
			String disponibleAvanceDolarAutorizador) {
		this.disponibleAvanceDolarAutorizador = disponibleAvanceDolarAutorizador;
	}
	public String getDisponibleAvanceDolarAutorizadorTarjeta() {
		return disponibleAvanceDolarAutorizadorTarjeta;
	}
	public void setDisponibleAvanceDolarAutorizadorTarjeta(
			String disponibleAvanceDolarAutorizadorTarjeta) {
		this.disponibleAvanceDolarAutorizadorTarjeta = disponibleAvanceDolarAutorizadorTarjeta;
	}
	public String getDisponibleAvanceDolarTarjeta() {
		return disponibleAvanceDolarTarjeta;
	}
	public void setDisponibleAvanceDolarTarjeta(String disponibleAvanceDolarTarjeta) {
		this.disponibleAvanceDolarTarjeta = disponibleAvanceDolarTarjeta;
	}
	public String getDisponibleAvancePesos() {
		return disponibleAvancePesos;
	}
	public void setDisponibleAvancePesos(String disponibleAvancePesos) {
		this.disponibleAvancePesos = disponibleAvancePesos;
	}
	public String getDisponibleAvancePesosAutorizador() {
		return disponibleAvancePesosAutorizador;
	}
	public void setDisponibleAvancePesosAutorizador(
			String disponibleAvancePesosAutorizador) {
		this.disponibleAvancePesosAutorizador = disponibleAvancePesosAutorizador;
	}
	public String getDisponibleAvancePesosAutorizadorTarjeta() {
		return disponibleAvancePesosAutorizadorTarjeta;
	}
	public void setDisponibleAvancePesosAutorizadorTarjeta(
			String disponibleAvancePesosAutorizadorTarjeta) {
		this.disponibleAvancePesosAutorizadorTarjeta = disponibleAvancePesosAutorizadorTarjeta;
	}
	public String getDisponibleAvancePesosTarjeta() {
		return disponibleAvancePesosTarjeta;
	}
	public void setDisponibleAvancePesosTarjeta(String disponibleAvancePesosTarjeta) {
		this.disponibleAvancePesosTarjeta = disponibleAvancePesosTarjeta;
	}
	public String getDisponibleCompraDolar() {
		return disponibleCompraDolar;
	}
	public void setDisponibleCompraDolar(String disponibleCompraDolar) {
		this.disponibleCompraDolar = disponibleCompraDolar;
	}
	public String getDisponibleCompraDolarAutorizador() {
		return disponibleCompraDolarAutorizador;
	}
	public void setDisponibleCompraDolarAutorizador(
			String disponibleCompraDolarAutorizador) {
		this.disponibleCompraDolarAutorizador = disponibleCompraDolarAutorizador;
	}
	public String getDisponibleCompraDolarAutorizadorTarjeta() {
		return disponibleCompraDolarAutorizadorTarjeta;
	}
	public void setDisponibleCompraDolarAutorizadorTarjeta(
			String disponibleCompraDolarAutorizadorTarjeta) {
		this.disponibleCompraDolarAutorizadorTarjeta = disponibleCompraDolarAutorizadorTarjeta;
	}
	public String getDisponibleCompraDolarTarjeta() {
		return disponibleCompraDolarTarjeta;
	}
	public void setDisponibleCompraDolarTarjeta(String disponibleCompraDolarTarjeta) {
		this.disponibleCompraDolarTarjeta = disponibleCompraDolarTarjeta;
	}
	public String getDisponibleCompraPesos() {
		return disponibleCompraPesos;
	}
	public void setDisponibleCompraPesos(String disponibleCompraPesos) {
		this.disponibleCompraPesos = disponibleCompraPesos;
	}
	public String getDisponibleCompraPesosAutorizador() {
		return disponibleCompraPesosAutorizador;
	}
	public void setDisponibleCompraPesosAutorizador(
			String disponibleCompraPesosAutorizador) {
		this.disponibleCompraPesosAutorizador = disponibleCompraPesosAutorizador;
	}
	public String getDisponibleCompraPesosAutorizadorTarjeta() {
		return disponibleCompraPesosAutorizadorTarjeta;
	}
	public void setDisponibleCompraPesosAutorizadorTarjeta(
			String disponibleCompraPesosAutorizadorTarjeta) {
		this.disponibleCompraPesosAutorizadorTarjeta = disponibleCompraPesosAutorizadorTarjeta;
	}
	public String getDisponibleCompraPesosTarjeta() {
		return disponibleCompraPesosTarjeta;
	}
	public void setDisponibleCompraPesosTarjeta(String disponibleCompraPesosTarjeta) {
		this.disponibleCompraPesosTarjeta = disponibleCompraPesosTarjeta;
	}
	public String getDvRutTarjeta() {
		return dvRutTarjeta;
	}
	public void setDvRutTarjeta(String dvRutTarjeta) {
		this.dvRutTarjeta = dvRutTarjeta;
	}
	public String getDvRutTitular() {
		return dvRutTitular;
	}
	public void setDvRutTitular(String dvRutTitular) {
		this.dvRutTitular = dvRutTitular;
	}
	public String getEnvioEECCEmail() {
		return envioEECCEmail;
	}
	public void setEnvioEECCEmail(String envioEECCEmail) {
		this.envioEECCEmail = envioEECCEmail;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getFechaActivacionPac() {
		return fechaActivacionPac;
	}
	public void setFechaActivacionPac(String fechaActivacionPac) {
		this.fechaActivacionPac = fechaActivacionPac;
	}
	public String getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(String fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	public String getFechaBloqueo1() {
		return fechaBloqueo1;
	}
	public void setFechaBloqueo1(String fechaBloqueo1) {
		this.fechaBloqueo1 = fechaBloqueo1;
	}
	public String getFechaBloqueo2() {
		return fechaBloqueo2;
	}
	public void setFechaBloqueo2(String fechaBloqueo2) {
		this.fechaBloqueo2 = fechaBloqueo2;
	}
	public String getFechaDeActivacionTarjeta() {
		return fechaDeActivacionTarjeta;
	}
	public void setFechaDeActivacionTarjeta(String fechaDeActivacionTarjeta) {
		this.fechaDeActivacionTarjeta = fechaDeActivacionTarjeta;
	}
	public String getFechaDeAperturaTarjeta() {
		return fechaDeAperturaTarjeta;
	}
	public void setFechaDeAperturaTarjeta(String fechaDeAperturaTarjeta) {
		this.fechaDeAperturaTarjeta = fechaDeAperturaTarjeta;
	}
	public String getFechaDeBloqueoTarjeta() {
		return fechaDeBloqueoTarjeta;
	}
	public void setFechaDeBloqueoTarjeta(String fechaDeBloqueoTarjeta) {
		this.fechaDeBloqueoTarjeta = fechaDeBloqueoTarjeta;
	}
	public String getFechaDeInicioMora() {
		return fechaDeInicioMora;
	}
	public void setFechaDeInicioMora(String fechaDeInicioMora) {
		this.fechaDeInicioMora = fechaDeInicioMora;
	}
	public String getFechaDeModificacionCuenta() {
		return fechaDeModificacionCuenta;
	}
	public void setFechaDeModificacionCuenta(String fechaDeModificacionCuenta) {
		this.fechaDeModificacionCuenta = fechaDeModificacionCuenta;
	}
	public String getFechaDeModificacionTarjeta() {
		return fechaDeModificacionTarjeta;
	}
	public void setFechaDeModificacionTarjeta(String fechaDeModificacionTarjeta) {
		this.fechaDeModificacionTarjeta = fechaDeModificacionTarjeta;
	}
	public String getFechaDeVencimientoActual() {
		return fechaDeVencimientoActual;
	}
	public void setFechaDeVencimientoActual(String fechaDeVencimientoActual) {
		this.fechaDeVencimientoActual = fechaDeVencimientoActual;
	}
	public String getFechaDeVencimientoAnterior() {
		return fechaDeVencimientoAnterior;
	}
	public void setFechaDeVencimientoAnterior(String fechaDeVencimientoAnterior) {
		this.fechaDeVencimientoAnterior = fechaDeVencimientoAnterior;
	}
	public String getFechamodificacion1() {
		return fechamodificacion1;
	}
	public void setFechamodificacion1(String fechamodificacion1) {
		this.fechamodificacion1 = fechamodificacion1;
	}
	public String getFechaModificacion1EECC() {
		return fechaModificacion1EECC;
	}
	public void setFechaModificacion1EECC(String fechaModificacion1EECC) {
		this.fechaModificacion1EECC = fechaModificacion1EECC;
	}
	public String getFechaModificacion1Laboral() {
		return fechaModificacion1Laboral;
	}
	public void setFechaModificacion1Laboral(String fechaModificacion1Laboral) {
		this.fechaModificacion1Laboral = fechaModificacion1Laboral;
	}
	public String getFechaModificacionPac() {
		return fechaModificacionPac;
	}
	public void setFechaModificacionPac(String fechaModificacionPac) {
		this.fechaModificacionPac = fechaModificacionPac;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getFechaPac() {
		return fechaPac;
	}
	public void setFechaPac(String fechaPac) {
		this.fechaPac = fechaPac;
	}
	public String getFechaRenunciaPac() {
		return fechaRenunciaPac;
	}
	public void setFechaRenunciaPac(String fechaRenunciaPac) {
		this.fechaRenunciaPac = fechaRenunciaPac;
	}
	public String getFechaUltimaFacturacionDolar() {
		return fechaUltimaFacturacionDolar;
	}
	public void setFechaUltimaFacturacionDolar(String fechaUltimaFacturacionDolar) {
		this.fechaUltimaFacturacionDolar = fechaUltimaFacturacionDolar;
	}
	public String getFechaUltimaFacturacionPesos() {
		return fechaUltimaFacturacionPesos;
	}
	public void setFechaUltimaFacturacionPesos(String fechaUltimaFacturacionPesos) {
		this.fechaUltimaFacturacionPesos = fechaUltimaFacturacionPesos;
	}
	public String getFechaUltimoPagoDolar() {
		return fechaUltimoPagoDolar;
	}
	public void setFechaUltimoPagoDolar(String fechaUltimoPagoDolar) {
		this.fechaUltimoPagoDolar = fechaUltimoPagoDolar;
	}
	public String getFechaUltimoPagoPesos() {
		return fechaUltimoPagoPesos;
	}
	public void setFechaUltimoPagoPesos(String fechaUltimoPagoPesos) {
		this.fechaUltimoPagoPesos = fechaUltimoPagoPesos;
	}
	public String getFechaVencimientoFacturacionDolar() {
		return fechaVencimientoFacturacionDolar;
	}
	public void setFechaVencimientoFacturacionDolar(
			String fechaVencimientoFacturacionDolar) {
		this.fechaVencimientoFacturacionDolar = fechaVencimientoFacturacionDolar;
	}
	public String getFechaVencimientoFacturacionPesos() {
		return fechaVencimientoFacturacionPesos;
	}
	public void setFechaVencimientoFacturacionPesos(
			String fechaVencimientoFacturacionPesos) {
		this.fechaVencimientoFacturacionPesos = fechaVencimientoFacturacionPesos;
	}
	public String getFiller1() {
		return filler1;
	}
	public void setFiller1(String filler1) {
		this.filler1 = filler1;
	}
	public String getFiller2() {
		return filler2;
	}
	public void setFiller2(String filler2) {
		this.filler2 = filler2;
	}
	public String getFlagDeActivacionTarjetaActual() {
		return flagDeActivacionTarjetaActual;
	}
	public void setFlagDeActivacionTarjetaActual(
			String flagDeActivacionTarjetaActual) {
		this.flagDeActivacionTarjetaActual = flagDeActivacionTarjetaActual;
	}
	public String getFlagDeActivacionTarjetaAnterior() {
		return flagDeActivacionTarjetaAnterior;
	}
	public void setFlagDeActivacionTarjetaAnterior(
			String flagDeActivacionTarjetaAnterior) {
		this.flagDeActivacionTarjetaAnterior = flagDeActivacionTarjetaAnterior;
	}
	public String getHoraDeBloqueo() {
		return horaDeBloqueo;
	}
	public void setHoraDeBloqueo(String horaDeBloqueo) {
		this.horaDeBloqueo = horaDeBloqueo;
	}
	public String getIndicadorError() {
		return indicadorError;
	}
	public void setIndicadorError(String indicadorError) {
		this.indicadorError = indicadorError;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getMemberSinceTarjeta() {
		return memberSinceTarjeta;
	}
	public void setMemberSinceTarjeta(String memberSinceTarjeta) {
		this.memberSinceTarjeta = memberSinceTarjeta;
	}
	public String getMontoCarteraCastigo() {
		return montoCarteraCastigo;
	}
	public void setMontoCarteraCastigo(String montoCarteraCastigo) {
		this.montoCarteraCastigo = montoCarteraCastigo;
	}
	public String getMontoCobroDeAdministracion() {
		return montoCobroDeAdministracion;
	}
	public void setMontoCobroDeAdministracion(String montoCobroDeAdministracion) {
		this.montoCobroDeAdministracion = montoCobroDeAdministracion;
	}
	public String getMontoUltimoPagoDolar() {
		return montoUltimoPagoDolar;
	}
	public void setMontoUltimoPagoDolar(String montoUltimoPagoDolar) {
		this.montoUltimoPagoDolar = montoUltimoPagoDolar;
	}
	public String getMontoUltimoPagoPesos() {
		return montoUltimoPagoPesos;
	}
	public void setMontoUltimoPagoPesos(String montoUltimoPagoPesos) {
		this.montoUltimoPagoPesos = montoUltimoPagoPesos;
	}
	public String getNombreEmpresaTarjeta() {
		return nombreEmpresaTarjeta;
	}
	public void setNombreEmpresaTarjeta(String nombreEmpresaTarjeta) {
		this.nombreEmpresaTarjeta = nombreEmpresaTarjeta;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getNombreTarjeta() {
		return nombreTarjeta;
	}
	public void setNombreTarjeta(String nombreTarjeta) {
		this.nombreTarjeta = nombreTarjeta;
	}
	public String getNumeroCuentaCorriente() {
		return numeroCuentaCorriente;
	}
	public void setNumeroCuentaCorriente(String numeroCuentaCorriente) {
		this.numeroCuentaCorriente = numeroCuentaCorriente;
	}
	public String getNumeroDeAdicionales() {
		return numeroDeAdicionales;
	}
	public void setNumeroDeAdicionales(String numeroDeAdicionales) {
		this.numeroDeAdicionales = numeroDeAdicionales;
	}
	public String getNumeroDeCliente() {
		return numeroDeCliente;
	}
	public void setNumeroDeCliente(String numeroDeCliente) {
		this.numeroDeCliente = numeroDeCliente;
	}
	public String getNumeroDeCuenta() {
		return numeroDeCuenta;
	}
	public void setNumeroDeCuenta(String numeroDeCuenta) {
		this.numeroDeCuenta = numeroDeCuenta;
	}
	public String getNumeroDeRelacion() {
		return numeroDeRelacion;
	}
	public void setNumeroDeRelacion(String numeroDeRelacion) {
		this.numeroDeRelacion = numeroDeRelacion;
	}
	public String getNumeroInterno1() {
		return numeroInterno1;
	}
	public void setNumeroInterno1(String numeroInterno1) {
		this.numeroInterno1 = numeroInterno1;
	}
	public String getNumeroInterno2() {
		return numeroInterno2;
	}
	public void setNumeroInterno2(String numeroInterno2) {
		this.numeroInterno2 = numeroInterno2;
	}
	public String getNumeroSerieRut() {
		return numeroSerieRut;
	}
	public void setNumeroSerieRut(String numeroSerieRut) {
		this.numeroSerieRut = numeroSerieRut;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getNumeroTarjetaParis() {
		return numeroTarjetaParis;
	}
	public void setNumeroTarjetaParis(String numeroTarjetaParis) {
		this.numeroTarjetaParis = numeroTarjetaParis;
	}
	public String getOrganizacion() {
		return organizacion;
	}
	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}
	public String getPagoDolarDelCiclo() {
		return pagoDolarDelCiclo;
	}
	public void setPagoDolarDelCiclo(String pagoDolarDelCiclo) {
		this.pagoDolarDelCiclo = pagoDolarDelCiclo;
	}
	public String getPagoMinimo() {
		return pagoMinimo;
	}
	public void setPagoMinimo(String pagoMinimo) {
		this.pagoMinimo = pagoMinimo;
	}
	public String getPagoPesosDelCiclo() {
		return pagoPesosDelCiclo;
	}
	public void setPagoPesosDelCiclo(String pagoPesosDelCiclo) {
		this.pagoPesosDelCiclo = pagoPesosDelCiclo;
	}
	public String getParentescoTarjeta() {
		return parentescoTarjeta;
	}
	public void setParentescoTarjeta(String parentescoTarjeta) {
		this.parentescoTarjeta = parentescoTarjeta;
	}
	public String getPct() {
		return pct;
	}
	public void setPct(String pct) {
		this.pct = pct;
	}
	public String getPctAlfaDolar() {
		return pctAlfaDolar;
	}
	public void setPctAlfaDolar(String pctAlfaDolar) {
		this.pctAlfaDolar = pctAlfaDolar;
	}
	public String getPctAlfaPesos() {
		return pctAlfaPesos;
	}
	public void setPctAlfaPesos(String pctAlfaPesos) {
		this.pctAlfaPesos = pctAlfaPesos;
	}
	public String getPctDolar() {
		return pctDolar;
	}
	public void setPctDolar(String pctDolar) {
		this.pctDolar = pctDolar;
	}
	public String getPinoffSetTarjeta() {
		return pinoffSetTarjeta;
	}
	public void setPinoffSetTarjeta(String pinoffSetTarjeta) {
		this.pinoffSetTarjeta = pinoffSetTarjeta;
	}
	public String getPorcentajePagoMinimo() {
		return porcentajePagoMinimo;
	}
	public void setPorcentajePagoMinimo(String porcentajePagoMinimo) {
		this.porcentajePagoMinimo = porcentajePagoMinimo;
	}
	public String getPuntajeTarjeta() {
		return puntajeTarjeta;
	}
	public void setPuntajeTarjeta(String puntajeTarjeta) {
		this.puntajeTarjeta = puntajeTarjeta;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getRegionEECC() {
		return regionEECC;
	}
	public void setRegionEECC(String regionEECC) {
		this.regionEECC = regionEECC;
	}
	public String getRegionLaboral() {
		return regionLaboral;
	}
	public void setRegionLaboral(String regionLaboral) {
		this.regionLaboral = regionLaboral;
	}
	public String getRelacionCuenta() {
		return relacionCuenta;
	}
	public void setRelacionCuenta(String relacionCuenta) {
		this.relacionCuenta = relacionCuenta;
	}
	public String getRutaEnvioEECC() {
		return rutaEnvioEECC;
	}
	public void setRutaEnvioEECC(String rutaEnvioEECC) {
		this.rutaEnvioEECC = rutaEnvioEECC;
	}
	public String getRutTarjeta() {
		return rutTarjeta;
	}
	public void setRutTarjeta(String rutTarjeta) {
		this.rutTarjeta = rutTarjeta;
	}
	public String getRutTitular() {
		return rutTitular;
	}
	public void setRutTitular(String rutTitular) {
		this.rutTitular = rutTitular;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getStatusAutorizador() {
		return statusAutorizador;
	}
	public void setStatusAutorizador(String statusAutorizador) {
		this.statusAutorizador = statusAutorizador;
	}
	public String getStatusAutorizadorTarjeta() {
		return statusAutorizadorTarjeta;
	}
	public void setStatusAutorizadorTarjeta(String statusAutorizadorTarjeta) {
		this.statusAutorizadorTarjeta = statusAutorizadorTarjeta;
	}
	public String getStatusDireccionLaboral() {
		return statusDireccionLaboral;
	}
	public void setStatusDireccionLaboral(String statusDireccionLaboral) {
		this.statusDireccionLaboral = statusDireccionLaboral;
	}
	public String getStatusTarjeta() {
		return statusTarjeta;
	}
	public void setStatusTarjeta(String statusTarjeta) {
		this.statusTarjeta = statusTarjeta;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getTarjetasRequeridasTarjeta() {
		return tarjetasRequeridasTarjeta;
	}
	public void setTarjetasRequeridasTarjeta(String tarjetasRequeridasTarjeta) {
		this.tarjetasRequeridasTarjeta = tarjetasRequeridasTarjeta;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTelefonoEECC() {
		return telefonoEECC;
	}
	public void setTelefonoEECC(String telefonoEECC) {
		this.telefonoEECC = telefonoEECC;
	}
	public String getTelefonoLaboral() {
		return telefonoLaboral;
	}
	public void setTelefonoLaboral(String telefonoLaboral) {
		this.telefonoLaboral = telefonoLaboral;
	}
	public String getTipoClienteTarjeta() {
		return tipoClienteTarjeta;
	}
	public void setTipoClienteTarjeta(String tipoClienteTarjeta) {
		this.tipoClienteTarjeta = tipoClienteTarjeta;
	}
	public String getTipoDeCuenta() {
		return tipoDeCuenta;
	}
	public void setTipoDeCuenta(String tipoDeCuenta) {
		this.tipoDeCuenta = tipoDeCuenta;
	}
	public String getTipoPac() {
		return tipoPac;
	}
	public void setTipoPac(String tipoPac) {
		this.tipoPac = tipoPac;
	}
	public String getTramoMora1() {
		return tramoMora1;
	}
	public void setTramoMora1(String tramoMora1) {
		this.tramoMora1 = tramoMora1;
	}
	public String getTramoMora2() {
		return tramoMora2;
	}
	public void setTramoMora2(String tramoMora2) {
		this.tramoMora2 = tramoMora2;
	}
	public String getTramoMora3() {
		return tramoMora3;
	}
	public void setTramoMora3(String tramoMora3) {
		this.tramoMora3 = tramoMora3;
	}
	public String getTramoMora4() {
		return tramoMora4;
	}
	public void setTramoMora4(String tramoMora4) {
		this.tramoMora4 = tramoMora4;
	}
	public String getTramoMora5() {
		return tramoMora5;
	}
	public void setTramoMora5(String tramoMora5) {
		this.tramoMora5 = tramoMora5;
	}
	public String getTramoMora6() {
		return tramoMora6;
	}
	public void setTramoMora6(String tramoMora6) {
		this.tramoMora6 = tramoMora6;
	}
	public String getTramoMora7() {
		return tramoMora7;
	}
	public void setTramoMora7(String tramoMora7) {
		this.tramoMora7 = tramoMora7;
	}
	public String getTramoMora8() {
		return tramoMora8;
	}
	public void setTramoMora8(String tramoMora8) {
		this.tramoMora8 = tramoMora8;
	}
	public String getZipCodeEECC() {
		return zipCodeEECC;
	}
	public void setZipCodeEECC(String zipCodeEECC) {
		this.zipCodeEECC = zipCodeEECC;
	}

}
