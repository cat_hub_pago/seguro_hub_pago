package cl.cencosud.tarjetamas.pojo;

import java.io.Serializable;

public class ConsultarSaldoFullREQ implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1186234340042924023L;
	
	private String canal;
   	private String organizacion;
   	private String numeroCuenta;
   	private String numeroSecuencia;
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getNumeroSecuencia() {
		return numeroSecuencia;
	}
	public void setNumeroSecuencia(String numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}
	public String getOrganizacion() {
		return organizacion;
	}
	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	} 

}
