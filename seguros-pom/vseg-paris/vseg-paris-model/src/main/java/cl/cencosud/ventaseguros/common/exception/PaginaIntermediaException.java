package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

public class PaginaIntermediaException extends BusinessException {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 6866160122064948343L;

    public static final String SUBCATEGORIA_EXISTENTE =
        "cl.cencosud.ventaseguros.common.exception.SUBCATEGORIA_EXISTENTE";

    public PaginaIntermediaException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }
}
