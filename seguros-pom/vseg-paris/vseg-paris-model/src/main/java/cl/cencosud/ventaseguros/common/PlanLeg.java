package cl.cencosud.ventaseguros.common;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import cl.cencosud.ventaseguros.comparador.model.Caracteristica;

public class PlanLeg implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -8000269898109945450L;
    private Long id_plan;
    private String codigo_tipo_prod_leg;
    private String codigo_sub_tipo_prod_leg;
    private String codigo_producto_leg;
    private String codigo_plan_leg;
    private String nombre;
    private Integer id_pais;
    private Integer es_activo;
    private String compannia;
    private Date fecha_creacion;
    private Date fecha_modificacion;
    private Long id_plan_vseg;
    private String codigo_cia_leg;

    /**
     * Identificador que indica si a plan le corresponde
     * inspeccion vehicular.
     */
    private int inspeccionVehi;
    /**
     * Identificador que indica el dia maximo de activacion
     * del seguro.
     */
    private int maxDiaIniVig;

    private Integer solicitaAutoNuevo;
    private Integer solicitaFactura;
    private String formaPago;
    
    private String nombre_cia_leg;
    private List<Caracteristica> listCaracteristica;
    

    /**
     * @return retorna el valor del atributo id_plan
     */
    public Long getId_plan() {
        return id_plan;
    }

    /**
     * @param id_plan a establecer en el atributo id_plan.
     */
    public void setId_plan(Long id_plan) {
        this.id_plan = id_plan;
    }

    /**
     * @return retorna el valor del atributo codigo_tipo_prod_leg
     */
    public String getCodigo_tipo_prod_leg() {
        return codigo_tipo_prod_leg;
    }

    /**
     * @param codigo_tipo_prod_leg a establecer en el atributo codigo_tipo_prod_leg.
     */
    public void setCodigo_tipo_prod_leg(String codigo_tipo_prod_leg) {
        this.codigo_tipo_prod_leg = codigo_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_sub_tipo_prod_leg
     */
    public String getCodigo_sub_tipo_prod_leg() {
        return codigo_sub_tipo_prod_leg;
    }

    /**
     * @param codigo_sub_tipo_prod_leg a establecer en el atributo codigo_sub_tipo_prod_leg.
     */
    public void setCodigo_sub_tipo_prod_leg(String codigo_sub_tipo_prod_leg) {
        this.codigo_sub_tipo_prod_leg = codigo_sub_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_producto_leg
     */
    public String getCodigo_producto_leg() {
        return codigo_producto_leg;
    }

    /**
     * @param codigo_producto_leg a establecer en el atributo codigo_producto_leg.
     */
    public void setCodigo_producto_leg(String codigo_producto_leg) {
        this.codigo_producto_leg = codigo_producto_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_plan_leg
     */
    public String getCodigo_plan_leg() {
        return codigo_plan_leg;
    }

    /**
     * @param codigo_plan_leg a establecer en el atributo codigo_plan_leg.
     */
    public void setCodigo_plan_leg(String codigo_plan_leg) {
        this.codigo_plan_leg = codigo_plan_leg;
    }

    /**
     * @return retorna el valor del atributo nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre a establecer en el atributo nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public Integer getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(Integer id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo es_activo
     */
    public Integer getEs_activo() {
        return es_activo;
    }

    /**
     * @param es_activo a establecer en el atributo es_activo.
     */
    public void setEs_activo(Integer es_activo) {
        this.es_activo = es_activo;
    }

    /**
     * @return retorna el valor del atributo compannia
     */
    public String getCompannia() {
        return compannia;
    }

    /**
     * @param compannia a establecer en el atributo compannia.
     */
    public void setCompannia(String compannia) {
        this.compannia = compannia;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    /**
     * @return retorna el valor del atributo inspeccionVehi
     */
    public int getInspeccionVehi() {
        return inspeccionVehi;
    }

    /**
     * @param inspeccionVehi a establecer en el atributo inspeccionVehi.
     */
    public void setInspeccionVehi(int inspeccionVehi) {
        this.inspeccionVehi = inspeccionVehi;
    }

    /**
     * @return retorna el valor del atributo maxDiaIniVig
     */
    public int getMaxDiaIniVig() {
        return maxDiaIniVig;
    }

    /**
     * @param maxDiaIniVig a establecer en el atributo maxDiaIniVig.
     */
    public void setMaxDiaIniVig(int maxDiaIniVig) {
        this.maxDiaIniVig = maxDiaIniVig;
    }

    /**
     * @return retorna el valor del atributo id_plan_vseg
     */
    public Long getId_plan_vseg() {
        return id_plan_vseg;
    }

    /**
     * @param id_plan_vseg a establecer en el atributo id_plan_vseg.
     */
    public void setId_plan_vseg(Long id_plan_vseg) {
        this.id_plan_vseg = id_plan_vseg;
    }

    /**
     * @return retorna el valor del atributo solicitaAutoNuevo
     */
    public Integer getSolicitaAutoNuevo() {
        return solicitaAutoNuevo;
    }

    /**
     * @param solicitaAutoNuevo a establecer en el atributo solicitaAutoNuevo.
     */
    public void setSolicitaAutoNuevo(Integer solicitaAutoNuevo) {
        this.solicitaAutoNuevo = solicitaAutoNuevo;
    }

    /**
     * @return retorna el valor del atributo solicitaFactura
     */
    public Integer getSolicitaFactura() {
        return solicitaFactura;
    }

    /**
     * @param solicitaFactura a establecer en el atributo solicitaFactura.
     */
    public void setSolicitaFactura(Integer solicitaFactura) {
        this.solicitaFactura = solicitaFactura;
    }

    /**
     * @return retorna el valor del atributo formaPago
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * @param formaPago a establecer en el atributo formaPago.
     */
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    /**
     * @return retorna el valor del atributo codigo_cia_leg
     */
    public String getCodigo_cia_leg() {
        return codigo_cia_leg;
    }

    /**
     * @param codigo_cia_leg a establecer en el atributo codigo_cia_leg.
     */
    public void setCodigo_cia_leg(String codigo_cia_leg) {
        this.codigo_cia_leg = codigo_cia_leg;
    }

	public String getNombre_cia_leg() {
		return nombre_cia_leg;
	}

	public void setNombre_cia_leg(String nombreCiaLeg) {
		nombre_cia_leg = nombreCiaLeg;
	}

	public List<Caracteristica> getListCaracteristica() {
		return listCaracteristica;
	}

	public void setListCaracteristica(List<Caracteristica> listCaracteristica) {
		this.listCaracteristica = listCaracteristica;
	}

}
