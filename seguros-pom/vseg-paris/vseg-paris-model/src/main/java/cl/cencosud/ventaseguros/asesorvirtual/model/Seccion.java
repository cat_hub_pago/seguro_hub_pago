package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;

/**
 * Secciones de preguntas y respuestas.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
//@XStreamAlias("seccion")
public class Seccion implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 3418235047979424370L;

    /**
     * Identificador de la seccion.
     */
    //@XStreamAlias("id")
    //@XStreamAsAttribute
    private int id;

    /**
     * Titulo de la seccion.
     */
    //@XStreamAlias("titulo")
    //@XStreamAsAttribute
    private String titulo;

    /**
     * Preguntas asociadas a la seccion.
     */
    //@XStreamAlias("preguntas")
    private Preguntas preguntas;

    /**
     * Respuestas de las preguntas.
     */
    //@XStreamAlias("respuestas")
    private Respuestas respuestas;

    /**
     * @return retorna el valor del atributo id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id a establecer en el atributo id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return retorna el valor del atributo titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo a establecer en el atributo titulo.
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return retorna el valor del atributo preguntas
     */
    public Preguntas getPreguntas() {
        return preguntas;
    }

    /**
     * @param preguntas a establecer en el atributo preguntas.
     */
    public void setPreguntas(Preguntas preguntas) {
        this.preguntas = preguntas;
    }

    /**
     * @return retorna el valor del atributo respuestas
     */
    public Respuestas getRespuestas() {
        return respuestas;
    }

    /**
     * @param respuestas a establecer en el atributo respuestas.
     */
    public void setRespuestas(Respuestas respuestas) {
        this.respuestas = respuestas;
    }

}
