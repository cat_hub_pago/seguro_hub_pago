package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

/**
 * En caso de no se pueda encontrar el usuario.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/09/2010
 */
public class NodoNoEliminableException extends BusinessException {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -4927203197871156733L;

    public static final String PREGUNTA_NO_ELIMINABLE =
        "cl.cencosud.ventaseguros.common.exception.PREGUNTA_NO_ELIMINABLE";

    public static final String SECCION_NO_ELIMINABLE =
        "cl.cencosud.ventaseguros.common.exception.SECCION_NO_ELIMINABLE";

    /**
     * constructor.
     * @param messageKey llave del mensaje.
     * @param arguments argumentos del mensaje.
     */
    public NodoNoEliminableException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    /**
     * loadConfigurator.
     * @return configuracion.
     */
    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}
