package cl.cencosud.ventaseguros.informescomerciales;

import java.io.Serializable;
import java.util.Date;

public class InformeComercial implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6583703882404706653L;

    private long idseguro;

    private String numeropoliza;

    private String nombrecliente;

    private String fonocliente;

    private String emailcliente;

    private Date fechaseguro;

    private String rutcliente;

    private String nombreproducto;

    private String nemotecnico;

    private double primauf;

    private double proporcional;

    private String tipotarjeta;

    public long getIdseguro() {
        return idseguro;
    }

    public void setIdseguro(long idseguro) {
        this.idseguro = idseguro;
    }

    public String getNumeropoliza() {
        return numeropoliza;
    }

    public void setNumeropoliza(String numeropoliza) {
        this.numeropoliza = numeropoliza;
    }

    public String getNombrecliente() {
        return nombrecliente;
    }

    public void setNombrecliente(String nombrecliente) {
        this.nombrecliente = nombrecliente;
    }

    public String getFonocliente() {
        return fonocliente;
    }

    public void setFonocliente(String fonocliente) {
        this.fonocliente = fonocliente;
    }

    public String getEmailcliente() {
        return emailcliente;
    }

    public void setEmailcliente(String emailcliente) {
        this.emailcliente = emailcliente;
    }

    public Date getFechaseguro() {
        return fechaseguro;
    }

    public void setFechaseguro(Date fechaseguro) {
        this.fechaseguro = fechaseguro;
    }

    public String getRutcliente() {
        return rutcliente;
    }

    public void setRutcliente(String rutcliente) {
        this.rutcliente = rutcliente;
    }

    public String getNombreproducto() {
        return nombreproducto;
    }

    public void setNombreproducto(String nombreproducto) {
        this.nombreproducto = nombreproducto;
    }

    public String getNemotecnico() {
        return nemotecnico;
    }

    public void setNemotecnico(String nemotecnico) {
        this.nemotecnico = nemotecnico;
    }

    public double getPrimauf() {
        return primauf;
    }

    public void setPrimauf(double primauf) {
        this.primauf = primauf;
    }

    public double getProporcional() {
        return proporcional;
    }

    public void setProporcional(double proporcional) {
        this.proporcional = proporcional;
    }

    public String getTipotarjeta() {
        return tipotarjeta;
    }

    public void setTipotarjeta(String tipotarjeta) {
        this.tipotarjeta = tipotarjeta;
    }

}
