package cl.cencosud.ventaseguros.common.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * Clase utilizada para obtener elementos desde un archivo propertie.
 * <br/>
 * @author Miguel Cornejo V. (TInet)
 * @version 1.0
 * @created 22-06-2011
 */
public class VSPSQLConfig extends AbstractConfigurator {

	/**
     * TODO Describir atributo SQL_CONFIG_BASE_NAME.
     */
    public static final String SQL_CONFIG_BASE_NAME =
        "VSPSQLConfig";
    
    private static VSPSQLConfig instance = new VSPSQLConfig();
    
    public static VSPSQLConfig getInstance() {
    	return instance;
    }
    
	@Override
	public ResourceBundle loadBundle(Locale locale) {
		return ResourceBundle.getBundle(SQL_CONFIG_BASE_NAME, locale);
	}

}
