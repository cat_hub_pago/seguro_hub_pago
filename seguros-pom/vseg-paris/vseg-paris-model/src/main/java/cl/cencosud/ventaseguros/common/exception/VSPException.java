package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

public abstract class VSPException extends BusinessException {

    public VSPException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    public VSPException(BusinessException[] exceptions) {
        super(exceptions);
    }

    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}