package cl.cencosud.ventaseguros.common;

import java.io.Serializable;

public class LegalFicha implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2213698735650439380L;

	private Long id_ficha;
	private String texto_legal;
	private String activo;
	
	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getId_ficha() {
		return id_ficha;
	}

	public void setId_ficha(Long id_ficha) {
		this.id_ficha = id_ficha;
	}
	
	public String getTexto_legal() {
		return texto_legal;
	}

	public void setTexto_legal(String texto_legal) {
		this.texto_legal = texto_legal;
	}	
}
