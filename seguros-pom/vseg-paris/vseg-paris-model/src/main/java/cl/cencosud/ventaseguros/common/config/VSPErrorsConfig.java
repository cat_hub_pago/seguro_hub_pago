package cl.cencosud.ventaseguros.common.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * VSPErrorsConfig.
 * <br/>
 * @author tinet
 * @version 1.0
 * @created Jan 27, 2010
 */
public class VSPErrorsConfig extends AbstractConfigurator {

    /**
     * instance.
     */
    private static VSPErrorsConfig instance = new VSPErrorsConfig();

    /**
     * getInstance.
     * @return instancia actual.
     */
    public static VSPErrorsConfig getInstance() {
        return instance;
    }

    /**
     * loadBundle.
     * @param locale local.
     * @return recurso.
     */
    public ResourceBundle loadBundle(Locale locale) {
        return ResourceBundle.getBundle("VSPErrorsConfig", locale);
    }
}
