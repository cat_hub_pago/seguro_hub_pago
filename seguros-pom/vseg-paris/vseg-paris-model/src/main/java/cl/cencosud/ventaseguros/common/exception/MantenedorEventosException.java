package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

/**
 * En caso de que el tipo de archivo no corresponda con el esperado.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/09/2010
 */
public class MantenedorEventosException extends BusinessException {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1825704362896813634L;

    public static final String RUT_NO_VALIDO =
        "cl.cencosud.ventaseguros.common.exception.RUT_NO_VALIDO";
    
    public static final String RANGO_DE_FECHAS_NO_VALIDO = 
        "cl.cencosud.ventaseguros.common.exception.RANGO_DE_FECHAS_NO_VALIDO";        

    public static final String URL_NO_VALIDA = 
        "cl.cencosud.ventaseguros.common.exception.URL_NO_VALIDA";        

    public static final String EVENTO_NO_EXISTE = 
        "cl.cencosud.ventaseguros.common.exception.EVENTO_NO_EXISTE";        

    /**
     * constructor.
     * @param messageKey llave del mensaje.
     * @param arguments argumentos del mensaje.
     */
    public MantenedorEventosException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    /**
     * loadConfigurator.
     * @return configuracion.
     */
    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}
