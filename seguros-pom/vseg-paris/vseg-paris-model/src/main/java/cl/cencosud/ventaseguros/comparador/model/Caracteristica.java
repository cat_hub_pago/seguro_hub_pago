package cl.cencosud.ventaseguros.comparador.model;

import java.io.Serializable;

public class Caracteristica implements Serializable {

	private static final long serialVersionUID = 7974552209443794737L;
	private int id_plan_caracteristica;
	private int id_producto_caracteristica;
	private long id_producto;
	private long id_plan;
	private int id_caracteristica;
	private String valor;
	private float primaMensualUF;
    private long primaMensualPesos;
    private String idPlanCompannia;
	private String descripcion;
	private int tipo;
	private String es_eliminado;
	private String habilitado;
	private String tooltip;
	
	public int getId_plan_caracteristica() {
		return id_plan_caracteristica;
	}
	public void setId_plan_caracteristica(int idPlanCaracteristica) {
		id_plan_caracteristica = idPlanCaracteristica;
	}
	public long getId_plan() {
		return id_plan;
	}
	public void setId_plan(long idPlan) {
		id_plan = idPlan;
	}
	public int getId_caracteristica() {
		return id_caracteristica;
	}
	public void setId_caracteristica(int idCaracteristica) {
		id_caracteristica = idCaracteristica;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getEs_eliminado() {
		return es_eliminado;
	}
	public void setEs_eliminado(String esEliminado) {
		es_eliminado = esEliminado;
	}
	public String getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}
	public String getTooltip() {
		return tooltip;
	}
	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}
	public int getId_producto_caracteristica() {
		return id_producto_caracteristica;
	}
	public void setId_producto_caracteristica(int idProductoCaracteristica) {
		id_producto_caracteristica = idProductoCaracteristica;
	}
	public long getId_producto() {
		return id_producto;
	}
	public void setId_producto(long idProducto) {
		id_producto = idProducto;
	}
	public float getPrimaMensualUF() {
		return primaMensualUF;
	}
	public void setPrimaMensualUF(float primaMensualUF) {
		this.primaMensualUF = primaMensualUF;
	}
	public long getPrimaMensualPesos() {
		return primaMensualPesos;
	}
	public void setPrimaMensualPesos(long primaMensualPesos) {
		this.primaMensualPesos = primaMensualPesos;
	}
	public String getIdPlanCompannia() {
		return idPlanCompannia;
	}
	public void setIdPlanCompannia(String idPlanCompannia) {
		this.idPlanCompannia = idPlanCompannia;
	}
		
}
