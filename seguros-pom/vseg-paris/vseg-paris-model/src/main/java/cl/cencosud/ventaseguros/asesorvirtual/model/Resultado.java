package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;

/**
 * Resultados de las respuestas.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
//@XStreamAlias(value = "resultado")
//@XStreamConverter(ResultadoConverter.class)
public class Resultado implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 3242197357583882903L;

    /**
     * Identificador del resultado.
     */
    //@XStreamAlias(value = "id")
    //@XStreamAsAttribute
    private int id;

    /**
     * Titulo del resultado.
     */
    //@XStreamAlias(value = "titulo")
    //@XStreamAsAttribute
    private String titulo;

    /**
     * Nombre del plan asociado al resultado.
     */
    //@XStreamAlias(value = "nombreplan")
    //@XStreamAsAttribute
    private String nombreplan;

    /**
     * Direccion http asociada al resultado.
     */
    //@XStreamAlias(value = "link0")
    //@XStreamAsAttribute
    private String link0;

    /**
     * Direccion http asociada al resultado.
     */
    //@XStreamAlias(value = "link1")
    //@XStreamAsAttribute
    private String link1;

    /**
     * Icono del resultado.
     */
    //@XStreamAlias(value = "icono")
    //@XStreamAsAttribute
    private String icono;

    /**
     * Texto del resultado.
     */
    //@XStreamAlias(value = "value")
    private String value;

    /**
     * @return retorna el valor del atributo id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id a establecer en el atributo id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return retorna el valor del atributo titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo a establecer en el atributo titulo.
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return retorna el valor del atributo nombreplan
     */
    public String getNombreplan() {
        return nombreplan;
    }

    /**
     * @param nombreplan a establecer en el atributo nombreplan.
     */
    public void setNombreplan(String nombreplan) {
        this.nombreplan = nombreplan;
    }

    /**
     * @return retorna el valor del atributo link0
     */
    public String getLink0() {
        return link0;
    }

    /**
     * @param link0 a establecer en el atributo link0.
     */
    public void setLink0(String link0) {
        this.link0 = link0;
    }

    /**
     * @return retorna el valor del atributo link1
     */
    public String getLink1() {
        return link1;
    }

    /**
     * @param link1 a establecer en el atributo link1.
     */
    public void setLink1(String link1) {
        this.link1 = link1;
    }

    /**
     * @return retorna el valor del atributo icono
     */
    public String getIcono() {
        return icono;
    }

    /**
     * @param icono a establecer en el atributo icono.
     */
    public void setIcono(String icono) {
        this.icono = icono;
    }

    /**
     * @return retorna el valor del atributo value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value a establecer en el atributo value.
     */
    public void setValue(String value) {
        this.value = value;
    }

}
