package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;
import java.util.List;

/**
 * Preguntas.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
//@XStreamAlias("preguntas")
public class Preguntas implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -3880716345587946255L;

    /**
     * Identificador de la primera pregunta del listado.
     */
    //@XStreamAlias("idPrimeraPregunta")
    //@XStreamAsAttribute
    private int idPrimeraPregunta;

    /**
     * Total de preguntas.
     */
    //@XStreamAlias("cantidadpreguntas")
    //@XStreamAsAttribute
    private int cantidadpreguntas;

    /**
     * Listado de preguntas.
     */
    //@XStreamImplicit(itemFieldName = "pregunta")
    private List < Pregunta > preguntas;

    /**
     * @return retorna el valor del atributo idPrimeraPregunta
     */
    public int getIdPrimeraPregunta() {
        return idPrimeraPregunta;
    }

    /**
     * @param idPrimeraPregunta a establecer en el atributo idPrimeraPregunta.
     */
    public void setIdPrimeraPregunta(int idPrimeraPregunta) {
        this.idPrimeraPregunta = idPrimeraPregunta;
    }

    /**
     * @return retorna el valor del atributo cantidadpreguntas
     */
    public int getCantidadpreguntas() {
        return cantidadpreguntas;
    }

    /**
     * @param cantidadpreguntas a establecer en el atributo cantidadpreguntas.
     */
    public void setCantidadpreguntas(int cantidadpreguntas) {
        this.cantidadpreguntas = cantidadpreguntas;
    }

    /**
     * @return retorna el valor del atributo preguntas
     */
    public List < Pregunta > getPreguntas() {
        return preguntas;
    }

    /**
     * @param preguntas a establecer en el atributo preguntas.
     */
    public void setPreguntas(List < Pregunta > preguntas) {
        this.preguntas = preguntas;
    }

}
