package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;
import java.util.List;

/**
 * Defincion de la malla de preguntas-prespuestas.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
//@XStreamAlias("sitio")
public class Sitio implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -498561822002470949L;

    /**
     * Secciones pertenecientes al sitio.
     */
    //@XStreamImplicit(itemFieldName = "seccion")
    private List < Seccion > secciones;

    /**
     * @return retorna el valor del atributo secciones
     */
    public List < Seccion > getSecciones() {
        return secciones;
    }

    /**
     * @param secciones a establecer en el atributo secciones.
     */
    public void setSecciones(List < Seccion > secciones) {
        this.secciones = secciones;
    }

}
