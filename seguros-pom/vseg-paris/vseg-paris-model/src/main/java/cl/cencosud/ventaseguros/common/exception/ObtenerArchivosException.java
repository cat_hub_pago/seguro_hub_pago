package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

/**
 * En caso de no se pueda obtener el archivo.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/09/2010
 */
public class ObtenerArchivosException extends BusinessException {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -4469343167194653369L;
    public static final String ARCHIVO_NO_ENCONTRADO =
        "cl.cencosud.ventaseguros.common.exception.ARCHIVO_NO_ENCONTRADO";

    /**
     * constructor.
     * @param messageKey llave del mensaje.
     * @param arguments argumentos del mensaje.
     */
    public ObtenerArchivosException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    /**
     * loadConfigurator.
     * @return objeto de configuracion.
     */
    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}
