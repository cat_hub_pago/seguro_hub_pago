package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

/**
 * En caso de que el tipo de archivo no corresponda con el esperado.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/09/2010
 */
public class FichaSubcategoriaException extends BusinessException {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 4238843287019025260L;

    public static final String PLAN_NO_ENCONTRADO =
        "cl.cencosud.ventaseguros.common.exception.PLAN_NO_ENCONTRADO";

    /**
     * constructor.
     * @param messageKey llave del mensaje.
     * @param arguments argumentos del mensaje.
     */
    public FichaSubcategoriaException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    /**
     * loadConfigurator.
     * @return configuracion.
     */
    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}
