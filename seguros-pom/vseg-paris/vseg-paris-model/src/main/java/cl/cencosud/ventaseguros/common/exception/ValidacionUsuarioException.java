package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

/**
 * En caso de no se pueda encontrar el usuario.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/09/2010
 */
public class ValidacionUsuarioException extends BusinessException {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -4927203197871156733L;

    public static final String USUARIO_NO_ENCONTRADO =
        "cl.cencosud.ventaseguros.common.exception.USUARIO_NO_ENCONTRADO";

    public static final String USUARIO_EXISTE =
        "cl.cencosud.ventaseguros.common.exception.USUARIO_EXISTE";

    public static final String USUARIO_NO_VALIDO =
        "cl.cencosud.ventaseguros.common.exception.USUARIO_NO_VALIDO";

    public static final String NRO_SERIE_NO_VALIDO =
        "cl.cencosud.ventaseguros.common.exception.NUMERO_SERIE_RUT_NO_VALIDO";

    public static final String CAPTCHA_INVALIDO =
        "cl.cencosud.ventaseguros.common.exception.CAPTCHA_INVALIDO";

    public static final String EXCEPCION_EQUIFAX =
        "cl.cencosud.ventaseguros.common.exception.EXCEPCION_EQUIFAX";
    
    public static final String CLAVE_INCORRECTA =
            "cl.cencosud.ventaseguros.common.exception.CLAVE_INCORRECTA";
    
    public static final String CLAVE_NO_COINCIDE =
            "cl.cencosud.ventaseguros.common.exception.CLAVE_NO_COINCIDE";

    /**
     * constructor.
     * @param messageKey llave del mensaje.
     * @param arguments argumentos del mensaje.
     */
    public ValidacionUsuarioException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    /**
     * TODO Describir constructor de ValidacionUsuarioException.
     * @param messageKey
     * @param argument
     */
    public ValidacionUsuarioException(String messageKey, String argument) {
        super(messageKey, new Object[] { argument });
    }

    /**
     * loadConfigurator.
     * @return objeto de configuracion.
     */
    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}
