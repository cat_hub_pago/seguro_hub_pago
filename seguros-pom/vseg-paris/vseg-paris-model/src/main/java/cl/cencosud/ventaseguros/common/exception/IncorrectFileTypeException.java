package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

/**
 * En caso de que el tipo de archivo no corresponda con el esperado.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/09/2010
 */
public class IncorrectFileTypeException extends BusinessException {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1825704362896813634L;

    public static final String TIPO_ARCHIVO =
        "cl.cencosud.ventaseguros.fichasubcategoria.struts.action.tipoarchivo";

    /**
     * constructor.
     * @param messageKey llave del mensaje.
     * @param arguments argumentos del mensaje.
     */
    public IncorrectFileTypeException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    /**
     * loadConfigurator.
     * @return configuracion.
     */
    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}
