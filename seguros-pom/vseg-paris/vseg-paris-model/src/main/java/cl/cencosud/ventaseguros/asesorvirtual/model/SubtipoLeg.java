package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;
import java.util.Date;

public class SubtipoLeg implements Serializable {

    private static final long serialVersionUID = 1019385036648292237L;

    private int id_sub_tipo_prod;
    private String codigo_tipo_prod_leg;
    private String codigo_sub_tipo_prod_leg;
    private String nombre;
    private int id_pais;
    private int es_activo;
    private Date fecha_creacion;
    private Date fecha_modificacion;

    /**
     * @return retorna el valor del atributo id_sub_tipo_prod
     */
    public int getId_sub_tipo_prod() {
        return id_sub_tipo_prod;
    }

    /**
     * @param id_sub_tipo_prod a establecer en el atributo id_sub_tipo_prod.
     */
    public void setId_sub_tipo_prod(int id_sub_tipo_prod) {
        this.id_sub_tipo_prod = id_sub_tipo_prod;
    }

    /**
     * @return retorna el valor del atributo codigo_tipo_prod_leg
     */
    public String getCodigo_tipo_prod_leg() {
        return codigo_tipo_prod_leg;
    }

    /**
     * @param codigo_tipo_prod_leg a establecer en el atributo codigo_tipo_prod_leg.
     */
    public void setCodigo_tipo_prod_leg(String codigo_tipo_prod_leg) {
        this.codigo_tipo_prod_leg = codigo_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_sub_tipo_prod_leg
     */
    public String getCodigo_sub_tipo_prod_leg() {
        return codigo_sub_tipo_prod_leg;
    }

    /**
     * @param codigo_sub_tipo_prod_leg a establecer en el atributo codigo_sub_tipo_prod_leg.
     */
    public void setCodigo_sub_tipo_prod_leg(String codigo_sub_tipo_prod_leg) {
        this.codigo_sub_tipo_prod_leg = codigo_sub_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre a establecer en el atributo nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo es_activo
     */
    public int getEs_activo() {
        return es_activo;
    }

    /**
     * @param es_activo a establecer en el atributo es_activo.
     */
    public void setEs_activo(int es_activo) {
        this.es_activo = es_activo;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

}
