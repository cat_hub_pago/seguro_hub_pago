package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.interfaces.MantenedorUsuarios;
import cl.cencosud.ventaseguros.interfaces.MantenedorUsuariosHome;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.model.UsuarioInterno;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Clase que sirve de delegate entre el action y el EJB.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Oct 4, 2010
 */
public class MantenedorUsuariosDelegate {

    /**
     * Atributo para EJB.
     */
    private MantenedorUsuarios mantenedorUsuarios;
    /**
     * Atributo para EJB.
     */
    private MantenedorUsuariosHome mantenedorUsuariosHome;

    /**
     * Constructor de la clase.
     */
    public MantenedorUsuariosDelegate() {
        try {
            this.mantenedorUsuariosHome =
                (MantenedorUsuariosHome) ServiceLocator.singleton()
                    .getRemoteHome(MantenedorUsuariosHome.JNDI_NAME,
                        MantenedorUsuariosHome.class);

            this.mantenedorUsuarios = this.mantenedorUsuariosHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Metodo que obtiene los roles.
     * @return
     */
    public List < Map > obtenerRoles() {
        List < Map > rolesLista = null;
        try {
            rolesLista = this.mantenedorUsuarios.obtenerRoles();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return rolesLista;
    }

    /**
     * Metodo que inserta un nuevo personal cencosud.
     * @param mapdatos
     * @return
     */
    public boolean insertarNuevoPersonalCenco(Map mapdatos)
        throws BusinessException {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.insertarNuevoPersonalCenco(mapdatos);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }

        return resultado;
    }

    /**
     * Metodo que modificar personal Cencosud,condicion Username.
     * @param userName
     * @return
     */
    public boolean modificarPersonalCenco(String userName) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.modificarPersonalCenco(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }

        return resultado;
    }

    /**
     * Metodo que modifica personal cencosud, condicion IDusuario.
     * @param IdUsuarioInt
     * @return
     */
    public boolean modificarPersonalCenco(Integer IdUsuarioInt) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.modificarPersonalCenco(IdUsuarioInt);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo que obtiene personalCencosud.
     * @return
     */
    public List < Map > obtenerPersonalCenco() {
        List < Map > resultado = null;
        try {
            resultado = this.mantenedorUsuarios.obtenerPersonalCenco();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo que obtiene paises.
     * @return
     */
    public List < Map > obtenerPaises() {
        List < Map > resultado = null;
        try {
            resultado = this.mantenedorUsuarios.obtenerPaises();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo que obtiene el id de personal cencosud.
     * @param userName
     * @return
     */
    public Integer obtenerIdPersonalCenco(String userName) {
        Integer id = null;
        try {
            id = this.mantenedorUsuarios.obtenerIdPersonalCenco(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return id;
    }

    /**
     * Metodo para obtener la cantidad total de personal cencosud.
     * @return
     */
    public Integer obtenerCantidadTotalPersonalCenco() {
        Integer cantidad = null;
        try {
            cantidad =
                this.mantenedorUsuarios.obtenerCantidadTotalPersonalCenco();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return cantidad;
    }

    /**
     * Metodo, que eliminar un personal Cencosud.
     * @param IdUsuarioInt
     * @return
     */
    public boolean eliminarPersonalCenco(Integer IdUsuarioInt) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.eliminarPersonalCenco(IdUsuarioInt);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo, para eliminar una persona Cencosud.
     * @param userName
     * @return
     */
    public boolean eliminarPersonaCenco(String userName) {
        boolean resultado = false;
        try {
            resultado = this.mantenedorUsuarios.eliminarPersonaCenco(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para enviar correo.
     * @param userName
     * @param mensaje
     * @param correo
     * @return
     */
    public boolean enviarCorreo(String userName, String mensaje, String correo) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.enviarCorreo(userName, mensaje, correo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para obtener Usuario Interno cencosud.
     * @param userName
     * @return
     */
    public UsuarioInterno obtenerUsuarioInternoCenco(String userName) {
        UsuarioInterno usuarioInterno = null;
        try {
            usuarioInterno =
                this.mantenedorUsuarios.obtenerUsuarioInternoCenco(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return usuarioInterno;
    }

    /**
     * Metodo para verificar existencia de usuario.
     * @param userName
     * @return
     * @throws BusinessException
     */
    public boolean existenciaUsuario(String userName) throws BusinessException {
        boolean resultado = false;
        try {
            resultado = this.mantenedorUsuarios.existenciaUsuario(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para actualizar Usuario cencosud.
     * @param mapdatos
     * @param userName
     * @return
     */
    public boolean actualizarUsuarioCenco(Map mapdatos, String userName) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.actualizarUsuarioCenco(mapdatos,
                    userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para cambiar estado del usuario cencosud.
     * @param userName
     * @param estado
     * @return
     */
    public boolean cambiarEstadoUsuarioCenco(String userName, boolean estado) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.cambiarEstadoUsuarioCenco(userName,
                    estado);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para obtener usuario Externo cencosud.
     * @param userName
     * @return
     * @throws BusinessException
     */
    public UsuarioExterno obtenerUsuarioExternoCencosud(String userName)
        throws BusinessException {
        UsuarioExterno usuarioExterno = null;
        try {
            usuarioExterno =
                this.mantenedorUsuarios.obtenerUsuarioExternoCencosud(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return usuarioExterno;
    }

    /**
     * Metodo para actualizar Clientes Externos de cencosud.
     * @param datos
     * @param userName
     * @return
     */
    public boolean actualizarClienteExternoCenco(Map datos, String userName) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.actualizarClienteExternoCenco(datos,
                    userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para validacion contra LDAP.
     * @param userName
     * @return
     */
    public boolean validacionLDAP(String userName, String password) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.validacionLDAP(userName, password);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para obtener roles de usuario.
     * @param userName
     * @return
     */
    public List < Map > obtenerRolUsuario(String userName) {
        List < Map > resultado = null;
        try {
            resultado = this.mantenedorUsuarios.obtenerRolUsuario(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para obtener estado de usuario.
     * @param userName
     * @return
     */
    public boolean obtenerEstadoUsuario(String userName) {
        boolean resultado = false;
        try {
            resultado = this.mantenedorUsuarios.obtenerEstadoUsuario(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo para obtener estado de la password de un usuario.
     * @param userName usuario
     * @return estado
     */
    public boolean obtenerEstadoPasswordUsuario(String userName) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorUsuarios.obtenerEstadoPasswordUsuario(userName);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }
}
