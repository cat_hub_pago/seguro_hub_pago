package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedoretl.dao.MantenedorETLDAO;
import cl.cencosud.ventaseguros.mantenedoretl.dao.MantenedorETLWSDAO;

/**
 * @author Francisco Mendoza (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 13-Jun-2011
 * 
 * 
 * @ejb.bean name="MantenedorETL" display-name="MantenedorETL"
 *           description="Servicios relacionado con el mantenedor ETL"
 *           jndi-name="ejb/ventaseguros/MantenedorETL" type="Stateless"
 *           view-type="remote"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/seguridadDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/seguridadDS"
 *                   
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/VSPDS"
 *
 * @ejb.transaction type="Supports"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaTiposSeguros"
 *           interface="cl.cencosud.bigsa.cotizacion.ramos.was.WsBigsaTiposSeguros"
 *           jaxrpc-mapping-file="META-INF/WsBigsaTiposSegurosImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaSubTiposSeguros"
 *           interface="cl.cencosud.bigsa.cotizacion.ramos.sutipo.was.WsBigsaSubTiposSeguros"
 *           jaxrpc-mapping-file="META-INF/WsBigsaSubTiposSegurosImplPort_mapping.xml"
 *           
 * @ejb.ejb-service-ref name="service/WsBigsaProductos"
 *           interface="cl.cencosud.bigsa.cotizacion.ramos.producto.was.WsBigsaProductos"
 *           jaxrpc-mapping-file="META-INF/WsBigsaProductosImplPort_mapping.xml"
 *           
 * @ejb.ejb-service-ref name="service/WsBigsaPlanes"
 *           interface="cl.cencosud.bigsa.cotizacion.ramos.plan.was.WsBigsaPlanes"
 *           jaxrpc-mapping-file="META-INF/WsBigsaPlanesImplPort_mapping.xml"
 */
public class MantenedorETLBean implements SessionBean {

    /**
     * Logger de la clase.
     */
    private static final Log logger =
        LogFactory.getLog(MantenedorETLBean.class);

    /**
     * Identificador de la clase para serialización.
     */
    private static final long serialVersionUID = -7135318241649661910L;

    /**
     * Metodo create.
     */
    public void ejbCreate() {
        // Create.
    }

    public void ejbActivate() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void ejbPassivate() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void ejbRemove() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void setSessionContext(SessionContext arg0) throws EJBException,
        RemoteException {
        // TODO Auto-generated method stub

    }

    /**
     * Obtiene listado de Tipos de producto.
     * @ejb.interface-method "remote"
     */
    public TipoLeg[] obtenerTipoLeg() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorETLWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorETLWSDAO.class);
        try {
            return dao.obtenerTipoLeg();
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método obtenerSubTipoLeg.
     * @param codigo_tipo_prod_leg
     * @return
     * @ejb.interface-method "remote"
     */
    public SubtipoLeg[] obtenerSubTipoLeg(String codigo_tipo_prod_leg) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorETLWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorETLWSDAO.class);
        try {
            return dao.obtenerSubTipoLeg(codigo_tipo_prod_leg);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método obtenerProductoLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @return
     * @ejb.interface-method "remote"
     */
    public ProductoLeg[] obtenerProductoLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorETLWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorETLWSDAO.class);
        try {
            return dao.obtenerProductoLeg(codigo_tipo_prod_leg,
                codigo_sub_tipo_prod_leg);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método obtenerPlanLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param codigo_producto_leg
     * @return
     * @ejb.interface-method "remote"
     */
    public PlanLeg[] obtenerPlanLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String codigo_producto_leg) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorETLWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorETLWSDAO.class);
        try {
            return dao.obtenerPlanLeg(codigo_tipo_prod_leg,
                codigo_sub_tipo_prod_leg, codigo_producto_leg);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método enviarPlan.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param codigo_producto_leg
     * @param planes
     * @ejb.interface-method "remote"
     */
    public void enviarPlan(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String codigo_producto_leg,
        String[] planes) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorETLDAO dao =
            factory.getVentaSegurosDAO(MantenedorETLDAO.class);
        MantenedorETLWSDAO wsdao =
            factory.getVentaSegurosDAO(MantenedorETLWSDAO.class);

        try {
            //Obtener listado de planes.
            PlanLeg[] planList =
                wsdao.obtenerPlanLeg(codigo_tipo_prod_leg,
                    codigo_sub_tipo_prod_leg, codigo_producto_leg);

            List < String > lPlanes = Arrays.asList(planes);

            for (int i = 0; i < planList.length; i++) {
                if (lPlanes.contains(planList[i].getCodigo_plan_leg())) {
                    try {
                        //Validar existencia previa del plan.
                        long idPlan =
                            new Long(planList[i].getCodigo_plan_leg())
                                .longValue();

                        logger.info("Enviar plan: " + idPlan);

                        PlanLeg plan = dao.obtenerDatosPlan(idPlan);

                        if (plan != null) {
                            dao.actualizarDatosPlan(plan);
                            logger.info("Plan actualizado. SE DEBE ACTIVAR "
                                + "PARA PODER UTILIZAR.");
                        } else {
                            dao.enviarPlan(planList[i]);
                            logger.info("Plan enviado. SE DEBE ACTIVAR "
                                + "PARA PODER UTILIZAR.");
                        }

                    } catch (Exception e) {
                        logger.error("Error al grabar plan");
                    }
                }
            }
            logger.info("Planes enviados");
        } finally {
            dao.close();
            wsdao.close();
        }
    }

    /**
     * TODO Describir método enviarProducto.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param productos
     * @ejb.interface-method "remote"
     */
    public void enviarProducto(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String[] productos) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorETLDAO dao =
            factory.getVentaSegurosDAO(MantenedorETLDAO.class);
        MantenedorETLWSDAO wsdao =
            factory.getVentaSegurosDAO(MantenedorETLWSDAO.class);

        try {

            ProductoLeg[] productoList =
                wsdao.obtenerProductoLeg(codigo_tipo_prod_leg,
                    codigo_sub_tipo_prod_leg);

            List < String > lProductos = Arrays.asList(productos);

            for (ProductoLeg productoLeg : productoList) {
                if (lProductos.contains(productoLeg.getCodigo_producto_leg())) {
                    try {
                        //Validar existencia previa del producto.
                        long idProducto =
                            new Long(productoLeg.getCodigo_producto_leg())
                                .longValue();

                        //Validar existencia previa del producto.
                        ProductoLeg producto =
                            dao.obtenerDatosProducto(idProducto);

                        if (producto != null) {
                            logger.info("Producto existente (" + idProducto
                                + ")");
                            //dao.actualizarDatosProducto(producto);
                        } else {
                            logger.info("Producto nuevo, se crea ("
                                + idProducto + ")");
                            //Crear el producto.
                            dao.enviarProducto(productoLeg);
                        }

                        logger.info("Obtener planes del producto: "
                            + idProducto);

                        PlanLeg[] planes =
                            wsdao.obtenerPlanLeg(codigo_tipo_prod_leg,
                                codigo_sub_tipo_prod_leg, productoLeg
                                    .getCodigo_producto_leg());

                        try {
                            for (PlanLeg planLeg : planes) {
                                long idPlan =
                                    new Long(planLeg.getCodigo_plan_leg())
                                        .longValue();

                                logger.info("Enviar plan: " + idPlan);

                                //Validar existencia previa del plan.
                                PlanLeg plan = dao.obtenerDatosPlan(idPlan);

                                if (plan != null) {
                                    //dao.actualizarDatosPlan(plan);
                                    logger.info("Plan existente (" + idPlan
                                        + ")");
                                } else {
                                    dao.enviarPlan(planLeg);
                                    logger
                                        .info("Plan enviado. SE DEBE ACTIVAR "
                                            + "PARA PODER UTILIZAR.");
                                }
                            }

                        } catch (Exception e) {
                            logger.error("Error al grabar plan");
                        }
                    } catch (Exception e) {
                        logger.error("Error al grabar producto");
                    }
                    logger.info("Planes enviados");
                }
            }
        } finally {
            dao.close();
            wsdao.close();
        }
    }
}
