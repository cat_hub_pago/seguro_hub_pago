package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessAsesorVirtual_2ae6f2b4
 */
public class EJSRemoteStatelessAsesorVirtual_2ae6f2b4 extends EJSWrapper implements AsesorVirtual {
	/**
	 * EJSRemoteStatelessAsesorVirtual_2ae6f2b4
	 */
	public EJSRemoteStatelessAsesorVirtual_2ae6f2b4() throws java.rmi.RemoteException {
		super();	}
	/**
	 * sincronizarXML
	 */
	public boolean sincronizarXML(java.lang.String rutaLocal) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = rutaLocal;
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.sincronizarXML(rutaLocal);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * cargarArbol
	 */
	public cl.cencosud.ventaseguros.asesorvirtual.model.Sitio cargarArbol(int idArbol) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.asesorvirtual.model.Sitio _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Integer(idArbol);
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.cargarArbol(idArbol);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * buscarArbol
	 */
	public java.util.List buscarArbol(int idArbol) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Integer(idArbol);
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.buscarArbol(idArbol);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * actualizarAlternativa
	 */
	public void actualizarAlternativa(int idAlternativa, int idSeccion, int idPregunta, int idRespuesta, java.lang.String username) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[5];
				_jacc_parms[0] = new java.lang.Integer(idAlternativa);
				_jacc_parms[1] = new java.lang.Integer(idSeccion);
				_jacc_parms[2] = new java.lang.Integer(idPregunta);
				_jacc_parms[3] = new java.lang.Integer(idRespuesta);
				_jacc_parms[4] = username;
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			beanRef.actualizarAlternativa(idAlternativa, idSeccion, idPregunta, idRespuesta, username);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * actualizarProximaPregunta
	 */
	public void actualizarProximaPregunta(int idAlternativa, int idSeccion, int idPregunta, int idProxPregunta, java.lang.String username) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[5];
				_jacc_parms[0] = new java.lang.Integer(idAlternativa);
				_jacc_parms[1] = new java.lang.Integer(idSeccion);
				_jacc_parms[2] = new java.lang.Integer(idPregunta);
				_jacc_parms[3] = new java.lang.Integer(idProxPregunta);
				_jacc_parms[4] = username;
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			beanRef.actualizarProximaPregunta(idAlternativa, idSeccion, idPregunta, idProxPregunta, username);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * grabarAlternativa
	 */
	public void grabarAlternativa(cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa alternativa, int idPregunta, int idSeccion, java.lang.String username) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = alternativa;
				_jacc_parms[1] = new java.lang.Integer(idPregunta);
				_jacc_parms[2] = new java.lang.Integer(idSeccion);
				_jacc_parms[3] = username;
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			beanRef.grabarAlternativa(alternativa, idPregunta, idSeccion, username);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * grabarArbol
	 */
	public void grabarArbol(cl.cencosud.ventaseguros.asesorvirtual.model.Sitio sitio, java.lang.String username, int idArbol) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = sitio;
				_jacc_parms[1] = username;
				_jacc_parms[2] = new java.lang.Integer(idArbol);
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			beanRef.grabarArbol(sitio, username, idArbol);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * grabarPregunta
	 */
	public void grabarPregunta(cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta pregunta, int idSeccion, java.lang.String username) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = pregunta;
				_jacc_parms[1] = new java.lang.Integer(idSeccion);
				_jacc_parms[2] = username;
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			beanRef.grabarPregunta(pregunta, idSeccion, username);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * resultadoGrabar
	 */
	public void resultadoGrabar(cl.cencosud.ventaseguros.asesorvirtual.model.Resultado resultado, int idSeccion, java.lang.String username) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = resultado;
				_jacc_parms[1] = new java.lang.Integer(idSeccion);
				_jacc_parms[2] = username;
			}
	cl.cencosud.ventaseguros.ejb.AsesorVirtualBean beanRef = (cl.cencosud.ventaseguros.ejb.AsesorVirtualBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			beanRef.resultadoGrabar(resultado, idSeccion, username);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
}
