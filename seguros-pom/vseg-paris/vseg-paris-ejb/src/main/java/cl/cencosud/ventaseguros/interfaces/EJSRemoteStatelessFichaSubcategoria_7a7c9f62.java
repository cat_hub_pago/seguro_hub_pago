package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessFichaSubcategoria_7a7c9f62
 */
public class EJSRemoteStatelessFichaSubcategoria_7a7c9f62 extends EJSWrapper implements FichaSubcategoria {
	/**
	 * EJSRemoteStatelessFichaSubcategoria_7a7c9f62
	 */
	public EJSRemoteStatelessFichaSubcategoria_7a7c9f62() throws java.rmi.RemoteException {
		super();	}
	/**
	 * existePrimaUnica
	 */
	public boolean existePrimaUnica(long idPlan) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idPlan);
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.existePrimaUnica(idPlan);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerRestriccionVida
	 */
	public cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida obtenerRestriccionVida(long idPlan) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idPlan);
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRestriccionVida(idPlan);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerFicha
	 */
	public cl.cencosud.ventaseguros.common.Ficha obtenerFicha(java.lang.Long id_ficha) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Ficha _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = id_ficha;
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerFicha(id_ficha);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	
	/**
	 * obtenerFicha
	 */

	
	
	/**
	 * obtenerDatosPlan
	 */
	public cl.cencosud.ventaseguros.common.PlanLeg obtenerDatosPlan(long idPlan) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.PlanLeg _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idPlan);
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosPlan(idPlan);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerFichasSubcategoria
	 */
	public java.util.List obtenerFichasSubcategoria(int idRama, int idSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Integer(idRama);
				_jacc_parms[1] = new java.lang.Integer(idSubcategoria);
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerFichasSubcategoria(idRama, idSubcategoria);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPlanes
	 */
	public java.util.List obtenerPlanes(int idSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Integer(idSubcategoria);
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPlanes(idSubcategoria);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPlanesPromocion
	 */
	public java.util.List obtenerPlanesPromocion(int idRama, int idSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Integer(idRama);
				_jacc_parms[1] = new java.lang.Integer(idSubcategoria);
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPlanesPromocion(idRama, idSubcategoria);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerArchivo
	 */
	public java.util.Map obtenerArchivo(long id_ficha, java.lang.String tipo) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Long(id_ficha);
				_jacc_parms[1] = tipo;
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerArchivo(id_ficha, tipo);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * agregarFichaProducto
	 */
	public long agregarFichaProducto(cl.cencosud.ventaseguros.common.Ficha ficha) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = ficha;
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.agregarFichaProducto(ficha);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	
	
	
	
	/**
	 * actualizarFichaProducto
	 */
	public void actualizarFichaProducto(cl.cencosud.ventaseguros.common.Ficha ficha) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = ficha;
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 9, _EJS_s, _jacc_parms);
			beanRef.actualizarFichaProducto(ficha);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	
	
	
	
	
	/**
	 * administrarPlan
	 */
	public void administrarPlan(cl.cencosud.ventaseguros.common.PlanLeg plan, cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida restriccion, boolean primaUnica, int rama) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = plan;
				_jacc_parms[1] = restriccion;
				_jacc_parms[2] = new java.lang.Boolean(primaUnica);
				_jacc_parms[3] = new java.lang.Integer(rama);
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 10, _EJS_s, _jacc_parms);
			beanRef.administrarPlan(plan, restriccion, primaUnica, rama);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 10, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * subirArchivo
	 */
	public void subirArchivo(long id_ficha, byte[] bios, java.lang.String tipo) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = new java.lang.Long(id_ficha);
				_jacc_parms[1] = bios;
				_jacc_parms[2] = tipo;
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 11, _EJS_s, _jacc_parms);
			beanRef.subirArchivo(id_ficha, bios, tipo);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 11, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	
	public cl.cencosud.ventaseguros.common.LegalFicha obtenerLegalFicha(java.lang.Long id_ficha) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.LegalFicha _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = id_ficha;
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 12, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerLegalFicha(id_ficha);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 12, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	
	public void agregarLegalFichaProducto(cl.cencosud.ventaseguros.common.LegalFicha legalFicha) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = legalFicha;
			}
			cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 13, _EJS_s, _jacc_parms);
			beanRef.agregarLegalFichaProducto(legalFicha);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 13, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}
	
	
	public void actualizarLegalFichaProducto(cl.cencosud.ventaseguros.common.LegalFicha legalFicha) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = legalFicha;
			}
	cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean beanRef = (cl.cencosud.ventaseguros.ejb.FichaSubcategoriaBean)container.preInvoke(this, 14, _EJS_s, _jacc_parms);
			beanRef.actualizarLegalFichaProducto(legalFicha);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 14, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
}
