package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.interfaces.InformesComerciales;
import cl.cencosud.ventaseguros.interfaces.InformesComercialesHome;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

public class InformesComercialesDelegate {

    /**
     * Instancia de la clase InformesComerciales.
     */
    private InformesComerciales informesComerciales;

    /**
     * Home de InformesComerciales.
     */
    private InformesComercialesHome informesComercialesHome;

    /**
     * Constructor de InformesComercialesDelegate.
     */

    public InformesComercialesDelegate() {
        try {
            this.informesComercialesHome =
                (InformesComercialesHome) ServiceLocator.singleton()
                    .getRemoteHome(InformesComercialesHome.JNDI_NAME,
                        InformesComercialesHome.class);
            this.informesComerciales = this.informesComercialesHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }

    }

    public List < HashMap < String, Object > > obtenerInformeComercialVehiculo(
        Date fechaDesde, Date fechaHasta, boolean esPaginado, int numeroPagina) {
        try {
            return this.informesComerciales.obtenerInformeComercialVehiculo(
                fechaDesde, fechaHasta, esPaginado, numeroPagina);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public List < HashMap < String, Object > > obtenerInformeComercialTarjetas(
        Date fechaDesde, Date fechaHasta, boolean esPaginado, int numeroPagina) {
        try {
            return this.informesComerciales.obtenerInformeComercialTarjetas(
                fechaDesde, fechaHasta, esPaginado, numeroPagina);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }
    
    public List < HashMap < String, Object > > obtenerInformeComercialVentas(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, int rama,
            int subcategoria, int id_plan, String compannia) {
            try {
				return this.informesComerciales.obtenerInformeComercialVentas(
				    fechaDesde, fechaHasta, esPaginado, numeroPagina, rama, subcategoria,
				    id_plan, compannia);
			} catch (RemoteException re) {
				throw new SystemException(re);
			}
        }
    
    public List < HashMap < String, Object > > obtenerInformeComercialCotizaciones(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, int rama,
            int subcategoria, int id_plan, String compannia) {
            try {
				return this.informesComerciales.obtenerInformeComercialCotizaciones(
				    fechaDesde, fechaHasta, esPaginado, numeroPagina, rama, subcategoria,
				    id_plan, compannia);
			} catch (RemoteException re) {
				throw new SystemException(re);
			}
        }


    public Map < String, Object > obtenerArchivoFactura(long id_factura) {
        try {
            return this.informesComerciales.obtenerArchivoFactura(id_factura);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }

    }



	public List<HashMap<String, Object>> obtenerVitrineoGeneral(
			String fechaDesde, String fechaHasta, boolean esPaginado,
			int numerosPagina, int rama, int subcategoria) {
		try {
			System.out.println(fechaDesde + "-" + fechaHasta + "-" + esPaginado + "-" + numerosPagina + "-" + rama + "-" + subcategoria);
			System.out.println("ALO");
			
            return this.informesComerciales.obtenerVitrineoGeneral(
            		fechaDesde, fechaHasta, esPaginado, numerosPagina, rama, subcategoria);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
	}

	public boolean ingresarPromocionPrincipal(String principal, String tipo, String posicion,
			String nombre, String linkVer, String tracker, String imagen, Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, String linkConocer, String linkCotizar,String idPlan) {
		boolean resultado = false;
		try {
            resultado = this.informesComerciales.ingresarPromocionPrincipal(
            		principal, tipo, posicion, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner,rama,linkConocer,linkCotizar,idPlan);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
        return resultado;
		
	}



	public boolean ingresarPromocionSecundaria(String principal, String tipo,
			String ordinal, String nombre, String linkVer,
			String tracker, String imagen, Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, String linkConocer, String linkCotizar, String idPlan) {
		boolean resultado = false;
		try {
            resultado = this.informesComerciales.ingresarPromocionSecundaria(
            		principal, tipo, ordinal, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar,idPlan);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
        return resultado;
		
	}

	public List<HashMap<String, Object>> obtenerPromocionesPrincipalBP(
			String tipoS) {
		try {
            return this.informesComerciales.obtenerPromocionesPrincipalBP(
            		tipoS);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
	}

	public List<Map<String, Object>> obtenerPromocionesBO() {
		try {
            return this.informesComerciales.obtenerPromocionesBO();
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
	}

	public List<Map<String, Object>> obtenerPromocionesBOPrincipal() {
		try {
            return this.informesComerciales.obtenerPromocionesBOPrincipal();
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
	}



}
