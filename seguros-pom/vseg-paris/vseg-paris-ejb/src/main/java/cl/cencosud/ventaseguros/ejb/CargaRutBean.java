package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.informescomerciales.dao.CargaRutsDAO;
//import cl.cencosud.ventaseguros.mantenedoreventos.dao.MantenedorEventosDAO;

/**
 * @author Felipe Gaete (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 08-SEP-2011 15:32:03
 *
 *
 * @ejb.bean name="CargaRut"
 *           display-name="CargaRut"
 *           description="Servicios relacionados a Carga rut."
 *           jndi-name="ejb/ventaseguros/CargaRut"
 *           type="Stateless"
 *           transaction-type="Container"
 *           view-type="remote"
 *
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/VSPDS"
 *
 * @ejb.transaction type="Supports"
 */
public class CargaRutBean implements SessionBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5472865210799714532L;
	
	public void ejbCreate() {
        //Create.
    }
	
	
	/**
     * Metodo relacionado con la insercion de los Rut desde un archivo.
     *
     * @param datos
     * @return
     * @ejb.interface-method "remote"
     */
	public boolean insertarRutArchivos(String[] rut) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        CargaRutsDAO dao =
            factory.getVentaSegurosDAO(CargaRutsDAO.class);
        try {
            return dao.insertarRutArchivos(rut);
        } finally {
            dao.close();
        }
    }

	
	 

	public void ejbActivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void ejbPassivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void ejbRemove() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void setSessionContext(SessionContext arg0) throws EJBException,
			RemoteException {
		// TODO Auto-generated method stub
		
	}
	

	
}