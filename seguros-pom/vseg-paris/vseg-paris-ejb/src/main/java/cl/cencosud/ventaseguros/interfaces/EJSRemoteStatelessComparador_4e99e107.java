package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessComparador_4e99e107
 */
public class EJSRemoteStatelessComparador_4e99e107 extends EJSWrapper implements Comparador {
	/**
	 * EJSRemoteStatelessComparador_4e99e107
	 */
	public EJSRemoteStatelessComparador_4e99e107() throws java.rmi.RemoteException {
		super();	}
	/**
	 * enviarComparador
	 */
	public boolean enviarComparador(java.lang.String emailCliente, java.lang.String html) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = emailCliente;
				_jacc_parms[1] = html;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.enviarComparador(emailCliente, html);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * isProductoHabilitado
	 */
	public boolean isProductoHabilitado(java.lang.Long idProducto) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idProducto;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.isProductoHabilitado(idProducto);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * getCaracteristicasPorPlan
	 */
	public java.util.List getCaracteristicasPorPlan(java.lang.Integer idPlan, java.lang.Integer idEstado) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idPlan;
				_jacc_parms[1] = idEstado;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.getCaracteristicasPorPlan(idPlan, idEstado);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerCaracteristicasCompartidas
	 */
	public java.util.List obtenerCaracteristicasCompartidas(java.lang.Long idProducto, java.lang.Integer estado) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idProducto;
				_jacc_parms[1] = estado;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCaracteristicasCompartidas(idProducto, estado);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerCaracteristicasCompartidasFront
	 */
	public java.util.List obtenerCaracteristicasCompartidasFront(java.lang.Long idProducto, java.lang.Integer idEstado) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idProducto;
				_jacc_parms[1] = idEstado;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCaracteristicasCompartidasFront(idProducto, idEstado);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerCaracteristicasPorPlan
	 */
	public java.util.List obtenerCaracteristicasPorPlan(int idPlan, int tipoCaracteristica, int estado) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = new java.lang.Integer(idPlan);
				_jacc_parms[1] = new java.lang.Integer(tipoCaracteristica);
				_jacc_parms[2] = new java.lang.Integer(estado);
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCaracteristicasPorPlan(idPlan, tipoCaracteristica, estado);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPlanesPorProducto
	 */
	public java.util.List obtenerPlanesPorProducto(int estado, java.lang.Long idProducto) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Integer(estado);
				_jacc_parms[1] = idProducto;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPlanesPorProducto(estado, idProducto);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerProductos
	 */
	public java.util.List obtenerProductos(int idRama, int idSubcategoria, java.lang.Long idProducto, int idTipo) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = new java.lang.Integer(idRama);
				_jacc_parms[1] = new java.lang.Integer(idSubcategoria);
				_jacc_parms[2] = idProducto;
				_jacc_parms[3] = new java.lang.Integer(idTipo);
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerProductos(idRama, idSubcategoria, idProducto, idTipo);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * activarProducto
	 */
	public void activarProducto(java.lang.Long idProducto, int estado) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idProducto;
				_jacc_parms[1] = new java.lang.Integer(estado);
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			beanRef.activarProducto(idProducto, estado);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * actualizarHabilitadoPorPlan
	 */
	public void actualizarHabilitadoPorPlan(int idPlan, int habilitado, int tipoCaract) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = new java.lang.Integer(idPlan);
				_jacc_parms[1] = new java.lang.Integer(habilitado);
				_jacc_parms[2] = new java.lang.Integer(tipoCaract);
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 9, _EJS_s, _jacc_parms);
			beanRef.actualizarHabilitadoPorPlan(idPlan, habilitado, tipoCaract);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * guardarComparador
	 */
	public void guardarComparador(long rutCliente, java.util.LinkedHashMap mapResultComparador) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Long(rutCliente);
				_jacc_parms[1] = mapResultComparador;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 10, _EJS_s, _jacc_parms);
			beanRef.guardarComparador(rutCliente, mapResultComparador);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 10, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * guardarContenidoCaracteristicasCompartidas
	 */
	public void guardarContenidoCaracteristicasCompartidas(java.util.LinkedHashMap mapCaractComp, cl.tinet.common.seguridad.model.UsuarioInterno usuario, java.lang.String idProducto) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = mapCaractComp;
				_jacc_parms[1] = usuario;
				_jacc_parms[2] = idProducto;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 11, _EJS_s, _jacc_parms);
			beanRef.guardarContenidoCaracteristicasCompartidas(mapCaractComp, usuario, idProducto);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 11, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * guardarContenidoComparadorProducto
	 */
	public void guardarContenidoComparadorProducto(java.util.LinkedHashMap mapCaracteristicas, java.util.LinkedHashMap mapPlanCaracteristica, cl.tinet.common.seguridad.model.UsuarioInterno usuario, java.lang.String habilitado) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = mapCaracteristicas;
				_jacc_parms[1] = mapPlanCaracteristica;
				_jacc_parms[2] = usuario;
				_jacc_parms[3] = habilitado;
			}
	cl.cencosud.ventaseguros.ejb.ComparadorBean beanRef = (cl.cencosud.ventaseguros.ejb.ComparadorBean)container.preInvoke(this, 12, _EJS_s, _jacc_parms);
			beanRef.guardarContenidoComparadorProducto(mapCaracteristicas, mapPlanCaracteristica, usuario, habilitado);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 12, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
}
