package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida;
import cl.cencosud.ventaseguros.common.Ficha;
import cl.cencosud.ventaseguros.common.LegalFicha;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.common.exception.FichaSubcategoriaException;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.fichasubcategoria.dao.FichaSubcategoriaDAO;

import com.tinet.exceptions.system.SystemException;

/**
 * @author Francisco Mendoza (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="FichaSubcategoria"
 *           display-name="FichaSubcategoria"
 *           description="Servicios relacionados a la ficha subcategoria."
 *           jndi-name="ejb/ventaseguros/FichaSubcategoria"
 *           type="Stateless"
 *           view-type="remote"
 *
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/VSPDS"
 *           
 * @ejb.transaction type="Supports"
 */
public class FichaSubcategoriaBean implements SessionBean {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -332601374972402102L;

    /**
     * ejbCreate.
     */
    public void ejbCreate() {
        //Create.
    }

    /**
     * ejbActivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbActivate() throws EJBException, RemoteException {
        // activate.

    }

    /**
     * ejbPassivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbPassivate() throws EJBException, RemoteException {
        //Passivate.

    }

    /**
     * ejbRemove.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbRemove() throws EJBException, RemoteException {
        //Remove.

    }

    /**
     * setSessionContext.
     * @param arg0
     * @throws EJBException
     * @throws RemoteException
     */
    public void setSessionContext(SessionContext arg0) throws EJBException,
        RemoteException {
        // seccioncontext.

    }

    /**
     * Obtiene un listado de fichas asociadas a una subcategoria.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de fichas.
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerFichasSubcategoria(int idRama,
        int idSubcategoria) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerFichasSubcategoria(idRama, idSubcategoria);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene un listado de planes en promocion asociadas a una subcategoria.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de fichas.
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerPlanesPromocion(int idRama,
        int idSubcategoria) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerPlanesPromocion(idRama, idSubcategoria);
        } finally {
            dao.close();
        }
    }

    /**
     * Agrega una ficha de producto.
     * @param ficha Ficha a agregar.
     * @return identificador de la ficha agregada.
     * @ejb.interface-method "remote"
     */
    public long agregarFichaProducto(Ficha ficha) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.agregarFichaProducto(ficha);
        } finally {
            dao.close();
       }
    }
    
    //CBM
    
    public void agregarLegalFichaProducto(LegalFicha legalFicha) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
             dao.agregarLegalFichaProducto(legalFicha);
        } finally {
            dao.close();
       }
    }
    /**
     * Actualiza una ficha de producto.
     * @param ficha Ficha a actualizar.
     * @ejb.interface-method "remote"
     */
    public void actualizarFichaProducto(Ficha ficha) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            dao.actualizarFichaProducto(ficha);
        } finally {
            dao.close();
        }
    }

    //CBM
    
    public void actualizarLegalFichaProducto(LegalFicha legalFicha){
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            dao.actualizarLegalFichaProducto(legalFicha);
        } finally {
            dao.close();
        }
    }
    
    /**
     * @ejb.interface-method "remote"
     */
    public void subirArchivo(long id_ficha, byte[] bios, String tipo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            //validar existencia previa del archivo.
            long id_archivo = dao.buscarArchivo(id_ficha, tipo);
            if (id_archivo > 0) {
                dao.actualizarArchivo(id_archivo, id_ficha, bios, tipo);
            } else {
                dao.subirArchivo(id_ficha, bios, tipo);
            }
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerArchivo(long id_ficha, String tipo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerArchivo(id_ficha, tipo);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public List < PlanLeg > obtenerPlanes(int idSubcategoria) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerPlanes(idSubcategoria);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public Ficha obtenerFicha(Long id_ficha) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerFicha(id_ficha);
        } finally {
            dao.close();
        }
    }
    
    //CBM legal
    public LegalFicha obtenerLegalFicha(Long id_ficha) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerLegalFicha(id_ficha);
        } finally {
            dao.close();
        }
    }
    

    /**
     * @ejb.interface-method "remote"
     */
    public PlanLeg obtenerDatosPlan(long idPlan) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerDatosPlan(idPlan);
        } finally {
            dao.close();
        }
    }

    /**
     * Identifica si tiene prima unica un determinado plan.
     * @param idRama
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean existePrimaUnica(long idPlan) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.existePrimaUnica(idPlan);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene las restricciones de un plan.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public RestriccionVida obtenerRestriccionVida(long idPlan) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            return dao.obtenerRestriccionVida(idPlan);

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Se encarga de manejar los datos de un plan.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public void administrarPlan(PlanLeg plan, RestriccionVida restriccion,
        boolean primaUnica, int rama) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        FichaSubcategoriaDAO dao =
            factory.getVentaSegurosDAO(FichaSubcategoriaDAO.class);
        try {
            //Grabar datos generales del plan.
            PlanLeg planBD = obtenerDatosPlan(plan.getId_plan_vseg());
            if (planBD == null) {
                throw new FichaSubcategoriaException(
                    FichaSubcategoriaException.PLAN_NO_ENCONTRADO,
                    new Object[] { plan.getId_plan() });
            }
            dao.actualizarPlanLeg(plan);

            if (!"".equals(plan.getCodigo_cia_leg())) {
                if (planBD.getCodigo_cia_leg() == null) {
                    //Crear plan cia
                    dao.agregarPlanCompannia(plan);
                } else {
                    //Actualizar plan cia.
                    dao.actualizarPlanCompannia(plan);
                }
            }

            //Grabar restriccion vida.
            if (rama == 3) {
                RestriccionVida restriccionBD =
                    obtenerRestriccionVida(plan.getId_plan_vseg());
                if (restriccionBD != null && restriccionBD.getIdPlan() > 0) {
                    //Actualizar
                    dao.actualizaRestriccionVida(restriccion);
                } else {
                    //Crear
                    dao.agregarRestriccionVida(restriccion);
                }
            }

            //Grabar prima Unica.
            boolean primaBD = existePrimaUnica(plan.getId_plan_vseg());
            if (primaUnica && !primaBD) {
                //Crear registro.
                dao.agregarPrimaUnica(plan);
            } else if (!primaUnica && primaBD) {
                //Eliminar registro
                dao.eliminarPrimaUnica(plan);
            }

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }
}
