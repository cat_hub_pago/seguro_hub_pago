package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import cl.cencosud.ventaseguros.asesorvirtual.dao.AsesorVirtualDAO;
import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Preguntas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Respuestas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.tinet.common.model.exception.BusinessException;

/**
 * @author Roberto San Martín (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="AsesorVirtual"
 *           display-name="AsesorVirtual"
 *           description="Servicios relacionados a asesor virtual."
 *           jndi-name="ejb/ventaseguros/AsesorVirtual"
 *           type="Stateless"
 *           view-type="remote"
 *
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/VSPDS"
 *           
 * @ejb.transaction type="Supports"
 */
public class AsesorVirtualBean implements SessionBean {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 8177626271904259384L;

    /**
     * ejbCreate.
     */
    public void ejbCreate() {
        //Create.
    }

    /**
     * ejbActivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbActivate() throws EJBException, RemoteException {
        // activate.

    }

    /**
     * ejbPassivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbPassivate() throws EJBException, RemoteException {
        //Passivate.

    }

    /**
     * ejbRemove.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbRemove() throws EJBException, RemoteException {
        //Remove.

    }

    /**
     * setSessionContext.
     * @param arg0
     * @throws EJBException
     * @throws RemoteException
     */
    public void setSessionContext(SessionContext arg0) throws EJBException,
        RemoteException {
        // seccioncontext.

    }

    /**
     * Almacena en base de datos el objeto que define el arbol.
     * @param sitio Objeto que define el arbol.
     * @param username Usuario que genera la ejecucion.
     * @param idArbol Identificador del arbol.
     * @throws BusinessException Cuado no puede grabar.
     * @ejb.interface-method "remote"
     */
    public void grabarArbol(Sitio sitio, String username, int idArbol)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            //Borrar arbol anterior.
            this.borrarArbol(dao);

            //Grabar arbol
            dao.grabarArbol(idArbol, username);

            //Recorrer el arbol para grabar.
            List < Seccion > secciones = sitio.getSecciones();

            for (Seccion seccion : secciones) {
                //Grabar seccion.
                dao.grabarSeccion(seccion, idArbol, username);

                List < Pregunta > preguntas =
                    seccion.getPreguntas().getPreguntas();

                for (Pregunta pregunta : preguntas) {
                    //Grabar pregunta.
                    dao.grabarPregunta(pregunta, seccion.getId(), username);
                    List < Alternativa > alternativas =
                        pregunta.getAlternativas();

                    for (Alternativa alternativa : alternativas) {
                        //Grabar alternativa
                        dao.grabarAlternativa(alternativa, pregunta.getId(),
                            seccion.getId(), username);
                    }
                }

                List < Resultado > resultados =
                    seccion.getRespuestas().getResultados();
                for (Resultado resultado : resultados) {
                    //Grabar resultado.
                    dao.grabarResultado(resultado, seccion.getId(), username);
                }
            }
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene el arbol a partir de lo almancenado en la base de datos.
     * @param idArbol Identificador del arbol.
     * @return Sitio objeto arbol.
     * @ejb.interface-method "remote"
     */
    public Sitio cargarArbol(int idArbol) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            Sitio sitio = new Sitio();
            List < Map < String, Object > > lSitio = dao.buscarArbol(idArbol);
            for (Map < String, Object > mSitio : lSitio) {

                Number idSitio = (Number) mSitio.get("id_arbol");

                List < Seccion > lArbolSecciones = new ArrayList < Seccion >();

                List < Map < String, Object > > lSecciones =
                    dao.buscarSecciones(idSitio.intValue());
                for (Map < String, Object > mSeccion : lSecciones) {
                    Number idSeccion = (Number) mSeccion.get("id_preocupacion");

                    Preguntas arbolPreguntas = new Preguntas();

                    List < Pregunta > lArbolPreguntas =
                        cargarPreguntas(dao, idSeccion.intValue());

                    arbolPreguntas.setCantidadpreguntas(lArbolPreguntas.size());
                    arbolPreguntas.setIdPrimeraPregunta(0);
                    arbolPreguntas.setPreguntas(lArbolPreguntas);

                    Respuestas arbolRespuestas = new Respuestas();
                    List < Resultado > lArbolResultados =
                        cargarResultados(dao, idSeccion.intValue());

                    arbolRespuestas.setResultados(lArbolResultados);

                    Seccion arbolSeccion = new Seccion();
                    arbolSeccion.setId(idSeccion.intValue());
                    arbolSeccion.setTitulo(getStringValue(mSeccion
                        .get("titulo_preocupacion")));
                    arbolSeccion.setPreguntas(arbolPreguntas);
                    arbolSeccion.setRespuestas(arbolRespuestas);

                    lArbolSecciones.add(arbolSeccion);
                }
                sitio.setSecciones(lArbolSecciones);
            }

            return sitio;
        } finally {
            dao.close();
        }

    }

    /**
     * Obtiene los valores de los resultados.
     * @param dao Conexion a la base de datos.
     * @param idSeccion Identificador de la seccion.
     * @return Listado de resultados.
     */
    private List < Resultado > cargarResultados(AsesorVirtualDAO dao,
        int idSeccion) {
        List < Resultado > lArbolResultados = new ArrayList < Resultado >();

        List < Map < String, Object > > lResultados =
            dao.buscarResultados(idSeccion);
        for (Map < String, Object > mResultado : lResultados) {
            Resultado resultado = new Resultado();
            resultado.setId(getIntValue(mResultado.get("id_resultado")));
            resultado.setTitulo(getStringValue(mResultado
                .get("titulo_resultado")));
            resultado.setNombreplan(getStringValue(mResultado
                .get("nombre_plan")));
            resultado.setLink0(getStringValue(mResultado.get("link0")));
            resultado.setLink1(getStringValue(mResultado.get("link1")));
            resultado.setIcono(getStringValue(mResultado.get("icono")));
            resultado.setValue(getStringValue(mResultado.get("valor")));

            lArbolResultados.add(resultado);
        }
        return lArbolResultados;
    }

    /**
     * Obtiene los valores de las preguntas.
     * @param dao Conexion a la base de datos.
     * @param idSeccion Identificador de seccion.
     * @return Listado de preguntas.
     */
    private List < Pregunta > cargarPreguntas(AsesorVirtualDAO dao,
        int idSeccion) {
        List < Pregunta > lArbolPreguntas = new ArrayList < Pregunta >();

        List < Map < String, Object > > lPreguntas =
            dao.buscarPreguntas(idSeccion);
        for (Map < String, Object > mPregunta : lPreguntas) {

            Pregunta pregunta = new Pregunta();
            pregunta.setTipoPregunta(getStringValue(mPregunta
                .get("tipo_pregunta")));
            pregunta
                .setTitulo(getStringValue(mPregunta.get("titulo_pregunta")));

            Number idPregunta = (Number) mPregunta.get("id_pregunta");

            pregunta.setId(idPregunta.intValue());

            //Obtiene las alternativas de la pregunta.
            List < Alternativa > lArbolAlternativas =
                cargarAlternativas(dao, idSeccion, idPregunta.intValue());

            pregunta.setAlternativas(lArbolAlternativas);

            lArbolPreguntas.add(pregunta);
        }
        return lArbolPreguntas;
    }

    /**
     * Obtiene los valores de las alternativas.
     * @param dao Conexion a la base de datos.
     * @param idSeccion Identificador de seccion.
     * @param idPregunta Identificador de pregunta.
     * @return Listado de alternativas.
     */
    private List < Alternativa > cargarAlternativas(AsesorVirtualDAO dao,
        int idSeccion, int idPregunta) {

        List < Alternativa > lArbolAlternativas =
            new ArrayList < Alternativa >();

        List < Map < String, Object > > lAlternativas =
            dao.buscarAlternativas(idSeccion, idPregunta);
        for (Map < String, Object > mAlternativa : lAlternativas) {
            Alternativa alternativa = new Alternativa();
            alternativa.setId(getIntValue(mAlternativa.get("id_alternativa")));
            alternativa.setProximaPregunta(getIntValue(mAlternativa
                .get("id_proxima_pregunta")));
            alternativa.setRespuesta(getIntValue(mAlternativa
                .get("id_resultado")));
            alternativa.setTexto0(getStringValue(mAlternativa
                .get("texto_alternativa_0")));
            alternativa.setTexto1(getStringValue(mAlternativa
                .get("texto_alternativa_1")));
            alternativa.setValor0(getStringValue(mAlternativa
                .get("valor_alternativa_0")));
            alternativa.setValor1(getStringValue(mAlternativa
                .get("valor_alternativa_1")));

            lArbolAlternativas.add(alternativa);
        }
        return lArbolAlternativas;
    }

    /**
     * Obtiene el valor de un objeto como Integer.
     * @param val Objeto a transformar.
     * @return Integer.
     */
    private Integer getIntValue(Object val) {
        Integer res = -1;
        if (val != null) {
            Number num = (Number) val;
            res = num.intValue();
        }
        return res;
    }

    /**
     * Obtiene el valor de un Objeto como String.
     * @param val Objeto a transformar.
     * @return String.
     */
    private String getStringValue(Object val) {
        String res = "";
        if (val != null) {
            res = (String) val;
        }
        return res;
    }

    /**
     * @ejb.interface-method "remote"
     */
    public List < Map < String, Object > > buscarArbol(int idArbol) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            return dao.buscarArbol(idArbol);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public void resultadoGrabar(Resultado resultado, int idSeccion,
        String username) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            dao.grabarResultado(resultado, idSeccion, username);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public void actualizarAlternativa(int idAlternativa, int idSeccion,
        int idPregunta, int idRespuesta, String username) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            //  dao.actualizarAlternativa(idAlternativa, idSeccion, idPregunta,
            //    idRespuesta, username);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public void grabarAlternativa(Alternativa alternativa, int idPregunta,
        int idSeccion, String username) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            dao.grabarAlternativa(alternativa, idPregunta, idSeccion, username);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public void grabarPregunta(Pregunta pregunta, int idSeccion, String username) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            dao.grabarPregunta(pregunta, idSeccion, username);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public void actualizarProximaPregunta(int idAlternativa, int idSeccion,
        int idPregunta, int idProxPregunta, String username) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            //  dao.actualizarProximaPregunta(idAlternativa, idSeccion, idPregunta,
            //    idProxPregunta, username);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public boolean sincronizarXML(String rutaLocal) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        boolean estado = false;
        try {

            //sincronizar primer servidor.
            boolean serv1 = this.sincronizarArchivo("server1", rutaLocal);

            //sincronizar segundo servidor.
            boolean serv2 = this.sincronizarArchivo("server2", rutaLocal);

            if (serv1 && serv2) {
                estado = true;
            }

            return estado;

        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método sincronizarArchivo.
     * @param nodo
     * @param rutaLocal
     */
    private boolean sincronizarArchivo(String nodo, String rutaLocal) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        AsesorVirtualDAO dao =
            factory.getVentaSegurosDAO(AsesorVirtualDAO.class);
        try {
            return dao.sincronizarArchivo(nodo, rutaLocal);
        } finally {
            dao.close();
        }
    }

    /**
     * Borra el arbol anterior.
     * @param dao instancia del dao.
     */
    private void borrarArbol(AsesorVirtualDAO dao) {

        //   dao.borrarAlternativas();
        //   dao.borrarResultados();
        //   dao.borrarPreguntas();
        //   dao.borrarPreocupaciones();
        //   dao.borrarArbol();
    }
}
