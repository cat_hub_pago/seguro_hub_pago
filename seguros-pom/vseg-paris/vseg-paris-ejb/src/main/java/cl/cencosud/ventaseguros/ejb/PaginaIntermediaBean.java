package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.common.TipoRama;
import cl.cencosud.ventaseguros.common.exception.PaginaIntermediaException;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.paginaintermedia.dao.PaginaIntermediaDAO;
import cl.tinet.common.model.exception.BusinessException;

/**
 * @author Roberto San Martín (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="PaginaIntermedia"
 *           description="Servicios relacionados a la pagina intermedia."
 *           display-name="PaginaIntermedia"
 *           jndi-name="ejb/ventaseguros/PaginaIntermedia"
 *           type="Stateless"
 *           view-type="remote"
 *
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/VSPDS"
 *           
 * @ejb.transaction type="Supports"
 */
public class PaginaIntermediaBean implements SessionBean {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -8138403145564178331L;

    /**
     * ejbCreate.
     */
    public void ejbCreate() {
        //Create.
    }

    /**
     * ejbActivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbActivate() throws EJBException, RemoteException {
        // activate.

    }

    /**
     * ejbPassivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbPassivate() throws EJBException, RemoteException {
        //Passivate.

    }

    /**
     * ejbRemove.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbRemove() throws EJBException, RemoteException {
        //Remove.

    }

    /**
     * setSessionContext.
     * @param arg0
     * @throws EJBException
     * @throws RemoteException
     */
    public void setSessionContext(SessionContext arg0) throws EJBException,
        RemoteException {
        // seccioncontext.

    }

    /**
     * Obtiene un listado de ramas de productos.
     * @return Listado de ramas.
     * @ejb.interface-method "remote"
     */
    public List < Rama > obtenerRamas() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerRamas();
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene un listado de subcategorias de productos.
     * @return Listado de subcategorias.
     * @param idRama Identificador de la rama.
     * @ejb.interface-method "remote"
     */
    public List < Subcategoria > obtenerSubcategorias(int idRama) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerSubcategorias(idRama);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene un listado de subcategorias de productos.
     * @return Listado de subcategorias.
     * @param idRama Identificador de la rama.
     * @param idRama Identificador del tipo de subcategoria.
     * @ejb.interface-method "remote"
     */
    public List < Subcategoria > obtenerSubcategoriasTipo(int idRama, int idTipo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerSubcategoriasTipo(idRama, idTipo);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene un listado de tipos de rama.
     * @return Listado de tipos de rama.
     * @param idRama Identificador de la rama.
     * @ejb.interface-method "remote"
     */
    public List < TipoRama > obtenerTiposRama(int idRama) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerTiposRama(idRama);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene un listado de subcategorias asociadas a una rama.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de subcategorias.
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerListaPaginaIntermedia(int idRama,
        int idSubcategoria) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerListaPaginaIntermedia(idRama, idSubcategoria);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene los datos de una subcatrgoria a partir del id especificado.
     * @param idSubcategoria identificador de subcategoria.
     * @return Subcatogoria.
     * @ejb.interface-method "remote"
     */
    public Subcategoria obtenerSubcategoria(int idSubcategoria) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerSubcategoria(idSubcategoria);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene un listado de producto que tienen relacion con la subcategoria.
     * @param idSubcategoria Identificador de la subcategoria.
     * @param asignado Si es verdadero, se obtienen los productos asociados a 
     * la subcategoria, si es falso, se obtienen los productos disponibles 
     * para asociar a la subcategoria.
     * @param nombre Nombre del producto a buscar.
     * @return Listado de productos.
     * @ejb.interface-method "remote"
     */
    public List < ProductoLeg > obtenerListaProductos(int idSubcategoria,
        boolean asignado, String nombre) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerListaProductos(idSubcategoria, asignado, nombre);
        } finally {
            dao.close();
        }
    }

    /**
     * Agrega un producto a un a subcategoria.
     * @param idSubcategoria identificador de subcategoria.
     * @param idProductoLeg identificador de producto legacy.
     * @return identificador de insercion.
     * @ejb.interface-method "remote"
     */
    public long agregarProducto(int idSubcategoria, int idProductoLeg) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        long res = -1;
        try {
            if (dao.existeProducto(idSubcategoria, idProductoLeg)) {
                dao.activarProducto(idSubcategoria, idProductoLeg);
                res = 1;
            } else {
                res = dao.agregarProducto(idSubcategoria, idProductoLeg);
            }
        } finally {
            dao.close();
        }
        return res;
    }

    /**
    * @ejb.interface-method "remote"
    */
    public void eliminarProducto(int idSubcategoria, int idProductoLeg) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            dao.eliminarProducto(idSubcategoria, idProductoLeg);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public void modificarSubcategoria(int idSubcategoria, String titulo,
        int estado) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            dao.modificarSubcategoria(idSubcategoria, titulo, estado);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene un listado de subcategorias disponibles para la Página
     * Intermedia.
     * @param idRama Identificador de la rama..
     * @return Listado de subcategorias disponibles.
     * @ejb.interface-method "remote"
     */
    public List < Subcategoria > obtenerSubcategoriasPaginaIntermedia(int idRama) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerSubcategoriasPaginaIntermedia(idRama);
        } finally {
            dao.close();
        }
    }

    /**
     * Agrega una nueva subcategoria a la rama.
     * @param idRama identificador de rama
     * @param subcategoria Titulo de la subcategoria.
     * @return codigo de la nueva subcategoria.
     * @throws BusinessException En caso de que exista la subcategoria.
     * @ejb.interface-method "remote"
     */
    public long agregarSubcategoria(int idRama, String subcategoria, int idTipo)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            List < Subcategoria > oList = dao.obtenerSubcategorias(idRama);

            if (oList != null && !oList.isEmpty()) {

                for (Iterator < Subcategoria > oIterator = oList.iterator(); oIterator
                    .hasNext();) {
                    Subcategoria elemento = (Subcategoria) oIterator.next();

                    if (elemento.getTitulo_subcategoria().equalsIgnoreCase(
                        subcategoria)) {
                        throw new PaginaIntermediaException(
                            PaginaIntermediaException.SUBCATEGORIA_EXISTENTE,
                            new Object[] { subcategoria });
                    }
                }

            }
            return dao.agregarSubcategoria(idRama, subcategoria, idTipo);

        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene las promociones que se encuentran en el sitio 
     * @param tipo Tipo promocion.
     * @param subcategoria Principal o Secundaria.
     * @return Listado de promociones disponibles.
     * @ejb.interface-method "remote"
     */
    public List <HashMap<String, Object>> obtenerPromociones(String tipo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerPromociones(tipo);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene las promociones que se encuentran en el sitio 
     * @param idRama rama de las promociones.
     * @return Listado de promociones disponibles.
     * @ejb.interface-method "remote"
     */
    public List <HashMap<String, Object>> obtenerPromocionesSecundarias(String idRama) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        PaginaIntermediaDAO dao =
            factory.getVentaSegurosDAO(PaginaIntermediaDAO.class);
        try {
            return dao.obtenerPromocionesSecundarias(idRama);
        } finally {
            dao.close();
        }
    }
}
