package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.common.exception.ClientesException;
import cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.MantenedorClientesDAO;
import cl.cencosud.ventaseguros.mantenedorusuarios.dao.MantenedorUsuariosCencosudDAO;
import cl.tinet.common.mail.Mail;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.delegate.GestorDeSeguridadDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.model.UsuarioInterno;
import cl.tinet.common.util.validate.ValidacionUtil;

import com.tinet.exceptions.system.SystemException;

/**
 * @author Roberto San Martín (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="MantenedorUsuarios" display-name="MantenedorUsuarios"
 *           description="Servicios relacionado con el mantenedor de usuario"
 *           jndi-name="ejb/ventaseguros/MantenedorUsuarios" type="Stateless"
 *           view-type="remote"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/seguridadDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/seguridadDS"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/VSPDS"
 * 
 * @ejb.transaction type="Supports"
 */
public class MantenedorUsuariosBean implements SessionBean {

    /**
     * Logger de la clase.
     */
    private static final Log LOGGER =
        LogFactory.getLog(MantenedorUsuariosBean.class);

    /**
     * Identificador de la clase para serialización.
     */
    private static final long serialVersionUID = -5376284956145498632L;

    /**
     * Metodo create.
     */
    public void ejbCreate() {
        // Create.
    }

    /**
     * Obtiene los roles.
     * 
     * @return Lista de roles
     * @ejb.interface-method "remote"
     */
    public List < Map > obtenerRoles() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        try {
            return (List) dao.obtenerRoles();
        } finally {
            dao.close();
        }
    }

    public int cantUsuarioPersonalCenco(String userName) {

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        try {
            return (int) dao.cantUsuarioPersonalCenco(userName);
        } finally {
            dao.close();
        }
    }

    /**
     * Insert una persona.
     * 
     * @param Map
     *            .
     * @return boolean
     * @ejb.interface-method "remote"
     * 
     */
    public boolean insertarNuevoPersonalCenco(Map mapdatos)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        int cantUsuario = 0;
        boolean resultado = false;
        try {
            String userName = (String) mapdatos.get("userName");
            //            String password = (String) mapdatos.get("password");
            //            if (validacionLDAP(userName, password)) {

            cantUsuario = dao.cantUsuarioPersonalCenco(userName);
            if (cantUsuario == 0) {
                // No se pueden repetir, dos usuarios con el mismo userName
                if (dao.insertarNuevoPersonalCencosud(mapdatos)) {

                    String correo = dao.obtenerCorreoUsuario(userName);
                    this.enviarCorreo(userName, "Se activado su cuenta: "
                        + userName, correo);
                    return true;
                }
            } else {
                throw new ValidacionUsuarioException(
                    ValidacionUsuarioException.USUARIO_EXISTE,
                    new Object[] { userName });
            }
            //            } else {
            //                throw new ValidacionUsuarioException(
            //                    ValidacionUsuarioException.USUARIO_NO_VALIDO,
            //                    new Object[] { userName });
            //            }
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Modificar Una personal cenco con su UserName.
     * 
     * @param nombre
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public boolean modificarPersonalCenco(String userName) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado;
        try {
            resultado = dao.modificarPersonalCencosud(userName);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Modificar Una personal cenco con su Id.
     * 
     * @param Id
     *            .
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public boolean modificarPersonalCenco(Integer IdUsuarioInt) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado;
        try {
            resultado = dao.modificarPersonalCenco(IdUsuarioInt);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Obtiene una lista de los usuarios de cencosud Internos.
     * 
     * @ejb.interface-method "remote"
     * @return lis de personas cenco
     */
    public List < Map > obtenerPersonalCenco() {

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        List < Map > resultado;
        try {
            resultado = dao.obtenerPersonalCenco();
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Obtiene una lista de paises.
     * 
     * @return lista
     * @ejb.interface-method "remote"
     */
    public List < Map > obtenerPaises() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        List < Map > resultado;
        try {
            resultado = dao.obtenerPaises();
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Obtiene el Id de la persona con el Username.
     * 
     * @return Id del usuario
     * @ejb.interface-method "remote"
     */
    public Integer obtenerIdPersonalCenco(String userName) {

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        Integer resultado;
        try {
            resultado = dao.obtenerIdPersonalCenco(userName);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Obtiene la cantidad total de personas internas.
     * 
     * @return Integer cantidad de personas
     * @ejb.interface-method "remote"
     */
    public Integer obtenerCantidadTotalPersonalCenco() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        Integer cant;
        try {
            cant = dao.obtenerCantidadTotalPersonalCenco();
        } finally {
            dao.close();
        }
        return cant;
    }

    /**
     * Elimina una persona por su ID.
     * 
     * @param IdUsuarioInt
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public boolean eliminarPersonalCenco(Integer IdUsuarioInt) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado = false;
        try {
            resultado = dao.eliminarPersonalCenco(IdUsuarioInt);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Elimina una persona por su userName.
     * 
     * @param userName
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public boolean eliminarPersonaCenco(String userName) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado = false;
        try {
            resultado = dao.eliminarPersonalCenco(userName);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Obtiene un @see UsuarioInterno con el usuerName.
     * 
     * @param userName
     * @param usuarioInterno
     * @ejb.interface-method "remote"
     */
    public UsuarioInterno obtenerUsuarioInternoCenco(String userName) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        UsuarioInterno usuarioInterno = null;
        try {
            usuarioInterno = dao.obtenerUsuarioInternoCenco(userName);
        } finally {
            dao.close();
        }
        return usuarioInterno;
    }

    /**
     * Envia un correo electronico al destinatario.
     * 
     * @param userName
     * @param mensaje
     * @param email
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public boolean enviarCorreo(String userName, String mensaje, String email) {
        boolean resultado = false;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            String server = dao.obtenerDatosEmail("SERVER_CORREO");
            String from = dao.obtenerDatosEmail("MAIL_FROM");
            String asunto =
                dao.obtenerDatosEmail("MAIL_SUBJECT_CUENTA_CENCOSUD");

            // Enviar mail.
            Mail mail = new Mail(from, email, asunto, "", server);

            try {
                mail.addContenido(mensaje);
                mail.sendMail();
            } catch (Exception e) {
                LOGGER.error("Excepcion general al generar email", e);
            }
        } catch (Exception e) {
            LOGGER.error("Error al enviar correo: " + e, e);
            throw new SystemException(e);
        }

        return resultado;
    }

    /**
     * Comprueba la existencia del usuario.
     * 
     * @param userName
     * @return boolean de resultado
     * @ejb.interface-method "remote"
     */
    public boolean existenciaUsuario(String userName) throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado = false;
        try {
            resultado = dao.existenciaUsuario(userName);
            if (!resultado) {
                throw new ClientesException(
                    ClientesException.USUARIO_NO_ENCONTRADO,
                    new Object[] { userName });
            }
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Actualiza un usuario Interno.
     * 
     * @param map
     * @return userName, nombre de usuario
     * @ejb.interface-method "remote"
     */
    public boolean actualizarUsuarioCenco(Map mapdatos, String userName) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado = false;
        try {
            resultado = dao.actualizarUsuarioCenco(mapdatos, userName);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Cambia el estado de un usuario en especial.
     * 
     * @param userName
     * @param boolean
     * @return boolean de resultado
     * @ejb.interface-method "remote"
     */
    public boolean cambiarEstadoUsuarioCenco(String userName, boolean estado) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado = false;
        try {
            resultado = dao.cambiarEstadoUsuarioCenco(userName, estado);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo Activate EJB.
     * 
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbActivate() throws EJBException, RemoteException {
    }

    /**
     * Metodo passivate EJB.
     * 
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbPassivate() throws EJBException, RemoteException {
    }

    /**
     * Metodo Remove EJB.
     * 
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbRemove() throws EJBException, RemoteException {
    }

    /**
     * Metodo setSessionContext EJB.
     * 
     * @param ctx
     * @throws EJBException
     * @throws RemoteException
     */
    public void setSessionContext(SessionContext ctx) throws EJBException,
        RemoteException {
    }

    /**
     * Obtiene un usuario Externo desde un userName.
     * 
     * @param userName
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public UsuarioExterno obtenerUsuarioExternoCencosud(String userName)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        UsuarioExterno usuarioExterno = null;
        try {
            // Validar RUT cliente.
            String rut = userName.substring(0, userName.length() - 2);
            char dv =
                userName.substring(userName.length() - 1, userName.length())
                    .charAt(0);

            if (!ValidacionUtil.isValidoRUT(Long.valueOf(rut), dv)) {
                throw new ClientesException(ClientesException.RUT_NO_VALIDO,
                    new Object[] { userName });
            }

            usuarioExterno = dao.obtenerUsuarioExternoCencosud(userName);

            if (usuarioExterno == null) {
                throw new ClientesException(
                    ClientesException.USUARIO_NO_ENCONTRADO,
                    new Object[] { userName });
            }
        } finally {
            dao.close();
        }
        return usuarioExterno;

    }

    /**
     * Actualiza un cliente Externo.
     * 
     * @param datos
     * @param userName
     * @return boolean de resultado
     * @ejb.interface-method "remote"
     */
    public boolean actualizarClienteExternoCenco(Map datos, String userName) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        boolean resultado;
        try {
            resultado = dao.actualizarClienteExternoCenco(datos, userName);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Valida la existencia de un usuario en LDAP.
     * 
     * @param userName
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean validacionLDAP(String userName, String password) {
        boolean resultado = false;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        try {
            GestorDeSeguridadDelegate gestorDeSeguridadDelegate =
                new GestorDeSeguridadDelegate();
            resultado =
                gestorDeSeguridadDelegate.validacionLDAP(userName, password, 2);

            // resultado = dao.validacionLDAP(userName);
        } finally {
            dao.close();
        }

        return resultado;
    }

    /**
     * TODO Describir método obtenerRolUsuario.
     * 
     * @param userName
     * @return
     * @ejb.interface-method "remote"
     */
    public List < Map > obtenerRolUsuario(String userName) {
        List < Map > resultado = null;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        try {
            resultado = dao.obtenerRolUsuario(userName);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }

        return resultado;
    }

    /**
     * Obtiene el estado del un usuario.
     * 
     * @param userName
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public boolean obtenerEstadoUsuario(String userName) {
        boolean resultado = false;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        try {
            resultado = dao.obtenerEstadoUsuario(userName);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }

        return resultado;
    }

    /**
     * Obtiene el estado de la password de un usuario.
     * @param userName usuario
     * @return boolean estado
     * @ejb.interface-method "remote"
     */
    public boolean obtenerEstadoPasswordUsuario(String userName) {
        boolean resultado = false;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorUsuariosCencosudDAO dao =
            factory.getVentaSegurosDAO(MantenedorUsuariosCencosudDAO.class);
        try {
            resultado = dao.obtenerEstadoPasswordUsuario(userName);
        } finally {
            dao.close();
        }

        return resultado;
    }
}
