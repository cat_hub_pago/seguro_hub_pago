package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessMantenedorEventosHomeBean_5dbc7e54
 */
public class EJSStatelessMantenedorEventosHomeBean_5dbc7e54 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessMantenedorEventosHomeBean_5dbc7e54
	 */
	public EJSStatelessMantenedorEventosHomeBean_5dbc7e54() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.MantenedorEventos create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.MantenedorEventos result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.MantenedorEventos) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
