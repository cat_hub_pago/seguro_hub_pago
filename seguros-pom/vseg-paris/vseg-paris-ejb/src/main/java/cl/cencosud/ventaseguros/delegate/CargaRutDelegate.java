package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

import cl.cencosud.ventaseguros.interfaces.CargaRut;
import cl.cencosud.ventaseguros.interfaces.CargaRutHome;


public class CargaRutDelegate {
	
	/**
     * EJB.
     */
	private CargaRut cargaRuts;
	
	/**
     * Interfaz.
     */
    private CargaRutHome CargaRutHome;

    /**
     * Constructor Delegate.
     */
	
	public CargaRutDelegate() {
        try {
            this.CargaRutHome =
                (CargaRutHome) ServiceLocator.singleton()
                    .getRemoteHome(CargaRutHome.JNDI_NAME,
                    		CargaRutHome.class);
            this.cargaRuts = this.CargaRutHome.create();

        } catch (Exception e) {
            throw new SystemException(e);
        }
    }
	
	
	public boolean insertarRutArchivos(String[] rut) {
        boolean resultado = false;
        try {
            resultado = this.cargaRuts.insertarRutArchivos(rut);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }
}

