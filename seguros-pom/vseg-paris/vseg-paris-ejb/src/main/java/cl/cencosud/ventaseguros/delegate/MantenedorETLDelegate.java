package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;

import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg;
import cl.cencosud.ventaseguros.interfaces.MantenedorETL;
import cl.cencosud.ventaseguros.interfaces.MantenedorETLHome;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Clase que sirve de delegate entre el action y el EJB.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Oct 4, 2010
 */
public class MantenedorETLDelegate {

    /**
     * Atributo para EJB.
     */
    private MantenedorETL mantenedorETL;
    /**
     * Atributo para EJB.
     */
    private MantenedorETLHome mantenedorETLHome;

    /**
     * Constructor de la clase.
     */
    public MantenedorETLDelegate() {
        try {
            this.mantenedorETLHome =
                (MantenedorETLHome) ServiceLocator.singleton().getRemoteHome(
                    MantenedorETLHome.JNDI_NAME, MantenedorETLHome.class);

            this.mantenedorETL = this.mantenedorETLHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método obtenerTipoLeg.
     * @return
     */
    public TipoLeg[] obtenerTipoLeg() {
        try {
            return this.mantenedorETL.obtenerTipoLeg();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método obtenerSubTipoLeg.
     * @param codigo_tipo_prod_leg
     * @return
     */
    public SubtipoLeg[] obtenerSubTipoLeg(String codigo_tipo_prod_leg) {
        try {
            return this.mantenedorETL.obtenerSubTipoLeg(codigo_tipo_prod_leg);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método obtenerProductoLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @return
     */
    public ProductoLeg[] obtenerProductoLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg) {
        try {
            return this.mantenedorETL.obtenerProductoLeg(codigo_tipo_prod_leg,
                codigo_sub_tipo_prod_leg);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método obtenerPlanLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param codigo_producto_leg
     * @return
     */
    public PlanLeg[] obtenerPlanLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String codigo_producto_leg) {
        try {
            return this.mantenedorETL.obtenerPlanLeg(codigo_tipo_prod_leg,
                codigo_sub_tipo_prod_leg, codigo_producto_leg);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método enviarPlan.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param codigo_producto_leg
     * @param planes
     */
    public void enviarPlan(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String codigo_producto_leg,
        String[] planes) {
        try {
            this.mantenedorETL.enviarPlan(codigo_tipo_prod_leg,
                codigo_sub_tipo_prod_leg, codigo_producto_leg, planes);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método enviarProducto.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param productos
     */
    public void enviarProducto(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String[] productos) {
        try {
            this.mantenedorETL.enviarProducto(codigo_tipo_prod_leg,
                codigo_sub_tipo_prod_leg, productos);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
}
