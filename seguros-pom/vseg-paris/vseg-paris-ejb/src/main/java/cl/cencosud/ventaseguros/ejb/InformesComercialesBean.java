package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.informescomerciales.dao.InformesComercialesDAO;

/**
 * @author Francisco Mendoza (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="InformesComerciales"
 *           display-name="InformesComerciales"
 *           description="Servicios relacionados a obtener informes comerciales."
 *           jndi-name="ejb/ventaseguros/InformesComerciales"
 *           type="Stateless"
 *           view-type="remote"
 *
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/VSPDS"
 *           
 * @ejb.transaction type="Supports"
 */
public class InformesComercialesBean implements SessionBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4616249695248362835L;

    /**
     * ejbCreate.
     */
    public void ejbCreate() {
        //Create.
    }

    /**
     * ejbActivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbActivate() throws EJBException, RemoteException {
        // activate.

    }

    /**
     * ejbPassivate.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbPassivate() throws EJBException, RemoteException {
        //Passivate.

    }

    /**
     * ejbRemove.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbRemove() throws EJBException, RemoteException {
        //Remove.

    }

    /**
     * setSessionContext.
     * @param arg0
     * @throws EJBException
     * @throws RemoteException
     */
    public void setSessionContext(SessionContext arg0) throws EJBException,
        RemoteException {
        // seccioncontext.

    }

    /**
     * @ejb.interface-method "remote"
     */
    public List < HashMap < String, Object > > obtenerInformeComercialVehiculo(
        Date fechaDesde, Date fechaHasta, boolean esPaginado, int numeroPagina) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        InformesComercialesDAO dao =
            factory.getVentaSegurosDAO(InformesComercialesDAO.class);
        try {
            return dao.obtenerInformeComercialVehiculos(fechaDesde, fechaHasta,
                esPaginado, numeroPagina);
        } finally {
            dao.close();
        }
    }

    /**
     * @ejb.interface-method "remote"
     */
    public List < HashMap < String, Object > > obtenerInformeComercialTarjetas(
        Date fechaDesde, Date fechaHasta, boolean esPaginado, int numeroPagina) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        InformesComercialesDAO dao =
            factory.getVentaSegurosDAO(InformesComercialesDAO.class);
        try {
            return dao.obtenerInformeComercialTarjetas(fechaDesde, fechaHasta,
                esPaginado, numeroPagina);
        } finally {
            dao.close();
        }
    }
    /**
     * @ejb.interface-method "remote"
     */
    public List < HashMap < String, Object > > obtenerInformeComercialVentas(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, 
            int rama, int subcategoria, int id_plan, String compannia) {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.obtenerInformeComercialVentas(fechaDesde, fechaHasta,
                    esPaginado, numeroPagina, rama, subcategoria, id_plan, compannia);
            } finally {
                dao.close();
            }
        }
    /**
     * @ejb.interface-method "remote"
     */
    public List < HashMap < String, Object > > obtenerInformeComercialCotizaciones(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, 
            int rama, int subcategoria, int id_plan, String compannia) {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.obtenerInformeComercialCotizaciones(fechaDesde, fechaHasta,
                    esPaginado, numeroPagina, rama, subcategoria, id_plan, compannia);
            } finally {
                dao.close();
            }
        }
    
    /**
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerArchivoFactura(long id_factura) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        InformesComercialesDAO dao =
            factory.getVentaSegurosDAO(InformesComercialesDAO.class);
        try {
            return dao.obtenerArchivoFactura(id_factura);
        } finally {
            dao.close();
        }
    }
    
    /**
     * @ejb.interface-method "remote"
     */
    public List < HashMap < String, Object > > obtenerVitrineoGeneral(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, 
            int rama, int subcategoria) {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.obtenerVitrineoGeneral(fechaDesde, fechaHasta,
                    esPaginado, numeroPagina, rama, subcategoria);
            } finally {
                dao.close();
            }
        }
    
    /**
     * @ejb.interface-method "remote"
     */
    public boolean ingresarPromocionPrincipal(
    		String principal, String tipo, String posicion,
			String nombre, String linkVer, String tracker, String imagen,
			Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, String linkConocer, String linkCotizar, String idPlan) {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.ingresarPromocionPrincipal(principal, tipo, posicion, nombre, linkVer, tracker, imagen,
                		fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar,idPlan);
            } finally {
                dao.close();
            }
        }

    
    /**
     * @ejb.interface-method "remote"
     */
    public boolean ingresarPromocionSecundaria(
    		String principal, String tipo,
    		String ordinal, String nombre, String linkVer, String tracker,
    		String imagen, Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, String linkConocer, String linkCotizar,String idPlan) {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.ingresarPromocionSecundaria(principal, tipo, ordinal, nombre, linkVer, tracker, imagen, fechaDesde, 
                		fechaHasta, tipoBanner, rama, linkConocer, linkCotizar,idPlan);
            } finally {
                dao.close();
            }
        }

    /**
     * @ejb.interface-method "remote"
     */
    public List < HashMap < String, Object > > obtenerPromocionesPrincipalBP(
            String tipoS) {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.obtenerPromocionesPrincipalBP(tipoS);
            } finally {
                dao.close();
            }
        }
    
    /**
     * @ejb.interface-method "remote"
     */
    public List<Map<String, Object>> obtenerPromocionesBO() {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.obtenerPromocionesBO();
            } finally {
                dao.close();
            }
        }
    
    /**
     * @ejb.interface-method "remote"
     */
    public List<Map<String, Object>> obtenerPromocionesBOPrincipal() {
            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            InformesComercialesDAO dao =
                factory.getVentaSegurosDAO(InformesComercialesDAO.class);
            try {
                return dao.obtenerPromocionesBOPrincipal();
            } finally {
                dao.close();
            }
        }
    
}
