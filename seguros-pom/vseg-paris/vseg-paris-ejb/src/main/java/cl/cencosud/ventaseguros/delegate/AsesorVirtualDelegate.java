package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.cencosud.ventaseguros.interfaces.AsesorVirtual;
import cl.cencosud.ventaseguros.interfaces.AsesorVirtualHome;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Delegate encargado de manejar lo referente al asesor virtual.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/08/2010
 */
public class AsesorVirtualDelegate {

    /**
     * Instancia de la clase FichaSubcategoria.
     */
    private AsesorVirtual asesorVirtual;

    /**
     * Home de FichaSubcategoria.
     */
    private AsesorVirtualHome asesorVirtualHome;

    /**
     * Constructor de FichaSubcategoriaDelegate.
     */
    public AsesorVirtualDelegate() {
        try {
            this.asesorVirtualHome =
                (AsesorVirtualHome) ServiceLocator.singleton().getRemoteHome(
                    asesorVirtualHome.JNDI_NAME, AsesorVirtualHome.class);

            this.asesorVirtual = this.asesorVirtualHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * .
     * @param sitio .
     * @param username .
     * @param idArbol .
     * @throws BusinessException 
     */
    public void grabarArbol(Sitio sitio, String username, int idArbol)
        throws BusinessException {
        try {
            this.asesorVirtual.grabarArbol(sitio, username, idArbol);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Carga el arbol desde la base de datos.
     * @param idArbol Identificador del arbol a cargar.
     * @return Arbol.
     */
    public Sitio cargarArbol(int idArbol) {
        try {
            return this.asesorVirtual.cargarArbol(idArbol);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public List < Map < String, Object > > buscarArbol(int idArbol) {
        try {
            return this.asesorVirtual.buscarArbol(idArbol);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public void resultadoGrabar(Resultado resultado, int idSeccion,
        String username) {
        try {
            this.asesorVirtual.resultadoGrabar(resultado, idSeccion, username);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public void actualizarAlternativa(int idAlternativa, int idSeccion,
        int idPregunta, int idRespuesta, String username) {
        try {
            this.asesorVirtual.actualizarAlternativa(idAlternativa, idSeccion,
                idPregunta, idRespuesta, username);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public void grabarAlternativa(Alternativa alternativa, int idPregunta,
        int idSeccion, String username) {
        try {
            this.asesorVirtual.grabarAlternativa(alternativa, idPregunta,
                idSeccion, username);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public void grabarPregunta(Pregunta pregunta, int idSeccion, String username) {
        try {
            this.asesorVirtual.grabarPregunta(pregunta, idSeccion, username);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public void actualizarProximaPregunta(int idAlternativa, int idSeccion,
        int idPregunta, int idProxPregunta, String username) {
        try {
            this.asesorVirtual.actualizarProximaPregunta(idAlternativa,
                idSeccion, idPregunta, idProxPregunta, username);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public boolean sincronizarXML(String rutaLocal) {
        try {
            return this.asesorVirtual.sincronizarXML(rutaLocal);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

}
