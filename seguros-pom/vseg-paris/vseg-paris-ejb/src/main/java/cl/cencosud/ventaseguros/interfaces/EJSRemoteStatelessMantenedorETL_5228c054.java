package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessMantenedorETL_5228c054
 */
public class EJSRemoteStatelessMantenedorETL_5228c054 extends EJSWrapper implements MantenedorETL {
	/**
	 * EJSRemoteStatelessMantenedorETL_5228c054
	 */
	public EJSRemoteStatelessMantenedorETL_5228c054() throws java.rmi.RemoteException {
		super();	}
	/**
	 * obtenerPlanLeg
	 */
	public cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg[] obtenerPlanLeg(java.lang.String codigo_tipo_prod_leg, java.lang.String codigo_sub_tipo_prod_leg, java.lang.String codigo_producto_leg) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = codigo_tipo_prod_leg;
				_jacc_parms[1] = codigo_sub_tipo_prod_leg;
				_jacc_parms[2] = codigo_producto_leg;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorETLBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorETLBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPlanLeg(codigo_tipo_prod_leg, codigo_sub_tipo_prod_leg, codigo_producto_leg);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerProductoLeg
	 */
	public cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg[] obtenerProductoLeg(java.lang.String codigo_tipo_prod_leg, java.lang.String codigo_sub_tipo_prod_leg) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = codigo_tipo_prod_leg;
				_jacc_parms[1] = codigo_sub_tipo_prod_leg;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorETLBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorETLBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerProductoLeg(codigo_tipo_prod_leg, codigo_sub_tipo_prod_leg);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerSubTipoLeg
	 */
	public cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg[] obtenerSubTipoLeg(java.lang.String codigo_tipo_prod_leg) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = codigo_tipo_prod_leg;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorETLBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorETLBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerSubTipoLeg(codigo_tipo_prod_leg);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerTipoLeg
	 */
	public cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg[] obtenerTipoLeg() throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[0];
			}
	cl.cencosud.ventaseguros.ejb.MantenedorETLBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorETLBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerTipoLeg();
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * enviarPlan
	 */
	public void enviarPlan(java.lang.String codigo_tipo_prod_leg, java.lang.String codigo_sub_tipo_prod_leg, java.lang.String codigo_producto_leg, java.lang.String[] planes) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = codigo_tipo_prod_leg;
				_jacc_parms[1] = codigo_sub_tipo_prod_leg;
				_jacc_parms[2] = codigo_producto_leg;
				_jacc_parms[3] = planes;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorETLBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorETLBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			beanRef.enviarPlan(codigo_tipo_prod_leg, codigo_sub_tipo_prod_leg, codigo_producto_leg, planes);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * enviarProducto
	 */
	public void enviarProducto(java.lang.String codigo_tipo_prod_leg, java.lang.String codigo_sub_tipo_prod_leg, java.lang.String[] productos) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = codigo_tipo_prod_leg;
				_jacc_parms[1] = codigo_sub_tipo_prod_leg;
				_jacc_parms[2] = productos;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorETLBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorETLBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			beanRef.enviarProducto(codigo_tipo_prod_leg, codigo_sub_tipo_prod_leg, productos);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
}
