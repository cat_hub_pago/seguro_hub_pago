package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.interfaces.MantenedorEventos;
import cl.cencosud.ventaseguros.interfaces.MantenedorEventosHome;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Clase relacionada con el delegate Mantenedor de evento.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Sep 30, 2010
 */
public class MantenedorEventosDelegate {

    /**
     * EJB.
     */
    private MantenedorEventos mantenedorEventos;
    /**
     * Interfaz.
     */
    private MantenedorEventosHome mantenedorEventosHome;

    /**
     * Constructor Delegate.
     */
    public MantenedorEventosDelegate() {
        try {
            this.mantenedorEventosHome =
                (MantenedorEventosHome) ServiceLocator.singleton()
                    .getRemoteHome(MantenedorEventosHome.JNDI_NAME,
                        MantenedorEventosHome.class);
            this.mantenedorEventos = this.mantenedorEventosHome.create();

        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Metodo relacionado con la busqueda de eventos.
     * @param filtros
     * @return
     */
    public List < Map < String, Object >> buscarEventos(Map filtros) {

        List < Map < String, Object > > res = null;
        try {
            res = mantenedorEventos.buscarEventos(filtros);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return res;
    }

    /**
     * Metodo relacionado con la subida de archivo.
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     */
    public final boolean subirArchivo(Long id_evento, byte[] bios, String tipo) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorEventos.subirArchivo(id_evento, bios, tipo);

        } catch (RemoteException e) {
            throw new SystemException(e);
        }

        return resultado;
    }

    /**
     * Metodo relacionado con un nuevo evento.
     * @param datos
     * @return
     */
    public Long nuevoEvento(Map datos) {
        try {
            return this.mantenedorEventos.nuevoEvento(datos);

        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Metodo relacionado con la actualizacion de eventos.
     * @param datos
     * @return
     */
    public boolean actualizarEvento(Map datos) {
        boolean resultado = false;
        try {
            resultado = this.mantenedorEventos.actualizarEvento(datos);

        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la insercion de rut del archivo.
     * @param datos
     * @return
     */
    public boolean insertarRutArchivos(Map < String, Object > datos) {
        boolean resultado = false;
        try {
            resultado = this.mantenedorEventos.insertarRutArchivos(datos);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de un archivo desde la bd.
     * @param id_evento
     * @param tipo
     * @return la
     */
    public Map < String, Object > obtenerArchivo(String id_evento, String tipo) {
        Map < String, Object > resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerArchivo(id_evento, tipo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con obtener el evento.
     * @param idEvento
     * @return map
     */
    public Map < String, Object > obtenerEvento(Long idEvento) {
        Map < String, Object > resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerEvento(idEvento);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }

        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de posiciones.
     * @return
     */
    public List < Map < String, ? > > obtenerPosiciones() {
        List < Map < String, ? > > resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerPosiciones();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion del tipo de pagina.
     * @return
     */
    public List < Map < String, ? > > obtenerTipoPagina() {
        List < Map < String, ? > > resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerTipoPagina();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de prioridad.
     * @return
     */
    public List < Map < String, ? > > obtenerPrioridad() {
        List < Map < String, ? > > resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerPrioridad();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;

    }

    /**
     * Metodo relacionado con la obtencion de los tipos de eventos.
     * @return
     */
    public List < Map < String, ? > > obtenerTipoEvento() {
        List < Map < String, ? > > resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerTipoEvento();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de la cantidad de evento
     * por codigo de evento.
     * @param codigo
     * @return
     */
    public Integer obtenerCantidadEvento(String codigo) {
        Integer resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerCantidadEvento(codigo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la modificacion de archivos en la BD.
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     */
    public boolean modificarArchivo(Long id_evento, byte[] bios, String tipo) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorEventos.modificarArchivo(id_evento, bios, tipo);

        } catch (RemoteException e) {
            throw new SystemException(e);

        }
        return resultado;
    }

    /**
     * Metodo relacionado con la modificacion de Archivo rut.
     * @param datos
     * @return
     */
    public Map < Object, String > modificarArchivoRut(
        Map < String, Object > datos) throws BusinessException {
        Map < Object, String > resultado = new HashMap < Object, String >();
        try {
            resultado = this.mantenedorEventos.modificarArchivoRut(datos);

        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con obtener si existe conflicto 
     * de prioridades entre eventos.
     * @param prioridad
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     */
    public boolean conflictoPrioridad(Integer prioridad, Date fechaInicioDate,
        Date fechaTerminoDate) {
        boolean resultado = false;
        try {
            resultado =
                this.mantenedorEventos.conflictoPrioridad(prioridad,
                    fechaInicioDate, fechaTerminoDate);

        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de la nueva sugerencia.
     * @param prioridad
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     */
    public Integer obtenerSugerenciaPrioridad(Integer prioridad,
        Date fechaInicioDate, Date fechaTerminoDate) {
        Integer resultado = 0;
        try {
            resultado =
                this.mantenedorEventos.obtenerSugerenciaPrioridad(prioridad,
                    fechaInicioDate, fechaTerminoDate);

        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con obtener la prioridad de un evento.
     * @param idEvento
     * @return
     */
    public Integer obtenerPrioridadEvento(Long idEvento) {
        Integer resultado = 0;
        try {
            resultado = this.mantenedorEventos.obtenerPrioridadEvento(idEvento);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion
     * de la fecha de inicio de un evento.
     * @param idEvento
     * @return
     */
    public Date obtenerFechaInicio(Long idEvento) {
        Date resultado;
        try {
            resultado = this.mantenedorEventos.obtenerFechaInicio(idEvento);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion
     * de la fecha termino de un evento.
     * @param idEvento
     * @return
     */
    public Date obtenerFechaTermino(Long idEvento) {
        Date resultado;
        try {
            resultado = this.mantenedorEventos.obtenerFechaTermino(idEvento);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo relacionado con borrar un evento.
     * @param datos
     * @return
     */
    public boolean borrarEvento(Long idEvento) throws BusinessException {
        boolean resultado = false;
        try {
            resultado = this.mantenedorEventos.borrarEvento(idEvento);

        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * TODO Describir método obtenerEventoMostrar.
     * @param filtros
     * @return
     */
    public Map < String, Object > obtenerEventoMostrar(Map filtros) {
        Map < String, Object > resultado = null;
        try {
            resultado = this.mantenedorEventos.obtenerEventoMostrar(filtros);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }

        return resultado;
    }

    /**
     * TODO Describir método verEvento.
     * @param id
     * @param rut
     */
    public void verEvento(Long id, Long rut) {
        try {
            this.mantenedorEventos.verEvento(id, rut);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
}
