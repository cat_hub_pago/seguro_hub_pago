<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<logic:empty name="subcategorias">
	<p>El Seguros seleccionado no tiene productos asociados</p>
</logic:empty>

<logic:notEmpty name="subcategorias">
	<logic:iterate id="subcategoria" name="subcategorias">
		<div class="c-articles__article-item">
          	<div class="row">
          		<!--<div class="col-lg-6 u-npresp-992">
             		<div class="c-articles__image">
                		 <h1><bean:write name="subcategoria" property="titulo_subcategoria"></bean:write></h1><img src="img/article01.jpg" alt="">
            		</div>
        		</div>-->
		<div class="col-lg-12 u-npresp-992">
        	<div class="c-articles__content">
            	<h1><bean:write name="subcategoria" property="titulo_subcategoria"></bean:write></h1>
                <p><bean:write name="subcategoria" property="descripcion_pagina"></bean:write></p>
	
            <div class="c-articles__buttons">
              <logic:notEqual name="tipoSeguro" value="7">
                <div class="row">
                <div class="col-lg-6">
                <button style="margin-bottom: 10px;" type="button" class="o-btn o-btn--outline o-btn--secundary" onclick="location.href='<%=request.getContextPath() %>/desplegar-ficha.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual><logic:equal name="subcategoria" property="id_subcategoria" value="165">&VP=1</logic:equal>		<logic:equal name="subcategoria" property="id_subcategoria" value="179">&VP=1</logic:equal>'"><i class="mx-Info-icon"></i>M&aacute;s informaci&oacute;n</button>
                </div>
                <div class="col-lg-5">
                <button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual>'">Cotizar Seguro</button>
                </div>
                </div>
                <div class="clearfix"></div>
			  </logic:notEqual>
			  <logic:equal name="tipoSeguro" value="7">
			    <logic:equal name="subcategoria" property="id_subcategoria" value="161">
			    <div class="row">
                <div class="col-lg-6">
                <button style="margin-bottom: 10px;" type="button" class="o-btn o-btn--outline o-btn--secundary" onclick="location.href='<%=request.getContextPath() %>/desplegar-ficha.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual><logic:equal name="subcategoria" property="id_subcategoria" value="165">&VP=1</logic:equal>		<logic:equal name="subcategoria" property="id_subcategoria" value="179">&VP=1</logic:equal>'"><i class="mx-Info-icon"></i>M&aacute;s Informaci&oacute;n</button>
                </div>
                <div class="col-lg-5">
                <button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual>'">Cotizar Seguro</button>
                </div>
                </div>  
                <div class="clearfix"></div> 
			    </logic:equal>
			    <logic:equal name="subcategoria" property="id_subcategoria" value="162">
			     <div class="row">
                <div class="col-lg-6">
                <button style="margin-bottom: 10px;" type="button" class="o-btn o-btn--outline o-btn--secundary" onclick="location.href='<%=request.getContextPath() %>/desplegar-ficha.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual><logic:equal name="subcategoria" property="id_subcategoria" value="165">&VP=1</logic:equal>		<logic:equal name="subcategoria" property="id_subcategoria" value="179">&VP=1</logic:equal>'"><i class="mx-Info-icon"></i>M&aacute;s Informaci&oacute;n</button>
                </div>
                <div class="col-lg-5">
                <button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual>'">Cotizar Seguro</button>
                </div>
                </div>
                <div class="clearfix"></div>
			    </logic:equal>
			    <logic:equal name="subcategoria" property="id_subcategoria" value="163">
			    <div class="row">
                <div class="col-lg-6">
                <button style="margin-bottom: 10px;" type="button" class="o-btn o-btn--outline o-btn--secundary" onclick="location.href='<%=request.getContextPath() %>/desplegar-ficha.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual><logic:equal name="subcategoria" property="id_subcategoria" value="165">&VP=1</logic:equal>		<logic:equal name="subcategoria" property="id_subcategoria" value="179">&VP=1</logic:equal>'"><i class="mx-Info-icon"></i>M&aacute;s Informaci&oacute;n</button>
                </div>
                <div class="col-lg-5">
                <button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual>'">Cotizar Seguro</button>
                </div>
                </div>
                <div class="clearfix"></div>
			    </logic:equal>
			    <logic:equal name="subcategoria" property="id_subcategoria" value="164">
			   	<div class="row">
                <div class="col-lg-6">
                <button style="margin-bottom: 10px;" type="button" class="o-btn o-btn--outline o-btn--secundary" onclick="location.href='<%=request.getContextPath() %>/desplegar-ficha.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual><logic:equal name="subcategoria" property="id_subcategoria" value="165">&VP=1</logic:equal>		<logic:equal name="subcategoria" property="id_subcategoria" value="179">&VP=1</logic:equal>'"><i class="mx-Info-icon"></i>M&aacute;s Informaci&oacute;n</button>
                </div>
                <div class="col-lg-5">
                <button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual>'">Cotizar Seguro</button>
                </div>
                </div>
                <div class="clearfix"></div>
			    </logic:equal>
				<logic:equal name="subcategoria" property="id_subcategoria" value="165">
			    <div class="row">
                <div class="col-lg-6">
                <button style="margin-bottom: 10px;" type="button" class="o-btn o-btn--outline o-btn--secundary" onclick="location.href='<%=request.getContextPath() %>/desplegar-ficha.do?idRama=<bean:write name="tipoSeguro"/>&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria"/><logic:notEqual name="subcategoria" property="id_tipo" value="0">&idTipo=<bean:write name="subcategoria" property="id_tipo"/></logic:notEqual><logic:equal name="subcategoria" property="id_subcategoria" value="165">&VP=1</logic:equal>		<logic:equal name="subcategoria" property="id_subcategoria" value="179">&VP=1</logic:equal>'"><i class="mx-Info-icon"></i>M&aacute;s Informaci&oacute;n</button>
                </div>
                <div class="col-lg-5">
                <button type="button" class="o-btn o-btn--primary" onclick="javascript:cargaCatastroficoGrupal();">Cotizar Seguro</button>
                </div>
                </div>
                <div class="clearfix"></div>
			    </logic:equal>
			  </logic:equal>
            </div>
          </div>
        </div>
       </div>
       </div>
	</logic:iterate>
</logic:notEmpty>
	