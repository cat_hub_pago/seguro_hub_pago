<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

		<logic:equal name="tipoSeguro" value="1">
			<meta name="description" lang="es" content=" Cotiza y Contrata Online seguros de Auto y Moto en Seguros Cencosud. Encuentra las mejores ofertas y asesoría para cobertura total, pérdida total, daños a terceros, argentina, motos, escolar." />
			<title>Seguros de Auto y Moto | Seguros Cencosud</title>
		</logic:equal>
		<logic:equal name="tipoSeguro" value="2">
			<meta name="description" lang="es" content=" Cotiza y Contrata Online seguros de Casa y Hogar en Seguros Cencosud. Encuentra las mejores ofertas y asesoría contra incendio, robo, sismo y devolución de dinero."/>
			<title>Seguros Casa y Hogar | Seguros Cencosud</title>
		</logic:equal>
		<logic:equal name="tipoSeguro" value="3">
			<meta name="description" lang="es" content=" Cotiza y Contrata Online seguros de Vida para ti y tu familia en Seguros Cencosud. Encuentra las mejores ofertas y asesoría."/>
			<title> Seguros de Vida para ti y tu familia | Seguros Cencosud </title>
		</logic:equal>
		<logic:equal name="tipoSeguro" value="4">
			<meta name="description" lang="es" content=" Cotiza y Contrata Online seguros de Salud y protégete en caso de accidente en Seguros Cencosud. Encuentra las mejores ofertas y asesoría."/>
			<title> Seguros de Salud para ti y tu familia | Seguros Cencosud </title>
		</logic:equal>
		<logic:equal name="tipoSeguro" value="5">
			<meta name="description" lang="es" content=" Cotiza y Contrata Online seguros de Fraude en Seguros Cencosud. Encuentra las mejores ofertas y asesoría." />
			<title> Seguro de Fraude | Seguros Cencosud </title>
		</logic:equal>
		<logic:equal name="tipoSeguro" value="6">
			<meta name="description" lang="es" content=" Cotiza y Contrata Online seguros para protegerte en caso de robo o fraude en Seguros Cencosud. Encuentra las mejores ofertas y asesoría." />
			<title> Seguros contra Robo | Seguros Cencosud </title>
		</logic:equal>
		
<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
<script type="text/javascript">
<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</script>
</logic:present>
<script type="text/javascript">
			function asesorVirtual(rut){
				$("#rutVsegParis").val(rut);
				$("#tipoSeguroAsesor").val("<%= request.getAttribute("tipoSeguro") %>");
				$("#asesorForm").submit();
			}
</script>