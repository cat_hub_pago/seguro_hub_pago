<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.cencosud.ventaseguros.common.model.Solicitud"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<logic:notEmpty name="<%= SeguridadUtil.USUARIO_CONECTADO_KEY %>">
	<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</logic:notEmpty>

<!-- BANNER -->
<div id="banner-cliente-vigente">
	<div id="benner-cliente-vigente-iz">
		<img
			src="<bean:write name="contextpath" />/images/banner/banner-cliente-vigente1.jpg"
			alt="" width="157" height="168" />
	</div>
	<div id="missegurosonline">
		<div class="text">
			<h1>
				<span class="miseguros" style="color: #2D8CC4;">Mis Seguros</span>
				<span class="textonline">Online</span>
			</h1>
		</div>

		<!-- bloque datos cliete -->
		<!-- bloque datos cliete -->
	</div>
	<div id="bloque-general-datos">
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<!--  datos clinte vigente -->
		<div class="bloquedatos">
			<div class="datos">
				Nombre:
			</div>
			<div class="compdatos">
				<bean:write name="usuario" property="nombre" />
				<bean:write name="usuario" property="apellido_paterno" />
				<bean:write name="usuario" property="apellido_materno" />
			</div>
			<div class="bloquedatos">
				<div class="datos">
					Rut:
				</div>
				<div class="compdatos" id="rut_cliente">
					<bean:write name="usuario" property="rut_cliente" />
					-
					<bean:write name="usuario" property="dv_cliente" />
				</div>
			</div>
			<div class="bloquedatos">
				<div class="datos">
					Direcci�n:
				</div>
				<div class="compdatos">
					<bean:write name="usuario" property="calle" />
					<bean:write name="usuario" property="numero" />
					<bean:write name="usuario" property="numero_departamento" />
				</div>
			</div>
			<div class="bloquedatos">
				<div class="datos">
					Tel�fono:
				</div>
				<div class="compdatos">
					<bean:write name="usuario" property="telefono_1" />
				</div>
			</div>
			<div class="bloquedatos">
				<div class="datos">
					Email:
				</div>
				<div class="compdatos">
					<bean:write name="usuario" property="email" />
				</div>
			</div>
		</div>
	</div>
	<!-- actualiza-tus-datos -->
	<div id="actualiza-tus-datos">
		<div id="tetxo-actualiza-tus-datos">
			<div class="tetxo-actualiza_hola-cliente">
				Actualiza
				<span id="actualizar"
					style="text-decoration: underline; cursor: pointer;">aqu�</span>
				tus Datos
			</div>
			<div class="cantado_clave">
				<div class="candado_actualiza_tus_dato">
					<img src="<bean:write name="contextpath" />/images/btn-img/candado.jpg"
						alt="" border="0" />
				</div>
				<div class="cambiar_clave_dato">
					<p>
						<a href="javascript:void(0);"><div id="cambiarClave" style="font-size: 11px;">Cambiar Clave</div></a>
					</p>
					<script type="text/javascript">
						/*if(navigator.appName == "Microsoft Internet Explorer") {
							$("#cambiarClave").colorbox({
							     iframe:true, 
							     width:"750", 
							     height:"530", 
							     href: "/vseg-paris/secure/cambio-clave/cambio-clave.do",
							     close: "" 
			    			});
						}
						else{
							$("#cambiarClave").colorbox({
							     iframe:true, 
							     width:"400", 
							     height:"300", 
							     href: "/vseg-paris/secure/cambio-clave/cambio-clave.do",
							     close: "" 
			    			});
						}*/
						$("#cambiarClave").fancybox({ 
					    'onStart'   : function() {
						var url = '/vseg-paris/secure/cambio-clave/cambio-clave.do';
								
					     $("#cambiarClave").attr("href",url);
					     
					    },
					    'width'    : 410,
					    'height'   : 250,
					    'autoScale'   : false,
					    'transitionIn'  : 'none',
					    'transitionOut'  : 'none',
					    'type'    : 'iframe',
		    			'scrolling'  : 'no'
					  });
						
					</script>
				</div>
			</div>
		</div>
	</div>
	<!-- actualiza-tus-datos -->
</div>
<!-- BANNER FIN-->
