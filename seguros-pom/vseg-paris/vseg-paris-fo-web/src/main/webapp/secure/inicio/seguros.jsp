<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<logic:notEmpty name="<%= SeguridadUtil.USUARIO_CONECTADO_KEY %>">
	<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</logic:notEmpty>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta name="generator" content="Cencosud" />
		<meta name="Keywords"
			content="Seguros,Cencosud,Hogar,Vida,Vehiculos,Salud" />
		<meta name="description" lang="es"
			content="Venta de Seguros Paris - cencosud" />
		<title>Venta de Seguros Paris</title>
<!--
		<link href="<bean:write name="contextpath" />/css/estilos-general.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<bean:write name="contextpath" />/css/nubes.css" rel="stylesheet" type="text/css" media="all" />-->
<!-- 		<link href="<bean:write name="contextpath" />/css/style.css"
			rel="stylesheet" type="text/css" /> -->
<!-- 		<link href="<bean:write name="contextpath" />/css/colorbox.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<bean:write name="contextpath" />/css/jquery/jqModal.css" type="text/css" />
		<link rel="stylesheet" href="<bean:write name="contextpath" />/css/estilos-light-box.css" type="text/css" media="all" /> -->

		<link rel="stylesheet" href="<bean:write name="contextpath" />/css/header_multisitio.css"  type="text/css" media="screen" />
		<script src="<bean:write name="contextpath" />/js/cotizacion.js"></script>
		<script src="<bean:write name="contextpath" />/js/jquery/jquery-1.4.2.js"></script>
		<script src="<bean:write name="contextpath" />/js/jquery/jquery.scrollTo.js"></script>
		<script src="<bean:write name="contextpath" />/js/jquery/jqModal.js" type="text/javascript" ></script>
		<script src="<bean:write name="contextpath" />/js/jquery/jquery.colorbox.js" type="text/javascript"></script>


		<script type="text/javascript">

		$(document).ready(function() {
			/*$("#actualizar").colorbox({iframe:true, width:"100%", height:"100%", title:"mis datos", href:"<html:rewrite action="/actualizar-datos-cliente" module="/secure/inicio"/>" });*/

			$("#actualizar").fancybox ({
							  'onStart'   : function() {
						    				  
							var url = '<html:rewrite action="/actualizar-datos-cliente" module="/secure/inicio"/>';
											
						   $("#actualizar").attr("href",url);
							   
						   },
						    'width'    : 670,
						    'height'   : '80%',
						    'autoScale'   : false,
						    'transitionIn'  : 'none',
						    'transitionOut'  : 'none',
						    'type'    : 'iframe'
						   });
			
			$('#content-formulario2').jqm({modal:'true',overlay:55, trigger: 'a#pruebaFancyBox'});
			
		});

		</script>

		<script type="text/javascript">
	    var clear="<bean:write name="contextpath" />/images/clear.gif"; //path to clear.gif
        </script>
		<!--[if lt IE 7]>
			<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
		<![endif]-->
		<script type="text/javascript">
		function verPoliza(idPoliza){
			var id = idPoliza.replace("poliza_", "");
			$('#poliza input[name="nro_solicitud"]').attr("value", id);
			$('#poliza').submit();
			return false;
		}
		</script>
	</head>
	<body>
		<%@ include file="../../google-analytics/google-analytics.jsp" %>
		<!-- INICIO CONTENIDO -->
		<div id="content">
			<!-- Incio HEADER -->
			<%@ include file="includes/mi-cabecera.jsp"%>
			<!-- FIN HEADER -->
			<!-- INCIO CONTENIDO CENTRAL -->
			<div id="cuerpo-central">
				<div id="curva-topcont-secciones">
					<img
						src="<bean:write name="contextpath" />/images/background-curva-contenido-secciones.jpg"
						alt="" width="978" height="12" border="0" />
				</div>
				<div id="contenido-empresas">

					<%@ include file="includes/mi-menu-izquierdo.jsp"%>

					<!-- contenido ficha vida -->
					<div id="contenido-chica-vida">
						<!-- BANNER -->
						<%@ include file="includes/mis-datos-head.jsp"%>
						<!-- BANNER FIN-->
						<!-- -->

						<!-- fichas vida -->
						<div id="fichas-vida">

							<%@ include file="includes/mi-seguros.jsp"%>

						</div>
						<!-- fichas vida -->
					</div>
					<!-- FIN contenido ficha vida -->
					<!-- contenido ficha VIDA-->
					<!-- FIN contenido ficha VIDA-->

				</div>
			</div>
			<!-- FIN CONTENIDO CENTRAL -->
			<!-- curva-booton -->
			<div id="curva-bottoncont-secciones"></div>
			<!-- FINcurva-booton -->
			<!-- footer -->
			<%@ include file="includes/mi-pie-de-pagina.jsp"%>
			<!-- footer FIN -->

		</div>
		<!-- FIN INICIO CONTENIDO -->
	</body>

</html>
