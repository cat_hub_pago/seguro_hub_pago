<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<div id="menu-iz-teayudamos">
	<!-- menu desplegable vida izquerdo -->
	<!-- -->
		<div class="estilo-titulo-menu">
				<html:link action="/inicio" module="/secure/inicio" style="color: #2D8CC4;">BIENVENIDO</html:link>
		</div>
	<div id="menu-seguro-vida-iz">

		<!-- -->

		<script type="text/javascript">
			$(document).ready(function(){
				// Marcar titulo menu.
				<logic:present name="solicitudes">
					$("#link_mis_cotizaciones").css("color", "#000");
				</logic:present>
				
				<logic:present name="solicitudesWS">
					$("#link_mis_seguros").css("color", "#000");
				</logic:present>
			});
		</script>

		<!-- -->
		<div id="btn-desplegado-vida-iz2">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img
						src="<bean:write name="contextpath" />/images/btn-img/icono_flecha_indica_2.jpg"
						alt="" width="11" height="23" border="0" />
				</div>
				<p>
					<html:link action="/inicio" module="/secure/inicio" styleId="link_mis_seguros">Mis Seguros</html:link>
				</p>
			</div>
		</div>
		<!-- -->

		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img
						src="<bean:write name="contextpath" />/images/btn-img/icono_flecha_indica_2.jpg"
						alt="" width="11" height="23" border="0" />
				</div>
				<p>
					<html:link action="/cotizaciones" module="/secure/inicio" styleId="link_mis_cotizaciones">Mis Cotizaciones</html:link>
				</p>
			</div>
		</div>
		<!-- -->

		<!-- -->
<!-- 
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img
						src="<bean:write name="contextpath" />/images/btn-img/icono_flecha_indica_gris.jpg"
						alt="" width="11" height="23" border="0" />
				</div>
				<p>
					<a href="<bean:write name="contextpath" />/general/proximamente.html">Mis Servicios</a>
				</p>
			</div>
		</div>
 -->

		<!-- 
		<div id="btn-desplegable-vida-iz_fichas-mas">
			<div class="btn-desplegado-vida-iz_icono">
				<img
					src="<bean:write name="contextpath" />/images/btn-img/icono_signo_mas_bla.jpg"
					alt="" width="11" height="23" border="0" />
			</div>
			<p>
				<a
					href="#">Contrata
					Online</a>
			</p>
		</div>
		 -->

	</div>
	<!-- -->
	<!-- FINmenu desplegable vida izquerdo -->

<!-- INICIO MENU IZQUIERDO -->
<div id="menu-iz-teayudamos">
    <div class="estilo-titulo-menu">Te Ayudamos</div>
    <div id="menu-seguro-vida-iz">
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro"> 
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/preguntas-frecuentes.html">Preguntas Frecuentes</a></p>	
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/caso-siniestro.html">En Caso de Siniestro</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/glosario.html">Glosario</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/polizas.html">P�lizas</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/Sucursales/">Nuestras Sucursales</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro2">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="http://www.svs.cl" target="_blank">Superintendencia de Valores y Seguros</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/empresa.html">Nuestra Empresa</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/formulario-contacto.do">Cont�ctanos</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/puntos.html">Acumula Puntos</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/terminos-condiciones.html">T�rminos y Condiciones</a></p>		
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/politicas.html">Pol�ticas de Seguridad</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro2">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/Circular_2131/Circular_Normativa_CAT_2131.pdf" target="_blank">Informaci�n sobre Atenci�n de Clientes</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro">
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/html/circular_2131.html">Seguro Desgravamen</a></p>
		</div>
		<div class="btn-desplegado-vida-iz-seguro">
			<div class="btn-desplegado-vida-iz_icono-seguro"> 
				<img src="/vseg-paris/images/ayudamos/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" />
			</div>
			<p><a href="/vseg-paris/">Inicio</a></p>
		</div>

		<div id="btn-desplegado-vida-iz3"></div>
		<div id="btn-desplegado-vida-iz6"></div>
		<div id="btn-desplegado-vida-iz6"></div>
    </div>
</div>
<!-- FIN MENU IZQUIERDO -->

</div>