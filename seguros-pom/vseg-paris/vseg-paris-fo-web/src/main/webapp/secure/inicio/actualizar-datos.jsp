<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
	<head>
		<html:base />
		<script src="/vseg-paris/js/jquery-1.11.3.min.js"></script>
		<%@ include file="../../home/includes/head.jsp"%>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="#">
		<script src="/cotizador/js/jquery-ui.js"></script>
		<link href="/cotizador/css/jquery-ui.css" rel="stylesheet">
		<script>
		function addDias(dias, select) {
			
			for(var i = 1; i <= dias ; i++){
				var optn = document.createElement("OPTION");
				optn.text = i;
				optn.value = i;
				document.getElementById(select).options.add(optn);
			}
		}
		
		//Generar objeto para celulares
		var codigo_celular = "<option value=''>Seleccione</option>";
			codigo_celular += "<option value=\"09\">09</option>";
			codigo_celular += "<option value=\"08\">08</option>";
			codigo_celular += "<option value=\"07\">07</option>";
			codigo_celular += "<option value=\"06\">06</option>";
		
		
		function validateInputRegistro(id) {
    			validacionInput = 0;
    			console.log('in validate input: ' + id);
    			$('#'+id+'').find(":input:not(:hidden,:button,:submit)").each(function(){
    				console.log('testing ' + this.id);
    				var optional = $(this).hasClass("optional");
    				if(optional==false) {
    					$(this).css("border-color","#cecece");
	    				if(!this.value) {
	    					$("#incomplete-text-"+id).html("Por favor complete el formulario correctamente");
	    					$("#incomplete-"+id).show("fast");
	    					$(this).css("border-color","red");
	    					$(this).focus();
	    					console.log(this.id + ' est� vac�o');
	    					validacionInput = 1;
		    				return false;
		    			} 
	    				if($(this).attr("mail")) {
	    					console.log("testing mail");
	    					var result = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value);
	    					if(result == false) {
	    						$("#incomplete-text-"+id).html("El correo tiene el formato incorrecto");
	    						$("#incomplete-"+id).show("fast");
	    						$(this).css("border-color","red");
	    						$(this).focus();
	    						console.log(this.id + ' formato incorrecto');
	    						validacionInput = 1;
	    						return false;
	    					} 
	    				}
	
					}
					
	    		});
				
    			if(validacionInput == 0) {
					$("#incomplete-text-"+id).html("");
					$("#incomplete-"+id).hide("fast");
					return true;
				}else{
					return false;
				}
    		}
    			    	
	    	function personalInfoRegistro() {
	    		if(document.getElementById('datos.rutcompleto')) {
	    	  		var rutCompleto = document.getElementById('datos.rutcompleto').value.split('-');
	    	  		if(rutCompleto.length > 0) {
		    			document.getElementById('datos.rut_cliente').value = rutCompleto[0];
		    			if(rutCompleto.length > 1) {
		    				document.getElementById('datos.dv_cliente').value = rutCompleto[1];
		    			}
	    			}
	    	  	}
	    		if(document.getElementById('fechaCompleta')) {
	    			var fechaCompleta = document.getElementById('fechaCompleta').value.split('-');
	    			if(fechaCompleta.length === 3) {
	    				document.getElementById('datos.diaFechaNacimiento').value = fechaCompleta[0];
	    				document.getElementById('datos.mesFechaNacimiento').value = fechaCompleta[1];
	    				document.getElementById('datos.anyoFechaNacimiento').value = fechaCompleta[2];
	    			}
	    		}
	    	}

		$(document).ready(function(){
			$("#fechaCompleta").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '1941:1998',
				dateFormat: 'dd-mm-yy',
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
				'Junio', 'Julio', 'Agosto', 'Septiembre',
				'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
				'May', 'Jun', 'Jul', 'Ago',
				'Sep', 'Oct', 'Nov', 'Dic']
			}); 
			// VALOR DEFAULT PARA DIRECCION
			$('.datos-rut-input').attr('placeholder','12345678-9');
			$('.datos-serie-input').attr('placeholder','N�mero De Serie');
			$('.datos-nombre-input').attr('placeholder','Nombre');
			$('.datos-paterno-input').attr('placeholder','Apellido Paterno');
			$('.datos-materno-input').attr('placeholder','Apellido Materno');
			$('.datos-email-input').attr('placeholder','ej: email@email.com');
			$('.datos-fecha-input').attr('placeholder','01-01-1980');
			$('.datos-telefono-input').attr('placeholder','Tel�fono');
			$('.datos-calle-input').attr('placeholder','Calle');
			$('.datos-direccion-input').attr('placeholder','N�');
			$('.datos-depto-input').attr('placeholder','N� Depto/Casa');
			
			$('#grupo_direccion :input').each(function(index) {
	    	 	
	    	 	if($(this).val() == "") {
	    	 		
	    	 		if(index == 0 || (index % 3 == 0)) {
	    	 			$(this).val("calle");
	    	 		}
	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
	    	 			$(this).val("n�");
	    	 		}
	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
	    	 			$(this).val("n� depto/casa");
	    	 		}
	    	 		
	    	 	}
		  	});
		  	
		  	$('#grupo_direccion :input').focus(function(){
		  		if($(this).val() == "calle" || $(this).val() == "n�" || $(this).val() == "n� depto/casa" ) {
		  			$(this).val("");
		  		}
		  	});
		  	
		  	$('#grupo_direccion :input').focusout(function(){
		  		$('#grupo_direccion :input').each(function(index) {
	    	 	if($(this).val() == "") {
	    	 		
	    	 		if(index == 0 || (index % 3 == 0)) {
	    	 			$(this).val("calle");
	    	 		}
	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
	    	 			$(this).val("n�");
	    	 		}
	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
	    	 			$(this).val("n� depto/casa");
	    	 		}
	    	 		
	    	 	}
		  		});
		  	});
			// FIN VALOR DEFAULT PARA DIRECCION
			
			// VALOR DEFAULT PARA NOMBRE
			$('#grupo_nombre :input').each(function(index) {
	    	 	
	    	 	if($(this).val() == "") {
	    	 		
	    	 		if(index == 0 || (index % 3 == 0)) {
	    	 			$(this).val("nombre");
	    	 		}
	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
	    	 			$(this).val("apellido paterno");
	    	 		}
	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
	    	 			$(this).val("apellido materno");
	    	 		}
	    	 		
	    	 	}
		  	});
		  	
		  	$('#grupo_nombre :input').focus(function(){
		  		if($(this).val() == "nombre" || $(this).val() == "apellido paterno" || $(this).val() == "apellido materno" ) {
		  			$(this).val("");
		  		}
		  	});
		  	
		  	$('#grupo_nombre :input').focusout(function(){
		  		$('#grupo_nombre :input').each(function(index) {
	    	 	if($(this).val() == "") {
	    	 		
	    	 		if(index == 0 || (index % 3 == 0)) {
	    	 			$(this).val("nombre");
	    	 		}
	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
	    	 			$(this).val("apellido paterno");
	    	 		}
	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
	    	 			$(this).val("apellido materno");
	    	 		}
	    	 		
	    	 	}
		  		});
		  	});
			// FIN VALOR DEFAULT PARA NOMBRE
			

			//parent.$.fn.colorbox.resize({width:"730px", height: $('#content-formulario').height() + 100 });

			var codigo_fijo = $("#formulario select[name='datos(codigo_area)']").html();
			
			try {
				if($("#formulario select[name='datos(tipo_telefono_1)'] :selected").text() == 'Celular') {
					$("#formulario select[name='datos(codigo_area)']").html(codigo_celular);
				}
			} catch(e) {
				alert("error1" + e.message);
			}
			
			$("#formulario select[name='datos(tipo_telefono_1)']").change(function(){
				try {
					var sel = $("#formulario select[name='datos(tipo_telefono_1)'] :selected").text();

					if(sel == 'Celular'){
						$("#formulario select[name='datos(codigo_area)']").html(codigo_celular);
					} else {
						$("#formulario select[name='datos(codigo_area)']").html(codigo_fijo);
					}
				} catch(e) {
					alert("error2" + e.message);
				}
			});
			
			var maxdias = '<bean:write name="datos-cliente-form" property="datos(maxDias)" />';
			var dia = '<bean:write name="datos-cliente-form" property="datos(diaFechaNacimiento)" />';

			try {
				$('select#diasFecha option').remove();
				$('select#diasFecha').append('<option value="">dia</option>');
				for(var i=1; i<=maxdias; i++) {
					if((dia!='') && ((dia*1) == i)) {
						$('select#diasFecha').append('<option value="' + i + '" selected>' + i + '</option>');
					} else {
						$('select#diasFecha').append('<option value="' + i + '">' + i + '</option>');
					}
				}
			} catch (e) {
				alert(e.message());
			}
			
			$("select#mesFecha").change(function(){
				var tdia = $('select#diasFecha').attr("value");
				
				if(this.value == 2) {
					$('select#diasFecha option').remove();
					$('select#diasFecha').html('<option value="">dia</option>');
					addDias(29, "diasFecha");
				}
				else if(this.value == 4 || this.value == 6 || this.value == 9 || this.value == 11) {
					$('select#diasFecha option').remove();
					$('select#diasFecha').html('<option value="">dia</option>');
					addDias(30, "diasFecha");
				}
				else{
					$('select#diasFecha option').remove();
					$('select#diasFecha').html('<option value="">dia</option>');
					addDias(31, "diasFecha");
				}
				
				$('select#diasFecha').attr("value", tdia);
			});

			$("select#id_region").change(function() {
				
				$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
					$('select#id_comuna option').remove();
					var options = '';
					options += '<option value="">Seleccione</option>';
					
					for(var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}

					$('select#id_comuna').html(options);
					$("select#id_ciudad").html('<option value="">Seleccione</option>');
				});
			});
			
			$("select#id_comuna").change(function() {
				
				$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
					$('select#id_ciudad option').remove();
					var options = '';
					options += '<option value="">Seleccione</option>';
					
					for(var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}

					$('select#id_ciudad').html(options);
				});
			});
			
			$("#enviar").click(function() {
				personalInfoRegistro();
				
				if (!validateInputRegistro("formularioRegistro")) {
					return false;
				}else{
					$("#formulario").attr("action", "<html:rewrite action="/actualizar-datos-cliente-grabar" module="/secure/inicio"/>");
				}
				
				
				
				$('#grupo_direccion :input').each(function(){
			  		if($(this).val() == "calle" || $(this).val() == "n�" || $(this).val() == "n� depto/casa" ) {
			  			$(this).val("");
			  		}
		  		});
		  		
		  		$('#grupo_nombre :input').each(function(){
			  		if($(this).val() == "nombre" || $(this).val() == "apellido paterno" || $(this).val() == "apellido materno" ) {
			  			$(this).val("");
			  		}
		  		});
				
				$("#formulario").submit();
				return false;
			});
			
			$("#cerrar").click(function() {
				parent.$.fn.colorbox.close();
				return false;
			});

			function desplegarErrores(){
				var campos = "";
				<logic:messagesPresent>
					<logic:iterate id="error" name="<%= ValidableActionForm.PROPERTY_NAMES %>">
						campos += "<bean:write name="error"/>" + ";";
					</logic:iterate>
				</logic:messagesPresent>
				
				
				if(campos != "" && campos.length > 0) {
					var errores = campos.split(";");
					for (var i = 0; i < errores.length; i++) {
						if(document.getElementById(errores[i]) != null) {
							document.getElementById(errores[i]).className='novalido';
						}
					}
				}
			}
			
			desplegarErrores();

		});
		</script>
		<style type="text/css">
			.ui-datepicker {
				color: #000000 width :       216px;
				height: auto;
				margin: 5px auto 0;
				font: 9pt Arial, sans-serif;
				-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
				-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
				box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
			}

			.ui-datepicker-month {
				color: #000000
			}

			.ui-datepicker-year {
				color: #000000
			}
		</style>
	</head>
	<body>
	<%@ include file="../../home/includes/header.jsp"%>
	<!-- INICIO  BREADCRUMB -->
<div class="container contenedor-sitio">
    <ol class="breadcrumb hidden-xs">
      <li><a href="/vseg-paris/index.html">Home</a></li>
      <li><a href="/vseg-paris/secure/inicio/cotizaciones.do">Mi perfil</a></li>
      <li class="active">Actualiza tus datos</li>
    </ol>
    <div class="row hidden-xs">
      <h1 class="titulo-cotizacion pull-left">ACTUALIZA TUS DATOS</h1>
    </div>
</div>
<!-- FIN  BREADCRUMB --> 
<!-- INICIO  CONTENIDOS -->
<div class="container" id="formularioRegistro">
	<html:form action="/actualizar-datos-cliente.do" styleId="formulario">
		<html:hidden property="datos(diaFechaNacimiento)" styleId="datos.diaFechaNacimiento" />
		<html:hidden property="datos(mesFechaNacimiento)" styleId="datos.mesFechaNacimiento" />
		<html:hidden property="datos(anyoFechaNacimiento)" styleId="datos.anyoFechaNacimiento" />
		<div class="row top15 registro">
			<div class="col-md-6 col-sm-6 col-xs-12 caja-form-fondo1">
			    <h2 class="titulo-cotizacion">Tus datos</h2>
		    <div class="row">
				<div class='col-md-12 col-sm-12 col-xs-12'>
				  <div class="form-horizontal">
					<div class="form-group">
					  <label for="inputRut" class="col-sm-4 control-label hidden-xs">Rut</label>
					  <div class="col-sm-6">
					  
						<input type="text" class="form-control rut-completo" placeholder="Rut" disabled="disabled" 
						value="<bean:write name='usuario' property='usuarioExterno.rut_cliente' />-<bean:write name='usuario' property='usuarioExterno.dv_cliente' />">
						<html:hidden property="datos(rut_cliente)" />
						<html:hidden property="datos(dv_cliente)" />
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputNombre" class="col-sm-4 control-label hidden-xs">Nombre</label>
					  <div class="col-sm-6">
						<html:text property="datos(nombre)" styleId="datos.nombre" styleClass="form-control" size="24" maxlength="30"/>
						<html:errors property="grupoNombre" />
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputApellidoPaterno" class="col-sm-4 control-label hidden-xs">Apellido Paterno</label>
					  <div class="col-sm-6">
						<html:text property="datos(apellido_paterno)" styleId="datos.apellido_paterno" styleClass="form-control" size="12" maxlength="30" onkeypress="return validar(event)"/>
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputApellidoMaterno" class="col-sm-4 control-label hidden-xs">Apellido Materno</label>
					  <div class="col-sm-6">
						<html:text property="datos(apellido_materno)" styleId="datos.apellido_materno" styleClass="form-control" size="12" maxlength="30" onkeypress="return validar(event)"/>
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputTelefono" class="col-sm-4 control-label hidden-xs">Tel�fono</label>
					  <div class="col-sm-8 telefono">
						<div class="col-md-3 col-xs-4 pleft">
						    <html:select property="datos(tipo_telefono_1)" styleClass="form-control" styleId="datos.tipo_telefono_1">
								<html:option value="">Seleccione</html:option>
								<html:optionsCollection name="tiposTelefono" label="descripcion" value="valor"/>
							</html:select>		
						</div>
						<div class="col-md-3 col-xs-4 pleft">
						    <html:select property="datos(codigo_area)" styleClass="form-control" styleId="datos.codigo_area_1">
								<option value="">Seleccione</option>
								<html:optionsCollection name="codigosArea" label="descripcion" value="valor"/>
							</html:select>
						</div>
						<div class="col-md-3 col-xs-4 p0">
						    <html:text property="datos(telefono_1)" styleClass="form-control" size="8" maxlength="8" styleId="datos.telefono_1" onkeypress="return isNumberKey(event)"/>
						</div>
						<script type="text/javascript">
							$(document).ready(function(){
								<logic:present property="datos(codigo_area)" name="datos-cliente-form">
									valor = "<bean:write name='datos-cliente-form' property='datos(codigo_area)'/>";
									$("select[name='datos(codigo_area)']").val(valor);
								</logic:present>
							});
						</script>
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputEmail" class="col-sm-4 control-label hidden-xs">Email</label>
					  <div class="col-sm-6">
						<html:text property="datos(email)" styleId="datos.email" styleClass="form-control" maxlength="60" onkeypress="return isCaracterEmail(event);"/>
						<small>&nbsp;ejemplo@ejemplo.com</small>
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputFechaNacimiento" class="col-sm-4 control-label hidden-xs">Fecha de Nacimiento</label>
					  <div class="col-sm-6">
						<input class="form-control input-min-width-95p" value="<bean:write name='usuario' property='usuarioExterno.fecha_nacimiento' format='dd-MM-yyyy' />" 
						placeholder="Fecha de Nacimiento" maxlength="10" id="fechaCompleta">
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputEstadoCivil" class="col-sm-4 control-label hidden-xs">Estado Civil</label>
					  <div class="col-sm-6"> 
						<html:select property="datos(estado_civil)" styleClass="form-control" styleId="datos.estado_civil">
							<html:option value="">Seleccione</html:option>
							<html:options collection="estadosCiviles" property="id" labelProperty="descripcion"/>
						</html:select>
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputSexo" class="col-sm-4 control-label hidden-xs">Sexo</label>
					  <div class="col-sm-6">
						<html:select property="datos(sexo)" styleClass="form-control" styleId="datos.sexo">
							<html:option value="">Seleccione Sexo</html:option>
							<html:option value="F">Femenino</html:option>
							<html:option value="M">Masculino</html:option>
						</html:select>
					  </div>
					</div>
					<p>&nbsp;</p>     
				  </div>
				</div>
			  </div>
			</div>
		<div class="col-md-6 col-sm-6 col-xs-12 caja-form-fondo2">
		  <h2 class="titulo-cotizacion">Tu direcci�n</h2>
		  <div class="row">
			<div class='col-md-12 col-sm-12 col-xs-12'>
			  <div class="form-horizontal" role="form">
				<div class="form-group">
				  <label for="region" class="col-sm-4 control-label hidden-xs">Regi�n</label>
				  <div class="col-sm-6">
					<html:select property="datos(id_region)" styleClass="form-control" styleId="id_region">
						<html:option value="">Seleccione</html:option>
						<html:options collection="regiones" property="id" labelProperty="descripcion"/>
					</html:select>
					<html:errors property="datos.id_region"/>
				  </div>
				</div>
				<div class="form-group">
				  <label for="comuna" class="col-sm-4 control-label hidden-xs">Comuna</label>
				  <div class="col-sm-6">
					<html:select property="datos(id_comuna)" styleClass="form-control" styleId="id_comuna">
						<html:option value="">Seleccione</html:option>
						<html:options collection="comunas" property="id" labelProperty="descripcion"/>
					</html:select>
					<html:errors property="datos.id_comuna"/>
				  </div>
				</div>
				<div class="form-group">
				  <label for="ciudad" class="col-sm-4 control-label hidden-xs">Ciudad</label>
				  <div class="col-sm-6">
					<html:select property="datos(id_ciudad)" styleClass="form-control" styleId="id_ciudad">
						<html:option value="">Seleccione</html:option>
						<html:options collection="ciudades" property="id" labelProperty="descripcion"/>
					</html:select>
					<html:errors property="datos.id_ciudad"/>
				  </div>
				</div>   
				<div class="form-group">
				  <label class="col-md-4 control-label hidden-xs">Direcci�n*</label>
				  <div class="col-md-8 telefono">
					  <div class="col-md-4 pleft">
						<div class='form-group m0direccion'>
							<html:text property="datos(calle)" styleId="datos.calle" styleClass="form-control" size="20" maxlength="40"/>
						</div>
					  </div>
					  <div class="col-md-3 col-xs-6 pleft">
						<div class='form-group m0'>
							<html:text property="datos(numero)" styleId="datos.numero" styleClass="form-control" size="5" maxlength="5" onkeypress="return isNumberKey(event)"/>
						</div>
					  </div>
					  <div class="col-md-2 col-xs-6 p0">
						<div class='form-group m0'>
							<html:text property="datos(numero_departamento)" styleId="datos.numero_departamento" styleClass="form-control optional" size="10" maxlength="5" onkeypress="return isNumberKey(event)"/>
						</div>
					  </div>
					  <html:errors property="grupoDireccion"/>
				  </div>
				  
				  </div>
				 <div class="form-group datosform top30">
				  <div class='col-md-5 col-md-offset-4'>
					<button type="button" class="btn btn-primary btn-block center-block" id="enviar" name="enviar">Guardar</button>
				  </div>
				 </div>
				 <div class="form-group">
				  <div class='col-md-5 col-md-offset-4'>
					<div class="alert alert-warning top20" id="incomplete-formularioRegistro" style="display:none">
						<p align="center" id="incomplete-text-formularioRegistro"></p>
					</div>
				  </div>
				 </div>
			  </div>
			</div>
		  </div>
		</div>
		</div>
  </html:form>
</div>
<!-- FIN CONTENIDOS --> 
	<%@ include file="../../home/includes/footer.jsp"%>
	</body>
</html:html>