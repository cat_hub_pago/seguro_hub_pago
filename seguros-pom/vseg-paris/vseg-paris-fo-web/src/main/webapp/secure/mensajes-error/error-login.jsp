<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
    <html:base />
    <title>Error Login</title>
	<link href="<%=request.getContextPath()%>/css/formulario-r.css" rel="stylesheet" type="text/css" />
	<!--[if lt IE 7]>
		<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
	<![endif]-->

<body>
	<!--BOX PROCESO -->
	<div id="estamos_procesando">
		<div id="curba_top_proceso">
			<a href="parent.$.fn.colorbox.close();"><img
					src="<%=request.getContextPath()%>/images/img_light_box/btn-cerrar.gif"
					onmouseover="this.src = '<%=request.getContextPath()%>/images/img_light_box/btn-cerrar-hover.gif'"
					border="0"
					onmouseout="this.src = '<%=request.getContextPath()%>/images/img_light_box/btn-cerrar.gif'"
					border="0" alt="" width="39" height="39" border="0" />
			</a>
		</div>
		<div class="pantalla-error_cuerpo">
			<div id="conte_estamos_procesando">
				<div class="icono_geeral">
					<img
						src="<%=request.getContextPath()%>/images/img/icono_error.png"
						alt="" width="46" height="46" border="0" />
				</div>

				<div id="texto_procesando">
					<h3>
						<span> <bean:write
								name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>"
								ignore="true" /> </span>
					</h3>
				</div>
			</div>
		</div>
	</div>
	<!--BOX PROCESO FIN-->

</body>
</html:html>

