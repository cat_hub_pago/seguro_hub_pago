<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
    <html:base />
    
    <%
    String path = request.getContextPath();
    String basePath =
        request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + path + "/";
  %>
    
    <title>Recuperar Clave</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

	<link href="/cotizador/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/estilos.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/base.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
	
	<!--[if lt IE 7]>
		<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
	<![endif]-->

  </head>
  
  <body>
	<div align="center">
	<!--BOX PROCESO -->
	<!--<div id="curba_top_proceso"></div>-->
	<div class="pantalla-exito_cuerpo col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<!--style="margin-left:-50px;margin-top:-32px;*margin-top:-25px;"-->
		<div class="row top20">

			<!--div class="icono_geeral">
				<img src="<%=request.getContextPath()%>/images/img_light_box/icono_ok.gif" alt="" width="46" height="46" border="0" />
			</div-->

			<div>
			   <h4><span><bean:message key="labels.correo.exito" bundle="labels-seguridad"/> </span> </h4> <strong><p> <bean:write  name="correo" /> </p></strong> 
			</div>					
		</div>
	</div>
	<!--BOX PROCESO FIN-->
	</div>	
	

  
 
    
  </body>
</html:html>
