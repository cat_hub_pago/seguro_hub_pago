<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
<script type="text/javascript">
<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</script>
</logic:present>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" lang="es"> 
 
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    	<meta name="language" content="es" /> 
    	<meta name="description" content="�Que te preocupa hoy?, cotiza hoy seguros para tus distintas necesidades." /> 
    
		<title>Asesor Virtual | Seguros &amp; Servicios te cuida m�s!</title> 
    
    	<link rel="icon" type="image/x-icon" href="/asesor/favicon.ico" />
    	<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
    	
    	<style>
			*{margin:0;padding:0}
			html, body {height:100%;width:100%;overflow:hidden}
			table {height:100%;width:100%;table-layout:static;border-collapse:collapse}
			iframe {height:100%;width:100%; overflow: hidden;}
			.header {border-bottom:1px solid #000}
			.content {height:100%}
		</style>
    	
        <script type="text/javascript" src="../js/jquery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="../js/jquery/jquery.colorbox.js"></script>
        
	<bean:define id="protocol" value="<%=request.getScheme()%>" />

        <script type="text/javascript">
        
        	$(document).ready(function(){

        		var url = "<bean:write name="protocol"/>://www.seguroscencosud.cl/asesor/index.html";
        		
        		var rut_cliente = $("#rut_cliente").val();
        	
        		var tipo_seguro_asesor = "<%= request.getParameter("tipoSeguroAsesor")%>";

        		var url_tipo_seguro = "";
        		
        		if(tipo_seguro_asesor == 1) {
        			url_tipo_seguro += "#/auto";
        		}
        		else if(tipo_seguro_asesor == 2) {
        			url_tipo_seguro += "#/casa";
        		}
        		else if(tipo_seguro_asesor == 3) {
        			url_tipo_seguro += "#/vida";
        		}
        		else if(tipo_seguro_asesor == 4) {
        			url_tipo_seguro += "#/salud";
        		}
        		else if(tipo_seguro_asesor == 5) {
        			url_tipo_seguro += "#/cesantia";
        		}
        		else if(tipo_seguro_asesor == 6) {
        			url_tipo_seguro += "#/robo-documentos";
        		}
        		
        		if(rut_cliente != null && rut_cliente != undefined && rut_cliente != "") {
        			
        			var params = "?rut=" + rut_cliente;
        			window.location.replace(url + params + url_tipo_seguro);
        			
        		}
        		else {
        		
        			window.location.replace(url + url_tipo_seguro);
        		}
        		
        	});
        	
        </script>
    </head> 
    
    <body>
    
    <logic:present name='usuario'>
    <input type="hidden" id="rut_cliente" value="<bean:write name='usuario' property='rut_cliente'/>-<bean:write name='usuario' property='dv_cliente'/>"/>
    </logic:present>
    
    <div id="asesor"></div>
    <iframe id="iframeasesor" src="<bean:write name="protocol"/>://www.seguroscencosud.cl/asesor/index.html" frameborder="0" >
    </iframe>
    	
    <%@ include file="../google-analytics/google-analytics.jsp" %>
    </body> 
</html>