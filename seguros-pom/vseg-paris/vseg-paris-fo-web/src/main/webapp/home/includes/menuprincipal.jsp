<div id="menu-principal2">
<div id="p7PMMcontenedorMenu">
	    <div id="p7PMM_1" class="p7PMMh02 p7PMMnoscript">
	      <ul class="p7PMM">
			<!-- Vehiculo -->
	         <li><a href="/vseg-paris/desplegar-pagina-intermedia-vehiculos.do?idRama=1">Veh�culo</a>
	          <div>
	            <ul>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1">Cobertura Total</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=21&idTipo=1">P�rdida Total</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=23">Da�os a Terceros</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=36">Seguro Obligatorio Mercosur</a></li>
				  <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=79">Robo Contenido</a></li>
				  <li><a target="_blank" href="/soap">SOAP</a></li>
				  <!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=1">Promociones</a></li-->
	            </ul>
	          </div>
	        </li>
			
	        <!-- Hogar -->
	        <li><a href="/vseg-paris/desplegar-pagina-intermedia-hogar.do?idRama=2">Hogar</a>
	        	<div>
	            <ul>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=42">Incendio</a></li-->
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=24">Incendio + Sismo / Estructura</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=43">Incendio + Sismo / Contenido</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=44">Incendio + Robo</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=25">Incendio + Robo + Sismo</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=26">Hogar con Devoluci�n Dinero</a></li>
	              <!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=2">Promociones</a></li-->
	            </ul>
	          </div>
	        </li>       
	        
	        <!-- Vida -->
	        <li><a href="/vseg-paris/desplegar-pagina-intermedia-vida.do?idRama=3">Vida</a>
	        	<div>
	            <ul>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=28">Seguro de Vida sin Tr�mites</a></li-->
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=29">Seguro de Accidentes</a></li>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=45">Seguro con Devoluci�n Dinero</a></li-->
	              <!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=3">Promociones</a></li-->
	            </ul>
	          </div>
	        </li>
	        		        	
	        <!-- Salud -->
	        <!--li><a href="/vseg-paris/desplegar-pagina-intermedia-salud.do?idRama=4">Salud</a>
	        	<div>
	            <ul>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=47">Seguro de Hospitalizaci�n</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=30">Seg. Reembolso M�dico</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=31">Atenci�n Oncol�gica IRAM</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=48">Seg. Fractura Accidental</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=57">Protecci�n Escolar</a></li>
                  <li><a href="/vseg-paris/desplegar-promociones.do?idRama=4">Promociones</a></li>
	            </ul>
	          </div>
	        </li-->
	        
	        <!-- Mas seguros -->
	        <li><a href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6">M�s Seguros</a>
	        	<div>
	            <ul>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=5&idSubcategoria=32">Cesant�a Cuentas B�sicas</a></li-->
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=46">Bolso Protegido</a></li-->
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=34">Fraude con Devoluci�n</a></li-->
				  <li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=119">Asistencia en Viaje</a></li>
	            </ul>
	           </div>
	        </li>
	        	  
			<!-- Promociones -->	
			<!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=50">Promociones</a></li-->
			
		</ul>
	      <div class="p7pmmclearfloat">&nbsp;</div>


	<style>.p7PMMh02 ul ul li {float:left; clear: both; width: 100%;}.p7PMMh02 {text-align: left;}.p7PMMh02, .p7PMMh02 ul ul a {zoom: 1;}</style>
	
	      
	<style>.p7PMMh02, .p7PMMh02 ul ul a {height: 1%; overflow: visible !important;} .p7PMMh02 {width: 100%;}</style>
	
	
	      
	<style>.p7PMMh02, .p7PMMh02 a{zoom:1;}.p7PMMh02 ul ul li{float:left;clear:both;width:100%;}</style>
	
	      <script type="text/javascript">
	<!--
	P7_PMMop('p7PMM_1',1,2,-5,-5,0,0,0,0,0,3,1,1,0,0,0);
	//-->
	      </script>
	    </div>
	</div>
  

</div>