<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
  <head>
    <html:base />
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">


<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
			$("select#rama").change(function() {
				$('#listProductos').hide();
				$('#table_btn').hide();
				$.getJSON("/vseg-paris/obtener-subcategorias.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
					$('select#subcategoria option').remove();
					$('select#producto option').remove();
					var options = '';
					options += '<option value="-1">S</option>';
					
					$('select#producto').html(options);
					
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
                    }
	      			$('select#subcategoria').html(options);
	    		})
  			})
  			
  			
	});
	</script>

  </head>
  
  <body>
    <form action="/vseg-paris" name="pruebaForm" id="pruebaForm">
        <!-- -->
		 <div id="cotizar">		
			<div id="cotizaTu"> <a href="#" > Cotiza </a> <a href="#" style="color:#42ABE1;" > Tu Seguro </a> 
				<div style="margin-top:8px;  ">
					<div class="my-skinnable-select">
					  <html:select property="rama" styleId="rama">
									<option value="-1">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="listRamas" property="id_rama" labelProperty="titulo_rama" />
					  </html:select>					  
					</div>					
				</div>				
				<div style="margin-top:4px;  ">				
					<div class="my-skinnable-select">
					  <html:select property="subcategoria" styleId="subcategoria">
									<option value="-1">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="listSubcategorias" property="id_subcategoria" labelProperty="titulo_subcategoria"/>
					  </html:select>
					</div>				
				</div>				
			</div>
					
			<div id="cotizarColumna">
				<div id="botonCotizar">
					<input type="submit" value="Cotizar" id="posicionCotizar" />
				</div>
			</div>			
		</div>
		</form>
  </body>
</html:html>
