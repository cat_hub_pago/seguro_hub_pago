<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>



<logic:present name="frameClaveTemporal">
<!--BOX PROCESO -->
<script type="text/javascript">

		$(document).ready(function(){
			var url = "/vseg-paris/secure/cambio-clave/cambio-clave-temporal.do";
			var params = "?desplegarClaveTemporal='true'";
			
			$("#cargaClaveTemporal").attr('src',url+params);
			$("#modalClaveTemporal").modal({
			backdrop: 'static',
			show: true
			});
		});		
		
</script>
<!-- INICIO ModalClaveTemporal -->
	  <div class="modal fade" id="modalClaveTemporal" role="dialog">
		<div class="modal-dialog modal-sm">

		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" onclick="javascript:parent.window.location.replace('<%=request.getContextPath()%>/autenticacion/cerrarSession');">&times;</button>
			  <h4>Cambio Clave</h4>
			</div>
			<div class="modal-body">
				<iframe class="" frameborder="0" id="cargaClaveTemporal" width="100%" height="320px">
				</iframe>	
			</div>
		  </div>
		</div>
	  </div> 
<!-- FIN ModalClaveTemporal -->
</logic:present>
