<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-54BFFF');</script>
<!-- End Google Tag Manager -->
<!--[if lte IE 9]>
<script>window.location.href='/vseg-paris/html/alerta-ie9.html'</script>
<![endif]-->
<!--[if gt IE 8]><!-->

<meta http-equiv="Content-Language" content="es" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="author" content="Cencosud">

<link type="image/x-icon" rel="shortcut icon"
	href="images/btn-img/favicon.ico">
<meta name="generator" content="Cencosud" />
<!-- Bootstrap core CSS -->

<!-- x<link href="/vseg-paris/css/bootstrap-theme.min.css" rel="stylesheet"> -->
<!-- x<link href="/vseg-paris/css/carousel.css" rel="stylesheet"> -->


<script src="/vseg-paris/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="/vseg-paris/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- link nueva vista -->


    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,700|Varela+Round" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js"></script>

<!-- <script src="/vseg-paris/js/jquery-1.11.3.min.js"></script> -->

<!-- Important Owl stylesheet -->
<!--x <link rel="stylesheet" href="/vseg-paris/css/owl.carousel.css"> -->

<!-- Default Theme -->
<!--x <link rel="stylesheet" href="/vseg-paris/css/owl.theme.css"> -->

<!-- Include js plugin -->
<!-- x <script src="/vseg-paris/js/owl.carousel.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		var owl = $("#owl-demo");

		owl.owlCarousel({

			items : 6, //10 items above 1000px browser width
			itemsDesktop : [ 1200, 8 ], //5 items between 1000px and 901px
			itemsDesktopSmall : [ 992, 3 ], // 3 items betweem 900px and 601px
			itemsTablet : [ 768, 2 ], //2 items between 600 and 0;
			itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option

		});
	});
</script> -->

<!-- <script type="text/javascript">
	//<![CDATA[
	$(document).ready(function() {
	   $('.toggle-menu').jPushMenu({
	 		closeOnClickLink : false
	 	});
		$('.dropdown-toggle').dropdown();
		$('html, body').animate({
			scrollTop : 0
		}, 0);
		$('[data-toggle="tooltip"]').tooltip();
	});
	//]]>
</script> -->
<script src="/vseg-paris/js/libs/jquery-3.2.1.min.js"></script>
<script src="/vseg-paris/js/vendor/bootstrap.min.js"></script>
<!-- <script src="/vseg-paris/js/jPushMenu.js"></script> -->
<!-- <script src="/vseg-paris/js/v2p.js"></script> -->


<script src="js/libs/slick.min.js"></script>
<script src="js/libs/jquery.responsiveTabs.js"></script>
<script src="js/libs/remodal.js"></script> 
<script src="js/main.js"></script> 
<script src="js/layout.js"></script>
<!-- Jerarquia de CSS  -->
<link href="/vseg-paris/css/font-awesome.min.css" rel="stylesheet">
<link href="/vseg-paris/css/bootstrap.min.css" rel="stylesheet">
<link href="/vseg-paris/css/main.css" rel="stylesheet">
<link href="/vseg-paris/css/layout.css" rel="stylesheet">


