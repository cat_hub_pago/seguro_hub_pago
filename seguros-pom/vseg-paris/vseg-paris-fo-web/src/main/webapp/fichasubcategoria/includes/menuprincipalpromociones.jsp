<!--INICIO DANOS TU OPINION-->
<!-- style type="text/css">
	.flotante {
		position: fixed;
		top: 100px;
		width: 60px;
		left: 0px;
		height: 180px;
	}
</style -->

<!-- FANCYBOX -->
<!-- link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

<script type="text/javascript" src="fancybox/jquery.js"></script>

<script type="text/javascript" src="css/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>

<script type="text/javascript" src="css/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script>
$(document).ready(function() {
	$("a#btn_encuesta").fancybox({
		maxWidth	: '100%',
		maxHeight	: '100%',
		fitToView	: false,
		width		: 750,
		height		: 630,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
</script -->
<!-- FIN FANCYBOX -->
	
<!-- BOT�N FLOTANE -->
<!-- div class="flotante">
<a class="iframe" id="btn_encuesta" href="DanosTuOpinion/ext/formulario.html"><img src="DanosTuOpinion/images/btn_formulario.png" width="60" height="180" border="0" /></a>
</div -->
<!-- FIN BOT�N FLOTANTE -->
<!--FIN DANOS TU OPINION-->

<div id="menu-principal">
<div id="p7PMMcontenedorMenu">
	    <div id="p7PMM_1" class="p7PMMh02 p7PMMnoscript">
	      <ul class="p7PMM">
			<!-- Vehiculo -->
	         <li><a href="/vseg-paris/desplegar-pagina-intermedia-vehiculos.do?idRama=1">Veh�culo</a>
	          <div>
	            <ul>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1">Cobertura Total</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=21&idTipo=1">P�rdida Total</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=23">Da�os a Terceros</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=36">Seguro Obligatorio Mercosur</a></li>
				  <li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=79">Robo Contenido</a></li>
				  <li><a target="_blank" href="/soap">SOAP</a></li>
				  <!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=1">Promociones</a></li-->
	            </ul>
	          </div>
	        </li>
			
	        <!-- Hogar -->
	        <li><a href="/vseg-paris/desplegar-pagina-intermedia-hogar.do?idRama=2">Hogar</a>
	        	<div>
	            <ul>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=42">Incendio</a></li-->
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=24">Incendio + Sismo / Estructura</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=43">Incendio + Sismo / Contenido</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=44">Incendio + Robo</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=25">Incendio + Robo + Sismo</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=26">Hogar con Devoluci�n Dinero</a></li>
	              <!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=2">Promociones</a></li-->
	            </ul>
	          </div>
	        </li>       
	        
	        <!-- Vida -->
	        <li><a href="/vseg-paris/desplegar-pagina-intermedia-vida.do?idRama=3">Vida</a>
	        	<div>
	            <ul>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=28">Seguro de Vida sin Tr�mites</a></li-->
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=29">Seguro de Accidentes</a></li>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=45">Seguro con Devoluci�n Dinero</a></li-->
	              <!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=3">Promociones</a></li-->
	            </ul>
	          </div>
	        </li>
	        		        	
	        <!-- Salud -->
	        <!--li><a href="/vseg-paris/desplegar-pagina-intermedia-salud.do?idRama=4">Salud</a>
	        	<div>
	            <ul>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=47">Seguro de Hospitalizaci�n</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=30">Seg. Reembolso M�dico</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=31">Atenci�n Oncol�gica IRAM</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=48">Seg. Fractura Accidental</a></li>
	              <li><a href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=57">Protecci�n Escolar</a></li>
                  <li><a href="/vseg-paris/desplegar-promociones.do?idRama=4">Promociones</a></li>
	            </ul>
	          </div>
	        </li-->
	        
	        <!-- Mas seguros -->
	        <li><a href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6">M�s Seguros</a>
	        	<div>
	            <ul>
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=5&idSubcategoria=32">Cesant�a Cuentas B�sicas</a></li-->
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=46">Bolso Protegido</a></li-->
	              <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=34">Fraude con Devoluci�n</a></li-->
				  <li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=119">Asistencia en Viaje</a></li>
	            </ul>
	           </div>
	        </li>
	        	  
			<!-- Promociones -->	
			<!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=50">Promociones</a></li-->
			
		</ul>
	      <div class="p7pmmclearfloat">&nbsp;</div>


	<style>.p7PMMh02 ul ul li {float:left; clear: both; width: 100%;}.p7PMMh02 {text-align: left;}.p7PMMh02, .p7PMMh02 ul ul a {zoom: 1;}</style>
	
	      
	<style>.p7PMMh02, .p7PMMh02 ul ul a {height: 1%; overflow: visible !important;} .p7PMMh02 {width: 100%;}</style>
	
	
	      
	<style>.p7PMMh02, .p7PMMh02 a{zoom:1;}.p7PMMh02 ul ul li{float:left;clear:both;width:100%;}</style>
	
	      <script type="text/javascript">
	<!--
	P7_PMMop('p7PMM_1',1,2,-5,-5,0,0,0,0,0,3,1,1,0,0,0);
	//-->
	      </script>
	    </div>
	</div>   
</div>



<!--<div id="btn-numero-top">

						<img src="images/btn-img/btn-600-500-500.png" alt="" width="287" height="40" border="0" onclick="javascript:top.location.href='/vseg-paris/html/contactanos.html'" style="cursor: pointer;"/></div>

-->