<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html>
<html lang="es">
  <head>
    <%@ include file="../home/includes/head_ficha.jsp" %>
  	<script type="text/javascript"> 
 
	function cargarFichas() {
	    $("#tab1").load('/vseg-paris-adm/obtener-archivo.do?tipo=beneficio&id_ficha=<bean:write name="idFicha" />');
	    $("#tab2").load('/vseg-paris-adm/obtener-archivo.do?tipo=cobertura&id_ficha=<bean:write name="idFicha" />');
	    $("#tab3").load('/vseg-paris-adm/obtener-archivo.do?tipo=exclusion&id_ficha=<bean:write name="idFicha" />');
	    $("#tab4").load('/vseg-paris-adm/obtener-archivo.do?tipo=aspecto_legal&id_ficha=<bean:write name="idFicha" />');
		
	}
	
	function openLegal(){
		
		if( ($('#legalTxt').css('display') == 'none') ){
		    $('.legalTxt').show();
		}else{
		    $('.legalTxt').hide();
		}
	}

	</script>
	
	
  </head>
<body onload="cargarFichas();">
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="/home/includes/header.jsp"%> 
<!-- INICIO  BREADCRUMB -->


<!-- FIN  BREADCRUMB --> 

<!-- INICIO  CONTENIDOS -->

<main role="main">
      <div class="remodal-bg">
        <div class="c-heronarrow">
          <div class="c-quotient u-mobile_second">
            <div class="c-quotient__inner">
              <h3 class="c-quotient__pretitle">COTIZA AHORA</h3>
              
              <logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="c-quotient__title">Seguro Auto Full Servicio</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="c-quotient__title">Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="c-quotient__title">Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="c-quotient__title">Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="c-quotient__title">Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="c-quotient__title">Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="c-quotient__title">Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="c-quotient__title">Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="c-quotient__title">Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="c-quotient__title">Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="c-quotient__title">Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="c-quotient__title">Seguro Viaje Grupal</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="300"><h1 class="c-quotient__title">Seguro Vida con Ahorro</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="c-quotient__title">Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="c-quotient__title">Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="c-quotient__title">Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="c-quotient__title">Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="c-quotient__title">Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="c-quotient__title">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="c-quotient__title">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="c-quotient__title">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="c-quotient__title">Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			      	
              
              <hr class="u-line">
              <div class="c-quotient__textbutton">
                <div class="c-quotient__tbtext">
                  <p class="c-quotient__subtitle">
                  	<logic:equal name="idSubcategoria" value="22">
						<logic:notEmpty name="descripcionFicha"><bean:write name="descripcionFicha"/></logic:notEmpty>
					</logic:equal>
					<logic:notEqual name="idSubcategoria" value="22">
						<logic:equal name="idSubcategoria" value="119">
							<logic:notEmpty name="descripcionFicha"><bean:write name="descripcionFicha"/></logic:notEmpty>
						</logic:equal>
						<logic:notEqual name="idSubcategoria" value="119">	
							<logic:notEmpty name="descripcionFicha"><bean:write name="descripcionFicha"/></logic:notEmpty>
						</logic:notEqual>
					</logic:notEqual>
                  </p>
                </div>
                <div class="c-quotient__tbbutton">
                
                	<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=22'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=21'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=36'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=79'">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=24'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=43'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=44'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=25'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=26'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=201'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=220'">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=3&idSubcategoria=29'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="300"><button type="button" class="o-btn o-btn--primary">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=5&idSubcategoria=139'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=5&idSubcategoria=159'">Cotizar</button></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=119'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=140'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=221'">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=7&idSubcategoria=161'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=7&idSubcategoria=162'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=7&idSubcategoria=164'">Cotizar</button></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=8&idSubcategoria=239'">Cotizar</button></logic:equal>
			      	</logic:equal>
					
                </div>
              </div>
            </div>
          </div>
          <div class="banner_inter">
				<img src="/vseg-paris-adm/obtener-archivo.do?tipo=imagen&id_ficha=<bean:write name="idFicha" />" width="820" height="280" alt=""> 
			</div>
        </div>        
      </div>
      <section class="tabsmix">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <div class="o-box o-box--tabs o-box--layer u-no-margin-bottom u-bdr_n u-r-pad20 u-no-margin-992">
                  <div class="undiv">
                    <div class="wrapper">
                      <select class="selectpicker" data-tabgroup="first-tab-group" id="tabselect">
                        <option value="#tab1">BENEFICIOS</option>
                        <option value="#tab2">COBERTURA</option>
                        <option value="#tab3">EXCLUSIONES</option>
                        <option value="#tab4">CONDICIONES</option>
                     <!-- <option value="#tab5">GIFT CARD</option> -->
                      </select>
                      <ul class="tabs clearfix" data-tabgroup="first-tab-group">
                        <li><a class="active" href="#tab1">BENEFICIOS</a></li>
                        <li><a href="#tab2">COBERTURA</a></li>
                        <li><a href="#tab3">EXCLUSIONES</a></li>
                        <li><a href="#tab4">CONDICIONES</a></li>
                     <!--   <li><a class="nb" href="#tab5">GIFT CARD</a></li>  -->
                      </ul>
                      <!-- CONTENIDO TAB --> 
                      <div class="tabgroup o-features" id="first-tab-group">
                        <div class="content-tabs" id="tab1">
                        </div>
                        <div class="content-tabs" id="tab2">
                        </div>
                        <div class="content-tabs" id="tab3">
                        </div>
                        <div class="content-tabs" id="tab4">
                        </div>
                     <!--   <div class="content-tabs" id="tab5">
                          <h2>Tab 5</h2>
                          <p>Adipisci autem obcaecati velit natus quos beatae explicabo at tempora minima</p>
                        </div> -->
                      </div>
                      <!-- FIN CONTENIDO TAB --> 
                    </div>
                  </div>
                  <!-- BTN COTIZAR --> 
				  <div class="row">
				  <div class="col-sm-12 u-bdr_tb">
				  <div class="o-features__quotebox"> 
				  <logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Auto Full Servicio</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro P�rdida Total</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Obligatorio Mercosur</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Robo Contenido</span></div></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Incendio y Sismo / Estructura</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Incendio y Sismo / Contenido</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Incendio y Robo</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Incendio, Robo y Sismo</span></div></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Hogar Premio a la Permanencia</span></div></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Hogar Full Asistencia</span></div></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Hogar Vacaciones</span></div></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Viaje Grupal</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="300"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Vida con Ahorro</span></div></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Fraude</span></div></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Fraude con devoluci�n</span></div></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Viaje</span></div></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Mascota</span></div></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Viaje Grupal</span></div></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Hospitalizaci�n con devoluci�n</span></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Accidentes Personales con devoluci�n</span></div></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Oncol�gico con devoluci�n</span></div></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><div class="o-features__text">Obt�n tu cotizaci�n online<br><span>Seguro Obligatorio Mercosur</span></div></logic:equal>
			      	</logic:equal> 
			      	
				  <div class="o-features__button"> 
				  	
				  	<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=22'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=21'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=36'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=79'">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=24'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=43'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=44'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=25'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=26'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=201'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=220'">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=3&idSubcategoria=29'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="300"><button type="button" class="o-btn o-btn--primary" onclick="">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=5&idSubcategoria=139'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=5&idSubcategoria=159'">Cotizar</button></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=119'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=140'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=221'">Cotizar</button></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=7&idSubcategoria=161'">Cotizar</button></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=7&idSubcategoria=162'">Cotizar</button></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=7&idSubcategoria=164'">Cotizar</button></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><button type="button" class="o-btn o-btn--primary" onclick="location.href='/cotizador/desplegar-cotizacion.do?idRama=8&idSubcategoria=239'">Cotizar</button></logic:equal>
			      	</logic:equal>
				  </div>  
				  </div> 
				  </div> 
				  </div> 
				  <!-- FIN BTN COTIZAR -->
				  <!-- INICIO LEGAL -->
				  
				  <div class="row"> 
				  <div class="col-12"> 
				  <div class="o-features__featTitle txt_legal linea_bottom" id="txtLegal"> 
				  			<logic:present name="legalFicha" >
			               		 <logic:notEmpty name="legalFicha">
			                        <logic:equal name="habilitarLegalFicha" value="S">
								             <a onclick="openLegal()"><h5>Condiciones legales</h5></a>
							  			     <p class="legalTxt" id="legalTxt" style="display: none;"><bean:write name="legalFicha"/></p>        
								   </logic:equal>
								</logic:notEmpty>
			                </logic:present>
				  </div> 
			      </div> 
				  </div> 
				  <!-- FIN LEGAL -->
                </div>
              </div>
            </div>
          </div>
        </section>
    </main>
<!-- FIN CONTENIDOS --> 

<!-- MODALS -->
 					<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><div class="include" data-path="modal/Seguro_Vehiculo/Seguro_Full_Cobertura.html"></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><div class="include" data-path="modal/Seguro_Vehiculo/Seguro_Perdida_Total.html"></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><div class="include" data-path="modal/Seguro_Hogar/Seguro_Incendio_Sismo_Estructura.html"></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><div class="include" data-path="modal/Seguro_Hogar/Seguro_Incendio_Sismo_Contenido.html"></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><div class="include" data-path="modal/Seguro_Hogar/Seguro_Incendio_Robo.html"></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><div class="include" data-path="modal/Seguro_Hogar/Seguro_Incendio_Robo_Sismo.html"></div></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><div class="include" data-path="modal/Seguro_Hogar/Seguro_Hogar_Premio_Permanencia.html"></div></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><div class="include" data-path="modal/Seguro_Hogar/Seguro_Hogar_Full_Asistencia.html"></div></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><div class="include" data-path="modal/Seguro_Hogar/Seguro_Hogar_Vacaciones.html"></div></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"></div></logic:equal>
				      	<logic:equal name="idSubcategoria" value="300"><div class="include" data-path="modal/Seguro_Vida_Salud/Seguro_Vida_Ahorro.html"></div></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><div class="include" data-path="modal/Seguro_Fraude/Seguro_Fraude.html"></div></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><div class="include" data-path="modal/Seguro_Fraude/Seguro_Fraude_Premio_Permanencia.html"></div></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><div class="include" data-path="modal/Seguro_Viajes/Seguro_Viajes.html"></div></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><div class="include" data-path="modal/Seguro_Vida_Salud/Seguro_Mascotas.html"></div></logic:equal>
						<logic:equal name="idSubcategoria" value="221"></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"></div></logic:equal>
			      	</logic:equal>
<!-- FIN MODAL-->

<%@ include file="/home/includes/footer.jsp"%> 
<script src="/vseg-paris/js/libs/slick.min.js"></script>
	<script src="/vseg-paris/js/libs/jquery.responsiveTabs.js"></script>
	<script src="/vseg-paris/js/main.js"></script>
	<script src="/vseg-paris/js/layout.js"></script>
</body>

</html>
