<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Promociones Vida</title>



<link rel="stylesheet" href="/vseg-paris/css/p7pmm/css/aw_promociones.css" />

 
  
</head>

<body>
		
<div id="contenedorPromociones_rama">
		
			
		
				<!-- SLIDER CENTRAL -->
				<!-- AW -->
				
				<!-- 
                <div id="PromocionCentralAuto">
                	
                    <a target='_blank' href="https://www.segurosparis.cl/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=26">  <img src="/vseg-paris/images/promociones/banner_promociones2.jpg" width="798" height="276" border="0" alt="">  </a>
                
                </div> 
				-->
                <!-- AW -->
				<div id="PromocionCentralAuto">
					<img border="0" width="798" height="276" usemap="#botones" alt="" src="<bean:write name="contextpath"/><bean:write name="listBannerRamaPpal" property="imagen"/>">
					<map name="botones">
					</map>
				</div>
				
			
			
			<div id="tablaPromociones_rama"> 
				<logic:equal name="listBannerRamaPpal" property="tipo" value="BSXR">
					<logic:iterate id="listBanner" name="listBannerRamaPpal">
					<div id="columnaPromociones_auto">
							<div id="calugaPromociones_rama">
								
								<div id="verBases-promociones"> <a  href="<bean:write name="contextpath"/><bean:write name="listBanner" property="link_Ver_Bases"/>" target='_blank'>ver bases</a> </div>
								
								<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
								
								<div id="botonesCaluga"> 
								
									<a target='_parent' href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
									<a onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>

								</div>
							
							</div>
					</div>
				</logic:iterate>
				</logic:equal>
				
			</div>
        </div>
		
</body>
</html>
