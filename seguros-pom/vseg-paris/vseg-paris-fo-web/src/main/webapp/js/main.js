
$(document).ready(function() {
	// Center Image
	$('.js-getImage').each(function(i, e) {
		var image;
		image = $(e).find('.js-image img').attr('src');
		$(e).find('.js-image').css('background-image', 'url(' + image + ')');
		return $(e).find('.js-image');
	});

	
	
	function goTo(target){
		$('html, body').animate({scrollTop: $(target).offset().top}, 600);
	}

	// Menu mobile
	$('body').on('click', '#menu-mobile', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).attr('state', 0);
			$('.c-nav').removeClass('is-active');
		} else {
			$(this).attr('state', 1);
			$('.c-nav').addClass('is-active');
		}
	});

	// Cierra el menu
	$('#close_menu').on('click',function(event) {
		event.preventDefault();
		$('#menu-mobile').trigger('click');
	});

	$('body').on('click','.is_mobile .js-nav-mobile',function(event) {
		event.preventDefault();
		$(this).toggleClass('is-active');
		$(this).parent().find('.c-nav__sub').toggle();

	});

	// Detecta si es mobile
	if( window.matchMedia('(max-width:992px)').matches) {
  		 $('.c-nav').addClass('is_mobile');
  	}


	// Detecta si es mobile
	var consulta = window.matchMedia('(max-width:992px)');
	consulta.addListener(mediaquery);
	function mediaquery(){
		if(consulta.matches){
			$('.c-nav').addClass('is_mobile');
		}else{
			 $('.c-nav').removeClass('is_mobile');
			 $('.c-nav__sub').show();
		}
	}

	// slider ofertas offer
	$('#offer-slick').slick({
		centerMode: true,
		centerPadding: '0px',
		arrows: false,
		slidesToShow: 3,
		dots: false,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '0px',
					slidesToShow: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				}
			}
		]
	});

	// slider reason
	$('#reason-slick').slick({
		centerMode: true,
		centerPadding: '0px',
		arrows: false,
		slidesToShow: 3,
		dots: false,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					arrows: false,
					dots: true,
					centerMode: true,
					centerPadding: '0px',
					slidesToShow: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					dots: true,
					centerMode: true,
					centerPadding: '0px',
					slidesToShow: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					dots: true,
					centerMode: true,
					centerPadding: '00px',
					slidesToShow: 1
				}
			}
		]
	});

	// function toogle
	$('body').on('click', '.js-toogle', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).parents('.o-list').removeClass('is-active');
			$(this).attr('state', 0);
		} else {
			$('.js-toogle').attr('state', 0);
			$('.o-list').removeClass('is-active');
			$(this).parents('.o-list').addClass('is-active');
			$(this).attr('state', 1);
		}
	});

	// Slider
	$('.slider').slick({
		dots: false,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
				responsive: [
				{
					breakpoint: 992,
					settings: {
						arrows: true,
						dots: false,
						centerMode: true,
						centerPadding: '0px',
						slidesToShow: 1
					}
				},
				{
					breakpoint: 768,
					settings: {
						arrows: true,
						dots: false,
						centerMode: true,
						centerPadding: '0px',
						slidesToShow: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						arrows: true,
						dots: false,
						centerMode: false,
						centerPadding: '0px',
						slidesToShow: 1
					}
				}
			]
	});

	$('body').on('click', '.js-collapse', function(event) {
		 $(this).toggleClass('is_active');
		 $(this).next().slideToggle();
	});


});
// end ready

function collapse(event){
	element = event.target;
	event.preventDefault();
	if($(element).attr('state') == 1) {
		$(element).removeClass('is-active');
		$(element).next('.o-collapse__content').removeClass('is-active');
		$(element).attr('state', 0);
	} else {
		$(element).addClass('is-active');
		$(element).next('.o-collapse__content').addClass('is-active');
		$(element).attr('state', 1);
	}
}



// Muestra y esconde tabla comparativa
$('.o-table.o-table--comparative').on('click', '#show_cover', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).parents('.o-table').find('.o-table__row.is-visible').addClass('is-hidden');
			$(this).parents('.o-table').find('.o-table__row.is-hidden').removeClass('is-visible');
			$(this).html('Ver más coberturas');
			$(this).attr('state', 0);
		} else {
			$(this).parents('.o-table').find('.o-table__row.is-hidden').addClass('is-visible');
			$(this).parents('.o-table').find('.o-table__row.is-hidden').removeClass('is-hidden');
			$(this).html('Ver menos coberturas');
			$(this).attr('state', 1);
		}
	});
$('.o-box.o-box--plan').on('click', '.js-show_cover', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).parents('.o-box.o-box--plan').find('.o-box__body').removeClass('is-active');
			$(this).removeClass('is-active');
			$(this).attr('state', 0);
		} else {
			$(this).parents('.o-box.o-box--plan').find('.o-box__body').addClass('is-active');
			$(this).addClass('is-active');
			$(this).attr('state', 1);
		}
	});

// tabs

// select tabs

$('.tabgroup > div').hide();
$('.tabgroup > div:first-of-type').show();
$('.tabs a').click(function(e){
  e.preventDefault();
    var $this = $(this),
        tabgroup = '#'+$this.parents('.tabs').data('tabgroup'),
        others = $this.closest('li').siblings().children('a'),
        target = $this.attr('href');
    others.removeClass('active');
    $this.addClass('active');
    $(tabgroup).children('div').hide();
    $(target).show();
});
// Muestra y esconde los tabs
$('#tabselect').on('change', function() {
	if ( this.value == '#tab1')
	{
		$("#tab1").show();
	}
	else
	{
		$("#tab1").hide();
	}
	if ( this.value == '#tab2')
	{
		$("#tab2").show();
	}
	else
	{
		$("#tab2").hide();
	}
	if ( this.value == '#tab3')
	{
		$("#tab3").show();
	}
	else
	{
		$("#tab3").hide();
	}
	if ( this.value == '#tab4')
	{
		$("#tab4").show();
	}
	else
	{
		$("#tab4").hide();
	}
	if ( this.value == '#tab5')
	{
		$("#tab5").show();
	}
	else
	{
		$("#tab5").hide();
	}
});

// Compare
$('.c-result .o-checkbox__input').each(function(index,element){
	$(element).change(function() {
		var checked = $('.c-result .o-checkbox__input:checked').length;
		if(this.checked) {
			if (checked === 1) {
				$(this).parents('.o-checkbox').addClass('is-checked');
				$(this).parents('.o-checkbox').append(tooltip01);
			}
			if (checked >= 2 && checked <= 4) {
				$('#tooltip01').remove();
				$('#tooltip02').remove();
				$(this).parents('.o-checkbox').addClass('is-checked');
				$(this).parents('.o-checkbox').append(tooltip02);
			}
		}else{
			$(this).parents('.o-checkbox').removeClass('is-checked');
			$('#tooltip02').remove();
			// $('#tooltip01').remove();
		}
		if ($("#tooltip01").length){
			setTimeout(function(){
				$('#tooltip01').fadeOut('400', function() {
					$('#tooltip01').remove();
				});
			},3000);
		};
		if (checked >= 4) {
			$('.c-result .o-checkbox__input:checkbox:not(:checked)').prop("disabled", true);
		}else{
			$('.c-result .o-checkbox__input:checkbox:not(:checked)').prop("disabled", false);
		}
	});

});


// format money
var formatNumber = {
	separador: ".",
	sepDecimal: ',',
	formatear:function (num){
		num +='';
		var splitStr = num.split('.');
		var splitLeft = splitStr[0];
		var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		var regx = /(\d+)(\d{3})/;
		while (regx.test(splitLeft)) {
			splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		}
		return this.simbol + splitLeft  +splitRight;
	},
	new:function(num, simbol){
		this.simbol = simbol ||'';
		return this.formatear(num);
	}
}

// Scroll control
$(window).on("scroll", function () {
	// console.log($(this).scrollTop());
	var altodiv = $('.o-box--recruiting').height() - 260;
	var anchoTotal = $(document).width();
	// console.log(anchoTotal);
	if(anchoTotal > 991 ){
		// console.log('1');
		if ($('.o-box--recruiting').height()>800) {

			if ($(this).scrollTop() > 450 && $(this).scrollTop() < altodiv) {
				$('#summary_sure').addClass('is-fixed');
			}else{
				$('#summary_sure').removeClass('is-fixed');
			}
			if ($(this).scrollTop() > altodiv) {
				$('#summary_sure').addClass('is-absolute');
			}else{
				$('#summary_sure').removeClass('is-absolute');
			}
		}
	}else{
		// console.log('2');
		$('#summary_sure').removeClass('is-fixed');
	}

});

  //  Modal
  $('[data-remodal-id=loginModal]').remodal({
    modifier: 'with-red-theme'
  });

// only numbres
function just_numbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;
	if ((keynum == 8) || (keynum == 46))
	return true;
	return /\d/.test(String.fromCharCode(keynum));
}

// Mandatory inspection
	$('#mandatory_inspection_check').change(function() {
		if(this.checked) {
			$('#mandatory_inspection_input').prop("disabled", false);
		}else{
			$('#mandatory_inspection_input').prop("disabled", true);
		}
	});

// separador
$('#card_number').on('keypress change', function () {
	$(this).val(function (index, value) {
		return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
	});
});
