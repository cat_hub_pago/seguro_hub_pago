


/*
 * Muestra y Oculta un div determinado por su id.
 * 
 * @param {Object} valor {true, false}. true: muestra, false:oculta.
 * @param {Object} nombreDiv id del Elemento
 */
function mostrarDiv(valor, id) {
	var elemento = document.getElementById(id);
	if(valor) {
		elemento.style.display = "none";
	}
	else {
		elemento.style.display = "block";
	}
} 

function datosDuenyoVehiculo(valor){
	var idElemento = "formulario-datos-vehiculo";
	mostrarDiv(valor, idElemento);
}

function datosVivienda(valor) {
	var idElemento = "formulario-datos-vivienda";
	mostrarDiv(valor,idElemento);
}


function resizeIframe(){
	//alert(document.getElementById("iframePlanes").height);
	//alert(document.getElementById('iframePlanes').contentWindow.document.body.scrollHeight + "px");
	document.getElementById("iframePlanes").height = document.getElementById('iframePlanes').contentWindow.document.body.scrollHeight + "px";
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	 
	return true;
}

function validar(e) {
	tecla = (document.all) ? e.keyCode : e.which;
	if (tecla==8) return true;
	patron =/[A-Za-z\s]/;
	te = String.fromCharCode(tecla);
	return patron.test(te);
} 

function isCaracterEmail(evt) {
                var tecla = (evt.which) ? evt.which : evt.keyCode;
                var isCtrl = false;
                var isShift = false;
                var teclasPermitidas = new Array("a","n","c","x","v","j", "~", "�");
                var codigosNoPermitidos = new Array('!', '"', '#', '$', '%', '&', '/', '(', ')', '=', '\'', '�', '�', '?', ':', ';', ',', '�', '|', '�', '�', '+', '{', '}', '[', ']', '<', '>', '/', '*', '�', '�', '~');
                var noPermitidosSinShift = new Array("|", "/", "*",  "+", "<", "�", "{", "}", ",", "'", "�");
                if (window.event) {
                               key = window.event.keyCode;     //IE      
                               isCtrl = false;
                               if (window.event.ctrlKey) { isCtrl = true; }
                               if (window.event.shiftKey) { isShift = true; }
                } else {                                 
                               key = evt.which;     //firefox
                               isCtrl = false;
                               if (evt.ctrlKey) { isCtrl = true; }
                               if (evt.shiftKey) { isShift = true; }
                }
                if (isCtrl) {
                               for (i = 0; i < teclasPermitidas.length; i++) {
                                               if (teclasPermitidas[i].toLowerCase() == String.fromCharCode(key).toLowerCase()) {
                                                               return false;
                                                               break;
                                               }
                               }
                }
                if (isShift) {
                               for (j = 0; j < codigosNoPermitidos.length; j++) {
                                               if (codigosNoPermitidos[j] == String.fromCharCode(key).toLowerCase()) {
                                                               return false;
                                                               break;
                                               }
                               }                              
                } else {
                               for (k = 0; k < noPermitidosSinShift.length; k++) {
                                               if (noPermitidosSinShift[k] == String.fromCharCode(key).toLowerCase()) {
                                                               return false;
                                                               break;
                                               }
                               } 
                }                              
}
