package cl.cencosud.ventaseguros.common.struts.form;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.common.struts.action.ThreadPrueba;

public class PruebaThreads {

	int value;
	int valueASumar;
	int numero;
	List < Map < String, ? > > promocionesVehiculo = null;
	Thread a = null;
	
	public List<Map<String, ?>> getPromocionesVehiculo() {
		return promocionesVehiculo;
	}

	public void setPromocionesVehiculo(List<Map<String, ?>> promocionesVehiculo) {
		this.promocionesVehiculo = promocionesVehiculo;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public int getValueASumar() {
		return valueASumar;
	}

	public void setValueASumar(int valueASumar) {
		this.valueASumar = valueASumar;
	}

	public void setData(int str) {
		
		this.value = str;
        //process returned data
    }
	
	public int getData() {
		
		return this.value;
        //process returned data
    }
	

	public Thread getA() {
		return a;
	}

	public void setA(Thread a) {
		this.a = a;
	}
  
    public void requestData() {
        try{
            //new Thread(new ThreadPrueba(this)).start();
        	ThreadPrueba asd = new ThreadPrueba(this);
            a = new Thread(asd,"Prueba Runnable"+this.numero+"");
            setA(a);
            a.start();
            //a.join();
        } catch (Exception e){                      
            //e.printStackTrace();
    }
}

}
    
