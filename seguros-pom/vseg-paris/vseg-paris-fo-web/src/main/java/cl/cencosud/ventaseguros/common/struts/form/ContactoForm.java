package cl.cencosud.ventaseguros.common.struts.form;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import cl.tinet.common.struts.form.BuilderActionFormBaseVSPFO;
import cl.tinet.common.util.validate.ValidacionUtil;

public class ContactoForm extends BuilderActionFormBaseVSPFO {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8933764463245419354L;

    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return ContactoForm.class
            .getResourceAsStream("resource/validation.xml");
    }

    @Override
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {

        ActionErrors errores = super.validate(mapping, request);

        if (this.getDatos().get("rut").equals("") || this.getDatos().get("dv").equals("")) {
            errores.add("datos.rut", new ActionMessage("errors.required"));
            errores.add("datos.dv", new ActionMessage("errors.required"));
            errores.add("datos.rut_grupo", new ActionMessage("errors.required"));
        } else if (errores.get("datos.rut").hasNext()) {
            errores.add("datos.dv", new ActionMessage("errors.required"));
            errores.add("datos.rut_grupo", new ActionMessage("errors.validacion"));
        } else {
            try {
                if (!ValidacionUtil.isValidoRUT(Long.parseLong(this.getDatos().get("rut")), this.getDatos().get("dv").charAt(0))) {
                    errores.add("datos.rut", new ActionMessage("errors.required"));
                    errores.add("datos.dv", new ActionMessage("errors.required", "Digito Verificador"));
                    errores.add("datos.rut_grupo", new ActionMessage("errors.validacion"));
                }
            } catch (IllegalArgumentException e) {
                errores.add("datos.rut", new ActionMessage("error.required"));
                errores.add("datos.dv", new ActionMessage("error.required"));
                errores.add("datos.rut_grupo", new ActionMessage("errors.validacion"));
            }
        }

        if (this.getDatos().get("numeroTelefono1").equals("")) {
            errores.add("datos.telefono_grupo", new ActionMessage("errors.required"));
        } else if (errores.get("numeroTelefono1") != null) {
            try {
                new Long(this.getDatos().get("numeroTelefono1"));
            } catch (NumberFormatException e) {
                errores.add("datos.telefono_grupo", new ActionMessage("errors.validacion"));
            }
        }

        if (this.getDatos().get("numeroTelefono2")!=null && !"".equals(this.getDatos().get("numeroTelefono2"))) {
            try {
                new Long(this.getDatos().get("numeroTelefono2"));
            } catch (NumberFormatException e) {
                errores.add("datos.numeroTelefono2", new ActionMessage("errors.validacion"));
                errores.add("datos.telefono2_grupo", new ActionMessage("errors.validacion"));
            }
        }
        
        request.setAttribute(PROPERTY_NAMES, errores.properties());
        return errores;
    }

}
