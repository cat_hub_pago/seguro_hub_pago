package cl.cencosud.ventaseguros.seguridad.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** 
 * MyEclipse Struts
 * Creation date: 08-25-2010
 * 
 * XDoclet definition:
 * @struts.action validate="true"
 */
public class CambioClaveAction extends Action {

    /**
     * Atributo de flujo de exito.
     */
    public static final String FLUJO_EXITO = "continuar";

    /** 
     * Metodo ActionForward struts.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        return mapping.findForward(FLUJO_EXITO);
    }
}