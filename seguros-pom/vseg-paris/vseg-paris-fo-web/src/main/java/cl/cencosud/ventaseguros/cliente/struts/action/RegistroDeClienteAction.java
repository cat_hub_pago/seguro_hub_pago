package cl.cencosud.ventaseguros.cliente.struts.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.cliente.struts.form.RegistroClienteForm;
import cl.cencosud.ventaseguros.common.EstadoCivil;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;


public class RegistroDeClienteAction extends Action {
	
	private static final Log logger = LogFactory.getLog(RegistroDeClienteAction.class);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		RegistroClienteForm oForm = (RegistroClienteForm)form;
		
		String rutCliente = request.getParameter("rut_cliente");
		String dvCliente = request.getParameter("dv_cliente");
		
		MantenedorClientesDelegate delegate = new MantenedorClientesDelegate();
		
		Map<String, String> datos = oForm.getDatos();
		datos.put("maxDias", "31");
		
		if (rutCliente != null && dvCliente != null && !rutCliente.equals("") && !dvCliente.equals("")) {
			datos.put("rut_cliente", rutCliente);
			datos.put("dv_cliente", dvCliente);
			request.setAttribute("sinRut", "false");
		}
		
		
		oForm.setDatos(datos);
		logger.debug(datos);
		
		//Tipos de telefono.
		List<Map<String, ?>> tiposTelefono = delegate.obtenerParametro("TIPO_TELEFONO");
		request.setAttribute("tiposTelefono", tiposTelefono);
		logger.debug(tiposTelefono);

		//Codigos de area.
		List<Map<String, ?>> codigosArea = delegate.obtenerParametro("TEL_CODIGO_AREA");
		request.setAttribute("codigosArea", codigosArea);
		logger.debug(codigosArea);

		//Meses
		List<Map<String, ?>> meses = delegate.obtenerParametro("MESES");
		request.setAttribute("meses", meses.iterator());
		logger.debug(meses);

		//Agnos
		List<Map<String, ?>> anyos = delegate.obtenerParametro("ANYOS");
		request.setAttribute("anyos", anyos.iterator());
		logger.debug(anyos);
		
		//Estado civil.
		EstadoCivil[] estadosCiviles = delegate.obtenerEstadosCiviles();
		request.setAttribute("estadosCiviles", estadosCiviles);
		logger.debug(estadosCiviles);

		//Sexo
		List<Map<String, ?>> sexos = delegate.obtenerParametro("SEXO");
		request.setAttribute("sexos", sexos.iterator());
		logger.debug(sexos);
		
		//Region
		Parametro[] regiones = delegate.obtenerRegiones();
		request.setAttribute("regiones", regiones);
		logger.debug(regiones);
		
		String idRegion = "-1";
		String idComuna = "-1";
		
		//Comuna
		Parametro[] comunas = delegate.obtenerComunas(idRegion);
		request.setAttribute("comunas", comunas);
		logger.debug(comunas);
		
		//Ciudad
		Parametro[] ciudades = delegate.obtenerCiudades(idComuna);
		request.setAttribute("ciudades", ciudades);
		logger.debug(ciudades);
		
		return mapping.findForward("continuar");
	}
	
	

}
