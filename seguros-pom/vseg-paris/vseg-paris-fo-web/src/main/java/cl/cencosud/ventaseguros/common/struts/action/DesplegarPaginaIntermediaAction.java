package cl.cencosud.ventaseguros.common.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

/**
 * Despliega la vista de Pagina Intermedia.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 08/10/2010
 */
public class DesplegarPaginaIntermediaAction extends Action {

    private static final Log logger =
        LogFactory.getLog(DesplegarPaginaIntermediaAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        String idRama =
            request.getParameter("idRama") == null ? "1" : request
                .getParameter("idRama");

        logger.info("Obteniendo subcategorias (Id Rama:" + idRama + ")");

        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
        List < Subcategoria > listado =
            delegate.obtenerSubcategoriasPaginaIntermedia(Integer
                .parseInt(idRama));
        Subcategoria[] subcategorias = null;

        if (listado != null && !listado.isEmpty()) {
            logger.info("Subcategorias encontradas:" + listado.size());
            subcategorias =
                (Subcategoria[]) listado.toArray(new Subcategoria[listado
                    .size()]);
        }
        
        if(request.getSession().getAttribute("idPlan") != null) {
            request.getSession().removeAttribute("idPlan");
        }

        request.setAttribute("subcategorias", subcategorias);
        request.setAttribute("tipoSeguro", idRama);

        return mapping.findForward("desplegarSubcategorias");
    }

}
