package cl.cencosud.ventaseguros.common.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import cl.tinet.common.struts.form.BuilderActionFormBaseVSPFO;
import cl.tinet.common.util.validate.ValidacionUtil;

public class CotizadorForm extends BuilderActionFormBaseVSPFO{

	private static final long serialVersionUID = 7550819922613112246L;

	public InputStream getValidationRules(HttpServletRequest request) {
        return ContactoForm.class.getResourceAsStream("resource/validation.xml");
	}
	
	@Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		
        ActionErrors errores = super.validate(mapping, request);
               
        if (!errores.get("datos.dv").hasNext() && !errores.get("datos.rut_cliente").hasNext() 
        		&& !this.getDatos().get("rut").equals("") && !this.getDatos().get("dv").equals("")) {
        	long numRUT = Long.parseLong(this.getDatos().get("rut"));
            char digitoVerificador = this.getDatos().get("dv").length() == 0 ? ' ' : this.getDatos().get("dv").charAt(0);
            try {
            	if (!ValidacionUtil.isValidoRUT(numRUT, digitoVerificador)) {
	            	errores.add("datos.rutdv", new ActionMessage("errors.validacion"));
	                errores.add("datos.dv", new ActionMessage("errors.validacion"));
	                errores.add("datos.rut", new ActionMessage("errors.validacion", true));
                }
	        } catch (IllegalArgumentException ie) {
	        	errores.add("datos.rutdv",new ActionMessage("errors.validacion"));
	            errores.add("datos.rutdv", new ActionMessage("errors.validacion"));
	            errores.add("datos.dv", new ActionMessage("errors.validacion"));
	            errores.add("datos.rut", new ActionMessage("errors.validacion"));
	        }
        } else {
	    	errores.add("datos.rutdv", new ActionMessage("errors.required"));
	        errores.add("datos.rut", new ActionMessage("errors.required"));
	        errores.add("datos.dv", new ActionMessage("errors.required"));
        }
        
        if(!((String)this.getDatos().get("nombre")).equals("")) {
        	if(errores.get("datos.nombre").hasNext()) {
        		errores.add("datos.nombres", new ActionMessage("errors.validacion"));
            }
        }else{
        	errores.add("datos.nombres", new ActionMessage("errors.required"));
        }        
        
        if (this.getDatos().get("telefono_1").equals("")) {
        	errores.add("datos.telefono_1", new ActionMessage("errors.required"));
        }
        
        request.setAttribute(PROPERTY_NAMES, errores.properties());
        return errores;
    }

}
