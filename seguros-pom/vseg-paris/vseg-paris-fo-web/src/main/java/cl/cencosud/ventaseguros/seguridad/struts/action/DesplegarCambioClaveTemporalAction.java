package cl.cencosud.ventaseguros.seguridad.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCambioClaveTemporalAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        
        String forward = "";
        
        if(request.getParameter("desplegarClaveTemporal") == null){
            // atributo que permite levantar lightbox cambio clave en la pagina principal.
            request.getSession().setAttribute("frameClaveTemporal", "true");
            forward = "inicio";
        }
        else{
            // flujo proviene desde la pagina principal.
            // atributo que discrimina tipo de clave en la p�gina de cambio de clave.
            
            request.setAttribute("esCambioClaveTemporal", "true");
            request.getSession().removeAttribute("frameClaveTemporal");
            
            forward = "continuar";
        }
        return mapping.findForward(forward);
    }

}
