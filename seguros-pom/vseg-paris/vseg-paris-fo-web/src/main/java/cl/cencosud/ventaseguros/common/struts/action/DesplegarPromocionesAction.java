package cl.cencosud.ventaseguros.common.struts.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

/**
 * Action encargado de desplegar una Ficha de Subcategoria.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 01/10/2010
 */
public class DesplegarPromocionesAction extends Action {
	
	private static String TIPO_BANER_PPAL = "PPBP";
	
	private static String TIPO_BANNER_SEC = "PPBS";
	
	private static String TIPO_BANNER_PPAL_RAMA = "BPXR";
	
	private static String TIPO_BANNER_SEC_RAMA = "BSXR";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        String idRama = request.getParameter("idRama");
        String idSubcategoria = request.getParameter("idSubcategoria");
        if (idRama != null){
        FichaSubcategoriaDelegate delegate = new FichaSubcategoriaDelegate();
        
        List < Map < String, ? > > promocionesVehiculo =
            delegate.obtenerPlanesPromocion(1, 0);
        if (promocionesVehiculo.size() > 0) {
            request.setAttribute("promocionesVehiculo", promocionesVehiculo);
        }

        List < Map < String, ? > > promocionesHogar =
            delegate.obtenerPlanesPromocion(2, 0);
        if (promocionesHogar.size() > 0) {
            request.setAttribute("promocionesHogar", promocionesHogar);
        }

        List < Map < String, ? > > promocionesVida =
            delegate.obtenerPlanesPromocion(3, 0);
        if (promocionesVida.size() > 0) {
            request.setAttribute("promocionesVida", promocionesVida);
        }

        List < Map < String, ? > > promocionesSalud =
            delegate.obtenerPlanesPromocion(4, 0);
        if (promocionesSalud.size() > 0) {
            request.setAttribute("promocionesSalud", promocionesSalud);
        }

        List < Map < String, ? > > promocionesCesantia =
            delegate.obtenerPlanesPromocion(5, 0);
        if (promocionesCesantia.size() > 0) {
            request.setAttribute("promocionesCesantia", promocionesCesantia);
        }

        List < Map < String, ? > > promocionesFraude =
            delegate.obtenerPlanesPromocion(6, 0);
        if (promocionesFraude.size() > 0) {
            request.setAttribute("promocionesFraude", promocionesFraude);
        }
        }else {
        	idRama = "50";
        	//Se agrega un valor de 50 para que el valor del idRama se encuentre fuera de los rangos permitidos
        }
        PaginaIntermediaDelegate delegateInter = new PaginaIntermediaDelegate();
        if (idRama.equalsIgnoreCase("50")){
        
        List<HashMap<String, Object>> listBannerPpal = delegateInter.obtenerPromociones(TIPO_BANER_PPAL);
        request.setAttribute("listBannerPpal", listBannerPpal);
        
        List<HashMap<String, Object>> listBannerSec =  delegateInter.obtenerPromociones(TIPO_BANNER_SEC);
        request.setAttribute("listBannerSec", listBannerSec);
        } else if (idRama.equalsIgnoreCase("1")){
        	List<HashMap<String, Object>> listBannerRamaPpal = delegateInter.obtenerPromocionesSecundarias(idRama);
        	request.setAttribute("listBannerRamaPpal", listBannerRamaPpal);
        } else if (idRama.equalsIgnoreCase("2")){
        	List<HashMap<String, Object>> listBannerRamaPpal = delegateInter.obtenerPromocionesSecundarias(idRama);
        	request.setAttribute("listBannerRamaPpal", listBannerRamaPpal);
        } else if (idRama.equalsIgnoreCase("3")){
        	List<HashMap<String, Object>> listBannerRamaPpal = delegateInter.obtenerPromocionesSecundarias(idRama);
        	request.setAttribute("listBannerRamaPpal", listBannerRamaPpal);
        } else if (idRama.equalsIgnoreCase("4")){
        	List<HashMap<String, Object>> listBannerRamaPpal = delegateInter.obtenerPromocionesSecundarias(idRama);
        	request.setAttribute("listBannerRamaPpal", listBannerRamaPpal);
        }
        
        // Se depliega la Rama a la cual corresponden las subcategorias.
        request.setAttribute("idRama", idRama);
        request.setAttribute("idSubcategoria", idSubcategoria);

        return mapping.findForward("desplegarPromociones");

    }
}
