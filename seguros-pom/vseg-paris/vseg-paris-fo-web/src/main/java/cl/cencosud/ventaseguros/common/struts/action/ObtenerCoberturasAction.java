package cl.cencosud.ventaseguros.common.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ObtenerCoberturasAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        MantenedorClientesDelegate delegate = new MantenedorClientesDelegate();
        int idPlan = -1;
        try {

            String oIdPlan = request.getParameter("idPlan");

            if (oIdPlan != null && !oIdPlan.equals("")) {
                idPlan = Integer.parseInt(oIdPlan);
            }

            List < Map < String, Object > > coberturas =
                delegate.obtenerCoberturas(idPlan);

            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, coberturas);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        } catch (BusinessException e) {
            throw new SystemException(e);
        }

        return null;
    }
}
