package cl.cencosud.ventaseguros.common.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;



import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

import com.tinet.exceptions.system.SystemException;

/**
 * Action encargado de desplegar subcategorias.
 * <br/>
 * @author fgaete
 * @version 1.0
 * @created 30/09/2010
 */
public class ObtenerPromocionesAction extends Action{		
	public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

		PaginaIntermediaDelegate oDelegate = new PaginaIntermediaDelegate();
		String tipo = request.getParameter("tipo");
		//String subcategoria = request.getParameter("subcategoria");
		List < HashMap < String, Object > > promociones = new ArrayList < HashMap < String, Object > >();
		if (tipo != ""){
		    promociones = oDelegate.obtenerPromociones(tipo);	
		}
        
        return null;
        }	
}