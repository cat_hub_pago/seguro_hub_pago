package cl.cencosud.ventaseguros.common.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.struts.form.ContactoForm;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;

public class EnviarFormularioContactoAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        ContactoForm oForm = (ContactoForm) form;

        MantenedorClientesDelegate oDelegate = new MantenedorClientesDelegate();

        UsuarioExterno usuario = new UsuarioExterno();
        usuario.setRut_cliente(oForm.getDatos().get("rut"));
        usuario.setDv_cliente(oForm.getDatos().get("dv"));
        usuario.setNombre(oForm.getDatos().get("nombre"));
        usuario.setApellido_paterno(oForm.getDatos().get("apellidoPaterno"));
        usuario.setApellido_materno(oForm.getDatos().get("apellidoMaterno"));
        usuario.setEmail(oForm.getDatos().get("email"));

        usuario.setTipo_telefono_1(oForm.getDatos().get("tipoTelefono1"));
        String nroTelefono1 =
            oForm.getDatos().get("codigoTelefono1")
                + oForm.getDatos().get("numeroTelefono1");
        usuario.setTelefono_1(nroTelefono1);

        if (!oForm.getDatos().get("numeroTelefono2").equals("")) {
            String nroTelefono2 =
                oForm.getDatos().get("codigoTelefono2")
                    + oForm.getDatos().get("numeroTelefono2");

            usuario.setTipo_telefono_2(oForm.getDatos().get("tipoTelefono2"));
            usuario.setTelefono_2(nroTelefono2);
        } else {
            usuario.setTipo_telefono_2("");
            usuario.setTelefono_2("");
        }
        
        String tipoContacto = oForm.getDatos().get("tipoComentario");
        String mensajeContacto = oForm.getDatos().get("comentario");
        
        oDelegate.enviarEmailFormularioContacto(usuario, tipoContacto,
            mensajeContacto);

        request.getSession().setAttribute("send", "1");
        
        return mapping.findForward("continuar");

    }

}
