package cl.cencosud.ventaseguros.common.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.struts.form.DenunciasForm;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;


public class ContactoDenunciasAction extends Action{
	  @Override
	    public ActionForward execute(ActionMapping mapping, ActionForm form,
	        HttpServletRequest request, HttpServletResponse response)
	        throws Exception {   
		  
	    
	    	MantenedorClientesDelegate oDelegate = new MantenedorClientesDelegate();
	    	String donde = null;
	    	String url = "errorgeneral";
	    	String ruta = "https://" + request.getServerName() + request.getContextPath();
			//String ruta = request.getScheme()+ "://" + request.getServerName() + request.getContextPath();			

	    	System.out.println("RUTA Denuncias  :  " + ruta);
	    	
	    	donde = request.getParameter("donde");
	    	DenunciasForm denunciasForm = (DenunciasForm) form;
	    	request.setAttribute("DenunciasFormu", denunciasForm);

	    	if(donde.equals("inicio") ){
	    		url = "inicio";
	    		return mapping.findForward(url);
	    		   		
	    	}else if(donde.equals("enviar")){
			
				String captcha = request.getParameter("g-recaptcha-response"); //CBM nuevo captcha
		
				 // CBM nuevo captcha 
				if (captcha == null || ("").equals(captcha.trim())){
				  request.setAttribute("errorCaptcha","invalido");
				  url = "inicio";
				  return mapping.findForward(url);
				}else{
							
					try{
					String FolioDenuncia = oDelegate.generarSecuenciaDenuncia();
					
					if(!FolioDenuncia.equals("NOK")){
						
						FolioDenuncia = getNumConCeros(Long.valueOf(FolioDenuncia), 10);
						denunciasForm.setFolioDenuncia("TC" + FolioDenuncia);
						
						String folio = denunciasForm.getFolioDenuncia();
						String rut = denunciasForm.getRutDenuncia();
						String nombre = denunciasForm.getNombreDenuncia();
						String apellidoPat = denunciasForm.getApePatDenuncia();
						String apellidoMat = denunciasForm.getApeMatDenuncia();
						String direccion = denunciasForm.getDireccionDenuncia();
						String comuna = denunciasForm.getComunaDenuncia();
						String ciudad = denunciasForm.getCiudadDenuncia();
						String fonoFijo = denunciasForm.getTelefonoFijoDenuncia();
						String fonoCelular = denunciasForm.getTelefonoCelularDenuncia();
						String detalle = denunciasForm.getDetalleDenuncia();

						oDelegate.enviarEmailDenuncias(ruta,folio, rut, nombre, apellidoPat, apellidoMat, direccion, comuna, ciudad, fonoFijo, fonoCelular, detalle);
						url = "exito";
					}else{

					url = "errorgeneral";
					}
					
					}catch(Exception e){
						e.printStackTrace();
						url = "errorgeneral";
						return mapping.findForward(url);
					}
				}
			}
   	
	 return mapping.findForward(url);
}

	  
public static String getNumConCeros(Long num, int largo) {
	    			String numero;
	    			if (num != null)
	    			    numero = num.toString();
	    			else
	    			    numero = "0";
	    			// Completa con ceros a la izquierda
	    			for (int i = numero.length(); numero.length() < largo; i++) {
	    			    numero = "0" + numero;
	    			}
	    			return numero;
}
	    		    	
}
