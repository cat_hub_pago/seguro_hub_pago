package cl.cencosud.ventaseguros.common.struts.form;

import org.apache.struts.action.ActionForm;

public class DenunciasForm extends ActionForm {

	/**
	 * 
	 */
private static final long serialVersionUID = 1L;

	 	private String detalleDenuncia ;
	    private String rutDenuncia ;
	    private String nombreDenuncia ;
	    private String apePatDenuncia ;
	    private String apeMatDenuncia ;
	    private String direccionDenuncia ;
	    private String comunaDenuncia ;
	    private String ciudadDenuncia ;
	    private String telefonoFijoDenuncia ;
	    private String telefonoCelularDenuncia ;
	    private String folioDenuncia;
	    private String flagHtml ;
	    private String codTelFijo ;
	    private String codCelular;
	    
	    public String getCodTelFijo() {
	        return codTelFijo;
	    }
	    public void setCodTelFijo(String codTelFijo) {
	        this.codTelFijo = codTelFijo;
	    }
	    public String getCodCelular() {
	        return codCelular;
	    }
	    public void setCodCelular(String codCelular) {
	        this.codCelular = codCelular;
	    }
	    public String getFlagHtml() {
	        return flagHtml;
	    }
	    public void setFlagHtml(String flagHtml) {
	        this.flagHtml = flagHtml;
	    }
	    public String getDetalleDenuncia() {
	        return detalleDenuncia;
	    }
	    public void setDetalleDenuncia(String detalleDenuncia) {
	        this.detalleDenuncia = detalleDenuncia;
	    }
	    public String getRutDenuncia() {
	        return rutDenuncia;
	    }
	    public void setRutDenuncia(String rutDenuncia) {
	        this.rutDenuncia = rutDenuncia;
	    }
	    public String getNombreDenuncia() {
	        return nombreDenuncia;
	    }
	    public void setNombreDenuncia(String nombreDenuncia) {
	        this.nombreDenuncia = nombreDenuncia;
	    }
	    public String getApePatDenuncia() {
	        return apePatDenuncia;
	    }
	    public void setApePatDenuncia(String apePatDenuncia) {
	        this.apePatDenuncia = apePatDenuncia;
	    }
	    public String getApeMatDenuncia() {
	        return apeMatDenuncia;
	    }
	    public void setApeMatDenuncia(String apeMatDenuncia) {
	        this.apeMatDenuncia = apeMatDenuncia;
	    }
	    public String getDireccionDenuncia() {
	        return direccionDenuncia;
	    }
	    public void setDireccionDenuncia(String direccionDenuncia) {
	        this.direccionDenuncia = direccionDenuncia;
	    }
	    public String getComunaDenuncia() {
	        return comunaDenuncia;
	    }
	    public void setComunaDenuncia(String comunaDenuncia) {
	        this.comunaDenuncia = comunaDenuncia;
	    }
	    public String getCiudadDenuncia() {
	        return ciudadDenuncia;
	    }
	    public void setCiudadDenuncia(String ciudadDenuncia) {
	        this.ciudadDenuncia = ciudadDenuncia;
	    }
	    public String getTelefonoFijoDenuncia() {
	        return telefonoFijoDenuncia;
	    }
	    public void setTelefonoFijoDenuncia(String telefonoFijoDenuncia) {
	        this.telefonoFijoDenuncia = telefonoFijoDenuncia;
	    }
	    public String getTelefonoCelularDenuncia() {
	        return telefonoCelularDenuncia;
	    }
	    public void setTelefonoCelularDenuncia(String telefonoCelularDenuncia) {
	        this.telefonoCelularDenuncia = telefonoCelularDenuncia;
	    }
	    public String getFolioDenuncia() {
	        return folioDenuncia;
	    }
	    public void setFolioDenuncia(String folioDenuncia) {
	        this.folioDenuncia = folioDenuncia;
	    }	
	
}
