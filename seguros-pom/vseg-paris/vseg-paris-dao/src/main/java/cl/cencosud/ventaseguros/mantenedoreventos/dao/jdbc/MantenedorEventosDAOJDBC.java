package cl.cencosud.ventaseguros.mantenedoreventos.dao.jdbc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.mantenedoreventos.dao.MantenedorEventosDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

/**
 * Clase DAO.
 *
 * @author Ricardo
 *
 */
public class MantenedorEventosDAOJDBC extends ManagedBaseDAOJDBC implements
    MantenedorEventosDAO {

    private static final Log LOGGER =
        LogFactory.getLog(MantenedorEventosDAOJDBC.class);

    /**
     * String relacionado con la query para obtener los codigos de eventos.
     */
    private static final String OBTENER_CODIGOS_EVENTOS =
        "Select  NOMBRE,CODIGO_EVENTO FROM EVENTO";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO =
        " Select" + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion," + " POSICION_EVENTO as posicion, "
            + " FECHA_INICIO," + " FECHA_TERMINO," + " TOTAL_RUTS, "
            + " RUTS_LISTOS," + " OCURRENCIA ,"
            + " ESTADO_EVENTO.NOMBRE as estado_evento"
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO)"
            + "   inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_FECHA =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + " inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO "
            + " = EVENTO.POSICION) WHERE FECHA_INICIO >= ? "
            + " AND FECHA_TERMINO <= ? AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_FECHA_TIPO_EVENTO =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + " inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE FECHA_INICIO >= ? AND FECHA_TERMINO <= ?"
            + " AND (0 = ? OR EVENTO.ESTADO =  ?) AND EVENTO.TIPO_EVENTO = ? ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_TRAMO_FECHA =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + " inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE FECHA_INICIO >= ? AND FECHA_TERMINO <= ? "
            + " AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_FILTRO =
        "   Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + " inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE FECHA_INICIO >= ? AND FECHA_TERMINO <= ? AND "
            + "EVENTO.POSICION = ? AND EVENTO.TIPO_EVENTO = ?  "
            + " AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_FECHA_TIPO_EVENTO_SIN_FECHAS =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE (0 = ? OR EVENTO.ESTADO =  ?) AND EVENTO.TIPO_EVENTO = ? ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_TIPO =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + "   AND EVENTO.TIPO_EVENTO = ? ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_FECHA_TERMINO_EVENTO =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " AND FECHA_TERMINO <= ? AND (0 = ? OR EVENTO.ESTADO =  ?) " +
                      "AND EVENTO.TIPO_EVENTO = ? ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_FECHA_INICIO_EVENTO =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  "
            + " POSICION_EVENTO as posicion, " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE FECHA_INICIO >= ? AND (0 = ? OR EVENTO.ESTADO =  ?) AND "
            + " EVENTO.TIPO_EVENTO = ? ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_POSICION_COMBO =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE EVENTO.POSICION = ? AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_POSICION_TRAMO_FECHA =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE FECHA_INICIO >= ? AND FECHA_TERMINO <= ? "
            + "AND EVENTO.POSICION = ? AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_POSICION =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO ="
            + " EVENTO.POSICION) WHERE FECHA_INICIO >= ? "
            + " AND FECHA_TERMINO  <= ? EVENTO.POSICION = ? "
            + "AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_TIPO_PAGINA =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " ESTADO_EVENTO.NOMBRE as estado_evento,  "
            + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE EVENTO.TIPO_PAGINA = ? AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_INICIAL =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + " (POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE FECHA_INICIO >= ? AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_TERMINAL =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE FECHA_TERMINO <= ? AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_FECHA_ESTADO =
        " Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "   inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE  (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para buscar eventos.
     */
    private static final String BUSCAR_EVENTO_CODIGO =
        "  Select " + " ID_EVENTO ," + " EVENTO.nombre as nombre, "
            + " descripcion,  " + " FECHA_INICIO,  " + " FECHA_TERMINO,  "
            + " TOTAL_RUTS,   " + " RUTS_LISTOS,  " + " OCURRENCIA , "
            + " POSICION_EVENTO as posicion, "
            + " ESTADO_EVENTO.NOMBRE as estado_evento  "
            + " FROM  EVENTO inner join ESTADO_EVENTO  "
            + " on( EVENTO.ESTADO = ESTADO_EVENTO.ID_ESTADO_EVENTO) "
            + "  inner join POSICIONES_EVENTO on "
            + "(POSICIONES_EVENTO.ID_POSICION_EVENTO = EVENTO.POSICION) "
            + " WHERE EVENTO.CODIGO_EVENTO = ? AND (0 = ? OR EVENTO.ESTADO =  ?) ";

    /**
     * String relacionado con la query para insercion de una
     * fotografia en la base de datos.
     */
    private static final String INSERTAR_FOTOGRAFIA =
        " INSERT INTO IMAGEN_EVENTO(ID_IMAGEN, ID_EVENTO,"
            + " IMAGEN, FECHA_CREACION, FECHA_MODIFICACION) "
            + " VALUES(INCREMENTO_IMAGEN_EVENTO.nextval, ?, ?,"
            + " {fn CURTIME()}, {fn CURTIME()}) ";

    /**
     *String relacionado con la query para la modificacion
     * de una fotografia en la base de datos.
     */
    private static final String MODIFICAR_FOTOGRAFIA =
        "UPDATE IMAGEN_EVENTO "
            + "SET IMAGEN= ?, FECHA_MODIFICACION={fn CURTIME()} "
            + "WHERE ID_IMAGEN = ? ";

    /**
     * String relacionado con la query para modificar
     *  un archivo RUT en la base de datos
     */
    private static final String MODIFICAR_ARCHIVO_RUT =
        " UPDATE VSP_ADM.ARCHIVO_RUT "
            + " SET ARCHIVO_SEGMENTADO=?, FECHA_MODIFICACION={fn CURTIME()} "
            + " WHERE ARCHIVO_RUT.ID_ARCHIVO =? ";

    /**
     * String relacionado con la query para
     * obtener el ID de una fotografia.
     */
    private static final String OBTENER_ID_FOTOGRAFIA =
        "SELECT ID_IMAGEN FROM IMAGEN_EVENTO inner join EVENTO "
            + " on(IMAGEN_EVENTO.ID_EVENTO = EVENTO.ID_EVENTO) "
            + " WHERE EVENTO.ID_EVENTO = ? ";

    /**
     * String relacionado con la query para obtener el ID de un archivo rut.
     */
    private static final String OBTENER_ID_ARCHIVO_RUT =
        " SELECT ARCHIVO_RUT.ID_ARCHIVO FROM ARCHIVO_RUT inner join EVENTO on "
            + " (ARCHIVO_RUT.ID_EVENTO = EVENTO.ID_EVENTO)"
            + " WHERE EVENTO.ID_EVENTO = ? ";

    /**
     * String relacionado con la query para insertar archivo rut.
     */
    private static final String INSERTAR_ARCHIVOS_RUT =
        " INSERT INTO ARCHIVO_RUT(ID_ARCHIVO, ID_EVENTO,"
            + " ARCHIVO_SEGMENTADO, FECHA_CREACION, FECHA_MODIFICACION) "
            + " VALUES(INCREMENTO_ARCHIVO_RUT.nextval, ?,?,"
            + " {fn CURTIME()}, {fn CURTIME()})";

    /**
     * String relacionado con la query para insertar un nuevo evento.
     */
    private static final String INSERTAR_EVENTO =
        " INSERT INTO EVENTO("
            + " ID_EVENTO , ID_RAMA , CODIGO_EVENTO , ID_PAIS ,"
            + " NOMBRE , DESCRIPCION , FECHA_INICIO , "
            + "FECHA_TERMINO , OCURRENCIA , TIPO_IMAGEN ,"
            + " URL , TIPO_PAGINA , ESTADO , POSICION ,"
            + " PRIORIDAD , TIPO_EVENTO , SEGMENTADO ,"
            + " FECHA_CREACION , FECHA_MODIFICACION , ES_PLAN_PROMOCION )"
            + " VALUES(INCREMENTO_EVENTO.nextval,"
            + "?, ?, ?, ?, ?, ?,?, ?,?, ?, ?, ?, ?,"
            + " ?, ? , ?,{fn CURTIME()} ,{fn CURTIME()}, ?)";

    /**
     * String relacionado con obtener la cantidad total de rut listos.
     */
    private static final String CANTIDAD_TOTAL_RUT_LISTOS =
        " Select count(RUT_EVENTO.ID_RUT_EVENTO) as total FROM EVENTO"
            + " inner join RUT_EVENTO on (RUT_EVENTO.id_evento = evento.id_evento)"
            + " inner join OCURRENCIA_EVENTO_RUT "
            + " on (OCURRENCIA_EVENTO_RUT.ID_RUT_EVENTO = "
            + " RUT_EVENTO.ID_RUT_EVENTO) "
            + " WHERE RUT_EVENTO.OCURRENCIA > EVENTO.OCURRENCIA "
            + "AND RUT_EVENTO.ID_EVENTO = ? ";

    /**
     * String relacionado con obtener la cantidad total de rut segmentado.
     */
    private static final String CANTIDAD_TOTAL_RUT_SEGMENTADO =
        " Select count(EVENTO.ID_EVENTO) as total FROM EVENTO "
            + " inner join RUT_EVENTO "
            + " on (RUT_EVENTO.id_evento = evento.id_evento) "
            + " WHERE EVENTO.SEGMENTADO = 1 AND EVENTO.ID_EVENTO = ? ";

    /**
     * String relacionado con insertar RUT.
     */
    private static final String INSERTAR_RUTS =
        " INSERT INTO RUT_EVENTO(ID_RUT_EVENTO, RUT, "
            + " OCURRENCIA, FECHA_CREACION, FECHA_MODIFICACION,"
            + " ID_PAIS, ID_EVENTO) VALUES(INCREMENTO_RUT_EVENTO.nextval,"
            + " ?, ? ,{fn CURTIME()}, {fn CURTIME()}, ? , ?) ";

    /**
     * String relacionado con Obtener Ocurrencias de un evento.
     */
    private static final String OBTENER_OCURRENCIA_EVENTO =
        "Select OCURRENCIA from EVENTO WHERE ID_EVENTO = ?";

    /**
     * String relacionado con obtener un archivo Imagen.
     */
    private static final String OBTENER_ARCHIVO_IMAGEN =
        " Select IMAGEN_EVENTO.ID_IMAGEN, " + " EVENTO.id_evento, "
            + " IMAGEN_EVENTO.IMAGEN as archivo, "
            + " IMAGEN_EVENTO.FECHA_CREACION, "
            + " IMAGEN_EVENTO.FECHA_MODIFICACION  from EVENTO "
            + " inner join IMAGEN_EVENTO on "
            + " (EVENTO.ID_EVENTO = IMAGEN_EVENTO.ID_EVENTO) "
            + " WHERE EVENTO.ID_EVENTO = ? ";

    /**
     * String relacionado con obtener Archivo rut.
     */
    private static final String OBTENER_ARCHIVO_RUT =
        " Select ARCHIVO_RUT.ID_ARCHIVO, " + " EVENTO.id_evento, "
            + " ARCHIVO_RUT.ARCHIVO_SEGMENTADO as archivo, "
            + " ARCHIVO_RUT.FECHA_CREACION, "
            + " ARCHIVO_RUT.FECHA_MODIFICACION  from "
            + "EVENTO inner join ARCHIVO_RUT on "
            + " (EVENTO.ID_EVENTO = ARCHIVO_RUT.ID_EVENTO) "
            + " WHERE EVENTO.ID_EVENTO  = ? ";

    /**
     * String relacionado con obtener Evento.
     */
    private static final String OBTENER_EVENTO =
        " Select " + " EVENTO.ID_EVENTO, " + " CODIGO_EVENTO, " + " NOMBRE, "
            + " DESCRIPCION, " + " FECHA_INICIO, " + " FECHA_TERMINO, "
            + " OCURRENCIA, " + " URL, " + " ESTADO, " + " PRIORIDAD, "
            + " SEGMENTADO, " + " DESCRIPCION, " + " TIPO_PAGINA, "
            + " POSICION, " + " TIPO_EVENTO "
            + " FROM  EVENTO WHERE EVENTO.ID_EVENTO = ?";

    /**
     * String relacionado con la obtencion de posiciones.
     */
    private static final String OBTENER_POSICIONES =
        "SELECT * FROM POSICIONES_EVENTO";

    /**
     * String relacionado con la obtencion de estado.
     */
    private static final String OBTENER_TIPO_ESTADO =
        "SELECT * FROM TIPO_PAGINA_EVENTO";

    /**
     * String relacionado con la obtencion de prioridad.
     */
    private static final String OBTENER_PRIORIDAD =
        " SELECT * FROM PRIORIDAD_EVENTO ORDER BY "
            + "PRIORIDAD_EVENTO.ID_PRIORIDAD_EVENTO";

    /**
     * String relacionado con la obtencion de tipo de eventos.
     */
    private static final String OBTENER_TIPO_EVENTOS =
        "SELECT ID_TIPO_EVENTO, TIPo_EVENTO FROM TIPO_EVENTO";

    /**
     * String relacionado con la obtencion de evento.
     */
    private static final String OBTENER_TOTAL_EVENTO =
        "Select count(*) as total  FROM EVENTO WHERE EVENTO.CODIGO_EVENTO = ?";

    /**
     * String relacionado con la actualizacion de evento.
     */
    private static final String ACTUALIZAR_EVENTO =
        " UPDATE EVENTO " + " SET  ID_PAIS= ?, NOMBRE= ?, DESCRIPCION= ?,"
            + " FECHA_INICIO= ?, FECHA_TERMINO= ?, "
            + " OCURRENCIA= ?, TIPO_IMAGEN= ?, URL= ?, TIPO_PAGINA= ?, "
            + " ESTADO= ?, POSICION= ?, PRIORIDAD= ?, TIPO_EVENTO= ?, "
            + " SEGMENTADO= ?, RUTS_LISTOS= ?, TOTAL_RUTS= ?, "
            + " FECHA_MODIFICACION={fn CURTIME()}, ES_PLAN_PROMOCION= ? "
            + " WHERE EVENTO.ID_EVENTO = ? ";

    /**
     * String relacionado con buscar RUT.
     */
    private static final String BUSCAR_RUT =
        "Select ID_RUT_EVENTO from RUT_EVENTO WHERE ID_EVENTO = ? AND RUT = ?";

    /**
     * String relacionada con la busqueda de una prioridad
     *  en un tramo  de tiempo.
     */
    private static final String BUSCAR_PRIORIDA_TIEMPO =
        " Select EVENTO.ID_EVENTO as total from EVENTO WHERE "
            + " EVENTO.FECHA_INICIO > = ? AND  EVENTO.FECHA_TERMINO < = ? "
            + "  AND EVENTO.PRIORIDAD = ? ";

    /**
     * String relacionado con obtener la maxima  prioridad en un tramo de fecha.
     */
    private static final String OBTENER_MAX_PRIORIDAD_FECHA =
        " Select MAX(EVENTO.PRIORIDAD)as maximo from EVENTO WHERE "
            + " EVENTO.FECHA_INICIO > = ? AND EVENTO.FECHA_TERMINO < = ? "
            + " AND EVENTO.ESTADO = 1";

    /**
     * String relacionado con Actualizar Archivo.
     */
    private static final String ACTUALIZAR_ARCHIVO =
        " UPDATE EVENTO  SET RUTS_LISTOS= ?, TOTAL_RUTS= "
            + "? WHERE ID_EVENTO = ? ";

    /**
     * String relacionado con la insercion de una nueva prioridad.
     */
    private static final String INSERTAR_NUEVO_PRIORIDAD =
        " INSERT INTO PRIORIDAD_EVENTO(ID_PRIORIDAD_EVENTO, PRIORIDAD) "
            + " VALUES(INCREMENTO_PRIORIDAD.nextval, ? )";

    /**
     * String relacioanadio con obtener el maximo de prioridades.
     */
    private static final String OBTENER_MAX_PRIORIDAD =
        "Select max(PRIORIDAD_EVENTO.ID_PRIORIDAD_EVENTO) as maximo "
            + "FROM PRIORIDAD_EVENTO";

    /**
     * String relacionado con la obtencion del prioridad.
     */
    private static final String OBTENER_PRIORIDAD_EVENTO =
        "Select EVENTO.PRIORIDAD as prioridad FROM EVENTO where EVENTO.ID_EVENTO = ? ";

    /**
     * String relacionado con la obtencon de la fecha de inicio de evento.
     */
    private static final String OBTENER_FECHA_INICIO_EVENTO =
        "select evento.fecha_inicio from evento where evento.id_evento = ?";

    /**
     * String relacionado con la obtencion de la fecha de termino.
     */
    private static final String OBTENER_FECHA_TERMINO_EVENTO =
        "select evento.fecha_termino from evento where evento.id_evento = ?";

    private static final String BORRAR_EVENTO =
        " DELETE FROM EVENTO WHERE EVENTO.ID_EVENTO = ? ";

    private static final String BORRAR_ARCHIVO_RUT =
        "DELETE FROM ARCHIVO_RUT WHERE ID_EVENTO = ?";

    private static final String BORRAR_RUT_EVENTO =
        "DELETE FROM RUT_EVENTO WHERE ID_EVENTO = ?";

    private static final String BORRAR_IMAGEN_EVENTO =
        "DELETE FROM IMAGEN_EVENTO WHERE ID_EVENTO = ?";

    private static final String BUSCAR_EVENTO_MOSTRAR =
        "select e.id_evento from evento e, POSICIONES_EVENTO pe where e.POSICION = pe.ID_POSICION_EVENTO "
        + "AND TO_CHAR(sysdate, 'rrrrmmdd') BETWEEN TO_CHAR(e.FECHA_INICIO, 'rrrrmmdd') AND TO_CHAR(e.FECHA_TERMINO, 'rrrrmmdd') "
//            + "AND {fn CONCAT({fn YEAR(e.FECHA_INICIO)}, {fn CONCAT({fn MONTH(e.FECHA_INICIO)}, {fn DAYOFMONTH(e.FECHA_INICIO)})})} "
//            + "<= {fn CONCAT({fn YEAR({fn CURDATE()})}, {fn CONCAT({fn MONTH({fn CURDATE()})}, {fn DAYOFMONTH({fn CURDATE()})})})} "
//            + "AND {fn CONCAT({fn YEAR(e.FECHA_TERMINO)}, {fn CONCAT({fn MONTH(e.FECHA_TERMINO)}, {fn DAYOFMONTH(e.FECHA_TERMINO)})})} "
//            + ">= {fn CONCAT({fn YEAR({fn CURDATE()})}, {fn CONCAT({fn MONTH({fn CURDATE()})}, {fn DAYOFMONTH({fn CURDATE()})})})} "
            + "AND e.ESTADO = 1 AND e.SEGMENTADO = 0";

    private static final String BUSCAR_EVENTO_USUARIO_MOSTRAR =
        "select e.id_evento from evento e, POSICIONES_EVENTO pe, RUT_EVENTO r "
            + "where e.POSICION = pe.ID_POSICION_EVENTO "
            + "AND e.ID_EVENTO = r.ID_EVENTO "
            + "AND e.OCURRENCIA > (select count(1) FROM OCURRENCIA_EVENTO_RUT oer WHERE oer.ID_RUT_EVENTO = r.ID_RUT_EVENTO) "
            + "AND r.RUT = ? "
            + "AND TO_CHAR(sysdate, 'rrrrmmdd') BETWEEN TO_CHAR(e.FECHA_INICIO, 'rrrrmmdd') AND TO_CHAR(e.FECHA_TERMINO, 'rrrrmmdd') "
//            + "AND {fn CONCAT({fn YEAR(e.FECHA_INICIO)}, {fn CONCAT({fn MONTH(e.FECHA_INICIO)}, {fn DAYOFMONTH(e.FECHA_INICIO)})})} "
//            + "<= {fn CONCAT({fn YEAR({fn CURDATE()})}, {fn CONCAT({fn MONTH({fn CURDATE()})}, {fn DAYOFMONTH({fn CURDATE()})})})} "
//            + "AND {fn CONCAT({fn YEAR(e.FECHA_TERMINO)}, {fn CONCAT({fn MONTH(e.FECHA_TERMINO)}, {fn DAYOFMONTH(e.FECHA_TERMINO)})})} "
//            + ">= {fn CONCAT({fn YEAR({fn CURDATE()})}, {fn CONCAT({fn MONTH({fn CURDATE()})}, {fn DAYOFMONTH({fn CURDATE()})})})} "
            + "AND e.ESTADO = 1 AND e.SEGMENTADO = 1";

    private static final String VER_EVENTO =
         "INSERT INTO OCURRENCIA_EVENTO_RUT VALUES (?, ?, {fn CURTIME()}, 1) ";
 
    private static final String VALIDAR_RUT_EVENTO = 
        "SELECT ID_RUT_EVENTO FROM RUT_EVENTO WHERE RUT=? AND ID_EVENTO=?";

    private static final String OBTENER_TOTAL_OCURRENCIAS =
        "SELECT NVL(MAX(ID_OCURRENCIA),0) AS total FROM OCURRENCIA_EVENTO_RUT";

    /**
     * Metodo relacionado con el buscar Evento.
     * @param filtros
     * @return
     */
    public List < Map < String, Object > > buscarEventos(Map filtros) {

        ArrayList < Map < String, Object >> result = null;
        String codigo = (String) filtros.get("codigoFiltro");
        Date fechaInicio = (Date) filtros.get("fechaInicio");
        Date fechaTermino = (Date) filtros.get("fechaTermino");
        Integer posicion = (Integer) filtros.get("posicion");
        Integer estado = (Integer) filtros.get("estado");
        Integer tipoPagina = (Integer) filtros.get("paginaEntero");

        List < Map < String, ? > > ocurrenciasList =
            this
                .query(
                    Map.class,
                    "select count(1) as cantidad, ID_EVENTO as idEvento from (select count(1), re.RUT, re.ID_EVENTO from OCURRENCIA_EVENTO_RUT oc, RUT_EVENTO re where oc.ID_RUT_EVENTO = re.ID_RUT_EVENTO group by re.RUT, RE.ID_EVENTO having count(1) >= (select ocurrencia from evento where id_evento = RE.ID_EVENTO)) group by ID_EVENTO ",
                    new Object[] {});
        Map < String, Long > ocurrencia = new HashMap < String, Long >();
        for (Iterator iterator = ocurrenciasList.iterator(); iterator.hasNext();) {
            Map < String, ? > datos = (Map < String, ? >) iterator.next();
            String llave = String.valueOf((Number) datos.get("idEvento"));
            Number valor = (Number) datos.get("cantidad");
            ocurrencia.put(llave, valor.longValue());
        }

        if (codigo == null) {

            if (tipoPagina != null) {

                if (fechaInicio != null && fechaTermino != null) {
                    Object[] param =
                    { fechaInicio, fechaTermino, estado,estado, tipoPagina };
                    result =
                        (ArrayList < Map < String, Object >>) this.query(
                            Map.class, BUSCAR_EVENTO_FECHA_TIPO_EVENTO, param);
                    formatearRespuesta(result, ocurrencia);
                } else {

                    if (fechaInicio == null && fechaTermino == null) {
                        Object[] param = { estado,estado, tipoPagina };
                        result =
                            (ArrayList < Map < String, Object >>) this.query(
                                Map.class,
                                BUSCAR_EVENTO_FECHA_TIPO_EVENTO_SIN_FECHAS,
                                param);
                        formatearRespuesta(result, ocurrencia);
                    }

                    if (fechaInicio == null && fechaTermino != null) {
                        Object[] param = { fechaTermino, estado,estado, tipoPagina };
                        result =
                            (ArrayList < Map < String, Object >>) this.query(
                                Map.class, BUSCAR_EVENTO_FECHA_TERMINO_EVENTO,
                                param);
                        formatearRespuesta(result, ocurrencia);
                    } else {

                        if (fechaInicio != null && fechaTermino == null) {
                            Object[] param =
                            { fechaInicio, estado,estado, tipoPagina };
                            result =
                                (ArrayList < Map < String, Object >>) this
                                    .query(Map.class,
                                        BUSCAR_EVENTO_FECHA_INICIO_EVENTO,
                                        param);
                            formatearRespuesta(result, ocurrencia);
                        }
                    }
                }
            } else {

                if (fechaInicio != null && fechaTermino != null) {
                    Object[] param = { fechaInicio, estado,estado, tipoPagina };
                    result =
                        (ArrayList < Map < String, Object >>) this
                            .query(Map.class,
                                BUSCAR_EVENTO_FECHA_INICIO_EVENTO, param);

                    formatearRespuesta(result, ocurrencia);
                }
            }

            if (codigo == null && fechaInicio == null && fechaTermino == null
                && posicion == null && tipoPagina == null) {
                Object[] param = { estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo != null && fechaInicio == null && fechaTermino == null
                && posicion == null && tipoPagina == null) {
                Object[] param = { codigo, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_CODIGO, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio != null && fechaTermino == null
                && posicion == null && tipoPagina == null) {
                Object[] param = { fechaInicio, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_INICIAL, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio == null && fechaTermino != null
                && posicion == null && tipoPagina == null) {
                Object[] param = { fechaTermino, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_TERMINAL, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio != null && fechaTermino != null
                && posicion == null && tipoPagina == null) {
                Object[] param = { fechaInicio, fechaTermino, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_FECHA, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio != null && fechaTermino == null
                && posicion != null && tipoPagina == null) {
                Object[] param = { posicion, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_POSICION, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio != null && fechaTermino == null
                && posicion == null && tipoPagina == null) {
                Object[] param = { tipoPagina, estado,estado };
                result =
                    (ArrayList < Map < String, Object > >) this.query(
                        Map.class, BUSCAR_EVENTO_TIPO_PAGINA, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio != null && fechaTermino != null
                && posicion == null && tipoPagina == null) {

                Object[] param = { fechaInicio, fechaTermino, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_TRAMO_FECHA, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio != null && fechaTermino != null
                && posicion != null && tipoPagina == null) {

                Object[] param =
                { fechaInicio, fechaTermino, posicion, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_POSICION_TRAMO_FECHA, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio == null && fechaTermino == null
                && posicion != null && tipoPagina == null) {

                Object[] param = { posicion, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_POSICION_COMBO, param);
                formatearRespuesta(result, ocurrencia);
            }

            if (codigo == null && fechaInicio != null && fechaTermino != null
                && posicion != null && tipoPagina != null) {

                Object[] param =
                { fechaInicio, fechaTermino, posicion, tipoPagina, estado,estado };
                result =
                    (ArrayList < Map < String, Object >>) this.query(Map.class,
                        BUSCAR_EVENTO_FILTRO, param);
                formatearRespuesta(result, ocurrencia);
            }

        } else {
            // Cuando es solo una busqueda por codigo de evento
            Object[] param = { codigo, estado,estado };
            result =
                (ArrayList < Map < String, Object >>) this.query(Map.class,
                    BUSCAR_EVENTO_CODIGO, param);

            formatearRespuesta(result, ocurrencia);
        }

        return result;
    }

    /**
     * FormatearRespuesta.
     * @param result
     * @param ocurrencia
     */
    private void formatearRespuesta(List < Map < String, Object >> result,
        Map < String, Long > ocurrencia) {
        for (Iterator iterator = result.iterator(); iterator.hasNext();) {
            Map < String, Object > map =
                (Map < String, Object >) iterator.next();
            String idEvento = String.valueOf((Number) map.get("ID_EVENTO"));
            long cantidad = 0;
            if (ocurrencia.containsKey(idEvento)) {
                cantidad = ocurrencia.get(idEvento);
            }

            map.put("ruts_listos", cantidad);
        }
    }

    /**
     * Metodo relacionado con subir Archivos.
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     */
    public boolean subirArchivo(Long id_evento, byte[] archivo, String tipo) {

        ByteArrayInputStream bios = new ByteArrayInputStream(archivo);

        Object[] param = { id_evento, bios };
        boolean resultado = false;

        if (tipo.equals("archivo")) {
            int cant = (int) this.insert(INSERTAR_FOTOGRAFIA, param);
            if (cant > 0) {
                resultado = true;
            }

        } else if (tipo.equals("cargaRut")) {
            int cant = (int) this.update(INSERTAR_ARCHIVOS_RUT, param);
            if (cant > 0) {
                resultado = true;
            }
        }

        return resultado;
    }

    /**
     * Metodo relacionado con agregar nuevo Evento.
     * @param datos
     * @return
     */
    public Long nuevoEvento(Map datos) {

        Integer valor = 0;

        if (datos.get("estado_habilitado") == null) {
            valor = 0;
        } else {

            if (datos.get("estado_habilitado").equals("on")) {
                valor = 1;
            }
        }

        Object[] parametros =
            { 1, (String) datos.get("codigo_evento"), 1,
                (String) datos.get("nombre"),
                (String) datos.get("descripcion"),
                (Date) datos.get("fecha_inicio"),
                (Date) datos.get("fecha_termino"),
                Integer.parseInt((String) datos.get("ocurrencia")),
                datos.get("tipo_imagen"), (String) datos.get("url"),
                Integer.parseInt((String) datos.get("tipo_pagina")), valor,
                Integer.parseInt((String) datos.get("posicion")),
                Integer.parseInt((String) datos.get("prioridad")),
                Integer.parseInt((String) datos.get("tipo_evento")),
                Integer.parseInt((String) datos.get("radio_segmentado")), 1 };
        Long id = null;
        id = this.insert(INSERTAR_EVENTO, parametros);

        return id;
    }

    /**
     * metodo relacion con actualizar un evento en particular.
     * @param datos
     * @return
     */
    public boolean actualizarEvento(Map datos) {

        boolean resultado = false;

        Integer idEvento =
            Integer.parseInt(((String) datos.get("id_evento")).trim());

        Object[] param = { idEvento };
        Map resul =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_LISTOS, param);
        Number total = (Number) resul.get("total");
        Integer cantRuTlistos = total.intValue();

        Map resulTotalRut =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_SEGMENTADO, param);
        Number totalRut = (Number) resulTotalRut.get("total");
        Integer CantRut = totalRut.intValue();

        Integer valor = 0;

        if (datos.get("estado_habilitado") == null) {
            valor = 0;
        } else {

            if (datos.get("estado_habilitado").equals("on")) {
                valor = 1;
            }
        }

        String cadenaOcurrencia = ((String) datos.get("ocurrencia")).trim();

        Object[] parametros =
            { 1, (String) datos.get("nombre"),
                (String) datos.get("descripcion"),
                (Date) datos.get("fecha_inicio"),
                (Date) datos.get("fecha_termino"),
                Integer.parseInt(((String) datos.get("ocurrencia")).trim()), 1,
                (String) datos.get("url"),
                Integer.parseInt((String) datos.get("tipo_pagina")), valor,
                Integer.parseInt((String) datos.get("posicion")),
                Integer.parseInt((String) datos.get("prioridad")),
                Integer.parseInt(((String) datos.get("tipo_evento")).trim()),
                Integer.parseInt((String) datos.get("radio_segmentado")),
                cantRuTlistos, CantRut, 1,
                Integer.parseInt(((String) datos.get("id_evento")).trim()) };

        Integer fila = null;
        fila = this.update(ACTUALIZAR_EVENTO, parametros);
        if (fila > 0) {
            resultado = true;
        }
        return resultado;
    }

    /**
     * Metodo que tiene relacion con Insertar Rut desde el archivo.
     *
     * @param datos
     * @return
     */
    public boolean insertarRutArchivos(Map < String, Object > datos) {

        boolean resultado = false;
        Integer ocurrencia;
        Long IdEvento = (Long) datos.get("ID_evento");

        String[] arregloRut = (String[]) datos.get("ArregloRut");
        Object[] parametros = { IdEvento };

        Map resultadoMap =
            (Map) this.find(Map.class, OBTENER_OCURRENCIA_EVENTO, parametros);
        ocurrencia = ((Number) resultadoMap.get("OCURRENCIA")).intValue();

        if (ocurrencia != null) {
            Integer id_pais = 1;

            for (int i = 0; i < arregloRut.length; i++) {
                String ruttmp = arregloRut[i].trim();
                String srut = ruttmp.substring(0, ruttmp.length() - 2);
                Object[] param =
                    { Integer.parseInt(srut), ocurrencia, id_pais, IdEvento };
                this.update(INSERTAR_RUTS, param);
                resultado = true;
            }
        }

        Object[] paraEvento = { IdEvento };
        Map resul =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_LISTOS, paraEvento);
        Number total = (Number) resul.get("total");
        Integer cantRuTlistos = total.intValue();

        Map resulTotalRut =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_SEGMENTADO,
                paraEvento);
        Number totalRut = (Number) resulTotalRut.get("total");
        Integer cantRut = totalRut.intValue();
        Object[] param = { cantRuTlistos, cantRut, IdEvento };

        Integer filasResultado = this.update(ACTUALIZAR_ARCHIVO, param);

        if (filasResultado > 0) {
            resultado = true;
        }

        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de un archivo, con el id especifico y su tipo.
     * @param id_evento
     * @param tipo
     * @return
     */
    public Map < String, Object > obtenerArchivo(String id_evento, String tipo) {
        Map < String, Object > res = null;

        if (tipo.equals("imagen")) {
            Object[] parametros = { id_evento };
            res =
                (Map) this.find(Map.class, OBTENER_ARCHIVO_IMAGEN, parametros);
        } else {
            Object[] parametros = { id_evento };
            res = (Map) this.find(Map.class, OBTENER_ARCHIVO_RUT, parametros);
        }

        if (res != null && res.size() > 0) {
            try {
                Blob imagen = (Blob) res.get("archivo");
                InputStream is = imagen.getBinaryStream();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int cnt = is.read(buffer);
                while (cnt != -1) {
                    baos.write(buffer);
                    cnt = is.read(buffer);
                }
                is.close();

                res.put("archivo", baos.toByteArray());

            } catch (SQLException e) {
                return null;
            } catch (IOException e) {
                return null;
            }

        }
        return res;
    }

    /**
     * Metodo relacionado con la obtencion de eventos con un ID especifico.
     * @param idEvento
     * @return
     */
    public Map < String, Object > obtenerEvento(Long idEvento) {
        Map < String, Object > resultado = null;
        Object[] param = { idEvento };
        resultado = (Map) this.find(Map.class, OBTENER_EVENTO, param);
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de posiciones.
     * @return
     */
    public List < Map < String, ? > > obtenerPosiciones() {
        List < Map < String, ? > > resultado = null;
        Object[] param = {};
        resultado =
            (List < Map < String, ? > >) this.query(Map.class,
                OBTENER_POSICIONES, param);
        return resultado;
    }

    /**
     * Metodo relacion con la obtencion de tipos de paginas.
     * @return
     */
    public List < Map < String, ? > > obtenerTipoPagina() {
        List < Map < String, ? > > resultado = null;
        Object[] param = {};
        resultado =
            (List < Map < String, ? >>) this.query(Map.class,
                OBTENER_TIPO_ESTADO, param);
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de prioridad
     * @return
     */
    public List < Map < String, ? > > obtenerPrioridad() {
        List < Map < String, ? > > resultado = null;
        Object[] param = {};
        resultado =
            (List < Map < String, ? >>) this.query(Map.class,
                OBTENER_PRIORIDAD, param);
        return resultado;
    }

    /**
     * Metodo relacionado con obtener los tipos de eventos
     * @return
     */
    public List < Map < String, ? >> obtenerTipoEvento() {
        List < Map < String, ? > > resultado = null;
        Object[] param = {};
        resultado =
            (List < Map < String, ? >>) this.query(Map.class,
                OBTENER_TIPO_EVENTOS, param);
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de la cantidad de eventos
     * con un ID especifico.
     * @param codigo
     * @return
     */
    public Integer obtenerCantidadEvento(String codigo) {
        Object[] param = { codigo };
        Map resultado = (Map) this.find(Map.class, OBTENER_TOTAL_EVENTO, param);
        Number total = (Number) resultado.get("total");

        return total.intValue();
    }

    /**
     * Metodo relacionado con la modificacio del archivo
     * (fisico) en la base de datos.
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     */
    public boolean modificarArchivo(Long id_evento, byte[] bytes, String tipo) {

        ByteArrayInputStream bios = new ByteArrayInputStream(bytes);

        boolean resultado = false;

        if (tipo.equals("archivo")) {

            Object[] parametros = { id_evento };
            Map resultadoIdFoto =
                (Map) this.find(Map.class, OBTENER_ID_FOTOGRAFIA, parametros);
            if (resultadoIdFoto != null) {
                Number idNumero = (Number) resultadoIdFoto.get("id_imagen");
                Integer idFotografia = idNumero.intValue();

                Object[] parametrosInsert = { bios, idFotografia };
                int cant =
                    (int) this.update(MODIFICAR_FOTOGRAFIA, parametrosInsert);

                if (cant > 0) {
                    resultado = true;
                }
            } else {

                Object[] paramInsert = { id_evento, bios };
                if (tipo.equals("archivo")) {
                    int cant =
                        (int) this.insert(INSERTAR_FOTOGRAFIA, paramInsert);
                    if (cant > 0) {
                        resultado = true;
                    }
                }

            }
        } else {
            if (tipo.equals("cargaRut")) {

                Object[] paramRut = { id_evento };
                Map resultadoId =
                    (Map) this
                        .find(Map.class, OBTENER_ID_ARCHIVO_RUT, paramRut);

                if (resultadoId != null) {
                    Number idNumero = (Number) resultadoId.get("id_archivo");
                    Integer idRut = idNumero.intValue();

                    Object[] parametrosUpdate = { bios, idRut };
                    int cant =
                        (int) this.update(MODIFICAR_ARCHIVO_RUT,
                            parametrosUpdate);

                    if (cant > 0) {
                        resultado = true;
                    }
                } else {
                    Object[] paramInsert = { id_evento, bios };
                    if (tipo.equals("cargaRut")) {
                        int cant =
                            (int) this.insert(INSERTAR_ARCHIVOS_RUT,
                                paramInsert);
                        if (cant > 0) {
                            resultado = true;
                        }
                    }

                }

            }
        }
        return resultado;
    }

    /**
     * metodo relaciondo con la modificacion de los rut segmentados.
     * @param datos
     * @return
     */
    public boolean modificarArchivoRut(Map < String, Object > datos) {

        boolean devolver = false;
        long id_evento = (Long) datos.get("ID_evento");
        String[] rut = (String[]) datos.get("ArregloRut");

        Object[] paramOcurrencia = { id_evento };
        Integer id_pais = 1;
        Map resultadoMap =
            (Map) this.find(Map.class, OBTENER_OCURRENCIA_EVENTO,
                paramOcurrencia);
        Integer ocurrencia =
            ((Number) resultadoMap.get("OCURRENCIA")).intValue();

        for (int i = 0; i < rut.length; i++) {
            String ruttmp = rut[i].trim();
            String srut = ruttmp.substring(0, ruttmp.length() - 2);
            Object[] param = { id_evento, Integer.parseInt(srut) };
            Map resultado = (Map) this.find(Map.class, BUSCAR_RUT, param);
            if (resultado == null) {
                Object[] paramInsertRut =
                    { Integer.parseInt(srut), ocurrencia, id_pais, id_evento };
                this.update(INSERTAR_RUTS, paramInsertRut);
                devolver = true;
            }
        }

        Object[] paraEvento = { id_evento };
        Map resul =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_LISTOS, paraEvento);
        Number total = (Number) resul.get("total");
        Integer cantRuTlistos = total.intValue();

        Map resulTotalRut =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_SEGMENTADO,
                paraEvento);
        Number totalRut = (Number) resulTotalRut.get("total");
        Integer CantRut = totalRut.intValue();
        Object[] param = { cantRuTlistos, CantRut, id_evento };

        Integer filasResultado = this.update(ACTUALIZAR_ARCHIVO, param);

        if (filasResultado > 0) {
            devolver = true;
        }
        return devolver;
    }

    /**
     * Metodo relacionado con la comparacion de prioridad en un tramo de tiempo
     * Devolvera true, si encuentra una posicion igual, false si no.
     * @param prioridad
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     */
    public boolean conflictoPrioridad(Integer prioridad, Date fechaInicioDate,
        Date fechaTerminoDate) {

        boolean retorno = false;
        Object[] parametros = { fechaInicioDate, fechaTerminoDate, prioridad };

        List < Map > resultado =
            this.query(Map.class, BUSCAR_PRIORIDA_TIEMPO, parametros);

        if (resultado != null && resultado.size() > 0) {
            retorno = true;
        }

        return retorno;
    }

    /**
     * Metodo relacionado con obtener la maxima prioridad entre un tramo de fechas.
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     */
    public Integer obtenerMaxPriori(Date fechaInicioDate, Date fechaTerminoDate) {

        Integer retorno = null;
        Object[] parametros = { fechaInicioDate, fechaTerminoDate };
        Map resultado =
            (Map) this.find(Map.class, OBTENER_MAX_PRIORIDAD_FECHA, parametros);

        if (resultado.get("maximo") != null) {
            retorno = ((Number) resultado.get("maximo")).intValue();
        } else {
            retorno = 0;
        }

        return retorno;
    }

    /**
     * Metodo relacionado con la insercion de la nueva Prioridad.
     * @param sugerencia
     */
    public void insertarNuevaPrioridad(Integer sugerencia) {
        String suge = String.valueOf(sugerencia);
        Object[] param = { suge };
        this.insert(INSERTAR_NUEVO_PRIORIDAD, param);
    }

    /**
     * Metodo relacionado para obtener la maxima prioridad.
     * @return
     */
    public Integer obtenerMaxPriori() {
        Object[] param = {};
        Map resultado =
            (Map) this.find(Map.class, OBTENER_MAX_PRIORIDAD, param);
        Number maximo = (Number) resultado.get("maximo");
        return maximo.intValue();
    }

    /**
     * Metodo relacionado para obtener prioridad de evento.
     * @param idEvento
     * @return
     */
    public Integer obtenerPrioridadEvento(Long idEvento) {
        Object[] param = { idEvento };
        Map resultado =
            (Map) this.find(Map.class, OBTENER_PRIORIDAD_EVENTO, param);
        Number maximo = (Number) resultado.get("prioridad");
        return maximo.intValue();
    }

    /**
     * Metodo relacionado con la obtencion de fecha de inicio.
     * @param idEvento
     * @return
     */
    public Date obtenerFechaInicio(Long idEvento) {
        Object[] param = { idEvento };
        Map resultado =
            (Map) this.find(Map.class, OBTENER_FECHA_INICIO_EVENTO, param);
        return (Date) resultado.get("fecha_inicio");
    }

    /**
     * Metodo relacionado con la obtencion de fecha de termino.
     * @param idEvento
     * @return
     */
    public Date obtenerFechaTermino(Long idEvento) {
        Object[] param = { idEvento };
        Map resultado =
            (Map) this.find(Map.class, OBTENER_FECHA_TERMINO_EVENTO, param);
        return (Date) resultado.get("fecha_termino");
    }

    /**
     * metodo relacion con borrar un evento en particular.
     * @param datos
     * @return
     */
    public boolean borrarEvento(Long idEvento) {

        boolean resultado = false;

        Object[] param = { idEvento };

        //Borrar evento RUT_EVENTO
        this.update(BORRAR_RUT_EVENTO, param);

        //Borrar Archivo rut
        this.update(BORRAR_ARCHIVO_RUT, param);

        //Borrar imagen evento
        this.update(BORRAR_IMAGEN_EVENTO, param);

        //Borrar evento
        Integer fila = null;
        fila = this.update(BORRAR_EVENTO, param);
        if (fila > 0) {
            resultado = true;
        }
        return resultado;
    }

    public Integer obtenerOcurrencia(Long idEvento) {
        Object[] paramOcurrencia = { idEvento };
        Integer id_pais = 1;
        Map resultadoMap =
            (Map) this.find(Map.class, OBTENER_OCURRENCIA_EVENTO,
                paramOcurrencia);
        Integer ocurrencia =
            ((Number) resultadoMap.get("OCURRENCIA")).intValue();

        return ocurrencia;
    }

    public Map buscarRut(Long idEvento, Long rut) {
        Object[] param = { idEvento, rut };
        return (Map) this.find(Map.class, BUSCAR_RUT, param);
    }

    /**
     * metodo relaciondo con la modificacion de los rut segmentados.
     * @param datos
     * @return
     */
    public boolean grabarRut(Long rut, Long idEvento, Integer ocurrencia) {

        int idPais = 1;
        boolean devolver = false;

        Object[] paramInsertRut = { rut, ocurrencia, idPais, idEvento };
        this.update(INSERTAR_RUTS, paramInsertRut);
        devolver = true;

        Object[] paraEvento = { idEvento };
        Map resul =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_LISTOS, paraEvento);
        Number total = (Number) resul.get("total");
        Integer cantRuTlistos = total.intValue();

        Map resulTotalRut =
            (Map) this.find(Map.class, CANTIDAD_TOTAL_RUT_SEGMENTADO,
                paraEvento);
        Number totalRut = (Number) resulTotalRut.get("total");
        Integer CantRut = totalRut.intValue();
        Object[] param = { cantRuTlistos, CantRut, idEvento };

        Integer filasResultado = this.update(ACTUALIZAR_ARCHIVO, param);

        if (filasResultado > 0) {
            devolver = true;
        }
        return devolver;
    }

    /**
     * TODO Describir m�todo buscarEventoMostrar.
     * @param filtros
     * @return
     */
    public Long buscarEventoMostrar(Map < String, Object > filtros) {
        long res = -1;

        List < Object > params = new ArrayList < Object >();

        String sql = "";

        //Filtro de usuario
        String usuario = (String) filtros.get("usuario");
        if (usuario == null) {
            //Obtener eventos genericos.
            sql = BUSCAR_EVENTO_MOSTRAR;
        } else {
            //Obtener eventos para el usuario.
            sql = BUSCAR_EVENTO_USUARIO_MOSTRAR;
            params.add(Long.parseLong(usuario));
        }

        //Filtro de posicion
        String posicion = (String) filtros.get("posicion");
        if (posicion != null) {
            sql += " AND pe.POSICION_EVENTO = ? ";
            params.add(posicion);
        }

        String rama = (String) filtros.get("rama");
        if (rama != null) {
            sql += " AND e.ID_RAMA = ?";
            params.add(Long.parseLong(rama));
        }

        sql += " ORDER BY PRIORIDAD desc";

        Map < String, Object > evento =
            (Map) this.find(Map.class, sql, params.toArray());
        if (evento != null && evento.get("id_evento") != null) {
            Number evt = (Number) evento.get("id_evento");
            res = evt.longValue();
        }
        return res;
    }

    /**
     * TODO Describir m�todo verEvento.
     * @param id
     * @param rut
     */
    public void verEvento(Long id, Long rut) {
        //Obtener ocurrencias para el rut.
        Map ocurrencias =
            (Map) this.find(Map.class, OBTENER_TOTAL_OCURRENCIAS, null);
        Number total = (Number) ocurrencias.get("total");

        Object[] params = new Object[] {rut, id};
        
        Map rut_evento = (HashMap)this.find(Map.class, VALIDAR_RUT_EVENTO, params);
        
        if(rut_evento!=null && rut_evento.get("id_rut_evento")!=null) {
            params = new Object[] { total.intValue() + 1, rut_evento.get("id_rut_evento") };
        }
        this.insertWithoutGeneratedKey(VER_EVENTO, params);
    }
}
