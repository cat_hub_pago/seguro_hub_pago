package cl.cencosud.ventaseguros.mantenedoretl.dao.jbdc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.cencosud.ventaseguros.mantenedoretl.dao.MantenedorETLDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

/**
 * Encargado de manejar las consultas del mantenedor Usuarios, agregar usuario.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class MantenedorETLDAOJDBC extends ManagedBaseDAOJDBC implements
    MantenedorETLDAO {

    private static final Log logger =
        LogFactory.getLog(MantenedorETLDAOJDBC.class);

    private static final String SQL_ENVIAR_PLAN =
        "insert into PLAN_LEG (ID_PLAN, CODIGO_TIPO_PROD_LEG, CODIGO_SUB_TIPO_PROD_LEG, "
            + "CODIGO_PRODUCTO_LEG, CODIGO_PLAN_LEG, NOMBRE, ID_PAIS, ES_ACTIVO, COMPANNIA, "
            + "FECHA_CREACION, FECHA_MODIFICACION, MAX_DIAS_INI_VIG, INSPECCIONBSP, "
            + "SOLICITA_NUEVO_USADO, SOLICITA_FACTURA, FORMA_PAGO) values "
            + "(PLAN_LEG_SEQ.nextval, ?, ?, ?, ?, ?, 1, 0, 'CIA', sysdate, "
            + "sysdate, null, null, null, null, '1111')";

    /**
     * TODO Describir m�todo enviarPlan.
     * @param plan
     */
    public long enviarPlan(PlanLeg plan) throws Exception {
        long res = -1;
        try {
            Object[] params =
                new Object[] { plan.getCodigo_tipo_prod_leg(),
                    plan.getCodigo_sub_tipo_prod_leg(),
                    plan.getCodigo_producto_leg(), plan.getCodigo_plan_leg(),
                    plan.getNombre() };

            res = this.insert(SQL_ENVIAR_PLAN, params);
        } catch (Exception e) {
            logger.error("Error al enviar plan", e);
            throw e;
        }
        return res;
    }

    private static final String SQL_OBTENER_DATOS_PLAN =
        "select ID_PLAN, CODIGO_TIPO_PROD_LEG, CODIGO_SUB_TIPO_PROD_LEG, "
            + "CODIGO_PRODUCTO_LEG, CODIGO_PLAN_LEG, NOMBRE, ID_PAIS, ES_ACTIVO, COMPANNIA, "
            + "FECHA_CREACION, FECHA_MODIFICACION, MAX_DIAS_INI_VIG, INSPECCIONBSP, "
            + "SOLICITA_NUEVO_USADO, SOLICITA_FACTURA, FORMA_PAGO FROM PLAN_LEG WHERE CODIGO_PLAN_LEG = ?";

    /**
     * TODO Describir m�todo obtenerDatosPlan.
     * @param idPlanLeg
     * @return
     */
    public PlanLeg obtenerDatosPlan(long idPlanLeg) {
        PlanLeg plan = null;
        Object[] params = new Object[] { idPlanLeg };

        plan =
            (PlanLeg) this.find(PlanLeg.class, SQL_OBTENER_DATOS_PLAN, params);
        return plan;
    }

    private static final String SQL_ACTUALIZAR_DATOS_PLAN =
        "update PLAN_LEG set CODIGO_TIPO_PROD_LEG = ?, CODIGO_SUB_TIPO_PROD_LEG = ?, "
            + "CODIGO_PRODUCTO_LEG = ?, CODIGO_PLAN_LEG = ?, NOMBRE = ?, ES_ACTIVO = 0, "
            + "FECHA_MODIFICACION = sysdate WHERE ID_PLAN = ?";

    /**
     * TODO Describir m�todo actualizarPlan.
     * @param plan
     * @throws Exception
     */
    public void actualizarDatosPlan(PlanLeg plan) throws Exception {
        try {
            Object[] params =
                new Object[] { plan.getCodigo_tipo_prod_leg(),
                    plan.getCodigo_sub_tipo_prod_leg(),
                    plan.getCodigo_producto_leg(), plan.getCodigo_plan_leg(),
                    plan.getNombre(), plan.getId_plan() };

            this.update(SQL_ACTUALIZAR_DATOS_PLAN, params);
        } catch (Exception e) {
            logger.error("Error al actualizar plan", e);
            throw e;
        }
    }

    private static final String SQL_OBTENER_DATOS_PRODUCTO =
        "select ID_PRODUCTO as ID_PRODUCTO_LEG, CODIGO_TIPO_PROD_LEG, CODIGO_SUB_TIPO_PROD_LEG, "
            + "CODIGO_PRODUCTO_LEG, NOMBRE, ID_PAIS, ES_ACTIVO, "
            + "FECHA_CREACION, FECHA_MODIFICACION FROM PRODUCTO_LEG WHERE CODIGO_PRODUCTO_LEG = ?";

    /**
     * TODO Describir m�todo obtenerDatosPlan.
     * @param idPlanLeg
     * @return
     */
    public ProductoLeg obtenerDatosProducto(long idProductoLeg) {
        ProductoLeg producto = null;
        Object[] params = new Object[] { idProductoLeg };

        producto =
            (ProductoLeg) this.find(ProductoLeg.class,
                SQL_OBTENER_DATOS_PRODUCTO, params);
        return producto;
    }

    private static final String SQL_ENVIAR_PRODUCTO =
        "insert into PRODUCTO_LEG (ID_PRODUCTO, CODIGO_TIPO_PROD_LEG, CODIGO_SUB_TIPO_PROD_LEG, "
            + "CODIGO_PRODUCTO_LEG, NOMBRE, ID_PAIS, ES_ACTIVO, "
            + "FECHA_CREACION, FECHA_MODIFICACION) values "
            + "(PRODUCTO_LEG_SEQ.nextval, ?, ?, ?, ?, 1, 0, sysdate, "
            + "sysdate)";

    /**
     * TODO Describir m�todo enviarProducto.
     * @param producto
     * @return
     */
    public long enviarProducto(ProductoLeg producto) throws Exception {
        long res = -1;
        try {
            Object[] params =
                new Object[] { producto.getCodigo_tipo_prod_leg(),
                    producto.getCodigo_sub_tipo_prod_leg(),
                    producto.getCodigo_producto_leg(), producto.getNombre() };

            res = this.insert(SQL_ENVIAR_PRODUCTO, params);
        } catch (Exception e) {
            logger.error("Error al enviar plan", e);
            throw e;
        }
        return res;
    }

    private static final String SQL_ACTUALIZAR_DATOS_PRODUCTO =
        "update PRODUCTO_LEG set CODIGO_TIPO_PROD_LEG = ?, CODIGO_SUB_TIPO_PROD_LEG = ?, "
            + "CODIGO_PRODUCTO_LEG = ?, NOMBRE = ?, ES_ACTIVO = 0, "
            + "FECHA_MODIFICACION = sysdate WHERE ID_PRODUCTO = ?";

    /**
     * TODO Describir m�todo actualizarDatosProducto.
     * @param producto
     * @throws Exception
     */
    public void actualizarDatosProducto(ProductoLeg producto) throws Exception {
        try {
            Object[] params =
                new Object[] { producto.getCodigo_tipo_prod_leg(),
                    producto.getCodigo_sub_tipo_prod_leg(),
                    producto.getCodigo_producto_leg(), producto.getNombre(),
                    producto.getId_producto_leg() };

            this.update(SQL_ACTUALIZAR_DATOS_PRODUCTO, params);
        } catch (Exception e) {
            logger.error("Error al actualizar producto", e);
            throw e;
        }
    }
}
