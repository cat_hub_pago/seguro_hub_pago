package cl.cencosud.ventaseguros.asesorvirtual.dao;

import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * DAO de Asesor Virtual.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public interface AsesorVirtualDAO extends BaseDAO {

    void grabarArbol(int idArbol, String username);

    /**
     * Se encarga de grabar las secciones del arbol.
     * @param seccion objeto a grabar.
     */
    void grabarSeccion(Seccion seccion, int idArbol, String username);

    void grabarPregunta(Pregunta pregunta, int idSeccion, String username);

    void grabarAlternativa(Alternativa alternativa, int idPregunta,
        int idSeccion, String username);

    void grabarResultado(Resultado resultado, int idSeccion, String username);

    /**
     * Busca la informacion del arbol.
     * @param idArbol Identificador del arbol guardado.
     * @return Listado de datos.
     */
    List < Map < String, Object > > buscarArbol(int idArbol);

    /**
     * Busca las secciones que pertenecen a un arbol.
     * @param idArbol Identificador del arbol guardado.
     * @return Listado de secciones.
     */
    List < Map < String, Object > > buscarSecciones(int idArbol);

    /**
     * Busca las preguntas asociadas a una seccion.
     * @param idSeccion Identificador de la seccion.
     * @return Listado de Preguntas.
     */
    List < Map < String, Object > > buscarPreguntas(int idSeccion);

    /**
     * Busca las alternativas de una pregunta.
     * @param idSeccion identificador de seccion.
     * @param idPregunta identificador de pregunta.
     * @return Lsitado de Alternativas.
     */
    List < Map < String, Object > > buscarAlternativas(int idSeccion,
        int idPregunta);

    /**
     * Buscar resultados de una seccion.
     * @param idSeccion identificador de seccion.
     * @return Listado de respuestas.
     */
    List < Map < String, Object > > buscarResultados(int idSeccion);

    void actualizarAlternativa(int idAlternativa, int idSeccion,
        int idPregunta, int idRespuesta, String username);

    void actualizarProximaPregunta(int idAlternativa, int idSeccion,
        int idPregunta, int idProxPregunta, String username);

    void borrarAlternativas();

    void borrarResultados();

    void borrarPreguntas();

    void borrarPreocupaciones();

    void borrarArbol();

    boolean sincronizarArchivo(String nodo, String rutaLocal);
}
