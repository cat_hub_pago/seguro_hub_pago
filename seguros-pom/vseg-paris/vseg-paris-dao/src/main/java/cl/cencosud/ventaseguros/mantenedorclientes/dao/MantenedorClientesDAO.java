package cl.cencosud.ventaseguros.mantenedorclientes.dao;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion;
import cl.cencosud.ventaseguros.common.EstadoCivil;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.common.model.Solicitud;
import cl.tinet.common.dao.jdbc.BaseDAO;
import cl.tinet.common.seguridad.model.UsuarioExterno;

/**
 * DAO de Pagina intermedia.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public interface MantenedorClientesDAO extends BaseDAO {

    /**
     * Un listado de seg�n parametro de sistema.
     * @param idGrupo identificador del grupo de parametros.
     * @return obtiene un parametro en particular.
     */
    List < Map < String, ? > > obtenerParametro(String idGrupo);

    /**
     * Obtiene un parametro de sistema en particular.
     * @param idGrupo identificador de grupo de parametros.
     * @param valorParametro valor del parametro.
     * @return parametro de sistema.
     */
    List < Map < String, ? > > obtenerParametro(String idGrupo,
        String valorParametro);

    /**
    * Obtiene listado de Estado Civiles disponibles. 
    * @return estados civiles.
    */
    EstadoCivil[] obtenerEstadosCiviles();

    /**
     * Obtiene listado de Regiones.
     * @return listado de regiones.
     */
    Parametro[] obtenerRegiones();

    /**
     * Obtiene listado de Comunas.
     * @param idRegion id de region.
     * @return listado de comunas.
     */
    Parametro[] obtenerComunas(String idRegion);

    /**
     * Obtiene listado de Ciudades.
     * @param idComuna id de comuna.
     * @return listado de ciudades.
     */
    Parametro[] obtenerCiudades(String idComuna);

    /**
     * TODO Describir m�todo obtenerComunaPorCiudad.
     * @param idCiudad
     * @return
     */
    Parametro[] obtenerComunaPorCiudad(String idCiudad);

    /**
     * TODO Describir m�todo obtenerRegionPorComuna.
     * @param idComuna
     * @return
     */
    Parametro[] obtenerRegionPorComuna(String idComuna);

    /**
     * TODO Describir m�todo actualizarCliente.
     * @param datos
     */
    void actualizarCliente(Map < String, String > datos);

    /**
     * Obtener solicitudes de un usuario.
     * @param rutUsuario rut del usuario
     * @return listado de solicitudes.
     */
    List < Solicitud > obtenerSolicitudes(Long rutUsuario);

    /**
     * obtenerSolicitudesWS.
     * @param rutCliente rut del cliente
     * @return listado de solicitudes.
     */
    public SolicitudCotizacion[] obtenerSolicitudesWS(Long rutCliente);

    long registrarUsuario(UsuarioExterno usuario);

    long registrarUsuarioExterno(UsuarioExterno usuario);

    long registrarClaveUsuarioExterno(long idUsuario, String password);

    UsuarioExterno getCliente(String rutCliente, String dv);

    void actualizarRolUsuario(long idUsuario);

    int obtenerNroOrdenCompra(String idSolicitud);

    public String obtenerDatosEmail(String nombre);

    public InputStream obtenerCuerpoEmail(String nombre);

    public List < Map < String, Object > > obtenerCoberturas(int idPlan);
    
    /**
     * Obtiene el nombre de la subcategoria desde el identificador.
     * @param idSubcategoria identificador subcategoria.
     * @return nombre subcategoria.
     */
    public String obtenerSubcategoria(String idSubcategoria);
    
    /**
     * Guarda datos del formulario de cotizacion.
     * @param rut 
     * @param nombre
     * @param telefono
     * @param descSubcategoria
     */
    void registrarCotizacion(String rut, String nombre, String telefono, String descSubcategoria);
    
    
    //CBM 
    public String generarSecuenciaDenuncias();
}
