package cl.cencosud.ventaseguros.asesorvirtual.dao.jdbc;

import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.dao.AsesorVirtualDAO;
import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.cencosud.ventaseguros.common.config.VSPConfig;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;
import cl.tinet.common.ssh.FileUploader;

/**
 * Encargado de manejar las consultas de Asesor Virtual.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class AsesorVirtualDAOJDBC extends ManagedBaseDAOJDBC implements
    AsesorVirtualDAO {

    private static final String SQL_BUSCAR_ARBOL =
        "select id_arbol, estado, es_sincronizado, fecha_sincronizacion, "
            + "fecha_creacion, fecha_modificacion, id_pais, username "
            + "from ARBOL where id_arbol = ?";

    private static final String SQL_ACTUALIZAR_ARBOL =
        "update ARBOL set fecha_modificacion = {fn CURTIME()}, username = ? "
            + "where id_arbol = ?";

    private static final String SQL_CREAR_ARBOL =
        "insert into ARBOL values (?, 1, 0, {fn CURTIME()}, "
            + "{fn CURTIME()}, {fn CURTIME()}, 1, ?)";

    private static final String SQL_BUSCAR_SECCION =
        "select id_preocupacion, id_arbol, titulo_preocupacion, "
            + "fecha_creacion, fecha_modificacion, id_pais, username "
            + "from PREOCUPACION where id_preocupacion = ? and id_arbol=?";

    private static final String SQL_BUSCAR_SECCIONES_POR_ARBOL =
        "select id_preocupacion, id_arbol, titulo_preocupacion, "
            + "fecha_creacion, fecha_modificacion, id_pais, username "
            + "from PREOCUPACION where id_arbol=?";

    private static final String SQL_ACTUALIZAR_SECCION =
        "update PREOCUPACION set titulo_preocupacion = ?, fecha_modificacion = "
            + "{fn CURTIME()}, username = ? where id_preocupacion = ? "
            + "and id_arbol = ?";

    private static final String SQL_CREAR_SECCION =
        "insert into PREOCUPACION values (?, ?, ?, {fn CURTIME()}, "
            + "{fn CURTIME()}, 1, ?)";

    private static final String SQL_BUSCAR_PREGUNTA =
        "select id_pregunta, id_preocupacion, titulo_pregunta, tipo_pregunta, "
            + "fecha_creacion, fecha_modificacion, id_pais, username "
            + "from PREGUNTA where id_pregunta = ? and id_preocupacion = ?";

    private static final String SQL_BUSCAR_PREGUNTAS_POR_SECCION =
        "select id_pregunta, id_preocupacion, titulo_pregunta, tipo_pregunta, "
            + "fecha_creacion, fecha_modificacion, id_pais, username "
            + "from PREGUNTA where id_preocupacion = ?";

    private static final String SQL_ACTUALIZAR_PREGUNTA =
        "update PREGUNTA set titulo_pregunta = ?, tipo_pregunta = ?, "
            + "fecha_modificacion = {fn CURTIME()}, username = ? where "
            + "id_pregunta = ? and id_preocupacion = ?";

    private static final String SQL_CREAR_PREGUNTA =
        "insert into PREGUNTA values (?, ?, ?, ?, {fn CURTIME()}, "
            + "{fn CURTIME()}, 1, ?)";

    private static final String SQL_BUSCAR_ALTERNATIVA =
        "select id_alternativa, id_preocupacion, id_pregunta, id_resultado, "
            + "id_proxima_pregunta, texto_alternativa_0, texto_alternativa_1, "
            + "valor_alternativa_1, valor_alternativa_0, fecha_creacion, "
            + "fecha_modificacion, id_pais, username from ALTERNATIVA "
            + "where id_alternativa = ? and id_preocupacion = ? "
            + "and id_pregunta = ?";

    private static final String SQL_BUSCAR_ALTERNATIVA_POR_PREGUNTA =
        "select id_alternativa, id_preocupacion, id_pregunta, id_resultado, "
            + "id_proxima_pregunta, texto_alternativa_0, texto_alternativa_1, "
            + "valor_alternativa_1, valor_alternativa_0, fecha_creacion, "
            + "fecha_modificacion, id_pais, username from ALTERNATIVA "
            + "where id_preocupacion = ? and id_pregunta = ?";

    private static final String SQL_ACTUALIZAR_ALTERNATIVA =
        "update ALTERNATIVA set id_resultado = ?, id_proxima_pregunta= ?, "
            + "texto_alternativa_0 = ?, texto_alternativa_1 = ?, "
            + "valor_alternativa_1 = ?, valor_alternativa_0 = ?, "
            + "fecha_modificacion = {fn CURTIME()}, username = ? "
            + "where id_alternativa = ? and id_preocupacion = ? "
            + "and id_pregunta = ? ";

    private static final String SQL_CREAR_ALTERNATIVA =
        "insert into ALTERNATIVA values (?, ?, ?, ?, ?, ?, ?, ?, ?, "
            + "{fn CURTIME()}, {fn CURTIME()}, 1, ?)";

    private static final String SQL_BUSCAR_RESULTADO =
        "select id_resultado, id_preocupacion, titulo_resultado, nombre_plan, "
            + "link0, link1, icono, valor, fecha_creacion, fecha_modificacion, "
            + "id_pais, username from RESULTADO where id_resultado = ? and "
            + "id_preocupacion = ?";

    private static final String SQL_BUSCAR_RESULTADO_POR_SECCION =
        "select id_resultado, id_preocupacion, titulo_resultado, nombre_plan, "
            + "link0, link1, icono, valor, fecha_creacion, fecha_modificacion, "
            + "id_pais, username from RESULTADO where id_preocupacion = ?";

    private static final String SQL_ACTUALIZAR_RESULTADO =
        "update RESULTADO set titulo_resultado = ?, nombre_plan = ?, "
            + "link0 = ?, link1 = ?, icono = ?, valor = ?, "
            + "fecha_modificacion = {fn CURTIME()}, username = ? "
            + "where id_resultado = ? and id_preocupacion = ?";

    private static final String SQL_CREAR_RESULTADO =
        "insert into RESULTADO values (?, ?, ?, ?, ?, ?, ?, ?, "
            + "{fn CURTIME()}, {fn CURTIME()}, 1, ?)";

    private static final String SQL_ACTUALIZAR_RESULTADO_ALTERNATIVA =
        "update alternativa set id_resultado=?, "
            + "fecha_modificacion={fn CURTIME()}, username=? "
            + "where id_alternativa=? and id_pregunta = ?, id_preocupacion=?";

    private static final String SQL_ACTUALIZAR_PROX_PREGUNTA_ALTERNATIVA =
        "update alternativa set id_proxima_pregunta=?, "
            + "fecha_modificacion={fn CURTIME()}, username=? "
            + "where id_alternativa=? and id_pregunta = ?, id_preocupacion=?";

    private static final String SQL_BORRAR_ALTERNATIVAS =
        "delete from alternativa";

    private static final String SQL_BORRAR_PREGUNTAS = "delete from pregunta";

    private static final String SQL_BORRAR_RESULTADOS = "delete from resultado";

    private static final String SQL_BORRAR_PREOCUPACIONES =
        "delete from preocupacion";

    private static final String SQL_BORRAR_ARBOL = "delete from arbol";

    private static final String XML_MZZO_SERVER_NAME =
        "cl.cencosud.ventaseguros.ejb.sincronizarArchivo.servername.";

    private static final String XML_MZZO_USERNAME =
        "cl.cencosud.ventaseguros.ejb.sincronizarArchivo.username.";

    private static final String XML_MZZO_PASSWORD =
        "cl.cencosud.ventaseguros.ejb.sincronizarArchivo.password.";

    private static final String XML_MZZO_REMOTE_PATH =
        "cl.cencosud.ventaseguros.ejb.sincronizarArchivo.remotePath.";

    /**
     * Graba la informacion de cabecera del arbol.
     * @param idArbol Identificador del arbol.
     * @param username Nombre de usuario que genera la consulta.
     */
    public void grabarArbol(int idArbol, String username) {
        Object[] params = new Object[] { idArbol };
        //Validar existencia
        List < Map < String, Object > > res =
            this.query(Map.class, SQL_BUSCAR_ARBOL, params);

        if (res != null && res.size() > 0) {
            //Si existe update
            params = new Object[] { username, idArbol };
            this.update(SQL_ACTUALIZAR_ARBOL, params);

        } else {
            //Si no existe Insert
            params = new Object[] { idArbol, username };
            this.insertWithoutGeneratedKey(SQL_CREAR_ARBOL, params);
        }
    }

    public void grabarSeccion(Seccion seccion, int idArbol, String username) {

        Object[] params = new Object[] { seccion.getId(), idArbol };

        //Validar existencia
        List < Map < String, Object > > res =
            this.query(Map.class, SQL_BUSCAR_SECCION, params);

        if (res != null && res.size() > 0) {
            //Si existe update
            params =
                new Object[] { seccion.getTitulo(), username, seccion.getId(),
                    idArbol };
            this.update(SQL_ACTUALIZAR_SECCION, params);

        } else {
            //Si no existe Insert
            params =
                new Object[] { seccion.getId(), idArbol, seccion.getTitulo(),
                    username };
            this.insertWithoutGeneratedKey(SQL_CREAR_SECCION, params);
        }
    }

    public void grabarPregunta(Pregunta pregunta, int idSeccion, String username) {
        Object[] params = new Object[] { pregunta.getId(), idSeccion };

        //Validar existencia
        List < Map < String, Object > > res =
            this.query(Map.class, SQL_BUSCAR_PREGUNTA, params);

        if (res != null && res.size() > 0) {
            //Si existe update
            params =
                new Object[] { pregunta.getTitulo(),
                    pregunta.getTipoPregunta(), username, pregunta.getId(),
                    idSeccion };
            this.update(SQL_ACTUALIZAR_PREGUNTA, params);

        } else {
            //Si no existe Insert
            params =
                new Object[] { pregunta.getId(), idSeccion,
                    pregunta.getTitulo(), pregunta.getTipoPregunta(), username };
            this.insertWithoutGeneratedKey(SQL_CREAR_PREGUNTA, params);
        }
    }

    public void grabarAlternativa(Alternativa alternativa, int idPregunta,
        int idSeccion, String username) {
        Object[] params =
            new Object[] { alternativa.getId(), idSeccion, idPregunta };

        //Validar existencia
        List < Map < String, Object > > res =
            this.query(Map.class, SQL_BUSCAR_ALTERNATIVA, params);

        if (res != null && res.size() > 0) {
            //Si existe update
            params =
                new Object[] { alternativa.getRespuesta(),
                    alternativa.getProximaPregunta(), alternativa.getTexto0(),
                    alternativa.getTexto1(), alternativa.getValor0(),
                    alternativa.getValor1(), username, alternativa.getId(),
                    idSeccion, idPregunta };
            this.update(SQL_ACTUALIZAR_ALTERNATIVA, params);

        } else {
            //Si no existe Insert
            params =
                new Object[] { alternativa.getId(), idPregunta, idSeccion,
                    alternativa.getRespuesta(),
                    alternativa.getProximaPregunta(), alternativa.getTexto0(),
                    alternativa.getTexto1(), alternativa.getValor0(),
                    alternativa.getValor1(), username };
            this.insertWithoutGeneratedKey(SQL_CREAR_ALTERNATIVA, params);
        }
    }

    public void grabarResultado(Resultado resultado, int idSeccion,
        String username) {
        Object[] params = new Object[] { resultado.getId(), idSeccion };

        //Validar existencia
        List < Map < String, Object > > res =
            this.query(Map.class, SQL_BUSCAR_RESULTADO, params);

        if (res != null && res.size() > 0) {
            //Si existe update
            params =
                new Object[] { resultado.getTitulo(),
                    resultado.getNombreplan(), resultado.getLink0(),
                    resultado.getLink1(), resultado.getIcono(),
                    resultado.getValue(), username, resultado.getId(),
                    idSeccion };
            this.update(SQL_ACTUALIZAR_RESULTADO, params);

        } else {
            //Si no existe Insert
            params =
                new Object[] { resultado.getId(), idSeccion,
                    resultado.getTitulo(), resultado.getNombreplan(),
                    resultado.getLink0(), resultado.getLink1(),
                    resultado.getIcono(), resultado.getValue(), username };
            this.insertWithoutGeneratedKey(SQL_CREAR_RESULTADO, params);
        }
    }

    /**
     * Busca la informacion del arbol.
     * @param idArbol Identificador del arbol guardado.
     * @return Listado de datos.
     */
    public List < Map < String, Object > > buscarArbol(int idArbol) {
        Object[] params = new Object[] { idArbol };
        return this.query(Map.class, SQL_BUSCAR_ARBOL, params);
    }

    /**
     * Busca las secciones que pertenecen a un arbol.
     * @param idArbol Identificador del arbol guardado.
     * @return Listado de secciones.
     */
    public List < Map < String, Object > > buscarSecciones(int idArbol) {
        Object[] params = new Object[] { idArbol };
        return this.query(Map.class, SQL_BUSCAR_SECCIONES_POR_ARBOL, params);
    }

    /**
     * Busca las preguntas asociadas a una seccion.
     * @param idSeccion Identificador de la seccion.
     * @return Listado de Preguntas.
     */
    public List < Map < String, Object > > buscarPreguntas(int idSeccion) {
        Object[] params = new Object[] { idSeccion };
        return this.query(Map.class, SQL_BUSCAR_PREGUNTAS_POR_SECCION, params);
    }

    /**
     * Busca las alternativas de una pregunta.
     * @param idSeccion identificador de seccion.
     * @param idPregunta identificador de pregunta.
     * @return Lsitado de Alternativas.
     */
    public List < Map < String, Object > > buscarAlternativas(int idSeccion,
        int idPregunta) {
        Object[] params = new Object[] { idSeccion, idPregunta };
        return this.query(Map.class, SQL_BUSCAR_ALTERNATIVA_POR_PREGUNTA,
            params);
    }

    /**
     * Buscar resultados de una seccion.
     * @param idSeccion identificador de seccion.
     * @return Listado de respuestas.
     */
    public List < Map < String, Object > > buscarResultados(int idSeccion) {
        Object[] params = new Object[] { idSeccion };
        return this.query(Map.class, SQL_BUSCAR_RESULTADO_POR_SECCION, params);
    }

    public void actualizarAlternativa(int idAlternativa, int idSeccion,
        int idPregunta, int idRespuesta, String username) {
        Object[] params =
            new Object[] { idRespuesta, username, idAlternativa, idPregunta,
                idSeccion };
        this.update(SQL_ACTUALIZAR_RESULTADO_ALTERNATIVA, params);
    }

    public void actualizarProximaPregunta(int idAlternativa, int idSeccion,
        int idPregunta, int idProxPregunta, String username) {
        Object[] params =
            new Object[] { idProxPregunta, username, idAlternativa, idPregunta,
                idSeccion };
        this.update(SQL_ACTUALIZAR_PROX_PREGUNTA_ALTERNATIVA, params);
    }

    public void borrarAlternativas() {
        this.update(SQL_BORRAR_ALTERNATIVAS, new Object[] {});
    }

    public void borrarPreguntas() {
        this.update(SQL_BORRAR_PREGUNTAS, new Object[] {});
    }

    public void borrarResultados() {
        this.update(SQL_BORRAR_RESULTADOS, new Object[] {});
    }

    public void borrarPreocupaciones() {
        this.update(SQL_BORRAR_PREOCUPACIONES, new Object[] {});
    }

    public void borrarArbol() {
        this.update(SQL_BORRAR_ARBOL, new Object[] {});
    }

    /**
     * TODO Describir m�todo sincronizarArchivo.
     * @param nodo
     * @param rutaLocal
     */
    public boolean sincronizarArchivo(String nodo, String rutaLocal) {
        //Obtener datos desde archivo de configuracion.
        //servidor destino, usuario, password, ruta destino
        VSPConfig vsp = VSPConfig.getInstance();

        String serverName = vsp.getString(XML_MZZO_SERVER_NAME + nodo);
        String username = vsp.getString(XML_MZZO_USERNAME + nodo);
        String password = vsp.getString(XML_MZZO_PASSWORD + nodo);
        String remotePath = vsp.getString(XML_MZZO_REMOTE_PATH + nodo);

        FileUploader fileUploader =
            new FileUploader(serverName, username, password);
        return fileUploader.sendFile(rutaLocal, remotePath);

    }

}
