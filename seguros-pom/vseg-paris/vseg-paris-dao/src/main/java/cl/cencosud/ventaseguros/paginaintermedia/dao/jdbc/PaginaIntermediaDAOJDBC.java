package cl.cencosud.ventaseguros.paginaintermedia.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.common.TipoRama;
import cl.cencosud.ventaseguros.paginaintermedia.dao.PaginaIntermediaDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

/**
 * Encargado de manejar las consultas de Pagina intermedia.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class PaginaIntermediaDAOJDBC extends ManagedBaseDAOJDBC implements
    PaginaIntermediaDAO {

    /**
     * Consulta para obtener las ramas.
     */
    private static final String SQL_LISTA_RAMAS =
        "SELECT id_rama, id_tipo_producto, titulo_rama, fecha_creacion,"
            + " fecha_modificacion, id_pais, descripcion_rama FROM RAMA";
    /**
     * Consulta para obtener las subcategorias.
     */
    private static final String SQL_LISTA_SUBCATEGORIAS =
        "SELECT id_subcategoria, id_rama, titulo_subcategoria, es_eliminado,"
            + " fecha_creacion, fecha_modificacion, id_pais, username FROM"
            + " SUBCATEGORIA WHERE id_rama = ? ";
    
    /**
     * Consulta para obtener las subcategorias.
     */
    private static final String SQL_LISTA_SUBCATEGORIAS_TIPO =
        "SELECT id_subcategoria, id_rama, titulo_subcategoria, es_eliminado,"
            + " fecha_creacion, fecha_modificacion, id_pais, username FROM"
            + " SUBCATEGORIA WHERE id_rama = ? AND id_tipo = ?";

    private static final String SQL_LISTA_TIPOS_RAMA =
        "SELECT id_tipo, id_rama, descripcion_tipo"
            + " FROM TIPO_RAMA WHERE id_rama = ?";
    
    /**
     * Consulta para obtener el listado de subcategorias con descripcion de la
     * rama.
     */
    private static final String SQL_LISTA_PAGINA_INTERMEDIA =
        "select s.id_subcategoria, s.titulo_subcategoria, r.titulo_rama, "
            + "s.es_eliminado from SUBCATEGORIA s, RAMA r where "
            + "s.id_rama = r.id_rama ";

    /**
     * Consulta  para obtener los datos de una subcategoria.
     */
    private static final String SQL_SUBCATEGORIA =
        "select id_subcategoria, id_rama, titulo_subcategoria, es_eliminado,"
            + " fecha_creacion, fecha_modificacion, id_pais, username "
            + " from SUBCATEGORIA where id_subcategoria = ?";

    private static final String SQL_PRODUCTOS_SUB =
        "select pl.* from PRODUCTO p, PRODUCTO_LEG pl where "
            + "p.id_subcategoria = ? and pl.id_producto = p.id_producto_leg "
            + "and p.es_eliminado = 0 and p.es_activo = 1 order by pl.nombre";

    private static final String SQL_PRODUCTOS_NOSUB =
        "select pl.* from PRODUCTO_LEG pl where not exists "
            + "(select 1 from PRODUCTO p where p.id_subcategoria=? and "
            + "p.id_producto_leg = pl.id_producto and p.es_activo = 1) and "
            + "pl.es_activo = 1 and upper(pl.nombre) like (upper(?)) "
            + "order by pl.nombre";

    private static final String SQL_AGREGAR_PRODUCTO =
        "insert into PRODUCTO (id_producto, id_subcategoria, id_producto_leg, "
            + "es_eliminado, fecha_creacion, fecha_actualizacion, id_pais, "
            + "username, es_activo) values (producto_seq.nextval, ?, ?, 0, "
            + "sysdate, sysdate, 1, ?, 1)";

    private static final String SQL_EXISTE_PRODUCTO =
        "select 1 from PRODUCTO where id_subcategoria = ? and "
            + "id_producto_leg = ?";

    private static final String SQL_ACTUALIZAR_PRODUCTO =
        "update PRODUCTO set es_activo=? where id_subcategoria = ? and "
            + "id_producto_leg = ?";

    private static final String SQL_ACTUALIZAR_SUBCATEGORIA =
        "update SUBCATEGORIA set es_eliminado=?, titulo_subcategoria=? "
            + "where id_subcategoria = ?";

    private static final String SQL_OBTIENE_SUBCATEGORIAS_PAGINA_INTERMENEDIA =
        "select s.id_subcategoria, s.id_rama, s.titulo_subcategoria, "
            + "f.descripcion_pagina, s.es_eliminado, s.fecha_creacion, "
            + "s.fecha_modificacion, s.id_pais, s.username, s.id_tipo "
            + "from SUBCATEGORIA s, FICHA f where s.id_rama = ? "
            + "and s.id_subcategoria = f.id_subcategoria and s.es_eliminado = 0 "
            + "and f.estado_ficha = 1 and f.es_subcategoria = 1 ";

    private static final String SQL_AGREGAR_SUBCATEGORIA =
        "insert into SUBCATEGORIA (ID_SUBCATEGORIA,ID_RAMA,TITULO_SUBCATEGORIA,"
            + "ES_ELIMINADO,FECHA_CREACION,FECHA_MODIFICACION,"
            + "ID_PAIS,USERNAME, ID_TIPO) "
            + "values(subcategoria_seq.nextval, ?, ?, 1, "
            + "{fn CURTIME()}, {fn CURTIME()}, 1, ?, ?)";
    
    private static final String SQL_OBTENER_PROMOCIONES =
    	"SELECT ID, TIPO, ORDINAL, NOMBRE, LINK_VER_BASES, TRACKER, IMAGEN, FECHA_INICIO, FECHA_TERMINO, TIPO_BANNER, " +
    	"LINK_CONOCER, LINK_COTIZAR, RAMA FROM PROMOCION_PRINCIPAL" +
    	" WHERE TIPO = ? AND ID_PLAN != 0";
    
    private static final String SQL_OBTENER_PROMOCIONES_SECUNDARIAS =
    	"SELECT ID, TIPO, ORDINAL, NOMBRE, LINK_VER_BASES, TRACKER, IMAGEN, FECHA_INICIO, FECHA_TERMINO, TIPO_BANNER, " +
    	"LINK_CONOCER, LINK_COTIZAR, RAMA FROM PROMOCION_SECUNDARIA" +
    	" WHERE RAMA = ? AND ID_PLAN != 0";

    /**
     * Obtiene un listado de ramas de productos.
     * @return Listado de ramas.
     */
    public List < Rama > obtenerRamas() {
        Object[] params = new Object[] {};
        List < Rama > res =
            (ArrayList < Rama >) this
                .query(Rama.class, SQL_LISTA_RAMAS, params);

        return res;
    }

    /**
     * Obtiene un listado de subcategorias de productos.
     * @param idRama Identificador de la rama.
     * @return Listado de subcategorias.
     */
    public List < Subcategoria > obtenerSubcategorias(int idRama) {
        Object[] params = new Object[] { idRama, };
        List < Subcategoria > res =
            (ArrayList < Subcategoria >) this.query(Subcategoria.class,
                SQL_LISTA_SUBCATEGORIAS, params);

        return res;
    }
    
    /**
     * Obtiene un listado de subcategorias de productos.
     * @param idRama Identificador de la rama.
     * @param idTipo Identificador del tipo de subcategoria.
     * @return Listado de subcategorias.
     */
    public List < Subcategoria > obtenerSubcategoriasTipo(int idRama, int idTipo) {
        Object[] params = new Object[] { idRama, idTipo};
        List < Subcategoria > res =
            (ArrayList < Subcategoria >) this.query(Subcategoria.class,
                SQL_LISTA_SUBCATEGORIAS_TIPO, params);

        return res;
    }
    
    /**
     * Obtiene un listado de tipos de rama.
     * @param idRama Identificador de la rama.
     * @return Listado de tipos de rama.
     */
    public List < TipoRama > obtenerTiposRama(int idRama) {
        Object[] params = new Object[] { idRama};
        List < TipoRama > res =
            (ArrayList < TipoRama >) this.query(TipoRama.class,
                SQL_LISTA_TIPOS_RAMA, params);
        return res;
    }

    /**
     * Obtiene un listado de subcategorias asociadas a una rama.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de subcategorias.
     */
    public List < Map < String, ? >> obtenerListaPaginaIntermedia(int idRama,
        int idSubcategoria) {

        List < Map < String, ? > > res = null;
        String sql = "";

        if (idSubcategoria > 0) {
            sql =
                SQL_LISTA_PAGINA_INTERMEDIA
                    + "and r.id_rama = ? and s.id_subcategoria = ? ";

            Object[] params = new Object[] { idRama, idSubcategoria, };

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class, sql,
                    params);
        } else if (idRama > 0) {
            sql = SQL_LISTA_PAGINA_INTERMEDIA + "and r.id_rama = ? ";

            Object[] params = new Object[] { idRama, };

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class, sql,
                    params);
        } else {
            Object[] params = new Object[] {};

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class,
                    SQL_LISTA_PAGINA_INTERMEDIA, params);
        }
        return res;
    }

    /**
     * Obtiene los datos de una subcatrgoria a partir del id especificado.
     * @param idSubcategoria identificador de subcategoria.
     * @return Subcatogoria.
     */
    public Subcategoria obtenerSubcategoria(int idSubcategoria) {
        Subcategoria subcategoria = new Subcategoria();
        Object[] params = new Object[] { idSubcategoria };
        List < Subcategoria > res =
            (List < Subcategoria >) this.query(Subcategoria.class,
                SQL_SUBCATEGORIA, params);
        if (res != null && !res.isEmpty()) {
            subcategoria = (Subcategoria) res.get(0);
        }

        return subcategoria;
    }

    /**
     * Obtiene un listado de producto que tienen relacion con la subcategoria.
     * @param idSubcategoria Identificador de la subcategoria.
     * @param asignado Si es verdadero, se obtienen los productos asociados a 
     * la subcategoria, si es falso, se obtienen los productos disponibles 
     * para asociar a la subcategoria.
     * @param nombre Nombre del producto a buscar.
     * @return Listado de productos.
     */
    public List < ProductoLeg > obtenerListaProductos(int idSubcategoria,
        boolean asignado, String nombre) {
        List < ProductoLeg > res = null;
        if (asignado) {
            Object[] params = new Object[] { idSubcategoria };
            //Buscar los productos que pertenecen a la subcategoria.
            res =
                (ArrayList < ProductoLeg >) this.query(ProductoLeg.class,
                    SQL_PRODUCTOS_SUB, params);
        } else {
            String nombreBuscar = (nombre == null ? "%" : "" + nombre + "%");
            Object[] params = new Object[] { idSubcategoria, nombreBuscar };
            //Buscar los productos que se pueden asignar a la subcategoria.
            res =
                (ArrayList < ProductoLeg >) this.query(ProductoLeg.class,
                    SQL_PRODUCTOS_NOSUB, params);
        }

        return res;
    }

    /**
     * Agrega un producto a un a subcategoria.
     * @param idSubcategoria identificador de subcategoria.
     * @param idProductoLeg identificador de producto legacy.
     * @return identificador de insercion.
     */
    public long agregarProducto(int idSubcategoria, int idProductoLeg) {
        long res = -1;
        Object[] params =
            new Object[] { idSubcategoria, idProductoLeg, "dummy" };
        res = this.insert(SQL_AGREGAR_PRODUCTO, params);
        return res;
    }

    public boolean existeProducto(int idSubcategoria, int idProductoLeg) {
        boolean res = false;

        Object[] params = new Object[] { idSubcategoria, idProductoLeg };
        List productos = this.query(Map.class, SQL_EXISTE_PRODUCTO, params);
        if (productos != null && !productos.isEmpty()) {
            res = true;
        }

        return res;
    }

    public void activarProducto(int idSubcategoria, int idProductoLeg) {
        Object[] params = new Object[] { 1, idSubcategoria, idProductoLeg };
        this.update(SQL_ACTUALIZAR_PRODUCTO, params);

    }

    public void eliminarProducto(int idSubcategoria, int idProductoLeg) {
        Object[] params = new Object[] { 0, idSubcategoria, idProductoLeg };
        this.update(SQL_ACTUALIZAR_PRODUCTO, params);
    }

    public void modificarSubcategoria(int idSubcategoria, String titulo,
        int estado) {
        Object[] params = new Object[] { estado, titulo, idSubcategoria };
        this.update(SQL_ACTUALIZAR_SUBCATEGORIA, params);
    }

    public List < Subcategoria > obtenerSubcategoriasPaginaIntermedia(int idRama) {
        Object[] params = new Object[] { idRama };
        List < Subcategoria > res = null;
        res =
            (List < Subcategoria >) this.query(Subcategoria.class,
                SQL_OBTIENE_SUBCATEGORIAS_PAGINA_INTERMENEDIA, params);
        return res;
    }

    public long agregarSubcategoria(int idRama, String subcategoria, int idTipo) {
        long res = -1;
        Object[] params = new Object[] { idRama, subcategoria, "dummy", idTipo };
        res = this.insert(SQL_AGREGAR_SUBCATEGORIA, params);
        return res;
    }

	public List <HashMap <String, Object>> obtenerPromociones(String tipo) {
		Object[] params = new Object[] { tipo };
		List <HashMap <String, Object>> res = null;
		res = (List <HashMap <String, Object>>) this.query(Map.class, SQL_OBTENER_PROMOCIONES, params);
		
		return res;
	}

	public List<HashMap<String, Object>> obtenerPromocionesSecundarias(
			String idRama) {
		Object[] params = new Object[] { idRama };
		List <HashMap <String, Object>> res = null;
		res = (List <HashMap <String, Object>>) this.query(Map.class, SQL_OBTENER_PROMOCIONES_SECUNDARIAS, params);
		
		return res;
	}

}
