package cl.cencosud.ventaseguros.informescomerciales.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.tinet.common.dao.jdbc.BaseDAO;

public interface InformesComercialesDAO extends BaseDAO {

    List < HashMap < String, Object > > obtenerInformeComercialVehiculos(Date fechaDesde,
        Date fechaHasta, boolean esPaginado, int numeroPagina);

    List < HashMap < String, Object > > obtenerInformeComercialTarjetas(Date fechaDesde,
        Date fechaHasta, boolean esPaginado, int numeroPagina);
    
    List < HashMap < String, Object > > obtenerInformeComercialVentas(String fechaDesde,
    	String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria,
    	int id_plan, String compannia);
    
    List < HashMap < String, Object > > obtenerInformeComercialCotizaciones(String fechaDesde,
        	String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria,
        	int id_plan, String compannia);
    
    Map < String, Object > obtenerArchivoFactura(long id_factura);

	List<HashMap<String, Object>> obtenerVitrineoGeneral(
			String fechaDesde, String fechaHasta, boolean esPaginado,
			int numeroPagina, int rama, int subcategoria);

	boolean ingresarPromocionPrincipal(String principal, String tipo, String posicion,
			String nombre, String linkVer, String tracker, String imagen,
			Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, String linkConocer, String linkCotizar,String idPlan);


	boolean ingresarPromocionSecundaria(String principal, String tipo,
			String ordinal, String nombre, String linkVer, String tracker,
			String imagen, Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, String linkConocer, String linkCotizar,String idPlan);

	List<HashMap<String, Object>> obtenerPromocionesPrincipalBP(String tipoS);

	List<Map<String, Object>> obtenerPromocionesBO();

	List<Map<String, Object>> obtenerPromocionesBOPrincipal();


	
}
