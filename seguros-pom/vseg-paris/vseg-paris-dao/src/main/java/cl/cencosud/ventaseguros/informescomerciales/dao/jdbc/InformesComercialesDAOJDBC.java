package cl.cencosud.ventaseguros.informescomerciales.dao.jdbc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.informescomerciales.InformeComercialVehiculo;
import cl.cencosud.ventaseguros.informescomerciales.dao.InformesComercialesDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

import com.tinet.exceptions.system.SystemException;

public class InformesComercialesDAOJDBC extends ManagedBaseDAOJDBC implements
    InformesComercialesDAO {

    /**
     * Logger de la clase
     */
    private static final Log logger =
        LogFactory.getLog(InformesComercialesDAOJDBC.class);

    private static final String SQL_INFORME_VEHICULOS =
        "select P.CODIGO_PLAN_LEG as IdSeguro, T.NUMERO_ORDEN_COMPRA as NumeroPoliza ,"
            + " C.NOMBRE || ' ' || C.APELLIDO_PATERNO || ' ' || C.APELLIDO_MATERNO as NombreCliente , "
            + "C.TELEFONO_1 as FonoCliente , C.EMAIL as EmailCliente , F.ID_FACTURA as IdFactura ,"
            + " T.FECHA_CREACION as FechaSeguro , C.RUT_CONTRATANTE || '-' || C.DV_CONTRATANTE as RutCliente , "
            + "P.NOMBRE as NombreProducto , 'NEM' as Nemotecnico , S.PRIMA_MESUAL_UF as PrimaUF , "
            + "T.MONTO as Proporcional , "
            + "PS.DESCRIPCION as TipoTarjeta  "
            + "from FACTURA F, MATERIA_ASEGURADA_VEHICULO MAV, "
            + "SOLICITUD S, PLAN_LEG P, TRANSACCION T "
            + "LEFT JOIN PARAMETRO_SISTEMA PS ON PS.GRUPO = 'WEBPAY' AND PS.VALOR = T.TIPO_MEDIO_PAGO , "
            + "CONTRATANTE C where F.FACTURA IS NOT NULL "
            + "AND F.ID_MAT_ASEG_VEHICULO = MAV.ID_MAT_ASEG_VEHICULO AND S.ID_SOLICITUD = MAV.ID_SOLICITUD "
            + "AND P.ID_PLAN = S.ID_PLAN AND T.ID_SOLICITUD = S.ID_SOLICITUD "
            + "AND C.ID_SOLICITUD = S.ID_SOLICITUD "
            + "AND S.ESTADO_SOLICITUD = 4 " //Solo compras finalizadas
            + "AND TO_CHAR(T.FECHA_CREACION, 'rrrrmmdd') BETWEEN TO_CHAR(?, 'rrrrmmdd') AND TO_CHAR(?, 'rrrrmmdd') "
            + "order by T.FECHA_CREACION DESC";

    private static final String SQL_INFORME_TARJETAS =
        "select P.CODIGO_PLAN_LEG as IdSeguro , T.NUMERO_ORDEN_COMPRA as NumeroPoliza , "
            + "C.NOMBRE || ' ' || C.APELLIDO_PATERNO || ' ' || C.APELLIDO_MATERNO as NombreCliente , "
            + "C.TELEFONO_1 as FonoCliente , C.EMAIL as EmailCliente , T.FECHA_CREACION as FechaSeguro , "
            + "C.RUT_CONTRATANTE || '-' || C.DV_CONTRATANTE as RutCliente , P.NOMBRE as NombreProducto , "
            + "'NEM' as Nemotecnico , S.PRIMA_MESUAL_UF as PrimaUF ,  "
            + "PS.DESCRIPCION as TipoTarjeta   , "
            + "R.TBK_CODIGO_AUTORIZACION as CodigoAutorizacion, "
            + "S.ESTADO_SOLICITUD as estadoSolicitud, "
            + "NVL(TO_CHAR(R.TBK_FINAL_NUMERO_TARJETA), T.NRO_TARJETA_ENTRADA)  as DigitosTarjeta, " //Se debe desencriptar al momento de mostrar
            + "T.MONTO as Proporcional "        //Se agrega monto proporcional al reporte.
            + "from SOLICITUD S, PLAN_LEG P, CONTRATANTE C, TRANSACCION T LEFT "
            + "JOIN RESPUESTA_TBK R ON R.TBK_ORDEN_COMPRA = T.NUMERO_ORDEN_COMPRA "
            + "LEFT JOIN PARAMETRO_SISTEMA PS ON PS.GRUPO = 'WEBPAY' AND PS.VALOR = T.TIPO_MEDIO_PAGO "
            + "where P.ID_PLAN = S.ID_PLAN AND T.ID_SOLICITUD = S.ID_SOLICITUD "
            + "AND C.ID_SOLICITUD = S.ID_SOLICITUD "
            + "AND S.ESTADO_SOLICITUD = 4 " //Solo compras finalizadas
            + "AND (T.ESTADO_TRANSACCION = 'RESPUESTA A WEBPAY: ACEPTADO' "
            + "OR T.ESTADO_TRANSACCION = 'RESPUESTA A TMAS: ACEPTADO') " //Solo incluir transacciones aceptadas
            + "AND TO_CHAR(T.FECHA_CREACION, 'rrrrmmdd') BETWEEN TO_CHAR(?, 'rrrrmmdd') AND TO_CHAR(?, 'rrrrmmdd') "          
            + "order by T.FECHA_CREACION DESC";

    private static final String SQL_INFORME_VENTAS =
    	"select distinct(P.CODIGO_PLAN_LEG) as IdSeguro , T.NUMERO_ORDEN_COMPRA as NumeroPoliza , T.FECHA_CREACION as FechaSeguro ,"
    	+ "A.RUT_ASEGURADO || '-' || A.DV_ASEGURADO as RutAsegurado ,"
    	+ "'NEM' as Nemotecnico ,P.NOMBRE as NombreProducto , RA.TITULO_RAMA as Rama , PC.NOMBRE_CIA_LEG as NombreCia ," 
    	+ "S.PRIMA_MESUAL_UF as PrimaUF , T.MONTO as Proporcional, T.TIPO_MEDIO_PAGO as MedioPago,"
    	+ "R.TBK_CODIGO_AUTORIZACION as CodigoAutorizacion "
        + "from SOLICITUD S, PLAN_LEG P, ASEGURADO A, SUBCATEGORIA SC,PLAN_COMPANNIA PC, RAMA RA,TRANSACCION T LEFT "
        + "JOIN RESPUESTA_TBK R ON R.TBK_ORDEN_COMPRA = T.NUMERO_ORDEN_COMPRA "
        + "LEFT JOIN PARAMETRO_SISTEMA PS ON PS.GRUPO = 'WEBPAY' AND PS.VALOR = T.TIPO_MEDIO_PAGO "
        + "where P.ID_PLAN = S.ID_PLAN AND T.ID_SOLICITUD = S.ID_SOLICITUD "
        + "AND A.ID_SOLICITUD = S.ID_SOLICITUD AND SC.ID_RAMA = RA.ID_RAMA " 
        + "AND PC.ID_PLAN_COMPANNIA = P.ID_PLAN "
        + "AND P.CODIGO_TIPO_PROD_LEG = RA.ID_RAMA "
        + "AND S.ESTADO_SOLICITUD IN (4, 6) " //Solo compras finalizadas y rechazadas con aceptacion BIGSA.
        + "AND T.ESTADO_TRANSACCION <> 'INICIO' "   //Solo incluir transacciones distintas a INICIO.
        + "AND T.FECHA_CREACION BETWEEN TO_DATE(?, 'dd/MM/yyyy') AND TO_DATE(?, 'dd/MM/yyyy') ";          
                 
    private static final String SQL_INFORME_COTIZACIONES =
    	"select T.FECHA_CREACION as FechaCotizacion, CO.NOMBRE||' '||CO.APELLIDO_PATERNO||' '||CO.APELLIDO_MATERNO as NombreAsegurado,CO.RUT_CONTRATANTE||'-'||CO.DV_CONTRATANTE as RutAsegurado, CO.FECHA_NACIMIENTO as FechaNacimiento, "  
    	+"CO.RUT_CONTRATANTE||'-'||CO.DV_CONTRATANTE as RutAsegurado, CO.CALLE||','||CO.NUMERO||','||CO.NUMERO_DEPARTAMENTO||','|| "  
    	+"CL.DESCRIPCION_COMUNA||','||RE.DESCRIPCION_REGION as Direccion, CO.TELEFONO_1 as Telefono1, " 
    	+"CO.TELEFONO_2 as Telefono2, CO.EMAIL as Email, RA.TITULO_RAMA as Rama, PL.NOMBRE as NomPlan, MR.DESCRIPCION_MARCA as Marca, "  
    	+"MO.DESCRIPCION_MODELO as ModeloVehiculo, MA.ANNIO_VEHICULO as AnnoVehiculo "  
    	+"from TRANSACCION T, CONTRATANTE CO, COMUNA_LEG CL, REGION_LEG RE, RAMA RA, SUBCATEGORIA SC, PLAN_LEG PL, " 
    	+"MATERIA_ASEGURADA_VEHICULO MA, MARCA_VEHICULO_LEG MR, MODELO_VEHICULO_LEG MO, PLAN_COMPANNIA PC, SOLICITUD S "   
    	+"where CO.ID_SOLICITUD = S.ID_SOLICITUD AND CO.RUT_CONTRATANTE = T.RUT_CLIENTE "
        +"AND RE.ID_REGION = CO.ID_REGION AND CO.ID_COMUNA = CL.ID_COMUNA AND CO.ID_SOLICITUD = MA.ID_SOLICITUD "
        +"AND MA.ID_MARCA_VEHICULO = MR.ID_MARCA_VEHICULO "
        +"AND MA.ID_MODELO_VEHICULO = MO.ID_MODELO_VEHICULO AND PL.CODIGO_TIPO_PROD_LEG = RA.ID_RAMA "
        +"AND PC.CODIGO_PLAN_LEG = PL.ID_PLAN "
        +"AND RA.ID_RAMA = S.ID_RAMA  AND SC.ID_RAMA = RA.ID_RAMA "
        +"AND T.ID_SOLICITUD = S.ID_SOLICITUD "
        +"AND T.FECHA_CREACION BETWEEN TO_DATE(?, 'dd/MM/yyyy') AND TO_DATE(?, 'dd/MM/yyyy') " 
        +"AND S.ESTADO_SOLICITUD IN (1,2,3) AND T.ESTADO_TRANSACCION != 'WEBPAY' " 
        +"AND PL.ID_PLAN = S.ID_PLAN";
    
    private static final String SQL_OBTENER_FACTURA =
        "select id_factura, factura as archivo, fecha_creacion, fecha_modificacion, id_pais "
            + "from FACTURA " + "where id_factura = ?";

    private static final String SQL_OBTENER_VITRINEO_VEHICULO  =
    	"select VG.RUT||'-'||VG.DV as RutCliente, VG.NOMBRE||' '||VG.APELLIDO_PATERNO||' '||VG.APELLIDO_MATERNO as NombreCliente," +
    	"VG.FECHA_CREACION as FechaCreacion, R.TITULO_RAMA as Rama, SC.TITULO_SUBCATEGORIA as Subcategoria, PL.NOMBRE as NombreProducto," + 
    	"MV.DESCRIPCION_MARCA as Marca, MODV.DESCRIPCION_MODELO as Modelo, VG.ANYO_VEHICULO as AnnioVehiculo," + 
    	"VG.TELEFONO as Telefono, VG.MAIL as Email,COM.DESCRIPCION_COMUNA||', '||RE.DESCRIPCION_REGION as Direccion, VG.DIA_NACIMIENTO||'/'||VG.MES_NACIMIENTO||'/'||VG.ANYO_NACIMIENTO as FechaNacimiento from" +  
    	" VITRINEO_GENERAL VG, RAMA R, SUBCATEGORIA SC, PRODUCTO_LEG PL, PRODUCTO P," + 
    	"MARCA_VEHICULO_LEG MV, MODELO_VEHICULO_LEG MODV,COMUNA_LEG COM, REGION_LEG RE where" +  
    	" R.ID_RAMA = VG.RAMA AND SC.ID_SUBCATEGORIA = VG.SUBCATEGORIA AND "+  
    	"VG.PRODUCTO = P.ID_PRODUCTO AND P.ID_PRODUCTO_LEG = PL.ID_PRODUCTO AND VG.MARCA_VEHICULO = MV.ID_MARCA_VEHICULO " +  
    	"AND VG.MODELO_VEHICULO = MODV.ID_MODELO_VEHICULO AND VG.FECHA_CREACION BETWEEN TO_DATE(?, 'dd/MM/yyyy') AND TO_DATE(?, 'dd/MM/yyyy') AND COM.ID_COMUNA = VG.COMUNA AND RE.ID_REGION = VG.REGION ";

	private static final String SQL_INGRESA_PROMOCION_PRINCIPAL = 
		  "INSERT INTO PROMOCION_PRINCIPAL (ID, TIPO, ORDINAL, NOMBRE, LINK_VER_BASES, TRACKER, IMAGEN, FECHA_INICIO, FECHA_TERMINO, TIPO_BANNER, RAMA, LINK_CONOCER, LINK_COTIZAR,ID_PLAN) " +
		  "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
	
	private static final String SQL_INGRESA_PROMOCION_SECUNDARIA = 
		"INSERT INTO PROMOCION_SECUNDARIA (ID, TIPO, ORDINAL, NOMBRE, LINK_VER_BASES, TRACKER, IMAGEN, FECHA_INICIO, FECHA_TERMINO, TIPO_BANNER, RAMA, LINK_CONOCER, LINK_COTIZAR,ID_PLAN) " +
		  "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

	private static final String SQL_OBTENER_PROMOCION_PRINCIPALBP = 
		"SELECT tracker from PROMOCION_PRINCIPAL where ID = ? and TIPO = ?";

	private static final String SQL_ACTUALIZAR_PROMOCION_PRINCIPAL = 
		"UPDATE PROMOCION_PRINCIPAL SET ID = ?, TIPO = ?, ORDINAL = ?, NOMBRE = ?, LINK_VER_BASES = ?,TRACKER = ?, IMAGEN = ?, FECHA_INICIO = ?, " +
		"FECHA_TERMINO = ?, TIPO_BANNER = ?, RAMA = ?, LINK_CONOCER = ?, LINK_COTIZAR = ?,ID_PLAN = ? WHERE ID = ? AND TIPO = ? AND ORDINAL = ?";

	private static final String SQL_OBTENER_PROMOCION_SECUNDARIABP = 
		"SELECT tracker from PROMOCION_SECUNDARIA where ID = ? and TIPO = ?";

	private static final String SQL_ACTUALIZAR_PROMOCION_SECUNDARIA = 
		"UPDATE PROMOCION_SECUNDARIA SET ID = ?, TIPO = ?, ORDINAL = ?, NOMBRE = ?, LINK_VER_BASES = ?,TRACKER = ?, IMAGEN = ?, FECHA_INICIO = ?, " +
		"FECHA_TERMINO = ?, TIPO_BANNER = ?, RAMA = ?, LINK_CONOCER = ?, LINK_COTIZAR = ?, ID_PLAN = ? WHERE ID = ? AND TIPO = ?";

	private static final String SQL_OBTENER_PROMOCION_BO = 
		"SELECT ID_PLAN as idPlan,ORDINAL as ordinal,ID as id,TIPO as tipo,NOMBRE as nombre, LINK_COTIZAR as linkCotizar, TRACKER as tracker, IMAGEN as imagen, LINK_VER_BASES as linkVer, " +
		"LINK_CONOCER linkConocer, FECHA_INICIO as fechaInicio, FECHA_TERMINO as fechaTermino FROM PROMOCION_SECUNDARIA";

	private static final String SQL_OBTENER_PROMOCION_BO_PRINCIPAL = 
		"SELECT ID_PLAN as idPlan,ORDINAL as ordinal,ID as id,TIPO as tipo,NOMBRE as nombre, LINK_COTIZAR as linkCotizar, TRACKER as tracker, IMAGEN as imagen, LINK_VER_BASES as linkVer," +
		" LINK_CONOCER linkConocer, FECHA_INICIO as fechaInicio, FECHA_TERMINO as fechaTermino FROM PROMOCION_PRINCIPAL";
	
    public List < HashMap < String, Object > > obtenerInformeComercialVehiculos(
        Date fechaDesde, Date fechaHasta, boolean esPaginado, int numeroPagina) {

        logger.info("INFORME VEHICULOS:");
        logger.info("DESDE: " + fechaDesde);
        logger.info("HASTA: " + fechaHasta);

        String sql = SQL_INFORME_VEHICULOS;
        Object[] params = new Object[] { fechaDesde, fechaHasta };

        List < HashMap < String, Object > > res =
            new ArrayList < HashMap < String, Object > >();

        if (esPaginado) {

            try {
                ArrayList < Object > pParamQuery = new ArrayList < Object >();
                pParamQuery.add(fechaDesde);
                pParamQuery.add(fechaHasta);

                HashMap < String, String > hParameters =
                    new HashMap < String, String >();

                hParameters.put("getPageOnList", "Y");
                hParameters.put("page", String.valueOf(numeroPagina));
                hParameters.put("totalPerPage", "15");

                res =
                    InformesComercialesDAOJDBC.getQueryPaginate(sql,
                        pParamQuery, this.getConnection(), hParameters);

            } catch (Exception e) {
                logger.error("ERROR PAGINACION: ", e);
                throw new SystemException(e);
            }
        } else {
            res = this.query(InformeComercialVehiculo.class, sql, params);
        }

        return res;
    }

    public List < HashMap < String, Object > > obtenerInformeComercialTarjetas(
        Date fechaDesde, Date fechaHasta, boolean esPaginado, int numeroPagina) {

        logger.info("INFORME TARJETAS:");
        logger.info("DESDE: " + fechaDesde);
        logger.info("HASTA: " + fechaHasta);

        String sql = SQL_INFORME_TARJETAS;
        Object[] params = new Object[] { fechaDesde, fechaHasta };

        List < HashMap < String, Object > > res =
            new ArrayList < HashMap < String, Object > >();

        if (esPaginado) {

            try {
                ArrayList < Object > pParamQuery = new ArrayList < Object >();
                pParamQuery.add(fechaDesde);
                pParamQuery.add(fechaHasta);

                HashMap < String, String > hParameters =
                    new HashMap < String, String >();

                hParameters.put("getPageOnList", "Y");
                hParameters.put("page", String.valueOf(numeroPagina));
                hParameters.put("totalPerPage", "15");

                res =
                    InformesComercialesDAOJDBC.getQueryPaginate(sql,
                        pParamQuery, this.getConnection(), hParameters);

            } catch (Exception e) {
                logger.error("ERROR PAGINACION: ", e);
                throw new SystemException(e);
            }
        } else {
            res = this.query(Map.class, sql, params);
        }

        return res;
    }
    
    public List < HashMap < String, Object > > obtenerInformeComercialVentas(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria, int id_plan, String compannia) {

            logger.info("INFORME VENTAS:");
            logger.info("DESDE: " + fechaDesde);
            logger.info("HASTA: " + fechaHasta);
            logger.info("RAMA:  " + rama);
            logger.info("SUBCATEGORIA:  " + subcategoria);
            logger.info("PLAN:  " + id_plan);
            logger.info("COMPA�IA:  " + compannia);

            String sql =SQL_INFORME_VENTAS;
            
            
            if (rama != 0){
            	sql = sql + " and RA.ID_RAMA = ?";
            }else {
            	sql = sql + " and RA.ID_RAMA != ?";
            }
            if (subcategoria != 0){
            	sql = sql + " and SC.ID_SUBCATEGORIA = ?";
            }else {
            	sql = sql + " and SC.ID_SUBCATEGORIA != ?";
            }
            if (id_plan != 0){
            	sql = sql + " and P.ID_PLAN = ?";
            }else {
            	sql = sql + " and P.ID_PLAN != ?";
            }
            if (compannia.equals("")){
            	sql = sql + " and PC.CODIGO_CIA_LEG != CONCAT(?,'  ')";
            	
            }else {
            	sql = sql + " and PC.CODIGO_CIA_LEG = ?";
            }
            Object[] params = new Object[] {  fechaDesde,fechaHasta,rama, subcategoria, id_plan,compannia };
           

            List < HashMap < String, Object > > res =
                new ArrayList < HashMap < String, Object > >();

            if (esPaginado) {

                try {
                    ArrayList < Object > pParamQuery = new ArrayList < Object >();
                    pParamQuery.add(fechaDesde);
                    pParamQuery.add(fechaHasta);
                    pParamQuery.add(rama);
                    pParamQuery.add(subcategoria);
                    pParamQuery.add(id_plan);
                    pParamQuery.add(compannia);

                    HashMap < String, String > hParameters =
                        new HashMap < String, String >();

                    hParameters.put("getPageOnList", "Y");
                    hParameters.put("page", String.valueOf(numeroPagina));
                    hParameters.put("totalPerPage", "15");
                    logger.info("iniciando consulta");
                    res =
                        InformesComercialesDAOJDBC.getQueryPaginate(sql,
                            pParamQuery, this.getConnection(), hParameters);
                    logger.info("consulta terminada");
                } catch (Exception e) {
                    logger.error("ERROR PAGINACION: ", e);
                    throw new SystemException(e);
                }
            } else {
                res = this.query(Map.class, sql, params);
            }

            return res;
        }
    
    public List < HashMap < String, Object > > obtenerInformeComercialCotizaciones(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria, int id_plan, String compannia) {

            logger.info("INFORME Cotizaciones:");
            logger.info("DESDE: " + fechaDesde);
            logger.info("HASTA: " + fechaHasta);
            logger.info("RAMA:  " + rama);
            logger.info("SUBCATEGORIA:  " + subcategoria);
            logger.info("PLAN:  " + id_plan);
            logger.info("COMPA�IA:  " + compannia);

            String sql = SQL_INFORME_COTIZACIONES;
            
            if (rama != 0){
            	sql = sql + " and RA.ID_RAMA = ?";
            }else {
            	sql = sql + " and RA.ID_RAMA != ?";
            }
            if (subcategoria != 0){
            	sql = sql + " and SC.ID_SUBCATEGORIA = ?";
            }else {
            	sql = sql + " and SC.ID_SUBCATEGORIA != ?";
            }
            if (id_plan != 0){
            	sql = sql + " and PL.ID_PLAN = ?";
            }else {
            	sql = sql + " and PL.ID_PLAN != ?";
            }
            if (compannia.equals("")){
            	sql = sql + " and PC.CODIGO_CIA_LEG != CONCAT(?,'  ')";
        	
            }else {
            	sql = sql + " and PC.CODIGO_CIA_LEG = ?";
            }
            
            Object[] params = new Object[] { fechaDesde, fechaHasta, rama, subcategoria, id_plan,compannia };

            List < HashMap < String, Object > > res =
                new ArrayList < HashMap < String, Object > >();

            if (esPaginado) {

                try {
                    ArrayList < Object > pParamQuery = new ArrayList < Object >();
                    pParamQuery.add(fechaDesde);
                    pParamQuery.add(fechaHasta);
                    pParamQuery.add(rama);
                    pParamQuery.add(subcategoria);
                    pParamQuery.add(id_plan);
                    pParamQuery.add(compannia);
                    
                    HashMap < String, String > hParameters =
                        new HashMap < String, String >();

                    hParameters.put("getPageOnList", "Y");
                    hParameters.put("page", String.valueOf(numeroPagina));
                    hParameters.put("totalPerPage", "15");

                    res =
                        InformesComercialesDAOJDBC.getQueryPaginate(sql,
                            pParamQuery, this.getConnection(), hParameters);

                } catch (Exception e) {
                    logger.error("ERROR PAGINACION: ", e);
                    throw new SystemException(e);
                }
            } else {
                res = this.query(Map.class, sql, params);
            }

            return res;
        }

   
    public List < HashMap < String, Object > > obtenerVitrineoGeneral(
            String fechaDesde, String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria) {

            logger.info("INFORME Cotizaciones:");
            logger.info("DESDE: " + fechaDesde);
            logger.info("HASTA: " + fechaHasta);
            logger.info("RAMA:  " + rama);
            logger.info("SUBCATEGORIA:  " + subcategoria);

            String sql = SQL_OBTENER_VITRINEO_VEHICULO;
            
            if (rama != 0){
            	sql = sql + " and R.ID_RAMA = ?";
            }else {
            	sql = sql + " and R.ID_RAMA != ?";
            }
            if (subcategoria != 0){
            	sql = sql + " and SC.ID_SUBCATEGORIA = ?";
            }else {
            	sql = sql + " and SC.ID_SUBCATEGORIA != ?";
            }

            
            Object[] params = new Object[] { fechaDesde, fechaHasta, rama, subcategoria };

            List < HashMap < String, Object > > res =
                new ArrayList < HashMap < String, Object > >();

            if (esPaginado) {

                try {
                    ArrayList < Object > pParamQuery = new ArrayList < Object >();
                    pParamQuery.add(fechaDesde);
                    pParamQuery.add(fechaHasta);
                    pParamQuery.add(rama);
                    pParamQuery.add(subcategoria);
                    
                    HashMap < String, String > hParameters =
                        new HashMap < String, String >();

                    hParameters.put("getPageOnList", "Y");
                    hParameters.put("page", String.valueOf(numeroPagina));
                    hParameters.put("totalPerPage", "15");

                    res =
                        InformesComercialesDAOJDBC.getQueryPaginate(sql,
                            pParamQuery, this.getConnection(), hParameters);

                } catch (Exception e) {
                    logger.error("ERROR PAGINACION: ", e);
                    throw new SystemException(e);
                }
            } else {
                res = this.query(Map.class, sql, params);
            }

            return res;
        }
    
    
    /**
    � � * @param SQL
    � � * @param pParamQuery
    � � * @param pConex
    � � */
    private static ArrayList getQueryPaginate(String SQL,
        java.util.ArrayList pParamQuery, java.sql.Connection pConex,
        HashMap hParameters) throws Exception {
        ArrayList result = new ArrayList();
        ArrayList vQuery = new ArrayList();
        PreparedStatement stmt1 = null;
        PreparedStatement stmt2 = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;

        try {
            String getPageOnList = (String) hParameters.get("getPageOnList");
            String page = (String) hParameters.get("page");
            int iPage = 0;
            String totalPerPage = (String) hParameters.get("totalPerPage");
            int iTotalPerPage = 0;
            int desde = 0;
            int hasta = 0;

            if (page != null && !page.equals("")) {
                iPage = new Integer(page).intValue();
            }
            if (totalPerPage != null && !totalPerPage.equals("")) {
                iTotalPerPage = new Integer(totalPerPage).intValue();
                desde = (iPage - 1) * iTotalPerPage + 1;
                hasta = desde + iTotalPerPage;
            }

            /*Total de registros*/
            String SQLcount = SQL;
            int count = 0;
            //Quita todo antes del FROM
            int posFrom = SQLcount.toLowerCase().indexOf("from");
            if (posFrom > 0 && posFrom < SQL.length())
                SQLcount = SQLcount.substring(posFrom, SQLcount.length());

            //Quita despues de un ORDER BY
            int posOrderby = SQLcount.toLowerCase().indexOf("order by");
            if (posOrderby > 0 && posOrderby < SQLcount.length())
                SQLcount = SQLcount.substring(0, posOrderby);

            SQLcount = "SELECT count(1) " + SQLcount;
            stmt1 =
                pConex
                    .prepareStatement(SQLcount,
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
            setFillParameters(stmt1, pParamQuery);
            rs1 = stmt1.executeQuery();
            if (rs1.next())
                count = rs1.getInt(1);
            String pages =
                String.valueOf((count - 1) / new Long(totalPerPage).longValue()
                    + 1);

            if (getPageOnList != null && getPageOnList.equalsIgnoreCase("Y")) {
                if (page == null || new Integer(page).intValue() < 0)
                    page = "1";

                HashMap hDatos = new HashMap();
                hDatos.put("pages", pages);
                hDatos.put("rows", String.valueOf(count));

                int totalAtPage = 0;
                if (!pages.equals(page)) {
                    totalAtPage =
                        (new Integer(page).intValue() * new Integer(
                            totalPerPage).intValue());
                } else {
                    totalAtPage = count;
                }
                hDatos.put("totalAtPage", String.valueOf(totalAtPage));
                result.add(0, hDatos);
            } else {
                result.add(pages);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("ConnectionUtil.getQueryPaginate: " + SQL);
            }

            stmt2 =
                pConex.prepareStatement(SQL, ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            setFillParameters(stmt2, pParamQuery);
            rs2 = stmt2.executeQuery();

            for (int i = desde; i < hasta; i++) {
                if (rs2.absolute(i)) {
                    vQuery = getRowAbsResulset(rs2);
                    if (vQuery.size() > 0) {
                        HashMap hTemp = new HashMap();
                        hTemp = (HashMap) vQuery.get(0);
                        result.add(hTemp);
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs1 != null)
                rs1.close();
            if (rs2 != null)
                rs2.close();
            if (stmt1 != null)
                stmt1.close();
            if (stmt2 != null)
                stmt2.close();
        }
        return result;
    }

    public Map < String, Object > obtenerArchivoFactura(long id_factura) {

        Object[] params = new Object[] { id_factura };
        List < Map < String, Object >> res = null;

        res = this.query(Map.class, SQL_OBTENER_FACTURA, params);

        if (res != null && res.size() > 0) {
            try {
                Blob imagen = (Blob) res.get(0).get("archivo");
                InputStream is = imagen.getBinaryStream();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int cnt = is.read(buffer);
                while (cnt != -1) {
                    baos.write(buffer);
                    cnt = is.read(buffer);
                }
                is.close();

                res.get(0).put("archivo", baos.toByteArray());

                return res.get(0);
            } catch (SQLException e) {
                return null;
            } catch (IOException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
    � � * @param stmt
    � � * @param pParamQuery
    � � * @throws Exception
    � � */
    private static void setFillParameters(PreparedStatement stmt,
        java.util.ArrayList pParamQuery) throws Exception {
        int i = 1;

        try {
            for (int x = 0; x < pParamQuery.size(); x++) {
                Object oObjeto = (Object) pParamQuery.get(x);
                if (oObjeto instanceof Double) {
                    if (oObjeto != null)
                        stmt.setDouble(i++, ((Double) oObjeto).doubleValue());
                    else
                        stmt.setString(i++, null);
                } else if (oObjeto instanceof Float) {
                    if (oObjeto != null)
                        stmt.setFloat(i++, ((Float) oObjeto).floatValue());
                    else
                        stmt.setString(i++, null);
                } else if (oObjeto instanceof Integer) {
                    if (oObjeto != null)
                        stmt.setInt(i++, ((Integer) oObjeto).intValue());
                    else
                        stmt.setString(i++, null);
                } else if (oObjeto instanceof Long) {
                    if (oObjeto != null)
                        stmt.setLong(i++, ((Long) oObjeto).longValue());
                    else
                        stmt.setString(i++, null);
                } else if (oObjeto instanceof String) {
                    if (oObjeto != null)
                        stmt.setString(i++, (String) oObjeto);
                    else
                        stmt.setString(i++, null);
                } else if (oObjeto instanceof Timestamp) {
                    if (oObjeto != null)
                        stmt.setTimestamp(i++, (Timestamp) oObjeto);
                    else
                        stmt.setString(i++, null);
                } else if (oObjeto instanceof Date) {
                    if (oObjeto != null)
                        stmt.setTimestamp(i++, new java.sql.Timestamp(
                            ((java.util.Date) oObjeto).getTime()));
                    else
                        stmt.setString(i++, null);
                } else if (oObjeto instanceof Boolean) {
                    if (oObjeto != null)
                        stmt
                            .setBoolean(i++, ((Boolean) oObjeto).booleanValue());
                    else
                        stmt.setString(i++, null);
                } else {
                    if (oObjeto == null)
                        stmt.setObject(i++, null);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * TODO Describir m�todo getRowAbsResulset.
     * @param rs
     * @return
     * @throws Exception
     */
    private static ArrayList getRowAbsResulset(ResultSet rs) throws Exception {
        ArrayList result = new ArrayList();
        String llave = "";
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            int columnas = metaData.getColumnCount();

            if (!rs.isAfterLast()) {
                HashMap oRow = new HashMap();
                for (int column = 0; column < columnas; column++) {
                    llave = metaData.getColumnLabel(column + 1);

                    if (llave.indexOf(">") != -1) {
                        String llave1 = llave.substring(0, llave.indexOf(">"));
                        String llave2 =
                            llave.substring(llave.indexOf(">"), llave.length());
                        llave = llave1 + llave2;
                    } else {
                        llave = llave.toLowerCase();
                    }

                    if (rs.getObject(column + 1) != null) {
                        if (rs.getObject(column + 1) instanceof Clob) {
                            oRow.put(llave, rs.getClob(column + 1)
                                .getSubString(1,
                                    (int) rs.getClob(column + 1).length()));
                        } else
                            oRow.put(llave, rs.getObject(column + 1));
                    }
                }
                if (oRow.size() > 0) {
                    result.add(oRow);
                }
            }
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

	public boolean ingresarPromocionPrincipal(String principal, String tipo, String posicion,
			String nombre, String linkVer, String tracker, String imagen, Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, 
			String linkConocer, String linkCotizar, String idPlan) {
		  //boolean existe;
		  String id = principal;
		  String tipoS = tipo;
		  String posicionS = posicion;
		  Object[] parametros = new Object[] { id, tipoS };
		  String existe = String.valueOf(this.find(Map.class, SQL_OBTENER_PROMOCION_PRINCIPALBP, parametros));
		  if (existe.equalsIgnoreCase("null")){
			  Object[] params = {principal, tipo, posicion, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar,idPlan};
		      this.insertrut(SQL_INGRESA_PROMOCION_PRINCIPAL, params);
		  } else {
			  Object[] params = {principal, tipo, posicion, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar,Integer.parseInt(idPlan),id, tipoS,posicionS};
			  logger.info("Principal:" + principal);
			  logger.info("Tipo:" + tipo);
			  logger.info("Posicion:" + posicion);
			  logger.info("Nombre:"+ nombre);
			  logger.info("Link Ver:" + linkVer);
			  logger.info("Tracker:"+ tracker);
			  logger.info("Imagen:"+ imagen);
			  logger.info("Fecha Desde:" + fechaDesde);
			  logger.info("fechaHasta:" + fechaHasta);
			  logger.info("Tipo Banner:" + tipoBanner);
			  logger.info("Rama:" + rama);
			  logger.info("Link Conocer:" + linkConocer);
			  logger.info("Link Cotizar:" + linkCotizar);
			  logger.info("idPlan:" + idPlan);
			  this.update(SQL_ACTUALIZAR_PROMOCION_PRINCIPAL, params);
			  logger.info("SQL SEC:" + SQL_ACTUALIZAR_PROMOCION_PRINCIPAL);
			  logger.info("Principal:" + principal);
			  logger.info("Tipo:" + tipo);
			  logger.info("Posicion:" + posicion);
			  logger.info("Nombre:"+ nombre);
			  logger.info("Link Ver:" + linkVer);
			  logger.info("Tracker:"+ tracker);
			  logger.info("Imagen:"+ imagen);
			  logger.info("Fecha Desde:" + fechaDesde);
			  logger.info("fechaHasta:" + fechaHasta);
			  logger.info("Tipo Banner:" + tipoBanner);
			  logger.info("Rama:" + rama);
			  logger.info("Link Conocer:" + linkConocer);
			  logger.info("Link Cotizar:" + linkCotizar);
			  logger.info("idPlan:" + idPlan);
			  logger.info("Promocion Principal Actualizada");
		  }
	      return true; 
	}
	
	
	public boolean ingresarPromocionSecundaria(String principal, String tipo, String posicion,
			String nombre, String linkVer, String tracker, String imagen, Date fechaDesde, Date fechaHasta, String tipoBanner, String rama, 
			String linkConocer, String linkCotizar, String idPlan) {
		  
		  String id = principal;
		  String tipoS = tipo;
		  Object[] parametros = new Object[] { id, tipoS };
		  String existe = String.valueOf(this.find(Map.class, SQL_OBTENER_PROMOCION_SECUNDARIABP, parametros));
		  if (existe.equalsIgnoreCase("null")){
			  Object[] params = {principal, tipo, posicion, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar, Integer.parseInt(idPlan)};
		      this.insertrut(SQL_INGRESA_PROMOCION_SECUNDARIA, params);
		  } else {
			  logger.info("Principal:" + principal);
			  logger.info("Tipo:" + tipo);
			  logger.info("Posicion:" + posicion);
			  logger.info("Nombre:"+ nombre);
			  logger.info("Link Ver:" + linkVer);
			  logger.info("Tracker:"+ tracker);
			  logger.info("Imagen:"+ imagen);
			  logger.info("Fecha Desde:" + fechaDesde);
			  logger.info("fechaHasta:" + fechaHasta);
			  logger.info("Tipo Banner:" + tipoBanner);
			  logger.info("Rama:" + rama);
			  logger.info("Link Conocer:" + linkConocer);
			  logger.info("Link Cotizar:" + linkCotizar);
			  logger.info("idPlan:" + idPlan);
			  Object[] params = {principal, tipo, posicion, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar, Integer.parseInt(idPlan),id, tipoS};
			  this.update(SQL_ACTUALIZAR_PROMOCION_SECUNDARIA, params);
			  logger.info("SQL SEC:" + SQL_ACTUALIZAR_PROMOCION_SECUNDARIA);
			  logger.info("Principal:" + principal);
			  logger.info("Tipo:" + tipo);
			  logger.info("Posicion:" + posicion);
			  logger.info("Nombre:"+ nombre);
			  logger.info("Link Ver:" + linkVer);
			  logger.info("Tracker:"+ tracker);
			  logger.info("Imagen:"+ imagen);
			  logger.info("Fecha Desde:" + fechaDesde);
			  logger.info("fechaHasta:" + fechaHasta);
			  logger.info("Tipo Banner:" + tipoBanner);
			  logger.info("Rama:" + rama);
			  logger.info("Link Conocer:" + linkConocer);
			  logger.info("Link Cotizar:" + linkCotizar);
			  logger.info("idPlan:" + idPlan);
			  logger.info("Promocion Secundaria Actualizada");
		  }
	      return true; 
	}

	public List<HashMap<String, Object>> obtenerPromocionesPrincipalBP(
			String tipoS) {
		Object[] params = new Object[] { tipoS };

        List < HashMap < String, Object > > res =
            new ArrayList < HashMap < String, Object > >();

        return res = this.query(Map.class, SQL_OBTENER_PROMOCION_PRINCIPALBP, params);

	}
	
	public List<Map<String, Object>> obtenerPromocionesBO() {
		//Object[] params = new Object[] { rama, subcategoria };

		List < Map < String, Object >> res = null;

		res = this.query(Map.class,SQL_OBTENER_PROMOCION_BO, null);
        return res;

	}
	
	public List<Map<String, Object>> obtenerPromocionesBOPrincipal() {
		//Object[] params = new Object[] { rama, subcategoria };

		List < Map < String, Object >> res = null;
		res = this.query(Map.class,SQL_OBTENER_PROMOCION_BO_PRINCIPAL, null);
        return res;

	}



}
