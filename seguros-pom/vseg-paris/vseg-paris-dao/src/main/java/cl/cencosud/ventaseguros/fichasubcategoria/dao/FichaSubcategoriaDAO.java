package cl.cencosud.ventaseguros.fichasubcategoria.dao;

import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida;
import cl.cencosud.ventaseguros.common.Ficha;
import cl.cencosud.ventaseguros.common.LegalFicha;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * DAO de Ficha subcategoria.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public interface FichaSubcategoriaDAO extends BaseDAO {

    List < Map < String, ? >> obtenerFichasSubcategoria(int idRama,
        int idSubcategoria);

    List < Map < String, ? >> obtenerPlanesPromocion(int idRama,
        int idSubcategoria);

    long agregarFichaProducto(Ficha ficha);
    
    void agregarLegalFichaProducto(LegalFicha legalFicha);

    void actualizarFichaProducto(Ficha ficha);
    
    void actualizarLegalFichaProducto(LegalFicha legalFicha);

    void subirArchivo(long id_ficha, byte[] bios, String tipo);

    Map < String, Object > obtenerArchivo(long id_ficha, String tipo);

    long buscarArchivo(long id_ficha, String tipo);

    void actualizarArchivo(long id_archivo, long id_ficha, byte[] bios,
        String tipo);

    List < PlanLeg > obtenerPlanes(int idSubcategoria);

    Ficha obtenerFicha(Long id_ficha);
    
    //CBM
    LegalFicha obtenerLegalFicha(Long id_ficha) ;

    PlanLeg obtenerDatosPlan(long idPlan);

    boolean existePrimaUnica(long idPlan);

    RestriccionVida obtenerRestriccionVida(long idPlan);

    long agregarPrimaUnica(PlanLeg plan);

    void eliminarPrimaUnica(PlanLeg plan);

    long agregarRestriccionVida(RestriccionVida restriccion);

    void actualizaRestriccionVida(RestriccionVida restriccion);

    void actualizarPlanLeg(PlanLeg plan);
    
    long agregarPlanCompannia(PlanLeg plan);

    void actualizarPlanCompannia(PlanLeg plan);

}
