package cl.cencosud.ventaseguros.mantenedorclientes.dao.ws;

import cl.cencosud.bigsa.gestion.formapago.grabar.webservice.client.was.WsBigsaActulizaFormaPagoImplDelegate;
import cl.cencosud.bigsa.gestion.formapago.grabar.webservice.client.was.WsBigsaActulizaFormaPagoService;
import cl.cencosud.bigsa.gestion.solicitud.webservice.client.was.SolicitudVO;
import cl.cencosud.bigsa.gestion.solicitud.webservice.client.was.WsBigsaSolicitudes;
import cl.cencosud.bigsa.gestion.solicitud.webservice.client.was.WsBigsaSolicitudesImplDelegate;
import cl.cencosud.bigsa.poliza.consultahtml.webservice.client.was.WsBigsaPolizaHtml;
import cl.cencosud.bigsa.poliza.consultahtml.webservice.client.was.WsBigsaPolizaHtmlImplDelegate;
import cl.cencosud.bigsa.poliza.consultapdf.webservice.client.was.WsBigsaPolizaPDFDelegate;
import cl.cencosud.bigsa.poliza.consultapdf.webservice.client.was.WsBigsaPolizaPDFService;
import cl.cencosud.siisa.consultaced.webservice.cliente.was.detallecedula.DetalleCedula;
import cl.cencosud.siisa.consultaced.webservice.cliente.was.detallecedula.Salida;
import cl.cencosud.siisa.consultaced.webservice.cliente.was.services.ConsultaVerificacionCedulaPub;
import cl.cencosud.siisa.consultaced.webservice.cliente.was.services.ConsultaVerificacionCedulaPubService;
import cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion;
import cl.cencosud.ventaseguros.common.config.VSPConfig;
import cl.cencosud.ventaseguros.common.exception.ObtenerArchivosException;
import cl.cencosud.ventaseguros.common.exception.SolicitudException;
import cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.MantenedorClientesWSDAO;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.VSPConfigDAO;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.util.resource.ResourceLeakUtil;
import com.tinet.exceptions.system.SystemException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MantenedorClientesWSDAOWSWAS extends BaseConfigurable
  implements MantenedorClientesWSDAO
{
  private static Log logger = LogFactory.getLog(MantenedorClientesWSDAOWSWAS.class);
  private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";
  private static final String WSDL_ICOMERCIAL = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.verificacionCedula";
  private static final int SEGUROS_VIGENTES = 4;
  private static final String ICOMERCIAL_USER_CEDULA = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWAS.ICOMERCIAL_USER_CEDULA";
  private static final String ICOMERCIAL_PSW_CEDULA = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWAS.ICOMERCIAL_PSW_CEDULA";
  private static final String ICOMERCIAL_NIVEL_CEDULA = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWAS.ICOMERCIAL_NIVEL_CEDULA";
  private static final String ICOMERCIAL_SRCEI_CEDULA = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWAS.ICOMERCIAL_SRCEI_CEDULA";
  private static final String WSDL_ACTUALIZAR_FORMA_PAGO = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.actualizaFormaPago";
  private static final String WSDL_POLIZA_PDF = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.pdf";
  private static final String WSDL_POLIZA_HTML = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.html";
  private static final String WSDL_SOLICITUDES = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.solicitudes";
  private static final int LENGTH_RUT_WS_ICOMERCIAL = 10;

  public void close()
  {
  }

  public Class getDAOInterface()
  {
    return null;
  }

  public void setDAOInterface(Class interfazDAO)
  {
  }

  private <T> T inicializarWS(T stub, Map<String, ?> param)
    throws SystemException
  {
    if (stub == null) {
      throw new NullPointerException("Debe especificar un stub no nulo.");
    }
    if (param == null) {
      logger.warn("No se especific� URL destino del servicio.");
    }
    if (param.containsKey("SERVICE_ENDPOINT_KEY")) {
      Object endpoint = param.get("SERVICE_ENDPOINT_KEY");
      if (logger.isDebugEnabled())
        logger.debug("Intentando establecer URL destino: " + endpoint);
      try
      {
        Method method = 
          stub.getClass().getMethod("_setProperty", new Class[] { String.class, 
          Object.class });
        logger.debug("M�todo encontrado. Invocando...");
        method.invoke(stub, new Object[] { "javax.xml.rpc.service.endpoint.address", 
          param.get("SERVICE_ENDPOINT_KEY") });
        logger.debug("Propiedad establecida exitosamente.");
      } catch (SecurityException se) {
        throw new SystemException(se);
      } catch (NoSuchMethodException nsme) {
        throw new SystemException(nsme);
      } catch (IllegalArgumentException iae) {
        throw new SystemException(iae);
      } catch (IllegalAccessException iae) {
        throw new SystemException(iae);
      } catch (InvocationTargetException ite) {
        throw new SystemException(ite);
      }
    } else {
      logger.warn("No se especific� URL destino del servicio.");
    }
    return stub;
  }

  private void obtenerExcepcionWS(RemoteException e)
    throws SolicitudException
  {
    if ((e.detail instanceof SOAPFaultException)) {
      SOAPFaultException sfe = (SOAPFaultException)e.detail;
      Detail detail = sfe.getDetail();
      if (detail != null) {
        SolicitudException vex = 
          obtenerErrorValorizacion(detail.getChildNodes());
        if (vex != null) {
          logger.debug("Error de negocio durante invocacion a WS.", 
            vex);
          throw vex;
        }
      }
    }
  }

  private SolicitudException obtenerErrorValorizacion(NodeList nodes)
  {
    if (nodes == null) {
      return null;
    }
    SolicitudException vex = null;
    String message = null;
    String codigo = null;
    for (int i = 0; i < nodes.getLength(); i++) {
      Node item = nodes.item(i);
      vex = obtenerErrorValorizacion(item.getChildNodes());
      if (logger.isDebugEnabled()) {
        logger.debug(i + ") item : " + item);
        logger.debug(i + ") name : " + item.getNodeName());
        logger.debug(i + ") value: " + item.getNodeValue());
        logger.debug(i + ") eie  : " + vex);
      }
      if (vex == null) {
        if ("mensaje".equals(item.getNodeName()))
          message = item.getFirstChild().getNodeValue();
        else if ("codigo".equals(item.getNodeName()))
          codigo = item.getFirstChild().getNodeValue();
      }
      else {
        return vex;
      }
    }
    if ((message != null) && (codigo != null)) {
      logger.debug("Descripcion encontrada. Se crea excepcion.");
      vex = new SolicitudException(message, Integer.valueOf(codigo).intValue());
    }
    return vex;
  }

  public SolicitudCotizacion[] obtenerSolicitudes(Long rutCliente)
    throws SolicitudException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    logger.info("WS: Obtener Solicitudes");
    logger.info("Antes de intanciar IMPL.");

    logger.info("Antes de Ininicalizar el servicio.");

    Context context = null;

    SolicitudVO[] result = (SolicitudVO[])null;
    SolicitudCotizacion[] res = (SolicitudCotizacion[])null;
    try
    {
      context = new InitialContext();
      WsBigsaSolicitudes svc = 
        (WsBigsaSolicitudes)context
        .lookup("java:comp/env/service/WsBigsaSolicitudes");

      WsBigsaSolicitudesImplDelegate ws = 
        svc.getWsBigsaSolicitudesImplPort(new URL(dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.solicitudes")));

      String sRut = String.valueOf(rutCliente);
      int iRut = Integer.parseInt(sRut);

      result = ws.obtenerListaSolicitudes(iRut, 4);

      if (result != null) {
        res = new SolicitudCotizacion[result.length];
        for (int i = 0; i < result.length; i++) {
          SolicitudVO solicitudVO = result[i];
          try
          {
            res[i] = new SolicitudCotizacion();
            BeanUtils.copyProperties(res[i], solicitudVO);
          } catch (IllegalAccessException e) {
            logger.error("Error al copiar objeto", e);
          } catch (InvocationTargetException e) {
            logger.error("Error al copiar objeto", e);
          }
        }
        Arrays.sort(res);
      } else {
        res = new SolicitudCotizacion[0];
      }
    }
    catch (NamingException e) {
      logger.error("Error durante invocando WS obtenerListaSolicitudes.", 
        e);
      throw new SystemException(e);
    } catch (cl.cencosud.bigsa.gestion.solicitud.webservice.client.was.ErrorInternoException e) {
      logger.error("Error durante invocando WS obtenerListaSolicitudes.", 
        e);
      throw new SystemException(e);
    } catch (RemoteException e) {
      logger.error("error remoto obtenerListaSolicitudes", e);
      if (e.getMessage().contains("NO REGISTRA")){
    	  logger.info("ERROR CONTROLADO: EL CLIENTE NO REGISTRA SOLICITUDES EN EL SISTEMA");
      }else if (e.getMessage().contains("NO EST&#193; REGISTRADO")){
          logger.info("ERROR CONTROLADO: EL CLIENTE NO ESTA REGISTRADO");
      }else{
    	  throw new SystemException(e);
      }
    }
    catch (ServiceException e) {
      logger.error("Error durante invocando WS obtenerListaSolicitudes.", 
        e);
      throw new SystemException(e);
    } catch (MalformedURLException e) {
      logger.error("Error durante invocando WS obtenerListaSolicitudes.", 
        e);
      throw new SystemException(e);
    } finally {
      ResourceLeakUtil.close(context);
      ResourceLeakUtil.close(dao);
    }

    return res;
  }

  public byte[] obtenerPolizaPdf(int rutCliente, int numeroSolicitud)
    throws BusinessException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    logger.debug("WS: Obtener PDF");
    logger.debug("Antes de intanciar IMPL.");

    logger.debug("Antes de Ininicalizar el servicio.");

    Context context = null;

    byte[] result = (byte[])null;
    try
    {
      context = new InitialContext();
      WsBigsaPolizaPDFService svc = 
        (WsBigsaPolizaPDFService)context
        .lookup("java:comp/env/service/WsBigsaPolizaPDFService");

      WsBigsaPolizaPDFDelegate ws = 
        svc
        .getWsBigsaPolizaPDFPort(new URL(dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.pdf")));

      result = ws.obtenerPolizaPDF(rutCliente, numeroSolicitud);
    }
    catch (NamingException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } catch (RemoteException e) {
      logger.error("error remoto", e);
      throw new ObtenerArchivosException(
        "cl.cencosud.ventaseguros.common.exception.ARCHIVO_NO_ENCONTRADO", 
        new Object[] { "poliza" });
    } catch (cl.cencosud.bigsa.poliza.consultapdf.webservice.client.was.ErrorInternoException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } catch (ServiceException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } catch (MalformedURLException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } finally {
      ResourceLeakUtil.close(context);
      ResourceLeakUtil.close(dao);
    }

    return result;
  }

  public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud)
    throws BusinessException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    logger.debug("WS: Obtener PDF");
    logger.debug("Antes de intanciar IMPL.");

    logger.debug("Antes de Ininicalizar el servicio.");

    Context context = null;

    byte[] result = (byte[])null;
    try
    {
      context = new InitialContext();
      WsBigsaPolizaHtml svc = 
        (WsBigsaPolizaHtml)context
        .lookup("java:comp/env/service/WsBigsaPolizaHtml");

      WsBigsaPolizaHtmlImplDelegate ws = 
        svc
        .getWsBigsaPolizaHtmlImplPort(new URL(dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.html")));

      result = ws.obtenerCotizacionHTML(rutCliente, numeroSolicitud);
    }
    catch (NamingException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } catch (RemoteException e) {
      logger.error("error remoto", e);
      throw new ObtenerArchivosException(
        "cl.cencosud.ventaseguros.common.exception.ARCHIVO_NO_ENCONTRADO", 
        new Object[] { "poliza" });
    } catch (cl.cencosud.bigsa.poliza.consultahtml.webservice.client.was.ErrorInternoException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } catch (ServiceException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } catch (MalformedURLException e) {
      logger.error("Error durante invocando WS obtenerPolizaPdf.", e);
      throw new SystemException(e);
    } finally {
      ResourceLeakUtil.close(context);
      ResourceLeakUtil.close(dao);
    }

    return result;
  }

  public boolean validarSerieRut(String rutDv, String nroSerie)
    throws ValidacionUsuarioException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    logger.info("WS: Validar serie del rut");

    VSPConfig vsp = VSPConfig.getInstance();
    boolean result = false;
    Context context = null;
    try
    {
      logger.info("Antes de Ininicalizar el servicio.");

      context = new InitialContext();
      ConsultaVerificacionCedulaPubService svc = 
        (ConsultaVerificacionCedulaPubService)context
        .lookup("java:comp/env/service/ConsultaVerificacionCedulaPubService");
      String wsdl = dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.verificacionCedula");
      logger.info("LLAMAR A: " + wsdl);
      ConsultaVerificacionCedulaPub ws = svc.getVerificacionCedula(new URL(wsdl));

      String descripcion = "";

      logger.info("Ejecutando WS IComercial getCedulasBloqueadas ");

      logger.info("DATOS ENVIADOS");

      logger.info("rutDv: " + rutDv);
      logger.info("nroSerie: " + nroSerie);

      logger.info("descripcion: " + descripcion);
      logger.info("FIN DATOS");

      Salida out = 
        ws.getVerificacionCedula(rutDv.substring(0, rutDv.length() - 1), rutDv.substring(rutDv.length() - 1), nroSerie, "", "", "", "77218570", "7", "J161");

      DetalleCedula resumen = out.getDetalleCedula();

      if (resumen.getCedula() == null) {
        logger.info("No existe detalle para el rut " + rutDv + 
          "  -  NroSerie " + nroSerie);
      }
      else
      {
        logger.info("RUT           : " + rutDv);
        logger.info("NroSerie      : " + nroSerie);

        logger.info("Estado Cedula : " + resumen.getEstado());
        logger.info("Motivo        : " + resumen.getRazon());
        if ("V".equalsIgnoreCase(resumen.getEstado())) {
          logger.info("CEDULA: " + rutDv + " VALIDA");
          result = true;
        } else {
          logger.info("CEDULA: " + rutDv + " NO VALIDA");
        }
      }
    }
    catch (NamingException e)
    {
      logger.error(
        "Naming Error durante la invocaci�n del WS de ConsultaVerificacionCedulaPubService ", e);
      throw new SystemException(e);
    } catch (MalformedURLException e) {
      logger.error(
        "Malformed Error durante la invocaci�n del WS de ConsultaVerificacionCedulaPubService ", 
        e);
      throw new SystemException(e);
    } catch (ServiceException e) {
      logger.error(
        "Service Error durante la invocaci�n del WS de ConsultaVerificacionCedulaPubService ", e);
      throw new SystemException(e);
    } catch (RemoteException e) {
      logger.error(
        "Runtime Error durante la invocaci�n del WS de ConsultaVerificacionCedulaPubService ", e);

      throw new ValidacionUsuarioException(
        "cl.cencosud.ventaseguros.common.exception.NUMERO_SERIE_RUT_NO_VALIDO", 
        ValidacionUsuarioException.SIN_ARGUMENTOS);
    } finally {
      ResourceLeakUtil.close(context);
      ResourceLeakUtil.close(dao);
    }

    return result;
  }

  private String formatearRutWSIComercial(String rutConDv)
  {
    int diff = 10 - rutConDv.length();
    String rutFormatter = rutConDv;
    while (diff > 0) {
      rutFormatter = "0" + rutFormatter;
      diff--;
    }
    return rutFormatter;
  }

  public boolean actualizarFormaPago(int rutCliente, int numeroCotizacion, int codigoFormaPago)
    throws BusinessException
  {
    logger.debug("WS: Actualizar Forma de Pago");

    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    boolean result = false;
    Context context = null;
    try
    {
      logger.debug("Antes de Ininicalizar el servicio.");

      context = new InitialContext();
      WsBigsaActulizaFormaPagoService svc = 
        (WsBigsaActulizaFormaPagoService)context
        .lookup("java:comp/env/service/WsBigsaActulizaFormaPagoService");
      String wsdl = dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.actualizaFormaPago");
      WsBigsaActulizaFormaPagoImplDelegate ws = 
        svc.getWsBigsaActulizaFormaPagoImplPort(new URL(wsdl));
      logger.info("LLAMAR A: " + wsdl);

      logger.info("Ejecutando WS Actualizar forma de pago ");

      logger.info("rutCliente: " + rutCliente);
      logger.info("numeroCotizacion: " + numeroCotizacion);
      logger.info("codigoFormaPago: " + codigoFormaPago);

      result = 
        ws.actualizarFormaPago(rutCliente, numeroCotizacion, 
        codigoFormaPago);

      logger.info("Respuesta: " + result);
    }
    catch (NamingException e) {
      logger
        .error(
        "Naming Error durante invocando WS WsBigsaActulizaFormaPagoService.", 
        e);
      throw new SystemException(e);
    } catch (RemoteException e) {
      logger.error("Remote error remoto", e);
      throw new SystemException(e.detail);
    } catch (cl.cencosud.bigsa.gestion.formapago.grabar.webservice.client.was.ErrorInternoException e) {
      logger
        .error(
        "Interno Error durante invocando WS WsBigsaActulizaFormaPagoService.", 
        e);
      throw new SystemException(e);
    } catch (ServiceException e) {
      logger
        .error(
        "Service Error durante invocando WS WsBigsaActulizaFormaPagoService.", 
        e);
      throw new SystemException(e);
    } catch (MalformedURLException e) {
      logger
        .error(
        "MalFormed Error durante invocando WS WsBigsaActulizaFormaPagoService.", 
        e);
      throw new SystemException(e);
    } finally {
      ResourceLeakUtil.close(context);
      ResourceLeakUtil.close(dao);
    }

    return result;
  }
}