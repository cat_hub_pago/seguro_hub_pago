package cl.cencosud.ventaseguros.fichasubcategoria.dao.jdbc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida;
import cl.cencosud.ventaseguros.common.Ficha;
import cl.cencosud.ventaseguros.common.LegalFicha;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.fichasubcategoria.dao.FichaSubcategoriaDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

/**
 * Encargado de manejar las consultas de Ficha subcategoria.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class FichaSubcategoriaDAOJDBC extends ManagedBaseDAOJDBC implements
    FichaSubcategoriaDAO {

    private static final String SQL_FICHA_SUBCATEGORIA =
        "select f.id_ficha, f.id_subcategoria, f.id_plan, "
            + "f.nombre_comercial, f.descripcion_ficha, f.descripcion_pagina, "
            + "f.es_subcategoria, f.estado_ficha, f.monto_uf, f.id_pais, "
            + "f.fecha_creacion, f.fecha_modificacion, s.titulo_subcategoria,"
            + " p.nombre, r.titulo_rama from SUBCATEGORIA s, RAMA r, {OJ FICHA"
            + " f LEFT OUTER JOIN plan_leg p ON p.id_plan = "
            + "f.id_plan } where f.id_subcategoria=s.id_subcategoria "
            + "and s.id_rama=r.id_rama ";

    private static final String SQL_PLAN_PROMOCION =
        "select f.id_ficha, f.id_subcategoria, f.id_plan, "
            + "f.nombre_comercial, f.descripcion_ficha, f.descripcion_pagina, "
            + "f.es_subcategoria, f.estado_ficha, f.monto_uf, f.id_pais, "
            + "f.fecha_creacion, f.fecha_modificacion, s.titulo_subcategoria,"
            + " p.nombre, r.titulo_rama, s.id_tipo from SUBCATEGORIA s, RAMA r, {OJ FICHA"
            + " f LEFT OUTER JOIN plan_leg p ON p.id_plan = "
            + "f.id_plan } where f.id_subcategoria=s.id_subcategoria "
            + "and s.id_rama=r.id_rama and f.es_subcategoria=0 and f.ESTADO_FICHA=1 ";

    private static final String SQL_AGREGAR_FICHA =
        "insert into FICHA (id_ficha, id_subcategoria, id_plan, "
            + "nombre_comercial, descripcion_ficha, descripcion_pagina, "
            + "es_subcategoria, estado_ficha, monto_uf, id_pais, "
            + "fecha_creacion, fecha_modificacion) values "
            + "(ficha_seq.nextval, ?, ?, ?, ?, ?, ?, ?, ?, 1, sysdate, "
            + "sysdate)";
    
	private static final String SQL_AGREGAR_LEGAL_FICHA =  
			"insert into texto_legal_ficha ( id_ficha , texto_legal , activo ) values "
            + "(?, ?, ?)";


    private static final String SQL_ACTUALIZAR_FICHA =
        "update FICHA set nombre_comercial=?, descripcion_ficha=?, "
            + "descripcion_pagina=?, estado_ficha=?, monto_uf=?, "
            + "fecha_modificacion=sysdate where id_ficha = ?";

	private static final String SQL_ACTUALIZAR_LEGAL_FICHA = 
			 "update texto_legal_ficha set texto_legal  = ? , activo = ? where id_ficha = ?";

    
    private static final String SQL_AGREGAR_IMAGEN =
        "insert into IMAGEN_FICHA (id_imagen, id_ficha, imagen, "
            + "fecha_creacion, fecha_modificacion, id_pais) values "
            + "(imagen_ficha_seq.nextval, ?, ?, sysdate, sysdate, 1)";

    private static final String SQL_AGREGAR_BENEFICIO =
        "insert into BENEFICIO_FICHA (id_ventaja, id_ficha, html, "
            + "fecha_creacion, fecha_modificacion, id_pais) values "
            + "(beneficio_ficha_seq.nextval, ?, ?, sysdate, sysdate, 1)";

    private static final String SQL_AGREGAR_COBERTURA =
        "insert into COBERTURA_FICHA (id_cobertura, id_ficha, html, "
            + "fecha_creacion, fecha_modificacion, id_pais) values "
            + "(cobertura_ficha_seq.nextval, ?, ?, sysdate, sysdate, 1)";

    private static final String SQL_AGREGAR_ASPECTO_LEGAL =
        "insert into ASPECTO_LEGAL_FICHA (id_aspecto, id_ficha, html, "
            + "fecha_creacion, fecha_modificacion, id_pais) values "
            + "(aspecto_legal_ficha_seq.nextval, ?, ?, sysdate, sysdate, 1)";

    private static final String SQL_AGREGAR_EXCLUSION =
        "insert into EXCLUSION_FICHA (id_exclusion, id_ficha, html, "
            + "fecha_creacion, fecha_modificacion, id_pais) values "
            + "(exclusion_ficha_seq.nextval, ?, ?, sysdate, sysdate, 1)";

    private static final String SQL_BUSCAR_IMAGEN =
        "select id_imagen as id from IMAGEN_FICHA where id_ficha = ?";

    private static final String SQL_BUSCAR_BENEFICIO =
        "select id_ventaja as id from BENEFICIO_FICHA where id_ficha = ?";

    private static final String SQL_BUSCAR_COBERTURA =
        "select id_cobertura as id from COBERTURA_FICHA where id_ficha = ?";

    private static final String SQL_BUSCAR_ASPECTO_LEGAL =
        "select id_aspecto as id from ASPECTO_LEGAL_FICHA where id_ficha = ?";

    private static final String SQL_BUSCAR_EXCLUSION =
        "select id_exclusion as id from EXCLUSION_FICHA where id_ficha = ?";

    private static final String SQL_ACTUALIZAR_IMAGEN =
        "update IMAGEN_FICHA set imagen=?, fecha_modificacion=sysdate where "
            + "id_imagen = ?";

    private static final String SQL_ACTUALIZAR_BENEFICIO =
        "update BENEFICIO_FICHA set html=?, fecha_modificacion=sysdate where "
            + "id_ventaja = ?";

    private static final String SQL_ACTUALIZAR_COBERTURA =
        "update COBERTURA_FICHA set html=?, fecha_modificacion=sysdate where "
            + "id_cobertura = ?";

    private static final String SQL_ACTUALIZAR_ASPECTO_LEGAL =
        "update ASPECTO_LEGAL_FICHA set html=?, fecha_modificacion=sysdate "
            + "where id_aspecto = ?";

    private static final String SQL_ACTUALIZAR_EXCLUSION =
        "update EXCLUSION_FICHA set html=?, fecha_modificacion=sysdate where "
            + "id_exclusion = ?";

    private static final String SQL_OBTENER_PLANES =
        "select pla.id_plan, pla.CODIGO_TIPO_PROD_LEG,"
            + " pla.CODIGO_SUB_TIPO_PROD_LEG, pla.CODIGO_PRODUCTO_LEG, "
            + "pla.CODIGO_PLAN_LEG, pla.NOMBRE, pla.ID_PAIS, pla.ES_ACTIVO, "
            + "pla.COMPANNIA, pla.FECHA_CREACION, pla.FECHA_MODIFICACION from "
            + "SUBCATEGORIA s, PRODUCTO p, PRODUCTO_LEG pl, PLAN_LEG pla where "
            + "s.ID_SUBCATEGORIA = ? and s.ID_SUBCATEGORIA = p.ID_SUBCATEGORIA "
            + "and p.ID_PRODUCTO_LEG = pl.ID_PRODUCTO and "
            + "pl.CODIGO_TIPO_PROD_LEG = pla.CODIGO_TIPO_PROD_LEG and "
            + "pl.CODIGO_SUB_TIPO_PROD_LEG = pla.CODIGO_SUB_TIPO_PROD_LEG and "
            + "pl.CODIGO_PRODUCTO_LEG = pla.CODIGO_PRODUCTO_LEG "
            + "order by pla.nombre";

    //CBM
    
	private static final String SQL_OBTENER_LEGAL =  
			"select t.id_ficha, t.texto_legal ,t.activo"
			+" FROM texto_legal_ficha t "
            + "where t.id_ficha = ? ";
    
    
    private static final String SQL_OBTENER_FICHA =
        "select f.id_ficha, f.id_subcategoria, f.id_plan, "
            + "f.nombre_comercial, f.descripcion_ficha, f.descripcion_pagina, "
            + "f.es_subcategoria, f.estado_ficha, f.monto_uf, f.id_pais, "
            + "f.fecha_creacion, f.fecha_modificacion FROM FICHA f "
            + "where f.id_ficha = ? ";

    private static final String SQL_OBTENER_IMAGEN =
        "select id_imagen, id_ficha, imagen as archivo, fecha_creacion, "
            + "fecha_modificacion, id_pais from IMAGEN_FICHA "
            + "where id_ficha = ?";

    private static final String SQL_OBTENER_BENEFICIO =
        "select id_ventaja, id_ficha, html as archivo, id_pais, "
            + "fecha_creacion, fecha_modificacion from BENEFICIO_FICHA "
            + "where id_ficha = ?";

    private static final String SQL_OBTENER_COBERTURA =
        "select id_cobertura, id_ficha, html as archivo, fecha_creacion, "
            + "fecha_modificacion, id_pais from COBERTURA_FICHA "
            + "where id_ficha = ?";

    private static final String SQL_OBTENER_ASPECTO_LEGAL =
        "select id_aspecto, id_ficha, html as archivo, fecha_creacion, "
            + "fecha_modificacion, id_pais from ASPECTO_LEGAL_FICHA "
            + "where id_ficha = ?";

    private static final String SQL_OBTENER_EXCLUSION =
        "select id_exclusion, id_ficha, html as archivo, fecha_creacion, "
            + "fecha_modificacion, id_pais from EXCLUSION_FICHA "
            + "where id_ficha = ?";

    private static final String SQL_OBTENER_DATOS_PLAN =
        "SELECT P3.ID_PLAN AS id_plan_vseg, nvl(P4.nombre_cia_leg, 'CIA') as compannia, P3.NOMBRE as nombre, "
            + "P3.CODIGO_PLAN_LEG as id_plan, p4.CODIGO_CIA_INSPECCION as codigoCompanniaInspeccion, "
            + "P3.INSPECCIONBSP as inspeccionVehi, P3.MAX_DIAS_INI_VIG as maxDiaIniVig, "
            + "P3.SOLICITA_NUEVO_USADO as solicitaAutoNuevo, P3.SOLICITA_FACTURA as solicitaFactura, "
            + "P3.FORMA_PAGO as formaPago, "
            + "P3.CODIGO_TIPO_PROD_LEG as codigo_tipo_prod_leg, "
            + "P3.CODIGO_SUB_TIPO_PROD_LEG as codigo_sub_tipo_prod_leg, "
            + "P3.CODIGO_PRODUCTO_LEG as codigo_producto_leg, "
            + "P4.CODIGO_CIA_LEG as codigo_cia_leg "
            + "FROM PRODUCTO P1, PRODUCTO_LEG P2, {OJ PLAN_LEG P3 LEFT OUTER JOIN PLAN_COMPANNIA p4 "
            + " ON P3.CODIGO_TIPO_PROD_LEG = P4.CODIGO_TIPO_PROD_LEG "
            + "AND P3.CODIGO_SUB_TIPO_PROD_LEG = P4.CODIGO_SUB_TIPO_PROD_LEG "
            + "AND P3.CODIGO_PRODUCTO_LEG = P4.CODIGO_PRODUCTO_LEG "
            + "AND P3.CODIGO_PLAN_LEG = P4.CODIGO_PLAN_LEG} "
            + "WHERE P3.ID_PLAN = ? "
            + "AND P1.ID_PRODUCTO_LEG = P2.ID_PRODUCTO "
            + "AND P2.CODIGO_TIPO_PROD_LEG = P3.CODIGO_TIPO_PROD_LEG "
            + "AND P2.CODIGO_SUB_TIPO_PROD_LEG = P3.CODIGO_SUB_TIPO_PROD_LEG "
            + "AND P2.CODIGO_PRODUCTO_LEG = P3.CODIGO_PRODUCTO_LEG AND P1.ES_ACTIVO = 1";

    private static final String SQL_EXISTE_PRIMA_UNICA =
        "SELECT COUNT(PRI.ID_PRIMA) as existeprima FROM PLAN_LEG_PRIMA PRI, PLAN_LEG PL "
            + "WHERE PRI.CODIGO_TIPO_PROD_LEG = PL.CODIGO_TIPO_PROD_LEG "
            + "AND PRI.CODIGO_SUB_TIPO_PRO_LEG = PL.CODIGO_SUB_TIPO_PROD_LEG "
            + "AND PRI.CODIGO_PRODUCTO_LEG = PL.CODIGO_PRODUCTO_LEG "
            + "AND PRI.CODIGO_PLAN_LEG = PL.CODIGO_PLAN_LEG "
            + "AND PRI.ID_PAIS = PL.ID_PAIS " + "AND PL.ID_PLAN = ? ";

    private static final String SQL_OBTENER_RESTRICCION_VIDA =
        "select ID_RESTRICCION as idRestriccion, ID_PLAN as idPlan, REQ_GRUPO_FAMILIAR "
            + "as requiereGrupoFamiliar, REQ_CAPITAL as requiereCapital, EDAD_MIN_INGRESO as "
            + "edadMinimaIngreso, EDAD_MAX_HIJO as edadMaximaHijo, REQ_VALIDAR_PLAN as requiereValidarPlan, "
            + "REQ_PREG_SCORING as requierePreguntaScoring, REQ_BENEFICIARIOS as requiereBeneficiarios"
            + " from RESTRICCION_VIDA_LEG where ID_PLAN = ?";

    private final String SQL_AGREGAR_PRIMA_UNICA =
        "INSERT INTO PLAN_LEG_PRIMA (ID_PRIMA, CODIGO_TIPO_PROD_LEG, CODIGO_SUB_TIPO_PRO_LEG,"
            + "CODIGO_PRODUCTO_LEG, CODIGO_PLAN_LEG, ID_PAIS, FECHA_CREACION, FECHA_MODIFICACION ) "
            + "VALUES (PLAN_LEG_SEQ.nextval, ?, ?, ?, ?, 1, sysdate, sysdate)";

    private final String SQL_ALIMINAR_PRIMA_UNICA =
        "DELETE FROM PLAN_LEG_PRIMA WHERE CODIGO_PLAN_LEG = ?";

    private static final String SQL_AGREGAR_RESTRICCION_VIDA =
        "INSERT INTO RESTRICCION_VIDA_LEG (ID_RESTRICCION, ID_PLAN, REQ_GRUPO_FAMILIAR, "
            + "REQ_BENEFICIARIOS, ID_PAIS, FECHA_CREACION, FECHA_MODIFICACION) "
            + "VALUES(RESTRICCION_VIDA_SEQ.nextval, ?, ?, ?, 1, sysdate, sysdate)";

    private final String SQL_ACTUALIZAR_RESTRICCION_VIDA =
        "UPDATE RESTRICCION_VIDA_LEG set REQ_GRUPO_FAMILIAR = ?, REQ_BENEFICIARIOS = ?, "
            + "FECHA_MODIFICACION=sysdate WHERE ID_PLAN=? ";

    private static final String SQL_ACTUALIZAR_PLAN_LEG =
        "UPDATE PLAN_LEG SET FECHA_MODIFICACION = sysdate, MAX_DIAS_INI_VIG = ?, "
            + "INSPECCIONBSP = ?, SOLICITA_NUEVO_USADO = ?, SOLICITA_FACTURA = ?, "
            + "FORMA_PAGO = ? WHERE ID_PLAN = ?";

    private static final String SQL_AGREGAR_PLAN_COMPANNIA =
        "INSERT INTO PLAN_COMPANNIA (ID_PLAN_COMPANNIA, CODIGO_TIPO_PROD_LEG, "
            + "CODIGO_SUB_TIPO_PROD_LEG, CODIGO_PRODUCTO_LEG, CODIGO_PLAN_LEG, "
            + "CODIGO_CIA_LEG, NOMBRE_CIA_LEG, CODIGO_CIA_INSPECCION, ID_PAIS, "
            + "ES_ACTIVO, FECHA_CREACION, FECHA_MODIFICACION) VALUES ("
            + "INCREMENTO_PLAN_COMPANNIA.nextval, ?, ?, ?, ?, "
            + "?, ?, ?, 1, 1, sysdate, sysdate)";

    private static final String SQL_ACTUALIZAR_PLAN_COMPANNIA =
        "UPDATE PLAN_COMPANNIA SET CODIGO_CIA_LEG = ?, NOMBRE_CIA_LEG = ?"
            + "WHERE CODIGO_PLAN_LEG = ?";





    /**
     * OBtener los datos de una ficha subcategoria.
     * @param idRama Identificador de rama.
     * @param idSubcategoria Identificador de subcategoria.
     * @return Listado de subcategorias.
     */
    public List < Map < String, ? > > obtenerFichasSubcategoria(int idRama,
        int idSubcategoria) {
        List < Map < String, ? > > res = null;
        String sql = "";

        if (idSubcategoria > 0) {
            sql =
                SQL_FICHA_SUBCATEGORIA
                    + "and s.id_rama = ? and s.id_subcategoria = ? order by f.id_plan desc";

            Object[] params = new Object[] { idRama, idSubcategoria, };

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class, sql,
                    params);
        } else if (idRama > 0) {
            sql = SQL_FICHA_SUBCATEGORIA + "and r.id_rama = ? order by f.id_plan desc";

            Object[] params = new Object[] { idRama, };

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class, sql,
                    params);
        } else {
            Object[] params = new Object[] {};

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class,
                    SQL_FICHA_SUBCATEGORIA , params);
        }
        return res;
    }

    /**
     * OBtener los datos de un plan en promocion.
     * @param idRama Identificador de rama.
     * @param idSubcategoria Identificador de subcategoria.
     * @return Listado de subcategorias.
     */
    public List < Map < String, ? > > obtenerPlanesPromocion(int idRama,
        int idSubcategoria) {
        List < Map < String, ? > > res = null;
        String sql = "";

        if (idSubcategoria > 0) {
            sql =
                SQL_PLAN_PROMOCION
                    + "and s.id_rama = ? and s.id_subcategoria = ? order by f.id_ficha";

            Object[] params = new Object[] { idRama, idSubcategoria, };

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class, sql,
                    params);
        } else if (idRama > 0) {
            sql = SQL_PLAN_PROMOCION + "and s.id_rama = ? order by f.id_ficha";

            Object[] params = new Object[] { idRama, };

            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class, sql,
                    params);
        } else {
            Object[] params = new Object[] {};

            sql = SQL_PLAN_PROMOCION + " order by f.id_ficha";
            res =
                (ArrayList < Map < String, ? > >) this.query(Map.class, sql,
                    params);
        }
        return res;
    }

    /**
     * Agrega una nueva ficha de producto.
     * @param ficha Datos de la ficha.
     * @return identificador de creacion.
     */
    public long agregarFichaProducto(Ficha ficha) {
        long res = -1;

        Object[] params =
            new Object[] { ficha.getId_subcategoria(), ficha.getId_plan(),
                ficha.getNombre_comercial(), ficha.getDescripcion_ficha(),
                ficha.getDescripcion_pagina(), ficha.getEs_subcategoria(),
                ficha.getEstado_ficha(), ficha.getMonto_uf() };

        res = this.insert(SQL_AGREGAR_FICHA, params);

        return res;
    }
    
    public void agregarLegalFichaProducto(LegalFicha legalFicha){
    		long res = -1;
    		Object[] params =
    	            new Object[] {legalFicha.getId_ficha(), legalFicha.getTexto_legal(),legalFicha.getActivo() };

    	    res = this.insert(SQL_AGREGAR_LEGAL_FICHA, params);

    }

    /**
     * TODO Describir m�todo actualizarFichaProducto.
     * @param ficha
     */
    public void actualizarFichaProducto(Ficha ficha) {
        Object[] params =
            new Object[] { ficha.getNombre_comercial(),
                ficha.getDescripcion_ficha(), ficha.getDescripcion_pagina(),
                ficha.getEstado_ficha(), ficha.getMonto_uf(),
                ficha.getId_ficha() };

        this.update(SQL_ACTUALIZAR_FICHA, params);

    }
    
    //CBM
    
    public void actualizarLegalFichaProducto(LegalFicha legalFicha) {
        Object[] params =
                new Object[] { legalFicha.getTexto_legal(),legalFicha.getActivo(),legalFicha.getId_ficha() };

            this.update(SQL_ACTUALIZAR_LEGAL_FICHA, params);

        }


    public void subirArchivo(long id_ficha, byte[] bytes, String tipo) {

        ByteArrayInputStream bios = new ByteArrayInputStream(bytes);

        Object[] params = new Object[] { id_ficha, bios };

        if ("imagen".equals(tipo)) {
            this.insert(SQL_AGREGAR_IMAGEN, params);
        } else if ("beneficio".equals(tipo)) {
            this.insert(SQL_AGREGAR_BENEFICIO, params);
        } else if ("cobertura".equals(tipo)) {
            this.insert(SQL_AGREGAR_COBERTURA, params);
        } else if ("aspecto_legal".equals(tipo)) {
            this.insert(SQL_AGREGAR_ASPECTO_LEGAL, params);
        } else if ("exclusion".equals(tipo)) {
            this.insert(SQL_AGREGAR_EXCLUSION, params);
        }
    }

    public Map < String, Object > obtenerArchivo(long id_ficha, String tipo) {
        Object[] params = new Object[] { id_ficha };
        List < Map < String, Object >> res = null;
        if ("imagen".equals(tipo)) {
            res = this.query(Map.class, SQL_OBTENER_IMAGEN, params);
        } else if ("beneficio".equals(tipo)) {
            res = this.query(Map.class, SQL_OBTENER_BENEFICIO, params);
        } else if ("cobertura".equals(tipo)) {
            res = this.query(Map.class, SQL_OBTENER_COBERTURA, params);
        } else if ("aspecto_legal".equals(tipo)) {
            res = this.query(Map.class, SQL_OBTENER_ASPECTO_LEGAL, params);
        } else if ("exclusion".equals(tipo)) {
            res = this.query(Map.class, SQL_OBTENER_EXCLUSION, params);
        }

        if (res != null && res.size() > 0) {
            try {
                Blob imagen = (Blob) res.get(0).get("archivo");
                InputStream is = imagen.getBinaryStream();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int cnt = is.read(buffer);
                while (cnt != -1) {
                    baos.write(buffer);
                    cnt = is.read(buffer);
                }
                is.close();

                res.get(0).put("archivo", baos.toByteArray());

                return res.get(0);
            } catch (SQLException e) {
                return null;
            } catch (IOException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public long buscarArchivo(long id_ficha, String tipo) {
        long res = -1;
        List < Map < String, Object > > list = null;

        Object[] params = new Object[] { id_ficha };
        if ("imagen".equals(tipo)) {
            list = this.query(Map.class, SQL_BUSCAR_IMAGEN, params);
        } else if ("beneficio".equals(tipo)) {
            list = this.query(Map.class, SQL_BUSCAR_BENEFICIO, params);
        } else if ("cobertura".equals(tipo)) {
            list = this.query(Map.class, SQL_BUSCAR_COBERTURA, params);
        } else if ("aspecto_legal".equals(tipo)) {
            list = this.query(Map.class, SQL_BUSCAR_ASPECTO_LEGAL, params);
        } else if ("exclusion".equals(tipo)) {
            list = this.query(Map.class, SQL_BUSCAR_EXCLUSION, params);
        }

        if (list != null && list.size() > 0) {
            Number num = (Number) list.get(0).get("id");
            res = num.longValue();
        }

        return res;
    }

    public void actualizarArchivo(long id_archivo, long id_ficha, byte[] bytes,
        String tipo) {

        ByteArrayInputStream bios = new ByteArrayInputStream(bytes);

        Object[] params = new Object[] { bios, id_archivo };

        if ("imagen".equals(tipo)) {
            this.update(SQL_ACTUALIZAR_IMAGEN, params);
        } else if ("beneficio".equals(tipo)) {
            this.update(SQL_ACTUALIZAR_BENEFICIO, params);
        } else if ("cobertura".equals(tipo)) {
            this.update(SQL_ACTUALIZAR_COBERTURA, params);
        } else if ("aspecto_legal".equals(tipo)) {
            this.update(SQL_ACTUALIZAR_ASPECTO_LEGAL, params);
        } else if ("exclusion".equals(tipo)) {
            this.update(SQL_ACTUALIZAR_EXCLUSION, params);
        }
    }

    public List < PlanLeg > obtenerPlanes(int idSubcategoria) {
        Object[] params = new Object[] { idSubcategoria };
        List < PlanLeg > res =
            this.query(PlanLeg.class, SQL_OBTENER_PLANES, params);
        return res;
    }

    public Ficha obtenerFicha(Long id_ficha) {
        Ficha ficha = null;
        Object[] params = new Object[] { id_ficha };
        List < Ficha > fichas =
            this.query(Ficha.class, SQL_OBTENER_FICHA, params);
        if (fichas.size() > 0) {
            ficha = fichas.get(0);
        }
        return ficha;
    }
    //CBM legal
    
    public LegalFicha obtenerLegalFicha(Long id_ficha) {
    	LegalFicha legal = null;
        Object[] params = new Object[] { id_ficha };
        List < LegalFicha > legales =
            this.query(LegalFicha.class, SQL_OBTENER_LEGAL, params);
        if (legales.size() > 0) {
            legal = legales.get(0);
        }
        return legal;
    }
    

    /**
     * TODO Describir m�todo obtenerDatosPlan.
     * @param idPlan
     * @return
     */
    public PlanLeg obtenerDatosPlan(long idPlan) {
        Object[] params = new Object[] { idPlan };
        return (PlanLeg) this.find(PlanLeg.class, SQL_OBTENER_DATOS_PLAN,
            params);
    }

    /**
    * TODO Describir m�todo existePrimaUnica.
    * @param idPlan
    * @return
    */
    public boolean existePrimaUnica(long idPlan) {
        boolean existe = false;
        Object[] params = new Object[] { idPlan };
        Map map = (Map) this.find(Map.class, SQL_EXISTE_PRIMA_UNICA, params);

        if (map.containsKey("existeprima")) {
            Object opt = (Object) map.get("existeprima");
            Integer count = new Integer(opt.toString());
            if (count != null && count > 0) {
                existe = true;
            }
        }

        return existe;
    }

    public RestriccionVida obtenerRestriccionVida(long idPlan) {
        Object[] params = new Object[] { idPlan };
        List < RestriccionVida > result =
            this.query(RestriccionVida.class, SQL_OBTENER_RESTRICCION_VIDA,
                params);

        RestriccionVida parentesco;
        if (result.size() > 0) {
            parentesco = (RestriccionVida) result.get(0);
        } else {
            parentesco = new RestriccionVida();
        }

        return parentesco;
    }

    public long agregarPrimaUnica(PlanLeg plan) {
        long res = -1;

        Object[] params =
            new Object[] { plan.getCodigo_tipo_prod_leg(),
                plan.getCodigo_sub_tipo_prod_leg(),
                plan.getCodigo_producto_leg(), plan.getId_plan() };

        res = this.insert(SQL_AGREGAR_PRIMA_UNICA, params);

        return res;
    }

    public void eliminarPrimaUnica(PlanLeg plan) {
        Object[] params = new Object[] { plan.getId_plan() };

        this.update(SQL_ALIMINAR_PRIMA_UNICA, params);
    }

    public long agregarRestriccionVida(RestriccionVida restriccion) {
        long res = -1;

        int iBeneficiarios = (restriccion.isRequiereBeneficiarios() ? 1 : 0);

        Object[] params =
            new Object[] { restriccion.getIdPlan(),
                restriccion.getRequiereGrupoFamiliar(), iBeneficiarios };

        res = this.insert(SQL_AGREGAR_RESTRICCION_VIDA, params);

        return res;
    }

    public void actualizaRestriccionVida(RestriccionVida restriccion) {
        int iBeneficiarios = (restriccion.isRequiereBeneficiarios() ? 1 : 0);

        Object[] params =
            new Object[] { restriccion.getRequiereGrupoFamiliar(),
                iBeneficiarios, restriccion.getIdPlan() };

        this.update(SQL_ACTUALIZAR_RESTRICCION_VIDA, params);
    }

    public void actualizarPlanLeg(PlanLeg plan) {
        Object[] params =
            new Object[] { plan.getMaxDiaIniVig(), plan.getInspeccionVehi(),
                plan.getSolicitaAutoNuevo(), plan.getSolicitaFactura(),
                plan.getFormaPago(), plan.getId_plan_vseg() };

        this.update(SQL_ACTUALIZAR_PLAN_LEG, params);
    }

    public long agregarPlanCompannia(PlanLeg plan) {
        long res = -1;

        Object[] params =
            new Object[] { plan.getCodigo_tipo_prod_leg(),
                plan.getCodigo_sub_tipo_prod_leg(),
                plan.getCodigo_producto_leg(), plan.getId_plan(),
                plan.getCodigo_cia_leg(), plan.getId_plan(), "300" };

        res = this.insert(SQL_AGREGAR_PLAN_COMPANNIA, params);

        return res;
    }

    public void actualizarPlanCompannia(PlanLeg plan) {
        Object[] params =
            new Object[] { plan.getCodigo_cia_leg(), plan.getCompannia(),
                plan.getId_plan() };

        this.update(SQL_ACTUALIZAR_PLAN_COMPANNIA, params);
    }
}
