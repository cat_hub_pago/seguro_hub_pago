package cl.cencosud.ventaseguros.dao;

import java.util.Locale;

import cl.cencosud.ventaseguros.common.config.VSPConfig;
import cl.tinet.common.dao.AbstractDAOFactory;

import com.tinet.exceptions.system.SystemException;

/**
 * @author tinet
 * @version 1.0
 * @created 26-Jul-2010 15:32:02
 */
public class VSPDAOFactory extends AbstractDAOFactory {

    /**
     * DEFAULT_KEY.
     */
    public static final String DEFAULT_KEY =
        "cl.cencosud.ventaseguros.DAOFACTORY";

    /**
     * getInstance.
     * @param locale locale
     * @return {@link VSPDAOFactory}
     */
    public static VSPDAOFactory getInstance(Locale locale) {
        return (VSPDAOFactory) loadFactory(VSPDAOFactory.class, DEFAULT_KEY,
            VSPConfig.getInstance(), locale);
    }

    /**
     * getInstance.
     * @return {@link VSPDAOFactory}
     */
    public static VSPDAOFactory getInstance() {
        return (VSPDAOFactory) loadFactory(VSPDAOFactory.class, DEFAULT_KEY,
            VSPConfig.getInstance());
    }

    /**
     * getVentaSegurosDAO.
     * @param <T> T
     * @param interfazDAO interfazDAO
     * @return T
     */
    public < T > T getVentaSegurosDAO(Class < T > interfazDAO) {
        String className =
            this.getConfigurator().getString(interfazDAO.getName() + ".IMPL");
        try {
            Class < ? > clase = Class.forName(className);
            return (T) this.getDAO(interfazDAO, clase);
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        }
    }
}
