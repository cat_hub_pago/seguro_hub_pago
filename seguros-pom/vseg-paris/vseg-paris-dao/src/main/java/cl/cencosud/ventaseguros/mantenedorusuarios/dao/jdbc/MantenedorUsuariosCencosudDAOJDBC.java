package cl.cencosud.ventaseguros.mantenedorusuarios.dao.jdbc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.mantenedorusuarios.dao.MantenedorUsuariosCencosudDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.model.UsuarioInterno;

/**
 * Encargado de manejar las consultas del mantenedor Usuarios, agregar usuario.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class MantenedorUsuariosCencosudDAOJDBC extends ManagedBaseDAOJDBC
    implements MantenedorUsuariosCencosudDAO {

    /**
     * Consulta para obtener los Roles.
     */
    private static final String SQL_ROLES_USUARIOS =
        "Select ID_ROL as id, nombre_rol as nombre from ROL";

    /**
     * Cadena, SQL que obtiene el total de los usuarios.
     */
    public static final String SQL_CANT_DE_USUARIO =
        "Select count(*) as total from USUARIO WHERE USUARIO.USERNAME = ?";

    /**
     * Cadena, SQL que inserta un nuevo usuario Personal.
     */
    private static final String SQL_INSERT_NUEVO_PERSONAL_CENCO =

        "INSERT INTO VSP_SEG.USUARIO(ID_USUARIO, ID_PAIS,  "
            + "    TIPO_USUARIO, FECHA_CREACION, "
            + "    FECHA_MODIFICACION, USERNAME, ES_ACTIVO,  "
            + "    CANTINTENTOS) VALUES(incremento_usuario.nextval, ?, ?, "
            + "{fn CURTIME()}, {fn CURTIME()},?, ?, ?)";

    /**
     * Cadena, SQL que cuenta los usuarios segun condicion,
     * valida la existencia de un usuario.
     */
    private static final String SQL_EXISTENCIA_USUARIO =
        "Select count(*) as total from usuario where username = ?";

    /**
     * Cadena, SQL que obtiene datos de un usuario Interno.
     */
    public static final String SQL_USUARIO_INTERNO =
        " SELECT ID_USUARIO_INT as IdUsuarioInt, "
            + " ID_USUARIO as IdUsuario , " + " EMAIL as email, "
            + " LOGIN as login, " + " ID_PAIS as IdPais, "
            + " FECHA_CREACION as fechaCreacion , "
            + " FECHA_MODIFICACION as fechaModifiacion  "
            + "     FROM VSP_SEG.USUARIO_INTERNO WHERE  "
            + "     USUARIO_INTERNO.ID_USUARIO = ( Select USUARIO.ID_USUARIO"
            + " FROM USUARIO WHERE  USUARIO.USERNAME = ?)";

    /**
     * Cadena, SQL que obtiene datos de un usuario Externo.
     */
    public static final String SQL_USUARIO_EXTERNO =
        " Select nombre, apellido_paterno, apellido_materno, "
            + "Telefono_1, Telefono_2, "
            + " email, numero, rut_cliente, dv_cliente, numero_departamento, "
            + " calle  from USUARIO_EXTERNO inner join USUARIO ON ( "
            + " USUARIO.ID_USUARIO = USUARIO_EXTERNO.ID_USUARIO) WHERE "
            + " USUARIO.USERNAME = ? ";

    /**
     * Cadena, SQL que actualiza un usuario Externo.
     */
    public static final String SQL_ACTUALIZAR_USUARIO_EXTERNO =
        " UPDATE USUARIO_EXTERNO " + " SET numero= ?, " + " calle= ?, "
            + " apellido_paterno= ?, " + " apellido_materno= ?, "
            + " email= ?, " + " numero_departamento= ?, " + " telefono_2= ?, "
            + " telefono_1= ?, " + " nombre= ? "
            + "    WHERE USUARIO_EXTERNO.ID_USUARIO = (SELECT ID_USUARIO "
            + "    FROM USUARIO WHERE USERNAME = ?) ";

    /**
     * Cadena, SQL que obtiene los roles de un usuario.
     */
    public static final String SQL_OBTENER_ROL_USUARIO =
        " Select ID_ROL, USERNAME, NOMBRE_ROL from USUARIO_INTERNO "
            + "inner join USUARIO_ROL  on ( "
            + " USUARIO_INTERNO.ID_USUARIO = USUARIO_ROL.ID_USUARIO) "
            + "inner join ROL on ( "
            + " USUARIO_ROL.ID_ROL = ROL.ID_ROL)inner join USUARIO ON( "
            + " USUARIO_INTERNO.ID_USUARIO = USUARIO.ID_USUARIO) "
            + "WHERE USUARIO.USERNAME = ?";

    /**
     * Cadena, SQL que obtiene el estado de usuario.
     */
    public static final String SQL_OBTENER_ESTADO =
        "Select ES_ACTIVO FROM USUARIO WHERE USUARIO.USERNAME = ?";

    /**
     * Cadena, SQL que obtiene el estado de la password del usuario.
     */
    public static final String SQL_OBTENER_ESTADO_PASSWORD =
        "Select ID_ESTADO_PSW FROM PSW WHERE ID_USUARIO = "
            + "(SELECT ID_USUARIO FROM USUARIO WHERE USERNAME = ?)";

    /**
     * Cadena, SQL que cambia el estado de un usuario.
     */
    public static final String SQL_CAMBIAR_ESTADO =
        "update USUARIO SET ES_ACTIVO = ? WHERE USUARIO.USERNAME = ? ";

    /**
     * Cadena, SQL que cambia el estado de la password.
     */
    public static final String SQL_CAMBIAR_ESTADO_PASSWORD =
        "update PSW SET ID_ESTADO_PSW = ? WHERE ID_USUARIO = "
            + "(SELECT ID_USUARIO FROM USUARIO WHERE USERNAME = ?) ";

    /**
     * Cadena, SQL que vuelve a ceros los intentos.
     */
    public static final String SQL_VOLVER_CERO =
        "update USUARIO SET CANTINTENTOS = 0 WHERE USUARIO.USERNAME = ?";

    /**
     * Cadena, SQL que obtiene el id del ultimo de los usuarios.
     */
    public static final String SQL_ULTIMO_ID_USUARIO =
        "select id_usuario from usuario where id_usuario ="
            + " (select max(id_usuario) from usuario)";

    /**
     * Cadena, Sql que inserta un nuevo usuario Interno.
     */
    public static final String SQL_INSERT_NUEVO_USUARIO_INTERNO =
        "INSERT INTO USUARIO_INTERNO(" + "ID_USUARIO_INT,"
            + " ID_USUARIO," + " EMAIL," + " LOGIN, " + "ID_PAIS,"
            + "FECHA_CREACION," + " FECHA_MODIFICACION)"
            + " VALUES(INCREMENTO_USUARIO_INTERNO.NEXTVAL,?,?,?,?,"
            + "{fn CURTIME()},  {fn CURTIME()})";

    /**
     * Cadena, sql que inserta un rol de usuario.
     */
    public static final String SQL_INSERT_ROL_USUARIO =
        "INSERT INTO USUARIO_ROL(ID_ROL, ID_USUARIO, "
            + "FECHA_CREACION, FECHA_MODIFICACION) "
            + "VALUES(?, ?, {fn CURTIME()}, {fn CURTIME()})";

    /**
     * Cadena, SQL que obtiene el correo de un usuario.
     */
    public static final String SQL_OBTENER_CORREO =
        "Select email from usuario_interno where usuario_interno.id_usuario = "
            + "(Select id_usuario from usuario  where username = ? )";

    /**
     * Cadena, SQL que actualiza un usuario Interno.
     */
    public static final String SQL_ACTUALIZAR_USUARIO_INTERNO =
        "UPDATE USUARIO_INTERNO"
            + " SET  EMAIL=?, FECHA_MODIFICACION= {fn CURTIME()} "
            + "WHERE ID_USUARIO = "
            + "(select ID_USUARIO FROM USUARIO WHERE USERNAME=?)";

    /**
     * Cadena, SQL que actualiza rol de usuario.
     */
    public static final String SQL_ACTUALIZAR_ROL_USUARIO =
        " UPDATE USUARIO_ROL  "
            + " SET ID_ROL=?,FECHA_MODIFICACION={fn CURTIME()} "
            + "WHERE ID_USUARIO = "
            + "(select ID_USUARIO FROM USUARIO WHERE USERNAME=? )";

    /**
     * Metodo que obtiene los roles.
     * @return Listado de roles.
     */
    public List < Map < String, ? > > obtenerRoles() {
        Object[] param = {};
        List < Map < String, ? > > query =
            (List < Map < String, ? > >) this.query(Map.class,
                SQL_ROLES_USUARIOS, param);
        return query;
    }

    /**
     * Insertar Nuevo Personal Cencosud.
     * @param mapdatos Datos del usuario.
     * @return true si se creo el usuario.
     */
    public boolean insertarNuevoPersonalCencosud(Map < String, ? > mapdatos) {
        boolean resultado = false;
        Object[] param = { 1, 2, mapdatos.get("userName"), 1, 0 };

        Integer cantidad = this.update(SQL_INSERT_NUEVO_PERSONAL_CENCO, param);
        if (cantidad > 0) {
            Object[] paramvacio = {};
            Map valorResultado =
                (Map) this.find(Map.class, SQL_ULTIMO_ID_USUARIO, paramvacio);
            Number id = (Number) valorResultado.get("id_usuario");
            String correo = (String) mapdatos.get("email");
            String userName = (String) mapdatos.get("userName");
            Object[] paramInterno = { id.intValue(), correo, userName, 1 };

            Integer filasAfec =
                this.update(SQL_INSERT_NUEVO_USUARIO_INTERNO, paramInterno);
            if (filasAfec > 0) {
                String combo = (String) mapdatos.get("combo");

                /*   SQL_INSERT_ROL_USUARIO*/
                Object[] paramUsuarRol = { combo, id };

                Integer filasAfectInsert =
                    this.update(SQL_INSERT_ROL_USUARIO, paramUsuarRol);

                if (filasAfectInsert > 0) {
                    resultado = true;
                }
            }
        }
        return resultado;
    }

    public boolean modificarPersonalCencosud(String userName) {
        return false;
    }

    public boolean modificarPersonalCenco(Integer idUsuarioInt) {
        return false;
    }

    public List < Map > obtenerPersonalCenco() {
        return null;
    }

    public List < Map > obtenerPaises() {
        return null;
    }

    public Integer obtenerIdPersonalCenco(String userName) {
        return null;
    }

    public Integer obtenerCantidadTotalPersonalCenco() {
        return null;
    }

    public boolean eliminarPersonalCenco(Integer idUsuarioInt) {
        return false;
    }

    public boolean eliminarPersonalCenco(String userName) {
        return false;
    }

    /**
     * Metodo que obtiene el usuareio Interno Cencosud.
     * @param userName login de usuario.
     * @return Usuario encontrado.
     */
    public UsuarioInterno obtenerUsuarioInternoCenco(String userName) {
        Object[] param = { userName };
        UsuarioInterno usuarioInterno;
        usuarioInterno =
            (UsuarioInterno) this.find(UsuarioInterno.class,
                SQL_USUARIO_INTERNO, param);

        return usuarioInterno;
    }

    /**
     * Metodo para enviar correos.
     * @param userName usuario
     * @return trus si se pudo enviar el correo.
     */
    public boolean enviarCorreo(String userName) {
        return false;
    }

    /**
     * Metodo de actualiza un usuario Cencosud.
     * @param mapdatos Datos del usario.
     * @param userName login del usuario.
     * @return true si se pudo modificar los datos.
     */
    public boolean actualizarUsuarioCenco(Map mapdatos, String userName) {
        boolean resultado = false;
        //String userName = (String) mapdatos.get("userName");
        String combo = (String) mapdatos.get("combo");
        String email = (String) mapdatos.get("email");
        String radio = (String) mapdatos.get("radio");

        int estado = 0;

        if ("si".equals(radio)) {
            estado = 1;
        }

        Object[] param = { email, userName };

        int filasAfectadas = this.update(SQL_ACTUALIZAR_USUARIO_INTERNO, param);

        if (filasAfectadas > 0) {
            //Actualizar estado del usuario.
            param = new Object[] { estado, userName };
            this.update(SQL_CAMBIAR_ESTADO, param);

            //Actualizar rol.
            Object[] parametros = { combo, userName };
            filasAfectadas =
                this.update(SQL_ACTUALIZAR_ROL_USUARIO, parametros);
            if (filasAfectadas > 0) {
                resultado = true;
            }
        }
        return resultado;
    }

    /**
     * Metodo que valida la existencia de un usuario.
     * @param userName login de usuario.
     * @return true si el usuario existe en la base de datos.
     */
    public boolean existenciaUsuario(String userName) {
        Object[] param = { userName };
        Map mapResultado;
        boolean resultado = false;

        mapResultado =
            (Map) this.find(Map.class, SQL_EXISTENCIA_USUARIO, param);
        Number total = (Number) mapResultado.get("total");

        if (total.intValue() == 1) {

            //Para validacion de LDAP
            /*LDAPJumbo ldap = iniciaObjectoLdap();
            ldap.existeMember(userName, 2);*/

            resultado = true;
        }
        return resultado;

    }

    /**
     * Metodo que valida el estado de un usuario Cencosud.
     * @param userName login de usuario
     * @param estado nuevo estado del usuario
     * @return true si se pudo cambiar el estado del usuario.
     */
    public boolean cambiarEstadoUsuarioCenco(String userName, boolean estado) {
        int paramEstado = 0;
        boolean resultado = false;
        if (estado) {
            paramEstado = 1;
            Object[] parametros = { userName };
            this.update(SQL_VOLVER_CERO, parametros);

            Object[] param = { paramEstado, userName };
            int resultadoConsulta = this.update(SQL_CAMBIAR_ESTADO, param);
            if (resultadoConsulta == 1) {
                resultado = true;
            }
        } else {
            Object[] param = { paramEstado, userName };
            int resultadoConsulta = this.update(SQL_CAMBIAR_ESTADO, param);
            if (resultadoConsulta == 1) {
                resultado = true;
            }
        }

        return resultado;
    }

    /**
     * Obtiene un objeto relleno. del usuarioExterno.
     * @param userName login de usuario.
     * @return Usuario obtenido.
     */
    public UsuarioExterno obtenerUsuarioExternoCencosud(String userName) {
        Object[] param = { userName };

        return (UsuarioExterno) this.find(UsuarioExterno.class,
            SQL_USUARIO_EXTERNO, param);
    }

    /**
     * Actualiza los datos de un cliente Externo.
     * @param datos datos del usuario.
     * @param userName login del usuario.
     * @return true si se pudo modificar los datos del cliente.
     */
    public boolean actualizarClienteExternoCenco(Map datos, String userName) {
        boolean resultado = false;
        Object[] param =
            { datos.get("numero"), datos.get("calle"),
                datos.get("apellido_paterno"), datos.get("apellido_materno"),
                datos.get("email"), datos.get("numero_departamento"),
                datos.get("telefono_2"), datos.get("telefono_1"),
                datos.get("nombre"), userName };

        Integer cant = this.update(SQL_ACTUALIZAR_USUARIO_EXTERNO, param);

        if (cant == 1) {
            boolean estado = false;
            if ("si".equals(datos.get("combo"))) {
                estado = true;
            }
            this.cambiarEstadoUsuarioCenco(userName, estado);

            boolean estadoPassword = false;
            if ("si".equals(datos.get("combo_password"))) {
                estadoPassword = true;
            }
            this.cambiarEstadoPasswordUsuarioCenco(userName, estadoPassword);

            resultado = true;
        }
        return resultado;
    }

    /**
     * Metodo que valida el estado la password de un usuario Cencosud.
     * @param userName login de usuario
     * @param estadoPassword nuevo estado de la password
     * @return true si se pudo cambiar el estado del usuario.
     */
    public boolean cambiarEstadoPasswordUsuarioCenco(String userName,
        boolean estadoPassword) {
        int paramEstado = 2;
        boolean resultado = false;
        if (estadoPassword) {
            paramEstado = 1;

            Object[] param = { paramEstado, userName };
            int resultadoConsulta =
                this.update(SQL_CAMBIAR_ESTADO_PASSWORD, param);
            if (resultadoConsulta == 1) {
                resultado = true;
            }
        } else {
            Object[] param = { paramEstado, userName };
            int resultadoConsulta =
                this.update(SQL_CAMBIAR_ESTADO_PASSWORD, param);
            if (resultadoConsulta == 1) {
                resultado = true;
            }
        }

        return resultado;
    }

    /**
     * Valida un usuario contra el LDAP.
     * @param userName login del usuario.
     * @return true si se encuentra el usuario.
     */
    public boolean validacionLDAP(String userName) {
        //LDAPJumbo ldap = iniciaObjectoLdap();
        //return ldap.existeMember(userName, 1);
        return true;
    }

    /**
     * Obtiene el rol especifico de un usuario.
     * @param userName login de usuario.
     * @return roles del usuario.
     */
    public List < Map > obtenerRolUsuario(String userName) {
        Object[] param = { userName };
        List < Map > resultado =
            (List < Map >) this
                .query(Map.class, SQL_OBTENER_ROL_USUARIO, param);
        return resultado;
    }

    /**
     * Obtiene la cantidad de usuarios (personal) de cenco.
     * @param userName login del usuario.
     * @return cantidad de veces que se encuentra el login en la base de datos.
     */
    public int cantUsuarioPersonalCenco(String userName) {

        Object[] param = { userName };
        Map resultado = (Map) this.find(Map.class, SQL_CANT_DE_USUARIO, param);
        Number cantidad = (Number) resultado.get("total");
        return cantidad.intValue();
    }

    /**
     * Metodo que devuelve verdadero o falso, dependiendo del estado del 
     * usuario.
     * @param userName login del usuario.
     * @return true si el usuario no esta bloqueado.
     */
    public boolean obtenerEstadoUsuario(String userName) {
        Object[] param = { userName };
        boolean respuesta = false;

        Map resultado = (Map) this.find(Map.class, SQL_OBTENER_ESTADO, param);
        Number estadoInt = (Number) resultado.get("ES_ACTIVO");
        if (estadoInt.intValue() == 1) {
            respuesta = true;
        }
        return respuesta;
    }

    /**
     * Metodo que devuelve verdadero o falso, dependiendo del estado de 
     * la password del usuario.
     * @param userName login del usuario.
     * @return true si la password del usuario no esta bloqueada.
     */
    public boolean obtenerEstadoPasswordUsuario(String userName) {
        Object[] param = { userName };
        boolean respuesta = false;

        Map resultado =
            (Map) this.find(Map.class, SQL_OBTENER_ESTADO_PASSWORD, param);
        Number estadoInt = (Number) resultado.get("ID_ESTADO_PSW");
        if (estadoInt.intValue() == 1) {
            respuesta = true;
        }
        return respuesta;
    }

    /**
     * Metodo relacionado con la obtencion del correo del usuario.
     * @param userName login de usuario.
     * @return correo electronico del usuario.
     */
    public String obtenerCorreoUsuario(String userName) {
        Object[] param = { userName };
        Map resultado = (Map) this.find(Map.class, SQL_OBTENER_CORREO, param);

        String correo = (String) resultado.get("email");

        return correo;
    }
}
