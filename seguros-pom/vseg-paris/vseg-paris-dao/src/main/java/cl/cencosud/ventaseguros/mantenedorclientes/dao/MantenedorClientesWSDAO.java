package cl.cencosud.ventaseguros.mantenedorclientes.dao;

import cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion;
import cl.cencosud.ventaseguros.common.exception.SolicitudException;
import cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException;
import cl.tinet.common.dao.jdbc.BaseDAO;
import cl.tinet.common.model.exception.BusinessException;

/**
 * DAO de Pagina intermedia.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public interface MantenedorClientesWSDAO extends BaseDAO {

    /**
     * Obtener solicitudes vigentes.
     * @param rutCliente rut del cliente.
     * @return listado de solicitudes.
     * @throws SolicitudException en caso de no poder obtener solicitudes.
     */
    SolicitudCotizacion[] obtenerSolicitudes(Long rutCliente)
        throws SolicitudException;

    byte[] obtenerPolizaPdf(int rutCliente, int numeroSolicitud)
        throws BusinessException;

    byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud)
        throws BusinessException;

    boolean validarSerieRut(String rut, String nroSerie)
        throws ValidacionUsuarioException;

    boolean actualizarFormaPago(int rutCliente, int numeroCotizacion,
        int codigoFormaPago) throws BusinessException;
}
