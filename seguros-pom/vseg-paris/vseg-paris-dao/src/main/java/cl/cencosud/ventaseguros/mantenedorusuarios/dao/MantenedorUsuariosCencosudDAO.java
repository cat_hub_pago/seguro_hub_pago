package cl.cencosud.ventaseguros.mantenedorusuarios.dao;

import java.util.List;
import java.util.Map;

import cl.tinet.common.dao.jdbc.BaseDAO;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.model.UsuarioInterno;

/**
 * DAO de Pagina intermedia.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public interface MantenedorUsuariosCencosudDAO extends BaseDAO {

    /**
     * Retorna los roles del sistema.
     * @return listado de roles.
     */
    public List < Map < String, ? > > obtenerRoles();

    /**
     * Ingresa un nuevo usuario para personal Cencosud (Usuario Interno).
     * @param mapdatos datos del nuevo usuario Cencosud.
     * @return indicador de la insersic�n.
     */
    public boolean insertarNuevoPersonalCencosud(Map < String, ? > mapdatos);

    /**
     * Modifica un usuario Cencosud (Usuario Interno).
     * @param userName login del usuario.
     * @return true si se pudo modificar el usuario.
     */
    public boolean modificarPersonalCencosud(String userName);

    /**
     * Metodo relacionado con la modificacion del personal Cenco.
     * @param idUsuarioInt id de usuario.
     * @return true si se pudo modificar el usuario.
     */
    public boolean modificarPersonalCenco(Integer idUsuarioInt);

    /**
     * Obtener datos del usuario cencosud.
     * @return Listado de datos.
     */
    public List < Map > obtenerPersonalCenco();

    /**
     * Obtener listado de paises.
     * @return Listado de paises.
     */
    public List < Map > obtenerPaises();

    /**
     * Obtener id interno del usuario cencosud.
     * @param userName login de usuario.
     * @return id interno del usuario.
     */
    public Integer obtenerIdPersonalCenco(String userName);

    /**
     * obtener cantidad usuarios cencosud definidos en la base de datos.
     * @return cantidad de usuarios.
     */
    public Integer obtenerCantidadTotalPersonalCenco();

    /**
     * Eliminar usuario cencosud.
     * @param idUsuarioInt identificador de usuario interno.
     * @return true si se pudo eliminar el usuario.
     */
    public boolean eliminarPersonalCenco(Integer idUsuarioInt);

    /**
     * Eliminar usuario cencosud.
     * @param userName login de usuario.
     * @return true si pudo eliminar el usuario.
     */
    public boolean eliminarPersonalCenco(String userName);

    /**
     * Obtener datos del usuario cencosud.
     * @param userName login de usuario
     * @return datos del usuario.
     */
    public UsuarioInterno obtenerUsuarioInternoCenco(String userName);

    /**
     * Enviar un correo electronico.
     * @param userName login.
     * @return true si se puede enviar el correo.
     */
    public boolean enviarCorreo(String userName);

    /**
     * Valida la existencia de un usuario en la base de datos.
     * @param userName login de usuario.
     * @return true si el usuario existe.
     */
    public boolean existenciaUsuario(String userName);

    /**
     * Actualiza los datos de un usuario cencosud.
     * @param mapdatos Datos actualizados del usuario.
     * @param userName login de usuario.
     * @return true si se pudo modificar el usuario.
     */
    public boolean actualizarUsuarioCenco(Map mapdatos, String userName);

    /**
     * Modifica el estado de un usuario cencosud.
     * @param userName login de usuario.
     * @param estado nuevo estado.
     * @return true si se pudo modificar el estado.
     */
    public boolean cambiarEstadoUsuarioCenco(String userName, boolean estado);

    /**
     * Obtener datos de usuario externo.
     * @param userName login de usuario
     * @return Datos obtenidos.
     */
    public UsuarioExterno obtenerUsuarioExternoCencosud(String userName);

    /**
     * actualiza los datos de un cliente.
     * @param datos Datos actualizados del cliente.
     * @param userName login de usuario.
     * @return true si se pudo modificar el usuario.
     */
    public boolean actualizarClienteExternoCenco(Map datos, String userName);

    /**
     * Valida un usuario contra el LDAP.
     * @param userName login de usuario.
     * @return verdadero si se encontro el usuario en el LDAP.
     */
    public boolean validacionLDAP(String userName);

    /**
     * Obtener roles de un usuario.
     * @param userName login de usuario.
     * @return listado de roles obtenidos.
     */
    public List < Map > obtenerRolUsuario(String userName);

    /**
     * Obtene la cantidad de veces que se encuentra un usuario en la base 
     * de datos. Debiera existir solo una vez.
     * @param userName login de usuario.
     * @return cantidad de veces.
     */
    public int cantUsuarioPersonalCenco(String userName);

    /**
     * Obtener el estado de un usuario.
     * @param userName login de usuario.
     * @return true si el usuario esta activo.
     */
    public boolean obtenerEstadoUsuario(String userName);

    /**
     * Metodo que devuelve verdadero o falso, dependiendo del estado de 
     * la password del usuario.
     * @param userName login del usuario.
     * @return true si la password del usuario no esta bloqueada.
     */
    public boolean obtenerEstadoPasswordUsuario(String userName);

    /**
     * Obtener correo electronico de un usuario.
     * @param userName login de usuario.
     * @return Correo electronico del usuario.
     */
    public String obtenerCorreoUsuario(String userName);
}
