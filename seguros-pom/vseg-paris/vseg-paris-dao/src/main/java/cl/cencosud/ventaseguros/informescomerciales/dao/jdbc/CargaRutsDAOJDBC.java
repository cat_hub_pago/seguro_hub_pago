package cl.cencosud.ventaseguros.informescomerciales.dao.jdbc;

//import java.sql.Connection;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.informescomerciales.dao.CargaRutsDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;



public class CargaRutsDAOJDBC extends ManagedBaseDAOJDBC implements
CargaRutsDAO {
	private static final Log logger =
        LogFactory.getLog(CargaRutsDAOJDBC.class);
	
	
	private static final String SQL_INGRESA_RUTS =
	"insert into VSP_ADM.RUTSINCAPTCHA (RUT_USR) values (?) ";
	

	public boolean insertarRutArchivos(
	        String[] rut) {
		boolean resultado = false;

		
      for (int i = 0; i < rut.length; i++) {
      String srut = rut[i].trim();      
      Object[] param =
          { Long.parseLong(srut)};
      this.insertrut(SQL_INGRESA_RUTS, param);
      resultado = true;
      
      }
		

//        long filasResultado = this.insertrut(SQL_INGRESA_RUTS, rut);
//
        

        return resultado;         
	}



}