<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Mensaje Error</title>
	<link href="css/stylebcicar.css" rel="stylesheet" type="text/css" />

</head>
<style type="text/css">
.alert{
	font-family:Arial, Helvetica, sans-serif;
	width:600px;
	/*background-color:#edf0e9;*/
	background-image:url(images/alert_fondo.jpg);
	margin:5px 0 20px 0;
	color:#333333;
	font-size:13px;
	float:left;
	/*border:1px solid #e1e4dd;*/
	}

	
	.alert .alert_ico{
		float:left;
		margin:0 10px 5px 6px;
		}	

		.alert .mensaje{
			float:left;
			margin:28px 5px 5px 5px;
			width:450px;
			font-size:16px;
			}

</style>
<body>

            
            <div class="alert" >
            <img src="images/alert_top.jpg" alt="alert" width="600" height="7" class="round"/>
            <img src="images/alert_ico2.jpg" alt="alert" width="99" height="86" class="alert_ico" />
            
           	  <div class="mensaje" align="center"><strong>Ud no tiene permisos para <br/>accesar esta funcionalidad.</strong></div>
              
              <img src="images/alert_bt.jpg" alt="alert" width="600" height="4" class="round"/>
           </div>   


</body>
</html>
