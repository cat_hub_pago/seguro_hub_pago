<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<%
    String path = request.getContextPath();
    String basePath =
        request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
 
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
	<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery.colorbox.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="<%= request.getContextPath() %>/js/jquery-ui-1.8.4.custom.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/css/style_seguros.css" />
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/colorbox.css" />
	
	
  </head>
  
  <body>
 	<a href="<%= request.getContextPath() %>/autenticacion/cerrarSession" target="_parent">Cerrar Sesi�n</a>
  </body>
</html>