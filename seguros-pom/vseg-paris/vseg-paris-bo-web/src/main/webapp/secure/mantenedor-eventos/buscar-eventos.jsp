<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
<head>
	<html:base />

	<title>buscar.jsp</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>

<body>
	<html:form action="/buscarEventosEjecutar">
		<table>
			<tr>
				<td>
					<bean:message bundle="labels" key="mantenedor.evento.buscar.codigo" />
				</td>
				<td>
					<html:select property="datos(codigo_evento)">
						<html:option value="datos(codigo)">Seleccione una opcion</html:option>

					</html:select>
				</td>
			</tr>


			<tr>
				<td>
					<bean:message bundle="labels"
						key="mantenedor.evento.buscar.Rango_vigencia" />
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<bean:message bundle="labels"
						key="mantenedor.evento.buscar.tipo_evento" />
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<bean:message bundle="labels"
						key="mantenedor.evento.buscar.estado_evento" />
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<bean:message bundle="labels"
						key="mantenedor.evento.buscar.posicion" />
				</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit"
						value="<bean:message bundle="labels" key="mantenedor.evento.buscar.boton_buscar"/>" />
				</td>
			</tr>
		</table>
	</html:form>
</body>
</html:html>
