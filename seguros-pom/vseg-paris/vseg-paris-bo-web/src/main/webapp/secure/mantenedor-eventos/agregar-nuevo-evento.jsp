<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Documento sin t&iacute;tulo</title>

	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<script type="text/javascript" src="../../js/jquery.form.js"></script>
	<script type="text/javascript" src="../../js/jquery.maxlength.js"></script>

	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />

	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<link type="text/css" rel="stylesheet" href="../../css/jquery-ui.css" />
	<link href="../../js/jquery.alerts.css" rel="stylesheet"
		type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<script type="text/javascript">
		
	var subir_archivo = -1;
	var subir_cargaRut = -1;
	var dataruts = '';

	function uploadfile(archivo){
		try {
			if( $("#"+archivo).attr('value') != "") { 
				valor_subir(archivo, 1);
				$.ajaxFileUpload( {
					url:'subir-archivo.do?tipo='+archivo+'&action=nuevo', 
					secureuri:false, 
					fileElementId: archivo, 
					dataType: 'json', 
					success: function (data, status) {
						updloadFinished(archivo, data);
					},
					error: function (data, status, e) {
						jAlert(data.mensaje);
						updloadFinished(archivo, data); 
					}
				});
			}
		} catch (e) {
			alert(e.message);
		}
	} 
	
	function updloadFinished(archivo, data){
		if(data.estado == 'success') {
			//$("#"+archivo).attr('disabled', true);
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.archivoListo" />');
			$("#"+archivo+"_div").show();
		} else if (data.estado  == 'nofile') {
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.noSubioArchivo" />');
			$("#"+archivo+"_div").show();		
		} else {
			$("#"+archivo+"_div").html(data.estado + ': ' + data.mensaje);
			$("#"+archivo+"_div").show();
		}

		if(data.ruts != null) {
			dataruts = data.ruts;
		}

		valor_subir(archivo, -1);
		check_termino();
	}

	function guardarEvento(){
	  
		var codigo =                 $("#codigo_nuevo").attr("value");
		var nombre_evento =          $("#nombre_evento").attr("value");
		var descripcion =            $("#descripcion").attr("value");
		var fecha_inicio_agregar =   $("#fecha_inicio_agregar").attr("value");
		var fecha_termino_agregar  = $("#fecha_termino_agregar").attr("value");
		var ocurrencia_nuevo  =      $("#ocurrencia_nuevo").attr("value");
		var archivo =                $("#archivo").attr("value");
		var url  =                   $("#url").attr("value");
		var cargaRut  =              $("#cargaRut").attr("value");
		var posicion =               $("Select#select_posicion").attr("value");
		var select_tipo_pagina  =    $("Select#select_tipo_pagina").attr("value");
		var select_orden_prioridad = $("Select#select_orden_prioridad").attr("value");
		var radio_segmentado =       $("input[name='radio_segmentado']:checked").val(); 
		var radio_pagina =           $("input[name='radio_pagina']:checked").val(); 

		$.getJSON("/vseg-paris-adm/secure/mantenedor-eventos/agregarNuevoEventoEjecutar.do", {
			/*datos(codigo) : codigo, 
			datos(nombre) :nombre_evento,
			datos(descripcion): descripcion,
			datos(fecha_inicio): fecha_inicio_agregar,
			datos(fecha_termino): fecha_termino_agregar,
			datos(ocurrencia): ocurrencia_nuevo,
			datos(imagen): archivo,
			datos(url): url,
			datos(posicion):posicion,
			datos(prioridad):select_orden_prioridad,
			datos(tipo_evento): radio_segmentado,*/
			ajax : 'true'}, function(j){
									}
		)
	}

	function valor_subir(archivo, valor) {
		eval("subir_" + archivo + "=" + valor + ";");
	}

	function cerrar(){
		self.close();
	}

	function check_termino() {
		if(subir_archivo < 0 && subir_cargaRut < 0) {
			var msj_imagen = $("#archivo_div").html();
			var msj_cargarut = $("#cargaRut_div").html();
			
			var msj = "";
			if(msj_imagen != '') {
				msj += "<li>imagen: "+msj_imagen+"</li>";
			}
			if(msj_cargarut != "") {
				msj += "<li>carga rut: "+msj_cargarut+"</li>";
			}

			if(dataruts != '') {
				parent.jAlert(dataruts, 'RESULTADO CARGA', function() { 
					cerrarVentana(msj);
				});
			} else {
				cerrarVentana(msj);
			}
		}
		
	}

	function cerrarVentana(msj)�{
		parent.jAlert("<li>Evento agregado con exito</li> <br>" + msj + " <br>�Cerrar ventana?", "Evento", function(r) {
			if(r) {
				self.close();
			}
		});
	}

	function enviar_documentos() {
		if(id_evento > 0) {
			uploadfile('archivo');
			uploadfile('cargaRut');
			 check_termino();
		}
	}
	
	function cargarComboPriorid(sugerencia){
    var  htmlCode = "";
     $.getJSON("/vseg-paris-adm/secure/mantenedor-eventos/CargarComboPrioridad.do", {
            ajax : 'true'}, function(j){
           
             if(j.length>0){
                 
                 htmlCode += '<select name="datos(prioridad)">';
                 htmlCode +=' <option value="">Seleccionar una opcion </option>';
                 
                 for (var i = 0; i < j.length; i++) {
                  htmlCode+='<option ';
                  if(j[i].id_prioridad_evento==sugerencia){
                  	htmlCode+='value="'+j[i].id_prioridad_evento+'" selected ="true" >';
                  }else{
                  	htmlCode+='value="'+j[i].id_prioridad_evento+'">';
                  }
                  htmlCode+=j[i].prioridad;
                  htmlCode+='</option>';  
                 }
                 htmlCode += '</select>';
              }

		      $("#elementoPrioridad").empty();
		      $("#elementoPrioridad").append(htmlCode);
      });
    }
    
  $(document).ready(function() {
  
	$('textarea.limited').maxlength({
		'useInput' : true
	});

   $("#cargaRut").removeAttr('disabled');
   
     $("#radioNosegmentado").click(function(){
          $("#cargaRut").attr('disabled','disabled'); 
     });
     
     $("#radioSegmentado").click(function(){
          $("#cargaRut").removeAttr('disabled');
     });
    
    $("#cerrar").click(function(){
    	cerrar();
    });
  
    $("#guardar").click(function() {
    	//Validar segmentacion.
    	if($("input[id=radioSegmentado]:checked").val() == 1) {
    		if($("#cargaRut").attr("value").length == 0) {
    			parent.jAlert('Debe indicar archivo carga rut','Error');
    			return false;
    		}
    	} else {
    		//NO aplica archivo de rut.
    		$("#cargaRut").attr("value", "");
    	}
		$("#formulario").attr("ACTION","agregarNuevoEventoEjecutar.do");
		$("#formulario").attr("action","agregarNuevoEventoEjecutar.do");
		$("#formulario").attr("target", "frameOculto");
		$("#formulario").submit();
		return false;
     });

     var dates2 = $( "#fecha_inicio_agregar, #fecha_termino_agregar" ).datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "button",
			buttonImage: "../../images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy',
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
			'Junio', 'Julio', 'Agosto', 'Septiembre',
			'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
			'May', 'Jun', 'Jul', 'Ago',
			'Sep', 'Oct', 'Nov', 'Dic'],
			onSelect: function( selectedDate ) {
				var option = this.id == "fecha_inicio_agregar" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" );
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates2.not( this ).datepicker( "option", option, date );
			}
		});
          
            if('<html:errors/>'!=""){
	            parent.jAlert('<html:errors/>','Errores');
            }
      }); 
      
  </script>

</head>

<body>
	<table cellpadding="0" cellspacing="0" border="0" width="850"
		align="center" class="datos">
		<tr>
			<td>
				<html:form action="/agregarNuevoEventoEjecutar" styleId="formulario"
					method="post" enctype="multipart/form-data">
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						id="cont_crear">

						<tr>
							<th width="100">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.codigo" />
								:
							</th>
							<td width="250">
								<input type="text" style="width: 100px" value=""
									id="datos(codigo_evento)" name="datos(codigo_evento)" />
							</td>
							<th width="120">
								&nbsp;

							</th>
							<td>
								&nbsp;
							</td>
							<td width="136">
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.nombre_evento" />
								:
							</th>
							<td width="110">
								<input type="text" style="width: 100px" id="nombre_evento"
									name="datos(nombre)" />
							</td>
							<th valign="top">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.descripcion" />
								:
							</th>
							<td rowspan="3" valign="top">
								<textarea rows="4" cols="26" id="descripcion"
									name="datos(descripcion)" class="limited"></textarea>
								<input type="hidden" name="maxlength" value="254" />
							</td>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>

							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.fecha_inicio" />
								:
							</th>
							<td width="110">
								<input type="text" style="width: 70px" id="fecha_inicio_agregar"
									name="datos(fecha_inicio)" />

							</td>
							<th>
								&nbsp;
							</th>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.fecha_termino" />
								:
							</th>
							<td width="110">
								<input type="text" style="width: 70px"
									id="fecha_termino_agregar" name="datos(fecha_termino)" />
							</td>

							<th>
								&nbsp;
							</th>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.ocurrencia" />
								:
							</th>
							<td width="110">
								<input type="text" id="ocurrencia_nuevo"
									name="datos(ocurrencia)" style="width: 30px" />
							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.Imagen" />
								:
							</th>
							<td colspan="2">

								<span id="rut_arch"> <input type="file" name="archivo"
										id="archivo" /> </span>

								<div style="display: none;" id="archivo_div"></div>

							</td>

						</tr>
						<tr>
							<th>
								<bean:message bundle="labels" key="mantenedor.evento.crear.url" />
								:
							</th>
							<td>
								<input type="text" style="width: 220px" value="http://"
									name="datos(url)" id="url" />
							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_pagina" />
								:
							</th>
							<td>
								<input type="radio" name="datos(tipo_pagina)" value="1" />
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_pagina_tipo_1" />
								&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" checked="checked" name="datos(tipo_pagina)"
									value="0" />
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_pagina_tipo_2" />
							</td>
							<td>
								&nbsp;
							</td>

						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.estado" />
								:
							</th>
							<td width="110">
								<input type="checkbox" checked="checked"
									name="datos(estado_habilitado)" id="habilitado" />
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.estado_habilitado" />
							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.posicion" />
								:
							</th>

							<td>

								<html:select property="datos(posicion)" name="datos(posicion)"
									value="tipo" styleId="posicion">
									<option value="">
										<bean:message bundle="labels"
											key="mantenedor.evento.crear.seleccionar_opcion" />
									</option>
									<html:optionsCollection name="posiciones"
										value="id_posicion_evento" label="posicion_evento" />
								</html:select>
							</td>

							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.orden" />
								/
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.Prioridad" />
								:
							</th>
							<td width="110">
								<div id="elementoPrioridad">
									<html:select property="datos(prioridad)"
										name="datos(prioridad)" value="tipo" styleId="posicion">
										<option value="">
											<bean:message bundle="labels"
												key="mantenedor.evento.crear.seleccionar_opcion" />
										</option>
										<html:optionsCollection name="prioridad"
											value="id_prioridad_evento" label="prioridad" />
									</html:select>
								</div>

							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_evento" />
								:
							</th>
							<td>

								<html:select property="datos(tipo_evento)"
									name="datos(tipo_evento)" value="tipo" styleId="posicion">
									<option value="">
										<bean:message bundle="labels"
											key="mantenedor.evento.crear.seleccionar_opcion" />
									</option>
									<html:optionsCollection name="tipoEvento"
										value="ID_TIPO_EVENTO" label="TIPO_EVENTO" />
								</html:select>

							</td>

							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.Segmentado" />
								:
							</th>
							<td>

								<input type="radio" name="datos(radio_segmentado)" value="1"
									checked="checked" id="radioSegmentado" />
								Si &nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="datos(radio_segmentado)" value="0"
									id="radioNosegmentado" />
								No

							</td>


							<th>
								<span id="rut"><bean:message bundle="labels"
										key="mantenedor.evento.crear.carga_rut" /> </span>
							</th>
							<td>
								<span id="rut_arch"><input type="file" name="cargaRut"
										id="cargaRut" /> </span>

								<div style="display: none;" id="cargaRut_div"></div>
							</td>
							<td>
								&nbsp;
							</td>

						</tr>
						<tr>
							<td colspan="5">

								<input type="button"
									value="<bean:message bundle="labels" key="general.guardar"/>"
									id="guardar" name="guardar" />
								&nbsp;&nbsp;
								<input id="cerrar" type="button"
									value="<bean:message bundle="labels" key="general.cerrar"/>" />
							</td>
						</tr>
					</table>
				</html:form>
			</td>
		</tr>
	</table>
</body>

</html:html>
