<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Filtrar evento</title>


	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<script type="text/javascript" src="../../js/jquery.form.js"></script>
	<script type="text/javascript" src="../../js/jquery.tooltip.js"></script>
	<script type="text/javascript" src="../../js/jquery.delegate.js"></script>
	<script type="text/javascript" src="../../js/jquery.bgiframe.js"></script>
	<script type="text/javascript" src="../../js/jquery.delegate.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>

	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />
	<link rel="stylesheet" type="text/css" href="../../css/jquery.tooltip.css" />


	<link rel="stylesheet" type="text/css" href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />


	<link type="text/css" rel="stylesheet" href="../../css/jquery-ui.css" />
	<link href="../../js/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />


	<script type="text/javascript">
	var id_evento = -1;
	var win_evento = new Object();
	
	var error_fechainicio = "<bean:message key="mantenedor.evento.crear.FechaInicio" />";
	var error_fechatermino = "<bean:message key="mantenedor.evento.crear.FechaTermino" />";
	var error_rangoFechas = "<bean:message key="mantenedor.evento.crear.rangoFechas" />";
	
		
    function pop_modificar(id_evento){
        parametros="width=920,height=450,top=100,left=150,scrollbars=no,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
	    window.open("modificarEvento.do?id_evento="+id_evento,"_detalle",parametros);
    }
    
    function descripcion_corta(descripcion) {
    	if(descripcion!=null && descripcion.length > 30) {
    		descripcion = "<span id='fancy' title='"+descripcion+"'>" + descripcion.substring(0,25) + "...</span>";
    	} 
   		return descripcion;
    }
          
	$(document).ready(function() {

		$("#fancy").tooltip({
			track: true,
			delay: 0,
			showURL: false,
			showBody: " - ",
			fade: 250
		});
  
  
		$("#cargaRut").removeAttr('disabled');
		
		$("#nuevoEvento").click(function(){
			parametros="width=860,height=360,top=100,left=150,scrollbars=no,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
			win_evento = window.open("agregarNuevoEvento.do","_evento",parametros);
		}); 
		
		var dates = $( "#SelectorFechaInicio, #SelectorFechaTermino" ).datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "button",
			buttonImage: "../../images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy',
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
				'Junio', 'Julio', 'Agosto', 'Septiembre',
				'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
				'May', 'Jun', 'Jul', 'Ago',
				'Sep', 'Oct', 'Nov', 'Dic'],
			onSelect: function( selectedDate ) {
				var option = this.id == "SelectorFechaInicio" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" );
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
          
		if('<html:errors/>'!=""){
			parent.jAlert('<html:errors/>','Errores');
		}
            
		$("#botonBuscar").click(function(){
			var codigo =        $("#codigo").attr("value");
			var fecha_inicial = $("#SelectorFechaInicio").attr("value");
			var fecha_termino = $("#SelectorFechaTermino").attr("value");
			var posicion =      $("select#posicion").attr("value");
			var tipo_evento =   $("select#tipoEventos").attr("value"); 
			var habilitado=     $("#habilitado").attr("checked");
			var html = "";
         
			$('#listado_adm tbody').remove();
         
			$.getJSON("/vseg-paris-adm/secure/mantenedor-eventos/buscarEventos.do", {
				codigo: codigo,
				fecha_inicial:fecha_inicial,
				fecha_termino:fecha_termino,
				posicion:posicion,
				tipo_evento:tipo_evento,
				habilitado:habilitado,
				ajax : 'true'}, 
				function(j){
					if(j.error!=null) {
						try {
							msg = eval(j.error);
							jAlert(msg);
						} catch(e) {
							alert(e.message);
						}
					}
	
					if(j.length>0){
						var htmlCode = '<tbody>';
		
						for (var i = 0; i < j.length; i++) {
							var id_evento = j[i].id_evento;
							if(i%2 == 0){
								htmlCode +='<tr class="listadoSegPar">';
							}else{
								htmlCode +='<tr class="listadoSegImpar">';
							}
							htmlCode+='<td>';
							htmlCode+=j[i].nombre; 
							htmlCode+='</td>';
							
							htmlCode+='<td>';
							htmlCode+=descripcion_corta(j[i].descripcion);
							htmlCode+='</td>';
							
							htmlCode+='<td>';
							try {
							htmlCode+=j[i].fecha_inicio;
							} catch(e) {
								alert(e.message);
							}
							htmlCode+='</td>'; 
		
							htmlCode+='<td>';
							htmlCode+=j[i].fecha_termino;
							htmlCode+='</td>'; 
	
							htmlCode+='<td>';
							htmlCode+=j[i].ocurrencia; 
							htmlCode+='</td>';
							
							if(j[i].total_ruts ==null){
								htmlCode+='<td>';
								htmlCode+='no segmentado';
								htmlCode+='</td>';
							}else{
								htmlCode+='<td>';
								htmlCode+=j[i].total_ruts; 
								htmlCode+='</td>';
							}
							
							htmlCode+='<td>';
							htmlCode+=j[i].ruts_listos; 
							htmlCode+='</td>';
							
							htmlCode+='<td id="ver_imagen_' + id_evento + '">';
							htmlCode+='<img style="cursor:pointer" src="../../images/ver_ficha.gif" border="0">' 
							htmlCode+='</td>';
							
							htmlCode+='<td>';
							htmlCode+=j[i].estado_evento; 
							htmlCode+='</td>';
							
							htmlCode+='<td>';
							htmlCode+=j[i].posicion; 
							htmlCode+='</td>';
							
							htmlCode+='<td>';
							htmlCode+=' <a href="#" onclick="pop_modificar('+ id_evento +');return false;" target="_blank" ><img src="../../images/modif.gif"  border="0"></a>';
							htmlCode+='</td>';
							
							htmlCode +='</tr>';
						}
		
						htmlCode += '</tbody>';
		
						$('#listado_adm').append(htmlCode);
						
						for (var i = 0; i < j.length; i++) {
							var id_evento = j[i].id_evento;
							$("#ver_imagen_" + id_evento).colorbox({
								width:'100%',
								title:'Imagen',
								href:"/vseg-paris-adm/secure/mantenedor-eventos/obtener-archivo.do?tipo=imagen&id_evento=" + id_evento + "&x=imagen.jpg"
							});
						}
	
						$("#grillaResultado").show();
		         	} else{
						htmlCode="";
						htmlCode += '<tbody> <tr class="listadoSegImpar"> <td colspan="11" > Sin resultados </td> </tr> </tbody> ';
						$('#listado_adm').append(htmlCode);
					}
		
					$('#listado_adm').show();
				})
		})
	}); 
	</script>
</head>

<body>
	<table cellpadding="0" cellspacing="0" border="0" width="850"
		align="center" class="datos">
		<tr>
			<td>
				<html:form action="/filtrarEvento" styleId="formulario">
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						id="cont_adm" style="display: inline">
						<tr>
							<td colspan="6">
								&nbsp;
							</td>
						</tr>
						<tr>
							<th width="93">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.codigo" />
								:
							</th>
							<td width="130">
								<input type="text" style="width: 100px;" name="codigo"
									id="codigo" />
							</td>

							<th width="111">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.fecha_inicio" />
								:
							</th>
							<td width="149">
								<input type="text" style="width: 100px;"
									id="SelectorFechaInicio" />
							</td>
							<th width="114">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.fecha_termino" />
								:
							</th>
							<td width="169">
								<input type="text" style="width: 100px;"
									id="SelectorFechaTermino" />
							</td>
						</tr>
						<tr>
							<th width="93">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_evento" />
								:
							</th>

							<td width="130">

								<html:select property="datos(tipoEventos)" name="tipoEventos"
									value="tipo" styleId="tipoEventos">
									<option value="">
										<bean:message bundle="labels"
											key="mantenedor.evento.crear.seleccionar_opcion" />
									</option>
									<html:optionsCollection name="tipoEvento"
										value="id_tipo_evento" label="tipo_evento" />
								</html:select>


							</td>
							<th width="111">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.estado" />
								:
							</th>
							<td width="149">
								<html:checkbox property="datos(estado_habilitado)"
									styleId="habilitado">
									<bean:message bundle="labels"
										key="mantenedor.evento.crear.estado_habilitado" />
								</html:checkbox>
							</td>
							<th width="114">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.posicion" />
								:
							</th>
							<td width="169">

								<html:select property="datos(posicion)" name="seleccionados"
									value="tipo" styleId="posicion">
									<option value="">
										<bean:message bundle="labels"
											key="mantenedor.evento.crear.seleccionar_opcion" />
									</option>
									<html:optionsCollection name="posiciones"
										value="id_posicion_evento" label="posicion_evento" />
								</html:select>

							</td>

						</tr>
						<tr>
							<td colspan="6">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="6" align="center">
								<input type="button" value="Buscar" id="botonBuscar" />
							</td>
						</tr>
					</table>
				</html:form>
			</td>
		</tr>
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					align="center" id="listado_adm" style="display: none">
					<thead>
						<tr class="listadoSegCab">
							<th width="20%">
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.nombre" />
								</div>
							</th>

							<th width="20%">
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.descripcion" />
								</div>
							</th>
							<th width="8%" nowrap="nowrap">
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.fecha_inicio" />
								</div>
							</th>
							<th width="8%" nowrap="nowrap">
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.fecha_termino" />
								</div>
							</th>
							<th>
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.ocurrencia" />
								</div>
							</th>
							<th>
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.total_rut" />
								</div>
							</th>
							<th width="7%">
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.rut_listos" />
								</div>
							</th>

							<th>
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.imagen" />
								</div>
							</th>
							<th width="4%">
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.estado" />
								</div>
							</th>
							<th>
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.posicion" />
								</div>
							</th>

							<th width="3%">
								<div align="center">
									<bean:message bundle="labels"
										key="mantenedor.eventos.titulo.modificar_Eventos" />
								</div>
							</th>
						</tr>
					</thead>
				</table>
				<table cellpadding="0" cellspacing="0" border="0" width="100%"
					align="center" id="nuevo">
					<tr>
						<td colspan="6" align="right">
							<input type="button" name="nuevoEvento" id="nuevoEvento"
								value="Ingresar Nuevo Evento" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<iframe id="frameOculto" frameborder="0" width="500" height="0"
		src="" name="frameOculto">
	</iframe>

</body>

</html:html>
