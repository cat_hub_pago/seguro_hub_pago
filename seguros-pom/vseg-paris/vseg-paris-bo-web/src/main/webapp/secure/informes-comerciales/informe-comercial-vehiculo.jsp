<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
	<head>
		<html:base />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />
		
		<title>:::::: BACK OFFICE ::::::</title>
	   
		<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
		<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
		<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
		<script type="text/javascript" src="../../js/jquery-tinet.js"></script>

		<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />
		<link rel="stylesheet" type="text/css" href="../../css/jquery.tooltip.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
		<link type="text/css" rel="stylesheet" href="../../css/jquery-ui.css" />
	    <link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />

		<script type="text/javascript">
		function exportar() {
			try {
				$("#excel").attr("value", 1);
				$("#informeComercialVehiculo").attr("target", "iframe-excel");
				$("#informeComercialVehiculo").submit();	
			} catch (e) {
				alert(e.message);
			}
		}

		$(document).ready(function(){
			
			var dates = $( "#SelectorFechaInicio, #SelectorFechaTermino" ).datepicker({
				changeMonth: true,
				changeYear: true,
				showOn: "button",
				buttonImage: "../../images/calendar.gif",
				buttonImageOnly: true,
				dateFormat: 'dd/mm/yy',
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
					'Junio', 'Julio', 'Agosto', 'Septiembre',
					'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
					'May', 'Jun', 'Jul', 'Ago',
					'Sep', 'Oct', 'Nov', 'Dic'],
				onSelect: function( selectedDate ) {
					var option = this.id == "SelectorFechaInicio" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" );
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
					dates.not( this ).datepicker( "option", option, date );
				}
			});
			
		
			$("#numeroPagina").change(function(){
				$("#excel").attr("value", 0);
				$("#informeComercialVehiculo").attr("target", "");
				$("#informeComercialVehiculo").submit();
			});
			
			$("#buscar").click(function () {
				try {
					$("#excel").attr("value", 0);
					$("#informeComercialVehiculo").attr("target", "");
					$("#informeComercialVehiculo").submit();	
				} catch (e) {
					alert(e.message);
				}
				return false;
			});
			
			
			$("a[name='factura_informe']").click(function(){
				 
				 $(this).colorbox({
			     href: $(this).attr("href"), 
			     photo: true
			     });
			});
			
			colores('lista');
		});
		</script>

	</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" width="800" align="center" class="datos" >
	<tr>
		<td>
            <table cellpadding="0" cellspacing="0" border="0"  width="100%"  align="center" >
				<tr valign="top">
					<td height="33" >Informe Validaci�n de Factura Autos Nuevos<br/></td>
			  </tr>
    			<tr>
    				<td>
							<html:form action="/obtener-informe-vehiculo" styleId="informeComercialVehiculo">
							<html:hidden property="datos(excel)" name="informeComercialTarjetas" styleId="excel" value="0"/>
                        	<table cellpadding="0" cellspacing="0" border="0" width="90%" align="center">
                            	<tr>
                                	<th width="26%" >Fecha Desde: </th>
                                    <td width="23%"><html:text property="datos(fechaDesde)" name="informeComercialVehiculo" styleId="SelectorFechaInicio" size="12"></html:text></td>
                                    <th width="26%" >Fecha Hasta: </th>
                                    <td width="51%"><html:text property="datos(fechaHasta)" name="informeComercialVehiculo" styleId="SelectorFechaTermino" size="12"></html:text></td>
                              </tr>
                              
                              <tr>
                              	<td colspan="4" align="center">
                              		<html:errors property="datos.fechaDesde"/>
                                    <html:errors property="datos.fechaHasta"/>
                                    <html:errors property="fechas"/>
                              	</td>
                              </tr>

                              <tr>
                              	<td colspan="4" align="center"><br />
	                              	<input value="Buscar" type="button" name="buscar" id="buscar"/>
                              	</td>
                              </tr>
                              <tr><td colspan="4">&nbsp;</td></tr>
                            </table>
                            
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" id="lista">
                           	    <tr class="listadoSegCab">

									<td width="9%" align="center">Id</td>
								  	<td width="6%" align="center">N�P�liza</td>
									<td width="6%" align="center">Nombre</td>
									<td width="7%" align="center">Tel�fono</td>
									<td width="10%" align="center">E-mail</td>
                                    <td width="10%" align="center">Factura</td>
                                    <td width="10%" align="center">Fecha</td>

                                    <td width="10%" align="center">Rut</td>
                                    <td width="10%" align="center">Producto</td>
                                    <td width="10%" align="center">Prima UF</td>
                                    <td width="10%" align="center">Proporcional</td>
                                    <td width="12%" align="center">Tarjeta</td>
                              </tr>
                              
                              
                              <logic:present name="informe">
                              <logic:iterate id="informeVehiculo" name="informe" scope="request" indexId="index">
                              
                               <tr >
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="idseguro"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="numeropoliza"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="nombrecliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="fonocliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="emailcliente"/></td>
                                 <td valign="top" align="right"><a name="factura_informe" href="<bean:write name="contextpath" />/secure/obtener-factura.do?id_factura=<bean:write name="informeVehiculo" property="idfactura"/>"><bean:write name="informeVehiculo" property="idfactura"/></a></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="fechaseguro" format="dd-MM-yyyy"/></td>
                                 <td valign="top" align="left" nowrap="nowrap"><bean:write name="informeVehiculo" property="rutcliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="nombreproducto"/></td>
                                 <td valign="top" align="right"><bean:write name="informeVehiculo" property="primauf"/></td>
                                 <td valign="top" align="right"><bean:write name="informeVehiculo" property="proporcional"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="tipotarjeta"/></td>
                              </tr>
                              

                              </logic:iterate>
                              
                              
                              <tr>
                              	<th colspan="12" bgcolor="#CCCCCC" align="center">
                                Pagina: <html:select property="datos(numeroPagina)"  name="informeComercialVehiculo" styleId="numeroPagina">
									<html:options collection="listadoPaginas" property="id" labelProperty="descripcion"/>
                                </html:select>
                                </th>
                              </tr>
                              <tr>
                              	<th colspan="12" bgcolor="#CCCCCC">&nbsp;&nbsp;&nbsp;&nbsp;

                                <span class="link_List3"><a href="javascript:exportar();" >Exportar a Excel</a></span></th>
                              </tr>
                              </logic:present>
                              <tr><td colspan="12">&nbsp;</td></tr>
                            </table>
                            </html:form>
       			 	</td>
    			</tr>
				
                 
			</table>

         </td>
	</tr>
</table>
<iframe frameborder="0" height="0" width="100" id="iframe-excel" name="iframe-excel" src=""></iframe>

</body>

</html:html>