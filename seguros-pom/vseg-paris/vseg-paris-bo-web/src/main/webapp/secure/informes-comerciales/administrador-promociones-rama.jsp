<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
	   <link type="text/css" rel="stylesheet" href="../../css/jquery-ui.css" />	
	   <link href="../../css/colorbox.css" rel="stylesheet" type="text/css" />   
		<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
		<script src="../../js/jquery-tinet.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	
	

<script type="text/javascript">
 $(document).ready(function(){
 $("#inputFechaDesde, #inputFechaHasta").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesde, #inputFechaHasta").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
  $("#inputFechaDesdeHP, #inputFechaHastaHP").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHP, #inputFechaHastaHP").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
 $("#inputFechaDesdeVIP, #inputFechaHastaVIP").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVIP, #inputFechaHastaVIP").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
 $("#inputFechaDesdeSP, #inputFechaHastaSP").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeSP, #inputFechaHastaSP").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
 $("#inputFechaDesdeVB1, #inputFechaHastaVB1").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB1, #inputFechaHastaVB1").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVB2, #inputFechaHastaVB2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB2, #inputFechaHastaVB2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVB3, #inputFechaHastaVB3").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB3, #inputFechaHastaVB3").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVB4, #inputFechaHastaVB4").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB4, #inputFechaHastaVB4").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVB5, #inputFechaHastaVB5").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB5, #inputFechaHastaVB5").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
			
	$("#inputFechaDesdeVB6, #inputFechaHastaVB6").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB6, #inputFechaHastaVB6").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeHB1, #inputFechaHastaHB1").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB1, #inputFechaHastaHB1").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeHB2, #inputFechaHastaHB2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB2, #inputFechaHastaHB2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeHB3, #inputFechaHastaHB3").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB3, #inputFechaHastaHB3").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeHB4, #inputFechaHastaHB4").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB4, #inputFechaHastaHB4").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeHB5, #inputFechaHastaHB5").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB5, #inputFechaHastaHB5").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeHB6, #inputFechaHastaHB6").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB6, #inputFechaHastaHB6").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVIB1, #inputFechaHastaVIB1").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVIB1, #inputFechaHastaVIB1").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVIB2, #inputFechaHastaVIB2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVIB2, #inputFechaHastaVIB2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
			
	$("#inputFechaDesdeVIB3, #inputFechaHastaVIB3").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVIB3, #inputFechaHastaVIB3").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVIB4, #inputFechaHastaVIB4").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVIB4, #inputFechaHastaVIB4").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVIB5, #inputFechaHastaVIB5").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVIB5, #inputFechaHastaVIB5").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeVIB6, #inputFechaHastaVIB6").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVIB6, #inputFechaHastaVIB6").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeSB1, #inputFechaHastaSB1").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeSB1, #inputFechaHastaSB1").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeSB2, #inputFechaHastaSB2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeSB2, #inputFechaHastaSB2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeSB3, #inputFechaHastaSB3").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeSB3, #inputFechaHastaSB3").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeSB4, #inputFechaHastaSB4").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeSB4, #inputFechaHastaSB4").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeSB5, #inputFechaHastaSB5").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeSB5, #inputFechaHastaSB5").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
	$("#inputFechaDesdeSB6, #inputFechaHastaSB6").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeSB6, #inputFechaHastaSB6").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
	
		
 })
 
</script>	

<script type="text/javascript">
$(document).ready(function(){
			$('div#principal-auto').hide();
			$('div#principal-hogar').hide();
			$('div#principal-vida').hide();
			$('div#principal-salud').hide();
			$('div#secundario-auto').hide();
			$('div#secundario-hogar').hide();
			$('div#secundario-vida').hide();
			$('div#secundario-salud').hide();
		});
	
		function cargarPagina(){
			var rama = $('select#rama').val();
			var subcat = $('select#subcategoria').val();
			if (subcat == 1 && rama == 1){
				$('div#principal-vida').hide();
				$('div#principal-hogar').hide();
				$('div#principal-auto').show();
				$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').hide();
				//var url = '/obtener-promociones-bo.do';
				//var params = '?rama=' + rama + '?subcategoria=' + subcat;
				//$.getJSON(url + params, function(data){
					//document.getElementById('idPlan').value = data.idPlan;
					//document.getElementById('nombre').value = data.nombre;
					//document.getElementById('tracker').value = data.tracker;
					//document.getElementById('imagen').value = data.imagen;
					//document.getElementById('link').value = data.idPlan;
				//});
			}else if (subcat == 1 && rama == 2){
				$('div#principal-auto').hide();
				$('div#principal-vida').hide();
				$('div#principal-hogar').show();
				$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').hide();
			}else if (subcat == 1 && rama == 3){
				$('div#principal-auto').hide();
				$('div#principal-hogar').hide();
				$('div#principal-vida').show();	
				$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').hide();
			}else if (subcat == 1 && rama == 4){
				$('div#principal-auto').hide();
				$('div#principal-hogar').hide();
				$('div#principal-vida').hide();	
				$('div#principal-salud').show();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').hide();
			}else if (rama == 0 || subcat == 0){
				$('div#principal-auto').hide();
				$('div#principal-hogar').hide();
				$('div#principal-vida').hide();
				$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').hide();
			}else if (subcat == 2 && rama == 1){
				$('div#principal-auto').hide();
				$('div#principal-hogar').hide();
				$('div#principal-vida').hide();
				$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').show();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').hide();
			}else if (subcat == 2 && rama == 2){
				$('div#principal-auto').hide();
				$('div#principal-hogar').hide();
				$('div#principal-vida').hide();
				$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').show();
				$('div#secundario-vida').hide();			
			}else if (rama == 3 && subcat == 2){
				$('div#principal-auto').hide();
				$('div#principal-hogar').hide();
				$('div#principal-vida').hide();
				$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').show();
			}else if (rama == 4 && subcat == 2){
				$('div#principal-auto').hide();
				$('div#principal-hogar').hide();
				$('div#principal-vida').hide();
				$('div#principal-salud').hide();
				$('div#secundario-salud').show();
				$('div#secundario-auto').hide();
				$('div#secundario-hogar').hide();
				$('div#secundario-vida').hide();
			}
		}
		
		function cargarSub(){
			$('div#principal-auto').hide();
			$('div#principal-hogar').hide();
			$('div#principal-vida').hide();
			$('div#secundario-auto').hide();
			$('div#principal-salud').hide();
				$('div#secundario-salud').hide();
			$('div#secundario-hogar').hide();
			$('div#secundario-vida').hide();
			$('select#subcategoria').val(0);
		}
		
		function guardarPromocion(){
			var rama = $('select#rama').val();
			//alert(rama);
			var subcategoria = $('select#subcategoria').val();			
			if (subcategoria == 1){
				if (rama == 1){
				var idPlan = document.getElementById('idPlan').value;
				var nombre = document.getElementById('nombre').value;
					var tracker = document.getElementById('tracker').value;
					var link = document.getElementById('link').value;
					var imagen = document.getElementById('imagen').value;
						document.getElementById('hiddNombre').value = nombre;
						document.getElementById('hiddTracker').value = tracker;
						document.getElementById('hiddLink').value = link;
						document.getElementById('hiddImagen').value = imagen;
						document.getElementById('hiddRama').value = rama;
						document.getElementById('hiddSubcategoria').value = subcategoria;
						document.getElementById('hiddIdPlan').value = idPlan;
						alert('Promoci�n insertada correctamente');
						$("#informeComercialTarjetas").submit();
						//return false;
				}
				else if (rama == 2){
					var idPlan = document.getElementById('idPlanH').value;
				var nombre = document.getElementById('nombreH').value;
					var tracker = document.getElementById('trackerH').value;
					var link = document.getElementById('linkH').value;
					var imagen = document.getElementById('imagenH').value;
						document.getElementById('hiddNombreH').value = nombre;
						document.getElementById('hiddTrackerH').value = tracker;
						document.getElementById('hiddLinkH').value = link;
						document.getElementById('hiddImagenH').value = imagen;
						document.getElementById('hiddRamaH').value = rama;
						document.getElementById('hiddSubcategoriaH').value = subcategoria;
						document.getElementById('hiddIdPlanH').value = idPlan;
						alert('Promoci�n insertada correctamente');
						$("#informeComercialTarjetas").submit();
						//return false;
				}
				else if (rama == 3){
					var idPlan = document.getElementById('idPlanV').value;
				var nombre = document.getElementById('nombreV').value;
					var tracker = document.getElementById('trackerV').value;
					var link = document.getElementById('linkV').value;
					var imagen = document.getElementById('imagenV').value;
						document.getElementById('hiddNombreV').value = nombre;
						document.getElementById('hiddTrackerV').value = tracker;
						document.getElementById('hiddLinkV').value = link;
						document.getElementById('hiddImagenV').value = imagen;
						document.getElementById('hiddRamaV').value = rama;
						document.getElementById('hiddSubcategoriaV').value = subcategoria;
						document.getElementById('hiddIdPlanV').value = idPlan;
						alert('Promoci�n insertada correctamente');
						$("#informeComercialTarjetas").submit();
						//return false;
				}
				else if (rama == 4){
					var idPlan = document.getElementById('idPlanS').value;
				var nombre = document.getElementById('nombreS').value;
					var tracker = document.getElementById('trackerS').value;
					var link = document.getElementById('linkS').value;
					var imagen = document.getElementById('imagenS').value;
						document.getElementById('hiddNombreS').value = nombre;
						document.getElementById('hiddTrackerS').value = tracker;
						document.getElementById('hiddLinkS').value = link;
						document.getElementById('hiddImagenS').value = imagen;
						document.getElementById('hiddRamaS').value = rama;
						document.getElementById('hiddSubcategoriaS').value = subcategoria;
						document.getElementById('hiddIdPlanS').value = idPlan;
						alert('Promoci�n insertada correctamente');
						document.getElementById('hiddSubmit').value = "0";
						$("#informeComercialTarjetas").submit();
						//return false;
				}
			}
			else if (subcategoria == 2){
				var rama = $('select#rama').val();
					var nombreVB1 = document.getElementById('nombreVB1').value;
					var nombreVB2 = document.getElementById('nombreVB2').value;
					var nombreVB3 = document.getElementById('nombreVB3').value;
					var nombreVB4 = document.getElementById('nombreVB4').value;
					var nombreVB5 = document.getElementById('nombreVB5').value;
					var nombreVB6 = document.getElementById('nombreVB6').value;
					var nombreHB1 = document.getElementById('nombreHB1').value;
					var nombreHB2 = document.getElementById('nombreHB2').value;
					var nombreHB3 = document.getElementById('nombreHB3').value;
					var nombreHB4 = document.getElementById('nombreHB4').value;
					var nombreHB5 = document.getElementById('nombreHB5').value;
					var nombreHB6 = document.getElementById('nombreHB6').value;
					var nombreVIB1 = document.getElementById('nombreVIB1').value;
					var nombreVIB2 = document.getElementById('nombreVIB2').value;
					var nombreVIB3 = document.getElementById('nombreVIB3').value;
					var nombreVIB4 = document.getElementById('nombreVIB4').value;
					var nombreVIB5 = document.getElementById('nombreVIB5').value;
					var nombreVIB6 = document.getElementById('nombreVIB6').value;
					var nombreSB1 = document.getElementById('nombreSB1').value;
					var nombreSB2 = document.getElementById('nombreSB2').value;
					var nombreSB3 = document.getElementById('nombreSB3').value;
					var nombreSB4 = document.getElementById('nombreSB4').value;
					var nombreSB5 = document.getElementById('nombreSB5').value;
					var nombreSB6 = document.getElementById('nombreSB6').value;
					var trackerVB1 = document.getElementById('trackerVB1').value;
					var trackerVB2 = document.getElementById('trackerVB2').value;
					var trackerVB3 = document.getElementById('trackerVB3').value;
					var trackerVB4 = document.getElementById('trackerVB4').value;
					var trackerVB5 = document.getElementById('trackerVB5').value;
					var trackerVB6 = document.getElementById('trackerVB6').value;
					var trackerHB1 = document.getElementById('trackerHB1').value;
					var trackerHB2 = document.getElementById('trackerHB2').value;
					var trackerHB3 = document.getElementById('trackerHB3').value;
					var trackerHB4 = document.getElementById('trackerHB4').value;
					var trackerHB5 = document.getElementById('trackerHB5').value;
					var trackerHB6 = document.getElementById('trackerHB6').value;
					var trackerVIB1 = document.getElementById('trackerVIB1').value;
					var trackerVIB2 = document.getElementById('trackerVIB2').value;
					var trackerVIB3 = document.getElementById('trackerVIB3').value;
					var trackerVIB4 = document.getElementById('trackerVIB4').value;
					var trackerVIB5 = document.getElementById('trackerVIB5').value;
					var trackerVIB6 = document.getElementById('trackerVIB6').value;
					var trackerSB1 = document.getElementById('trackerSB1').value;
					var trackerSB2 = document.getElementById('trackerSB2').value;
					var trackerSB3 = document.getElementById('trackerSB3').value;
					var trackerSB4 = document.getElementById('trackerSB4').value;
					var trackerSB5 = document.getElementById('trackerSB5').value;
					var trackerSB6 = document.getElementById('trackerSB6').value;
					var imagenVB1 = document.getElementById('imagenVB1').value;
					var imagenVB2 = document.getElementById('imagenVB2').value;
					var imagenVB3 = document.getElementById('imagenVB3').value;
					var imagenVB4 = document.getElementById('imagenVB4').value;
					var imagenVB5 = document.getElementById('imagenVB5').value;
					var imagenVB6 = document.getElementById('imagenVB6').value;
					var imagenHB1 = document.getElementById('imagenHB1').value;
					var imagenHB2 = document.getElementById('imagenHB2').value;
					var imagenHB3 = document.getElementById('imagenHB3').value;
					var imagenHB4 = document.getElementById('imagenHB4').value;
					var imagenHB5 = document.getElementById('imagenHB5').value;
					var imagenHB6 = document.getElementById('imagenHB6').value;
					var imagenVIB1 = document.getElementById('imagenVIB1').value;
					var imagenVIB2 = document.getElementById('imagenVIB2').value;
					var imagenVIB3 = document.getElementById('imagenVIB3').value;
					var imagenVIB4 = document.getElementById('imagenVIB4').value;
					var imagenVIB5 = document.getElementById('imagenVIB5').value;
					var imagenVIB6 = document.getElementById('imagenVIB6').value;
					var imagenSB1 = document.getElementById('imagenSB1').value;
					var imagenSB2 = document.getElementById('imagenSB2').value;
					var imagenSB3 = document.getElementById('imagenSB4').value;
					var imagenSB4 = document.getElementById('imagenSB4').value;
					var imagenSB5 = document.getElementById('imagenSB5').value;
					var imagenSB6 = document.getElementById('imagenSB6').value;
					var linkVerVB1 = document.getElementById('linkVerBasesVB1').value;
					var linkVerVB2 = document.getElementById('linkVerBasesVB2').value;
					var linkVerVB3 = document.getElementById('linkVerBasesVB3').value;
					var linkVerVB4 = document.getElementById('linkVerBasesVB4').value;
					var linkVerVB5 = document.getElementById('linkVerBasesVB5').value;
					var linkVerVB6 = document.getElementById('linkVerBasesVB6').value;
					var linkVerHB1 = document.getElementById('linkVerBasesHB1').value;
					var linkVerHB2 = document.getElementById('linkVerBasesHB2').value;
					var linkVerHB3 = document.getElementById('linkVerBasesHB3').value;
					var linkVerHB4 = document.getElementById('linkVerBasesHB4').value;
					var linkVerHB5 = document.getElementById('linkVerBasesHB5').value;
					var linkVerHB6 = document.getElementById('linkVerBasesHB6').value;
					var linkVerVIB1 = document.getElementById('linkVerBasesVIB1').value;
					var linkVerVIB2 = document.getElementById('linkVerBasesVIB2').value;
					var linkVerVIB3 = document.getElementById('linkVerBasesVIB3').value;
					var linkVerVIB4 = document.getElementById('linkVerBasesVIB4').value;
					var linkVerVIB5 = document.getElementById('linkVerBasesVIB5').value;
					var linkVerVIB6 = document.getElementById('linkVerBasesVIB6').value;
					var linkVerSB1 = document.getElementById('linkVerBasesSB1').value;
					var linkVerSB2 = document.getElementById('linkVerBasesSB2').value;
					var linkVerSB3 = document.getElementById('linkVerBasesSB3').value;
					var linkVerSB4 = document.getElementById('linkVerBasesSB4').value;
					var linkVerSB5 = document.getElementById('linkVerBasesSB5').value;
					var linkVerSB6 = document.getElementById('linkVerBasesSB6').value;
					var linkConocerVB1 = document.getElementById('linkConocerVB1').value;
					var linkConocerVB2 = document.getElementById('linkConocerVB2').value;
					var linkConocerVB3 = document.getElementById('linkConocerVB3').value;
					var linkConocerVB4 = document.getElementById('linkConocerVB4').value;
					var linkConocerVB5 = document.getElementById('linkConocerVB5').value;
					var linkConocerVB6 = document.getElementById('linkConocerVB6').value;
					var linkConocerHB1 = document.getElementById('linkConocerHB1').value;
					var linkConocerHB2 = document.getElementById('linkConocerHB2').value;
					var linkConocerHB3 = document.getElementById('linkConocerHB3').value;
					var linkConocerHB4 = document.getElementById('linkConocerHB4').value;
					var linkConocerHB5 = document.getElementById('linkConocerHB5').value;
					var linkConocerHB6 = document.getElementById('linkConocerHB6').value;
					var linkConocerVIB1 = document.getElementById('linkConocerVIB1').value;
					var linkConocerVIB2 = document.getElementById('linkConocerVIB2').value;
					var linkConocerVIB3 = document.getElementById('linkConocerVIB3').value;
					var linkConocerVIB4 = document.getElementById('linkConocerVIB4').value;
					var linkConocerVIB5 = document.getElementById('linkConocerVIB5').value;
					var linkConocerVIB6 = document.getElementById('linkConocerVIB6').value;
					var linkConocerSB1 = document.getElementById('linkConocerSB1').value;
					var linkConocerSB2 = document.getElementById('linkConocerSB2').value;
					var linkConocerSB3 = document.getElementById('linkConocerSB3').value;
					var linkConocerSB4 = document.getElementById('linkConocerSB4').value;
					var linkConocerSB5 = document.getElementById('linkConocerSB5').value;
					var linkConocerSB6 = document.getElementById('linkConocerSB6').value;
					var linkCotizarVB1 = document.getElementById('linkCotizarVB1').value;
					var linkCotizarVB2 = document.getElementById('linkCotizarVB2').value;
					var linkCotizarVB3 = document.getElementById('linkCotizarVB3').value;
					var linkCotizarVB4 = document.getElementById('linkCotizarVB4').value;
					var linkCotizarVB5 = document.getElementById('linkCotizarVB5').value;
					var linkCotizarVB6 = document.getElementById('linkCotizarVB6').value;
					var linkCotizarHB1 = document.getElementById('linkCotizarHB1').value;
					var linkCotizarHB2 = document.getElementById('linkCotizarHB2').value;
					var linkCotizarHB3 = document.getElementById('linkCotizarHB3').value;
					var linkCotizarHB4 = document.getElementById('linkCotizarHB4').value;
					var linkCotizarHB5 = document.getElementById('linkCotizarHB5').value;
					var linkCotizarHB6 = document.getElementById('linkCotizarHB6').value;
					var linkCotizarVIB1 = document.getElementById('linkCotizarVIB1').value;
					var linkCotizarVIB2 = document.getElementById('linkCotizarVIB2').value;
					var linkCotizarVIB3 = document.getElementById('linkCotizarVIB3').value;
					var linkCotizarVIB4 = document.getElementById('linkCotizarVIB4').value;
					var linkCotizarVIB5 = document.getElementById('linkCotizarVIB5').value;
					var linkCotizarVIB6 = document.getElementById('linkCotizarVIB6').value;
					var linkCotizarSB1 = document.getElementById('linkCotizarSB1').value;
					var linkCotizarSB2 = document.getElementById('linkCotizarSB2').value;
					var linkCotizarSB3 = document.getElementById('linkCotizarSB3').value;
					var linkCotizarSB4 = document.getElementById('linkCotizarSB4').value;
					var linkCotizarSB5 = document.getElementById('linkCotizarSB5').value;
					var linkCotizarSB6 = document.getElementById('linkCotizarSB6').value;
					var idPlanVB1 = document.getElementById('idPlanVB1').value;
					var idPlanVB2 = document.getElementById('idPlanVB2').value;
					var idPlanVB3 = document.getElementById('idPlanVB3').value;
					var idPlanVB4 = document.getElementById('idPlanVB4').value;
					var idPlanVB5 = document.getElementById('idPlanVB5').value;
					var idPlanVB6 = document.getElementById('idPlanVB6').value;
					var idPlanHB1 = document.getElementById('idPlanHB1').value;
					var idPlanHB2 = document.getElementById('idPlanHB2').value;
					var idPlanHB3 = document.getElementById('idPlanHB3').value;
					var idPlanHB4 = document.getElementById('idPlanHB4').value;
					var idPlanHB5 = document.getElementById('idPlanHB5').value;
					var idPlanHB6 = document.getElementById('idPlanHB6').value;
					var idPlanVIB1 = document.getElementById('idPlanVIB1').value;
					var idPlanVIB2 = document.getElementById('idPlanVIB2').value;
					var idPlanVIB3 = document.getElementById('idPlanVIB3').value;
					var idPlanVIB4 = document.getElementById('idPlanVIB4').value;
					var idPlanVIB5 = document.getElementById('idPlanVIB5').value;
					var idPlanVIB6 = document.getElementById('idPlanVIB6').value;
					var idPlanSB1 = document.getElementById('idPlanSB1').value;
					var idPlanSB2 = document.getElementById('idPlanSB2').value;
					var idPlanSB3 = document.getElementById('idPlanSB3').value;
					var idPlanSB4 = document.getElementById('idPlanSB4').value;
					var idPlanSB5 = document.getElementById('idPlanSB5').value;
					var idPlanSB6 = document.getElementById('idPlanSB6').value;
					
					
					alert('Insertado Correctamente');
					document.getElementById('hiddRamaVB').value = rama;
					document.getElementById('hiddSubcategoria').value = subcategoria;
					document.getElementById('hiddNombreVB1').value = nombreVB1;
					document.getElementById('hiddTrackerVB1').value = trackerVB1;
					document.getElementById('hiddLinkVerVB1').value = linkVerVB1;
					document.getElementById('hiddLinkConocerVB1').value = linkConocerVB1;
					document.getElementById('hiddLinkCotizarVB1').value = linkCotizarVB1;
					document.getElementById('hiddImagenVB1').value = imagenVB1;
					if (document.getElementById('hiddNombreVB1').value != "-" || document.getElementById('hiddImagenVB1').value != "-"
							|| document.getElementById('hiddLinkCotizarVB1').value != "-" || document.getElementById('hiddLinkVerVB1').value != "-"
							|| document.getElementById('hiddLinkCotizarVB1').value != "-"){
					if (document.getElementById('hiddNombreVB1').value != document.getElementById('hiddNombreVB1X').value
							|| document.getElementById('hiddImagenVB1').value != document.getElementById('hiddImagenVB1X').value
							|| document.getElementById('hiddLinkCotizarVB1').value != document.getElementById('hiddLinkCotizarVB1X').value
							|| document.getElementById('hiddLinkVerVB1').value != document.getElementById('hiddLinkVerVB1X').value
							|| document.getElementById('hiddLinkCotizarVB1').value != document.getElementById('hiddLinkCotizarVB1X').value
							|| document.getElementById('hiddTrackerVB1').value != document.getElementById('hiddTrackerVB1X').value){
					   document.getElementById('hiddOrdinalVB1').value = 1;	
					} else {
					   document.getElementById('hiddOrdinalVB1').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVB1').value = 0;	 
					}
					document.getElementById('hiddIdPlanVB1').value = idPlanVB1;
					document.getElementById('hiddNombreVB2').value = nombreVB2;
					document.getElementById('hiddTrackerVB2').value = trackerVB2;
					document.getElementById('hiddLinkVerVB2').value = linkVerVB2;
					document.getElementById('hiddLinkConocerVB2').value = linkConocerVB2;
					document.getElementById('hiddLinkCotizarVB2').value = linkCotizarVB2;
					document.getElementById('hiddImagenVB2').value = imagenVB2;
					if (document.getElementById('hiddNombreVB2').value != "-" || document.getElementById('hiddImagenVB2').value != "-"
							|| document.getElementById('hiddLinkCotizarVB2').value != "-" || document.getElementById('hiddLinkVerVB2').value != "-"
							|| document.getElementById('hiddLinkCotizarVB2').value != "-"){
					if (document.getElementById('hiddNombreVB2').value != document.getElementById('hiddNombreVB2X').value
							|| document.getElementById('hiddImagenVB2').value != document.getElementById('hiddImagenVB2X').value
							|| document.getElementById('hiddLinkCotizarVB2').value != document.getElementById('hiddLinkCotizarVB2X').value
							|| document.getElementById('hiddLinkVerVB2').value != document.getElementById('hiddLinkVerVB2X').value
							|| document.getElementById('hiddLinkCotizarVB2').value != document.getElementById('hiddLinkCotizarVB2X').value
							|| document.getElementById('hiddTrackerVB2').value != document.getElementById('hiddTrackerVB2X').value){
					   document.getElementById('hiddOrdinalVB2').value = 2;	
					} else {
					   document.getElementById('hiddOrdinalVB2').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVB2').value = 0;	 
					}
					document.getElementById('hiddIdPlanVB2').value = idPlanVB2;
					document.getElementById('hiddNombreVB3').value = nombreVB3;
					document.getElementById('hiddTrackerVB3').value = trackerVB3;
					document.getElementById('hiddLinkVerVB3').value = linkVerVB3;
					document.getElementById('hiddLinkConocerVB3').value = linkConocerVB3;
					document.getElementById('hiddLinkCotizarVB3').value = linkCotizarVB3;
					document.getElementById('hiddImagenVB3').value = imagenVB3;
					if (document.getElementById('hiddNombreVB3').value != "-" || document.getElementById('hiddImagenVB3').value != "-"
							|| document.getElementById('hiddLinkCotizarVB3').value != "-" || document.getElementById('hiddLinkVerVB3').value != "-"
							|| document.getElementById('hiddLinkCotizarVB3').value != "-"){
					if (document.getElementById('hiddNombreVB3').value != document.getElementById('hiddNombreVB3X').value
							|| document.getElementById('hiddImagenVB3').value != document.getElementById('hiddImagenVB3X').value
							|| document.getElementById('hiddLinkCotizarVB3').value != document.getElementById('hiddLinkCotizarVB3X').value
							|| document.getElementById('hiddLinkVerVB3').value != document.getElementById('hiddLinkVerVB3X').value
							|| document.getElementById('hiddLinkCotizarVB3').value != document.getElementById('hiddLinkCotizarVB3X').value
							|| document.getElementById('hiddTrackerVB3').value != document.getElementById('hiddTrackerVB3X').value){
					   document.getElementById('hiddOrdinalVB3').value = 3;	
					} else {
					   document.getElementById('hiddOrdinalVB3').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVB3').value = 0;	 
					}
					document.getElementById('hiddIdPlanVB3').value = idPlanVB3;
					document.getElementById('hiddNombreVB4').value = nombreVB4;
					document.getElementById('hiddTrackerVB4').value = trackerVB4;
					document.getElementById('hiddLinkVerVB4').value = linkVerVB4;
					document.getElementById('hiddLinkConocerVB4').value = linkConocerVB4;
					document.getElementById('hiddLinkCotizarVB4').value = linkCotizarVB4;
					document.getElementById('hiddImagenVB4').value = imagenVB4;
					document.getElementById('hiddIdPlanVB4').value = idPlanVB4;
					if (document.getElementById('hiddNombreVB4').value != "-" || document.getElementById('hiddImagenVB4').value != "-"
							|| document.getElementById('hiddLinkCotizarVB4').value != "-" || document.getElementById('hiddLinkVerVB4').value != "-"
							|| document.getElementById('hiddLinkCotizarVB4').value != "-"){
					if (document.getElementById('hiddNombreVB4').value != document.getElementById('hiddNombreVB4X').value
							|| document.getElementById('hiddImagenVB4').value != document.getElementById('hiddImagenVB4X').value
							|| document.getElementById('hiddLinkCotizarVB4').value != document.getElementById('hiddLinkCotizarVB4X').value
							|| document.getElementById('hiddLinkVerVB4').value != document.getElementById('hiddLinkVerVB4X').value
							|| document.getElementById('hiddLinkCotizarVB4').value != document.getElementById('hiddLinkCotizarVB4X').value
							|| document.getElementById('hiddTrackerVB4').value != document.getElementById('hiddTrackerVB4X').value){
					   document.getElementById('hiddOrdinalVB4').value = 4;	
					} else {
					   document.getElementById('hiddOrdinalVB4').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVB4').value = 0;	 
					}
					document.getElementById('hiddNombreVB5').value = nombreVB5;
					document.getElementById('hiddTrackerVB5').value = trackerVB5;
					document.getElementById('hiddLinkVerVB5').value = linkVerVB5;
					document.getElementById('hiddLinkConocerVB5').value = linkConocerVB5;
					document.getElementById('hiddLinkCotizarVB5').value = linkCotizarVB5;
					document.getElementById('hiddImagenVB5').value = imagenVB5;
					if (document.getElementById('hiddNombreVB5').value != "-" || document.getElementById('hiddImagenVB5').value != "-"
							|| document.getElementById('hiddLinkCotizarVB5').value != "-" || document.getElementById('hiddLinkVerVB5').value != "-"
							|| document.getElementById('hiddLinkCotizarVB5').value != "-"){
					if (document.getElementById('hiddNombreVB5').value != document.getElementById('hiddNombreVB5X').value
							|| document.getElementById('hiddImagenVB5').value != document.getElementById('hiddImagenVB5X').value
							|| document.getElementById('hiddLinkCotizarVB5').value != document.getElementById('hiddLinkCotizarVB5X').value
							|| document.getElementById('hiddLinkVerVB5').value != document.getElementById('hiddLinkVerVB5X').value
							|| document.getElementById('hiddLinkCotizarVB5').value != document.getElementById('hiddLinkCotizarVB5X').value
							|| document.getElementById('hiddTrackerVB5').value != document.getElementById('hiddTrackerVB5X').value){
					   document.getElementById('hiddOrdinalVB5').value = 5;	
					} else {
					   document.getElementById('hiddOrdinalVB5').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVB5').value = 0;	 
					}
					document.getElementById('hiddIdPlanVB5').value = idPlanVB5;
					document.getElementById('hiddNombreVB6').value = nombreVB6;
					document.getElementById('hiddTrackerVB6').value = trackerVB6;
					document.getElementById('hiddLinkVerVB6').value = linkVerVB6;
					document.getElementById('hiddLinkConocerVB6').value = linkConocerVB6;
					document.getElementById('hiddLinkCotizarVB6').value = linkCotizarVB6;
					document.getElementById('hiddImagenVB6').value = imagenVB6;
					if (document.getElementById('hiddNombreVB6').value != "-" || document.getElementById('hiddImagenVB6').value != "-"
							|| document.getElementById('hiddLinkCotizarVB6').value != "-" || document.getElementById('hiddLinkVerVB6').value != "-"
							|| document.getElementById('hiddLinkCotizarVB6').value != "-"){
					if (document.getElementById('hiddNombreVB6').value != document.getElementById('hiddNombreVB6X').value
							|| document.getElementById('hiddImagenVB6').value != document.getElementById('hiddImagenVB6X').value
							|| document.getElementById('hiddLinkCotizarVB6').value != document.getElementById('hiddLinkCotizarVB6X').value
							|| document.getElementById('hiddLinkVerVB6').value != document.getElementById('hiddLinkVerVB6X').value
							|| document.getElementById('hiddLinkCotizarVB6').value != document.getElementById('hiddLinkCotizarVB6X').value
							|| document.getElementById('hiddTrackerVB6').value != document.getElementById('hiddTrackerVB6X').value){
					   document.getElementById('hiddOrdinalVB6').value = 6;	
					} else {
					   document.getElementById('hiddOrdinalVB6').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVB6').value = 0;	 
					}
					document.getElementById('hiddIdPlanVB6').value = idPlanVB6;
					
					document.getElementById('hiddRamaHB').value = rama;
					document.getElementById('hiddNombreHB1').value = nombreHB1;
					document.getElementById('hiddTrackerHB1').value = trackerHB1;
					document.getElementById('hiddLinkVerHB1').value = linkVerHB1;
					document.getElementById('hiddLinkConocerHB1').value = linkConocerHB1;
					document.getElementById('hiddLinkCotizarHB1').value = linkCotizarHB1;
					document.getElementById('hiddImagenHB1').value = imagenHB1;
					if (document.getElementById('hiddNombreHB1').value != "-" || document.getElementById('hiddImagenHB1').value != "-"
							|| document.getElementById('hiddLinkCotizarHB1').value != "-" || document.getElementById('hiddLinkVerHB1').value != "-"
							|| document.getElementById('hiddLinkCotizarHB1').value != "-"){
					if (document.getElementById('hiddNombreHB1').value != document.getElementById('hiddNombreHB1X').value
							|| document.getElementById('hiddImagenHB1').value != document.getElementById('hiddImagenHB1X').value
							|| document.getElementById('hiddLinkCotizarHB1').value != document.getElementById('hiddLinkCotizarHB1X').value
							|| document.getElementById('hiddLinkVerHB1').value != document.getElementById('hiddLinkVerHB1X').value
							|| document.getElementById('hiddLinkCotizarHB1').value != document.getElementById('hiddLinkCotizarHB1X').value
							|| document.getElementById('hiddTrackerHB1').value != document.getElementById('hiddTrackerHB1X').value){
					   document.getElementById('hiddOrdinalHB1').value = 1;	
					} else {
					   document.getElementById('hiddOrdinalHB1').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalHB1').value = 0;	 
					}
					document.getElementById('hiddIdPlanHB1').value = idPlanHB1;
					document.getElementById('hiddNombreHB2').value = nombreHB2;
					document.getElementById('hiddTrackerHB2').value = trackerHB2;
					document.getElementById('hiddLinkVerHB2').value = linkVerHB2;
					document.getElementById('hiddLinkConocerHB2').value = linkConocerHB2;
					document.getElementById('hiddLinkCotizarHB2').value = linkCotizarHB2;
					document.getElementById('hiddImagenHB2').value = imagenHB2;
					if (document.getElementById('hiddNombreHB2').value != "-" || document.getElementById('hiddImagenHB2').value != "-"
							|| document.getElementById('hiddLinkCotizarHB2').value != "-" || document.getElementById('hiddLinkVerHB2').value != "-"
							|| document.getElementById('hiddLinkCotizarHB2').value != "-"){
					if (document.getElementById('hiddNombreHB2').value != document.getElementById('hiddNombreHB2X').value
							|| document.getElementById('hiddImagenHB2').value != document.getElementById('hiddImagenHB2X').value
							|| document.getElementById('hiddLinkCotizarHB2').value != document.getElementById('hiddLinkCotizarHB2X').value
							|| document.getElementById('hiddLinkVerHB2').value != document.getElementById('hiddLinkVerHB2X').value
							|| document.getElementById('hiddLinkCotizarHB2').value != document.getElementById('hiddLinkCotizarHB2X').value
							|| document.getElementById('hiddTrackerHB2').value != document.getElementById('hiddTrackerHB2X').value){
					   document.getElementById('hiddOrdinalHB2').value = 2;	
					} else {
					   document.getElementById('hiddOrdinalHB2').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalHB2').value = 0;	 
					}
					document.getElementById('hiddIdPlanHB2').value = idPlanHB2;
					document.getElementById('hiddNombreHB3').value = nombreHB3;
					document.getElementById('hiddTrackerHB3').value = trackerHB3;
					document.getElementById('hiddLinkVerHB3').value = linkVerHB3;
					document.getElementById('hiddLinkConocerHB3').value = linkConocerHB3;
					document.getElementById('hiddLinkCotizarHB3').value = linkCotizarHB3;
					document.getElementById('hiddImagenHB3').value = imagenHB3;
					if (document.getElementById('hiddNombreHB3').value != "-" || document.getElementById('hiddImagenHB3').value != "-"
							|| document.getElementById('hiddLinkCotizarHB3').value != "-" || document.getElementById('hiddLinkVerHB3').value != "-"
							|| document.getElementById('hiddLinkCotizarHB3').value != "-"){
					if (document.getElementById('hiddNombreHB3').value != document.getElementById('hiddNombreHB3X').value
							|| document.getElementById('hiddImagenHB3').value != document.getElementById('hiddImagenHB3X').value
							|| document.getElementById('hiddLinkCotizarHB3').value != document.getElementById('hiddLinkCotizarHB3X').value
							|| document.getElementById('hiddLinkVerHB3').value != document.getElementById('hiddLinkVerHB3X').value
							|| document.getElementById('hiddLinkCotizarHB3').value != document.getElementById('hiddLinkCotizarHB3X').value
							|| document.getElementById('hiddTrackerHB3').value != document.getElementById('hiddTrackerHB3X').value){
					   document.getElementById('hiddOrdinalHB3').value = 3;	
					} else {
					   document.getElementById('hiddOrdinalHB3').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalHB3').value = 0;	 
					}
					document.getElementById('hiddIdPlanHB3').value = idPlanHB3;
					document.getElementById('hiddNombreHB4').value = nombreHB4;
					document.getElementById('hiddTrackerHB4').value = trackerHB4;
					document.getElementById('hiddLinkVerHB4').value = linkVerHB4;
					document.getElementById('hiddLinkConocerHB4').value = linkConocerHB4;
					document.getElementById('hiddLinkCotizarHB4').value = linkCotizarHB4;
					document.getElementById('hiddImagenHB4').value = imagenHB4;
					if (document.getElementById('hiddNombreHB4').value != "-" || document.getElementById('hiddImagenHB4').value != "-"
							|| document.getElementById('hiddLinkCotizarHB4').value != "-" || document.getElementById('hiddLinkVerHB4').value != "-"
							|| document.getElementById('hiddLinkCotizarHB4').value != "-"){
					if (document.getElementById('hiddNombreHB4').value != document.getElementById('hiddNombreHB4X').value
							|| document.getElementById('hiddImagenHB4').value != document.getElementById('hiddImagenHB4X').value
							|| document.getElementById('hiddLinkCotizarHB4').value != document.getElementById('hiddLinkCotizarHB4X').value
							|| document.getElementById('hiddLinkVerHB4').value != document.getElementById('hiddLinkVerHB4X').value
							|| document.getElementById('hiddLinkCotizarHB4').value != document.getElementById('hiddLinkCotizarHB4X').value
							|| document.getElementById('hiddTrackerHB4').value != document.getElementById('hiddTrackerHB4X').value){
					   document.getElementById('hiddOrdinalHB4').value = 4;	
					} else {
					   document.getElementById('hiddOrdinalHB4').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalHB4').value = 0;	 
					}
					document.getElementById('hiddIdPlanHB4').value = idPlanHB4;
					document.getElementById('hiddNombreHB5').value = nombreHB5;
					document.getElementById('hiddTrackerHB5').value = trackerHB5;
					document.getElementById('hiddLinkVerHB5').value = linkVerHB5;
					document.getElementById('hiddLinkConocerHB5').value = linkConocerHB5;
					document.getElementById('hiddLinkCotizarHB5').value = linkCotizarHB5;
					document.getElementById('hiddImagenHB5').value = imagenHB5;
					if (document.getElementById('hiddNombreHB5').value != "-" || document.getElementById('hiddImagenHB5').value != "-"
							|| document.getElementById('hiddLinkCotizarHB5').value != "-" || document.getElementById('hiddLinkVerHB5').value != "-"
							|| document.getElementById('hiddLinkCotizarHB5').value != "-"){
					if (document.getElementById('hiddNombreHB5').value != document.getElementById('hiddNombreHB5X').value
							|| document.getElementById('hiddImagenHB5').value != document.getElementById('hiddImagenHB5X').value
							|| document.getElementById('hiddLinkCotizarHB5').value != document.getElementById('hiddLinkCotizarHB5X').value
							|| document.getElementById('hiddLinkVerHB5').value != document.getElementById('hiddLinkVerHB5X').value
							|| document.getElementById('hiddLinkCotizarHB5').value != document.getElementById('hiddLinkCotizarHB5X').value
							|| document.getElementById('hiddTrackerHB5').value != document.getElementById('hiddTrackerHB5X').value){
					   document.getElementById('hiddOrdinalHB5').value = 5;	
					} else {
					   document.getElementById('hiddOrdinalHB5').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalHB5').value = 0;	 
					}
					document.getElementById('hiddIdPlanHB5').value = idPlanHB5;
					document.getElementById('hiddNombreHB6').value = nombreHB6;
					document.getElementById('hiddTrackerHB6').value = trackerHB6;
					document.getElementById('hiddLinkVerHB6').value = linkVerHB6;
					document.getElementById('hiddLinkConocerHB6').value = linkConocerHB6;
					document.getElementById('hiddLinkCotizarHB6').value = linkCotizarHB6;
					document.getElementById('hiddImagenHB6').value = imagenHB6;
					if (document.getElementById('hiddNombreHB6').value != "-" || document.getElementById('hiddImagenHB6').value != "-"
							|| document.getElementById('hiddLinkCotizarHB6').value != "-" || document.getElementById('hiddLinkVerHB6').value != "-"
							|| document.getElementById('hiddLinkCotizarHB6').value != "-"){
					if (document.getElementById('hiddNombreHB6').value != document.getElementById('hiddNombreHB6X').value
							|| document.getElementById('hiddImagenHB6').value != document.getElementById('hiddImagenHB6X').value
							|| document.getElementById('hiddLinkCotizarHB6').value != document.getElementById('hiddLinkCotizarHB6X').value
							|| document.getElementById('hiddLinkVerHB6').value != document.getElementById('hiddLinkVerHB6X').value
							|| document.getElementById('hiddLinkCotizarHB6').value != document.getElementById('hiddLinkCotizarHB6X').value
							|| document.getElementById('hiddTrackerHB6').value != document.getElementById('hiddTrackerHB6X').value){
					   document.getElementById('hiddOrdinalHB6').value = 6;	
					} else {
					   document.getElementById('hiddOrdinalHB6').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalHB6').value = 0;	 
					}
					document.getElementById('hiddIdPlanHB6').value = idPlanHB6;
					
					document.getElementById('hiddRamaVIB').value = rama;
					document.getElementById('hiddNombreVIB1').value = nombreVIB1;
					document.getElementById('hiddTrackerVIB1').value = trackerVIB1;
					document.getElementById('hiddLinkVerVIB1').value = linkVerVIB1;
					document.getElementById('hiddLinkConocerVIB1').value = linkConocerVIB1;
					document.getElementById('hiddLinkCotizarVIB1').value = linkCotizarVIB1;
					document.getElementById('hiddImagenVIB1').value = imagenVIB1;
					if (document.getElementById('hiddNombreVB1').value != "-" || document.getElementById('hiddImagenVIB1').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB1').value != "-" || document.getElementById('hiddLinkVerVIB1').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB1').value != "-"){
						
					
					if (document.getElementById('hiddNombreVIB1').value != document.getElementById('hiddNombreVIB1X').value
							|| document.getElementById('hiddImagenVIB1').value != document.getElementById('hiddImagenVIB1X').value
							|| document.getElementById('hiddLinkCotizarVIB1').value != document.getElementById('hiddLinkCotizarVIB1X').value
							|| document.getElementById('hiddLinkVerVIB1').value != document.getElementById('hiddLinkVerVIB1X').value
							|| document.getElementById('hiddLinkCotizarVIB1').value != document.getElementById('hiddLinkCotizarVIB1X').value
							|| document.getElementById('hiddTrackerVIB1').value != document.getElementById('hiddTrackerVIB1X').value){
					   document.getElementById('hiddOrdinalVIB1').value = 1;	
					} else {
					   document.getElementById('hiddOrdinalVIB1').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVIB1').value = 0;	 
					}
					document.getElementById('hiddIdPlanVIB1').value = idPlanVIB1;
					document.getElementById('hiddNombreVIB2').value = nombreVIB2;
					document.getElementById('hiddTrackerVIB2').value = trackerVIB2;
					document.getElementById('hiddLinkVerVIB2').value = linkVerVIB2;
					document.getElementById('hiddLinkConocerVIB2').value = linkConocerVIB2;
					document.getElementById('hiddLinkCotizarVIB2').value = linkCotizarVIB2;
					document.getElementById('hiddImagenVIB2').value = imagenVIB2;
					if (document.getElementById('hiddNombreVB2').value != "-" || document.getElementById('hiddImagenVIB2').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB2').value != "-" || document.getElementById('hiddLinkVerVIB2').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB2').value != "-"){
					if (document.getElementById('hiddNombreVIB2').value != document.getElementById('hiddNombreVIB2X').value
							|| document.getElementById('hiddImagenVIB2').value != document.getElementById('hiddImagenVIB2X').value
							|| document.getElementById('hiddLinkCotizarVIB2').value != document.getElementById('hiddLinkCotizarVIB2X').value
							|| document.getElementById('hiddLinkVerVIB2').value != document.getElementById('hiddLinkVerVIB2X').value
							|| document.getElementById('hiddLinkCotizarVIB2').value != document.getElementById('hiddLinkCotizarVIB2X').value
							|| document.getElementById('hiddTrackerVIB2').value != document.getElementById('hiddTrackerVIB2X').value){
					   document.getElementById('hiddOrdinalVIB2').value = 2;	
					} else {
					   document.getElementById('hiddOrdinalVIB2').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVIB2').value = 0;	 
					}
					document.getElementById('hiddIdPlanVIB2').value = idPlanVIB2;
					document.getElementById('hiddNombreVIB3').value = nombreVIB3;
					document.getElementById('hiddTrackerVIB3').value = trackerVIB3;
					document.getElementById('hiddLinkVerVIB3').value = linkVerVIB3;
					document.getElementById('hiddLinkConocerVIB3').value = linkConocerVIB3;
					document.getElementById('hiddLinkCotizarVIB3').value = linkCotizarVIB3;
					document.getElementById('hiddImagenVIB3').value = imagenVIB3;
					if (document.getElementById('hiddNombreVB3').value != "-" || document.getElementById('hiddImagenVIB3').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB3').value != "-" || document.getElementById('hiddLinkVerVIB3').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB3').value != "-"){
					if (document.getElementById('hiddNombreVIB3').value != document.getElementById('hiddNombreVIB3X').value
							|| document.getElementById('hiddImagenVIB3').value != document.getElementById('hiddImagenVIB3X').value
							|| document.getElementById('hiddLinkCotizarVIB3').value != document.getElementById('hiddLinkCotizarVIB3X').value
							|| document.getElementById('hiddLinkVerVIB3').value != document.getElementById('hiddLinkVerVIB3X').value
							|| document.getElementById('hiddLinkCotizarVIB3').value != document.getElementById('hiddLinkCotizarVIB3X').value
							|| document.getElementById('hiddTrackerVIB3').value != document.getElementById('hiddTrackerVIB3X').value){
					   document.getElementById('hiddOrdinalVIB3').value = 3;	
					} else {
					   document.getElementById('hiddOrdinalVIB3').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVIB3').value = 0;	 
					}
					document.getElementById('hiddIdPlanVIB3').value = idPlanVIB3;
					document.getElementById('hiddNombreVIB4').value = nombreVIB4;
					document.getElementById('hiddTrackerVIB4').value = trackerVIB4;
					document.getElementById('hiddLinkVerVIB4').value = linkVerVIB4;
					document.getElementById('hiddLinkConocerVIB4').value = linkConocerVIB4;
					document.getElementById('hiddLinkCotizarVIB4').value = linkCotizarVIB4;
					document.getElementById('hiddImagenVIB4').value = imagenVIB4;
					if (document.getElementById('hiddNombreVB4').value != "-" || document.getElementById('hiddImagenVIB4').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB4').value != "-" || document.getElementById('hiddLinkVerVIB4').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB4').value != "-"){
					if (document.getElementById('hiddNombreVIB4').value != document.getElementById('hiddNombreVIB4X').value
							|| document.getElementById('hiddImagenVIB4').value != document.getElementById('hiddImagenVIB4X').value
							|| document.getElementById('hiddLinkCotizarVIB4').value != document.getElementById('hiddLinkCotizarVIB4X').value
							|| document.getElementById('hiddLinkVerVIB4').value != document.getElementById('hiddLinkVerVIB4X').value
							|| document.getElementById('hiddLinkCotizarVIB4').value != document.getElementById('hiddLinkCotizarVIB4X').value
							|| document.getElementById('hiddTrackerVIB4').value != document.getElementById('hiddTrackerVIB4X').value){
					   document.getElementById('hiddOrdinalVIB4').value = 4;	
					} else {
					   document.getElementById('hiddOrdinalVIB4').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVIB4').value = 0;	 
					}
					document.getElementById('hiddIdPlanVIB4').value = idPlanVIB4;
					document.getElementById('hiddNombreVIB5').value = nombreVIB5;
					document.getElementById('hiddTrackerVIB5').value = trackerVIB5;
					document.getElementById('hiddLinkVerVIB5').value = linkVerVIB5;
					document.getElementById('hiddLinkConocerVIB5').value = linkConocerVIB5;
					document.getElementById('hiddLinkCotizarVIB5').value = linkCotizarVIB5;
					document.getElementById('hiddImagenVIB5').value = imagenVIB5;
					if (document.getElementById('hiddNombreVB5').value != "-" || document.getElementById('hiddImagenVIB5').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB5').value != "-" || document.getElementById('hiddLinkVerVIB5').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB5').value != "-"){
					if (document.getElementById('hiddNombreVIB5').value != document.getElementById('hiddNombreVIB5X').value
							|| document.getElementById('hiddImagenVIB5').value != document.getElementById('hiddImagenVIB5X').value
							|| document.getElementById('hiddLinkCotizarVIB5').value != document.getElementById('hiddLinkCotizarVIB5X').value
							|| document.getElementById('hiddLinkVerVIB5').value != document.getElementById('hiddLinkVerVIB5X').value
							|| document.getElementById('hiddLinkCotizarVIB5').value != document.getElementById('hiddLinkCotizarVIB5X').value
							|| document.getElementById('hiddTrackerVIB5').value != document.getElementById('hiddTrackerVIB5X').value){
					   document.getElementById('hiddOrdinalVIB5').value = 5;	
					} else {
					   document.getElementById('hiddOrdinalVIB5').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVIB5').value = 0;	 
					}
					document.getElementById('hiddIdPlanVIB5').value = idPlanVIB5;
					document.getElementById('hiddNombreVIB6').value = nombreVIB6;
					document.getElementById('hiddTrackerVIB6').value = trackerVIB6;
					document.getElementById('hiddLinkVerVIB6').value = linkVerVIB6;
					document.getElementById('hiddLinkConocerVIB6').value = linkConocerVIB6;
					document.getElementById('hiddLinkCotizarVIB6').value = linkCotizarVIB6;
					document.getElementById('hiddImagenVIB6').value = imagenVIB6;
					if (document.getElementById('hiddNombreVB6').value != "-" || document.getElementById('hiddImagenVIB6').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB6').value != "-" || document.getElementById('hiddLinkVerVIB6').value != "-"
							|| document.getElementById('hiddLinkCotizarVIB6').value != "-"){
					if (document.getElementById('hiddNombreVIB6').value != document.getElementById('hiddNombreVIB6X').value
							|| document.getElementById('hiddImagenVIB6').value != document.getElementById('hiddImagenVIB6X').value
							|| document.getElementById('hiddLinkCotizarVIB6').value != document.getElementById('hiddLinkCotizarVIB6X').value
							|| document.getElementById('hiddLinkVerVIB6').value != document.getElementById('hiddLinkVerVIB6X').value
							|| document.getElementById('hiddLinkCotizarVIB6').value != document.getElementById('hiddLinkCotizarVIB6X').value
							|| document.getElementById('hiddTrackerVIB6').value != document.getElementById('hiddTrackerVIB6X').value){
					   document.getElementById('hiddOrdinalVIB6').value = 6;	
					} else {
					   document.getElementById('hiddOrdinalVIB6').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalVIB6').value = 0;	 
					}
					document.getElementById('hiddIdPlanVIB6').value = idPlanVIB6;
					
					document.getElementById('hiddRamaSB').value = rama;
					document.getElementById('hiddNombreSB1').value = nombreSB1;
					document.getElementById('hiddTrackerSB1').value = trackerSB1;
					document.getElementById('hiddLinkVerSB1').value = linkVerSB1;
					document.getElementById('hiddLinkConocerSB1').value = linkConocerSB1;
					document.getElementById('hiddLinkCotizarSB1').value = linkCotizarSB1;
					document.getElementById('hiddImagenSB1').value = imagenSB1;
					if (document.getElementById('hiddNombreSB1').value != "-" || document.getElementById('hiddImagenSB1').value != "-"
							|| document.getElementById('hiddLinkCotizarSB1').value != "-" || document.getElementById('hiddLinkVerSB1').value != "-"
							|| document.getElementById('hiddLinkCotizarSB1').value != "-"){
					if (document.getElementById('hiddNombreSB1').value != document.getElementById('hiddNombreSB1X').value
							|| document.getElementById('hiddImagenSB1').value != document.getElementById('hiddImagenSB1X').value
							|| document.getElementById('hiddLinkCotizarSB1').value != document.getElementById('hiddLinkCotizarSB1X').value
							|| document.getElementById('hiddLinkVerSB1').value != document.getElementById('hiddLinkVerSB1X').value
							|| document.getElementById('hiddLinkCotizarSB1').value != document.getElementById('hiddLinkCotizarSB1X').value
							|| document.getElementById('hiddTrackerSB1').value != document.getElementById('hiddTrackerSB1X').value){
					   document.getElementById('hiddOrdinalSB1').value = 1;	
					} else {
					   document.getElementById('hiddOrdinalSB1').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalSB1').value = 0;	 
					}
					document.getElementById('hiddIdPlanSB1').value = idPlanSB1;
					
					document.getElementById('hiddNombreSB2').value = nombreSB2;
					document.getElementById('hiddTrackerSB2').value = trackerSB2;
					document.getElementById('hiddLinkVerSB2').value = linkVerSB2;
					document.getElementById('hiddLinkConocerSB2').value = linkConocerSB2;
					document.getElementById('hiddLinkCotizarSB2').value = linkCotizarSB2;
					document.getElementById('hiddImagenSB2').value = imagenSB2;
					if (document.getElementById('hiddNombreSB2').value != "-" || document.getElementById('hiddImagenSB2').value != "-"
							|| document.getElementById('hiddLinkCotizarSB2').value != "-" || document.getElementById('hiddLinkVerSB2').value != "-"
							|| document.getElementById('hiddLinkCotizarSB2').value != "-"){
					if (document.getElementById('hiddNombreSB2').value != document.getElementById('hiddNombreSB2X').value
							|| document.getElementById('hiddImagenSB2').value != document.getElementById('hiddImagenSB2X').value
							|| document.getElementById('hiddLinkCotizarSB2').value != document.getElementById('hiddLinkCotizarSB2X').value
							|| document.getElementById('hiddLinkVerSB2').value != document.getElementById('hiddLinkVerSB2X').value
							|| document.getElementById('hiddLinkCotizarSB2').value != document.getElementById('hiddLinkCotizarSB2X').value
							|| document.getElementById('hiddTrackerSB2').value != document.getElementById('hiddTrackerSB2X').value){
					   document.getElementById('hiddOrdinalSB2').value = 2;	
					} else {
					   document.getElementById('hiddOrdinalSB2').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalSB2').value = 0;	 
					}
					document.getElementById('hiddIdPlanSB2').value = idPlanSB2;
					
					document.getElementById('hiddNombreSB3').value = nombreSB3;
					document.getElementById('hiddTrackerSB3').value = trackerSB3;
					document.getElementById('hiddLinkVerSB3').value = linkVerSB3;
					document.getElementById('hiddLinkConocerSB3').value = linkConocerSB3;
					document.getElementById('hiddLinkCotizarSB3').value = linkCotizarSB3;
					document.getElementById('hiddImagenSB3').value = imagenSB3;
					if (document.getElementById('hiddNombreSB3').value != "-" || document.getElementById('hiddImagenSB3').value != "-"
							|| document.getElementById('hiddLinkCotizarSB3').value != "-" || document.getElementById('hiddLinkVerSB3').value != "-"
							|| document.getElementById('hiddLinkCotizarSB3').value != "-"){
					if (document.getElementById('hiddNombreSB3').value != document.getElementById('hiddNombreSB3X').value
							|| document.getElementById('hiddImagenSB3').value != document.getElementById('hiddImagenSB3X').value
							|| document.getElementById('hiddLinkCotizarSB3').value != document.getElementById('hiddLinkCotizarSB3X').value
							|| document.getElementById('hiddLinkVerSB3').value != document.getElementById('hiddLinkVerSB3X').value
							|| document.getElementById('hiddLinkCotizarSB3').value != document.getElementById('hiddLinkCotizarSB3X').value
							|| document.getElementById('hiddTrackerSB3').value != document.getElementById('hiddTrackerSB3X').value){
					   document.getElementById('hiddOrdinalSB3').value = 3;	
					} else {
					   document.getElementById('hiddOrdinalSB3').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalSB3').value = 0;	 
					}
					document.getElementById('hiddIdPlanSB3').value = idPlanSB3;
					document.getElementById('hiddNombreSB4').value = nombreSB4;
					document.getElementById('hiddTrackerSB4').value = trackerSB4;
					document.getElementById('hiddLinkVerSB4').value = linkVerSB4;
					document.getElementById('hiddLinkConocerSB4').value = linkConocerSB4;
					document.getElementById('hiddLinkCotizarSB4').value = linkCotizarSB4;
					document.getElementById('hiddImagenSB4').value = imagenSB4;
					if (document.getElementById('hiddNombreSB4').value != "-" || document.getElementById('hiddImagenSB4').value != "-"
							|| document.getElementById('hiddLinkCotizarSB4').value != "-" || document.getElementById('hiddLinkVerSB4').value != "-"
							|| document.getElementById('hiddLinkCotizarSB4').value != "-"){
					if (document.getElementById('hiddNombreSB4').value != document.getElementById('hiddNombreSB4X').value
							|| document.getElementById('hiddImagenSB4').value != document.getElementById('hiddImagenSB4X').value
							|| document.getElementById('hiddLinkCotizarSB4').value != document.getElementById('hiddLinkCotizarSB4X').value
							|| document.getElementById('hiddLinkVerSB4').value != document.getElementById('hiddLinkVerSB4X').value
							|| document.getElementById('hiddLinkCotizarSB4').value != document.getElementById('hiddLinkCotizarSB4X').value
							|| document.getElementById('hiddTrackerSB4').value != document.getElementById('hiddTrackerSB4X').value){
					   document.getElementById('hiddOrdinalSB4').value = 4;	
					} else {
					   document.getElementById('hiddOrdinalSB4').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalSB4').value = 0;	 
					}
					document.getElementById('hiddIdPlanSB4').value = idPlanSB4;
					document.getElementById('hiddNombreSB5').value = nombreSB5;
					document.getElementById('hiddTrackerSB5').value = trackerSB5;
					document.getElementById('hiddLinkVerSB5').value = linkVerSB5;
					document.getElementById('hiddLinkConocerSB5').value = linkConocerSB5;
					document.getElementById('hiddLinkCotizarSB5').value = linkCotizarSB5;
					document.getElementById('hiddImagenSB5').value = imagenSB5;
					if (document.getElementById('hiddNombreSB4').value != "-" || document.getElementById('hiddImagenSB4').value != "-"
							|| document.getElementById('hiddLinkCotizarSB4').value != "-" || document.getElementById('hiddLinkVerSB4').value != "-"
							|| document.getElementById('hiddLinkCotizarSB4').value != "-"){
					if (document.getElementById('hiddNombreSB5').value != document.getElementById('hiddNombreSB5X').value
							|| document.getElementById('hiddImagenSB5').value != document.getElementById('hiddImagenSB5X').value
							|| document.getElementById('hiddLinkCotizarSB5').value != document.getElementById('hiddLinkCotizarSB5X').value
							|| document.getElementById('hiddLinkVerSB5').value != document.getElementById('hiddLinkVerSB5X').value
							|| document.getElementById('hiddLinkCotizarSB5').value != document.getElementById('hiddLinkCotizarSB5X').value
							|| document.getElementById('hiddTrackerSB5').value != document.getElementById('hiddTrackerSB5X').value){
					   document.getElementById('hiddOrdinalSB5').value = 5;	
					} else {
					   document.getElementById('hiddOrdinalSB5').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalSB5').value = 0;	 
					}
					document.getElementById('hiddIdPlanSB5').value = idPlanSB5;
					document.getElementById('hiddNombreSB6').value = nombreSB6;
					document.getElementById('hiddTrackerSB6').value = trackerSB6;
					document.getElementById('hiddLinkVerSB6').value = linkVerSB6;
					document.getElementById('hiddLinkConocerSB6').value = linkConocerSB6;
					document.getElementById('hiddLinkCotizarSB6').value = linkCotizarSB6;
					document.getElementById('hiddImagenSB6').value = imagenSB6;
					if (document.getElementById('hiddNombreSB6').value != "-" || document.getElementById('hiddImagenSB6').value != "-"
							|| document.getElementById('hiddLinkCotizarSB6').value != "-" || document.getElementById('hiddLinkVerSB6').value != "-"
							|| document.getElementById('hiddLinkCotizarSB6').value != "-"){
					if (document.getElementById('hiddNombreSB6').value != document.getElementById('hiddNombreSB6X').value
							|| document.getElementById('hiddImagenSB6').value != document.getElementById('hiddImagenSB6X').value
							|| document.getElementById('hiddLinkCotizarSB6').value != document.getElementById('hiddLinkCotizarSB6X').value
							|| document.getElementById('hiddLinkVerSB6').value != document.getElementById('hiddLinkVerSB6X').value
							|| document.getElementById('hiddLinkCotizarSB6').value != document.getElementById('hiddLinkCotizarSB6X').value
							|| document.getElementById('hiddTrackerSB6').value != document.getElementById('hiddTrackerSB6X').value){
					   document.getElementById('hiddOrdinalSB6').value = 6;	
					} else {
					   document.getElementById('hiddOrdinalSB6').value = 0;	 
					}
					} else {
					   document.getElementById('hiddOrdinalSB6').value = 0;	 
					}
					document.getElementById('hiddIdPlanSB6').value = idPlanSB6;
					
					document.getElementById('hiddSubmit').value = "0";
					$("#informeComercialTarjetas").submit()
					
					
			}
			
		};
		
	
	
</script>
<script type="text/javascript">

	function abrirImagen(url){
        $.colorbox({width:"600", height:"300",  iframe:true, href:url});
    }


</script>

</head>

<body>
	<html:form action="/administrador-promociones-rama.do" method="post" styleId="informeComercialTarjetas">
	<input id="id_ficha" type="hidden" value="" name="id_ficha">
			<table class="datos" width="800" cellspacing="0" cellpadding="0" border="0" align="center">
				<tbody>
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" border="0" width="100%" style="float:left; border: 1px solid #ffffff; padding: 15px 15px 40px;" >
								
								<tr>
									<td colspan="4">
									<h2> P�gina por Ramo </h2>
									</td>
								</tr>
								<tr>
									<th width="220"> Seleccionar p�gina : </th>
									<td width="200">
										<select id="rama" name="rama" onChange="cargarSub();">
											
											<option value="0">Seleccionar</option>
											<option value="1">Promociones Auto</option>
											<option value="2">Promociones Hogar</option>
											<option value="3">Promociones Vida</option>
											<option value="4">Promociones Salud</option>
											
										</select>
									</td>
									<th width="80"> Ubicaci�n : </th>
									<td>
										<select id="subcategoria" name="subcategoria" onChange="cargarPagina();">
											<option value="0"> Seleccionar</option>
											<option value="1">Banner Principal</option>
											<option value="2">Banner Secundario</option>
											
										</select>
									</td>
								</tr>
							</table>
							<div id="principal-auto">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Auto - Banner Principal  </th>
										
									</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlan" type="text" value="<bean:write name="idPlan"></bean:write>" size="12" name="idPlan">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombre" class="hasDatepicker" type="text" value="<bean:write name="nombre"></bean:write>" size="12" name="nombre"/>
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="tracker" class="hasDatepicker" type="text" value="<bean:write name="tracker"></bean:write>" size="12" name="tracker"/>
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagen" class="hasDatepicker" type="text" value="<bean:write name="imagen"></bean:write>" size="12" name="imagen"/>
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="link" class="hasDatepicker" type="text" value="<bean:write name="link"></bean:write>" size="12" name="link"/>
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesde)" name="informeComercialTarjetas" styleId="inputFechaDesde" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHasta)" name="informeComercialTarjetas" styleId="inputFechaHasta" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%"> 
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagen').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
									<td>
										
									</td>
								</tr>
								
							</table>
							<html:hidden property="datos(hiddIdPlan)" value="1" styleId="hiddIdPlan"/>
							<html:hidden property="datos(hiddNombre)" value="1" styleId="hiddNombre"/>
							<html:hidden property="datos(hiddLink)" value="1" styleId="hiddLink"/>
							<html:hidden property="datos(hiddTracker)" value="1" styleId="hiddTracker"/>
							<html:hidden property="datos(hiddImagen)" value="1" styleId="hiddImagen"/>
							<html:hidden property="datos(hiddRama)" value="1" styleId="hiddRama"/>
							<html:hidden property="datos(hiddSubcategoria)" value="1" styleId="hiddSubcategoria"/>
							
							<html:hidden property="datos(hiddIdPlanX)" value="<bean:write name='idPlan'></bean:write>" styleId="hiddIdPlanX"/>
							<html:hidden property="datos(hiddNombreX)" value="<bean:write name='nombre'></bean:write>" styleId="hiddNombreX"/>
							<html:hidden property="datos(hiddLinkX)" value="<bean:write name='link'></bean:write>" styleId="hiddLinkX"/>
							<html:hidden property="datos(hiddTrackerX)" value="<bean:write name='tracker'></bean:write>" styleId="hiddTrackerX"/>
							<html:hidden property="datos(hiddImagenX)" value="<bean:write name='imagen'></bean:write>" styleId="hiddImagenX"/>
							</div>
							<div id="principal-hogar">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Hogar - Banner Principal  </th>
										
									</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanH" type="text" value="<bean:write name="idPlanH"></bean:write>" size="12" name="idPlan">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreH" class="hasDatepicker" type="text" value="<bean:write name="nombreH"></bean:write>" size="12" name="nombre"/>
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerH" class="hasDatepicker" type="text" value="<bean:write name="trackerH"></bean:write>" size="12" name="tracker"/>
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenH" class="hasDatepicker" type="text" value="<bean:write name="imagenH"></bean:write>" size="12" name="imagen"/>
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="linkH" class="hasDatepicker" type="text" value="<bean:write name="linkH"></bean:write>" size="12" name="link"/>
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeH)" name="informeComercialTarjetas" styleId="inputFechaDesdeHP" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaH)" name="informeComercialTarjetas" styleId="inputFechaHastaHP" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%"> 
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenH').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
									<td>
										
									</td>
								</tr>
								
							</table>
							<html:hidden property="datos(hiddIdPlanH)" value="1" styleId="hiddIdPlanH"/>
							<html:hidden property="datos(hiddNombreH)" value="1" styleId="hiddNombreH"/>
							<html:hidden property="datos(hiddLinkH)" value="1" styleId="hiddLinkH"/>
							<html:hidden property="datos(hiddTrackerH)" value="1" styleId="hiddTrackerH"/>
							<html:hidden property="datos(hiddImagenH)" value="1" styleId="hiddImagenH"/>
							<html:hidden property="datos(hiddRamaH)" value="1" styleId="hiddRamaH"/>
							<html:hidden property="datos(hiddSubcategoriaH)" value="1" styleId="hiddSubcategoriaH"/>
							
							<html:hidden property="datos(hiddIdPlanHX)" value="<bean:write name='idPlanH'></bean:write>" styleId="hiddIdPlanHX"/>
							<html:hidden property="datos(hiddNombreHX)" value="<bean:write name='nombreH'></bean:write>" styleId="hiddNombreHX"/>
							<html:hidden property="datos(hiddLinkHX)" value="<bean:write name='linkH'></bean:write>" styleId="hiddLinkHX"/>
							<html:hidden property="datos(hiddTrackerHX)" value="<bean:write name='trackerH'></bean:write>" styleId="hiddTrackerHX"/>
							<html:hidden property="datos(hiddImagenHX)" value="<bean:write name='imagenH'></bean:write>" styleId="hiddImagenHX"/>
							</div>
							<div id="principal-vida">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Vida y Salud - Banner Principal  </th>
										
									</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanV" type="text" value="<bean:write name="idPlanV"></bean:write>" size="12" name="idPlan">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreV" class="hasDatepicker" type="text" value="<bean:write name="nombreV"></bean:write>" size="12" name="nombre"/>
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerV" class="hasDatepicker" type="text" value="<bean:write name="trackerV"></bean:write>" size="12" name="tracker"/>
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenV" class="hasDatepicker" type="text" value="<bean:write name="imagenV"></bean:write>" size="12" name="imagen"/>
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="linkV" class="hasDatepicker" type="text" value="<bean:write name="linkV"></bean:write>" size="12" name="link"/>
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeV)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIP" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaV)" name="informeComercialTarjetas" styleId="inputFechaHastaVIP" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%"> 
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenV').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
									<td>
										
									</td>
								</tr>
								
							</table>
							<html:hidden property="datos(hiddIdPlanV)" value="1" styleId="hiddIdPlanV"/>
							<html:hidden property="datos(hiddNombreV)" value="1" styleId="hiddNombreV"/>
							<html:hidden property="datos(hiddLinkV)" value="1" styleId="hiddLinkV"/>
							<html:hidden property="datos(hiddTrackerV)" value="1" styleId="hiddTrackerV"/>
							<html:hidden property="datos(hiddImagenV)" value="1" styleId="hiddImagenV"/>
							<html:hidden property="datos(hiddRamaV)" value="1" styleId="hiddRamaV"/>
							<html:hidden property="datos(hiddSubcategoriaV)" value="1" styleId="hiddSubcategoriaV"/>		
							
							<html:hidden property="datos(hiddIdPlanVX)" value="<bean:write name='idPlanV'></bean:write>" styleId="hiddIdPlanVX"/>
							<html:hidden property="datos(hiddNombreVX)" value="<bean:write name='nombreV'></bean:write>" styleId="hiddNombreVX"/>
							<html:hidden property="datos(hiddLinkVX)" value="<bean:write name='linkV'></bean:write>" styleId="hiddLinkVX"/>
							<html:hidden property="datos(hiddTrackerVX)" value="<bean:write name='trackerV'></bean:write>" styleId="hiddTrackerVX"/>
							<html:hidden property="datos(hiddImagenVX)" value="<bean:write name='imagenV'></bean:write>" styleId="hiddImagenVX"/>		
							</div>
							<div id="principal-salud">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Salud - Banner Principal  </th>
										
									</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanS" type="text" value="<bean:write name="idPlanS"></bean:write>" size="12" name="idPlan">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreS" class="hasDatepicker" type="text" value="<bean:write name="nombreS"></bean:write>" size="12" name="nombre"/>
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerS" class="hasDatepicker" type="text" value="<bean:write name="trackerS"></bean:write>" size="12" name="tracker"/>
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenS" class="hasDatepicker" type="text" value="<bean:write name="imagenS"></bean:write>" size="12" name="imagen"/>
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="linkS" class="hasDatepicker" type="text" value="<bean:write name="linkS"></bean:write>" size="12" name="link"/>
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeS)" name="informeComercialTarjetas" styleId="inputFechaDesdeSP" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaS)" name="informeComercialTarjetas" styleId="inputFechaHastaSP" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%"> 
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenS').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
									<td>
										
									</td>
								</tr>
								
							</table>	
							<html:hidden property="datos(hiddIdPlanS)" value="1" styleId="hiddIdPlanS"/>
							<html:hidden property="datos(hiddNombreS)" value="1" styleId="hiddNombreS"/>
							<html:hidden property="datos(hiddLinkS)" value="1" styleId="hiddLinkS"/>
							<html:hidden property="datos(hiddTrackerS)" value="1" styleId="hiddTrackerS"/>
							<html:hidden property="datos(hiddImagenS)" value="1" styleId="hiddImagenS"/>
							<html:hidden property="datos(hiddRamaS)" value="1" styleId="hiddRamaS"/>
							<html:hidden property="datos(hiddSubcategoriaS)" value="1" styleId="hiddSubcategoriaS"/>		
							
							<html:hidden property="datos(hiddIdPlanSX)" value="<bean:write name='idPlanS'></bean:write>" styleId="hiddIdPlanSX"/>
							<html:hidden property="datos(hiddNombreSX)" value="<bean:write name='nombreS'></bean:write>" styleId="hiddNombreSX"/>
							<html:hidden property="datos(hiddLinkSX)" value="<bean:write name='linkS'></bean:write>" styleId="hiddLinkSX"/>
							<html:hidden property="datos(hiddTrackerSX)" value="<bean:write name='trackerS'></bean:write>" styleId="hiddTrackerSX"/>
							<html:hidden property="datos(hiddImagenSX)" value="<bean:write name='imagenS'></bean:write>" styleId="hiddImagenSX"/>		
							</div>
							<!-- Secundarios -->
							<div id="secundario-auto">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Auto - Banner Secundarios  </th>
										
									</tr>
							</table>
							
							
							<!-- Banner 1 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 1 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB1" type="text" value="<bean:write name="idPlanVB1"></bean:write>" size="12" name="idPlanVB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB1" class="hasDatepicker" type="text" value="<bean:write name="nombreVB1"></bean:write>" size="12" name="nombreVB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB1" class="hasDatepicker" type="text" value="<bean:write name="trackerVB1"></bean:write>" size="12" name="trackerVB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB1" class="hasDatepicker" type="text" value="<bean:write name="imagenVB1"></bean:write>" size="12" name="imagenVB1">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB1" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB1"></bean:write>" size="12" name="linkVerBasesVB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVB1" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB1"></bean:write>" size="12" name="linkConocerVB1">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB1" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB1"></bean:write>" size="12" name="linkCotizarVB1">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVB1)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB1" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVB1)" name="informeComercialTarjetas" styleId="inputFechaHastaVB1" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB1').value);">
								</td>
								<html:hidden property="datos(hiddRamaVB)" value="1" styleId="hiddRamaVB"/>
								<html:hidden property="datos(hiddIdPlanVB1)" value="1" styleId="hiddIdPlanVB1"/>
								<html:hidden property="datos(hiddNombreVB1)" value="1" styleId="hiddNombreVB1"/>
								<html:hidden property="datos(hiddLinkVerVB1)" value="1" styleId="hiddLinkVerVB1"/>
								<html:hidden property="datos(hiddLinkConocerVB1)" value="1" styleId="hiddLinkConocerVB1"/>
								<html:hidden property="datos(hiddLinkCotizarVB1)" value="1" styleId="hiddLinkCotizarVB1"/>
								<html:hidden property="datos(hiddTrackerVB1)" value="1" styleId="hiddTrackerVB1"/>
								<html:hidden property="datos(hiddImagenVB1)" value="1" styleId="hiddImagenVB1"/>
								<html:hidden property="datos(hiddPrincipalVB1)" value="1" styleId="hiddPrincipalVB1"/>
								<html:hidden property="datos(hiddOrdinalVB1)" value="1" styleId="hiddOrdinalVB1"/>
								
								<html:hidden property="datos(hiddIdPlanVB1X)" value="<bean:write name='idPlanVB1'></bean:write>" styleId="hiddIdPlanVB1X"/>
								<html:hidden property="datos(hiddLinkVerVB1X)" value="<bean:write name='linkVerBasesVB1'></bean:write>" styleId="hiddLinkVerVB1X"/>
								<html:hidden property="datos(hiddLinkConocerVB1X)" value="<bean:write name='linkConocerVB1'></bean:write>" styleId="hiddLinkConocerVB1X"/>
								<html:hidden property="datos(hiddLinkCotizarVB1X)" value="<bean:write name='linkCotizarVB1'></bean:write>" styleId="hiddLinkCotizarVB1X"/>
								<html:hidden property="datos(hiddTrackerVB1X)" value="<bean:write name='trackerVB1'></bean:write>" styleId="hiddTrackerVB1X"/>
								<html:hidden property="datos(hiddImagenVB1X)" value="<bean:write name='imagenVB1'></bean:write>" styleId="hiddImagenVB1X"/>
								<html:hidden property="datos(hiddNombreVB1X)" value="<bean:write name='nombreVB1'></bean:write>" styleId="hiddNombreVB1X"/>
							</table>
							
							
							
							<!-- Banner 2 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 2 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB2" type="text" value="<bean:write name="idPlanVB2"></bean:write>" size="12" name="idPlanVB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB2" class="hasDatepicker" type="text" value="<bean:write name="nombreVB2"></bean:write>" size="12" name="nombreVB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB2" class="hasDatepicker" type="text" value="<bean:write name="trackerVB2"></bean:write>" size="12" name="trackerVB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB2" class="hasDatepicker" type="text" value="<bean:write name="imagenVB2"></bean:write>" size="12" name="imagenVB2">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB2" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB2"></bean:write>" size="12" name="linkVerBasesVB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVB2" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB2"></bean:write>" size="12" name="linkConocerVB2">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB2" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB2"></bean:write>" size="12" name="linkCotizarVB2">
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVB2)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB2" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVB2)" name="informeComercialTarjetas" styleId="inputFechaHastaVB2" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB2').value);">
								</td>
								<html:hidden property="datos(hiddRamaVB)" value="1" styleId="hiddRamaVB"/>
								<html:hidden property="datos(hiddIdPlanVB2)" value="1" styleId="hiddIdPlanVB2"/>
								<html:hidden property="datos(hiddNombreVB2)" value="1" styleId="hiddNombreVB2"/>
								<html:hidden property="datos(hiddLinkVerVB2)" value="1" styleId="hiddLinkVerVB2"/>
								<html:hidden property="datos(hiddLinkConocerVB2)" value="1" styleId="hiddLinkConocerVB2"/>
								<html:hidden property="datos(hiddLinkCotizarVB2)" value="1" styleId="hiddLinkCotizarVB2"/>
								<html:hidden property="datos(hiddTrackerVB2)" value="1" styleId="hiddTrackerVB2"/>
								<html:hidden property="datos(hiddImagenVB2)" value="1" styleId="hiddImagenVB2"/>
								<html:hidden property="datos(hiddPrincipalVB2)" value="1" styleId="hiddPrincipalVB2"/>
								<html:hidden property="datos(hiddOrdinalVB2)" value="1" styleId="hiddOrdinalVB2"/>
								
								<html:hidden property="datos(hiddIdPlanVB2X)" value="<bean:write name='idPlanVB2'></bean:write>" styleId="hiddIdPlanVB2X"/>
								<html:hidden property="datos(hiddLinkVerVB2X)" value="<bean:write name='linkVerBasesVB2'></bean:write>" styleId="hiddLinkVerVB2X"/>
								<html:hidden property="datos(hiddLinkConocerVB2X)" value="<bean:write name='linkConocerVB2'></bean:write>" styleId="hiddLinkConocerVB2X"/>
								<html:hidden property="datos(hiddLinkCotizarVB2X)" value="<bean:write name='linkCotizarVB2'></bean:write>" styleId="hiddLinkCotizarVB2X"/>
								<html:hidden property="datos(hiddTrackerVB2X)" value="<bean:write name='trackerVB2'></bean:write>" styleId="hiddTrackerVB2X"/>
								<html:hidden property="datos(hiddImagenVB2X)" value="<bean:write name='imagenVB2'></bean:write>" styleId="hiddImagenVB2X"/>
								<html:hidden property="datos(hiddNombreVB2X)" value="<bean:write name='nombreVB2'></bean:write>" styleId="hiddNombreVB2X"/>
							</table>
							
							
							
							<!-- Banner 3 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 3 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB3" type="text" value="<bean:write name="idPlanVB3"></bean:write>" size="12" name="idPlanVB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB3" class="hasDatepicker" type="text" value="<bean:write name="nombreVB3"></bean:write>" size="12" name="nombreVB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB3" class="hasDatepicker" type="text" value="<bean:write name="trackerVB3"></bean:write>" size="12" name="trackerVB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB3" class="hasDatepicker" type="text" value="<bean:write name="imagenVB3"></bean:write>" size="12" name="imagenVB3">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB3" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB3"></bean:write>" size="12" name="linkVerBasesVB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVB3" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB3"></bean:write>" size="12" name="linkConocerVB3">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB3" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB3"></bean:write>" size="12" name="linkCotizarVB3">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVB3)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB3" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVB3)" name="informeComercialTarjetas" styleId="inputFechaHastaVB3" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB3').value);">
								</td>
								<html:hidden property="datos(hiddRamaVB)" value="1" styleId="hiddRamaVB"/>
								<html:hidden property="datos(hiddIdPlanVB3)" value="1" styleId="hiddIdPlanVB3"/>
								<html:hidden property="datos(hiddNombreVB3)" value="1" styleId="hiddNombreVB3"/>
								<html:hidden property="datos(hiddLinkVerVB3)" value="1" styleId="hiddLinkVerVB3"/>
								<html:hidden property="datos(hiddLinkConocerVB3)" value="1" styleId="hiddLinkConocerVB3"/>
								<html:hidden property="datos(hiddLinkCotizarVB3)" value="1" styleId="hiddLinkCotizarVB3"/>
								<html:hidden property="datos(hiddTrackerVB3)" value="1" styleId="hiddTrackerVB3"/>
								<html:hidden property="datos(hiddImagenVB3)" value="1" styleId="hiddImagenVB3"/>
								<html:hidden property="datos(hiddPrincipalVB3)" value="1" styleId="hiddPrincipalVB3"/>
								<html:hidden property="datos(hiddOrdinalVB3)" value="1" styleId="hiddOrdinalVB3"/>
								
								<html:hidden property="datos(hiddIdPlanVB3X)" value="<bean:write name='idPlanVB3'></bean:write>" styleId="hiddIdPlanVB3X"/>
								<html:hidden property="datos(hiddLinkVerVB3X)" value="<bean:write name='linkVerBasesVB3'></bean:write>" styleId="hiddLinkVerVB3X"/>
								<html:hidden property="datos(hiddLinkConocerVB3X)" value="<bean:write name='linkConocerVB3'></bean:write>" styleId="hiddLinkConocerVB3X"/>
								<html:hidden property="datos(hiddLinkCotizarVB3X)" value="<bean:write name='linkCotizarVB3'></bean:write>" styleId="hiddLinkCotizarVB3X"/>
								<html:hidden property="datos(hiddTrackerVB3X)" value="<bean:write name='trackerVB3'></bean:write>" styleId="hiddTrackerVB3X"/>
								<html:hidden property="datos(hiddImagenVB3X)" value="<bean:write name='imagenVB3'></bean:write>" styleId="hiddImagenVB3X"/>
								<html:hidden property="datos(hiddNombreVB3X)" value="<bean:write name='nombreVB3'></bean:write>" styleId="hiddNombreVB3X"/>
							</table>
							
							
							<!-- Banner 4 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 4 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB4" type="text" value="<bean:write name="idPlanVB4"></bean:write>" size="12" name="idPlanVB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB4" class="hasDatepicker" type="text" value="<bean:write name="nombreVB4"></bean:write>" size="12" name="nombreVB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB4" class="hasDatepicker" type="text" value="<bean:write name="trackerVB4"></bean:write>" size="12" name="trackerVB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB4" class="hasDatepicker" type="text" value="<bean:write name="imagenVB4"></bean:write>" size="12" name="imagenVB4">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB4" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB4"></bean:write>" size="12" name="linkVerBasesVB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVB4" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB4"></bean:write>" size="12" name="linkConocerVB4">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB4" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB4"></bean:write>" size="12" name="linkCotizarVB4">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVB4)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB4" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVB4)" name="informeComercialTarjetas" styleId="inputFechaHastaVB4" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB4').value);">
								</td>
								<html:hidden property="datos(hiddRamaVB)" value="1" styleId="hiddRamaVB"/>
								<html:hidden property="datos(hiddIdPlanVB4)" value="1" styleId="hiddIdPlanVB4"/>
								<html:hidden property="datos(hiddNombreVB4)" value="1" styleId="hiddNombreVB4"/>
								<html:hidden property="datos(hiddLinkVerVB4)" value="1" styleId="hiddLinkVerVB4"/>
								<html:hidden property="datos(hiddLinkConocerVB4)" value="1" styleId="hiddLinkConocerVB4"/>
								<html:hidden property="datos(hiddLinkCotizarVB4)" value="1" styleId="hiddLinkCotizarVB4"/>
								<html:hidden property="datos(hiddTrackerVB4)" value="1" styleId="hiddTrackerVB4"/>
								<html:hidden property="datos(hiddImagenVB4)" value="1" styleId="hiddImagenVB4"/>
								<html:hidden property="datos(hiddPrincipalVB4)" value="1" styleId="hiddPrincipalVB4"/>
								<html:hidden property="datos(hiddOrdinalVB4)" value="1" styleId="hiddOrdinalVB4"/>
								
								<html:hidden property="datos(hiddIdPlanVB4X)" value="<bean:write name='idPlanVB4'></bean:write>" styleId="hiddIdPlanVB4X"/>
								<html:hidden property="datos(hiddLinkVerVB4X)" value="<bean:write name='linkVerBasesVB4'></bean:write>" styleId="hiddLinkVerVB4X"/>
								<html:hidden property="datos(hiddLinkConocerVB4X)" value="<bean:write name='linkConocerVB4'></bean:write>" styleId="hiddLinkConocerVB4X"/>
								<html:hidden property="datos(hiddLinkCotizarVB4X)" value="<bean:write name='linkCotizarVB4'></bean:write>" styleId="hiddLinkCotizarVB4X"/>
								<html:hidden property="datos(hiddTrackerVB4X)" value="<bean:write name='trackerVB4'></bean:write>" styleId="hiddTrackerVB4X"/>
								<html:hidden property="datos(hiddImagenVB4X)" value="<bean:write name='imagenVB4'></bean:write>" styleId="hiddImagenVB4X"/>
								<html:hidden property="datos(hiddNombreVB4X)" value="<bean:write name='nombreVB4'></bean:write>" styleId="hiddNombreVB4X"/>
							</table>
							
							
							<!-- Banner 5 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 5 </th>
									<td width="55">
									
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB5" type="text" value="<bean:write name="idPlanVB5"></bean:write>" size="12" name="idPlanVB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB5" class="hasDatepicker" type="text" value="<bean:write name="nombreVB5"></bean:write>" size="12" name="nombreVB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB5" class="hasDatepicker" type="text" value="<bean:write name="trackerVB5"></bean:write>" size="12" name="trackerVB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB5" class="hasDatepicker" type="text" value="<bean:write name="imagenVB5"></bean:write>" size="12" name="imagenVB5">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB5" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB5"></bean:write>" size="12" name="linkVerBasesVB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVB5" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB5"></bean:write>" size="12" name="linkConocerVB5">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB5" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB5"></bean:write>" size="12" name="linkCotizarVB5">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVB5)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB5" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVB5)" name="informeComercialTarjetas" styleId="inputFechaHastaVB5" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB5').value);">
								</td>
								<html:hidden property="datos(hiddRamaVB)" value="1" styleId="hiddRamaVB"/>
								<html:hidden property="datos(hiddIdPlanVB5)" value="1" styleId="hiddIdPlanVB5"/>
								<html:hidden property="datos(hiddNombreVB5)" value="1" styleId="hiddNombreVB5"/>
								<html:hidden property="datos(hiddLinkVerVB5)" value="1" styleId="hiddLinkVerVB5"/>
								<html:hidden property="datos(hiddLinkConocerVB5)" value="1" styleId="hiddLinkConocerVB5"/>
								<html:hidden property="datos(hiddLinkCotizarVB5)" value="1" styleId="hiddLinkCotizarVB5"/>
								<html:hidden property="datos(hiddTrackerVB5)" value="1" styleId="hiddTrackerVB5"/>
								<html:hidden property="datos(hiddImagenVB5)" value="1" styleId="hiddImagenVB5"/>
								<html:hidden property="datos(hiddPrincipalVB5)" value="1" styleId="hiddPrincipalVB5"/>
								<html:hidden property="datos(hiddOrdinalVB5)" value="1" styleId="hiddOrdinalVB5"/>
								
								<html:hidden property="datos(hiddIdPlanVB5X)" value="<bean:write name='idPlanVB5'></bean:write>" styleId="hiddIdPlanVB5X"/>
								<html:hidden property="datos(hiddLinkVerVB5X)" value="<bean:write name='linkVerBasesVB5'></bean:write>" styleId="hiddLinkVerVB5X"/>
								<html:hidden property="datos(hiddLinkConocerVB5X)" value="<bean:write name='linkConocerVB5'></bean:write>" styleId="hiddLinkConocerVB5X"/>
								<html:hidden property="datos(hiddLinkCotizarVB5X)" value="<bean:write name='linkCotizarVB5'></bean:write>" styleId="hiddLinkCotizarVB5X"/>
								<html:hidden property="datos(hiddTrackerVB5X)" value="<bean:write name='trackerVB5'></bean:write>" styleId="hiddTrackerVB5X"/>
								<html:hidden property="datos(hiddImagenVB5X)" value="<bean:write name='imagenVB5'></bean:write>" styleId="hiddImagenVB5X"/>
								<html:hidden property="datos(hiddNombreVB5X)" value="<bean:write name='nombreVB5'></bean:write>" styleId="hiddNombreVB5X"/>
							</table>
							
							
							<!-- Banner 6 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 6 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB6" type="text" value="<bean:write name="idPlanVB6"></bean:write>" size="12" name="idPlanVB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB6" class="hasDatepicker" type="text" value="<bean:write name="nombreVB6"></bean:write>" size="12" name="nombreVB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 256); padding: 16px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB6" class="hasDatepicker" type="text" value="<bean:write name="trackerVB6"></bean:write>" size="12" name="trackerVB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB6" class="hasDatepicker" type="text" value="<bean:write name="imagenVB6"></bean:write>" size="12" name="imagenVB6">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB6" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB6"></bean:write>" size="12" name="linkVerBasesVB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVB6" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB6"></bean:write>" size="12" name="linkConocerVB6">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB6" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB6"></bean:write>" size="12" name="linkCotizarVB6">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVB6)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB6" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVB6)" name="informeComercialTarjetas" styleId="inputFechaHastaVB6" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB6').value);">
								</td>
								<html:hidden property="datos(hiddRamaVB)" value="1" styleId="hiddRamaVB"/>
								<html:hidden property="datos(hiddIdPlanVB6)" value="1" styleId="hiddIdPlanVB6"/>
								<html:hidden property="datos(hiddNombreVB6)" value="1" styleId="hiddNombreVB6"/>
								<html:hidden property="datos(hiddLinkVerVB6)" value="1" styleId="hiddLinkVerVB6"/>
								<html:hidden property="datos(hiddLinkConocerVB6)" value="1" styleId="hiddLinkConocerVB6"/>
								<html:hidden property="datos(hiddLinkCotizarVB6)" value="1" styleId="hiddLinkCotizarVB6"/>
								<html:hidden property="datos(hiddTrackerVB6)" value="1" styleId="hiddTrackerVB6"/>
								<html:hidden property="datos(hiddImagenVB6)" value="1" styleId="hiddImagenVB6"/>
								<html:hidden property="datos(hiddPrincipalVB6)" value="1" styleId="hiddPrincipalVB6"/>
								<html:hidden property="datos(hiddOrdinalVB6)" value="1" styleId="hiddOrdinalVB6"/>
								
								<html:hidden property="datos(hiddIdPlanVB6X)" value="<bean:write name='idPlanVB6'></bean:write>" styleId="hiddIdPlanVB6X"/>
								<html:hidden property="datos(hiddLinkVerVB6X)" value="<bean:write name='linkVerBasesVB6'></bean:write>" styleId="hiddLinkVerVB6X"/>
								<html:hidden property="datos(hiddLinkConocerVB6X)" value="<bean:write name='linkConocerVB6'></bean:write>" styleId="hiddLinkConocerVB6X"/>
								<html:hidden property="datos(hiddLinkCotizarVB6X)" value="<bean:write name='linkCotizarVB6'></bean:write>" styleId="hiddLinkCotizarVB6X"/>
								<html:hidden property="datos(hiddTrackerVB6X)" value="<bean:write name='trackerVB6'></bean:write>" styleId="hiddTrackerVB6X"/>
								<html:hidden property="datos(hiddImagenVB6X)" value="<bean:write name='imagenVB6'></bean:write>" styleId="hiddImagenVB6X"/>
								<html:hidden property="datos(hiddNombreVB6X)" value="<bean:write name='nombreVB6'></bean:write>" styleId="hiddNombreVB6X"/>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
							</table>
							</div>
						<!-- HOGAR -->	
							<div id="secundario-hogar">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Hogar - Banner Secundarios  </th>
										
									</tr>
							</table>
							
							
							<!-- Banner 1 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 1 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB1" type="text" value="<bean:write name="idPlanHB1"></bean:write>" size="12" name="idPlanHB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB1" class="hasDatepicker" type="text" value="<bean:write name="nombreHB1"></bean:write>" size="12" name="nombreHB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB1" class="hasDatepicker" type="text" value="<bean:write name="trackerHB1"></bean:write>" size="12" name="trackerHB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB1" class="hasDatepicker" type="text" value="<bean:write name="imagenHB1"></bean:write>" size="12" name="imagenHB1">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB1" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB1"></bean:write>" size="12" name="linkVerBasesHB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerHB1" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB1"></bean:write>" size="12" name="linkConocerVB1">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB1" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB1"></bean:write>" size="12" name="linkCotizarHB1">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeHB1)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB1" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaHB1)" name="informeComercialTarjetas" styleId="inputFechaHastaHB1" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB1').value);">
								</td>
								<html:hidden property="datos(hiddRamaHB)" value="1" styleId="hiddRamaHB"/>
								<html:hidden property="datos(hiddIdPlanHB1)" value="1" styleId="hiddIdPlanHB1"/>
								<html:hidden property="datos(hiddNombreHB1)" value="1" styleId="hiddNombreHB1"/>
								<html:hidden property="datos(hiddLinkVerHB1)" value="1" styleId="hiddLinkVerHB1"/>
								<html:hidden property="datos(hiddLinkConocerHB1)" value="1" styleId="hiddLinkConocerHB1"/>
								<html:hidden property="datos(hiddLinkCotizarHB1)" value="1" styleId="hiddLinkCotizarHB1"/>
								<html:hidden property="datos(hiddTrackerHB1)" value="1" styleId="hiddTrackerHB1"/>
								<html:hidden property="datos(hiddImagenHB1)" value="1" styleId="hiddImagenHB1"/>
								<html:hidden property="datos(hiddPrincipalHB1)" value="1" styleId="hiddPrincipalHB1"/>
								<html:hidden property="datos(hiddOrdinalHB1)" value="1" styleId="hiddOrdinalHB1"/>
								
								<html:hidden property="datos(hiddIdPlanHB1X)" value="<bean:write name='idPlanHB1'></bean:write>" styleId="hiddIdPlanHB1X"/>
								<html:hidden property="datos(hiddLinkVerHB1X)" value="<bean:write name='linkVerBasesHB1'></bean:write>" styleId="hiddLinkVerHB1X"/>
								<html:hidden property="datos(hiddLinkConocerHB1X)" value="<bean:write name='linkConocerHB1'></bean:write>" styleId="hiddLinkConocerHB1X"/>
								<html:hidden property="datos(hiddLinkCotizarHB1X)" value="<bean:write name='linkCotizarHB1'></bean:write>" styleId="hiddLinkCotizarHB1X"/>
								<html:hidden property="datos(hiddTrackerHB1X)" value="<bean:write name='trackerHB1'></bean:write>" styleId="hiddTrackerHB1X"/>
								<html:hidden property="datos(hiddImagenHB1X)" value="<bean:write name='imagenHB1'></bean:write>" styleId="hiddImagenHB1X"/>
								<html:hidden property="datos(hiddNombreHB1X)" value="<bean:write name='nombreHB1'></bean:write>" styleId="hiddNombreHB1X"/>
							</table>
							
							
							
							<!-- Banner 2 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 2 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB2" type="text" value="<bean:write name="idPlanHB2"></bean:write>" size="12" name="idPlanHB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB2" class="hasDatepicker" type="text" value="<bean:write name="nombreHB2"></bean:write>" size="12" name="nombreHB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB2" class="hasDatepicker" type="text" value="<bean:write name="trackerHB2"></bean:write>" size="12" name="trackerHB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB2" class="hasDatepicker" type="text" value="<bean:write name="imagenHB2"></bean:write>" size="12" name="imagenHB2">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB2" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB2"></bean:write>" size="12" name="linkVerBasesHB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerHB2" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB2"></bean:write>" size="12" name="linkConocerHB2">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB2" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB2"></bean:write>" size="12" name="linkCotizarHB2">
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeHB2)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB2" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaHB2)" name="informeComercialTarjetas" styleId="inputFechaHastaHB2" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB2').value);">
								</td>
								<html:hidden property="datos(hiddRamaHB)" value="1" styleId="hiddRamaHB"/>
								<html:hidden property="datos(hiddIdPlanHB2)" value="1" styleId="hiddIdPlanHB2"/>
								<html:hidden property="datos(hiddNombreHB2)" value="1" styleId="hiddNombreHB2"/>
								<html:hidden property="datos(hiddLinkVerHB2)" value="1" styleId="hiddLinkVerHB2"/>
								<html:hidden property="datos(hiddLinkConocerHB2)" value="1" styleId="hiddLinkConocerHB2"/>
								<html:hidden property="datos(hiddLinkCotizarHB2)" value="1" styleId="hiddLinkCotizarHB2"/>
								<html:hidden property="datos(hiddTrackerHB2)" value="1" styleId="hiddTrackerHB2"/>
								<html:hidden property="datos(hiddImagenHB2)" value="1" styleId="hiddImagenHB2"/>
								<html:hidden property="datos(hiddPrincipalHB2)" value="1" styleId="hiddPrincipalHB2"/>
								<html:hidden property="datos(hiddOrdinalHB2)" value="1" styleId="hiddOrdinalHB2"/>
								
								<html:hidden property="datos(hiddIdPlanHB2X)" value="<bean:write name='idPlanHB2'></bean:write>" styleId="hiddIdPlanHB2X"/>
								<html:hidden property="datos(hiddLinkVerHB2X)" value="<bean:write name='linkVerBasesHB2'></bean:write>" styleId="hiddLinkVerHB2X"/>
								<html:hidden property="datos(hiddLinkConocerHB2X)" value="<bean:write name='linkConocerHB2'></bean:write>" styleId="hiddLinkConocerHB2X"/>
								<html:hidden property="datos(hiddLinkCotizarHB2X)" value="<bean:write name='linkCotizarHB2'></bean:write>" styleId="hiddLinkCotizarHB2X"/>
								<html:hidden property="datos(hiddTrackerHB2X)" value="<bean:write name='trackerHB2'></bean:write>" styleId="hiddTrackerHB2X"/>
								<html:hidden property="datos(hiddImagenHB2X)" value="<bean:write name='imagenHB2'></bean:write>" styleId="hiddImagenHB2X"/>
								<html:hidden property="datos(hiddNombreHB2X)" value="<bean:write name='nombreHB2'></bean:write>" styleId="hiddNombreHB2X"/>
							</table>
							
							
							
							<!-- Banner 3 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 3 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB3" type="text" value="<bean:write name="idPlanHB3"></bean:write>" size="12" name="idPlanHB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB3" class="hasDatepicker" type="text" value="<bean:write name="nombreHB3"></bean:write>" size="12" name="nombreHB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB3" class="hasDatepicker" type="text" value="<bean:write name="trackerHB3"></bean:write>" size="12" name="trackerHB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB3" class="hasDatepicker" type="text" value="<bean:write name="imagenHB3"></bean:write>" size="12" name="imagenHB3">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB3" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB3"></bean:write>" size="12" name="linkVerBasesHB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerHB3" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB3"></bean:write>" size="12" name="linkConocerHB3">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB3" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB3"></bean:write>" size="12" name="linkCotizarHB3">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeHB3)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB3" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaHB3)" name="informeComercialTarjetas" styleId="inputFechaHastaHB3" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB3').value);">
								</td>
								<html:hidden property="datos(hiddRamaHB)" value="1" styleId="hiddRamaHB"/>
								<html:hidden property="datos(hiddIdPlanHB3)" value="1" styleId="hiddIdPlanHB3"/>
								<html:hidden property="datos(hiddNombreHB3)" value="1" styleId="hiddNombreHB3"/>
								<html:hidden property="datos(hiddLinkVerHB3)" value="1" styleId="hiddLinkVerHB3"/>
								<html:hidden property="datos(hiddLinkConocerHB3)" value="1" styleId="hiddLinkConocerHB3"/>
								<html:hidden property="datos(hiddLinkCotizarHB3)" value="1" styleId="hiddLinkCotizarHB3"/>
								<html:hidden property="datos(hiddTrackerHB3)" value="1" styleId="hiddTrackerHB3"/>
								<html:hidden property="datos(hiddImagenHB3)" value="1" styleId="hiddImagenHB3"/>
								<html:hidden property="datos(hiddPrincipalHB3)" value="1" styleId="hiddPrincipalHB3"/>
								<html:hidden property="datos(hiddOrdinalHB3)" value="1" styleId="hiddOrdinalHB3"/>
								
								<html:hidden property="datos(hiddIdPlanHB3X)" value="<bean:write name='idPlanHB3'></bean:write>" styleId="hiddIdPlanHB3X"/>
								<html:hidden property="datos(hiddLinkVerHB3X)" value="<bean:write name='linkVerBasesHB3'></bean:write>" styleId="hiddLinkVerHB3X"/>
								<html:hidden property="datos(hiddLinkConocerHB3X)" value="<bean:write name='linkConocerHB3'></bean:write>" styleId="hiddLinkConocerHB3X"/>
								<html:hidden property="datos(hiddLinkCotizarHB3X)" value="<bean:write name='linkCotizarHB3'></bean:write>" styleId="hiddLinkCotizarHB3X"/>
								<html:hidden property="datos(hiddTrackerHB3X)" value="<bean:write name='trackerHB3'></bean:write>" styleId="hiddTrackerHB3X"/>
								<html:hidden property="datos(hiddImagenHB3X)" value="<bean:write name='imagenHB3'></bean:write>" styleId="hiddImagenHB3X"/>
								<html:hidden property="datos(hiddNombreHB3X)" value="<bean:write name='nombreHB3'></bean:write>" styleId="hiddNombreHB3X"/>
							</table>
							
							
							<!-- Banner 4 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 4 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB4" type="text" value="<bean:write name="idPlanHB4"></bean:write>" size="12" name="idPlanHB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB4" class="hasDatepicker" type="text" value="<bean:write name="nombreHB4"></bean:write>" size="12" name="nombreHB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB4" class="hasDatepicker" type="text" value="<bean:write name="trackerHB4"></bean:write>" size="12" name="trackerHB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB4" class="hasDatepicker" type="text" value="<bean:write name="imagenHB4"></bean:write>" size="12" name="imagenHB4">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB4" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB4"></bean:write>" size="12" name="linkVerBasesHB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerHB4" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB4"></bean:write>" size="12" name="linkConocerHB4">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB4" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB4"></bean:write>" size="12" name="linkCotizarHB4">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeHB4)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB4" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaHB4)" name="informeComercialTarjetas" styleId="inputFechaHastaHB4" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB4').value);">
								</td>
								<html:hidden property="datos(hiddRamaHB)" value="1" styleId="hiddRamaHB"/>
								<html:hidden property="datos(hiddIdPlanHB4)" value="1" styleId="hiddIdPlanHB4"/>
								<html:hidden property="datos(hiddNombreHB4)" value="1" styleId="hiddNombreHB4"/>
								<html:hidden property="datos(hiddLinkVerHB4)" value="1" styleId="hiddLinkVerHB4"/>
								<html:hidden property="datos(hiddLinkConocerHB4)" value="1" styleId="hiddLinkConocerHB4"/>
								<html:hidden property="datos(hiddLinkCotizarHB4)" value="1" styleId="hiddLinkCotizarHB4"/>
								<html:hidden property="datos(hiddTrackerHB4)" value="1" styleId="hiddTrackerHB4"/>
								<html:hidden property="datos(hiddImagenHB4)" value="1" styleId="hiddImagenHB4"/>
								<html:hidden property="datos(hiddPrincipalHB4)" value="1" styleId="hiddPrincipalHB4"/>
								<html:hidden property="datos(hiddOrdinalHB4)" value="1" styleId="hiddOrdinalHB4"/>
								
								<html:hidden property="datos(hiddIdPlanHB4X)" value="<bean:write name='idPlanHB4'></bean:write>" styleId="hiddIdPlanHB4X"/>
								<html:hidden property="datos(hiddLinkVerHB4X)" value="<bean:write name='linkVerBasesHB4'></bean:write>" styleId="hiddLinkVerHB4X"/>
								<html:hidden property="datos(hiddLinkConocerHB4X)" value="<bean:write name='linkConocerHB4'></bean:write>" styleId="hiddLinkConocerHB4X"/>
								<html:hidden property="datos(hiddLinkCotizarHB4X)" value="<bean:write name='linkCotizarHB4'></bean:write>" styleId="hiddLinkCotizarHB4X"/>
								<html:hidden property="datos(hiddTrackerHB4X)" value="<bean:write name='trackerHB4'></bean:write>" styleId="hiddTrackerHB4X"/>
								<html:hidden property="datos(hiddImagenHB4X)" value="<bean:write name='imagenHB4'></bean:write>" styleId="hiddImagenHB4X"/>
								<html:hidden property="datos(hiddNombreHB4X)" value="<bean:write name='nombreHB4'></bean:write>" styleId="hiddNombreHB4X"/>
							</table>
							
							
							<!-- Banner 5 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 5 </th>
									<td width="55">
									
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB5" type="text" value="<bean:write name="idPlanHB5"></bean:write>" size="12" name="idPlanHB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB5" class="hasDatepicker" type="text" value="<bean:write name="nombreHB5"></bean:write>" size="12" name="nombreHB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB5" class="hasDatepicker" type="text" value="<bean:write name="trackerHB5"></bean:write>" size="12" name="trackerHB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB5" class="hasDatepicker" type="text" value="<bean:write name="imagenHB5"></bean:write>" size="12" name="imagenHB5">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB5" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB5"></bean:write>" size="12" name="linkVerBasesHB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerHB5" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB5"></bean:write>" size="12" name="linkConocerHB5">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB5" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB5"></bean:write>" size="12" name="linkCotizarHB5">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeHB5)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB5" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaHB5)" name="informeComercialTarjetas" styleId="inputFechaHastaHB5" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB5').value);">
								</td>
								<html:hidden property="datos(hiddRamaHB)" value="1" styleId="hiddRamaHB"/>
								<html:hidden property="datos(hiddIdPlanHB5)" value="1" styleId="hiddIdPlanHB5"/>
								<html:hidden property="datos(hiddNombreHB5)" value="1" styleId="hiddNombreHB5"/>
								<html:hidden property="datos(hiddLinkVerHB5)" value="1" styleId="hiddLinkVerHB5"/>
								<html:hidden property="datos(hiddLinkConocerHB5)" value="1" styleId="hiddLinkConocerHB5"/>
								<html:hidden property="datos(hiddLinkCotizarHB5)" value="1" styleId="hiddLinkCotizarHB5"/>
								<html:hidden property="datos(hiddTrackerHB5)" value="1" styleId="hiddTrackerHB5"/>
								<html:hidden property="datos(hiddImagenHB5)" value="1" styleId="hiddImagenHB5"/>
								<html:hidden property="datos(hiddPrincipalHB5)" value="1" styleId="hiddPrincipalHB5"/>
								<html:hidden property="datos(hiddOrdinalHB5)" value="1" styleId="hiddOrdinalHB5"/>
								
								<html:hidden property="datos(hiddIdPlanHB5X)" value="<bean:write name='idPlanHB5'></bean:write>" styleId="hiddIdPlanHB5X"/>
								<html:hidden property="datos(hiddLinkVerHB5X)" value="<bean:write name='linkVerBasesHB5'></bean:write>" styleId="hiddLinkVerHB5X"/>
								<html:hidden property="datos(hiddLinkConocerHB5X)" value="<bean:write name='linkConocerHB5'></bean:write>" styleId="hiddLinkConocerHB5X"/>
								<html:hidden property="datos(hiddLinkCotizarHB5X)" value="<bean:write name='linkCotizarHB5'></bean:write>" styleId="hiddLinkCotizarHB5X"/>
								<html:hidden property="datos(hiddTrackerHB5X)" value="<bean:write name='trackerHB5'></bean:write>" styleId="hiddTrackerHB5X"/>
								<html:hidden property="datos(hiddImagenHB5X)" value="<bean:write name='imagenHB5'></bean:write>" styleId="hiddImagenHB5X"/>
								<html:hidden property="datos(hiddNombreHB5X)" value="<bean:write name='nombreHB5'></bean:write>" styleId="hiddNombreHB5X"/>
							</table>
							
							
							<!-- Banner 6 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 6 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB6" type="text" value="<bean:write name="idPlanHB6"></bean:write>" size="12" name="idPlanHB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB6" class="hasDatepicker" type="text" value="<bean:write name="nombreHB6"></bean:write>" size="12" name="nombreHB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 256); padding: 16px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB6" class="hasDatepicker" type="text" value="<bean:write name="trackerHB6"></bean:write>" size="12" name="trackerHB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB6" class="hasDatepicker" type="text" value="<bean:write name="imagenHB6"></bean:write>" size="12" name="imagenHB6">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB6" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB6"></bean:write>" size="12" name="linkVerBasesHB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerHB6" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB6"></bean:write>" size="12" name="linkConocerHB6">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB6" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB6"></bean:write>" size="12" name="linkCotizarHB6">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeHB6)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB6" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaHB6)" name="informeComercialTarjetas" styleId="inputFechaHastaHB6" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB6').value);">
								</td>
								<html:hidden property="datos(hiddRamaHB)" value="1" styleId="hiddRamaHB"/>
								<html:hidden property="datos(hiddIdPlanHB6)" value="1" styleId="hiddIdPlanHB6"/>
								<html:hidden property="datos(hiddNombreHB6)" value="1" styleId="hiddNombreHB6"/>
								<html:hidden property="datos(hiddLinkVerHB6)" value="1" styleId="hiddLinkVerHB6"/>
								<html:hidden property="datos(hiddLinkConocerHB6)" value="1" styleId="hiddLinkConocerHB6"/>
								<html:hidden property="datos(hiddLinkCotizarHB6)" value="1" styleId="hiddLinkCotizarHB6"/>
								<html:hidden property="datos(hiddTrackerHB6)" value="1" styleId="hiddTrackerHB6"/>
								<html:hidden property="datos(hiddImagenHB6)" value="1" styleId="hiddImagenHB6"/>
								<html:hidden property="datos(hiddPrincipalHB6)" value="1" styleId="hiddPrincipalHB6"/>
								<html:hidden property="datos(hiddOrdinalHB6)" value="1" styleId="hiddOrdinalHB6"/>
								
								<html:hidden property="datos(hiddIdPlanHB6X)" value="<bean:write name='idPlanHB6'></bean:write>" styleId="hiddIdPlanHB6X"/>
								<html:hidden property="datos(hiddLinkVerHB6X)" value="<bean:write name='linkVerBasesHB6'></bean:write>" styleId="hiddLinkVerHB6X"/>
								<html:hidden property="datos(hiddLinkConocerHB6X)" value="<bean:write name='linkConocerHB6'></bean:write>" styleId="hiddLinkConocerHB6X"/>
								<html:hidden property="datos(hiddLinkCotizarHB6X)" value="<bean:write name='linkCotizarHB6'></bean:write>" styleId="hiddLinkCotizarHB6X"/>
								<html:hidden property="datos(hiddTrackerHB6X)" value="<bean:write name='trackerHB6'></bean:write>" styleId="hiddTrackerHB6X"/>
								<html:hidden property="datos(hiddImagenHB6X)" value="<bean:write name='imagenHB6'></bean:write>" styleId="hiddImagenHB6X"/>
								<html:hidden property="datos(hiddNombreHB6X)" value="<bean:write name='nombreHB6'></bean:write>" styleId="hiddNombreHB6X"/>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="buscar" type="button" name="buscar" value="Previsualizar" onclick="previsualizarPromocion();">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
							</table>
							</div>
							<!-- VIDA -->
							<div id="secundario-vida">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Vida - Banner Secundarios  </th>
										
									</tr>
							</table>
							
							
							<!-- Banner 1 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 1 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB1" type="text" value="<bean:write name="idPlanVIB1"></bean:write>" size="12" name="idPlanVIB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB1" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB1"></bean:write>" size="12" name="nombreVIB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB1" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB1"></bean:write>" size="12" name="trackerVIB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB1" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB1"></bean:write>" size="12" name="imagenVIB1">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB1" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB1"></bean:write>" size="12" name="linkVerBasesVIB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVIB1" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB1"></bean:write>" size="12" name="linkConocerVIB1">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB1" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB1"></bean:write>" size="12" name="linkCotizarVIB1">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB1)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB1" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVIB1)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB1" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB1').value);">
								</td>
								<html:hidden property="datos(hiddRamaVIB)" value="1" styleId="hiddRamaVIB"/>
								<html:hidden property="datos(hiddIdPlanVIB1)" value="1" styleId="hiddIdPlanVIB1"/>
								<html:hidden property="datos(hiddNombreVIB1)" value="1" styleId="hiddNombreVIB1"/>
								<html:hidden property="datos(hiddLinkVerVIB1)" value="1" styleId="hiddLinkVerVIB1"/>
								<html:hidden property="datos(hiddLinkConocerVIB1)" value="1" styleId="hiddLinkConocerVIB1"/>
								<html:hidden property="datos(hiddLinkCotizarVIB1)" value="1" styleId="hiddLinkCotizarVIB1"/>
								<html:hidden property="datos(hiddTrackerVIB1)" value="1" styleId="hiddTrackerVIB1"/>
								<html:hidden property="datos(hiddImagenVIB1)" value="1" styleId="hiddImagenVIB1"/>
								<html:hidden property="datos(hiddPrincipalVIB1)" value="1" styleId="hiddPrincipalVIB1"/>
								<html:hidden property="datos(hiddOrdinalVIB1)" value="1" styleId="hiddOrdinalVIB1"/>
								
								<html:hidden property="datos(hiddIdPlanVIB1X)" value="<bean:write name='idPlanVIB1'></bean:write>" styleId="hiddIdPlanVIB1X"/>
								<html:hidden property="datos(hiddLinkVerVIB1X)" value="<bean:write name='linkVerBasesVIB1'></bean:write>" styleId="hiddLinkVerVIB1X"/>
								<html:hidden property="datos(hiddLinkConocerVIB1X)" value="<bean:write name='linkConocerVIB1'></bean:write>" styleId="hiddLinkConocerVIB1X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB1X)" value="<bean:write name='linkCotizarVIB1'></bean:write>" styleId="hiddLinkCotizarVIB1X"/>
								<html:hidden property="datos(hiddTrackerVIB1X)" value="<bean:write name='trackerVIB1'></bean:write>" styleId="hiddTrackerVIB1X"/>
								<html:hidden property="datos(hiddImagenVIB1X)" value="<bean:write name='imagenVIB1'></bean:write>" styleId="hiddImagenVIB1X"/>
								<html:hidden property="datos(hiddNombreVIB1X)" value="<bean:write name='nombreVIB1'></bean:write>" styleId="hiddNombreVIB1X"/>
							</table>
							
							
							
							<!-- Banner 2 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 2 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB2" type="text" value="<bean:write name="idPlanVIB2"></bean:write>" size="12" name="idPlanVIB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB2" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB2"></bean:write>" size="12" name="nombreVIB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB2" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB2"></bean:write>" size="12" name="trackerVIB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB2" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB2"></bean:write>" size="12" name="imagenVIB2">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB2" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB2"></bean:write>" size="12" name="linkVerBasesVIB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVIB2" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB2"></bean:write>" size="12" name="linkConocerVIB2">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB2" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB2"></bean:write>" size="12" name="linkCotizarVIB2">
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB2)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB2" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVIB2)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB2" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB2').value);">
								</td>
								<html:hidden property="datos(hiddRamaVIB)" value="1" styleId="hiddRamaVIB"/>
								<html:hidden property="datos(hiddIdPlanVIB2)" value="1" styleId="hiddIdPlanVIB2"/>
								<html:hidden property="datos(hiddNombreVIB2)" value="1" styleId="hiddNombreVIB2"/>
								<html:hidden property="datos(hiddLinkVerVIB2)" value="1" styleId="hiddLinkVerVIB2"/>
								<html:hidden property="datos(hiddLinkConocerVIB2)" value="1" styleId="hiddLinkConocerVIB2"/>
								<html:hidden property="datos(hiddLinkCotizarVIB2)" value="1" styleId="hiddLinkCotizarVIB2"/>
								<html:hidden property="datos(hiddTrackerVIB2)" value="1" styleId="hiddTrackerVIB2"/>
								<html:hidden property="datos(hiddImagenVIB2)" value="1" styleId="hiddImagenVIB2"/>
								<html:hidden property="datos(hiddPrincipalVIB2)" value="1" styleId="hiddPrincipalVIB2"/>
								<html:hidden property="datos(hiddOrdinalVIB2)" value="1" styleId="hiddOrdinalVIB2"/>
								
								<html:hidden property="datos(hiddIdPlanVIB2X)" value="<bean:write name='idPlanVIB2'></bean:write>" styleId="hiddIdPlanVIB2X"/>
								<html:hidden property="datos(hiddLinkVerVIB2X)" value="<bean:write name='linkVerBasesVIB2'></bean:write>" styleId="hiddLinkVerVIB2X"/>
								<html:hidden property="datos(hiddLinkConocerVIB2X)" value="<bean:write name='linkConocerVIB2'></bean:write>" styleId="hiddLinkConocerVIB2X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB2X)" value="<bean:write name='linkCotizarVIB2'></bean:write>" styleId="hiddLinkCotizarVIB2X"/>
								<html:hidden property="datos(hiddTrackerVIB2X)" value="<bean:write name='trackerVIB2'></bean:write>" styleId="hiddTrackerVIB2X"/>
								<html:hidden property="datos(hiddImagenVIB2X)" value="<bean:write name='imagenVIB2'></bean:write>" styleId="hiddImagenVIB2X"/>
								<html:hidden property="datos(hiddNombreVIB2X)" value="<bean:write name='nombreVIB2'></bean:write>" styleId="hiddNombreVIB2X"/>
							</table>
							
							
							
							<!-- Banner 3 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 3 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB3" type="text" value="<bean:write name="idPlanVIB3"></bean:write>" size="12" name="idPlanVIB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB3" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB3"></bean:write>" size="12" name="nombreVIB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB3" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB3"></bean:write>" size="12" name="trackerVIB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB3" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB3"></bean:write>" size="12" name="imagenVIB3">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB3" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB3"></bean:write>" size="12" name="linkVerBasesVIB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVIB3" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB3"></bean:write>" size="12" name="linkConocerVIB3">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB3" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB3"></bean:write>" size="12" name="linkCotizarVIB3">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB3)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB3" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVIB3)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB3" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB3').value);">
								</td>
								<html:hidden property="datos(hiddRamaVIB)" value="1" styleId="hiddRamaVIB"/>
								<html:hidden property="datos(hiddIdPlanVIB3)" value="1" styleId="hiddIdPlanVIB3"/>
								<html:hidden property="datos(hiddNombreVIB3)" value="1" styleId="hiddNombreVIB3"/>
								<html:hidden property="datos(hiddLinkVerVIB3)" value="1" styleId="hiddLinkVerVIB3"/>
								<html:hidden property="datos(hiddLinkConocerVIB3)" value="1" styleId="hiddLinkConocerVIB3"/>
								<html:hidden property="datos(hiddLinkCotizarVIB3)" value="1" styleId="hiddLinkCotizarVIB3"/>
								<html:hidden property="datos(hiddTrackerVIB3)" value="1" styleId="hiddTrackerVIB3"/>
								<html:hidden property="datos(hiddImagenVIB3)" value="1" styleId="hiddImagenVIB3"/>
								<html:hidden property="datos(hiddPrincipalVIB3)" value="1" styleId="hiddPrincipalVIB3"/>
								<html:hidden property="datos(hiddOrdinalVIB3)" value="1" styleId="hiddOrdinalVIB3"/>
								
								<html:hidden property="datos(hiddIdPlanVIB3X)" value="<bean:write name='idPlanVIB3'></bean:write>" styleId="hiddIdPlanVIB3X"/>
								<html:hidden property="datos(hiddLinkVerVIB3X)" value="<bean:write name='linkVerBasesVIB3'></bean:write>" styleId="hiddLinkVerVIB3X"/>
								<html:hidden property="datos(hiddLinkConocerVIB3X)" value="<bean:write name='linkConocerVIB3'></bean:write>" styleId="hiddLinkConocerVIB3X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB3X)" value="<bean:write name='linkCotizarVIB3'></bean:write>" styleId="hiddLinkCotizarVIB3X"/>
								<html:hidden property="datos(hiddTrackerVIB3X)" value="<bean:write name='trackerVIB3'></bean:write>" styleId="hiddTrackerVIB3X"/>
								<html:hidden property="datos(hiddImagenVIB3X)" value="<bean:write name='imagenVIB3'></bean:write>" styleId="hiddImagenVIB3X"/>
								<html:hidden property="datos(hiddNombreVIB3X)" value="<bean:write name='nombreVIB3'></bean:write>" styleId="hiddNombreVIB3X"/>
							</table>
							
							
							<!-- Banner 4 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 4 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB4" type="text" value="<bean:write name="idPlanVIB4"></bean:write>" size="12" name="idPlanVIB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB4" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB4"></bean:write>" size="12" name="nombreVIB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB4" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB4"></bean:write>" size="12" name="trackerVIB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB4" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB4"></bean:write>" size="12" name="imagenVIB4">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB4" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB4"></bean:write>" size="12" name="linkVerBasesVIB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVIB4" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB4"></bean:write>" size="12" name="linkConocerVIB4">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB4" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB4"></bean:write>" size="12" name="linkCotizarVIB4">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB4)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB4" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVIB4)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB4" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB4').value);">
								</td>
								<html:hidden property="datos(hiddRamaVIB)" value="1" styleId="hiddRamaVIB"/>
								<html:hidden property="datos(hiddIdPlanVIB4)" value="1" styleId="hiddIdPlanVIB4"/>
								<html:hidden property="datos(hiddNombreVIB4)" value="1" styleId="hiddNombreVIB4"/>
								<html:hidden property="datos(hiddLinkVerVIB4)" value="1" styleId="hiddLinkVerVIB4"/>
								<html:hidden property="datos(hiddLinkConocerVIB4)" value="1" styleId="hiddLinkConocerVIB4"/>
								<html:hidden property="datos(hiddLinkCotizarVIB4)" value="1" styleId="hiddLinkCotizarVIB4"/>
								<html:hidden property="datos(hiddTrackerVIB4)" value="1" styleId="hiddTrackerVIB4"/>
								<html:hidden property="datos(hiddImagenVIB4)" value="1" styleId="hiddImagenVIB4"/>
								<html:hidden property="datos(hiddPrincipalVIB4)" value="1" styleId="hiddPrincipalVIB4"/>
								<html:hidden property="datos(hiddOrdinalVIB4)" value="1" styleId="hiddOrdinalVIB4"/>
								
								<html:hidden property="datos(hiddIdPlanVIB4X)" value="<bean:write name='idPlanVIB4'></bean:write>" styleId="hiddIdPlanVIB4X"/>
								<html:hidden property="datos(hiddLinkVerVIB4X)" value="<bean:write name='linkVerBasesVIB4'></bean:write>" styleId="hiddLinkVerVIB4X"/>
								<html:hidden property="datos(hiddLinkConocerVIB4X)" value="<bean:write name='linkConocerVIB4'></bean:write>" styleId="hiddLinkConocerVIB4X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB4X)" value="<bean:write name='linkCotizarVIB4'></bean:write>" styleId="hiddLinkCotizarVIB4X"/>
								<html:hidden property="datos(hiddTrackerVIB4X)" value="<bean:write name='trackerVIB4'></bean:write>" styleId="hiddTrackerVIB4X"/>
								<html:hidden property="datos(hiddImagenVIB4X)" value="<bean:write name='imagenVIB4'></bean:write>" styleId="hiddImagenVIB4X"/>
								<html:hidden property="datos(hiddNombreVIB4X)" value="<bean:write name='nombreVIB4'></bean:write>" styleId="hiddNombreVIB4X"/>
							</table>
							
							
							<!-- Banner 5 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 5 </th>
									<td width="55">
									
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB5" type="text" value="<bean:write name="idPlanVIB5"></bean:write>" size="12" name="idPlanVIB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB5" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB5"></bean:write>" size="12" name="nombreVIB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB5" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB5"></bean:write>" size="12" name="trackerVIB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB5" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB5"></bean:write>" size="12" name="imagenVIB5">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB5" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB5"></bean:write>" size="12" name="linkVerBasesVIB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVIB5" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB5"></bean:write>" size="12" name="linkConocerVIB5">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB5" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB5"></bean:write>" size="12" name="linkCotizarVIB5">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB5)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB5" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVIB5)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB5" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB5').value);">
								</td>
								<html:hidden property="datos(hiddRamaVIB)" value="1" styleId="hiddRamaVIB"/>
								<html:hidden property="datos(hiddIdPlanVIB5)" value="1" styleId="hiddIdPlanVIB5"/>
								<html:hidden property="datos(hiddNombreVIB5)" value="1" styleId="hiddNombreVIB5"/>
								<html:hidden property="datos(hiddLinkVerVIB5)" value="1" styleId="hiddLinkVerVIB5"/>
								<html:hidden property="datos(hiddLinkConocerVIB5)" value="1" styleId="hiddLinkConocerVIB5"/>
								<html:hidden property="datos(hiddLinkCotizarVIB5)" value="1" styleId="hiddLinkCotizarVIB5"/>
								<html:hidden property="datos(hiddTrackerVIB5)" value="1" styleId="hiddTrackerVIB5"/>
								<html:hidden property="datos(hiddImagenVIB5)" value="1" styleId="hiddImagenVIB5"/>
								<html:hidden property="datos(hiddPrincipalVIB5)" value="1" styleId="hiddPrincipalVIB5"/>
								<html:hidden property="datos(hiddOrdinalVIB5)" value="1" styleId="hiddOrdinalVIB5"/>
								
								<html:hidden property="datos(hiddIdPlanVIB5X)" value="<bean:write name='idPlanVIB5'></bean:write>" styleId="hiddIdPlanVIB5X"/>
								<html:hidden property="datos(hiddLinkVerVIB5X)" value="<bean:write name='linkVerBasesVIB5'></bean:write>" styleId="hiddLinkVerVIB5X"/>
								<html:hidden property="datos(hiddLinkConocerVIB5X)" value="<bean:write name='linkConocerVIB5'></bean:write>" styleId="hiddLinkConocerVIB5X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB5X)" value="<bean:write name='linkCotizarVIB5'></bean:write>" styleId="hiddLinkCotizarVIB5X"/>
								<html:hidden property="datos(hiddTrackerVIB5X)" value="<bean:write name='trackerVIB5'></bean:write>" styleId="hiddTrackerVIB5X"/>
								<html:hidden property="datos(hiddImagenVIB5X)" value="<bean:write name='imagenVIB5'></bean:write>" styleId="hiddImagenVIB5X"/>
								<html:hidden property="datos(hiddNombreVIB5X)" value="<bean:write name='nombreVIB5'></bean:write>" styleId="hiddNombreVIB5X"/>
							</table>
							
							
							<!-- Banner 6 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 6 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB6" type="text" value="<bean:write name="idPlanVIB6"></bean:write>" size="12" name="idPlanVIB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB6" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB6"></bean:write>" size="12" name="nombreVIB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 256); padding: 16px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB6" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB6"></bean:write>" size="12" name="trackerVIB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB6" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB6"></bean:write>" size="12" name="imagenVIB6">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB6" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB6"></bean:write>" size="12" name="linkVerBasesVIB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerVIB6" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB6"></bean:write>" size="12" name="linkConocerVIB6">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB6" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB6"></bean:write>" size="12" name="linkCotizarVIB6">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB6)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB6" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaVIB6)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB6" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB6').value);">
								</td>
								<html:hidden property="datos(hiddRamaVIB)" value="1" styleId="hiddRamaVIB"/>
								<html:hidden property="datos(hiddIdPlanVIB6)" value="1" styleId="hiddIdPlanVIB6"/>
								<html:hidden property="datos(hiddNombreVIB6)" value="1" styleId="hiddNombreVIB6"/>
								<html:hidden property="datos(hiddLinkVerVIB6)" value="1" styleId="hiddLinkVerVIB6"/>
								<html:hidden property="datos(hiddLinkConocerVIB6)" value="1" styleId="hiddLinkConocerVIB6"/>
								<html:hidden property="datos(hiddLinkCotizarVIB6)" value="1" styleId="hiddLinkCotizarVIB6"/>
								<html:hidden property="datos(hiddTrackerVIB6)" value="1" styleId="hiddTrackerVIB6"/>
								<html:hidden property="datos(hiddImagenVIB6)" value="1" styleId="hiddImagenVIB6"/>
								<html:hidden property="datos(hiddPrincipalVIB6)" value="1" styleId="hiddPrincipalVIB6"/>
								<html:hidden property="datos(hiddOrdinalVIB6)" value="1" styleId="hiddOrdinalVIB6"/>
								
								<html:hidden property="datos(hiddIdPlanVIB6X)" value="<bean:write name='idPlanVIB6'></bean:write>" styleId="hiddIdPlanVIB6X"/>
								<html:hidden property="datos(hiddLinkVerVIB6X)" value="<bean:write name='linkVerBasesVIB6'></bean:write>" styleId="hiddLinkVerVIB6X"/>
								<html:hidden property="datos(hiddLinkConocerVIB6X)" value="<bean:write name='linkConocerVIB6'></bean:write>" styleId="hiddLinkConocerVIB6X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB6X)" value="<bean:write name='linkCotizarVIB6'></bean:write>" styleId="hiddLinkCotizarVIB6X"/>
								<html:hidden property="datos(hiddTrackerVIB6X)" value="<bean:write name='trackerVIB6'></bean:write>" styleId="hiddTrackerVIB6X"/>
								<html:hidden property="datos(hiddImagenVIB6X)" value="<bean:write name='imagenVIB6'></bean:write>" styleId="hiddImagenVIB6X"/>
								<html:hidden property="datos(hiddNombreVIB6X)" value="<bean:write name='nombreVIB6'></bean:write>" styleId="hiddNombreVIB6X"/>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="buscar" type="button" name="buscar" value="Previsualizar" onclick="previsualizarPromocion();">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
							</table>
							</div>
							<!-- SALUD -->
							<div id="secundario-salud">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> P�gina Promociones Salud - Banner Secundarios  </th>
										
									</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 1 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanSB1" type="text" value="<bean:write name="idPlanSB1"></bean:write>" size="12" name="idPlanSB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreSB1" class="hasDatepicker" type="text" value="<bean:write name="nombreSB1"></bean:write>" size="12" name="nombreSB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerSB1" class="hasDatepicker" type="text" value="<bean:write name="trackerSB1"></bean:write>" size="12" name="trackerSB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenSB1" class="hasDatepicker" type="text" value="<bean:write name="imagenSB1"></bean:write>" size="12" name="imagenSB1">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesSB1" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesSB1"></bean:write>" size="12" name="linkVerBasesSB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerSB1" class="hasDatepicker" type="text" value="<bean:write name="linkConocerSB1"></bean:write>" size="12" name="linkConocerSB1">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarSB1" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarSB1"></bean:write>" size="12" name="linkCotizarSB1">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeSB1)" name="informeComercialTarjetas" styleId="inputFechaDesdeSB1" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaSB1)" name="informeComercialTarjetas" styleId="inputFechaHastaSB1" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenSB1').value);">
								</td>
								<html:hidden property="datos(hiddRamaSB)" value="1" styleId="hiddRamaSB"/>
								<html:hidden property="datos(hiddIdPlanSB1)" value="1" styleId="hiddIdPlanSB1"/>
								<html:hidden property="datos(hiddNombreSB1)" value="1" styleId="hiddNombreSB1"/>
								<html:hidden property="datos(hiddLinkVerSB1)" value="1" styleId="hiddLinkVerSB1"/>
								<html:hidden property="datos(hiddLinkConocerSB1)" value="1" styleId="hiddLinkConocerSB1"/>
								<html:hidden property="datos(hiddLinkCotizarSB1)" value="1" styleId="hiddLinkCotizarSB1"/>
								<html:hidden property="datos(hiddTrackerSB1)" value="1" styleId="hiddTrackerSB1"/>
								<html:hidden property="datos(hiddImagenSB1)" value="1" styleId="hiddImagenSB1"/>
								<html:hidden property="datos(hiddPrincipalSB1)" value="1" styleId="hiddPrincipalSB1"/>
								<html:hidden property="datos(hiddOrdinalSB1)" value="1" styleId="hiddOrdinalSB1"/>
								
								<html:hidden property="datos(hiddIdPlanSB1X)" value="<bean:write name='idPlanSB1'></bean:write>" styleId="hiddIdPlanSB1X"/>
								<html:hidden property="datos(hiddLinkVerSB1X)" value="<bean:write name='linkVerBasesSB1'></bean:write>" styleId="hiddLinkVerSB1X"/>
								<html:hidden property="datos(hiddLinkConocerSB1X)" value="<bean:write name='linkConocerSB1'></bean:write>" styleId="hiddLinkConocerSB1X"/>
								<html:hidden property="datos(hiddLinkCotizarSB1X)" value="<bean:write name='linkCotizarSB1'></bean:write>" styleId="hiddLinkCotizarSB1X"/>
								<html:hidden property="datos(hiddTrackerSB1X)" value="<bean:write name='trackerSB1'></bean:write>" styleId="hiddTrackerSB1X"/>
								<html:hidden property="datos(hiddImagenSB1X)" value="<bean:write name='imagenSB1'></bean:write>" styleId="hiddImagenSB1X"/>
								<html:hidden property="datos(hiddNombreSB1X)" value="<bean:write name='nombreSB1'></bean:write>" styleId="hiddNombreSB1X"/>
							</table>
							
							
							
							<!-- Banner 2 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 2 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanSB2" type="text" value="<bean:write name="idPlanSB2"></bean:write>" size="12" name="idPlanSB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreSB2" class="hasDatepicker" type="text" value="<bean:write name="nombreSB2"></bean:write>" size="12" name="nombreSB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerSB2" class="hasDatepicker" type="text" value="<bean:write name="trackerSB2"></bean:write>" size="12" name="trackerSB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenSB2" class="hasDatepicker" type="text" value="<bean:write name="imagenSB2"></bean:write>" size="12" name="imagenSB2">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesSB2" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesSB2"></bean:write>" size="12" name="linkVerBasesSB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerSB2" class="hasDatepicker" type="text" value="<bean:write name="linkConocerSB2"></bean:write>" size="12" name="linkConocerSB2">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarSB2" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarSB2"></bean:write>" size="12" name="linkCotizarSB2">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeSB2)" name="informeComercialTarjetas" styleId="inputFechaDesdeSB2" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaSB2)" name="informeComercialTarjetas" styleId="inputFechaHastaSB2" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenSB2').value);">
								</td>
								<html:hidden property="datos(hiddRamaSB)" value="1" styleId="hiddRamaSB"/>
								<html:hidden property="datos(hiddIdPlanSB2)" value="1" styleId="hiddIdPlanSB2"/>
								<html:hidden property="datos(hiddNombreSB2)" value="1" styleId="hiddNombreSB2"/>
								<html:hidden property="datos(hiddLinkVerSB2)" value="1" styleId="hiddLinkVerSB2"/>
								<html:hidden property="datos(hiddLinkConocerSB2)" value="1" styleId="hiddLinkConocerSB2"/>
								<html:hidden property="datos(hiddLinkCotizarSB2)" value="1" styleId="hiddLinkCotizarSB2"/>
								<html:hidden property="datos(hiddTrackerSB2)" value="1" styleId="hiddTrackerSB2"/>
								<html:hidden property="datos(hiddImagenSB2)" value="1" styleId="hiddImagenSB2"/>
								<html:hidden property="datos(hiddPrincipalSB2)" value="1" styleId="hiddPrincipalSB2"/>
								<html:hidden property="datos(hiddOrdinalSB2)" value="1" styleId="hiddOrdinalSB2"/>
								
								<html:hidden property="datos(hiddIdPlanSB2X)" value="<bean:write name='idPlanSB2'></bean:write>" styleId="hiddIdPlanSB2X"/>
								<html:hidden property="datos(hiddLinkVerSB2X)" value="<bean:write name='linkVerBasesSB2'></bean:write>" styleId="hiddLinkVerSB2X"/>
								<html:hidden property="datos(hiddLinkConocerSB2X)" value="<bean:write name='linkConocerSB2'></bean:write>" styleId="hiddLinkConocerSB2X"/>
								<html:hidden property="datos(hiddLinkCotizarSB2X)" value="<bean:write name='linkCotizarSB2'></bean:write>" styleId="hiddLinkCotizarSB2X"/>
								<html:hidden property="datos(hiddTrackerSB2X)" value="<bean:write name='trackerSB2'></bean:write>" styleId="hiddTrackerSB2X"/>
								<html:hidden property="datos(hiddImagenSB2X)" value="<bean:write name='imagenSB2'></bean:write>" styleId="hiddImagenSB2X"/>
								<html:hidden property="datos(hiddNombreSB2X)" value="<bean:write name='nombreSB2'></bean:write>" styleId="hiddNombreSB2X"/>
							</table>
							
							
							
							<!-- Banner 3 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 3 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanSB3" type="text" value="<bean:write name="idPlanSB3"></bean:write>" size="12" name="idPlanSB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreSB3" class="hasDatepicker" type="text" value="<bean:write name="nombreSB3"></bean:write>" size="12" name="nombreSB3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerSB3" class="hasDatepicker" type="text" value="<bean:write name="trackerSB3"></bean:write>" size="12" name="trackerSB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenSB3" class="hasDatepicker" type="text" value="<bean:write name="imagenSB3"></bean:write>" size="12" name="imagenSB3">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesSB3" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesSB3"></bean:write>" size="12" name="linkVerBasesSB3">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerSB3" class="hasDatepicker" type="text" value="<bean:write name="linkConocerSB3"></bean:write>" size="12" name="linkConocerSB3">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarSB3" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarSB3"></bean:write>" size="12" name="linkCotizarSB3">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeSB3)" name="informeComercialTarjetas" styleId="inputFechaDesdeSB3" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaSB3)" name="informeComercialTarjetas" styleId="inputFechaHastaSB3" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenSB3').value);">
								</td>
								<html:hidden property="datos(hiddRamaSB)" value="1" styleId="hiddRamaSB"/>
								<html:hidden property="datos(hiddIdPlanSB3)" value="1" styleId="hiddIdPlanSB3"/>
								<html:hidden property="datos(hiddNombreSB3)" value="1" styleId="hiddNombreSB3"/>
								<html:hidden property="datos(hiddLinkVerSB3)" value="1" styleId="hiddLinkVerSB3"/>
								<html:hidden property="datos(hiddLinkConocerSB3)" value="1" styleId="hiddLinkConocerSB3"/>
								<html:hidden property="datos(hiddLinkCotizarSB3)" value="1" styleId="hiddLinkCotizarSB3"/>
								<html:hidden property="datos(hiddTrackerSB3)" value="1" styleId="hiddTrackerSB3"/>
								<html:hidden property="datos(hiddImagenSB3)" value="1" styleId="hiddImagenSB3"/>
								<html:hidden property="datos(hiddPrincipalSB3)" value="1" styleId="hiddPrincipalSB3"/>
								<html:hidden property="datos(hiddOrdinalSB3)" value="1" styleId="hiddOrdinalSB3"/>
								
								<html:hidden property="datos(hiddIdPlanSB3X)" value="<bean:write name='idPlanSB3'></bean:write>" styleId="hiddIdPlanSB3X"/>
								<html:hidden property="datos(hiddLinkVerSB3X)" value="<bean:write name='linkVerBasesSB3'></bean:write>" styleId="hiddLinkVerSB3X"/>
								<html:hidden property="datos(hiddLinkConocerSB3X)" value="<bean:write name='linkConocerSB3'></bean:write>" styleId="hiddLinkConocerSB3X"/>
								<html:hidden property="datos(hiddLinkCotizarSB3X)" value="<bean:write name='linkCotizarSB3'></bean:write>" styleId="hiddLinkCotizarSB3X"/>
								<html:hidden property="datos(hiddTrackerSB3X)" value="<bean:write name='trackerSB3'></bean:write>" styleId="hiddTrackerSB3X"/>
								<html:hidden property="datos(hiddImagenSB3X)" value="<bean:write name='imagenSB3'></bean:write>" styleId="hiddImagenSB3X"/>
								<html:hidden property="datos(hiddNombreSB3X)" value="<bean:write name='nombreSB3'></bean:write>" styleId="hiddNombreSB3X"/>
							</table>
							
							
							<!-- Banner 4 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 4 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 5</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanSB4" type="text" value="<bean:write name="idPlanSB4"></bean:write>" size="12" name="idPlanSB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreSB4" class="hasDatepicker" type="text" value="<bean:write name="nombreSB4"></bean:write>" size="12" name="nombreSB4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerSB4" class="hasDatepicker" type="text" value="<bean:write name="trackerSB4"></bean:write>" size="12" name="trackerSB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenSB4" class="hasDatepicker" type="text" value="<bean:write name="imagenSB4"></bean:write>" size="12" name="imagenSB4">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesSB4" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesSB4"></bean:write>" size="12" name="linkVerBasesSB4">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerSB4" class="hasDatepicker" type="text" value="<bean:write name="linkConocerSB4"></bean:write>" size="12" name="linkConocerSB4">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarSB4" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarSB4"></bean:write>" size="12" name="linkCotizarSB4">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeSB4)" name="informeComercialTarjetas" styleId="inputFechaDesdeSB4" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaSB4)" name="informeComercialTarjetas" styleId="inputFechaHastaSB4" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenSB4').value);">
								</td>
								<html:hidden property="datos(hiddRamaSB)" value="1" styleId="hiddRamaSB"/>
								<html:hidden property="datos(hiddIdPlanSB4)" value="1" styleId="hiddIdPlanSB4"/>
								<html:hidden property="datos(hiddNombreSB4)" value="1" styleId="hiddNombreSB4"/>
								<html:hidden property="datos(hiddLinkVerSB4)" value="1" styleId="hiddLinkVerSB4"/>
								<html:hidden property="datos(hiddLinkConocerSB4)" value="1" styleId="hiddLinkConocerSB4"/>
								<html:hidden property="datos(hiddLinkCotizarSB4)" value="1" styleId="hiddLinkCotizarSB4"/>
								<html:hidden property="datos(hiddTrackerSB4)" value="1" styleId="hiddTrackerSB4"/>
								<html:hidden property="datos(hiddImagenSB4)" value="1" styleId="hiddImagenSB4"/>
								<html:hidden property="datos(hiddPrincipalSB4)" value="1" styleId="hiddPrincipalSB4"/>
								<html:hidden property="datos(hiddOrdinalSB4)" value="1" styleId="hiddOrdinalSB4"/>
								
								<html:hidden property="datos(hiddIdPlanSB4X)" value="<bean:write name='idPlanSB4'></bean:write>" styleId="hiddIdPlanSB4X"/>
								<html:hidden property="datos(hiddLinkVerSB4X)" value="<bean:write name='linkVerBasesSB4'></bean:write>" styleId="hiddLinkVerSB4X"/>
								<html:hidden property="datos(hiddLinkConocerSB4X)" value="<bean:write name='linkConocerSB4'></bean:write>" styleId="hiddLinkConocerSB4X"/>
								<html:hidden property="datos(hiddLinkCotizarSB4X)" value="<bean:write name='linkCotizarSB4'></bean:write>" styleId="hiddLinkCotizarSB4X"/>
								<html:hidden property="datos(hiddTrackerSB4X)" value="<bean:write name='trackerSB4'></bean:write>" styleId="hiddTrackerSB4X"/>
								<html:hidden property="datos(hiddImagenSB4X)" value="<bean:write name='imagenSB4'></bean:write>" styleId="hiddImagenSB4X"/>
								<html:hidden property="datos(hiddNombreSB4X)" value="<bean:write name='nombreSB4'></bean:write>" styleId="hiddNombreSB4X"/>
							</table>
							
							
							<!-- Banner 5 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 5 </th>
									<td width="55">
									
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="0">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 6</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanSB5" type="text" value="<bean:write name="idPlanSB5"></bean:write>" size="12" name="idPlanSB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreSB5" class="hasDatepicker" type="text" value="<bean:write name="nombreSB5"></bean:write>" size="12" name="nombreSB5">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerSB5" class="hasDatepicker" type="text" value="<bean:write name="trackerSB5"></bean:write>" size="12" name="trackerSB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenSB5" class="hasDatepicker" type="text" value="<bean:write name="imagenSB5"></bean:write>" size="12" name="imagenSB5">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesSB5" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesSB5"></bean:write>" size="12" name="linkVerBasesSB5">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerSB5" class="hasDatepicker" type="text" value="<bean:write name="linkConocerSB5"></bean:write>" size="12" name="linkConocerSB5">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarSB5" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarSB5"></bean:write>" size="12" name="linkCotizarSB5">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeSB5)" name="informeComercialTarjetas" styleId="inputFechaDesdeSB5" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaSB5)" name="informeComercialTarjetas" styleId="inputFechaHastaSB5" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenSB5').value);">
								</td>
								<html:hidden property="datos(hiddRamaSB)" value="1" styleId="hiddRamaSB"/>
								<html:hidden property="datos(hiddIdPlanSB5)" value="1" styleId="hiddIdPlanSB5"/>
								<html:hidden property="datos(hiddNombreSB5)" value="1" styleId="hiddNombreSB5"/>
								<html:hidden property="datos(hiddLinkVerSB5)" value="1" styleId="hiddLinkVerSB5"/>
								<html:hidden property="datos(hiddLinkConocerSB5)" value="1" styleId="hiddLinkConocerSB5"/>
								<html:hidden property="datos(hiddLinkCotizarSB5)" value="1" styleId="hiddLinkCotizarSB5"/>
								<html:hidden property="datos(hiddTrackerSB5)" value="1" styleId="hiddTrackerSB5"/>
								<html:hidden property="datos(hiddImagenSB5)" value="1" styleId="hiddImagenSB5"/>
								<html:hidden property="datos(hiddPrincipalSB5)" value="1" styleId="hiddPrincipalSB5"/>
								<html:hidden property="datos(hiddOrdinalSB5)" value="1" styleId="hiddOrdinalSB5"/>
								
								<html:hidden property="datos(hiddIdPlanSB5X)" value="<bean:write name='idPlanSB5'></bean:write>" styleId="hiddIdPlanSB5X"/>
								<html:hidden property="datos(hiddLinkVerSB5X)" value="<bean:write name='linkVerBasesSB5'></bean:write>" styleId="hiddLinkVerSB5X"/>
								<html:hidden property="datos(hiddLinkConocerSB5X)" value="<bean:write name='linkConocerSB5'></bean:write>" styleId="hiddLinkConocerSB5X"/>
								<html:hidden property="datos(hiddLinkCotizarSB5X)" value="<bean:write name='linkCotizarSB5'></bean:write>" styleId="hiddLinkCotizarSB5X"/>
								<html:hidden property="datos(hiddTrackerSB5X)" value="<bean:write name='trackerSB5'></bean:write>" styleId="hiddTrackerSB5X"/>
								<html:hidden property="datos(hiddImagenSB5X)" value="<bean:write name='imagenSB5'></bean:write>" styleId="hiddImagenSB5X"/>
								<html:hidden property="datos(hiddNombreSB5X)" value="<bean:write name='nombreSB5'></bean:write>" styleId="hiddNombreSB5X"/>
							</table>
							
							
							<!-- Banner 6 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner 6 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaRamaPosicion" name="subcategoriaRamaPosicion">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="1">Banner 2</option>
											<option value="1">Banner 3</option>
											<option value="1">Banner 4</option>
											<option value="1">Banner 5</option>
											
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanSB6" type="text" value="<bean:write name="idPlanSB6"></bean:write>" size="12" name="idPlanSB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreSB6" class="hasDatepicker" type="text" value="<bean:write name="nombreSB6"></bean:write>" size="12" name="nombreSB6">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 256); padding: 16px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerSB6" class="hasDatepicker" type="text" value="<bean:write name="trackerSB6"></bean:write>" size="12" name="trackerSB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenSB6" class="hasDatepicker" type="text" value="<bean:write name="imagenSB6"></bean:write>" size="12" name="imagenSB6">
									</td>
								<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesSB6" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesSB6"></bean:write>" size="12" name="linkVerBasesSB6">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer M�s : </th>
									<td>
										<input id="linkConocerSB6" class="hasDatepicker" type="text" value="<bean:write name="linkConocerSB6"></bean:write>" size="12" name="linkConocerSB6">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarSB6" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarSB6"></bean:write>" size="12" name="linkCotizarSB6">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaDesdeSB6)" name="informeComercialTarjetas" styleId="inputFechaDesdeSB6" size="12"></html:text></td>
                                   		<th width="23%">Fecha T�rmino Publicaci�n: </th>
                                    	<td><html:text property="datos(fechaHastaSB6)" name="informeComercialTarjetas" styleId="inputFechaHastaSB6" size="12"></html:text></td>
								</tr>
								<td>
									<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenSB6').value);">
								</td>
								<html:hidden property="datos(hiddRamaSB)" value="1" styleId="hiddRamaSB"/>
								<html:hidden property="datos(hiddIdPlanSB6)" value="1" styleId="hiddIdPlanSB6"/>
								<html:hidden property="datos(hiddNombreSB6)" value="1" styleId="hiddNombreSB6"/>
								<html:hidden property="datos(hiddLinkVerSB6)" value="1" styleId="hiddLinkVerSB6"/>
								<html:hidden property="datos(hiddLinkConocerSB6)" value="1" styleId="hiddLinkConocerSB6"/>
								<html:hidden property="datos(hiddLinkCotizarSB6)" value="1" styleId="hiddLinkCotizarSB6"/>
								<html:hidden property="datos(hiddTrackerSB6)" value="1" styleId="hiddTrackerSB6"/>
								<html:hidden property="datos(hiddImagenSB6)" value="1" styleId="hiddImagenSB6"/>
								<html:hidden property="datos(hiddPrincipalSB6)" value="1" styleId="hiddPrincipalSB6"/>
								<html:hidden property="datos(hiddOrdinalSB6)" value="1" styleId="hiddOrdinalSB6"/>
								
								<html:hidden property="datos(hiddIdPlanSB6X)" value="<bean:write name='idPlanSB6'></bean:write>" styleId="hiddIdPlanSB6X"/>
								<html:hidden property="datos(hiddLinkVerSB6X)" value="<bean:write name='linkVerBasesSB6'></bean:write>" styleId="hiddLinkVerSB6X"/>
								<html:hidden property="datos(hiddLinkConocerSB6X)" value="<bean:write name='linkConocerSB6'></bean:write>" styleId="hiddLinkConocerSB6X"/>
								<html:hidden property="datos(hiddLinkCotizarSB6X)" value="<bean:write name='linkCotizarSB6'></bean:write>" styleId="hiddLinkCotizarSB6X"/>
								<html:hidden property="datos(hiddTrackerSB6X)" value="<bean:write name='trackerSB6'></bean:write>" styleId="hiddTrackerSB6X"/>
								<html:hidden property="datos(hiddImagenSB6X)" value="<bean:write name='imagenSB6'></bean:write>" styleId="hiddImagenSB6X"/>
								<html:hidden property="datos(hiddNombreSB6X)" value="<bean:write name='nombreSB6'></bean:write>" styleId="hiddNombreSB6X"/>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<html:hidden property="datos(hiddSubmit)" value="1" styleId="hiddSubmit"/>
										<input id="buscar" type="button" name="buscar" value="Previsualizar" onclick="previsualizarPromocion();">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
							</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</input>
	</html:form>
</body>
</html:html>
