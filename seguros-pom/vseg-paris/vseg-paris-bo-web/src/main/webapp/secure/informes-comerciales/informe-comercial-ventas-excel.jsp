<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="application/excel" language="java"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:x="urn:schemas-microsoft-com:office:excel"
	xmlns="http://www.w3.org/TR/REC-html40">
	<head>
		<meta http-equiv="Content-Type"
			content="text/html; charset=windows-1252" />
		<meta name="ProgId" content="Excel.Sheet" />
		<meta name="Generator" content="Microsoft Excel 11" />

		<style>
table {
	mso-displayed-decimal-separator: "\.";
	mso-displayed-thousand-separator: "\,";
}

@page {
	margin: .98in .79in .98in .79in;
	mso-header-margin: 0in;
	mso-footer-margin: 0in;
}

tr {
	mso-height-source: auto;
}

col {
	mso-width-source: auto;
}

br {
	mso-data-placement: same-cell;
}

.style0 {
	mso-number-format: General;
	text-align: general;
	vertical-align: bottom;
	white-space: nowrap;
	mso-rotate: 0;
	mso-background-source: auto;
	mso-pattern: auto;
	color: windowtext;
	font-size: 10.0pt;
	font-weight: 400;
	font-style: normal;
	text-decoration: none;
	font-family: Arial;
	mso-generic-font-family: auto;
	mso-font-charset: 0;
	border: none;
	mso-protection: locked visible;
	mso-style-name: Normal;
	mso-style-id: 0;
}

td {
	mso-style-parent: style0;
	padding-top: 1px;
	padding-right: 1px;
	padding-left: 1px;
	mso-ignore: padding;
	color: windowtext;
	font-size: 10.0pt;
	font-weight: 400;
	font-style: normal;
	text-decoration: none;
	font-family: Arial;
	mso-generic-font-family: auto;
	mso-font-charset: 0;
	mso-number-format: General;
	text-align: general;
	vertical-align: bottom;
	border: none;
	mso-background-source: auto;
	mso-pattern: auto;
	mso-protection: locked visible;
	white-space: nowrap;
	mso-rotate: 0;
}

.xl24 {
	mso-style-parent: style0;
	white-space: normal;
}

.xl25 {
	mso-style-parent: style0;
	font-weight: 700;
	text-align: center;
	vertical-align: middle;
	white-space: normal;
}

.xl26 {
	mso-style-parent: style0;
	mso-number-format: "Short Date";
}

.xl27 {
	mso-style-parent: style0;
	mso-number-format:
		"\0022$\0022\\ \#\,\#\#0\.00\;\[Red\]\\-\0022$\0022\\ \#\,\#\#0\.00";
	text-align: right;
}

.xl28 {
	mso-style-parent: style0;
	mso-number-format:
		"\0022$\0022\\ \#\,\#\#0\;\[Red\]\\-\0022$\0022\\ \#\,\#\#0";
	text-align: right;
}

.xl29 {
	mso-style-parent: style0;
	font-weight: 700;
	text-align: right;
	vertical-align: middle;
	white-space: normal;
}

.xl38 {
	mso-style-parent: style0;
	mso-number-format: 0;
	white-space: normal;
}

.xl39 {
	mso-style-parent: style0;
	mso-number-format: "\[$-F400\]h\:mm\:ss\\ AM\/PM";
	white-space: normal;
}

.xl67 {
	mso-style-parent: style0;
	mso-number-format: "\#\,\#\#0";
	text-align: right;
}

.xl68 {
	mso-style-parent: style0;
	mso-number-format: "\#\,\#\#0\.000";
	text-align: right;
}
</style>

	</head>

	<body bottommargin="0" leftmargin="0" marginheight="0" marginwidth="0"
		rightmargin="0" topmargin="0">
		<table>
			<tr>
				<td class="xl25">
					ID
				</td>
				<td class="xl25">
					N� Poliza
				</td>
				<td class="xl25">
					Fecha de Compra
				</td>
				<td class="xl25">
					Rut
				</td>
				<td class="xl25">
					Producto
				</td>
				<td class="xl25">
					Descripcion Plan
				</td>
				<td class="xl25">
					Rama
				</td>
				<td class="xl25">
					Compa�ia
				</td>
				<td class="xl25">
					Prima UF
				</td>
				<td class="xl25">
					Monto Proporcional
				</td>
				<td class="xl25">
					Medio de Pago
				</td>
				<td class="xl25">
					Cod. Autorizacion
				</td>
			</tr>
			<logic:present name="informe">
				<logic:iterate id="informeVehiculo" name="informe" scope="request"
					indexId="index">
					<tr>
						<td class="xl38">
							<bean:write name="informeVehiculo" property="idseguro" />
						</td>
						<td class="xl38">
							<bean:write name="informeVehiculo" property="numeropoliza" />
						</td>
						<td class="x124">
							<bean:write name="informeVehiculo" property="fechaseguro" format="dd-MM-yyyy"/>
						</td>
						<td class="xl38">
							<bean:write name="informeVehiculo" property="rutasegurado" />
						</td>
						<td class="x124">
							<bean:write name="informeVehiculo" property="nombreproducto" />
						</td>
						<td class="xl38">
							<bean:write name="informeVehiculo" property="nemotecnico" />
						</td>
						<td class="xl26">
							<bean:write name="informeVehiculo" property="rama" />
						</td>
						<td class="x124">
							<bean:write name="informeVehiculo" property="plannombre" />
						</td>
						<td class="x124">
							<bean:write name="informeVehiculo" property="primauf" />
						</td>
						<td class="x124">
							<bean:write name="informeVehiculo" property="proporcional" />
						</td>
						<td class="xl68">
							<bean:write name="informeVehiculo" property="mediopago" />
						</td>
						<td class="xl67">
							<bean:write name="informeVehiculo" property="codigoautorizacion" />
						</td>
					</tr>

				</logic:iterate>

			</logic:present>
		</table>

	</body>

</html>