<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
	<head>
		<html:base />
		<title>ETL</title>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />
		<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
		
		<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
		<link rel="stylesheet" type="text/css" href="../../css/jquery.alerts.css" />

		<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				function cargaTipos() {
					$("#d_tipo").show();
					$("#d_subtipo").hide();
					$("#d_producto").hide();					
					$("#d_plan").hide();

					$.getJSON("/vseg-paris-adm/secure/etl-obtener-tipos.do",{ajax: 'true'}, function(tipos){
						try {
							s_tipos = $('#f_buscar').find('select#tipo');
							s_tipos.find('option:gt(0)').remove();

							s_subtipos = $('#f_buscar').find('select#subtipo');
							s_subtipos.find('option:gt(0)').remove();

							s_productos = $('#f_buscar').find('select#producto');
							s_productos.find('option:gt(0)').remove();

							s_planes = $('#f_buscar').find('select#plan');
							s_planes.find('option:gt(0)').remove();

							var tabla = $('#f_tipo').find('#t_tipo');
							tabla.find('tr').remove();

							var html = "";
							var tbl = "";
							for(i=0; i<tipos.length; i++) {
								html += '<option value="'+tipos[i].codigo_tipo_prod_leg+'">'+tipos[i].nombre+'</option>';
								tbl += '<tr><td><input type="checkbox" name="tipos" value="'+tipos[i].codigo_tipo_prod_leg+'" />'+tipos[i].nombre+'</td></tr>'
							}
							s_tipos.append(html);
							tbl += '<tr><td><input type="button" name="enviar" id="enviar_tipo" value="Enviar"/></td></tr>';
							tabla.append(tbl);
							
							$('#enviar_tipo').click(function() {
								f = $("#f_tipo").serialize();
								jAlert('No implementado en esta version', 'Error');
							});
							
						} catch(err) {
			            	alert("error: "+err.message);
						}
					});
				}

				cargaTipos();

				
				$('select#tipo').change(function() {
					var oTipo = $('#f_buscar').find('select#tipo').val();

					s_subtipos = $('#f_buscar').find('select#subtipo');
					s_subtipos.find('option:gt(0)').remove();

					s_productos = $('#f_buscar').find('select#producto');
					s_productos.find('option:gt(0)').remove();

					s_planes = $('#f_buscar').find('select#plan');
					s_planes.find('option:gt(0)').remove();

					$("#d_tipo").hide();
					$("#d_subtipo").show();
					$("#d_producto").hide();					
					$("#d_plan").hide();

					var tabla = $('#f_subtipo').find('#t_subtipo');
					tabla.find('tr').remove();
			
					if(oTipo != '') {
						$.getJSON("/vseg-paris-adm/secure/etl-obtener-subtipos.do",{ajax: 'true', tipo: oTipo}, function(subtipos){
							try {
								var html = "";
								var tbl = "";
								for(i=0; i<subtipos.length; i++) {
									html += '<option value="'+subtipos[i].codigo_sub_tipo_prod_leg+'">'+subtipos[i].nombre+'</option>';
									tbl += '<tr><td><input type="checkbox" name="subtipos" value="'+subtipos[i].codigo_sub_tipo_prod_leg+'" />'+subtipos[i].nombre+'</td></tr>'
								}
								s_subtipos.append(html);
								tbl += '<tr><td><input type="button" name="enviar" id="enviar_subtipo" value="Enviar"/></td></tr>';
								tabla.append(tbl);

								$('#enviar_subtipo').click(function() {
									var d_tipo = $("#f_subtipo").find("input[type=hidden][name=tipo]");
									d_tipo.val(oTipo);

									f = $("#f_subtipo").serialize();

									jAlert('No implementado en esta version', 'Error');
								});

							} catch(err) {
				            	alert("error: "+err.message);
							}
						});
					} else {
						$("#d_tipo").show();
						$("#d_subtipo").hide();
						$("#d_producto").hide();					
						$("#d_plan").hide();
					}
				});

				$('select#subtipo').change(function() {
					var oTipo = $('#f_buscar').find('select#tipo').val();
					var oSubTipo = $('#f_buscar').find('select#subtipo').val();

					s_productos = $('#f_buscar').find('select#producto');
					s_productos.find('option:gt(0)').remove();

					s_planes = $('#f_buscar').find('select#plan');
					s_planes.find('option:gt(0)').remove();

					$("#d_tipo").hide();
					$("#d_subtipo").hide();
					$("#d_producto").show();					
					$("#d_plan").hide();
					
					var tabla = $('#f_producto').find('#t_producto');
					tabla.find('tr').remove();

					if(oSubTipo != '') {
						$.getJSON("/vseg-paris-adm/secure/etl-obtener-productos.do",{ajax: 'true', tipo: oTipo, subtipo: oSubTipo}, function(productos){
							try {

								var html = "";
								var tbl = "";
								for(i=0; i<productos.length; i++) {
									html += '<option value="'+productos[i].codigo_producto_leg+'">'+productos[i].nombre+'</option>';
									tbl += '<tr><td><input type="checkbox" name="productos" value="'+productos[i].codigo_producto_leg+'" />'+productos[i].nombre+'</td></tr>'
								}
								s_productos.append(html);
								tbl += '<tr><td><input type="button" name="enviar" id="enviar_producto" value="Enviar"/></td></tr>';
								tabla.append(tbl);

								$('#enviar_producto').click(function() {
									var d_tipo = $("#f_producto").find("input[type=hidden][name=tipo]");
									d_tipo.val(oTipo);

									var d_subtipo = $("#f_producto").find("input[type=hidden][name=subtipo]");
									d_subtipo.val(oSubTipo);

									jConfirm('Se copiaran los productos seleccionados', 'Confirmacion', function(r) {
										if(r) {
											var f = $("#f_producto").serializeArray();

											$.getJSON("/vseg-paris-adm/secure/etl-enviar-productos.do", f, function(e){
												try {
													jAlert('Productos enviados exitosamente.', 'Exito');
												} catch(eerr) {
													alert("error envio: " + err.message);
												}
											});
										}
									});
								});
								
							} catch(err) {
				            	alert("error: "+err.message);
							}
						});
					} else {
						$("#d_tipo").hide();
						$("#d_subtipo").show();
						$("#d_producto").hide();					
						$("#d_plan").hide();
					}
				});

				$('select#producto').change(function() {
					var oTipo = $('#f_buscar').find('select#tipo').val();
					var oSubTipo = $('#f_buscar').find('select#subtipo').val();
					var oProducto = $('#f_buscar').find('select#producto').val();

					s_planes = $('#f_buscar').find('select#plan');
					s_planes.find('option:gt(0)').remove();

					$("#d_tipo").hide();
					$("#d_subtipo").hide();
					$("#d_producto").hide();					
					$("#d_plan").show();
					
					var tabla = $('#f_plan').find('#t_plan');
					tabla.find('tr').remove();
					
					if(oProducto != '') {
						$.getJSON("/vseg-paris-adm/secure/etl-obtener-planes.do",{ajax: 'true', tipo: oTipo, subtipo: oSubTipo, producto: oProducto}, function(planes){
							try {

								var html = "";
								var tbl = "";
								for(i=0; i<planes.length; i++) {
									html += '<option value="'+planes[i].codigo_plan_leg+'">'+planes[i].nombre+'</option>';
									tbl += '<tr><td><input type="checkbox" name="planes" value="'+planes[i].codigo_plan_leg+'" />'+planes[i].nombre+' - '+planes[i].codigo_plan_leg+'</td></tr>'
								}
								s_planes.append(html);
								tbl += '<tr><td><input type="button" name="enviar" id="enviar_plan" value="Enviar"/></td></tr>';
								tabla.append(tbl);

								$('#enviar_plan').click(function() {
									var d_tipo = $("#f_plan").find("input[type=hidden][name=tipo]");
									d_tipo.val(oTipo);

									var d_subtipo = $("#f_plan").find("input[type=hidden][name=subtipo]");
									d_subtipo.val(oSubTipo);
									
									var d_producto = $("#f_plan").find("input[type=hidden][name=producto]");
									d_producto.val(oProducto);

									jConfirm('Se copiaran los planes seleccionados', 'Confirmacion', function(r) {
										if(r) {
											var f = $("#f_plan").serializeArray();

											$.getJSON("/vseg-paris-adm/secure/etl-enviar-planes.do", f, function(e){
												try {
													jAlert('Planes enviados exitosamente.', 'Exito');
												} catch(eerr) {
													alert("error envio: " + err.message);
												}
											});
										}
									});
								});
								
							} catch(err) {
				            	alert("error: "+err.message);
							}
						});
					} else {
						$("#d_tipo").hide();
						$("#d_subtipo").hide();
						$("#d_producto").show();					
						$("#d_plan").hide();
					}
				});

				function pintarSeleccionado(obj) {
					buscar = obj.parent().find("input[type=checkbox]").attr("value");

					obj.parent().parent().parent().find("input[type=checkbox]").each(function(){
						v = $(this).attr("value");
						if(v == buscar) {
							$(this).parent().parent().css('background-color', 'LightGrey');
						} else {
							$(this).parent().parent().css('background-color', '#f1f3f2');
						}
					});
				}

			});
		</script>
	</head>
	<body>
		<div style="display: table; width: 100%;">
			Carga ETL ::
			<br/>
			
			<div style="display: table; width: 100%;">
			<form action="" name="f_buscar" id="f_buscar">
				<table class="datos">
					<tr>
						<td>Tipo: 
							<select name="tipo" id="tipo">
								<option value="">Seleccione</option>
							</select>
						</td>
						<td>Sub-tipo:
							<select name="subtipo" id="subtipo">
								<option value="">Seleccione</option>
							</select>
						</td>
						<td>Producto:
							<select name="producto" id="producto">
								<option value="">Seleccione</option>
							</select>
						</td>
					</tr>
				</table>
				</form>
			</div>

			<br/>

			<div style="display: table; width: 100%;" id="d_tipo">
				<form name="f_tipo" id="f_tipo" action="#">
				<table class="datos" id="t_tipo">
					<tr><td>Tipo</td></tr>
				</table>
				</form>
			</div>

			<div style="margin: 5px; float: left; display: none;" id="d_subtipo">
				<form name="f_subtipo" id="f_subtipo" action="#">
				<input type="hidden" name="tipo" value="" />
				<table class="datos" id="t_subtipo">
					<tr><td>Subtipo</td></tr>
				</table>
				</form>
			</div>

			<div style="margin: 5px; float: left; display: none;" id="d_producto">
				<form name="f_producto" id="f_producto" action="#">
				<input type="hidden" name="tipo" value="" />
				<input type="hidden" name="subtipo" value="" />
				<table class="datos" id="t_producto">
					<tr><td>Producto</td></tr>
				</table>
				</form>
			</div>

			<div style="margin: 5px; float: left; display: none;" id="d_plan">
				<form name="f_plan" id="f_plan" action="#">
				<input type="hidden" name="tipo" value="" />
				<input type="hidden" name="subtipo" value="" />
				<input type="hidden" name="producto" value="" />
				<table class="datos" id="t_plan">
					<tr><td>Plan</td></tr>
				</table>
				</form>
			</div>
		</div>
	</body>
</html:html>