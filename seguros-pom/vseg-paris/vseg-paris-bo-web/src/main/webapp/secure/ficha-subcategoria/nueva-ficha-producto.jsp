<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title><bean:message bundle="labels"
			key="nueva-ficha-producto.nuevaFichaProducto" />
	</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />
	<link rel="stylesheet" type="text/css" href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
	<script type="text/javascript" charset="utf-8">
	var id_ficha = -1;
	
	function uploadfile(archivo){
		if( $("#"+archivo).attr("disabled") == false ) {
			$("#"+archivo+"_div").show();
			$.ajaxFileUpload( {
				url:'<html:rewrite action="/subir-archivo" module="/secure" />?tipo='+archivo, 
				secureuri:false, 
				fileElementId: archivo, 
				dataType: 'json', 
				success: function (data, status) {
					updloadFinished(archivo, data);
				},
				error: function (data, status, e) {
					jAlert(e);
					updloadFinished(archivo, data); 
				}
			});
		}
	} 
	
	function updloadFinished(archivo, data){
		if(data.estado == 'success') {
			$("#"+archivo).attr('disabled', true);
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.archivoListo" />');
		} else if (data.estado  == 'nofile') {
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.noSubioArchivo" />');		
		} else {
			$("#"+archivo+"_div").html(data.estado + ': ' + data.mensaje);
		}
	}
	
	function enviar_documentos() {
		if(id_ficha > 0) {
			jQuery("input[name='tipo']").each(function(i) {
	            jQuery(this).attr('disabled', 'disabled');
	        });
			$("select#rama").attr('disabled', true);
			$("select#subcategoria").attr('disabled', true);
			$("select#id_plan").attr('disabled', true);

			//Subir archivo imagen;
			uploadfile('imagen');
			uploadfile('beneficio');
			uploadfile('cobertura');
			uploadfile('aspecto_legal');
			uploadfile('exclusion');

	  		$("#ver_imagen").colorbox({width:'100%', title:'Imagen', href:"<html:rewrite action="/obtener-archivo" module="/secure"/>?tipo=imagen&id_ficha=" + id_ficha + "&x=imagen.jpg"});
	  		$("#ver_beneficio").colorbox({width:'100%', title:'Beneficio', href:"<html:rewrite action="/obtener-archivo" module="/secure"/>?tipo=beneficio&id_ficha=" + id_ficha});
	  		$("#ver_cobertura").colorbox({width:'100%', title:'Cobertura', href:"<html:rewrite action="/obtener-archivo" module="/secure"/>?tipo=cobertura&id_ficha=" + id_ficha});
	  		$("#ver_aspecto_legal").colorbox({width:'100%', title:'Aspecto Legal', href:"<html:rewrite action="/obtener-archivo" module="/secure"/>?tipo=aspecto_legal&id_ficha=" + id_ficha});
	  		$("#ver_exclusion").colorbox({width:'100%', title:'Exclusion', href:"<html:rewrite action="/obtener-archivo" module="/secure"/>?tipo=exclusion&id_ficha=" + id_ficha});

			//Subir archivos uno a uno.
			$("#guardar").attr('disabled', false);
		}
	}
	
	function habilitaGuardar(){
		$("#guardar").attr('disabled', false);
	}
	
	function validarTipo(item) {
		if(item.value == 0) {
			$("#id_plan").attr('disabled', false);
			$("#plan_1").show();
			$("#plan_2").show();
			var idSubcategoria = $("select#subcategoria").attr("value");
			$.getJSON("/vseg-paris-adm/buscar-planes.do",{idSubcategoria: idSubcategoria, ajax: 'true'}, function(j){
				$('select#id_plan option').remove();
				var options = '';
				options += '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_plan + '">' + j[i].nombre + '</option>';
                }
      			$('select#id_plan').html(options);
    		})
   		} else {
			$("#id_plan").attr('disabled', true);
			$("#plan_1").hide();
			$("#plan_2").hide();
		}
	}

	$(document).ready(function() {
	
		$("input[name='habilita_texto_legal']").prop("checked","");
	
		$("select#rama").change(function() {
			$.getJSON("/vseg-paris-adm/buscar-subcategorias.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
				$('select#subcategoria option').remove();
				var options = '';
				options += '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
                   }
      			$('select#subcategoria').html(options);
				$('select#id_plan option').remove();
				options = '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
      			$('select#id_plan').html(options);
    		})
		})
		
		$("select#subcategoria").change(function() {
			$.getJSON("/vseg-paris-adm/buscar-planes.do",{idSubcategoria: $(this).val(), ajax: 'true'}, function(j){
				$('select#id_plan option').remove();
				var options = '';
				options += '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_plan + '">' + j[i].nombre + '</option>';
                }
      			$('select#id_plan').html(options);
    		})		
		})
		
		$("#guardar").click(function() {
			$("#guardar").attr('disabled', true);
			$("#formulario").attr("ACTION","<html:rewrite action="/agregar-ficha-producto" module="/secure" />");
			$("#formulario").attr("action","<html:rewrite action="/agregar-ficha-producto" module="/secure" />");
			$("#formulario").attr("target", "frameOculto");
			
			if(!$("input[name='habilitar']").attr("checked")){
				$("input[name='habilitar']").val(0);
			}
			
			if($("input[name='habilita_texto_legal']").attr("checked")){
				$("input[name='habilita_texto_legal']").val("S");
			}else{
				$("input[name='habilita_texto_legal']").val("N");
			}
						
			$("#formulario").submit();
  		})
  		
	});
	</script>

</head>
<body>
	<html:form action="/nueva-ficha-producto.do" styleId="formulario">
		<table cellpadding="0" cellspacing="0" border="0" width="800"
			align="center" class="datos">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td width="100" colspan="6">
								<h2>
									<bean:message bundle="labels"
										key="nueva-ficha-producto.nuevaFichaProducto" />
								</h2>
							</td>

						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td width="700">
								<b><bean:message bundle="labels"
										key="nueva-ficha-producto.tipoFicha" />:</b>
								<html:radio property="tipo" value="1"
									onchange="validarTipo(this);">
									<bean:message bundle="labels"
										key="nueva-ficha-producto.subcategoria" />
								</html:radio>
								&nbsp;&nbsp;&nbsp;
								<html:radio property="tipo" value="0"
									onchange="validarTipo(this);">
									<bean:message bundle="labels"
										key="nueva-ficha-producto.promocion" />
								</html:radio>
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<th width="150">
								<bean:message bundle="labels" key="nueva-ficha-producto.rama" />
								:
							</th>
							<td width="150">
								<html:select property="rama" styleId="rama">
									<option value="">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="ramas" property="id_rama"
										labelProperty="titulo_rama" />
								</html:select>
							</td>
							<th width="150">
								<bean:message bundle="labels"
									key="nueva-ficha-producto.subcategoria" />
								:
							</th>
							<td width="150">
								<html:select property="subcategoria" styleId="subcategoria">
									<option value="">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<logic:present name="subcategorias">
										<html:options collection="subcategorias"
											property="id_subcategoria"
											labelProperty="titulo_subcategoria" />
									</logic:present>
								</html:select>
							</td>
							<th width="150" id="plan_1" style="display: none;">
								<bean:message bundle="labels" key="nueva-ficha-producto.plan" />
								:
							</th>
							<td width="150" id="plan_2" style="display: none;">
								<html:select property="id_plan" styleId="id_plan"
									disabled="disabled">
									<option value="">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
								</html:select>
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td>
								<b><bean:message bundle="labels"
										key="nueva-ficha-producto.habilitar" />:</b>
								<html:checkbox property="habilitar" value="1" />
							</td>
						</tr>
					</table>

					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2>
									<bean:message bundle="labels"
										key="nueva-ficha-producto.nombreComercial" />
									<html:text property="nombre_comercial" size="100" />
								</h2>
							</td>
						</tr>
					</table>

					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2>
									<bean:message bundle="labels"
										key="nueva-ficha-producto.descripcionFicha" />
									<html:textarea property="descripcion_ficha" rows="3" cols="99" />
								</h2>
							</td>
						</tr>
					</table>

					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2>
									<bean:message bundle="labels"
										key="nueva-ficha-producto.descripcionPagina" />
									<html:textarea property="descripcion_pagina" rows="3" cols="99" />
								</h2>
							</td>
						</tr>
					</table>
					
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td>
								<b><bean:message bundle="labels"
										key="nueva-ficha-producto.habilitar_texto_legal" />:</b>
								<html:checkbox property="habilita_texto_legal" value="N" />
							</td>
						</tr>
					</table>
					
					<table cellpadding="0" cellspacing="0" border="0" width="100%" class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2>
									<bean:message bundle="labels"
										key="nueva-ficha-producto.legalFicha" />
									<html:textarea property="texto_legal" rows="3" cols="99" />
								</h2>
							</td>
						</tr>
					</table>

					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2><bean:message bundle="labels"
										key="nueva-ficha-producto.ingresar" />
									<bean:message bundle="labels"
										key="nueva-ficha-producto.imagen" />
								</h2>
							</td>
						</tr>
						<tr>
							<th width="239">
								<bean:message bundle="labels"
									key="nueva-ficha-producto.imagen" />
								:
							</th>
							<td width="539">
								<input type="file" name="imagen" id="imagen" />
								&nbsp;&nbsp;
								<input type="button"
									value="<bean:message bundle="labels" key="general.ver"/>"
									id="ver_imagen" />
								<div style="display: none;" id="imagen_div">
									<bean:message bundle="labels" key="general.subiendo" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								&nbsp;
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2><bean:message bundle="labels"
										key="nueva-ficha-producto.ingresar" />
									<bean:message bundle="labels"
										key="nueva-ficha-producto.beneficios" />
								</h2>
							</td>
						</tr>
						<tr>
							<th width="239">
								<bean:message bundle="labels"
									key="nueva-ficha-producto.beneficios" />
								:
							</th>
							<td width="539">
								<input type="file" name="beneficio" id="beneficio" />
								&nbsp;&nbsp;
								<input type="button"
									value="<bean:message bundle="labels" key="general.ver"/>"
									id="ver_beneficio" />
								<div style="display: none;" id="beneficio_div">
									<bean:message bundle="labels" key="general.subiendo" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								&nbsp;
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2><bean:message bundle="labels"
										key="nueva-ficha-producto.ingresar" />
									<bean:message bundle="labels"
										key="nueva-ficha-producto.cobertura" />
								</h2>
							</td>
						</tr>
						<tr>
							<th width="239">
								<bean:message bundle="labels"
									key="nueva-ficha-producto.cobertura" />
								:
							</th>
							<td width="539">
								<input type="file" name="cobertura" id="cobertura" />
								&nbsp;&nbsp;
								<input type="button"
									value="<bean:message bundle="labels" key="general.ver"/>"
									id="ver_cobertura" />
								<div style="display: none;" id="cobertura_div">
									<bean:message bundle="labels" key="general.subiendo" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								&nbsp;
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2><bean:message bundle="labels"
										key="nueva-ficha-producto.ingresar" />
									<bean:message bundle="labels"
										key="nueva-ficha-producto.aspectoLegal" />
								</h2>
							</td>
						</tr>
						<tr>
							<th width="239">
								<bean:message bundle="labels"
									key="nueva-ficha-producto.aspectoLegal" />
							</th>
							<td width="539">
								<input type="file" name="aspecto_legal" id="aspecto_legal" />
								&nbsp;&nbsp;
								<input type="button"
									value="<bean:message bundle="labels" key="general.ver"/>"
									id="ver_aspecto_legal" />
								<div style="display: none;" id="aspecto_legal_div">
									<bean:message bundle="labels" key="general.subiendo" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								&nbsp;
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						class="datos">
						<tr>
							<td colspan="2" align="left">
								<h2><bean:message bundle="labels"
										key="nueva-ficha-producto.ingresar" />
									<bean:message bundle="labels"
										key="nueva-ficha-producto.exclusiones" />
								</h2>
							</td>
						</tr>
						<tr>
							<th width="239">
								<bean:message bundle="labels"
									key="nueva-ficha-producto.exclusiones" />
								:
							</th>
							<td width="539">
								<input type="file" name="exclusion" id="exclusion" />
								<input type="button"
									value="<bean:message bundle="labels" key="general.ver"/>"
									id="ver_exclusion" />
								<div style="display: none;" id="exclusion_div">
									<bean:message bundle="labels" key="general.subiendo" />
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								&nbsp;
							</td>
						</tr>

						<tr>

							<td colspan="2" align="center">
								<input type="button"
									value="<bean:message bundle="labels" key="general.guardar"/>"
									id="guardar" name="guardar" />
								&nbsp;&nbsp;
								<input type="button"
									value="<bean:message bundle="labels" key="general.cerrar"/>"
									onclick="javascript:window.close()" />
							</td>

						</tr>
					</table>
				</td>
			</tr>
		</table>
		<iframe id="frameOculto" name="frameOculto" frameborder="0" width="600" height="0" src="">
		</iframe>
	</html:form>
</body>
</html:html>
