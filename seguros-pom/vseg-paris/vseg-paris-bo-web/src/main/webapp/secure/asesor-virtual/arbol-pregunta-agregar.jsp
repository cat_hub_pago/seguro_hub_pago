<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>asesor virtual</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$("#agregarAlternativa").click(function() {
			try {
				cnt = $("input[name='alternativas'][type='hidden']").attr("value");
				$("input[name='alternativas'][type='hidden']").attr("value", ++cnt);
				
				var clon = $('#agregarAlternativa').prev().clone();
				clon.find("#texto0").attr("value", "");
				clon.find("#texto1").attr("value", "");
				clon.find("#valor0").attr("value", "");
				clon.find("#valor1").attr("value", "");
				clon.insertBefore("#agregarAlternativa");
			} catch (e) {
				alert(e.message);
			}
		})
		
		$(":button#guardar").click(function(){
			try {
				$("#formulario").attr("target", "controlador");
				$("#formulario").attr("ACTION","/vseg-paris-adm/arbol-pregunta-grabar.do");
				$("#formulario").attr("action","/vseg-paris-adm/arbol-pregunta-grabar.do");
	  			$("#formulario").submit();
	  			return false;
			} catch (e) {
				alert(e.message);
			}
		})
		
	});

	</script>
</head>
<body>
	<html:form action="/arbol-pregunta-agregar.do" styleId="formulario">
		<html:hidden property="alternativas" value="1" />
		<html:hidden property="actual" write="false"/>
		<div style="width: 590px; height: 290px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%"
				height="290" align="center" style="margin-top: 5px;" class="datos"
				id="datos">
				<tr>
					<th colspan="4">
						<div align="left">
							<img src="../../images/save.gif" />
							<bean:message bundle="labels"
								key="arbol-pregunta-agregar.agregarPregunta" />
						</div>
					</th>
				</tr>
				<tr>
					<td colspan="4">
						&nbsp;
					</td>
				</tr>
				<tr>
					<th>
						<div align="right">
							<bean:message bundle="labels"
								key="arbol-pregunta-agregar.pregunta" />
							:
						</div>
					</th>
					<td colspan="3">
						<html:text property="pregunta" styleId="pregunta"
							style="width: 300px;" />
					</td>
				</tr>
				<tr>
					<th>
						<div align="right">
							<bean:message bundle="labels"
								key="arbol-pregunta-agregar.tipo" />
							:
						</div>
					</th>
					<td colspan="3">
						<html:text property="tipo" styleId="tipo"
							style="width: 80px;" />
					</td>
				</tr>
				<tr>
					<th>
						<div align="right">
							<bean:message bundle="labels"
								key="arbol-pregunta-agregar.alternativa" />
							:
						</div>
					</th>
					<td colspan="3">
						<bean:message bundle="labels" key="arbol-pregunta-agregar.texto0" />
						:
						<html:text property="texto0" styleId="texto0"
							style="width: 200px;" value="" />
						<br />
						<bean:message bundle="labels" key="arbol-pregunta-agregar.texto1" />
						:
						<html:text property="texto1" styleId="texto1"
							style="width: 200px;" value="" />
						<br />
						<bean:message bundle="labels" key="arbol-pregunta-agregar.valor0" />
						:
						<html:text property="valor0" styleId="valor0"
							style="width: 200px;" value="" />
						<br />
						<bean:message bundle="labels" key="arbol-pregunta-agregar.valor1" />
						:
						<html:text property="valor1" styleId="valor1"
							style="width: 200px;" value="" />
						<br />
					</td>
				</tr>
				<tr id="agregarAlternativa">
					<td colspan="2" align="right" style="cursor: pointer;">
						+
						<bean:message bundle="labels"
							key="arbol-pregunta-agregar.agregarAlternativa" />

					</td>
				</tr>
				<tr>
					<th colspan="4">
						<input type="button"
							value="<bean:message bundle="labels" key="general.guardar"/>"
							id="guardar" name="guardar" />
					</th>
				</tr>
			</table>
		</div>
	</html:form>
</body>
</html:html>
