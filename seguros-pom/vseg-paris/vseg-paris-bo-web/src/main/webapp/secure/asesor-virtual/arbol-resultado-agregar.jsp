<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>asesor virtual</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$("select#rama").change(function() {
			$.getJSON("/vseg-paris-adm/buscar-subcategorias.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
				$('select#subcategoria option').remove();
				var options = '';
				options += '<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
                   }
      			$('select#subcategoria').html(options);
				$('select#id_plan option').remove();
				options = '<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>';
      			$('select#id_plan').html(options);
    		})
		})
		
		$("select#subcategoria").change(function() {
			$.getJSON("/vseg-paris-adm/buscar-planes.do",{idSubcategoria: $(this).val(), ajax: 'true'}, function(j){
				$('select#id_plan option').remove();
				var options = '';
				options += '<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_plan + '">' + j[i].nombre + '</option>';
                }
      			$('select#id_plan').html(options);
    		})		
		})

		$("#guardar").click(function() {
			$("#guardar").attr('disabled', true);
			var nombreplan = $('#id_plan :selected').text();
			$("input[name='nombreplan'][type='hidden']").attr("value", nombreplan);
			$("#formulario").attr("target","controlador");
			$("#formulario").attr("ACTION","/vseg-paris-adm/arbol-resultado-grabar.do");
			$("#formulario").attr("action","/vseg-paris-adm/arbol-resultado-grabar.do");
			$("#formulario").submit();
			return false;
  		})
	});
	</script>
</head>
<body>
	<html:form action="/arbol-resultado-agregar.do" styleId="formulario">
		<html:hidden property="actual" write="false" />
		<html:hidden property="nombreplan" value="" />
		<table cellpadding="0" cellspacing="0" border="0" width="62%"
			align="center" style="margin-top: 5px;" class="datos">
			<tr>
				<th colspan="4">
					<div align="left">
						<img src="../../images/save.gif" />
						Agregar Respuesta
					</div>
				</th>
			</tr>
			<tr>
				<th width="178">
					<div align="right">
						Titulo Respuesta:
					</div>
				</th>
				<td width="358" colspan="3">
					<html:text property="titulo" style="width: 300px;" />
				</td>
			</tr>
			<tr>
				<th width="178" align="left" valign="top">
					<div align="right">
						Producto o Plan:
					</div>
				</th>
				<td colspan="3" align="left" valign="top">
					<table cellpadding="0" cellspacing="0" border="0" width="350"
						align="left">
						<tr>
							<th width="83">
								<div align="right">
									Rama :
								</div>
							</th>
							<td width="267">
								&nbsp;
								<html:select property="rama" styleId="rama">
									<option value="">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<logic:notEmpty name="ramas">
										<html:options collection="ramas" property="id_rama"
											labelProperty="titulo_rama" />
									</logic:notEmpty>
								</html:select>
							</td>
						</tr>
						<tr>
							<th width="83">
								<div align="right">
									Subcategoría :
								</div>
							</th>
							<td width="267">
								&nbsp;
								<html:select property="subcategoria" styleId="subcategoria">
									<option value="0">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<logic:present name="subcategorias">
										<html:options collection="subcategorias"
											property="id_subcategoria"
											labelProperty="titulo_subcategoria" />
									</logic:present>
								</html:select>
							</td>
						</tr>
						<tr>
							<th width="83">
								<div align="right">
									Plan :
								</div>
							</th>
							<td width="267">
								&nbsp;
								<html:select property="id_plan" styleId="id_plan"
									disabled="disabled">
									<option value="0">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
								</html:select>
							</td>
						</tr>
					</table>
					<br />
				</td>
			</tr>
			<tr>
				<th width="178">
					<div align="right">
						Link0:
					</div>
				</th>
				<td width="358" colspan="3">
					<html:text property="link0" style="width: 300px;" value="http://" />
				</td>
			</tr>
			<tr>
				<th width="178">
					<div align="right">
						Link1:
					</div>
				</th>
				<td width="358" colspan="3">
					<html:text property="link1" style="width: 300px;" value="http://" />
				</td>
			</tr>
			<tr>
				<th width="178">
					<div align="right">
						icono:
					</div>
				</th>
				<td width="358" colspan="3">
					<html:text property="icono" style="width: 300px;" value="" />
				</td>
			</tr>
			<tr>
				<th width="178" valign="top">
					<div align="right">
						Texto:
					</div>
				</th>
				<td valign="top">
					<html:textarea property="value" cols="50" rows="10" />
				</td>
			</tr>
			<tr>
				<th colspan="4">
					<input type="button" value="Guardar" id="guardar" name="guardar" />
				</th>
			</tr>
		</table>
	</html:form>
</body>
</html:html>