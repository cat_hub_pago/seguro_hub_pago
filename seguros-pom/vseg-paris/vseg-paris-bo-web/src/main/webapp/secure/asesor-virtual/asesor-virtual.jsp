<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title><bean:message bundle="labels"
			key="asesor-virtual.asesorVirtual" />
	</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />
	<script type="text/javascript" charset="utf-8">
	var ventana = new Object();
	
	$(document).ready(function() {

		$(":button#pregunta").click(function() {
			try {
				var actual = $("input[name='actual'][type='hidden']").attr("value");
				if(actual.charAt(0) == 'A' || actual.charAt(0) == 'S'){

					//Desplegar pagina de agregar pregunta.
					var url = "<html:rewrite action="/arbol-pregunta-agregar" module="/secure" />";
					$("#formulario").attr("ACTION", url);
					$("#formulario").attr("action", url);
					parametros="width=630,height=330,top=160,left=200,scrollbars=yes,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
					ventana = window.open("","_pregunta",parametros);
					$("#formulario").attr("target", "_pregunta");
					$("#formulario").submit();
				} else {
					jAlert("Debe seleccionar una Alternativa", "Alerta");
				}
				return false;
			} catch (e) {
				e.message;
			}
		})
		
		$(":button#resultado").click(function() {
			try {
				var actual = $("input[name='actual'][type='hidden']").attr("value");

				if(actual.charAt(0) == 'A'){

					//Desplegar pagina de agregar resultados.
					var url = "<html:rewrite action="/arbol-resultado-agregar" module="/secure" />";
					$("#formulario").attr("ACTION", url);
					$("#formulario").attr("action", url);
					parametros="width=520,height=460,top=0,left=0,scrollbars=no,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
					ventana = window.open("","_resultado",parametros);
					$("#formulario").attr("target", "_resultado");
					$("#formulario").submit();
					return false;
				} else {
					jAlert("Debe seleccionar una Alternativa", "Alerta");
				}
			} catch (e) {
				e.message;
			}
		})
		
		$(":button#grabar").click(function() {
			$.getJSON("<html:rewrite action="/arbol-grabar" module="/secure" />",{ajax: 'true'}, function(j){
				if(j!=null && j.status == 'ok') {
					jAlert("Arbol almacenado en la base de datos", "Alerta");
				} else {
					jAlert("Error al almacenar �rbol en la base de datos", "Alerta");
				}
    		})
		})


		$(":button#enviar").click(function() {
			$.getJSON("<html:rewrite action="/arbol-exportar" module="/secure" />",{ajax: 'true'}, function(j){
				if(j!=null && j.status == 'ok') {
					$("#sincronizado").html('<bean:message bundle="labels" key="asesor-virtual.sincronizado" />');
					jAlert("Archivo fue exportado con �xito", "Alerta");
				} else {
					jAlert("Error al exportar archivo", "Alerta");
				}
    		})
		})
		
		$(":button#eliminar").click(function() {
			try {
				$("#formulario").attr("target", "controlador");
				$("#formulario").attr("ACTION","<html:rewrite action="/arbol-nodo-eliminar" module="/secure" />");
				$("#formulario").attr("action","<html:rewrite action="/arbol-nodo-eliminar" module="/secure" />");
	  			$("#formulario").submit();
	  			return false;
			} catch (e) {
				alert(e.message);
			}
		})


		<logic:equal value="1" parameter="sincronizado">
			var sin = '<bean:message bundle="labels" key="asesor-virtual.sincronizado" />';
		</logic:equal>
		<logic:notEqual value="1" parameter="sincronizado">
			var sin = '<bean:message bundle="labels" key="asesor-virtual.noSincronizado" />';
		</logic:notEqual>
		$("#sincronizado").html(sin);
		
		<logic:equal value="1" parameter="estado">
			var est = '<bean:message bundle="labels" key="asesor-virtual.completo" />';
		</logic:equal>
		<logic:notEqual value="1" parameter="estado">
			var est = '<bean:message bundle="labels" key="asesor-virtual.incompleto" />';
		</logic:notEqual>
		$("#estado").html(est);
	});
	
	function agregado(msg) {
		jAlert(msg, "Asesor Virtual");
		//recargar arbol.
		$("#arbol").attr("src", $("#arbol").attr("src"));
	}
	</script>
</head>
<body>
	<form action="/vseg-paris-adm/asesor-virtual.do" name="formulario"
		id="formulario" method="post">
		<table cellpadding="0" cellspacing="0" border="0" width="800"
			align="center" class="datos">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td colspan="2">
								<bean:message bundle="labels" key="asesor-virtual.asesorVirtual" />
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						align="center" id="lista" style="display: noneX">
						<tr>
							<td>
								<br />
								<table border="1" cellspacing="0" bordercolor="#CCCCCC"
									width="60%" style="text-align: center">
									<tr>
										<th>
											<bean:message bundle="labels"
												key="asesor-virtual.sincronizacion" />
											: &nbsp;
										</th>
										<td>
											<div id="sincronizado">
											</div>
										</td>
										<td>
											<input type="button" name="grabar" id="grabar"
												value="<bean:message bundle="labels" key="asesor-virtual.grabarArbol"/>" />
											<input type="button" name="enviar" id="enviar"
												value="<bean:message bundle="labels" key="asesor-virtual.enviarArbol"/>" />
										</td>
									</tr>
								</table>
								<br />
							</td>
						</tr>
						<tr>
							<td colspan="6">
								<input type="button" name="pregunta" id="pregunta"
									value="<bean:message bundle="labels" key="asesor-virtual.agregarPregunta"/>" />
								&nbsp;
								<input type="button" name="resultado" id="resultado"
									value="<bean:message bundle="labels" key="asesor-virtual.agregarResultado"/>" />
								&nbsp;
								<input type="button" name="eliminar" id="eliminar"
									value="<bean:message bundle="labels" key="asesor-virtual.eliminarNodo"/>" />
							</td>
						</tr>
						<tr>
							<td>
								<iframe src="/vseg-paris-adm/arbol-decision.do" frameborder="0"
									width="800" height="400" id="arbol" name="arbol"></iframe>
							</td>
						</tr>
						<tr>
							<th colspan="6" bgcolor="#CCCCCC">
								<input type="hidden" name="actual" value="" />
								<bean:message bundle="labels" key="asesor-virtual.estadoArbol" />
								:
								<div id="estado"></div>
							</th>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<iframe src="" id="controlador" name="controlador" height="0"
			frameborder="0" width="500"></iframe>
	</form>
</body>
</html:html>
