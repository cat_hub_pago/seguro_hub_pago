<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Arbol decision</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery.treeview.js"></script>
	<link rel="stylesheet" type="text/css" href="../../css/screen.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.treeview.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<script type="text/javascript" charset="utf-8">
	function _debug(msg) {
		document.write(msg+"<br/>");
	}
	
	
	$(document).ready(function() {
		$.getJSON("/vseg-paris-adm/desplegar-arbol-decision.do",{ajax: 'true'}, function(j){
			try {
				var _arbol="";
				var rptas = [];
				var r = 0;
				var f_completed = false;
				if(j.secciones != null) {
					_arbol+="<ul id='secciones' class='filetree'>";
					_arbol+="<li>";
					_arbol+="<span class='folder' id='raiz'><a href=\"javascript:set_actual('')\">secciones</a></span>";

					var secciones = j.secciones;
					_arbol+="<ul>";

					for (var i = 0; i < secciones.length; i++) {
						var id_seccion = "S" + i + "_"  + secciones[i].id + "S"

						_arbol+="<li id='"+id_seccion+"'>";
						_arbol+="<span class='folder' id='NODO"+id_seccion+"'><a href=\"javascript:set_actual('"+id_seccion+"')\">" + secciones[i].titulo+"<a></span>";

						if(secciones[i].preguntas.preguntas !=null) {
							var preguntas = secciones[i].preguntas.preguntas;
							
							_arbol+="<ul id='" + 'P' + id_seccion + "'>";

							for(var a = 0; a < preguntas.length; a++) {
								var id_pregunta = "P" + i + "_"  + preguntas[a].id + "P";

								_arbol+="<li id='"+id_pregunta+"'>";
								_arbol+="<span class='folder' id='NODO"+id_pregunta+"'><a href=\"javascript:set_actual('"+id_pregunta+"')\">" + preguntas[a].titulo + "</a></span>";

								if(preguntas[a].alternativas != null ){
									var alternativas = preguntas[a].alternativas;

									_arbol+="<ul>";

									for(var b=0; b < alternativas.length; b++) {
										var id_alternativa = "A" + i + "_" + a + "_" + b + "_" + alternativas[b].id + "A";
										
										var texto = alternativas[b].texto0 + " " + alternativas[b].texto1 + " " 
													+ alternativas[b].valor0 + " " + alternativas[b].valor1;
										if(alternativas[b].proximaPregunta > 0 ){
											var id_prox = 'P' + i + "_" + alternativas[b].proximaPregunta + 'P';
											rptas[r++] = {id:id_alternativa, prox:id_prox};
										}

										_arbol+="<li id='" + id_alternativa + "'>";
										_arbol+="<span class='file' id='NODO"+id_alternativa+"'><a href=\"javascript:set_actual('"+id_alternativa+"')\">" + texto + "</a></span>";

										//RECORRER PARA BUSCAR LAS RESPUESTAS.
										if(alternativas[b].proximaPregunta != null && alternativas[b].proximaPregunta == 0 && secciones[i].respuestas.resultados != null) {
											var resultados = secciones[i].respuestas.resultados;

											_arbol+="<ul>";

											for(var c = 0; c < resultados.length; c++) {

												var id_resultado = "R" + i + "_" + a + "_" + b + "_" + c + "_" + resultados[c].id + "R";

												if( (alternativas[b].respuesta == resultados[c].id)) {
													var caption = resultados[c].titulo + ": " + resultados[c].nombreplan;
													
													_arbol+="<li id='" + id_resultado + "'><span class='file' id='NODO"+id_resultado+"'><a href=\"javascript:set_actual('"+id_resultado+"')\">" + caption + "</a></span></li>";
													break;
												}	//FIN IF ALTERNATIVA = RESPUESTA
											}	//FIN FOR RESULTADOS
											_arbol+="</ul>";
										}	//FIN VALIDACION RESPUESTA
										else {
											_arbol+="<ul id='dummy"+id_alternativa+"'>";
											_arbol+="<li id='borrar"+id_alternativa+"'><span class='folder'>borrar"+id_alternativa+"</span></li>";
											_arbol+="</ul>";											
										}
										_arbol+="</li>";
									}	//FIN FOR ALTERNATIVAS
									_arbol+="</ul>";
								}	//FIN IF ALTERNATIVAS
								_arbol+="</li>";								
							}	//FIN FOR PREGUNTAS
							_arbol+="</ul>";
						}	//FIN IF PREGUNTAS!=null
						_arbol+="</li>";
		            }	//FIN FOR SECCIONES
					_arbol+="</ul>";
					
					_arbol+="</li>";
					_arbol+="</ul>";
					
					$("#arbol").append(_arbol);

	            }
            }catch(err) {
            	alert("error: "+err.message);
            }

			//Mover nodos.
			for(var x=rptas.length-1; x >= 0 ; x--) {
				//Buscar nodo a replicar.
				var clon = $("#"+rptas[x].prox).clone();
				clon.attr("id", clon.attr("id")+'xx');
				clon.appendTo("#dummy"+rptas[x].id);
			}
			
			//Borrar nodos sobrantes;
			for(var x=rptas.length-1; x >= 0 ; x--) {
				try {
					$("#borrar"+rptas[x].id).remove();
					$("#"+rptas[x].prox).remove();
				} catch(err) {
					alert(err.message);
				}
			}
			
			$("#secciones").treeview({collapsed: false, persist: "location", unique: false});
   		})
  			
  	});
  	
  	var last = '';
  	function set_actual(id){
  		try{
  			if(last != '') {
  				var a = document.getElementById('NODO'+last);
  				if(a != null) {
					a.style.backgroundColor='';
				}
  			}
  			var a = document.getElementById('NODO'+id);
  			if(a != null) {
				a.style.backgroundColor='#f1f3f2';
				last = id;
	  			parent.$("input[name='actual'][type='hidden']").attr("value", id);
  			}
  		} catch (e) {
  			alert(e.message);
  		}
  	}
  	
  	function borrarNodo() {
  		var id = parent.$("input[name='actual'][type='hidden']").attr("value");
  		if(id != '') {
  			var a = $('#NODO'+id);
  			alert(a.parent().attr('id'));
  		} else {
  			parent.jAlert("Debe seleccionar un nodo");
  		}
  	}
	</script>
</head>

<body>
	<div id="arbol">
	</div>
</body>
</html:html>
