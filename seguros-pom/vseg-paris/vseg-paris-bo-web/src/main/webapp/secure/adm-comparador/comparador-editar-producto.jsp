<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
  <head>
    <html:base />
    <meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	
    <title>Comparador de Planes - Adm Productos</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript" src="../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

	<link rel="stylesheet" type="text/css" href="../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
	<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
	<link href="../../js/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
	
	
	
	<script type="text/javascript">
		var arrPlanes;
	
		function updateMap(obj) {
			var id = obj.id;
			var value = escape(obj.value);
			var editar = $("select#editar").attr("value");			
			
			$.getJSON("<html:rewrite action="/actualizar-map-caracteristicas" module="/secure/adm-comparador" />",{idCaract: id, valor: value, idTipo: editar, ajax: 'true'}, function(j){
				if (typeof(j.mensaje) != 'undefined' && j.mensaje != null && j.mensaje != '') {
					jAlert(j.mensaje, "");
				}
			});
		}
		
		function textCounter(field, maxlimit) {
			if (field.value.length > maxlimit) {
				var mensaje	= "El texto no debe superar los " + maxlimit + " caracteres.";	
				jAlert(mensaje, "");
				field.value = field.value.substring(0, maxlimit);
			} else {
				return false;
			}
		}
		
		function breadCrumb() {
			var idSubcategoria = $("select#subcategoria").attr("value");
			var idProducto = $("select#producto").attr("value");
			var idEditar = $("select#editar").attr("value");
			
			if (typeof($('#td_breadCrumb')) != 'undefined') {
				$('#td_breadCrumb').remove();
			}
			
			var txtSubcategoria = '';
			var txtProducto = '';
			var txtEditar = '';
			
			if (typeof(idSubcategoria) != 'undefined' && idSubcategoria != -1) {
				txtSubcategoria = $('#subcategoria :selected').text();
				
				if (typeof(idProducto) != 'undefined' && idProducto != -1) {
					txtProducto = $('#producto :selected').text();
				}
				
				if (typeof(idEditar) != 'undefined' && idEditar != -1) {
					txtEditar = $('#editar :selected').text();
				} 
			}
			
			var html = '';
			html += '<td id="td_breadCrumb" colspan="6">';
			html += '<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">';
			html += '<tr>';
			html += '<td bgcolor="#dbe1dd" width="2%">&nbsp;</td>';
			html += '<td bgcolor="#dbe1dd" width="45%">';
			html += '<b>&nbsp;' + txtSubcategoria + ' &nbsp;|&nbsp;' + txtProducto + ' &nbsp;|&nbsp;' + txtEditar + '</b>';
			html += '</td>'
			html += '<td bgcolor="#dbe1dd" width="45%">';
			if (typeof(idEditar) != 'undefined' && idEditar == 1) {
				html += '&nbsp; Habilitado &nbsp;<input type="checkbox" id="chk_habilitado"/> ';
			} else {
				html += '&nbsp;';
			}
			html += '</td>';
			html += '<td width="5%">&nbsp;</td>';
			html += '</tr>';
			html += '</table>';
			html += '</td>';
			
			$('#tr_breadCrumb').append(html);
			$('#tr_breadCrumb').show();
		}
		
		function redireccionar() {
		/*	var url = "<html:rewrite action="desplegarAdmComprador" module="/secure/adm-comparador" />";  				
			$("#formulario").attr("action", url + "?ramaSelected=1");
			$("#formulario").submit();
			return false; */
			
			var idProducto = $("select#producto").attr("value");
			var idSubcategoria = $("select#subcategoria").attr("value");			
			
			var url = "<html:rewrite action="/modificar-producto" module="/secure/adm-comparador" />";
			$("#idProducto").attr("value", idProducto);
			$("#idSubcategoria").attr("value", idSubcategoria);
			$("#formulario").attr("action", url); 
			$("#formulario").submit();
			return false;
			
		}
		
		function eliminarFila(idCaracteristica) {		
			var editar = $('select#editar').attr('value');
			
			jConfirm('�Esta seguro que desea eliminar la fila?', '', function(r) {
			
			if (r) {
				$.getJSON('<html:rewrite action="/eliminar-fila-caracteristica" module="/secure/adm-comparador" />', {idCaracteristica: idCaracteristica, idTipo: editar, ajax: 'true'}, function(j){
					
					if (editar == 3) {
						var listCaractComp = j.mapCaractComp
						desplegarCoberturasCompartidas(listCaractComp.listCaractComp);
					} else {
						var listPlanes = j.mapPlanes.columnData;
						var listValorPlanes = j.mapCaract;
						desplegarCaracteristicas(listPlanes, listValorPlanes);
					}
								
				});
			}});
		}
		
		function desplegarCoberturasCompartidas(listCaractComp) {
			$('#listCaracteristicas tbody').remove();
			html = '';
			
			if (listCaractComp != null && typeof(listCaractComp) != 'undefined') {
				html += '<tr>';
				//html += '<td width="90">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				html += '<td align="center" width="240" colspan="2"> Descripci�n Cobertura</td>';
				html += '<td align="center" width="180"> Tooltip </td>';
				html += '<td align="center" width="180"> Monto </td>';
				html += '<td>&nbsp;</td></tr>';
			
			
				if (listCaractComp.length > 0) {
					for (j=0 ; j < listCaractComp.length; j++) {
						var count = j + 1;
						html += '<tr>';
						html += '<td nowrap="nowrap" width="90"> Campo ' + count + ' </td>';		
						html += '<td align="left" width="150"><TEXTAREA ROWS="2" onkeydown="textCounter(this, 100);" cols="30" name="desc_caract" id="desc_mod_' + listCaractComp[j].id_caracteristica + '" onblur="updateMap(this)">' + listCaractComp[j].descripcion + '</TEXTAREA></td>';
						html += '<td align="center"><TEXTAREA ROWS="2" onkeydown="textCounter(this, 100);" cols="13" id="tooltip_' + listCaractComp[j].id_caracteristica + '" onblur="updateMap(this)">';
						
						if (typeof(listCaractComp[j].tooltip) != 'undefined' && listCaractComp[j].tooltip != null) {
							html += listCaractComp[j].tooltip;
						}
						html += '</TEXTAREA></td>';
						html += '<td align="center"><TEXTAREA ROWS="2" onkeydown="textCounter(this, 100);" cols="13" id="value_' + listCaractComp[j].id_caracteristica + '" onblur="updateMap(this)">';
						
						if (typeof(listCaractComp[j].valor) != 'undefined' && listCaractComp[j].valor != null) {
							html += listCaractComp[j].valor;
						}
						
						html += '</TEXTAREA></td>';
						html += '<td align="left" nowrap="nowrap" class="linkMenu"><a href="javascript:void(0);" onclick="eliminarFila(' + listCaractComp[j].id_caracteristica + ')">- eliminar fila</a></td>';
						html += '</tr>';
					}				
				} else {
					for (j=0 ; j < 4; j++) {
						var count = j + 1;							
						html += '<tr>';
						html += '<td nowrap="nowrap" width="90"> Campo ' + count + ' </td>';		
						html += '<td align="left" width="150"><TEXTAREA ROWS="2" cols="30" onkeydown="textCounter(this, 100);" id="desc_new_' + j + '" name="desc_caract" onblur="updateMap(this)"></TEXTAREA></td>';
						html += '<td align="center"><TEXTAREA ROWS="2" cols="13" onkeydown="textCounter(this, 100);" id="tooltip_' + j + '" onblur="updateMap(this)"></TEXTAREA></td>';
						html += '<td align="center"><TEXTAREA ROWS="2" cols="13" onkeydown="textCounter(this, 100);" id="value_' + j + '" onblur="updateMap(this)"></TEXTAREA></td>';
						html += '<td align="left" nowrap="nowrap" class="linkMenu"><a href="javascript:void(0);" onclick="eliminarFila(' + j + ')">- eliminar fila</a></td>';
						html += '</tr>';
					}			
				}
				
				html += '<tr><td style="padding-left:0px;" colspan="5" >';
				html += '<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" id="trAgregarFila"></table>';
				html += '</td></tr>';
			
			} 
			
			$('#listCaracteristicas').append(html);
			$('#listCaracteristicas').show();
			$('#table_btn').show();
		}
		
		function agregarFila() {
			var descripciones = $("TEXTAREA[name='desc_caract']");
			var editar = $("select#editar").attr("value");
			var count = descripciones.length;
			
			for (i=0; i < descripciones.length; i++) {
				var arrId = descripciones[i].id.split('_');
				var optTemp = arrId[2];
				
				if (arrId[1] == 'new' && optTemp != null && optTemp != undefined) {
					if (Number(optTemp) > Number(count)) {
						count = optTemp;
					}
				}				
			}
			count = Number(count) + 1;
			if (count < 10) {
			   nro = count + '&nbsp;&nbsp;';
			} else {
				nro = count;
			}
			html = '';
			
			if (editar == 3) {
				html += '<tr>';
				html += '<td nowrap="nowrap" width="90"> Campo ' + nro + ' </td>';		
				html += '<td align="left" width="150"><TEXTAREA ROWS="2" cols="30" onkeydown="textCounter(this, 100);" id="desc_new_' + count + '" name="desc_caract" onblur="updateMap(this)"></TEXTAREA></td>';
				html += '<td align="center" width="180"><TEXTAREA ROWS="2" cols="13" onkeydown="textCounter(this, 100);" id="tooltip_' + count + '" onblur="updateMap(this)"></TEXTAREA></td>';
				html += '<td align="center" width="180"><TEXTAREA ROWS="2" cols="13" onkeydown="textCounter(this, 100);" id="value_' + count + '" onblur="updateMap(this)"></TEXTAREA></td>';
				html += '<td align="left" nowrap="nowrap" class="linkMenu"><a href="javascript:void(0);" onclick="eliminarFila(' + j + ')">- eliminar fila</a></td>';
				html += '</tr>';
			} else {
				if (arrPlanes.length > 0) {
					html += '<tr>';	
					html += '<td nowrap="nowrap" width="90">Campo ' + nro + ' </td>'
					html += '<td align="left" width="125"><TEXTAREA ROWS="2" cols="25" onkeydown="textCounter(this, 100);" name="desc_caract" id="desc_new_' + count + '" onblur="updateMap(this)"></TEXTAREA></td>';
					html += '<td align="left" width="100"><TEXTAREA ROWS="2" cols="16" onkeydown="textCounter(this, 100);" name="tooltip_caract" id="tooltip_' + count + '" onblur="updateMap(this)"></TEXTAREA></td>';
					for (var i = 0; i < arrPlanes.length; i++) {				
						html += '<td align="center" width="140"><TEXTAREA ROWS="2" onkeydown="textCounter(this, 100);" cols="10" size="8" id="' + count + '_' + arrPlanes[i].id_plan + '" onblur="updateMap(this)"></TEXTAREA></td>';
					}
					html += '<td align="left" nowrap="nowrap" class="linkMenu"><a href="javascript:void(0);" onclick="eliminarFila(' + count + ')">- eliminar fila</a></td>';
					html += '</tr>';			
				
				}
			}			
			
			$('#trAgregarFila').append(html);
			return false;
		}
		
		function desplegarCaracteristicas(listPlanes, listValorPlanes) {
			$('#listCaracteristicas tbody').remove();
						
			var html = '';
			
			if (listPlanes != null && listPlanes.length > 0) {
					arrPlanes = listPlanes;
 					html += '<tr>';
 					//html += '<td width="90">&nbsp;</td>';
 					html += '<td align="center" width="215" colspan="2"> Descripci�n </td>';
 					html += '<td align="left" width="100"> Tooltip </td>';
					for (var i = 0; i < listPlanes.length; i++) {				
						html += '<td align="center" width="140">' + listPlanes[i].nombre_cia_leg;
						html += '<br/>';
						html += listPlanes[i].nombre + '</td>';
						
					}				
					html += '<td>&nbsp;</td></tr>';
				
					if (listValorPlanes != null && typeof(listValorPlanes) != "undefined") {
						var count = 0;
						for (var key in listValorPlanes) {
						
							if (listValorPlanes.hasOwnProperty(key)) {
								var valorPlan = listValorPlanes[key];
								count++;
								
								if (count < 10) {
									   nro = count + '&nbsp;&nbsp;';
								} else {
									nro = count;
								}
								
								if (valorPlan != null && valorPlan.length > 0) {
								
									html += '<tr>';
									html += '<td nowrap="nowrap" width="90">Campo ' + nro + ' </td>'
									html += '<td align="left" width="125"><TEXTAREA ROWS="2" cols="25" onkeydown="textCounter(this, 100);" name="desc_caract" id="desc_mod_' + valorPlan[0].id_caracteristica + '" onblur="updateMap(this)">' + valorPlan[0].descripcion + '</TEXTAREA></td>';
									html += '<td align="left" width="100"><TEXTAREA ROWS="2" cols="16" onkeydown="textCounter(this, 100);" name="tooltip_caract" id="tooltip_' + valorPlan[0].id_caracteristica + '" onblur="updateMap(this)">';
									
									if (valorPlan[0].tooltip != null) {
										html += valorPlan[0].tooltip;
									}
									
									html += '</TEXTAREA></td>';
									
									var habilitado = false;
									if (valorPlan[0].tipo == 1 && valorPlan[0].habilitado == 1) {
										habilitado = true;
									}
									
									if (typeof($('#chk_habilitado')) != 'undefined') {
						  					$('#chk_habilitado').attr('checked', habilitado);
						  			}
									
									for (var j = 0; j < listPlanes.length; j++) {
										var conValor = false;
										for (i=0 ; i < valorPlan.length; i++) {
											if (listPlanes[j].id_plan == valorPlan[i].id_plan) {
												html += '<td align="center" width="140"><TEXTAREA ROWS="2" onkeydown="textCounter(this, 100);" cols="10" id="' + valorPlan[i].id_caracteristica + '_' + valorPlan[i].id_plan + '" onblur="updateMap(this)">';
												
												if (typeof(valorPlan[i].valor) != 'undefined' && valorPlan[i].valor != null) {
													html += valorPlan[i].valor;
												}
												
												html += '</TEXTAREA></td>';
												conValor = true;
												break;
											} 
										}
										if (!conValor) {
											html += '<td align="center" width="140"><TEXTAREA ROWS="2" cols="10" onkeydown="textCounter(this, 100);" id="' + valorPlan[0].id_caracteristica + '_' + listPlanes[j].id_plan + '" onblur="updateMap(this)"></TEXTAREA></td>';
										}
									}
									html += '<td align="left" nowrap="nowrap" class="linkMenu"><a href="javascript:void(0);" onclick="eliminarFila(' + valorPlan[0].id_caracteristica + ')">- eliminar fila</a></td>';
									html += '<td>&nbsp;</td></tr>';										
								} else {
									html += '<tr>';	
									html += '<td nowrap="nowrap" width="90">Campo ' + nro + ' </td>'
									html += '<td align="left" width="125"><TEXTAREA ROWS="2" cols="25" name="desc_caract" onkeydown="textCounter(this, 100);" id="desc_new_0" onblur="updateMap(this)"></TEXTAREA></td>';
									html += '<td align="left" width="100"><TEXTAREA ROWS="2" cols="16" name="tooltip_caract" onkeydown="textCounter(this, 100);" id="tooltip_0" onblur="updateMap(this)"></TEXTAREA></td>';
									
									for (var i = 0; i < listPlanes.length; i++) {				
										html += '<td align="center" width="140"><TEXTAREA ROWS="2" cols="10" onkeydown="textCounter(this, 100);" id="' + i + '_' + listPlanes[i].id_plan + '" onblur="updateMap(this)"></TEXTAREA></td>';
																			
									}
									html += '<td align="left" nowrap="nowrap" class="linkMenu"><a href="javascript:void(0);" onclick="eliminarFila(0)">- eliminar fila</a></td>';
									html += '</tr>';
								}
							}
						}
					} else {
						for (j=0 ; j < 4; j++) {
							var count = j + 1;
							
							if (count < 10) {
								nro = count + '&nbsp;&nbsp;';
							} else {
								nro = count;
							}
														
							html += '<tr>';	
							html += '<td nowrap="nowrap" width="90">Campo ' + nro + ' </td>'
							html += '<td align="left" width="125"><TEXTAREA ROWS="2" cols="25" name="desc_caract" onkeydown="textCounter(this, 100);" id="desc_new_' + j + '" onblur="updateMap(this)"></TEXTAREA></td>';
							html += '<td align="left" width="100"><TEXTAREA ROWS="2" cols="16" name="tooltip_caract" onkeydown="textCounter(this, 100);" id="tooltip_' + j + '" onblur="updateMap(this)"></TEXTAREA></td>';
																					
							for (var i = 0; i < listPlanes.length; i++) {				
								html += '<td align="center" width="140"><TEXTAREA ROWS="2" cols="10" onkeydown="textCounter(this, 100);" id="' + j + '_' + listPlanes[i].id_plan + '" onblur="updateMap(this)"></TEXTAREA></td>';
							}
							html += '<td align="left" nowrap="nowrap" class="linkMenu"><a href="javascript:void(0);" onclick="eliminarFila(' + j + ')">- eliminar fila</a></td>';
							html += '</tr>';
						}								
					}
			} else {
				html += '<tr>'
				html += '<td align="left" colspan="5">No existen informaci�n de planes para los filtros ingresados.</td>'
				html += '</tr>'
			}
			
			var colspan = listPlanes.length + 4;
			
			html += '<tr><td style="padding-left:0px;" colspan="' + colspan + '" >';
			html += '<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" id="trAgregarFila"></table>';
			html += '</td></tr>';
			html += '';
			$('#listCaracteristicas').append(html);
			//colores('listCaracteristicas');
			$('#listCaracteristicas').show();
			$('#table_btn').show();
			
		}
	
	</script>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
  			
  			$("select#subcategoria").change(function() {
  				$('#listCaracteristicas').hide();
  				$('#table_btn').hide();
  				
				$.getJSON("/vseg-paris-adm/buscar-productos.do",{idSubcategoria: $(this).val(), ajax: 'true'}, function(j){
					$('select#producto option').remove();
					var options = '';
					options += '<option value="-1"><bean:message bundle="labels" key="general.seleccione" /></option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].id_producto + '">' + j[i].nombre + '</option>';
                    }
	      			$('select#producto').html(options);
	    		})
	    		
	    		breadCrumb();
  			})
  			
  			$("select#producto").change(function(){
  				$('#listCaracteristicas').hide();
  				$('#table_btn').hide();
  				
  				$("select#editar").attr("value", "-1");
  				breadCrumb();
  			});
  			
  			$("select#editar").change(function() {
  				var idSubcategoria = $("select#subcategoria").attr("value");
  				var idProducto = $("select#producto").attr("value");
  				var editar = $(this).val();
  				var idEstado = $("input[name='tipo']:checked").val();
  				breadCrumb();
  				
  				if (idSubcategoria == -1) {
  					jAlert("Debe selecionar una subcategoria.","");
  					return false;  				
  				}
  				
  				if (idProducto == -1) {
  					jAlert("Debe selecionar un producto.","");
  					return false;
  				}
  				
  				if (editar == 3) {
  					$.getJSON("<html:rewrite action="/buscar-coberturas-compartidas" module="/secure/adm-comparador" />",{idEstado: idEstado, idProducto: idProducto, ajax: 'true'}, function(j){
						desplegarCoberturasCompartidas(j.listCaractComp);
	    			});
	    			return false;
  				} else if (editar == -1) {
  					$('#listCaracteristicas').hide();
  					$('#table_btn').hide();
  				} else {
  					$.getJSON("<html:rewrite action="/buscar-planes" module="/secure/adm-comparador" />",{idEstado: idEstado, idProducto: idProducto, tipoCaracteristica: editar, ajax: 'true'}, function(j){
						var listPlanes = j.mapPlanes.columnData;
						var listValorPlanes = j.mapCaract;
						
						desplegarCaracteristicas(listPlanes, listValorPlanes);
	    			});
  				} 
  					  				
				return false;
  			})
  			
  			$("#guardar").click(function() {  			
  				var editar = $("select#editar").attr("value");
  				var idProducto = $("select#producto").attr("value");
  				var habilitado = '';
  				
  				if (typeof($('#chk_habilitado')) != 'undefined') {
  					habilitado = $('#chk_habilitado').attr('checked');
  				}
  			
  				$.getJSON("<html:rewrite action="/save-caracteristicas" module="/secure/adm-comparador" />", {idTipo: editar, habilitado: habilitado, idProducto: idProducto, ajax : 'true'}, function(j){
  					jAlert(j.status, "", redireccionar);  
  					
  				});  
  			});
  			
  			$("#preview").fancybox({	
				'onStart'			: function() {
											var idSubcategoria = $("select#subcategoria").attr("value");
											var idProducto = $("select#producto").attr("value");
											var idEstado = $("input[name='tipo']:checked").val();
											var idTipoCaract = $("select#editar").attr("value");
																					
											var txtSubcategoria = '';
											var txtProducto = '';
											
											if (typeof(idSubcategoria) != 'undefined' && idSubcategoria != -1) {
												txtSubcategoria = $('#subcategoria :selected').text();
												
												if (typeof(idProducto) != 'undefined' && idProducto != -1) {
													txtProducto = $('#producto :selected').text();
												}						
											}
											
											var url = '<html:rewrite action="/comparador-previsualizar" module="/secure/adm-comparador" />';
											url += '?idEstado=' + idEstado + '&idProducto=' + idProducto + '&txtSubcategoria=' + txtSubcategoria + '&txtProducto=' + txtProducto + '&idTipo=' + idTipoCaract;
											$("#preview").attr("href", url);
											return true;					
											
										},
				'width'				: 800,
				'height'			: '90%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
		});
  			
  			
  			
  		});
  		
  		//breadCrumb();
  		
	</script>

 </head>
  
  <body>
    <html:form action="/buscar-producto-comparador.do" styleId="formulario">
    	<html:hidden property="idProducto" value="" styleId="idProducto" />
    	<html:hidden property="idSubcategoria" value="" styleId="idSubcategoria" />
    	<input type="hidden" id="idRama" name="idRama" value="<bean:write name="idRama"/>" />
    	<div id="arreglo"></div>
		<table cellpadding="0" cellspacing="0" border="1" width="800px" style="overflow: auto;" align="center" class="datos">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="98%" align="center">
						<tr>
							<td colspan="6">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
									<tr>
										<td width="150">
											<b>
												Comparador de Planes
											</b>
										</td>

														

										<td colspan="2" bgcolor="#dbe1dd">

													<bean:message bundle="labels" key="comparador.adm-producto.tipoProducto.titulo" />

													<html:radio property="tipo" value="1" >
														<bean:message bundle="labels" key="comparador.adm-producto.tipoProducto.activo" />
													</html:radio>&nbsp;&nbsp;&nbsp;
													<html:radio property="tipo" value="0" >
														<bean:message bundle="labels" key="comparador.adm-producto.tipoProducto.inactivo" />
													</html:radio>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="15">&nbsp;</td>
						</tr>
						<tr>							
							<th width="90">
								<bean:message bundle="labels" key="comparador.adm-producto.filtro.subcategoria" />
								:
							</th>
							<td width="220">
								<html:select property="subcategoria" styleId="subcategoria">
									<option value="-1">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="listSubcategorias" property="id_subcategoria" labelProperty="titulo_subcategoria"/>
								</html:select>
							</td>
							<th width="80">
								<bean:message bundle="labels" key="comparador.adm-producto.filtro.producto" />
								:
							</th>
							<td width="150">
								<html:select property="producto" styleId="producto">
									<option value="-1">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="listProductos" property="id_producto" labelProperty="nombre"/>
								</html:select>
							</td>
							<th width="80">
								Editar:
							</th>
							<td width="150">
								<html:select property="editar" styleId="editar">
									<option value="-1"><bean:message bundle="labels" key="general.seleccione" /></option>									
									<option value="1">Beneficios</option>
									<option value="2">Coberturas</option>
									<option value="3">Coberturas Compartidas</option>
									<option value="4">Exclusiones</option>
								</html:select>
							</td>
						</tr>
						<tr>
							<td colspan="6">
								&nbsp;
							</td>
						</tr>
						<tr id="tr_breadCrumb" style="display: none;">
							
						</tr>
						<tr>
							<td colspan="6">
								&nbsp;
							</td>
						</tr>						
					</table>
					<div style="width:800px; height:auto; overflow:auto;">
					<table cellpadding="0" cellspacing="0" border="0" width="97%" 
						align="center" id="listCaracteristicas" style="display: none;">
							
							
					</table>
					</div>
					<table cellpadding="0" cellspacing="0" border="0" width="95%"
						align="center" id="table_btn" style="display: none">
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td align="center">
								<a href="javascript:void(0);"  onclick="javascript:agregarFila();" >- agregar campo</a>
							</td>
						</tr>
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td align="center">
								<a href="javascript:void(0);" id="preview" style="vertical-align: text-bottom;"><img src="../../images/btn_previsualizar.png" border="0" width="120" height="24" align="bottom"></img></a> &nbsp;&nbsp;
								<a href="javascript:void(0);" id="guardar" style="vertical-align: text-bottom;"><img src="../../images/btn_guardar.png" border="0" width="120" height="24" align="bottom"></img></a>
							</td>
						</tr>
					</table>														
				</td>
			</tr>
		</table>
	</html:form>
  </body>
</html:html>
