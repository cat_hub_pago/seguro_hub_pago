<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />

	<title>administrar-pagina-intermedia.jsp</title>

	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<script type="text/javascript" charset="utf-8">
	
	function nueva_subcategoria(){
		var url="<html:rewrite action="/administrar-subcategoria" module="/secure"/>";
		parametros="width=505,height=305,top=150,left=300,scrollbars=no,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
		window.open(url,"_detalle",parametros);
	}
	
	function mod_adm(id){
		var url="<html:rewrite action="/modificar-pagina-intermedia" module="/secure"/>?id="+id;
		parametros="width=505,height=305,top=150,left=300,scrollbars=no,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
		window.open(url,"_detalle",parametros);
	}
	
	function loadSubcategoria(obj, tipoObj){
		if(tipoObj === undefined){
			$.getJSON("/vseg-paris-adm/buscar-subcategorias.do",{idRama: obj.val(), ajax: 'true'}, function(j){
				$('select#subcategoria option').remove();
				var options = '';
				options += '<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
			    }
			    $('select#subcategoria').html(options);
			})
		}
		else{
			$.getJSON("/vseg-paris-adm/buscar-subcategorias-tipo.do",{idRama: obj.val(), idTipo: tipoObj.val(), ajax: 'true'}, function(j){
				$('select#subcategoria option').remove();
				var options = '';
				options += '<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>';
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
			    }
			    $('select#subcategoria').html(options);
			})
		}
	}
	
	$(document).ready(function() {
		$("select#rama").change(function() {
				$.getJSON("/vseg-paris-adm/buscar-rama-tipos.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
					$('select#tipo option').remove();
					var options = '';
					options += '<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>';
					if(j.length > 0){
						$('#labelTipo').attr('style','display:inline;');
						$('#selectTipo').attr('style','display:inline;');
						for (var i = 0; i < j.length; i++) {
							options += '<option value="' + j[i].id_tipo + '">' + j[i].descripcion_tipo + '</option>';
					    }
					    $('select#tipo').html(options);
					    $('select#subcategoria option').remove();
					    $('select#subcategoria').html('<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>');
					}
					else{
						$('#selectTipo').attr('style','display:none;');
						$('#labelTipo').attr('style','display:none;');
						$('select#tipo option').remove();
						loadSubcategoria($('select#rama'));
					}
				})
		});
		
		$("select#tipo").change(function() {
			if($(this).val() != '' && $(this).val() != 0){
				loadSubcategoria($('select#rama'),$(this));
			}
		});
  			
		$("#buscar").click(function() {
			var idRama = $("select#rama").attr("value");
			var idSubcategoria = $("select#subcategoria").attr("value");
			$.getJSON("<html:rewrite action="/buscar-lista-pagina-intermedia" module="/secure"/>", {idRama: idRama, idSubcategoria: idSubcategoria, ajax : 'true'}, function(j){
				$('#lista tbody').remove();
				var html = '<tbody>';
				for (var i = 0; i < j.length; i++) {
					html += '<tr>'
					html += '<td align="center">' + j[i].titulo_subcategoria + '</td>';
					html += '<td align="center">' + j[i].titulo_rama + '</td>';
					if(j[i].es_eliminado == 1) {
						html += '<td align="center"><bean:message bundle="labels" key="general.desactivado" /></td>';							
					} else {
						html += '<td align="center"><bean:message bundle="labels" key="general.activado" /></td>';							
					}
					html += '<td align="center"><img src="../../images/modif.gif" onClick="javascript:mod_adm(' + j[i].id_subcategoria + ');" style="cursor:pointer"/></td>';
					html += '</tr>';
				}
				html += '</tbody>';
				$('#lista').append(html);
				colores('lista');
				$('#lista').show();
			})
		});
  	});
	</script>
</head>

<html:form action="/administrar-pagina-intermedia">
	<body>
		<table cellpadding="0" cellspacing="0" border="0" width="800"
			align="center" class="datos">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						align="center">
						<tr valign="top">
							<td height="33">
								<div align="left">
									<bean:message bundle="labels"
										key="administrar-pagina-intermedia.administracion" />
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" width="100%"
										align="center">
										<tr>
											<th width="26%">
												<bean:message bundle="labels"
													key="administrar-pagina-intermedia.rama" />
												:
											</th>
											<td width="23%">
												<html:select property="rama" styleId="rama">
													<option value="">
														<bean:message bundle="labels" key="general.seleccione" />
													</option>
													<html:options collection="ramas" property="id_rama"
														labelProperty="titulo_rama" />
												</html:select>
											</td>
											<th width="10%">
												<div id="labelTipo" style="display:none;">
												<bean:message bundle="labels" key="administrar-pagina-intermedia.tipo" />
												:</div>
											</th>
											<td><div id="selectTipo" style="display:none;">
												<html:select property="tipo" styleId="tipo">
													<option value="">
														<bean:message bundle="labels" key="general.seleccione" />
													</option>
												</html:select>
												</div>
											</td>
											<th width="26%">
												<bean:message bundle="labels"
													key="administrar-pagina-intermedia.subcategoria" />
												:
											</th>
											<td width="41%">
												<html:select property="subcategoria" styleId="subcategoria">
													<option value="0">
														<bean:message bundle="labels" key="general.seleccione" />
													</option>
												</html:select>
											</td>
										</tr>
										<tr>
											<td colspan="4" align="center">
												<br />
												<input type="button" id="buscar" name="buscar"
													value="<bean:message bundle="labels" key="general.buscar"/>" />
											</td>
										</tr>
										<tr>
											<td colspan="4">
												<a href="javascript:nueva_subcategoria();"><bean:message bundle="labels" key="modificar-pagina-intermedia.agregarSubcategoria" /></a>&nbsp;
											</td>
										</tr>
									</table>
									<table cellpadding="0" cellspacing="0" border="0" width="100%"
										align="center" id="lista" style="display: none">
										<thead>
											<tr class="listadoSegCab">
												<td width="9%" align="center">
													<bean:message bundle="labels"
														key="administrar-pagina-intermedia.subcategoria" />
												</td>
												<td width="6%" align="center">
													<bean:message bundle="labels"
														key="administrar-pagina-intermedia.rama" />
												</td>
												<td width="6%" align="center">
													<bean:message bundle="labels" key="general.estado" />
												</td>
												<td width="7%" align="center">
													<bean:message bundle="labels" key="general.modificar" />
												</td>
											</tr>
										</thead>
									</table>
								</div>
							</td>
						</tr>


					</table>
				</td>
			</tr>
		</table>

	</body>
</html:form>
</html:html>
