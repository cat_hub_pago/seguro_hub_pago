<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Insert title here</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$("#modificar").click(function() {
			$("#formulario").attr("target","frameOculto");
			$("#formulario").attr("ACTION","/vseg-paris-adm/secure/mantenedor-usuario/modificar-Usuario-Cencesud-Guardar.do");
			$("#formulario").attr("action","/vseg-paris-adm/secure/mantenedor-usuario/modificar-Usuario-Cencesud-Guardar.do");
			$("#formulario").submit();
		});
		
		$("#cancelar").click(function() {
			$("#formulario").attr("target","");
			$("#formulario").attr("ACTION","/vseg-paris-adm/secure/mantenedor-usuario/inicio.do");
			$("#formulario").attr("action","/vseg-paris-adm/secure/mantenedor-usuario/inicio.do");
			$("#formulario").submit();
		});
	});
	</script>
</head>
<body>
	<html:errors />

	<h3>
		<bean:message bundle="labels" key="mantenedor.usuario.titulo" />
	</h3>
	<h2>
		<logic:notEqual name="estado" value="true">
			<bean:message bundle="labels"
				key="mantenedor.usuario.titulo.crear_usuario_bloqueado" />
		</logic:notEqual>
	</h2>
	<html:form action="/modificar-Cliente-Cencosud" method="post"
		styleId="formulario">
		<table class="datos" width="280">
			<tr>
				<td>
					<html:hidden property="datos(userName)" />
					<h2>
						<bean:message bundle="labels" key="mantenedor.usuario.nombre" />
					</h2>
				</td>
				<td>
					<html:text property="datos(nombre)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels"
							key="mantenedor.usuario.apellido_paterno" />
					</h2>
				</td>
				<td>
					<html:text property="datos(apellido_paterno)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels"
							key="mantenedor.usuario.apellodo_materno" />
					</h2>
				</td>
				<td>
					<html:text property="datos(apellido_materno)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels" key="mantenedor.usuario.telefono_1" />
					</h2>
				</td>
				<td>
					<html:text property="datos(telefono_1)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels" key="mantenedor.usuario.telefono_2" />
					</h2>
				</td>
				<td>
					<html:text property="datos(telefono_2)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels" key="mantenedor.usuario.email" />
					</h2>
				</td>
				<td>
					<html:text property="datos(email)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels" key="mantenedor.usuario.calle" />
					</h2>
				</td>
				<td>
					<html:text property="datos(calle)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels" key="mantenedor.usuario.numero" />
					</h2>
				</td>
				<td>
					<html:text property="datos(numero)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels"
							key="mantenedor.usuario.numero_departamento" />
					</h2>
				</td>
				<td>
					<html:text property="datos(numero_departamento)"
						styleId="datos(numero_departamento)" />
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels"
							key="mantenedor.usuario.estado_usuario" />
					</h2>
				</td>
				<td>
					<logic:equal name="estado" value="true">
						<input type="radio" name="datos(combo)" checked="checked"
							value="si" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_si" />
					|
					<input type="radio" name="datos(combo)" value="no" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_no" />
					</logic:equal>
					<logic:notEqual name="estado" value="true">
						<input type="radio" name="datos(combo)" value="si" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_si" />
					|
					<input type="radio" name="datos(combo)" value="no"
							checked="checked" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_no" />
					</logic:notEqual>
				</td>
			</tr>
			<tr>
				<td>
					<h2>
						<bean:message bundle="labels"
							key="mantenedor.usuario.estado_password" />
					</h2>
				</td>
				<td>
					<logic:equal name="estado_password" value="true">
						<input type="radio" name="datos(combo_password)" checked="checked"
							value="si" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_si" />
					|
					<input type="radio" name="datos(combo_password)" value="no" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_no" />
					</logic:equal>
					<logic:notEqual name="estado_password" value="true">
						<input type="radio" name="datos(combo_password)" value="si" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_si" />
					|
					<input type="radio" name="datos(combo_password)" value="no"
							checked="checked" />
						<bean:message bundle="labels"
							key="mantenedor.usuario.radio_mensaje_no" />
					</logic:notEqual>
				</td>
			</tr>
			<tr>
				<td>
					<input type="button" name="modificar" id="modificar"
						value="<bean:message bundle="labels" key="mantenedor.usuario.boton_modificar" />" />
				</td>
				<td>
					<input type="button" name="cancelar" id="cancelar"
						value="<bean:message bundle="labels" key="mantenedor.usuario.boton_cancelar" />" />
				</td>
			</tr>
		</table>
	</html:form>
	<iframe name="frameOculto" id="frameOculto" frameborder="0" src=""
		height="0" width="500"></iframe>
</body>
</html:html>
