<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.struts.Globals"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Mantenedor de usuarios</title>

	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>

	<script src="../../js/jquery.alerts.js" type="text/javascript"></script>
	<link href="../../js/jquery.alerts.css" rel="stylesheet"
		type="text/css" media="screen" />

	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<script src="jquery-1.3.2.min.js" type="text/javascript"></script>

	<script>
		$(document).ready(function(){
			$("#radio_").click(function(){
	        	if($("input[name='rad']:checked").val()==1) {
	        		$("#oculto").css("display", "none");
	        		$("#oculto2").css("display", "block");
	        	}
	        	else{
	        		$("#oculto2").css("display", "none");
	        		$("#oculto").css("display", "block");
	        	}
			});
   
			$("#boton").click(function(){
				if($("input[name='rad']:checked").val()==1)
				{
					jAlert($("input[name='rad']:checked").val());
				} else {
					var url = "<html:rewrite action='/nuevo-personal-cencosud' module='/secure/mantenedor-usuario'/> "; 
					$(location).attr('href',url);
				} 
			});
			
			$("#buscarUsuario").click(function() {
				var caractCaja = $("input#userName").val().length    
				if( caractCaja ==0){
					jAlert("el campo de busqueda no puede estar vacio","Error");
				}
				else{
					$("form#mi-form").submit();
				} 
			});

			$("#buscarCliente").click(function() {
				$("form#modificarUsuarioCencosudForm").attr("target", "");
				$("form#modificarUsuarioCencosudForm").attr("ACTION", "/vseg-paris-adm/secure/mantenedor-usuario/modificar-Cliente-Cencosud.do");
				$("form#modificarUsuarioCencosudForm").attr("action", "/vseg-paris-adm/secure/mantenedor-usuario/modificar-Cliente-Cencosud.do");
				$("form#modificarUsuarioCencosudForm").submit();
			});

			var msg_error = "<bean:write name="<%=Globals.EXCEPTION_KEY%>" property="message" ignore="true" />";
			if(msg_error != '') {
				jAlert("<li>"+msg_error+"</li>", "Error");
			} else {
				var errores = "<html:errors />";
				if(errores != "") {
					jAlert(errores, "<bean:message key="general.errores"/>");
				}
			}

		});


</script>

</head>


<body>

	<h1>
		<bean:message bundle="labels" key="mantenedor.usuario.titulo" />
	</h1>

	<div id="radio_">
		<table class="datos">
			<tr>
				<td>
					<bean:message bundle="labels"
						key="mantenedor.usuario.radio.cliente" />
				</td>
				<td>
					<input type="radio" name="rad" value="1" />
				</td>
			</tr>
			<tr>
				<td>
					<bean:message bundle="labels"
						key="mantenedor.usuario.radio.personal_cenco" />
				</td>
				<td>
					<input type="radio" name="rad" value="2" />
				</td>
			</tr>
		</table>
	</div>

	<div id="oculto" style="display: none;">
		<html:form action="/modificarUsuarioCencosud" styleId="mi-form"
			method="post">

			<table class="datos">
				<tr>
					<td>
						Buscar:
						<br />
						<br />
					</td>
					<td>
						<input name="datos(userName)" id="userName" type="text" />
						<input type="button" name="buscarUsuario" id="buscarUsuario"
							value="<bean:message bundle="labels"
								key="mantenedor.usuario.boton_mensaje.buscar" />" />
						<br />
						<br />
					</td>
				</tr>
				<tr>
					<td>
						Crear
						<br />
						<br />
					</td>
					<td>
						<input type="button" id="boton" name="boton" value="Nuevo Usuario" />
						<br />
						<br />
					</td>
				</tr>
			</table>
		</html:form>

	</div>

	<div id="oculto2" style="display: none">
		<html:form action="/modificar-Cliente-Cencosud"
			styleId="modificarUsuarioCencosudForm">
			<table class="datos">
				<tr>
					<td>
						rut
						<br />
					</td>
					<td>
						<input type="text" name="datos(rut)" id="datos(rut)" size="6" />
						-
						<input type="text" name="datos(dv)" id="datos(dv)" size="1" />
						<br />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" id="buscarCliente" name="buscarCliente"
							value="Buscar cliente" />
						<br />
					</td>
				</tr>
			</table>
		</html:form>
	</div>
</body>

</html:html>