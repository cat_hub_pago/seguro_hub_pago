
<%@page import="cl.tinet.common.seguridad.model.UsuarioInterno"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%><%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<%
    String path = request.getContextPath();
    String basePath =
        request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + path + "/";
            
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<title>My JSP 'index.jsp' starting page</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

		<script type="text/javascript"
			src="<%=request.getContextPath()%>/js/jquery-1.4.2.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/js/jquery.colorbox.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/js/jquery.alerts.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/js/jquery-ui-1.8.4.custom.min.js"></script>
		<link rel="stylesheet" type="text/css"
			href="<%=request.getContextPath()%>/css/jquery.alerts.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=request.getContextPath()%>/css/smoothness/jquery-ui-1.8.4.custom.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=request.getContextPath()%>/css/style_seguros.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=request.getContextPath()%>/css/colorbox.css" />

		<script type="text/javascript">
	
	 function SubmitFormulario() {
      userName = document.formularioLogin.userName.value;
      clave =    document.formularioLogin.clave.value;
      document.formularioLogin.username.value = userName;
      document.formularioLogin.password.value = clave;
      document.forms.formularioLogin.submit();  
   }
	</script>


	</head>

	<body>
		<form action="<%=request.getContextPath()%>/autenticacion/Login"
			method="post" name="formularioLogin">
			
			<logic:equal name="CONECTADO" value="false">
				<h2>Debe loguearse al sistema</h2>
	
				<input type="hidden" name="username" value="">
				<input type="hidden" name="password" value="">
				<table>
					<tr>
						<td>
							<bean:message key="login.bo.usuario" bundle="labels" />
						</td>
						<td>
							<input name="userName" type="text" class="textbox"
								value="" size="16" maxlength="16" />
						</td>
					</tr>
					<tr>
						<td>
							<bean:message key="login.bo.clave" bundle="labels" />
						</td>
						<td>
							<input name="clave" type="password" class="textbox"
								value="" size="16" maxlength="16" />
						</td>
					</tr>
	
					<tr>
						<td colspan="2">
							<input type="button" onclick="SubmitFormulario();" value="entrar" id="entrar" />
						</td>
					</tr>
				</table>
	
				<logic:notEmpty name="ERROR_LOGIN">
					<script>jAlert("<bean:write name="ERROR_LOGIN"/>", "Error");</script>
				</logic:notEmpty>
			</logic:equal>
			<logic:equal name="CONECTADO" value="true">
				Usuario: 
				<bean:define id="usuario" name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session" type="UsuarioInterno"/>
				<bean:write name="usuario" property="username"/>
				<br/><br/><br/>
			 	<a href="<%= request.getContextPath() %>/autenticacion/cerrarSession" target="_parent">Cerrar Sesi�n</a>

			</logic:equal>
		</form>




	</body>
</html>