package cl.cencosud.ventaseguros.fichasubcategoria.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class BuscarPlanesAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        FichaSubcategoriaDelegate delegate = new FichaSubcategoriaDelegate();
        int idSubcategoria = -1;
        try {

            String oIdSubcategoria = request.getParameter("idSubcategoria");

            if (oIdSubcategoria != null && !oIdSubcategoria.equals("")) {
                idSubcategoria = Integer.parseInt(oIdSubcategoria);
            }

            List < PlanLeg > lista = delegate.obtenerPlanes(idSubcategoria);
            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, lista);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;
    }
}
