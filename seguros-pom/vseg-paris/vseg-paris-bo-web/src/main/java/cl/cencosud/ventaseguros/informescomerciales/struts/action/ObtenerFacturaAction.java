package cl.cencosud.ventaseguros.informescomerciales.struts.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.InformesComercialesDelegate;

import com.tinet.exceptions.system.SystemException;

public class ObtenerFacturaAction extends Action {

    private static final String IMAGE_NOT_FOUND =
        "/cl/cencosud/ventaseguros/admin/struts/not.gif";

    private static final int BUFFER_SIZE = 4096;
    
    public static final Log logger =
        LogFactory.getLog(ObtenerFacturaAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        InformesComercialesDelegate delegate =
            new InformesComercialesDelegate();

        try {
            ServletOutputStream out = response.getOutputStream();

            if (request.getParameter("id_factura") == null) {
                String mensaje = "Falta id_factura!";
                out.write(mensaje.getBytes());

            } else {
                Long idFactura =
                    Long.valueOf(request.getParameter("id_factura"));
                Map < String, Object > archivo =
                    delegate.obtenerArchivoFactura(idFactura);

                if (archivo != null && archivo.get("archivo") != null) {
                    
                    response.setHeader("Pragma", "no-cache");
                    response.setHeader("cache-control",
                        "no-store, no-cache, must-revalidate");
                    response.setDateHeader("expires", -1);

                    response.setContentType("image/jpeg");
                    response.setHeader("Content-Disposition",
                        "inline;filename=imagen.pdf");

                    byte[] imagen = (byte[]) archivo.get("archivo");
                    out.write(imagen, 0, imagen.length);
                    

                } else {
                    response.setHeader("Pragma", "no-cache");
                    response.setHeader("cache-control",
                        "no-store, no-cache, must-revalidate");
                    response.setDateHeader("expires", -1);
                    
                    //Cargar imagen por defecto.
                    response.setContentType("image/gif");
                    response.setHeader("Content-Disposition",
                        "inline;filename=not.gif");

                    InputStream in =
                        this.getClass().getClassLoader().getResourceAsStream(
                            IMAGE_NOT_FOUND);

                    byte[] outputByte = new byte[BUFFER_SIZE];
                    while (in.read(outputByte, 0, BUFFER_SIZE) != -1) {
                        out.write(outputByte, 0, BUFFER_SIZE);
                    }
                    in.close();

                }
            }
            out.flush();
            out.close();

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;
    }

}
