package cl.cencosud.ventaseguros.asesorvirtual.struts.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Preguntas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.cencosud.ventaseguros.asesorvirtual.struts.form.ArbolPreguntaAgregarForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ArbolPreguntaGrabarAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        ArbolPreguntaAgregarForm oForm = (ArbolPreguntaAgregarForm) form;
        //AsesorVirtualDelegate delegate = new AsesorVirtualDelegate();

        //Obtener arbol de sesion.
        Sitio sitio = (Sitio) request.getSession().getAttribute("sitio");

        String actual = oForm.getActual();

        String tipo = actual.substring(0, 1);
        
        if ("A".equals(tipo)) {
            String[] arrActual = actual.replace("A", "").split("_");

            int seccionPos = Integer.valueOf(arrActual[0]);
            int preguntasPos = Integer.valueOf(arrActual[1]);
            //int alternativasPos = Integer.valueOf(actual[2]);
            int alternativaPos = Integer.valueOf(arrActual[3]);

            Pregunta pregunta = new Pregunta();
            pregunta.setTitulo(oForm.getPregunta());
            pregunta.setTipoPregunta(oForm.getTipo());

            //Agregar pregunta al arbol.
            Seccion seccion = sitio.getSecciones().get(seccionPos);
            List < Pregunta > lPreguntas =
                seccion.getPreguntas().getPreguntas();
            pregunta.setId(lPreguntas.size());

            int alternativas = (Integer) oForm.getAlternativas();

            List < Alternativa > lAlternativas =
                new ArrayList < Alternativa >();
            for (int i = 0; i < alternativas; i++) {
                Alternativa alternativa = new Alternativa();
                alternativa.setTexto0(oForm.getTexto0()[i]);
                alternativa.setTexto1(oForm.getTexto1()[i]);
                alternativa.setValor0(oForm.getValor0()[i]);
                alternativa.setValor1(oForm.getValor1()[i]);
                alternativa.setId(i);
                alternativa.setRespuesta(-1);
                //alternativa.setProximaPregunta(null);

                lAlternativas.add(alternativa);
            }
            pregunta.setAlternativas(lAlternativas);
            lPreguntas.add(pregunta);

            //Linkear alternativa con pregunta nueva.
            seccion.getPreguntas().getPreguntas().get(preguntasPos)
                .getAlternativas().get(alternativaPos).setProximaPregunta(
                    pregunta.getId());

        } else if ("S".equals(tipo)) {
            String[] arrActual = actual.replace("S", "").split("_");

            int seccionPos = Integer.valueOf(arrActual[0]);
//            int preguntasPos = Integer.valueOf(arrActual[1]);

            Pregunta pregunta = new Pregunta();
            pregunta.setTitulo(oForm.getPregunta());
            pregunta.setTipoPregunta(oForm.getTipo());

            //Agregar pregunta al arbol.
            Seccion seccion = sitio.getSecciones().get(seccionPos);
            List < Pregunta > lPreguntas =
                seccion.getPreguntas().getPreguntas();
            pregunta.setId(lPreguntas.size());

            int alternativas = (Integer) oForm.getAlternativas();

            List < Alternativa > lAlternativas =
                new ArrayList < Alternativa >();
            for (int i = 0; i < alternativas; i++) {
                Alternativa alternativa = new Alternativa();
                alternativa.setTexto0(oForm.getTexto0()[i]);
                alternativa.setTexto1(oForm.getTexto1()[i]);
                alternativa.setValor0(oForm.getValor0()[i]);
                alternativa.setValor1(oForm.getValor1()[i]);
                alternativa.setId(i);
                alternativa.setRespuesta(-1);
                //alternativa.setProximaPregunta(null);

                lAlternativas.add(alternativa);
            }
            pregunta.setAlternativas(lAlternativas);
            lPreguntas.add(pregunta);

        }
        //Actualzar datos
        actualizarCantidadPreguntas(sitio);

        request.setAttribute("pregunta", true);

        return mapping.findForward("continue");
    }

    /**
     * Recorre el objeto sitio para setear la cantidad correcta de preguntas.
     * @param sitio objeto a recorrer.
     */
    private void actualizarCantidadPreguntas(Sitio sitio) {
        for (int i = 0; i < sitio.getSecciones().size(); i++) {

            Preguntas preguntas = sitio.getSecciones().get(i).getPreguntas();

            preguntas.setCantidadpreguntas(preguntas.getPreguntas().size());
        }
    }
}
