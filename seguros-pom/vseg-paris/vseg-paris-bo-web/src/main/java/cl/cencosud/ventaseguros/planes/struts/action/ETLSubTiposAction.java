package cl.cencosud.ventaseguros.planes.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg;
import cl.cencosud.ventaseguros.delegate.MantenedorETLDelegate;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ETLSubTiposAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        try {
            String codigo_tipo_prod_leg = request.getParameter("tipo");

            if (codigo_tipo_prod_leg == null || "".equals(codigo_tipo_prod_leg)) {
                throw new SystemException(new Exception(
                    "Se debe denifir un tipo"));
            }

            MantenedorETLDelegate delegate = new MantenedorETLDelegate();
            SubtipoLeg[] subtipos =
                delegate.obtenerSubTipoLeg(codigo_tipo_prod_leg);

            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, subtipos);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }
        return null;
    }
}
