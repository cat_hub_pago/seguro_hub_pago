package cl.cencosud.ventaseguros.gestioneventos.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

public class ModificarEventosForm extends BuilderActionFormBaseVSP {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public InputStream getValidationRules(HttpServletRequest request) {
		return NuevoeventoForm.class
				.getResourceAsStream("resource/validation.xml");
	}

}
