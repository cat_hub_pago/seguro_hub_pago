package cl.cencosud.ventaseguros.informescomerciales.struts.form;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

public class InformeComercialTarjetasForm extends BuilderActionFormBaseVSP {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5072649001984648860L;
    
    private int[] paginas;
    
    private int rama;
    
    private int subcategoria;
    
    private int id_plan;
    
    private String compannia;
    
    private String hiddNombre;
    
    private String hiddLink;
    
    private String hiddTracker;
    
    private String hiddImagen;
    
    private String hiddNombreH;
    
    private String hiddLinkH;
    
    private String hiddTrackerH;
    
    private String hiddImagenH;
    
    private String hiddNombreS;
    
    private String hiddLinkS;
    
    private String hiddTrackerS;
    
    private String hiddImagenS;
    
private String hiddNombreV;
    
    private String hiddLinkV;
    
    private String hiddTrackerV;
    
    private String hiddImagenV;
    
    private String hiddNombreX;
    
    private String hiddLinkX;
    
    private String hiddTrackerX;
    
    private String hiddImagenX;
    
    private String hiddNombreX2;
    
    private String hiddLinkX2;
    
    private String hiddTrackerX2;
    
    private String hiddImagenX2;
    
    private String hiddNombreX3;
    
    private String hiddLinkX3;
    
    private String hiddTrackerX3;
    
    private String hiddImagenX3;
    
    private String hiddNombreX4;
    
    private String hiddLinkX4;
    
    private String hiddTrackerX4;
    
    private String hiddImagenX4;
    
    public String getHiddNombreX2() {
		return hiddNombreX2;
	}

	public void setHiddNombreX2(String hiddNombreX2) {
		this.hiddNombreX2 = hiddNombreX2;
	}

	public String getHiddLinkX2() {
		return hiddLinkX2;
	}

	public void setHiddLinkX2(String hiddLinkX2) {
		this.hiddLinkX2 = hiddLinkX2;
	}

	public String getHiddTrackerX2() {
		return hiddTrackerX2;
	}

	public void setHiddTrackerX2(String hiddTrackerX2) {
		this.hiddTrackerX2 = hiddTrackerX2;
	}

	public String getHiddImagenX2() {
		return hiddImagenX2;
	}

	public void setHiddImagenX2(String hiddImagenX2) {
		this.hiddImagenX2 = hiddImagenX2;
	}

	public String getHiddNombreX3() {
		return hiddNombreX3;
	}

	public void setHiddNombreX3(String hiddNombreX3) {
		this.hiddNombreX3 = hiddNombreX3;
	}

	public String getHiddLinkX3() {
		return hiddLinkX3;
	}

	public void setHiddLinkX3(String hiddLinkX3) {
		this.hiddLinkX3 = hiddLinkX3;
	}

	public String getHiddTrackerX3() {
		return hiddTrackerX3;
	}

	public void setHiddTrackerX3(String hiddTrackerX3) {
		this.hiddTrackerX3 = hiddTrackerX3;
	}

	public String getHiddImagenX3() {
		return hiddImagenX3;
	}

	public void setHiddImagenX3(String hiddImagenX3) {
		this.hiddImagenX3 = hiddImagenX3;
	}

	public String getHiddNombreX4() {
		return hiddNombreX4;
	}

	public void setHiddNombreX4(String hiddNombreX4) {
		this.hiddNombreX4 = hiddNombreX4;
	}

	public String getHiddLinkX4() {
		return hiddLinkX4;
	}

	public void setHiddLinkX4(String hiddLinkX4) {
		this.hiddLinkX4 = hiddLinkX4;
	}

	public String getHiddTrackerX4() {
		return hiddTrackerX4;
	}

	public void setHiddTrackerX4(String hiddTrackerX4) {
		this.hiddTrackerX4 = hiddTrackerX4;
	}

	public String getHiddImagenX4() {
		return hiddImagenX4;
	}

	public void setHiddImagenX4(String hiddImagenX4) {
		this.hiddImagenX4 = hiddImagenX4;
	}

	public String getNombre2() {
		return nombre2;
	}

	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}

	public String getLink2() {
		return link2;
	}

	public void setLink2(String link2) {
		this.link2 = link2;
	}

	public String getTracker2() {
		return tracker2;
	}

	public void setTracker2(String tracker2) {
		this.tracker2 = tracker2;
	}

	public String getImagen2() {
		return imagen2;
	}

	public void setImagen2(String imagen2) {
		this.imagen2 = imagen2;
	}

	public String getIdPlan2() {
		return idPlan2;
	}

	public void setIdPlan2(String idPlan2) {
		this.idPlan2 = idPlan2;
	}

	public String getNombre3() {
		return nombre3;
	}

	public void setNombre3(String nombre3) {
		this.nombre3 = nombre3;
	}

	public String getLink3() {
		return link3;
	}

	public void setLink3(String link3) {
		this.link3 = link3;
	}

	public String getTracker3() {
		return tracker3;
	}

	public void setTracker3(String tracker3) {
		this.tracker3 = tracker3;
	}

	public String getImagen3() {
		return imagen3;
	}

	public void setImagen3(String imagen3) {
		this.imagen3 = imagen3;
	}

	public String getIdPlan3() {
		return idPlan3;
	}

	public void setIdPlan3(String idPlan3) {
		this.idPlan3 = idPlan3;
	}

	public String getNombre4() {
		return nombre4;
	}

	public void setNombre4(String nombre4) {
		this.nombre4 = nombre4;
	}

	public String getLink4() {
		return link4;
	}

	public void setLink4(String link4) {
		this.link4 = link4;
	}

	public String getTracker4() {
		return tracker4;
	}

	public void setTracker4(String tracker4) {
		this.tracker4 = tracker4;
	}

	public String getImagen4() {
		return imagen4;
	}

	public void setImagen4(String imagen4) {
		this.imagen4 = imagen4;
	}

	public String getIdPlan4() {
		return idPlan4;
	}

	public void setIdPlan4(String idPlan4) {
		this.idPlan4 = idPlan4;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String hiddNombreHX;
    
    private String hiddLinkHX;
    
    private String hiddTrackerHX;
    
    private String hiddImagenHX;
    
    private String hiddNombreSX;
    
    private String hiddLinkSX;
    
    private String hiddTrackerSX;
    
    private String hiddImagenSX;
    
private String hiddNombreVX;
    
    private String hiddLinkVX;
    
    private String hiddTrackerVX;
    
    private String hiddImagenVX;
    
    private String hiddPrincipal;

    private String hiddPosicion;
    
    public String getHiddNombreH() {
		return hiddNombreH;
	}

	public void setHiddNombreH(String hiddNombreH) {
		this.hiddNombreH = hiddNombreH;
	}

	public String getHiddLinkH() {
		return hiddLinkH;
	}

	public void setHiddLinkH(String hiddLinkH) {
		this.hiddLinkH = hiddLinkH;
	}

	public String getHiddTrackerH() {
		return hiddTrackerH;
	}

	public void setHiddTrackerH(String hiddTrackerH) {
		this.hiddTrackerH = hiddTrackerH;
	}

	public String getHiddImagenH() {
		return hiddImagenH;
	}

	public void setHiddImagenH(String hiddImagenH) {
		this.hiddImagenH = hiddImagenH;
	}

	public String getHiddNombreS() {
		return hiddNombreS;
	}

	public void setHiddNombreS(String hiddNombreS) {
		this.hiddNombreS = hiddNombreS;
	}

	public String getHiddLinkS() {
		return hiddLinkS;
	}

	public void setHiddLinkS(String hiddLinkS) {
		this.hiddLinkS = hiddLinkS;
	}

	public String getHiddTrackerS() {
		return hiddTrackerS;
	}

	public void setHiddTrackerS(String hiddTrackerS) {
		this.hiddTrackerS = hiddTrackerS;
	}

	public String getHiddImagenS() {
		return hiddImagenS;
	}

	public void setHiddImagenS(String hiddImagenS) {
		this.hiddImagenS = hiddImagenS;
	}

	public String getHiddNombreV() {
		return hiddNombreV;
	}

	public void setHiddNombreV(String hiddNombreV) {
		this.hiddNombreV = hiddNombreV;
	}

	public String getHiddLinkV() {
		return hiddLinkV;
	}

	public void setHiddLinkV(String hiddLinkV) {
		this.hiddLinkV = hiddLinkV;
	}

	public String getHiddTrackerV() {
		return hiddTrackerV;
	}

	public void setHiddTrackerV(String hiddTrackerV) {
		this.hiddTrackerV = hiddTrackerV;
	}

	public String getHiddImagenV() {
		return hiddImagenV;
	}

	public void setHiddImagenV(String hiddImagenV) {
		this.hiddImagenV = hiddImagenV;
	}

	public String getHiddNombreX() {
		return hiddNombreX;
	}

	public void setHiddNombreX(String hiddNombreX) {
		this.hiddNombreX = hiddNombreX;
	}

	public String getHiddLinkX() {
		return hiddLinkX;
	}

	public void setHiddLinkX(String hiddLinkX) {
		this.hiddLinkX = hiddLinkX;
	}

	public String getHiddTrackerX() {
		return hiddTrackerX;
	}

	public void setHiddTrackerX(String hiddTrackerX) {
		this.hiddTrackerX = hiddTrackerX;
	}

	public String getHiddImagenX() {
		return hiddImagenX;
	}

	public void setHiddImagenX(String hiddImagenX) {
		this.hiddImagenX = hiddImagenX;
	}

	public String getHiddNombreHX() {
		return hiddNombreHX;
	}

	public void setHiddNombreHX(String hiddNombreHX) {
		this.hiddNombreHX = hiddNombreHX;
	}

	public String getHiddLinkHX() {
		return hiddLinkHX;
	}

	public void setHiddLinkHX(String hiddLinkHX) {
		this.hiddLinkHX = hiddLinkHX;
	}

	public String getHiddTrackerHX() {
		return hiddTrackerHX;
	}

	public void setHiddTrackerHX(String hiddTrackerHX) {
		this.hiddTrackerHX = hiddTrackerHX;
	}

	public String getHiddImagenHX() {
		return hiddImagenHX;
	}

	public void setHiddImagenHX(String hiddImagenHX) {
		this.hiddImagenHX = hiddImagenHX;
	}

	public String getHiddNombreSX() {
		return hiddNombreSX;
	}

	public void setHiddNombreSX(String hiddNombreSX) {
		this.hiddNombreSX = hiddNombreSX;
	}

	public String getHiddLinkSX() {
		return hiddLinkSX;
	}

	public void setHiddLinkSX(String hiddLinkSX) {
		this.hiddLinkSX = hiddLinkSX;
	}

	public String getHiddTrackerSX() {
		return hiddTrackerSX;
	}

	public void setHiddTrackerSX(String hiddTrackerSX) {
		this.hiddTrackerSX = hiddTrackerSX;
	}

	public String getHiddImagenSX() {
		return hiddImagenSX;
	}

	public void setHiddImagenSX(String hiddImagenSX) {
		this.hiddImagenSX = hiddImagenSX;
	}

	public String getHiddNombreVX() {
		return hiddNombreVX;
	}

	public void setHiddNombreVX(String hiddNombreVX) {
		this.hiddNombreVX = hiddNombreVX;
	}

	public String getHiddLinkVX() {
		return hiddLinkVX;
	}

	public void setHiddLinkVX(String hiddLinkVX) {
		this.hiddLinkVX = hiddLinkVX;
	}

	public String getHiddTrackerVX() {
		return hiddTrackerVX;
	}

	public void setHiddTrackerVX(String hiddTrackerVX) {
		this.hiddTrackerVX = hiddTrackerVX;
	}

	public String getHiddImagenVX() {
		return hiddImagenVX;
	}

	public void setHiddImagenVX(String hiddImagenVX) {
		this.hiddImagenVX = hiddImagenVX;
	}

	private String hiddNombreVB1;
    
    private String hiddLinkVB1;
    
    private String hiddTrackerVB1;
    
    private String hiddImagenVB1;
    
    private String hiddPrincipalVB1;
    
    private String hiddOrdinalVB1;
    
    private String hiddNombreVB2;
    
    private String hiddLinkVB2;
    
    private String hiddTrackerVB2;
    
    private String hiddImagenVB2;
    
    private String hiddPrincipalVB2;
    
    private String hiddOrdinalVB2;
    
    private String hiddNombreVB3;
    
    private String hiddLinkVB3;
    
    private String hiddTrackerVB3;
    
    private String hiddImagenVB3;
    
    private String hiddPrincipalVB3;
    
    private String hiddOrdinalVB3;
    
    private String hiddNombreVB4;
    
    private String hiddLinkVB4;
    
    private String hiddTrackerVB4;
    
    private String hiddImagenVB4;
    
    private String hiddPrincipalVB4;
    
    private String hiddOrdinalVB4;
    
    private String hiddNombreVB5;
    
    private String hiddLinkVB5;
    
    private String hiddTrackerVB5;
    
    private String hiddImagenVB5;
    
    private String hiddPrincipalVB5;
    
    private String hiddOrdinalVB5;
    
    private String hiddNombreVB6;
    
    private String hiddLinkVB6;
    
    private String hiddTrackerVB6;
    
    private String hiddImagenVB6;
    
    private String hiddPrincipalVB6;
    
    private String hiddOrdinalVB6;
    
    private String hiddNombreHB6;
    
    private String hiddLinkHB6;
    
    private String hiddTrackerHB6;
    
    private String hiddImagenHB6;
    
    private String hiddPrincipalHB6;
    
    private String hiddOrdinalHB6;
    
    private String hiddNombreHB1;
    
    private String hiddLinkHB1;
    
    private String hiddTrackerHB1;
    
    private String hiddImagenHB1;
    
    private String hiddPrincipalHB1;
    
    private String hiddOrdinalHB1;
    
    private String hiddNombreHB2;
    
    private String hiddLinkHB2;
    
    private String hiddTrackerHB2;
    
    private String hiddImagenHB2;
    
    private String hiddPrincipalHB2;
    
    private String hiddOrdinalHB2;
    
    private String hiddNombreHB3;
    
    private String hiddLinkHB3;
    
    private String hiddTrackerHB3;
    
    private String hiddImagenHB3;
    
    private String hiddPrincipalHB3;
    
    private String hiddOrdinalHB3;
    
    private String hiddNombreHB4;
    
    private String hiddLinkHB4;
    
    private String hiddTrackerHB4;
    
    private String hiddImagenHB4;
    
    private String hiddPrincipalHB4;
    
    private String hiddOrdinalHB4;
    
    private String hiddNombreHB5;
    
    private String hiddLinkHB5;
    
    private String hiddTrackerHB5;
    
    private String hiddImagenHB5;
    
    private String hiddPrincipalHB5;
    
    private String hiddOrdinalHB5;
    
    private String hiddNombreVIB1;
    
    private String hiddLinkVIB1;
    
    private String hiddTrackerVIB1;
    
    private String hiddImagenVIB1;
    
    private String hiddPrincipalVIB1;
    
    private String hiddOrdinalVIB1;
    
    private String hiddNombreVIB2;
    
    private String hiddLinkVIB2;
    
    private String hiddTrackerVIB2;
    
    private String hiddImagenVIB2;
    
    private String hiddPrincipalVIB2;
    
    private String hiddOrdinalVIB2;
    
    private String hiddNombreVIB3;
    
    private String hiddLinkVIB3;
    
    private String hiddTrackerVIB3;
    
    private String hiddImagenVIB3;
    
    private String hiddPrincipalVIB3;
    
    private String hiddOrdinalVIB3;
    
    private String hiddNombreVIB4;
    
    private String hiddLinkVIB4;
    
    private String hiddTrackerVIB4;
    
    private String hiddImagenVIB4;
    
    private String hiddPrincipalVIB4;
    
    private String hiddOrdinalVIB4;
    
    private String hiddNombreVIB5;
    
    private String hiddLinkVIB5;
    
    private String hiddTrackerVIB5;
    
    private String hiddImagenVIB5;
    
    private String hiddPrincipalVIB5;
    
    private String hiddOrdinalVIB5;
    
    private String hiddNombreVIB6;
    
    private String hiddLinkVIB6;
    
    private String hiddTrackerVIB6;
    
    private String hiddImagenVIB6;
    
    private String hiddPrincipalVIB6;
    
    private String hiddOrdinalVIB6;
    
    private String hiddNombreSB1;
    
    private String hiddLinkSB1;
    
    private String hiddTrackerSB1;
    
    private String hiddImagenSB1;
    
    private String hiddPrincipalSB1;
    
    private String hiddOrdinalSB1;
    
    private String hiddNombreSB2;
    
    private String hiddLinkSB2;
    
    private String hiddTrackerSB2;
    
    private String hiddImagenSB2;
    
    private String hiddPrincipalSB2;
    
    private String hiddOrdinalSB2;
    
    private String hiddNombreSB3;
    
    private String hiddLinkSB3;
    
    private String hiddTrackerSB3;
    
    private String hiddImagenSB3;
    
    private String hiddPrincipalSB3;
    
    private String hiddOrdinalSB3;
    
    private String hiddNombreSB4;
    
    private String hiddLinkSB4;
    
    private String hiddTrackerSB4;
    
    private String hiddImagenSB4;
    
    private String hiddPrincipalSB4;
    
    private String hiddOrdinalSB4;
    
    private String hiddNombreSB5;
    
    private String hiddLinkSB5;
    
    private String hiddTrackerSB5;
    
    private String hiddImagenSB5;
    
    private String hiddPrincipalSB5;
    
    private String hiddOrdinalSB5;
    
    private String hiddNombreSB6;
    
    private String hiddLinkSB6;
    
    private String hiddTrackerSB6;
    
    private String hiddImagenSB6;
    
    private String hiddPrincipalSB6;
    
    private String hiddOrdinalSB6;
    
    private String hiddSubmit;
    
    private String nombre;
    
    private String link;
    
    private String tracker;
    
    private String imagen;
    
    private String idPlan;
    
    private String nombre2;
    
    private String link2;
    
    private String tracker2;
    
    private String imagen2;
    
    private String idPlan2;
    
    private String nombre3;
    
    private String link3;
    
    private String tracker3;
    
    private String imagen3;
    
    private String idPlan3;
    
    private String nombre4;
    
    private String link4;
    
    private String tracker4;
    
    private String imagen4;
    
    private String idPlan4;
    
 private String hiddNombreVB1X;
    
    private String hiddLinkVB1X;
    
    private String hiddTrackerVB1X;
    
    private String hiddImagenVB1X;
    
    private String hiddPrincipalVB1X;
    
    private String hiddOrdinalVB1X;
    
    private String hiddNombreVB2X;
    
    public String getHiddLinkVB2X() {
		return hiddLinkVB2X;
	}

	public void setHiddLinkVB2X(String hiddLinkVB2X) {
		this.hiddLinkVB2X = hiddLinkVB2X;
	}

	public String getHiddTrackerVB2X() {
		return hiddTrackerVB2X;
	}

	public void setHiddTrackerVB2X(String hiddTrackerVB2X) {
		this.hiddTrackerVB2X = hiddTrackerVB2X;
	}

	public String getHiddImagenVB2X() {
		return hiddImagenVB2X;
	}

	public void setHiddImagenVB2X(String hiddImagenVB2X) {
		this.hiddImagenVB2X = hiddImagenVB2X;
	}

	public String getHiddPrincipalVB2X() {
		return hiddPrincipalVB2X;
	}

	public void setHiddPrincipalVB2X(String hiddPrincipalVB2X) {
		this.hiddPrincipalVB2X = hiddPrincipalVB2X;
	}

	public String getHiddOrdinalVB2X() {
		return hiddOrdinalVB2X;
	}

	public void setHiddOrdinalVB2X(String hiddOrdinalVB2X) {
		this.hiddOrdinalVB2X = hiddOrdinalVB2X;
	}

	public String getHiddNombreVB3X() {
		return hiddNombreVB3X;
	}

	public void setHiddNombreVB3X(String hiddNombreVB3X) {
		this.hiddNombreVB3X = hiddNombreVB3X;
	}

	public String getHiddLinkVB3X() {
		return hiddLinkVB3X;
	}

	public void setHiddLinkVB3X(String hiddLinkVB3X) {
		this.hiddLinkVB3X = hiddLinkVB3X;
	}

	public String getHiddTrackerVB3X() {
		return hiddTrackerVB3X;
	}

	public void setHiddTrackerVB3X(String hiddTrackerVB3X) {
		this.hiddTrackerVB3X = hiddTrackerVB3X;
	}

	public String getHiddImagenVB3X() {
		return hiddImagenVB3X;
	}

	public void setHiddImagenVB3X(String hiddImagenVB3X) {
		this.hiddImagenVB3X = hiddImagenVB3X;
	}

	public String getHiddPrincipalVB3X() {
		return hiddPrincipalVB3X;
	}

	public void setHiddPrincipalVB3X(String hiddPrincipalVB3X) {
		this.hiddPrincipalVB3X = hiddPrincipalVB3X;
	}

	public String getHiddOrdinalVB3X() {
		return hiddOrdinalVB3X;
	}

	public void setHiddOrdinalVB3X(String hiddOrdinalVB3X) {
		this.hiddOrdinalVB3X = hiddOrdinalVB3X;
	}

	public String getHiddNombreVB4X() {
		return hiddNombreVB4X;
	}

	public void setHiddNombreVB4X(String hiddNombreVB4X) {
		this.hiddNombreVB4X = hiddNombreVB4X;
	}

	public String getHiddLinkVB4X() {
		return hiddLinkVB4X;
	}

	public void setHiddLinkVB4X(String hiddLinkVB4X) {
		this.hiddLinkVB4X = hiddLinkVB4X;
	}

	public String getHiddTrackerVB4X() {
		return hiddTrackerVB4X;
	}

	public void setHiddTrackerVB4X(String hiddTrackerVB4X) {
		this.hiddTrackerVB4X = hiddTrackerVB4X;
	}

	public String getHiddImagenVB4X() {
		return hiddImagenVB4X;
	}

	public void setHiddImagenVB4X(String hiddImagenVB4X) {
		this.hiddImagenVB4X = hiddImagenVB4X;
	}

	public String getHiddPrincipalVB4X() {
		return hiddPrincipalVB4X;
	}

	public void setHiddPrincipalVB4X(String hiddPrincipalVB4X) {
		this.hiddPrincipalVB4X = hiddPrincipalVB4X;
	}

	public String getHiddOrdinalVB4X() {
		return hiddOrdinalVB4X;
	}

	public void setHiddOrdinalVB4X(String hiddOrdinalVB4X) {
		this.hiddOrdinalVB4X = hiddOrdinalVB4X;
	}

	public String getHiddNombreVB5X() {
		return hiddNombreVB5X;
	}

	public void setHiddNombreVB5X(String hiddNombreVB5X) {
		this.hiddNombreVB5X = hiddNombreVB5X;
	}

	public String getHiddLinkVB5X() {
		return hiddLinkVB5X;
	}

	public void setHiddLinkVB5X(String hiddLinkVB5X) {
		this.hiddLinkVB5X = hiddLinkVB5X;
	}

	public String getHiddTrackerVB5X() {
		return hiddTrackerVB5X;
	}

	public void setHiddTrackerVB5X(String hiddTrackerVB5X) {
		this.hiddTrackerVB5X = hiddTrackerVB5X;
	}

	public String getHiddImagenVB5X() {
		return hiddImagenVB5X;
	}

	public void setHiddImagenVB5X(String hiddImagenVB5X) {
		this.hiddImagenVB5X = hiddImagenVB5X;
	}

	public String getHiddPrincipalVB5X() {
		return hiddPrincipalVB5X;
	}

	public void setHiddPrincipalVB5X(String hiddPrincipalVB5X) {
		this.hiddPrincipalVB5X = hiddPrincipalVB5X;
	}

	public String getHiddOrdinalVB5X() {
		return hiddOrdinalVB5X;
	}

	public void setHiddOrdinalVB5X(String hiddOrdinalVB5X) {
		this.hiddOrdinalVB5X = hiddOrdinalVB5X;
	}

	public String getHiddNombreVB6X() {
		return hiddNombreVB6X;
	}

	public void setHiddNombreVB6X(String hiddNombreVB6X) {
		this.hiddNombreVB6X = hiddNombreVB6X;
	}

	public String getHiddLinkVB6X() {
		return hiddLinkVB6X;
	}

	public void setHiddLinkVB6X(String hiddLinkVB6X) {
		this.hiddLinkVB6X = hiddLinkVB6X;
	}

	public String getHiddTrackerVB6X() {
		return hiddTrackerVB6X;
	}

	public void setHiddTrackerVB6X(String hiddTrackerVB6X) {
		this.hiddTrackerVB6X = hiddTrackerVB6X;
	}

	public String getHiddImagenVB6X() {
		return hiddImagenVB6X;
	}

	public void setHiddImagenVB6X(String hiddImagenVB6X) {
		this.hiddImagenVB6X = hiddImagenVB6X;
	}

	public String getHiddPrincipalVB6X() {
		return hiddPrincipalVB6X;
	}

	public void setHiddPrincipalVB6X(String hiddPrincipalVB6X) {
		this.hiddPrincipalVB6X = hiddPrincipalVB6X;
	}

	public String getHiddOrdinalVB6X() {
		return hiddOrdinalVB6X;
	}

	public void setHiddOrdinalVB6X(String hiddOrdinalVB6X) {
		this.hiddOrdinalVB6X = hiddOrdinalVB6X;
	}

	public String getHiddNombreHB6X() {
		return hiddNombreHB6X;
	}

	public void setHiddNombreHB6X(String hiddNombreHB6X) {
		this.hiddNombreHB6X = hiddNombreHB6X;
	}

	public String getHiddLinkHB6X() {
		return hiddLinkHB6X;
	}

	public void setHiddLinkHB6X(String hiddLinkHB6X) {
		this.hiddLinkHB6X = hiddLinkHB6X;
	}

	public String getHiddTrackerHB6X() {
		return hiddTrackerHB6X;
	}

	public void setHiddTrackerHB6X(String hiddTrackerHB6X) {
		this.hiddTrackerHB6X = hiddTrackerHB6X;
	}

	public String getHiddImagenHB6X() {
		return hiddImagenHB6X;
	}

	public void setHiddImagenHB6X(String hiddImagenHB6X) {
		this.hiddImagenHB6X = hiddImagenHB6X;
	}

	public String getHiddPrincipalHB6X() {
		return hiddPrincipalHB6X;
	}

	public void setHiddPrincipalHB6X(String hiddPrincipalHB6X) {
		this.hiddPrincipalHB6X = hiddPrincipalHB6X;
	}

	public String getHiddOrdinalHB6X() {
		return hiddOrdinalHB6X;
	}

	public void setHiddOrdinalHB6X(String hiddOrdinalHB6X) {
		this.hiddOrdinalHB6X = hiddOrdinalHB6X;
	}

	public String getHiddNombreHB1X() {
		return hiddNombreHB1X;
	}

	public void setHiddNombreHB1X(String hiddNombreHB1X) {
		this.hiddNombreHB1X = hiddNombreHB1X;
	}

	public String getHiddLinkHB1X() {
		return hiddLinkHB1X;
	}

	public void setHiddLinkHB1X(String hiddLinkHB1X) {
		this.hiddLinkHB1X = hiddLinkHB1X;
	}

	public String getHiddTrackerHB1X() {
		return hiddTrackerHB1X;
	}

	public void setHiddTrackerHB1X(String hiddTrackerHB1X) {
		this.hiddTrackerHB1X = hiddTrackerHB1X;
	}

	public String getHiddImagenHB1X() {
		return hiddImagenHB1X;
	}

	public void setHiddImagenHB1X(String hiddImagenHB1X) {
		this.hiddImagenHB1X = hiddImagenHB1X;
	}

	public String getHiddPrincipalHB1X() {
		return hiddPrincipalHB1X;
	}

	public void setHiddPrincipalHB1X(String hiddPrincipalHB1X) {
		this.hiddPrincipalHB1X = hiddPrincipalHB1X;
	}

	public String getHiddOrdinalHB1X() {
		return hiddOrdinalHB1X;
	}

	public void setHiddOrdinalHB1X(String hiddOrdinalHB1X) {
		this.hiddOrdinalHB1X = hiddOrdinalHB1X;
	}

	public String getHiddNombreHB2X() {
		return hiddNombreHB2X;
	}

	public void setHiddNombreHB2X(String hiddNombreHB2X) {
		this.hiddNombreHB2X = hiddNombreHB2X;
	}

	public String getHiddLinkHB2X() {
		return hiddLinkHB2X;
	}

	public void setHiddLinkHB2X(String hiddLinkHB2X) {
		this.hiddLinkHB2X = hiddLinkHB2X;
	}

	public String getHiddTrackerHB2X() {
		return hiddTrackerHB2X;
	}

	public void setHiddTrackerHB2X(String hiddTrackerHB2X) {
		this.hiddTrackerHB2X = hiddTrackerHB2X;
	}

	public String getHiddImagenHB2X() {
		return hiddImagenHB2X;
	}

	public void setHiddImagenHB2X(String hiddImagenHB2X) {
		this.hiddImagenHB2X = hiddImagenHB2X;
	}

	public String getHiddPrincipalHB2X() {
		return hiddPrincipalHB2X;
	}

	public void setHiddPrincipalHB2X(String hiddPrincipalHB2X) {
		this.hiddPrincipalHB2X = hiddPrincipalHB2X;
	}

	public String getHiddOrdinalHB2X() {
		return hiddOrdinalHB2X;
	}

	public void setHiddOrdinalHB2X(String hiddOrdinalHB2X) {
		this.hiddOrdinalHB2X = hiddOrdinalHB2X;
	}

	public String getHiddNombreHB3X() {
		return hiddNombreHB3X;
	}

	public void setHiddNombreHB3X(String hiddNombreHB3X) {
		this.hiddNombreHB3X = hiddNombreHB3X;
	}

	public String getHiddLinkHB3X() {
		return hiddLinkHB3X;
	}

	public void setHiddLinkHB3X(String hiddLinkHB3X) {
		this.hiddLinkHB3X = hiddLinkHB3X;
	}

	public String getHiddTrackerHB3X() {
		return hiddTrackerHB3X;
	}

	public void setHiddTrackerHB3X(String hiddTrackerHB3X) {
		this.hiddTrackerHB3X = hiddTrackerHB3X;
	}

	public String getHiddImagenHB3X() {
		return hiddImagenHB3X;
	}

	public void setHiddImagenHB3X(String hiddImagenHB3X) {
		this.hiddImagenHB3X = hiddImagenHB3X;
	}

	public String getHiddPrincipalHB3X() {
		return hiddPrincipalHB3X;
	}

	public void setHiddPrincipalHB3X(String hiddPrincipalHB3X) {
		this.hiddPrincipalHB3X = hiddPrincipalHB3X;
	}

	public String getHiddOrdinalHB3X() {
		return hiddOrdinalHB3X;
	}

	public void setHiddOrdinalHB3X(String hiddOrdinalHB3X) {
		this.hiddOrdinalHB3X = hiddOrdinalHB3X;
	}

	public String getHiddNombreHB4X() {
		return hiddNombreHB4X;
	}

	public void setHiddNombreHB4X(String hiddNombreHB4X) {
		this.hiddNombreHB4X = hiddNombreHB4X;
	}

	public String getHiddLinkHB4X() {
		return hiddLinkHB4X;
	}

	public void setHiddLinkHB4X(String hiddLinkHB4X) {
		this.hiddLinkHB4X = hiddLinkHB4X;
	}

	public String getHiddTrackerHB4X() {
		return hiddTrackerHB4X;
	}

	public void setHiddTrackerHB4X(String hiddTrackerHB4X) {
		this.hiddTrackerHB4X = hiddTrackerHB4X;
	}

	public String getHiddImagenHB4X() {
		return hiddImagenHB4X;
	}

	public void setHiddImagenHB4X(String hiddImagenHB4X) {
		this.hiddImagenHB4X = hiddImagenHB4X;
	}

	public String getHiddPrincipalHB4X() {
		return hiddPrincipalHB4X;
	}

	public void setHiddPrincipalHB4X(String hiddPrincipalHB4X) {
		this.hiddPrincipalHB4X = hiddPrincipalHB4X;
	}

	public String getHiddOrdinalHB4X() {
		return hiddOrdinalHB4X;
	}

	public void setHiddOrdinalHB4X(String hiddOrdinalHB4X) {
		this.hiddOrdinalHB4X = hiddOrdinalHB4X;
	}

	public String getHiddNombreHB5X() {
		return hiddNombreHB5X;
	}

	public void setHiddNombreHB5X(String hiddNombreHB5X) {
		this.hiddNombreHB5X = hiddNombreHB5X;
	}

	public String getHiddLinkHB5X() {
		return hiddLinkHB5X;
	}

	public void setHiddLinkHB5X(String hiddLinkHB5X) {
		this.hiddLinkHB5X = hiddLinkHB5X;
	}

	public String getHiddTrackerHB5X() {
		return hiddTrackerHB5X;
	}

	public void setHiddTrackerHB5X(String hiddTrackerHB5X) {
		this.hiddTrackerHB5X = hiddTrackerHB5X;
	}

	public String getHiddImagenHB5X() {
		return hiddImagenHB5X;
	}

	public void setHiddImagenHB5X(String hiddImagenHB5X) {
		this.hiddImagenHB5X = hiddImagenHB5X;
	}

	public String getHiddPrincipalHB5X() {
		return hiddPrincipalHB5X;
	}

	public void setHiddPrincipalHB5X(String hiddPrincipalHB5X) {
		this.hiddPrincipalHB5X = hiddPrincipalHB5X;
	}

	public String getHiddOrdinalHB5X() {
		return hiddOrdinalHB5X;
	}

	public void setHiddOrdinalHB5X(String hiddOrdinalHB5X) {
		this.hiddOrdinalHB5X = hiddOrdinalHB5X;
	}

	public String getHiddNombreVIB1X() {
		return hiddNombreVIB1X;
	}

	public void setHiddNombreVIB1X(String hiddNombreVIB1X) {
		this.hiddNombreVIB1X = hiddNombreVIB1X;
	}

	public String getHiddLinkVIB1X() {
		return hiddLinkVIB1X;
	}

	public void setHiddLinkVIB1X(String hiddLinkVIB1X) {
		this.hiddLinkVIB1X = hiddLinkVIB1X;
	}

	public String getHiddTrackerVIB1X() {
		return hiddTrackerVIB1X;
	}

	public void setHiddTrackerVIB1X(String hiddTrackerVIB1X) {
		this.hiddTrackerVIB1X = hiddTrackerVIB1X;
	}

	public String getHiddImagenVIB1X() {
		return hiddImagenVIB1X;
	}

	public void setHiddImagenVIB1X(String hiddImagenVIB1X) {
		this.hiddImagenVIB1X = hiddImagenVIB1X;
	}

	public String getHiddPrincipalVIB1X() {
		return hiddPrincipalVIB1X;
	}

	public void setHiddPrincipalVIB1X(String hiddPrincipalVIB1X) {
		this.hiddPrincipalVIB1X = hiddPrincipalVIB1X;
	}

	public String getHiddOrdinalVIB1X() {
		return hiddOrdinalVIB1X;
	}

	public void setHiddOrdinalVIB1X(String hiddOrdinalVIB1X) {
		this.hiddOrdinalVIB1X = hiddOrdinalVIB1X;
	}

	public String getHiddNombreVIB2X() {
		return hiddNombreVIB2X;
	}

	public void setHiddNombreVIB2X(String hiddNombreVIB2X) {
		this.hiddNombreVIB2X = hiddNombreVIB2X;
	}

	public String getHiddLinkVIB2X() {
		return hiddLinkVIB2X;
	}

	public void setHiddLinkVIB2X(String hiddLinkVIB2X) {
		this.hiddLinkVIB2X = hiddLinkVIB2X;
	}

	public String getHiddTrackerVIB2X() {
		return hiddTrackerVIB2X;
	}

	public void setHiddTrackerVIB2X(String hiddTrackerVIB2X) {
		this.hiddTrackerVIB2X = hiddTrackerVIB2X;
	}

	public String getHiddImagenVIB2X() {
		return hiddImagenVIB2X;
	}

	public void setHiddImagenVIB2X(String hiddImagenVIB2X) {
		this.hiddImagenVIB2X = hiddImagenVIB2X;
	}

	public String getHiddPrincipalVIB2X() {
		return hiddPrincipalVIB2X;
	}

	public void setHiddPrincipalVIB2X(String hiddPrincipalVIB2X) {
		this.hiddPrincipalVIB2X = hiddPrincipalVIB2X;
	}

	public String getHiddOrdinalVIB2X() {
		return hiddOrdinalVIB2X;
	}

	public void setHiddOrdinalVIB2X(String hiddOrdinalVIB2X) {
		this.hiddOrdinalVIB2X = hiddOrdinalVIB2X;
	}

	public String getHiddNombreVIB3X() {
		return hiddNombreVIB3X;
	}

	public void setHiddNombreVIB3X(String hiddNombreVIB3X) {
		this.hiddNombreVIB3X = hiddNombreVIB3X;
	}

	public String getHiddLinkVIB3X() {
		return hiddLinkVIB3X;
	}

	public void setHiddLinkVIB3X(String hiddLinkVIB3X) {
		this.hiddLinkVIB3X = hiddLinkVIB3X;
	}

	public String getHiddTrackerVIB3X() {
		return hiddTrackerVIB3X;
	}

	public void setHiddTrackerVIB3X(String hiddTrackerVIB3X) {
		this.hiddTrackerVIB3X = hiddTrackerVIB3X;
	}

	public String getHiddImagenVIB3X() {
		return hiddImagenVIB3X;
	}

	public void setHiddImagenVIB3X(String hiddImagenVIB3X) {
		this.hiddImagenVIB3X = hiddImagenVIB3X;
	}

	public String getHiddPrincipalVIB3X() {
		return hiddPrincipalVIB3X;
	}

	public void setHiddPrincipalVIB3X(String hiddPrincipalVIB3X) {
		this.hiddPrincipalVIB3X = hiddPrincipalVIB3X;
	}

	public String getHiddOrdinalVIB3X() {
		return hiddOrdinalVIB3X;
	}

	public void setHiddOrdinalVIB3X(String hiddOrdinalVIB3X) {
		this.hiddOrdinalVIB3X = hiddOrdinalVIB3X;
	}

	public String getHiddNombreVIB4X() {
		return hiddNombreVIB4X;
	}

	public void setHiddNombreVIB4X(String hiddNombreVIB4X) {
		this.hiddNombreVIB4X = hiddNombreVIB4X;
	}

	public String getHiddLinkVIB4X() {
		return hiddLinkVIB4X;
	}

	public void setHiddLinkVIB4X(String hiddLinkVIB4X) {
		this.hiddLinkVIB4X = hiddLinkVIB4X;
	}

	public String getHiddTrackerVIB4X() {
		return hiddTrackerVIB4X;
	}

	public void setHiddTrackerVIB4X(String hiddTrackerVIB4X) {
		this.hiddTrackerVIB4X = hiddTrackerVIB4X;
	}

	public String getHiddImagenVIB4X() {
		return hiddImagenVIB4X;
	}

	public void setHiddImagenVIB4X(String hiddImagenVIB4X) {
		this.hiddImagenVIB4X = hiddImagenVIB4X;
	}

	public String getHiddPrincipalVIB4X() {
		return hiddPrincipalVIB4X;
	}

	public void setHiddPrincipalVIB4X(String hiddPrincipalVIB4X) {
		this.hiddPrincipalVIB4X = hiddPrincipalVIB4X;
	}

	public String getHiddOrdinalVIB4X() {
		return hiddOrdinalVIB4X;
	}

	public void setHiddOrdinalVIB4X(String hiddOrdinalVIB4X) {
		this.hiddOrdinalVIB4X = hiddOrdinalVIB4X;
	}

	public String getHiddNombreVIB5X() {
		return hiddNombreVIB5X;
	}

	public void setHiddNombreVIB5X(String hiddNombreVIB5X) {
		this.hiddNombreVIB5X = hiddNombreVIB5X;
	}

	public String getHiddLinkVIB5X() {
		return hiddLinkVIB5X;
	}

	public void setHiddLinkVIB5X(String hiddLinkVIB5X) {
		this.hiddLinkVIB5X = hiddLinkVIB5X;
	}

	public String getHiddTrackerVIB5X() {
		return hiddTrackerVIB5X;
	}

	public void setHiddTrackerVIB5X(String hiddTrackerVIB5X) {
		this.hiddTrackerVIB5X = hiddTrackerVIB5X;
	}

	public String getHiddImagenVIB5X() {
		return hiddImagenVIB5X;
	}

	public void setHiddImagenVIB5X(String hiddImagenVIB5X) {
		this.hiddImagenVIB5X = hiddImagenVIB5X;
	}

	public String getHiddPrincipalVIB5X() {
		return hiddPrincipalVIB5X;
	}

	public void setHiddPrincipalVIB5X(String hiddPrincipalVIB5X) {
		this.hiddPrincipalVIB5X = hiddPrincipalVIB5X;
	}

	public String getHiddOrdinalVIB5X() {
		return hiddOrdinalVIB5X;
	}

	public void setHiddOrdinalVIB5X(String hiddOrdinalVIB5X) {
		this.hiddOrdinalVIB5X = hiddOrdinalVIB5X;
	}

	public String getHiddNombreVIB6X() {
		return hiddNombreVIB6X;
	}

	public void setHiddNombreVIB6X(String hiddNombreVIB6X) {
		this.hiddNombreVIB6X = hiddNombreVIB6X;
	}

	public String getHiddLinkVIB6X() {
		return hiddLinkVIB6X;
	}

	public void setHiddLinkVIB6X(String hiddLinkVIB6X) {
		this.hiddLinkVIB6X = hiddLinkVIB6X;
	}

	public String getHiddTrackerVIB6X() {
		return hiddTrackerVIB6X;
	}

	public void setHiddTrackerVIB6X(String hiddTrackerVIB6X) {
		this.hiddTrackerVIB6X = hiddTrackerVIB6X;
	}

	public String getHiddImagenVIB6X() {
		return hiddImagenVIB6X;
	}

	public void setHiddImagenVIB6X(String hiddImagenVIB6X) {
		this.hiddImagenVIB6X = hiddImagenVIB6X;
	}

	public String getHiddPrincipalVIB6X() {
		return hiddPrincipalVIB6X;
	}

	public void setHiddPrincipalVIB6X(String hiddPrincipalVIB6X) {
		this.hiddPrincipalVIB6X = hiddPrincipalVIB6X;
	}

	public String getHiddOrdinalVIB6X() {
		return hiddOrdinalVIB6X;
	}

	public void setHiddOrdinalVIB6X(String hiddOrdinalVIB6X) {
		this.hiddOrdinalVIB6X = hiddOrdinalVIB6X;
	}

	public String getHiddNombreSB1X() {
		return hiddNombreSB1X;
	}

	public void setHiddNombreSB1X(String hiddNombreSB1X) {
		this.hiddNombreSB1X = hiddNombreSB1X;
	}

	public String getHiddLinkSB1X() {
		return hiddLinkSB1X;
	}

	public void setHiddLinkSB1X(String hiddLinkSB1X) {
		this.hiddLinkSB1X = hiddLinkSB1X;
	}

	public String getHiddTrackerSB1X() {
		return hiddTrackerSB1X;
	}

	public void setHiddTrackerSB1X(String hiddTrackerSB1X) {
		this.hiddTrackerSB1X = hiddTrackerSB1X;
	}

	public String getHiddImagenSB1X() {
		return hiddImagenSB1X;
	}

	public void setHiddImagenSB1X(String hiddImagenSB1X) {
		this.hiddImagenSB1X = hiddImagenSB1X;
	}

	public String getHiddPrincipalSB1X() {
		return hiddPrincipalSB1X;
	}

	public void setHiddPrincipalSB1X(String hiddPrincipalSB1X) {
		this.hiddPrincipalSB1X = hiddPrincipalSB1X;
	}

	public String getHiddOrdinalSB1X() {
		return hiddOrdinalSB1X;
	}

	public void setHiddOrdinalSB1X(String hiddOrdinalSB1X) {
		this.hiddOrdinalSB1X = hiddOrdinalSB1X;
	}

	public String getHiddNombreSB2X() {
		return hiddNombreSB2X;
	}

	public void setHiddNombreSB2X(String hiddNombreSB2X) {
		this.hiddNombreSB2X = hiddNombreSB2X;
	}

	public String getHiddLinkSB2X() {
		return hiddLinkSB2X;
	}

	public void setHiddLinkSB2X(String hiddLinkSB2X) {
		this.hiddLinkSB2X = hiddLinkSB2X;
	}

	public String getHiddTrackerSB2X() {
		return hiddTrackerSB2X;
	}

	public void setHiddTrackerSB2X(String hiddTrackerSB2X) {
		this.hiddTrackerSB2X = hiddTrackerSB2X;
	}

	public String getHiddImagenSB2X() {
		return hiddImagenSB2X;
	}

	public void setHiddImagenSB2X(String hiddImagenSB2X) {
		this.hiddImagenSB2X = hiddImagenSB2X;
	}

	public String getHiddPrincipalSB2X() {
		return hiddPrincipalSB2X;
	}

	public void setHiddPrincipalSB2X(String hiddPrincipalSB2X) {
		this.hiddPrincipalSB2X = hiddPrincipalSB2X;
	}

	public String getHiddOrdinalSB2X() {
		return hiddOrdinalSB2X;
	}

	public void setHiddOrdinalSB2X(String hiddOrdinalSB2X) {
		this.hiddOrdinalSB2X = hiddOrdinalSB2X;
	}

	public String getHiddNombreSB3X() {
		return hiddNombreSB3X;
	}

	public void setHiddNombreSB3X(String hiddNombreSB3X) {
		this.hiddNombreSB3X = hiddNombreSB3X;
	}

	public String getHiddLinkSB3X() {
		return hiddLinkSB3X;
	}

	public void setHiddLinkSB3X(String hiddLinkSB3X) {
		this.hiddLinkSB3X = hiddLinkSB3X;
	}

	public String getHiddTrackerSB3X() {
		return hiddTrackerSB3X;
	}

	public void setHiddTrackerSB3X(String hiddTrackerSB3X) {
		this.hiddTrackerSB3X = hiddTrackerSB3X;
	}

	public String getHiddImagenSB3X() {
		return hiddImagenSB3X;
	}

	public void setHiddImagenSB3X(String hiddImagenSB3X) {
		this.hiddImagenSB3X = hiddImagenSB3X;
	}

	public String getHiddPrincipalSB3X() {
		return hiddPrincipalSB3X;
	}

	public void setHiddPrincipalSB3X(String hiddPrincipalSB3X) {
		this.hiddPrincipalSB3X = hiddPrincipalSB3X;
	}

	public String getHiddOrdinalSB3X() {
		return hiddOrdinalSB3X;
	}

	public void setHiddOrdinalSB3X(String hiddOrdinalSB3X) {
		this.hiddOrdinalSB3X = hiddOrdinalSB3X;
	}

	public String getHiddNombreSB4X() {
		return hiddNombreSB4X;
	}

	public void setHiddNombreSB4X(String hiddNombreSB4X) {
		this.hiddNombreSB4X = hiddNombreSB4X;
	}

	public String getHiddLinkSB4X() {
		return hiddLinkSB4X;
	}

	public void setHiddLinkSB4X(String hiddLinkSB4X) {
		this.hiddLinkSB4X = hiddLinkSB4X;
	}

	public String getHiddTrackerSB4X() {
		return hiddTrackerSB4X;
	}

	public void setHiddTrackerSB4X(String hiddTrackerSB4X) {
		this.hiddTrackerSB4X = hiddTrackerSB4X;
	}

	public String getHiddImagenSB4X() {
		return hiddImagenSB4X;
	}

	public void setHiddImagenSB4X(String hiddImagenSB4X) {
		this.hiddImagenSB4X = hiddImagenSB4X;
	}

	public String getHiddPrincipalSB4X() {
		return hiddPrincipalSB4X;
	}

	public void setHiddPrincipalSB4X(String hiddPrincipalSB4X) {
		this.hiddPrincipalSB4X = hiddPrincipalSB4X;
	}

	public String getHiddOrdinalSB4X() {
		return hiddOrdinalSB4X;
	}

	public void setHiddOrdinalSB4X(String hiddOrdinalSB4X) {
		this.hiddOrdinalSB4X = hiddOrdinalSB4X;
	}

	public String getHiddNombreSB5X() {
		return hiddNombreSB5X;
	}

	public void setHiddNombreSB5X(String hiddNombreSB5X) {
		this.hiddNombreSB5X = hiddNombreSB5X;
	}

	public String getHiddLinkSB5X() {
		return hiddLinkSB5X;
	}

	public void setHiddLinkSB5X(String hiddLinkSB5X) {
		this.hiddLinkSB5X = hiddLinkSB5X;
	}

	public String getHiddTrackerSB5X() {
		return hiddTrackerSB5X;
	}

	public void setHiddTrackerSB5X(String hiddTrackerSB5X) {
		this.hiddTrackerSB5X = hiddTrackerSB5X;
	}

	public String getHiddImagenSB5X() {
		return hiddImagenSB5X;
	}

	public void setHiddImagenSB5X(String hiddImagenSB5X) {
		this.hiddImagenSB5X = hiddImagenSB5X;
	}

	public String getHiddPrincipalSB5X() {
		return hiddPrincipalSB5X;
	}

	public void setHiddPrincipalSB5X(String hiddPrincipalSB5X) {
		this.hiddPrincipalSB5X = hiddPrincipalSB5X;
	}

	public String getHiddOrdinalSB5X() {
		return hiddOrdinalSB5X;
	}

	public void setHiddOrdinalSB5X(String hiddOrdinalSB5X) {
		this.hiddOrdinalSB5X = hiddOrdinalSB5X;
	}

	public String getHiddNombreSB6X() {
		return hiddNombreSB6X;
	}

	public void setHiddNombreSB6X(String hiddNombreSB6X) {
		this.hiddNombreSB6X = hiddNombreSB6X;
	}

	public String getHiddLinkSB6X() {
		return hiddLinkSB6X;
	}

	public void setHiddLinkSB6X(String hiddLinkSB6X) {
		this.hiddLinkSB6X = hiddLinkSB6X;
	}

	public String getHiddTrackerSB6X() {
		return hiddTrackerSB6X;
	}

	public void setHiddTrackerSB6X(String hiddTrackerSB6X) {
		this.hiddTrackerSB6X = hiddTrackerSB6X;
	}

	public String getHiddImagenSB6X() {
		return hiddImagenSB6X;
	}

	public void setHiddImagenSB6X(String hiddImagenSB6X) {
		this.hiddImagenSB6X = hiddImagenSB6X;
	}

	public String getHiddPrincipalSB6X() {
		return hiddPrincipalSB6X;
	}

	public void setHiddPrincipalSB6X(String hiddPrincipalSB6X) {
		this.hiddPrincipalSB6X = hiddPrincipalSB6X;
	}

	public String getHiddOrdinalSB6X() {
		return hiddOrdinalSB6X;
	}

	public void setHiddOrdinalSB6X(String hiddOrdinalSB6X) {
		this.hiddOrdinalSB6X = hiddOrdinalSB6X;
	}

	private String hiddLinkVB2X;
    
    private String hiddTrackerVB2X;
    
    private String hiddImagenVB2X;
    
    private String hiddPrincipalVB2X;
    
    private String hiddOrdinalVB2X;
    
    private String hiddNombreVB3X;
    
    private String hiddLinkVB3X;
    
    private String hiddTrackerVB3X;
    
    private String hiddImagenVB3X;
    
    private String hiddPrincipalVB3X;
    
    private String hiddOrdinalVB3X;
    
    private String hiddNombreVB4X;
    
    private String hiddLinkVB4X;
    
    private String hiddTrackerVB4X;
    
    private String hiddImagenVB4X;
    
    private String hiddPrincipalVB4X;
    
    private String hiddOrdinalVB4X;
    
    private String hiddNombreVB5X;
    
    private String hiddLinkVB5X;
    
    private String hiddTrackerVB5X;
    
    private String hiddImagenVB5X;
    
    private String hiddPrincipalVB5X;
    
    private String hiddOrdinalVB5X;
    
    private String hiddNombreVB6X;
    
    private String hiddLinkVB6X;
    
    private String hiddTrackerVB6X;
    
    private String hiddImagenVB6X;
    
    private String hiddPrincipalVB6X;
    
    private String hiddOrdinalVB6X;
    
    private String hiddNombreHB6X;
    
    private String hiddLinkHB6X;
    
    private String hiddTrackerHB6X;
    
    private String hiddImagenHB6X;
    
    private String hiddPrincipalHB6X;
    
    private String hiddOrdinalHB6X;
    
    private String hiddNombreHB1X;
    
    private String hiddLinkHB1X;
    
    private String hiddTrackerHB1X;
    
    private String hiddImagenHB1X;
    
    private String hiddPrincipalHB1X;
    
    private String hiddOrdinalHB1X;
    
    private String hiddNombreHB2X;
    
    private String hiddLinkHB2X;
    
    private String hiddTrackerHB2X;
    
    private String hiddImagenHB2X;
    
    private String hiddPrincipalHB2X;
    
    private String hiddOrdinalHB2X;
    
    private String hiddNombreHB3X;
    
    private String hiddLinkHB3X;
    
    private String hiddTrackerHB3X;
    
    private String hiddImagenHB3X;
    
    private String hiddPrincipalHB3X;
    
    private String hiddOrdinalHB3X;
    
    private String hiddNombreHB4X;
    
    private String hiddLinkHB4X;
    
    private String hiddTrackerHB4X;
    
    private String hiddImagenHB4X;
    
    private String hiddPrincipalHB4X;
    
    private String hiddOrdinalHB4X;
    
    private String hiddNombreHB5X;
    
    private String hiddLinkHB5X;
    
    private String hiddTrackerHB5X;
    
    private String hiddImagenHB5X;
    
    private String hiddPrincipalHB5X;
    
    private String hiddOrdinalHB5X;
    
    private String hiddNombreVIB1X;
    
    private String hiddLinkVIB1X;
    
    private String hiddTrackerVIB1X;
    
    private String hiddImagenVIB1X;
    
    private String hiddPrincipalVIB1X;
    
    private String hiddOrdinalVIB1X;
    
    private String hiddNombreVIB2X;
    
    private String hiddLinkVIB2X;
    
    private String hiddTrackerVIB2X;
    
    private String hiddImagenVIB2X;
    
    private String hiddPrincipalVIB2X;
    
    private String hiddOrdinalVIB2X;
    
    private String hiddNombreVIB3X;
    
    private String hiddLinkVIB3X;
    
    private String hiddTrackerVIB3X;
    
    private String hiddImagenVIB3X;
    
    private String hiddPrincipalVIB3X;
    
    private String hiddOrdinalVIB3X;
    
    private String hiddNombreVIB4X;
    
    private String hiddLinkVIB4X;
    
    private String hiddTrackerVIB4X;
    
    private String hiddImagenVIB4X;
    
    private String hiddPrincipalVIB4X;
    
    private String hiddOrdinalVIB4X;
    
    private String hiddNombreVIB5X;
    
    private String hiddLinkVIB5X;
    
    private String hiddTrackerVIB5X;
    
    private String hiddImagenVIB5X;
    
    private String hiddPrincipalVIB5X;
    
    private String hiddOrdinalVIB5X;
    
    private String hiddNombreVIB6X;
    
    private String hiddLinkVIB6X;
    
    private String hiddTrackerVIB6X;
    
    private String hiddImagenVIB6X;
    
    private String hiddPrincipalVIB6X;
    
    private String hiddOrdinalVIB6X;
    
    private String hiddNombreSB1X;
    
    private String hiddLinkSB1X;
    
    private String hiddTrackerSB1X;
    
    private String hiddImagenSB1X;
    
    private String hiddPrincipalSB1X;
    
    private String hiddOrdinalSB1X;
    
    private String hiddNombreSB2X;
    
    private String hiddLinkSB2X;
    
    private String hiddTrackerSB2X;
    
    private String hiddImagenSB2X;
    
    private String hiddPrincipalSB2X;
    
    private String hiddOrdinalSB2X;
    
    private String hiddNombreSB3X;
    
    private String hiddLinkSB3X;
    
    private String hiddTrackerSB3X;
    
    private String hiddImagenSB3X;
    
    private String hiddPrincipalSB3X;
    
    private String hiddOrdinalSB3X;
    
    private String hiddNombreSB4X;
    
    private String hiddLinkSB4X;
    
    private String hiddTrackerSB4X;
    
    private String hiddImagenSB4X;
    
    private String hiddPrincipalSB4X;
    
    private String hiddOrdinalSB4X;
    
    private String hiddNombreSB5X;
    
    private String hiddLinkSB5X;
    
    private String hiddTrackerSB5X;
    
    private String hiddImagenSB5X;
    
    private String hiddPrincipalSB5X;
    
    private String hiddOrdinalSB5X;
    
    private String hiddNombreSB6X;
    
    private String hiddLinkSB6X;
    
    private String hiddTrackerSB6X;
    
    private String hiddImagenSB6X;
    
    private String hiddPrincipalSB6X;
    
    private String hiddOrdinalSB6X;
    
    public int[] getPaginas() {
        return paginas;
    }

    public void setPaginas(int[] paginas) {
        this.paginas = paginas;
    }

    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return InformeComercialTarjetasForm.class
            .getResourceAsStream("resource/validation-tarjetas.xml");
    }
    
    @Override
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {

        ActionErrors errores = super.validate(mapping, request);

        if (!this.getDatos().get("fechaDesde").equals("")
            && !this.getDatos().get("fechaHasta").equals("")) {

            try {
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDesde =
                    formato.parse(this.getDatos().get("fechaDesde"));
                Date fechaHasta =
                    formato.parse(this.getDatos().get("fechaHasta"));

                if (fechaHasta.before(fechaDesde)) {
                    errores.add("datos.fechaDesde",
                        new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                }

            } catch (ParseException e) {
                errores.add("datos.fechaDesde", new ActionMessage(
                    "errors.validacion-fecha", "Fecha Hasta"));
            }

        }
        if (!this.getDatos().get("fechaDesdeVB1").equals("")
                && !this.getDatos().get("fechaHastaVB1").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVB1"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVB1"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }

        if (!this.getDatos().get("fechaDesdeVB2").equals("")
                && !this.getDatos().get("fechaHastaVB2").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVB2"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVB2"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVB3").equals("")
                && !this.getDatos().get("fechaHastaVB3").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVB3"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVB3"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVB4").equals("")
                && !this.getDatos().get("fechaHastaVB4").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVB4"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVB4"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVB5").equals("")
                && !this.getDatos().get("fechaHastaVB5").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVB5"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVB5"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVB6").equals("")
                && !this.getDatos().get("fechaHastaVB6").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVB6"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVB6"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeHB1").equals("")
                && !this.getDatos().get("fechaHastaHB1").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeHB1"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaHB1"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeHB2").equals("")
                && !this.getDatos().get("fechaHastaHB2").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeHB2"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaHB2"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeHB3").equals("")
                && !this.getDatos().get("fechaHastaHB3").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeHB3"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaHB3"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeHB4").equals("")
                && !this.getDatos().get("fechaHastaHB4").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeHB4"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaHB4"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeHB5").equals("")
                && !this.getDatos().get("fechaHastaHB5").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeHB5"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaHB5"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeHB6").equals("")
                && !this.getDatos().get("fechaHastaHB6").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeHB6"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaHB6"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVIB1").equals("")
                && !this.getDatos().get("fechaHastaVIB1").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVIB1"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVIB1"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVIB2").equals("")
                && !this.getDatos().get("fechaHastaVIB2").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVIB2"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVIB2"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVIB3").equals("")
                && !this.getDatos().get("fechaHastaVIB3").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVIB3"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVIB3"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVIB4").equals("")
                && !this.getDatos().get("fechaHastaVIB4").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVIB4"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVIB4"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVIB5").equals("")
                && !this.getDatos().get("fechaHastaVIB5").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVIB5"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVIB5"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeVIB6").equals("")
                && !this.getDatos().get("fechaHastaVIB6").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeVIB6"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaVIB6"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeSB1").equals("")
                && !this.getDatos().get("fechaHastaSB1").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeSB1"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaSB1"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeSB2").equals("")
                && !this.getDatos().get("fechaHastaSB2").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeSB2"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaSB2"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeSB3").equals("")
                && !this.getDatos().get("fechaHastaSB3").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeSB3"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaSB3"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeSB4").equals("")
                && !this.getDatos().get("fechaHastaSB4").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeSB4"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaSB4"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeSB5").equals("")
                && !this.getDatos().get("fechaHastaSB5").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeSB5"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaSB5"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        if (!this.getDatos().get("fechaDesdeSB6").equals("")
                && !this.getDatos().get("fechaHastaSB6").equals("")) {

                try {
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                    Date fechaDesde =
                        formato.parse(this.getDatos().get("fechaDesdeSB6"));
                    Date fechaHasta =
                        formato.parse(this.getDatos().get("fechaHastaSB6"));

                    if (fechaHasta.before(fechaDesde)) {
                        errores.add("datos.fechaDesde",
                            new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                    }

                } catch (ParseException e) {
                    errores.add("datos.fechaDesde", new ActionMessage(
                        "errors.validacion-fecha", "Fecha Hasta"));
                }

            }
        
        return errores;
    }

    
	public void setRama(int rama) {
		this.rama = rama;
	}

	public int getRama() {
		return rama;
	}

	public void setSubcategoria(int subcategoria) {
		this.subcategoria = subcategoria;
	}

	public int getSubcategoria() {
		return subcategoria;
	}

	public void setId_plan(int id_plan) {
		this.id_plan = id_plan;
	}

	public int getId_plan() {
		return id_plan;
	}

	public void setCompannia(String compannia) {
		this.compannia = compannia;
	}

	public String getCompannia() {
		return compannia;
	}

	public void setHiddNombre(String hiddNombre) {
		this.hiddNombre = hiddNombre;
	}

	public String getHiddNombre() {
		return hiddNombre;
	}

	public void setHiddPosicion(String hiddPosicion) {
		this.hiddPosicion = hiddPosicion;
	}

	public String getHiddPosicion() {
		return hiddPosicion;
	}

	public void setHiddLink(String hiddLink) {
		this.hiddLink = hiddLink;
	}

	public String getHiddLink() {
		return hiddLink;
	}

	public void setHiddTracker(String hiddTracker) {
		this.hiddTracker = hiddTracker;
	}

	public String getHiddTracker() {
		return hiddTracker;
	}

	public void setHiddImagen(String hiddImagen) {
		this.hiddImagen = hiddImagen;
	}

	public String getHiddImagen() {
		return hiddImagen;
	}

	public void setHiddPrincipal(String hiddPrincipal) {
		this.hiddPrincipal = hiddPrincipal;
	}

	public String getHiddPrincipal() {
		return hiddPrincipal;
	}

	public void setHiddNombreVB1(String hiddNombreVB1) {
		this.hiddNombreVB1 = hiddNombreVB1;
	}

	public String getHiddNombreVB1() {
		return hiddNombreVB1;
	}

	public void setHiddLinkVB1(String hiddLinkVB1) {
		this.hiddLinkVB1 = hiddLinkVB1;
	}

	public String getHiddLinkVB1() {
		return hiddLinkVB1;
	}

	public void setHiddTrackerVB1(String hiddTrackerVB1) {
		this.hiddTrackerVB1 = hiddTrackerVB1;
	}

	public String getHiddTrackerVB1() {
		return hiddTrackerVB1;
	}

	public void setHiddImagenVB1(String hiddImagenVB1) {
		this.hiddImagenVB1 = hiddImagenVB1;
	}

	public String getHiddImagenVB1() {
		return hiddImagenVB1;
	}

	public void setHiddPrincipalVB1(String hiddPrincipalVB1) {
		this.hiddPrincipalVB1 = hiddPrincipalVB1;
	}

	public String getHiddPrincipalVB1() {
		return hiddPrincipalVB1;
	}

	public void setHiddOrdinalVB1(String hiddOrdinalVB1) {
		this.hiddOrdinalVB1 = hiddOrdinalVB1;
	}

	public String getHiddOrdinalVB1() {
		return hiddOrdinalVB1;
	}

	public void setHiddNombreVB2(String hiddNombreVB2) {
		this.hiddNombreVB2 = hiddNombreVB2;
	}

	public String getHiddNombreVB2() {
		return hiddNombreVB2;
	}

	public void setHiddLinkVB2(String hiddLinkVB2) {
		this.hiddLinkVB2 = hiddLinkVB2;
	}

	public String getHiddLinkVB2() {
		return hiddLinkVB2;
	}

	public void setHiddTrackerVB2(String hiddTrackerVB2) {
		this.hiddTrackerVB2 = hiddTrackerVB2;
	}

	public String getHiddTrackerVB2() {
		return hiddTrackerVB2;
	}

	public void setHiddImagenVB2(String hiddImagenVB2) {
		this.hiddImagenVB2 = hiddImagenVB2;
	}

	public String getHiddImagenVB2() {
		return hiddImagenVB2;
	}

	public void setHiddPrincipalVB2(String hiddPrincipalVB2) {
		this.hiddPrincipalVB2 = hiddPrincipalVB2;
	}

	public String getHiddPrincipalVB2() {
		return hiddPrincipalVB2;
	}

	public void setHiddOrdinalVB2(String hiddOrdinalVB2) {
		this.hiddOrdinalVB2 = hiddOrdinalVB2;
	}

	public String getHiddOrdinalVB2() {
		return hiddOrdinalVB2;
	}

	public void setHiddNombreVB3(String hiddNombreVB3) {
		this.hiddNombreVB3 = hiddNombreVB3;
	}

	public String getHiddNombreVB3() {
		return hiddNombreVB3;
	}

	public void setHiddLinkVB3(String hiddLinkVB3) {
		this.hiddLinkVB3 = hiddLinkVB3;
	}

	public String getHiddLinkVB3() {
		return hiddLinkVB3;
	}

	public void setHiddTrackerVB3(String hiddTrackerVB3) {
		this.hiddTrackerVB3 = hiddTrackerVB3;
	}

	public String getHiddTrackerVB3() {
		return hiddTrackerVB3;
	}

	public void setHiddImagenVB3(String hiddImagenVB3) {
		this.hiddImagenVB3 = hiddImagenVB3;
	}

	public String getHiddImagenVB3() {
		return hiddImagenVB3;
	}

	public void setHiddPrincipalVB3(String hiddPrincipalVB3) {
		this.hiddPrincipalVB3 = hiddPrincipalVB3;
	}

	public String getHiddPrincipalVB3() {
		return hiddPrincipalVB3;
	}

	public void setHiddOrdinalVB3(String hiddOrdinalVB3) {
		this.hiddOrdinalVB3 = hiddOrdinalVB3;
	}

	public String getHiddOrdinalVB3() {
		return hiddOrdinalVB3;
	}

	public void setHiddNombreVB4(String hiddNombreVB4) {
		this.hiddNombreVB4 = hiddNombreVB4;
	}

	public String getHiddNombreVB4() {
		return hiddNombreVB4;
	}

	public void setHiddLinkVB4(String hiddLinkVB4) {
		this.hiddLinkVB4 = hiddLinkVB4;
	}

	public String getHiddLinkVB4() {
		return hiddLinkVB4;
	}

	public void setHiddTrackerVB4(String hiddTrackerVB4) {
		this.hiddTrackerVB4 = hiddTrackerVB4;
	}

	public String getHiddTrackerVB4() {
		return hiddTrackerVB4;
	}

	public void setHiddImagenVB4(String hiddImagenVB4) {
		this.hiddImagenVB4 = hiddImagenVB4;
	}

	public String getHiddImagenVB4() {
		return hiddImagenVB4;
	}

	public void setHiddPrincipalVB4(String hiddPrincipalVB4) {
		this.hiddPrincipalVB4 = hiddPrincipalVB4;
	}

	public String getHiddPrincipalVB4() {
		return hiddPrincipalVB4;
	}

	public void setHiddOrdinalVB4(String hiddOrdinalVB4) {
		this.hiddOrdinalVB4 = hiddOrdinalVB4;
	}

	public String getHiddOrdinalVB4() {
		return hiddOrdinalVB4;
	}

	public void setHiddNombreVB5(String hiddNombreVB5) {
		this.hiddNombreVB5 = hiddNombreVB5;
	}

	public String getHiddNombreVB5() {
		return hiddNombreVB5;
	}

	public void setHiddLinkVB5(String hiddLinkVB5) {
		this.hiddLinkVB5 = hiddLinkVB5;
	}

	public String getHiddLinkVB5() {
		return hiddLinkVB5;
	}

	public void setHiddTrackerVB5(String hiddTrackerVB5) {
		this.hiddTrackerVB5 = hiddTrackerVB5;
	}

	public String getHiddTrackerVB5() {
		return hiddTrackerVB5;
	}

	public void setHiddImagenVB5(String hiddImagenVB5) {
		this.hiddImagenVB5 = hiddImagenVB5;
	}

	public String getHiddImagenVB5() {
		return hiddImagenVB5;
	}

	public void setHiddPrincipalVB5(String hiddPrincipalVB5) {
		this.hiddPrincipalVB5 = hiddPrincipalVB5;
	}

	public String getHiddPrincipalVB5() {
		return hiddPrincipalVB5;
	}

	public void setHiddOrdinalVB5(String hiddOrdinalVB5) {
		this.hiddOrdinalVB5 = hiddOrdinalVB5;
	}

	public String getHiddOrdinalVB5() {
		return hiddOrdinalVB5;
	}

	public void setHiddNombreVB6(String hiddNombreVB6) {
		this.hiddNombreVB6 = hiddNombreVB6;
	}

	public String getHiddNombreVB6() {
		return hiddNombreVB6;
	}

	public void setHiddLinkVB6(String hiddLinkVB6) {
		this.hiddLinkVB6 = hiddLinkVB6;
	}

	public String getHiddLinkVB6() {
		return hiddLinkVB6;
	}

	public void setHiddTrackerVB6(String hiddTrackerVB6) {
		this.hiddTrackerVB6 = hiddTrackerVB6;
	}

	public String getHiddTrackerVB6() {
		return hiddTrackerVB6;
	}

	public void setHiddImagenVB6(String hiddImagenVB6) {
		this.hiddImagenVB6 = hiddImagenVB6;
	}

	public String getHiddImagenVB6() {
		return hiddImagenVB6;
	}

	public void setHiddPrincipalVB6(String hiddPrincipalVB6) {
		this.hiddPrincipalVB6 = hiddPrincipalVB6;
	}

	public String getHiddPrincipalVB6() {
		return hiddPrincipalVB6;
	}

	public void setHiddOrdinalVB6(String hiddOrdinalVB6) {
		this.hiddOrdinalVB6 = hiddOrdinalVB6;
	}

	public String getHiddOrdinalVB6() {
		return hiddOrdinalVB6;
	}

	public void setHiddNombreHB6(String hiddNombreHB6) {
		this.hiddNombreHB6 = hiddNombreHB6;
	}

	public String getHiddNombreHB6() {
		return hiddNombreHB6;
	}

	public void setHiddLinkHB6(String hiddLinkHB6) {
		this.hiddLinkHB6 = hiddLinkHB6;
	}

	public String getHiddLinkHB6() {
		return hiddLinkHB6;
	}

	public void setHiddTrackerHB6(String hiddTrackerHB6) {
		this.hiddTrackerHB6 = hiddTrackerHB6;
	}

	public String getHiddTrackerHB6() {
		return hiddTrackerHB6;
	}

	public void setHiddImagenHB6(String hiddImagenHB6) {
		this.hiddImagenHB6 = hiddImagenHB6;
	}

	public String getHiddImagenHB6() {
		return hiddImagenHB6;
	}

	public void setHiddPrincipalHB6(String hiddPrincipalHB6) {
		this.hiddPrincipalHB6 = hiddPrincipalHB6;
	}

	public String getHiddPrincipalHB6() {
		return hiddPrincipalHB6;
	}

	public void setHiddOrdinalHB6(String hiddOrdinalHB6) {
		this.hiddOrdinalHB6 = hiddOrdinalHB6;
	}

	public String getHiddOrdinalHB6() {
		return hiddOrdinalHB6;
	}

	public void setHiddNombreHB1(String hiddNombreHB1) {
		this.hiddNombreHB1 = hiddNombreHB1;
	}

	public String getHiddNombreHB1() {
		return hiddNombreHB1;
	}

	public void setHiddLinkHB1(String hiddLinkHB1) {
		this.hiddLinkHB1 = hiddLinkHB1;
	}

	public String getHiddLinkHB1() {
		return hiddLinkHB1;
	}

	public void setHiddTrackerHB1(String hiddTrackerHB1) {
		this.hiddTrackerHB1 = hiddTrackerHB1;
	}

	public String getHiddTrackerHB1() {
		return hiddTrackerHB1;
	}

	public void setHiddImagenHB1(String hiddImagenHB1) {
		this.hiddImagenHB1 = hiddImagenHB1;
	}

	public String getHiddImagenHB1() {
		return hiddImagenHB1;
	}

	public void setHiddPrincipalHB1(String hiddPrincipalHB1) {
		this.hiddPrincipalHB1 = hiddPrincipalHB1;
	}

	public String getHiddPrincipalHB1() {
		return hiddPrincipalHB1;
	}

	public void setHiddOrdinalHB1(String hiddOrdinalHB1) {
		this.hiddOrdinalHB1 = hiddOrdinalHB1;
	}

	public String getHiddOrdinalHB1() {
		return hiddOrdinalHB1;
	}

	public void setHiddNombreHB2(String hiddNombreHB2) {
		this.hiddNombreHB2 = hiddNombreHB2;
	}

	public String getHiddNombreHB2() {
		return hiddNombreHB2;
	}

	public void setHiddLinkHB2(String hiddLinkHB2) {
		this.hiddLinkHB2 = hiddLinkHB2;
	}

	public String getHiddLinkHB2() {
		return hiddLinkHB2;
	}

	public void setHiddTrackerHB2(String hiddTrackerHB2) {
		this.hiddTrackerHB2 = hiddTrackerHB2;
	}

	public String getHiddTrackerHB2() {
		return hiddTrackerHB2;
	}

	public void setHiddImagenHB2(String hiddImagenHB2) {
		this.hiddImagenHB2 = hiddImagenHB2;
	}

	public String getHiddImagenHB2() {
		return hiddImagenHB2;
	}

	public void setHiddPrincipalHB2(String hiddPrincipalHB2) {
		this.hiddPrincipalHB2 = hiddPrincipalHB2;
	}

	public String getHiddPrincipalHB2() {
		return hiddPrincipalHB2;
	}

	public void setHiddOrdinalHB2(String hiddOrdinalHB2) {
		this.hiddOrdinalHB2 = hiddOrdinalHB2;
	}

	public String getHiddOrdinalHB2() {
		return hiddOrdinalHB2;
	}

	public void setHiddNombreHB3(String hiddNombreHB3) {
		this.hiddNombreHB3 = hiddNombreHB3;
	}

	public String getHiddNombreHB3() {
		return hiddNombreHB3;
	}

	public void setHiddLinkHB3(String hiddLinkHB3) {
		this.hiddLinkHB3 = hiddLinkHB3;
	}

	public String getHiddLinkHB3() {
		return hiddLinkHB3;
	}

	public void setHiddTrackerHB3(String hiddTrackerHB3) {
		this.hiddTrackerHB3 = hiddTrackerHB3;
	}

	public String getHiddTrackerHB3() {
		return hiddTrackerHB3;
	}

	public void setHiddImagenHB3(String hiddImagenHB3) {
		this.hiddImagenHB3 = hiddImagenHB3;
	}

	public String getHiddImagenHB3() {
		return hiddImagenHB3;
	}

	public void setHiddPrincipalHB3(String hiddPrincipalHB3) {
		this.hiddPrincipalHB3 = hiddPrincipalHB3;
	}

	public String getHiddPrincipalHB3() {
		return hiddPrincipalHB3;
	}

	public void setHiddOrdinalHB3(String hiddOrdinalHB3) {
		this.hiddOrdinalHB3 = hiddOrdinalHB3;
	}

	public String getHiddOrdinalHB3() {
		return hiddOrdinalHB3;
	}

	public void setHiddNombreHB4(String hiddNombreHB4) {
		this.hiddNombreHB4 = hiddNombreHB4;
	}

	public String getHiddNombreHB4() {
		return hiddNombreHB4;
	}

	public void setHiddLinkHB4(String hiddLinkHB4) {
		this.hiddLinkHB4 = hiddLinkHB4;
	}

	public String getHiddLinkHB4() {
		return hiddLinkHB4;
	}

	public void setHiddTrackerHB4(String hiddTrackerHB4) {
		this.hiddTrackerHB4 = hiddTrackerHB4;
	}

	public String getHiddTrackerHB4() {
		return hiddTrackerHB4;
	}

	public void setHiddImagenHB4(String hiddImagenHB4) {
		this.hiddImagenHB4 = hiddImagenHB4;
	}

	public String getHiddImagenHB4() {
		return hiddImagenHB4;
	}

	public void setHiddPrincipalHB4(String hiddPrincipalHB4) {
		this.hiddPrincipalHB4 = hiddPrincipalHB4;
	}

	public String getHiddPrincipalHB4() {
		return hiddPrincipalHB4;
	}

	public void setHiddOrdinalHB4(String hiddOrdinalHB4) {
		this.hiddOrdinalHB4 = hiddOrdinalHB4;
	}

	public String getHiddOrdinalHB4() {
		return hiddOrdinalHB4;
	}

	public void setHiddNombreHB5(String hiddNombreHB5) {
		this.hiddNombreHB5 = hiddNombreHB5;
	}

	public String getHiddNombreHB5() {
		return hiddNombreHB5;
	}

	public void setHiddLinkHB5(String hiddLinkHB5) {
		this.hiddLinkHB5 = hiddLinkHB5;
	}

	public String getHiddLinkHB5() {
		return hiddLinkHB5;
	}

	public void setHiddTrackerHB5(String hiddTrackerHB5) {
		this.hiddTrackerHB5 = hiddTrackerHB5;
	}

	public String getHiddTrackerHB5() {
		return hiddTrackerHB5;
	}

	public void setHiddImagenHB5(String hiddImagenHB5) {
		this.hiddImagenHB5 = hiddImagenHB5;
	}

	public String getHiddImagenHB5() {
		return hiddImagenHB5;
	}

	public void setHiddPrincipalHB5(String hiddPrincipalHB5) {
		this.hiddPrincipalHB5 = hiddPrincipalHB5;
	}

	public String getHiddPrincipalHB5() {
		return hiddPrincipalHB5;
	}

	public void setHiddOrdinalHB5(String hiddOrdinalHB5) {
		this.hiddOrdinalHB5 = hiddOrdinalHB5;
	}

	public String getHiddOrdinalHB5() {
		return hiddOrdinalHB5;
	}

	public void setHiddNombreVIB1(String hiddNombreVIB1) {
		this.hiddNombreVIB1 = hiddNombreVIB1;
	}

	public String getHiddNombreVIB1() {
		return hiddNombreVIB1;
	}

	public void setHiddLinkVIB1(String hiddLinkVIB1) {
		this.hiddLinkVIB1 = hiddLinkVIB1;
	}

	public String getHiddLinkVIB1() {
		return hiddLinkVIB1;
	}

	public void setHiddTrackerVIB1(String hiddTrackerVIB1) {
		this.hiddTrackerVIB1 = hiddTrackerVIB1;
	}

	public String getHiddTrackerVIB1() {
		return hiddTrackerVIB1;
	}

	public void setHiddImagenVIB1(String hiddImagenVIB1) {
		this.hiddImagenVIB1 = hiddImagenVIB1;
	}

	public String getHiddImagenVIB1() {
		return hiddImagenVIB1;
	}

	public void setHiddPrincipalVIB1(String hiddPrincipalVIB1) {
		this.hiddPrincipalVIB1 = hiddPrincipalVIB1;
	}

	public String getHiddPrincipalVIB1() {
		return hiddPrincipalVIB1;
	}

	public void setHiddOrdinalVIB1(String hiddOrdinalVIB1) {
		this.hiddOrdinalVIB1 = hiddOrdinalVIB1;
	}

	public String getHiddOrdinalVIB1() {
		return hiddOrdinalVIB1;
	}

	public void setHiddNombreVIB2(String hiddNombreVIB2) {
		this.hiddNombreVIB2 = hiddNombreVIB2;
	}

	public String getHiddNombreVIB2() {
		return hiddNombreVIB2;
	}

	public void setHiddLinkVIB2(String hiddLinkVIB2) {
		this.hiddLinkVIB2 = hiddLinkVIB2;
	}

	public String getHiddLinkVIB2() {
		return hiddLinkVIB2;
	}

	public void setHiddTrackerVIB2(String hiddTrackerVIB2) {
		this.hiddTrackerVIB2 = hiddTrackerVIB2;
	}

	public String getHiddTrackerVIB2() {
		return hiddTrackerVIB2;
	}

	public void setHiddImagenVIB2(String hiddImagenVIB2) {
		this.hiddImagenVIB2 = hiddImagenVIB2;
	}

	public String getHiddImagenVIB2() {
		return hiddImagenVIB2;
	}

	public void setHiddPrincipalVIB2(String hiddPrincipalVIB2) {
		this.hiddPrincipalVIB2 = hiddPrincipalVIB2;
	}

	public String getHiddPrincipalVIB2() {
		return hiddPrincipalVIB2;
	}

	public void setHiddOrdinalVIB2(String hiddOrdinalVIB2) {
		this.hiddOrdinalVIB2 = hiddOrdinalVIB2;
	}

	public String getHiddOrdinalVIB2() {
		return hiddOrdinalVIB2;
	}

	public void setHiddNombreVIB3(String hiddNombreVIB3) {
		this.hiddNombreVIB3 = hiddNombreVIB3;
	}

	public String getHiddNombreVIB3() {
		return hiddNombreVIB3;
	}

	public void setHiddLinkVIB3(String hiddLinkVIB3) {
		this.hiddLinkVIB3 = hiddLinkVIB3;
	}

	public String getHiddLinkVIB3() {
		return hiddLinkVIB3;
	}

	public void setHiddTrackerVIB3(String hiddTrackerVIB3) {
		this.hiddTrackerVIB3 = hiddTrackerVIB3;
	}

	public String getHiddTrackerVIB3() {
		return hiddTrackerVIB3;
	}

	public void setHiddImagenVIB3(String hiddImagenVIB3) {
		this.hiddImagenVIB3 = hiddImagenVIB3;
	}

	public String getHiddImagenVIB3() {
		return hiddImagenVIB3;
	}

	public void setHiddPrincipalVIB3(String hiddPrincipalVIB3) {
		this.hiddPrincipalVIB3 = hiddPrincipalVIB3;
	}

	public String getHiddPrincipalVIB3() {
		return hiddPrincipalVIB3;
	}

	public void setHiddOrdinalVIB3(String hiddOrdinalVIB3) {
		this.hiddOrdinalVIB3 = hiddOrdinalVIB3;
	}

	public String getHiddOrdinalVIB3() {
		return hiddOrdinalVIB3;
	}

	public void setHiddNombreVIB4(String hiddNombreVIB4) {
		this.hiddNombreVIB4 = hiddNombreVIB4;
	}

	public String getHiddNombreVIB4() {
		return hiddNombreVIB4;
	}

	public void setHiddLinkVIB4(String hiddLinkVIB4) {
		this.hiddLinkVIB4 = hiddLinkVIB4;
	}

	public String getHiddLinkVIB4() {
		return hiddLinkVIB4;
	}

	public void setHiddTrackerVIB4(String hiddTrackerVIB4) {
		this.hiddTrackerVIB4 = hiddTrackerVIB4;
	}

	public String getHiddTrackerVIB4() {
		return hiddTrackerVIB4;
	}

	public void setHiddImagenVIB4(String hiddImagenVIB4) {
		this.hiddImagenVIB4 = hiddImagenVIB4;
	}

	public String getHiddImagenVIB4() {
		return hiddImagenVIB4;
	}

	public void setHiddPrincipalVIB4(String hiddPrincipalVIB4) {
		this.hiddPrincipalVIB4 = hiddPrincipalVIB4;
	}

	public String getHiddPrincipalVIB4() {
		return hiddPrincipalVIB4;
	}

	public void setHiddOrdinalVIB4(String hiddOrdinalVIB4) {
		this.hiddOrdinalVIB4 = hiddOrdinalVIB4;
	}

	public String getHiddOrdinalVIB4() {
		return hiddOrdinalVIB4;
	}

	public void setHiddNombreVIB5(String hiddNombreVIB5) {
		this.hiddNombreVIB5 = hiddNombreVIB5;
	}

	public String getHiddNombreVIB5() {
		return hiddNombreVIB5;
	}

	public void setHiddLinkVIB5(String hiddLinkVIB5) {
		this.hiddLinkVIB5 = hiddLinkVIB5;
	}

	public String getHiddLinkVIB5() {
		return hiddLinkVIB5;
	}

	public void setHiddTrackerVIB5(String hiddTrackerVIB5) {
		this.hiddTrackerVIB5 = hiddTrackerVIB5;
	}

	public String getHiddTrackerVIB5() {
		return hiddTrackerVIB5;
	}

	public void setHiddImagenVIB5(String hiddImagenVIB5) {
		this.hiddImagenVIB5 = hiddImagenVIB5;
	}

	public String getHiddImagenVIB5() {
		return hiddImagenVIB5;
	}

	public void setHiddPrincipalVIB5(String hiddPrincipalVIB5) {
		this.hiddPrincipalVIB5 = hiddPrincipalVIB5;
	}

	public String getHiddPrincipalVIB5() {
		return hiddPrincipalVIB5;
	}

	public void setHiddOrdinalVIB5(String hiddOrdinalVIB5) {
		this.hiddOrdinalVIB5 = hiddOrdinalVIB5;
	}

	public String getHiddOrdinalVIB5() {
		return hiddOrdinalVIB5;
	}

	public void setHiddNombreVIB6(String hiddNombreVIB6) {
		this.hiddNombreVIB6 = hiddNombreVIB6;
	}

	public String getHiddNombreVIB6() {
		return hiddNombreVIB6;
	}

	public void setHiddLinkVIB6(String hiddLinkVIB6) {
		this.hiddLinkVIB6 = hiddLinkVIB6;
	}

	public String getHiddLinkVIB6() {
		return hiddLinkVIB6;
	}

	public void setHiddTrackerVIB6(String hiddTrackerVIB6) {
		this.hiddTrackerVIB6 = hiddTrackerVIB6;
	}

	public String getHiddTrackerVIB6() {
		return hiddTrackerVIB6;
	}

	public void setHiddImagenVIB6(String hiddImagenVIB6) {
		this.hiddImagenVIB6 = hiddImagenVIB6;
	}

	public String getHiddImagenVIB6() {
		return hiddImagenVIB6;
	}

	public void setHiddPrincipalVIB6(String hiddPrincipalVIB6) {
		this.hiddPrincipalVIB6 = hiddPrincipalVIB6;
	}

	public String getHiddPrincipalVIB6() {
		return hiddPrincipalVIB6;
	}

	public void setHiddOrdinalVIB6(String hiddOrdinalVIB6) {
		this.hiddOrdinalVIB6 = hiddOrdinalVIB6;
	}

	public String getHiddOrdinalVIB6() {
		return hiddOrdinalVIB6;
	}

	public void setHiddNombreSB1(String hiddNombreSB1) {
		this.hiddNombreSB1 = hiddNombreSB1;
	}

	public String getHiddNombreSB1() {
		return hiddNombreSB1;
	}

	public void setHiddLinkSB1(String hiddLinkSB1) {
		this.hiddLinkSB1 = hiddLinkSB1;
	}

	public String getHiddLinkSB1() {
		return hiddLinkSB1;
	}

	public void setHiddTrackerSB1(String hiddTrackerSB1) {
		this.hiddTrackerSB1 = hiddTrackerSB1;
	}

	public String getHiddTrackerSB1() {
		return hiddTrackerSB1;
	}

	public void setHiddImagenSB1(String hiddImagenSB1) {
		this.hiddImagenSB1 = hiddImagenSB1;
	}

	public String getHiddImagenSB1() {
		return hiddImagenSB1;
	}

	public void setHiddPrincipalSB1(String hiddPrincipalSB1) {
		this.hiddPrincipalSB1 = hiddPrincipalSB1;
	}

	public String getHiddPrincipalSB1() {
		return hiddPrincipalSB1;
	}

	public void setHiddOrdinalSB1(String hiddOrdinalSB1) {
		this.hiddOrdinalSB1 = hiddOrdinalSB1;
	}

	public String getHiddOrdinalSB1() {
		return hiddOrdinalSB1;
	}

	public void setHiddNombreSB2(String hiddNombreSB2) {
		this.hiddNombreSB2 = hiddNombreSB2;
	}

	public String getHiddNombreSB2() {
		return hiddNombreSB2;
	}

	public void setHiddLinkSB2(String hiddLinkSB2) {
		this.hiddLinkSB2 = hiddLinkSB2;
	}

	public String getHiddLinkSB2() {
		return hiddLinkSB2;
	}

	public void setHiddTrackerSB2(String hiddTrackerSB2) {
		this.hiddTrackerSB2 = hiddTrackerSB2;
	}

	public String getHiddTrackerSB2() {
		return hiddTrackerSB2;
	}

	public void setHiddImagenSB2(String hiddImagenSB2) {
		this.hiddImagenSB2 = hiddImagenSB2;
	}

	public String getHiddImagenSB2() {
		return hiddImagenSB2;
	}

	public void setHiddPrincipalSB2(String hiddPrincipalSB2) {
		this.hiddPrincipalSB2 = hiddPrincipalSB2;
	}

	public String getHiddPrincipalSB2() {
		return hiddPrincipalSB2;
	}

	public void setHiddOrdinalSB2(String hiddOrdinalSB2) {
		this.hiddOrdinalSB2 = hiddOrdinalSB2;
	}

	public String getHiddOrdinalSB2() {
		return hiddOrdinalSB2;
	}

	public void setHiddNombreSB3(String hiddNombreSB3) {
		this.hiddNombreSB3 = hiddNombreSB3;
	}

	public String getHiddNombreSB3() {
		return hiddNombreSB3;
	}

	public void setHiddLinkSB3(String hiddLinkSB3) {
		this.hiddLinkSB3 = hiddLinkSB3;
	}

	public String getHiddLinkSB3() {
		return hiddLinkSB3;
	}

	public void setHiddTrackerSB3(String hiddTrackerSB3) {
		this.hiddTrackerSB3 = hiddTrackerSB3;
	}

	public String getHiddTrackerSB3() {
		return hiddTrackerSB3;
	}

	public void setHiddImagenSB3(String hiddImagenSB3) {
		this.hiddImagenSB3 = hiddImagenSB3;
	}

	public String getHiddImagenSB3() {
		return hiddImagenSB3;
	}

	public void setHiddPrincipalSB3(String hiddPrincipalSB3) {
		this.hiddPrincipalSB3 = hiddPrincipalSB3;
	}

	public String getHiddPrincipalSB3() {
		return hiddPrincipalSB3;
	}

	public void setHiddOrdinalSB3(String hiddOrdinalSB3) {
		this.hiddOrdinalSB3 = hiddOrdinalSB3;
	}

	public String getHiddOrdinalSB3() {
		return hiddOrdinalSB3;
	}

	public void setHiddNombreSB4(String hiddNombreSB4) {
		this.hiddNombreSB4 = hiddNombreSB4;
	}

	public String getHiddNombreSB4() {
		return hiddNombreSB4;
	}

	public void setHiddLinkSB4(String hiddLinkSB4) {
		this.hiddLinkSB4 = hiddLinkSB4;
	}

	public String getHiddLinkSB4() {
		return hiddLinkSB4;
	}

	public void setHiddTrackerSB4(String hiddTrackerSB4) {
		this.hiddTrackerSB4 = hiddTrackerSB4;
	}

	public String getHiddTrackerSB4() {
		return hiddTrackerSB4;
	}

	public void setHiddImagenSB4(String hiddImagenSB4) {
		this.hiddImagenSB4 = hiddImagenSB4;
	}

	public String getHiddImagenSB4() {
		return hiddImagenSB4;
	}

	public void setHiddPrincipalSB4(String hiddPrincipalSB4) {
		this.hiddPrincipalSB4 = hiddPrincipalSB4;
	}

	public String getHiddPrincipalSB4() {
		return hiddPrincipalSB4;
	}

	public void setHiddOrdinalSB4(String hiddOrdinalSB4) {
		this.hiddOrdinalSB4 = hiddOrdinalSB4;
	}

	public String getHiddOrdinalSB4() {
		return hiddOrdinalSB4;
	}

	public void setHiddNombreSB5(String hiddNombreSB5) {
		this.hiddNombreSB5 = hiddNombreSB5;
	}

	public String getHiddNombreSB5() {
		return hiddNombreSB5;
	}

	public void setHiddLinkSB5(String hiddLinkSB5) {
		this.hiddLinkSB5 = hiddLinkSB5;
	}

	public String getHiddLinkSB5() {
		return hiddLinkSB5;
	}

	public void setHiddTrackerSB5(String hiddTrackerSB5) {
		this.hiddTrackerSB5 = hiddTrackerSB5;
	}

	public String getHiddTrackerSB5() {
		return hiddTrackerSB5;
	}

	public void setHiddImagenSB5(String hiddImagenSB5) {
		this.hiddImagenSB5 = hiddImagenSB5;
	}

	public String getHiddImagenSB5() {
		return hiddImagenSB5;
	}

	public void setHiddPrincipalSB5(String hiddPrincipalSB5) {
		this.hiddPrincipalSB5 = hiddPrincipalSB5;
	}

	public String getHiddPrincipalSB5() {
		return hiddPrincipalSB5;
	}

	public void setHiddOrdinalSB5(String hiddOrdinalSB5) {
		this.hiddOrdinalSB5 = hiddOrdinalSB5;
	}

	public String getHiddOrdinalSB5() {
		return hiddOrdinalSB5;
	}

	public void setHiddNombreSB6(String hiddNombreSB6) {
		this.hiddNombreSB6 = hiddNombreSB6;
	}

	public String getHiddNombreSB6() {
		return hiddNombreSB6;
	}

	public void setHiddLinkSB6(String hiddLinkSB6) {
		this.hiddLinkSB6 = hiddLinkSB6;
	}

	public String getHiddLinkSB6() {
		return hiddLinkSB6;
	}

	public void setHiddTrackerSB6(String hiddTrackerSB6) {
		this.hiddTrackerSB6 = hiddTrackerSB6;
	}

	public String getHiddTrackerSB6() {
		return hiddTrackerSB6;
	}

	public void setHiddImagenSB6(String hiddImagenSB6) {
		this.hiddImagenSB6 = hiddImagenSB6;
	}

	public String getHiddImagenSB6() {
		return hiddImagenSB6;
	}

	public void setHiddPrincipalSB6(String hiddPrincipalSB6) {
		this.hiddPrincipalSB6 = hiddPrincipalSB6;
	}

	public String getHiddPrincipalSB6() {
		return hiddPrincipalSB6;
	}

	public void setHiddOrdinalSB6(String hiddOrdinalSB6) {
		this.hiddOrdinalSB6 = hiddOrdinalSB6;
	}

	public String getHiddOrdinalSB6() {
		return hiddOrdinalSB6;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setTracker(String tracker) {
		this.tracker = tracker;
	}

	public String getTracker() {
		return tracker;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getImagen() {
		return imagen;
	}

	public void setIdPlan(String idPlan) {
		this.idPlan = idPlan;
	}

	public String getIdPlan() {
		return idPlan;
	}

	public void setHiddSubmit(String hiddSubmit) {
		this.hiddSubmit = hiddSubmit;
	}

	public String getHiddSubmit() {
		return hiddSubmit;
	}

	public void setHiddNombreVB1X(String hiddNombreVB1X) {
		this.hiddNombreVB1X = hiddNombreVB1X;
	}

	public String getHiddNombreVB1X() {
		return hiddNombreVB1X;
	}

	public void setHiddTrackerVB1X(String hiddTrackerVB1X) {
		this.hiddTrackerVB1X = hiddTrackerVB1X;
	}

	public String getHiddTrackerVB1X() {
		return hiddTrackerVB1X;
	}

	public void setHiddLinkVB1X(String hiddLinkVB1X) {
		this.hiddLinkVB1X = hiddLinkVB1X;
	}

	public String getHiddLinkVB1X() {
		return hiddLinkVB1X;
	}

	public void setHiddImagenVB1X(String hiddImagenVB1X) {
		this.hiddImagenVB1X = hiddImagenVB1X;
	}

	public String getHiddImagenVB1X() {
		return hiddImagenVB1X;
	}

	public void setHiddPrincipalVB1X(String hiddPrincipalVB1X) {
		this.hiddPrincipalVB1X = hiddPrincipalVB1X;
	}

	public String getHiddPrincipalVB1X() {
		return hiddPrincipalVB1X;
	}

	public void setHiddOrdinalVB1X(String hiddOrdinalVB1X) {
		this.hiddOrdinalVB1X = hiddOrdinalVB1X;
	}

	public String getHiddOrdinalVB1X() {
		return hiddOrdinalVB1X;
	}

	public void setHiddNombreVB2X(String hiddNombreVB2X) {
		this.hiddNombreVB2X = hiddNombreVB2X;
	}

	public String getHiddNombreVB2X() {
		return hiddNombreVB2X;
	}
}
