package cl.cencosud.ventaseguros.planes.struts.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class AdministrarPlanInicioAction extends Action {

    /**
     * Grupo Webpay dentro de base de datos parametros.
     */
    private final String GRUPO_WEBPAY = "WEBPAY";

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
        List < Rama > ramas = delegate.obtenerRamas();
        request.setAttribute("ramas", ramas);

        MantenedorClientesDelegate mantenedorDelegate =
            new MantenedorClientesDelegate();
        List < Map < String, ? > > lFormaPago =
            mantenedorDelegate.obtenerParametro(GRUPO_WEBPAY);
        request.setAttribute("formaPago", lFormaPago);
        
        List < Map < String, ? > > lCompannias =
            mantenedorDelegate.obtenerParametro("COMPANNIA");

        request.setAttribute("compannias", lCompannias);

        return mapping.findForward("continue");
    }
}
