package cl.cencosud.ventaseguros.fichasubcategoria.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class BuscarFichasSubcategoriaAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        FichaSubcategoriaDelegate delegate = new FichaSubcategoriaDelegate();
        int idRama = -1;
        int idSubcategoria = -1;
        
        try {

            String oIdRama = request.getParameter("idRama");
            String oIdSubcategoria = request.getParameter("idSubcategoria");
            
            if (oIdRama != null && !oIdRama.equals("")) {
                idRama = Integer.parseInt(oIdRama);
            }

            if (oIdSubcategoria != null && !oIdSubcategoria.equals("")) {
                idSubcategoria = Integer.parseInt(oIdSubcategoria);
            }

            List < Map < String, ? > > lista =
                delegate.obtenerFichasSubcategoria(idRama, idSubcategoria);
            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, lista);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;
    }
}
