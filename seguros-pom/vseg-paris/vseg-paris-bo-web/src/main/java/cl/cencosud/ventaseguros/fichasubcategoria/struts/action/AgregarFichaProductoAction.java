package cl.cencosud.ventaseguros.fichasubcategoria.struts.action;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Ficha;
import cl.cencosud.ventaseguros.common.LegalFicha;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.fichasubcategoria.struts.form.NuevaFichaProductoForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class AgregarFichaProductoAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        NuevaFichaProductoForm oForm = (NuevaFichaProductoForm) form;

        FichaSubcategoriaDelegate delegate = new FichaSubcategoriaDelegate();

        Ficha ficha = new Ficha();
        LegalFicha legalFicha= new LegalFicha();
        
        
        ficha.setId_subcategoria(oForm.getSubcategoria());
        ficha.setEs_subcategoria(oForm.getTipo());
        if (oForm.getHabilitar() == null) {
            oForm.setHabilitar(0);
        }
        
        if (oForm.getHabilita_texto_legal() == null || !oForm.getHabilita_texto_legal().equals("S")){
        	
        	oForm.setHabilita_texto_legal("N");
        }
        
        legalFicha.setActivo(oForm.getHabilita_texto_legal());
        
        ficha.setEstado_ficha(oForm.getHabilitar());

        ficha.setDescripcion_ficha(oForm.getDescripcion_ficha());
        ficha.setDescripcion_pagina(oForm.getDescripcion_pagina());

        ficha.setId_plan(oForm.getId_plan());
        ficha.setMonto_uf(0.0);
        ficha.setNombre_comercial(oForm.getNombre_comercial());
        
        if(oForm.getTexto_legal() == null || oForm.getTexto_legal().equals("")){       
            legalFicha.setTexto_legal("");
        }else{
            legalFicha.setTexto_legal(oForm.getTexto_legal());	
        }
        
        // Existe una sola ficha por Subcategoria.
        if (request.getSession().getAttribute("id_ficha") == null) {

            List < Map < String, ? > > listadoFicha =
                delegate.obtenerFichasSubcategoria(oForm.getRama(), oForm
                    .getSubcategoria());

            if (listadoFicha != null && !listadoFicha.isEmpty()) {

                //Buscar si existe para el plan.
                if (oForm.getId_plan() > 0) {
                    for (Iterator fichaCreada = listadoFicha.iterator(); fichaCreada
                        .hasNext();) {
                        Map < String, ? > map =
                            (Map < String, ? >) fichaCreada.next();
                        if (map.get("id_plan") != null) {
                            Number nIdPlan = (Number) map.get("id_plan");
                            String idPlan = nIdPlan.toString();
                            if (idPlan != null
                                && Integer.valueOf(idPlan) == ficha
                                    .getId_plan()) {
                                request.getSession().setAttribute(
                                    "id_ficha",
                                    Long.valueOf(listadoFicha.get(0).get(
                                        "id_ficha").toString()));
                            }
                        }
                    }
                } else {
                    request.getSession().setAttribute(
                        "id_ficha",
                        Long.valueOf(listadoFicha.get(0).get("id_ficha")
                            .toString()));
                }
            }

        }

        if (request.getSession().getAttribute("id_ficha") == null) {

            long idFicha = delegate.agregarFichaProducto(ficha);
            legalFicha.setId_ficha(idFicha);
            
            if(legalFicha.getTexto_legal() !=  null && !legalFicha.getTexto_legal().equals("")){          
            	delegate.agregarLegalFichaProducto(legalFicha);
            }
            
            request.getSession().setAttribute("id_ficha", idFicha);
                      
        } else {
            ficha.setId_ficha((Long) request.getSession().getAttribute(
                "id_ficha"));
            
            delegate.actualizarFichaProducto(ficha);
            
            legalFicha.setId_ficha(ficha.getId_ficha());
            LegalFicha legalFichaExiste = delegate.obtenerLegalFicha(ficha.getId_ficha());
            
            if(legalFichaExiste == null ){
            	 if(legalFicha.getTexto_legal() !=  null && !legalFicha.getTexto_legal().equals("")){          
                 	delegate.agregarLegalFichaProducto(legalFicha);
                 }
            }else{
            	delegate.actualizarLegalFichaProducto(legalFicha);
            }


        }

        return mapping.findForward("continue");
    }
}
