package cl.cencosud.ventaseguros.planes.struts.action;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.cencosud.ventaseguros.planes.struts.form.AdministrarPlanForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class AdministrarPlanGrabarAction extends Action {

    /**
     * Grupo Webpay dentro de base de datos parametros.
     */
    private final String GRUPO_WEBPAY = "WEBPAY";

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        AdministrarPlanForm oform = (AdministrarPlanForm) form;

        FichaSubcategoriaDelegate fichadelegate =
            new FichaSubcategoriaDelegate();

        PlanLeg plan = fichadelegate.obtenerDatosPlan(oform.getId_plan());

        //Grabar datos generales.
        MantenedorClientesDelegate mantenedorDelegate =
            new MantenedorClientesDelegate();
        List < Map < String, ? > > lFormaPago =
            mantenedorDelegate.obtenerParametro(GRUPO_WEBPAY);
        String forma_pago[] = oform.getForma_pago();
        String sFormaPago = "";
        for (int i = 0; i < lFormaPago.size(); i++) {
            Map < String, ? > forma = lFormaPago.get(i);
            String estado = "0";
            if (forma_pago != null) {
                String val = (String) forma.get("valor");
                if (val != null && Arrays.asList(forma_pago).contains(val)) {
                    estado = "1";
                }
            }
            sFormaPago += estado;
        }
        plan.setFormaPago(sFormaPago);

        String fecha_vigencia = oform.getFecha_vigencia();
        int max_dia_ini_vig =
            ((fecha_vigencia != null && !fecha_vigencia.equals("")) ? Integer
                .parseInt(fecha_vigencia) : 0);
        plan.setMaxDiaIniVig(max_dia_ini_vig);

        plan.setCodigo_cia_leg(oform.getCompannia());
        List < Map < String, ? > > info_compannia =
            mantenedorDelegate.obtenerParametro("COMPANNIA", plan
                .getCodigo_cia_leg());

        if (info_compannia != null && info_compannia.size() > 0) {
            plan.setCompannia((String) info_compannia.get(0).get("nombre"));
        }

        int rama = oform.getRama();

        RestriccionVida restricciones = new RestriccionVida();
        if (rama == 3) {
            //Grabar datos de vida.
            String asegurados_adicionales = oform.getAsegurados_adicionales();
            int grupoFamiliar =
                (asegurados_adicionales != null ? Integer
                    .parseInt(asegurados_adicionales) : 0);
            restricciones.setRequiereGrupoFamiliar(grupoFamiliar);

            String beneficiarios = oform.getBeneficiarios();
            boolean bBeneficiarios =
                (beneficiarios != null ? Boolean.parseBoolean(beneficiarios)
                    : false);
            restricciones.setRequiereBeneficiarios(bBeneficiarios);

            restricciones.setIdPlan(plan.getId_plan_vseg());
        } else if (rama == 1) {
            //Grabar datos de vehiculo.
            String inspeccion = oform.getInspeccion();
            int iInspeccion =
                ((inspeccion != null && !inspeccion.equals("")) ? Integer
                    .parseInt(inspeccion) : 0);
            plan.setInspeccionVehi(iInspeccion);

            String auto_nuevo = oform.getAuto_nuevo();
            int iAutoNuevo =
                (auto_nuevo != null ? Integer.parseInt(auto_nuevo) : 0);
            plan.setSolicitaAutoNuevo(iAutoNuevo);

            String factura = oform.getFactura();
            int iFactura = (factura != null ? Integer.parseInt(factura) : 0);
            plan.setSolicitaFactura(iFactura);
        }

        String tipo_pago = oform.getTipo_pago();
        boolean bPrimaUnica =
            ((tipo_pago != null && tipo_pago.equals("2")) ? true : false);

        fichadelegate.administrarPlan(plan, restricciones, bPrimaUnica, rama);

        return mapping.findForward("continue");
    }
}
