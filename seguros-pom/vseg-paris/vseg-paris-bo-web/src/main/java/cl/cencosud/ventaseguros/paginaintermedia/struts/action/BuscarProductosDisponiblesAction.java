package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class BuscarProductosDisponiblesAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
        try {
            List < ProductoLeg > productos = new ArrayList < ProductoLeg >();
            if (request.getParameter("idSubcategoria") != null
                && !request.getParameter("idSubcategoria").equals("")) {
                int idSubcategoria =
                    Integer.parseInt((String) request
                        .getParameter("idSubcategoria"));

                String nombre = (String) request.getParameter("nombre");

                productos =
                    delegate.obtenerListaProductos(idSubcategoria, false,
                        nombre);
            }
            StringBuffer oJson = new StringBuffer();
            oJson.append("{\"results\": [ ");
            for (ProductoLeg productoLeg : productos) {
                oJson.append("{\"id\":\"");
                oJson.append(productoLeg.getId_producto());
                oJson.append("\",\"value\":\"");
                oJson.append(productoLeg.getNombre());
                oJson.append("\"},");
            }

            oJson.deleteCharAt(oJson.length() - 1);
            oJson.append("]}");

            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("application/x-json");
            PrintWriter out;

            out = response.getWriter();

            out.print(oJson);

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;

    }
}
