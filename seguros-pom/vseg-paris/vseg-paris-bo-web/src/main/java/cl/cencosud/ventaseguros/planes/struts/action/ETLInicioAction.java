package cl.cencosud.ventaseguros.planes.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg;
import cl.cencosud.ventaseguros.delegate.MantenedorETLDelegate;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ETLInicioAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

//        MantenedorETLDelegate delegate = new MantenedorETLDelegate();
//        TipoLeg[] tipos = delegate.obtenerTipoLeg();
//
//        request.setAttribute("tipos", tipos);
//
//        SubtipoLeg[] subtipos = delegate.obtenerSubTipoLeg("1");
//
//        ProductoLeg[] productos = delegate.obtenerProductoLeg("1", "01");
//
//        PlanLeg[] planes = delegate.obtenerPlanLeg("1", "01", "06");

        return mapping.findForward("continue");
    }
}
