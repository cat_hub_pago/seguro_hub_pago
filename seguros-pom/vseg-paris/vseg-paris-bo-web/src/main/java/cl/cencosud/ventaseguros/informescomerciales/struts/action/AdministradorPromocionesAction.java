package cl.cencosud.ventaseguros.informescomerciales.struts.action;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.InformesComercialesDelegate;
import cl.cencosud.ventaseguros.informescomerciales.struts.form.InformeComercialTarjetasForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdministradorPromocionesAction extends Action{

/**
* @param args
*/
public ActionForward execute(ActionMapping mapping, ActionForm form,
	        HttpServletRequest request, HttpServletResponse response)
	        throws Exception {
	
	HttpSession session = request.getSession();

	InformeComercialTarjetasForm oForm =
        (InformeComercialTarjetasForm) form;
	
	InformesComercialesDelegate oDelegate =
        new InformesComercialesDelegate();
	
	SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
	String fechaDesde =
        oForm.getDatos().get("fechaDesde") == null ? "" : oForm.getDatos()
            .get("fechaDesde");
    String fechaHasta =
        oForm.getDatos().get("fechaHasta") == null ? "" : oForm.getDatos()
            .get("fechaHasta");
    String fechaDesde2 =
        oForm.getDatos().get("fechaDesde2") == null ? "" : oForm.getDatos()
            .get("fechaDesde2");
    String fechaHasta2 =
        oForm.getDatos().get("fechaHasta2") == null ? "" : oForm.getDatos()
            .get("fechaHasta2");
    String fechaDesde3 =
        oForm.getDatos().get("fechaDesde3") == null ? "" : oForm.getDatos()
            .get("fechaDesde3");
    String fechaHasta3 =
        oForm.getDatos().get("fechaHasta3") == null ? "" : oForm.getDatos()
            .get("fechaHasta3");
    String fechaDesde4 =
        oForm.getDatos().get("fechaDesde4") == null ? "" : oForm.getDatos()
            .get("fechaDesde4");
    String fechaHasta4 =
        oForm.getDatos().get("fechaHasta4") == null ? "" : oForm.getDatos()
            .get("fechaHasta4");
    String fechaDesdeVB1 =
        oForm.getDatos().get("fechaDesdeVB1") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB1");
    String fechaHastaVB1 =
        oForm.getDatos().get("fechaHastaVB1") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB1");
    String fechaDesdeVB2 =
        oForm.getDatos().get("fechaDesdeVB2") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB2");
    String fechaHastaVB2 =
        oForm.getDatos().get("fechaHastaVB2") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB2");
    String fechaDesdeHB1 =
        oForm.getDatos().get("fechaDesdeHB1") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB1");
    String fechaHastaHB1 =
        oForm.getDatos().get("fechaHastaHB1") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB1");
    String fechaDesdeHB2 =
        oForm.getDatos().get("fechaDesdeHB2") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB2");
    String fechaHastaHB2 =
        oForm.getDatos().get("fechaHastaHB2") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB2");
    String fechaDesdeVIB1 =
        oForm.getDatos().get("fechaDesdeVIB1") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB1");
    String fechaHastaVIB1 =
        oForm.getDatos().get("fechaHastaVIB1") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB1");
    String fechaDesdeVIB2 =
        oForm.getDatos().get("fechaDesdeVIB2") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB2");
    String fechaHastaVIB2 =
        oForm.getDatos().get("fechaHastaVIB2") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB2");
    
	String principal  = oForm.getDatos().get("hiddPrincipal");

    //String respuesta = "ingresada";
    if (principal != null){
    String tipoBanner = principal;
    if (principal.equalsIgnoreCase("1")){ 
    
            String posicion = oForm.getDatos().get("hiddPosicion"); 
            if (posicion.equals("1")){
            	if (fechaHasta.equals("")){
              		 fechaHasta = "02/01/2001";
              	 };
              	 if (fechaDesde.equals("")){
              		 fechaDesde = "01/01/2001";
              	 }
                if (!fechaDesde.equals("") && !fechaHasta.equals("")) {
                   if (!formatoFecha.parse(fechaHasta).before(
                   formatoFecha.parse(fechaDesde))) {
            	String linkConocer = null;
                String linkCotizar = null;
                String rama = null;
                String nombre = oForm.getDatos().get("hiddNombre");
                if (nombre.equals("")){
                	nombre = "-";
                }
                String link = oForm.getDatos().get("hiddLink");
                if (link.equals("")){
                	link = "-";
                }
                String tracker = oForm.getDatos().get("hiddTracker");
                if (tracker.equals("")){
                	tracker = "-";
                }
                String imagen = oForm.getDatos().get("hiddImagen");
                if (imagen.equals("")){
                	imagen = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlan");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHasta.equals("")){
             		fechaHasta = "01/01/2001";
             	}
             	if (fechaDesde.equals("")){
             		fechaDesde = "01/01/2001";
             	}
                principal = "11";
            	String tipo = "PPBP";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, posicion, nombre, link, tracker, imagen, 
            	formatoFecha.parse(fechaDesde), formatoFecha.parse(fechaHasta),tipoBanner,rama, linkConocer, linkCotizar,idPlan);
                   }
                }
            } else if (posicion.equals("2")){
            	if (fechaHasta2.equals("")){
              		 fechaHasta2 = "02/01/2001";
              	 };
              	 if (fechaDesde2.equals("")){
              		 fechaDesde2 = "01/01/2001";
              	 }
               if (!fechaDesde2.equals("") && !fechaHasta2.equals("")) {
                   if (!formatoFecha.parse(fechaHasta2).before(
                   formatoFecha.parse(fechaDesde2))) {
            	String linkConocer = null;
                String linkCotizar = null;
                String rama = null;
                String nombre = oForm.getDatos().get("hiddNombre2");
                if (nombre.equals("")){
                	nombre = "-";
                }
                String link = oForm.getDatos().get("hiddLink2");
                if (link.equals("")){
                	link = "-";
                }
                String tracker = oForm.getDatos().get("hiddTracker2");
                if (tracker.equals("")){
                	tracker = "-";
                }
                String imagen = oForm.getDatos().get("hiddImagen2");
                if (imagen.equals("")){
                	imagen = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlan2");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHasta2.equals("")){
             		fechaHasta2 = "01/01/2001";
             	}
             	if (fechaDesde2.equals("")){
             		fechaDesde2 = "01/01/2001";
             	}
                principal = "11";
            	String tipo = "PPBP";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, posicion, nombre, link, tracker, imagen, 
            	formatoFecha.parse(fechaDesde2), formatoFecha.parse(fechaHasta2),tipoBanner,rama, linkConocer, linkCotizar,idPlan);
                   }
               }
            } else if (posicion.equals("3")){
            	if (fechaHasta3.equals("")){
              		 fechaHasta3 = "02/01/2001";
              	 };
              	 if (fechaDesde3.equals("")){
              		 fechaDesde3 = "01/01/2001";
              	 }
               if (!fechaDesde3.equals("") && !fechaHasta3.equals("")) {
                   if (!formatoFecha.parse(fechaHasta3).before(
                   formatoFecha.parse(fechaDesde3))) {
            	String linkConocer = null;
                String linkCotizar = null;
                String rama = null;
                String nombre = oForm.getDatos().get("hiddNombre3");
                if (nombre.equals("")){
                	nombre = "-";
                }
                String link = oForm.getDatos().get("hiddLink3");
                if (link.equals("")){
                	link = "-";
                }
                String tracker = oForm.getDatos().get("hiddTracker3");
                if (tracker.equals("")){
                	tracker = "-";
                }
                String imagen = oForm.getDatos().get("hiddImagen3");
                if (imagen.equals("")){
                	imagen = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlan3");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHasta3.equals("")){
             		fechaHasta3 = "01/01/2001";
             	}
             	if (fechaDesde3.equals("")){
             		fechaDesde3 = "01/01/2001";
             	}
                principal = "11";
            	String tipo = "PPBP";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, posicion, nombre, link, tracker, imagen, 
            	formatoFecha.parse(fechaDesde3), formatoFecha.parse(fechaHasta3),tipoBanner,rama, linkConocer, linkCotizar,idPlan);
                   }
               }
            } else if (posicion.equals("4")){
            	if (fechaHasta4.equals("")){
              		 fechaHasta4 = "02/01/2001";
              	 };
              	 if (fechaDesde4.equals("")){
              		 fechaDesde4 = "01/01/2001";
              	 }
               if (!fechaDesde4.equals("") && !fechaHasta4.equals("")) {
                   if (!formatoFecha.parse(fechaHasta4).before(
                   formatoFecha.parse(fechaDesde4))) {
            	String linkConocer = null;
                String linkCotizar = null;
                String rama = null;
                String nombre = oForm.getDatos().get("hiddNombre4");
                if (nombre.equals("")){
                	nombre = "-";
                }
                String link = oForm.getDatos().get("hiddLink4");
                if (link.equals("")){
                	link = "-";
                }
                String tracker = oForm.getDatos().get("hiddTracker4");
                if (tracker.equals("")){
                	tracker = "-";
                }
                String imagen = oForm.getDatos().get("hiddImagen4");
                if (imagen.equals("")){
                	imagen = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlan4");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHasta4.equals("")){
             		fechaHasta4 = "01/01/2001";
             	}
             	if (fechaDesde4.equals("")){
             		fechaDesde4 = "01/01/2001";
             	}
                principal = "11";
            	String tipo = "PPBP";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, posicion, nombre, link, tracker, imagen, 
            	formatoFecha.parse(fechaDesde4), formatoFecha.parse(fechaHasta4),tipoBanner,rama, linkConocer, linkCotizar,idPlan);
                   }
               }
            }
            
    }
    else if (principal.equalsIgnoreCase("2")){
    	if (fechaHastaVB1.equals("")){
     		fechaHastaVB1 = "03/01/2001";
     	}
     	if (fechaDesdeVB1.equals("")){
     		fechaDesdeVB1 = "01/01/2001";
     	}
    	if (!fechaDesdeVB1.equals("") && !fechaHastaVB1.equals("")) {
            if (!formatoFecha.parse(fechaHastaVB1).before(
            formatoFecha.parse(fechaDesdeVB1))) {
            	String ordinalVB1 = oForm.getDatos().get("hiddOrdinalVB1"); 
            	String nombreVB1 = oForm.getDatos().get("hiddNombreVB1");
                if (nombreVB1.equals("")){
                	nombreVB1 = "-";
                }
                String linkVerVB1 = oForm.getDatos().get("hiddLinkVerBasesVB1");
                if (linkVerVB1.equals("")){
                	linkVerVB1 = "-";
                }
                String linkConocerVB1 = oForm.getDatos().get("hiddLinkConocerVB1");
                if (linkConocerVB1.equals("")){
                	linkConocerVB1 = "-";
                }
                String linkCotizarVB1 = oForm.getDatos().get("hiddLinkCotizarVB1");
                if (linkCotizarVB1.equals("")){
                	linkCotizarVB1 = "-";
                }
                String trackerVB1 = oForm.getDatos().get("hiddTrackerVB1");
                if (trackerVB1.equals("")){
                	trackerVB1 = "-";
                }
                String imagenVB1 = oForm.getDatos().get("hiddImagenVB1");
                if (imagenVB1.equals("")){
                	imagenVB1 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVB1");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVB1.equals("")){
             		fechaHastaVB1 = "03/01/2001";
             	}
             	if (fechaDesdeVB1.equals("")){
             		fechaDesdeVB1 = "01/01/2001";
             	}
                String rama = "1";
            	principal = "21";
            	String tipo = "PPBS";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, ordinalVB1, nombreVB1, linkVerVB1, trackerVB1, imagenVB1,
            			formatoFecha.parse(fechaDesdeVB1), formatoFecha.parse(fechaHastaVB1),tipoBanner, rama,linkConocerVB1, linkCotizarVB1,idPlan);
            }
    	}
    	if (fechaHastaVB2.equals("")){
     		fechaHastaVB2 = "03/01/2001";
     	}
     	if (fechaDesdeVB2.equals("")){
     		fechaDesdeVB2 = "01/01/2001";
     	}
    	if (!fechaDesdeVB2.equals("") && !fechaHastaVB2.equals("")) {
            if (!formatoFecha.parse(fechaHastaVB2).before(
            formatoFecha.parse(fechaDesdeVB2))) {
            	String ordinalVB2 = oForm.getDatos().get("hiddOrdinalVB2"); 
            	String nombreVB2 = oForm.getDatos().get("hiddNombreVB2");
                if (nombreVB2.equals("")){
                	nombreVB2 = "-";
                }
                String linkVerVB2 = oForm.getDatos().get("hiddLinkVerBasesVB2");
                if (linkVerVB2.equals("")){
                	linkVerVB2 = "-";
                }
                String linkConocerVB2 = oForm.getDatos().get("hiddLinkConocerVB2");
                if (linkConocerVB2.equals("")){
                	linkConocerVB2 = "-";
                }
                String linkCotizarVB2 = oForm.getDatos().get("hiddLinkCotizarVB2");
                if (linkCotizarVB2.equals("")){
                	linkCotizarVB2 = "-";
                }
                String trackerVB2 = oForm.getDatos().get("hiddTrackerVB2");
                if (trackerVB2.equals("")){
                	trackerVB2 = "-";
                }
                String imagenVB2 = oForm.getDatos().get("hiddImagenVB2");
                if (imagenVB2.equals("")){
                	imagenVB2 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVB2");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVB2.equals("")){
             		fechaHastaVB2 = "03/01/2001";
             	}
             	if (fechaDesdeVB2.equals("")){
             		fechaDesdeVB2 = "01/01/2001";
             	}
                String rama = "1";
            	principal = "22";
            	String tipo = "PPBS";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, ordinalVB2, nombreVB2, linkVerVB2, trackerVB2, imagenVB2,
            			formatoFecha.parse(fechaDesdeVB2), formatoFecha.parse(fechaHastaVB2),tipoBanner,rama,linkConocerVB2, linkCotizarVB2,idPlan);
            }
    	} 
    	if (fechaHastaHB1.equals("")){
     		fechaHastaHB1 = "03/01/2001";
     	}
     	if (fechaDesdeHB1.equals("")){
     		fechaDesdeHB1 = "01/01/2001";
     	}
    	if (!fechaDesdeHB1.equals("") && !fechaHastaHB1.equals("")) {
            if (!formatoFecha.parse(fechaHastaHB1).before(
            formatoFecha.parse(fechaDesdeHB1))) {
            	String ordinalHB1 = oForm.getDatos().get("hiddOrdinalHB1"); 
            	String nombreHB1 = oForm.getDatos().get("hiddNombreHB1");
                if (nombreHB1.equals("")){
                	nombreHB1 = "-";
                }
                String linkVerHB1 = oForm.getDatos().get("hiddLinkVerBasesHB1");
                if (linkVerHB1.equals("")){
                	linkVerHB1 = "-";
                }
                String linkConocerHB1 = oForm.getDatos().get("hiddLinkConocerHB1");
                if (linkConocerHB1.equals("")){
                	linkConocerHB1 = "-";
                }
                String linkCotizarHB1 = oForm.getDatos().get("hiddLinkCotizarHB1");
                if (linkCotizarHB1.equals("")){
                	linkCotizarHB1 = "-";
                }
                String trackerHB1 = oForm.getDatos().get("hiddTrackerHB1");
                if (trackerHB1.equals("")){
                	trackerHB1 = "-";
                }
                String imagenHB1 = oForm.getDatos().get("hiddImagenHB1");
                if (imagenHB1.equals("")){
                	imagenHB1 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB1");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB1.equals("")){
             		fechaHastaHB1 = "03/01/2001";
             	}
             	if (fechaDesdeHB1.equals("")){
             		fechaDesdeHB1 = "01/01/2001";
             	}
                String rama = "2";
            	principal = "23";
            	String tipo = "PPBS";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, ordinalHB1, nombreHB1, linkVerHB1, trackerHB1, imagenHB1,
            			formatoFecha.parse(fechaDesdeHB1), formatoFecha.parse(fechaHastaHB1),tipoBanner,rama,linkConocerHB1, linkCotizarHB1,idPlan);
            }
    	} 
    	if (fechaHastaHB2.equals("")){
     		fechaHastaHB2 = "03/01/2001";
     	}
     	if (fechaDesdeHB2.equals("")){
     		fechaDesdeHB2 = "01/01/2001";
     	}
    	if (!fechaDesdeHB2.equals("") && !fechaHastaHB2.equals("")) {
            if (!formatoFecha.parse(fechaHastaHB2).before(
            formatoFecha.parse(fechaDesdeHB2))) {
            	String ordinalHB2 = oForm.getDatos().get("hiddOrdinalHB2"); 
            	String nombreHB2 = oForm.getDatos().get("hiddNombreHB2");
                if (nombreHB2.equals("")){
                	nombreHB2 = "-";
                }
                String linkVerHB2 = oForm.getDatos().get("hiddLinkVerBasesHB2");
                if (linkVerHB2.equals("")){
                	linkVerHB2 = "-";
                }
                String linkConocerHB2 = oForm.getDatos().get("hiddLinkConocerHB2");
                if (linkConocerHB2.equals("")){
                	linkConocerHB2 = "-";
                }
                String linkCotizarHB2 = oForm.getDatos().get("hiddLinkCotizarHB2");
                if (linkCotizarHB2.equals("")){
                	linkCotizarHB2 = "-";
                }
                String trackerHB2 = oForm.getDatos().get("hiddTrackerHB2");
                if (trackerHB2.equals("")){
                	trackerHB2 = "-";
                }
                String imagenHB2 = oForm.getDatos().get("hiddImagenHB2");
                if (imagenHB2.equals("")){
                	imagenHB2 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB2");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB2.equals("")){
             		fechaHastaHB2 = "03/01/2001";
             	}
             	if (fechaDesdeHB2.equals("")){
             		fechaDesdeHB2 = "01/01/2001";
             	}
                String rama = "2";
            	principal = "24";
            	String tipo = "PPBS";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, ordinalHB2, nombreHB2, linkVerHB2, trackerHB2, imagenHB2,
            			formatoFecha.parse(fechaDesdeHB2), formatoFecha.parse(fechaHastaHB2),tipoBanner,rama,linkConocerHB2, linkCotizarHB2,idPlan);
            }
    	} 
    	if (fechaHastaVIB1.equals("")){
     		fechaHastaVIB1 = "03/01/2001";
     	}
     	if (fechaDesdeVIB1.equals("")){
     		fechaDesdeVIB1 = "01/01/2001";
     	}
    	if (!fechaDesdeVIB1.equals("") && !fechaHastaVIB1.equals("")) {
            if (!formatoFecha.parse(fechaHastaVIB1).before(
            formatoFecha.parse(fechaDesdeVIB1))) {
            	String ordinalVIB1 = oForm.getDatos().get("hiddOrdinalVIB1"); 
            	String nombreVIB1 = oForm.getDatos().get("hiddNombreVIB1");
                if (nombreVIB1.equals("")){
                	nombreVIB1 = "-";
                }
                String linkVerVIB1 = oForm.getDatos().get("hiddLinkVerBasesVIB1");
                if (linkVerVIB1.equals("")){
                	linkVerVIB1 = "-";
                }
                String linkConocerVIB1 = oForm.getDatos().get("hiddLinkConocerVIB1");
                if (linkConocerVIB1.equals("")){
                	linkConocerVIB1 = "-";
                }
                String linkCotizarVIB1 = oForm.getDatos().get("hiddLinkCotizarVIB1");
                if (linkCotizarVIB1.equals("")){
                	linkCotizarVIB1 = "-";
                }
                String trackerVIB1 = oForm.getDatos().get("hiddTrackerVIB1");
                if (trackerVIB1.equals("")){
                	trackerVIB1 = "-";
                }
                String imagenVIB1 = oForm.getDatos().get("hiddImagenVIB1");
                if (imagenVIB1.equals("")){
                	imagenVIB1 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVIB1");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVIB1.equals("")){
             		fechaHastaVIB1 = "03/01/2001";
             	}
             	if (fechaDesdeVIB1.equals("")){
             		fechaDesdeVIB1 = "01/01/2001";
             	}
                String rama = "3";
            	principal = "25";
            	String tipo = "PPBS";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, ordinalVIB1, nombreVIB1, linkVerVIB1, trackerVIB1, imagenVIB1,
            			formatoFecha.parse(fechaDesdeVIB1), formatoFecha.parse(fechaHastaVIB1),tipoBanner,rama,linkConocerVIB1, linkCotizarVIB1,idPlan);
            }
    	} 
    	if (fechaHastaVIB2.equals("")){
      		fechaHastaVIB2 = "03/01/2001";
      	}
      	if (fechaDesdeVIB2.equals("")){
      		fechaDesdeVIB2 = "01/01/2001";
      	}
    	if (!fechaDesdeVIB2.equals("") && !fechaHastaVIB2.equals("")) {
            if (!formatoFecha.parse(fechaHastaVIB2).before(
            formatoFecha.parse(fechaDesdeVIB2))) {
            	 String ordinalVIB2 = oForm.getDatos().get("hiddOrdinalVIB2"); 
            	 String nombreVIB2 = oForm.getDatos().get("hiddNombreVIB2");
                 if (nombreVIB2.equals("")){
                 	nombreVIB2 = "-";
                 }
                 String linkVerVIB2 = oForm.getDatos().get("hiddLinkVerBasesVIB2");
                 if (linkVerVIB2.equals("")){
                 	linkVerVIB2 = "-";
                 }
                 String linkConocerVIB2 = oForm.getDatos().get("hiddLinkConocerVIB2");
                 if (linkConocerVIB2.equals("")){
                 	linkConocerVIB2 = "-";
                 }
                 String linkCotizarVIB2 = oForm.getDatos().get("hiddLinkCotizarVIB2");
                 if (linkCotizarVIB2.equals("")){
                 	linkCotizarVIB2 = "-";
                 }
                 String trackerVIB2 = oForm.getDatos().get("hiddTrackerVIB2");
                 if (trackerVIB2.equals("")){
                 	trackerVIB2 = "-";
                 }
                 String imagenVIB2 = oForm.getDatos().get("hiddImagenVIB2");
                 if (imagenVIB2.equals("")){
                 	imagenVIB2 = "-";
                 }
                 String idPlan = oForm.getDatos().get("hiddIdPlanVIB2");
                 if (idPlan.equals("")){
                 	idPlan = "0";
                 }
                 
              	if (fechaHastaVIB2.equals("")){
              		fechaHastaVIB2 = "03/01/2001";
              	}
              	if (fechaDesdeVIB2.equals("")){
              		fechaDesdeVIB2 = "01/01/2001";
              	}
            	 String rama = "3";
            	principal = "26";
            	String tipo = "PPBS";
            	oDelegate.ingresarPromocionPrincipal(principal, tipo, ordinalVIB2, nombreVIB2, linkVerVIB2, trackerVIB2, imagenVIB2,
            			formatoFecha.parse(fechaDesdeVIB2), formatoFecha.parse(fechaHastaVIB2),tipoBanner,rama, linkConocerVIB2, linkCotizarVIB2,idPlan);
            }
    	} 
    }
    } else {
    	int i = 0;
    	List<Map<String, Object>> datosPromo = oDelegate.obtenerPromocionesBOPrincipal();
    	
    	for (i=0; i<datosPromo.size(); i++){
    		//System.out.println(datosPromo.get(i).get("tipo"));
    		if (!datosPromo.get(i).isEmpty()){
    		if (datosPromo.get(i).get("tipo").equals("PPBP")){
    			if (datosPromo.get(i).get("ordinal").equals("1")){
	    			if (datosPromo.get(i).get("idPlan") == null){
	    				request.setAttribute("idPlan","");
	    			}else {
	    				request.setAttribute("idPlan", datosPromo.get(i).get("idPlan"));	
	    			};
	    			if (datosPromo.get(i).get("nombre") == null){
	    				request.setAttribute("nombre","");
	    			}else {
	    				request.setAttribute("nombre", datosPromo.get(i).get("nombre"));	
	    			};
	    			if (datosPromo.get(i).get("linkVer") == null){
	    				request.setAttribute("link","");
	    			}else {
	    				request.setAttribute("link", datosPromo.get(i).get("linkVer"));	
	    			};
	    			if (datosPromo.get(i).get("tracker") == null){
	    				request.setAttribute("tracker","");
	    			}else {
	    				request.setAttribute("tracker", datosPromo.get(i).get("tracker"));	
	    			};
	    			if (datosPromo.get(i).get("imagen") == null){
	    				request.setAttribute("imagen","");
	    			}else {
	    				request.setAttribute("imagen", datosPromo.get(i).get("imagen"));	
	    			};
    			} else if (datosPromo.get(i).get("ordinal").equals("2")){
    				if (datosPromo.get(i).get("idPlan") == null){
	    				request.setAttribute("idPlan2","");
	    			}else {
	    				request.setAttribute("idPlan2", datosPromo.get(i).get("idPlan"));	
	    			};
	    			if (datosPromo.get(i).get("nombre") == null){
	    				request.setAttribute("nombre2","");
	    			}else {
	    				request.setAttribute("nombre2", datosPromo.get(i).get("nombre"));	
	    			};
	    			if (datosPromo.get(i).get("linkVer") == null){
	    				request.setAttribute("link2","");
	    			}else {
	    				request.setAttribute("link2", datosPromo.get(i).get("linkVer"));	
	    			};
	    			if (datosPromo.get(i).get("tracker") == null){
	    				request.setAttribute("tracker2","");
	    			}else {
	    				request.setAttribute("tracker2", datosPromo.get(i).get("tracker"));	
	    			};
	    			if (datosPromo.get(i).get("imagen") == null){
	    				request.setAttribute("imagen2","");
	    			}else {
	    				request.setAttribute("imagen2", datosPromo.get(i).get("imagen"));	
	    			};
    			} else if (datosPromo.get(i).get("ordinal").equals("3")){
    				if (datosPromo.get(i).get("idPlan") == null){
	    				request.setAttribute("idPlan3","");
	    			}else {
	    				request.setAttribute("idPlan3", datosPromo.get(i).get("idPlan"));	
	    			};
	    			if (datosPromo.get(i).get("nombre") == null){
	    				request.setAttribute("nombre3","");
	    			}else {
	    				request.setAttribute("nombre3", datosPromo.get(i).get("nombre"));	
	    			};
	    			if (datosPromo.get(i).get("linkVer") == null){
	    				request.setAttribute("link3","");
	    			}else {
	    				request.setAttribute("link3", datosPromo.get(i).get("linkVer"));	
	    			};
	    			if (datosPromo.get(i).get("tracker") == null){
	    				request.setAttribute("tracker3","");
	    			}else {
	    				request.setAttribute("tracker3", datosPromo.get(i).get("tracker"));	
	    			};
	    			if (datosPromo.get(i).get("imagen") == null){
	    				request.setAttribute("imagen3","");
	    			}else {
	    				request.setAttribute("imagen3", datosPromo.get(i).get("imagen"));	
	    			};
    			} else if (datosPromo.get(i).get("ordinal").equals("4")){
    				if (datosPromo.get(i).get("idPlan") == null){
	    				request.setAttribute("idPlan4","");
	    			}else {
	    				request.setAttribute("idPlan4", datosPromo.get(i).get("idPlan"));	
	    			};
	    			if (datosPromo.get(i).get("nombre") == null){
	    				request.setAttribute("nombre4","");
	    			}else {
	    				request.setAttribute("nombre4", datosPromo.get(i).get("nombre"));	
	    			};
	    			if (datosPromo.get(i).get("linkVer") == null){
	    				request.setAttribute("link4","");
	    			}else {
	    				request.setAttribute("link4", datosPromo.get(i).get("linkVer"));	
	    			};
	    			if (datosPromo.get(i).get("tracker") == null){
	    				request.setAttribute("tracker4","");
	    			}else {
	    				request.setAttribute("tracker4", datosPromo.get(i).get("tracker"));	
	    			};
	    			if (datosPromo.get(i).get("imagen") == null){
	    				request.setAttribute("imagen4","");
	    			}else {
	    				request.setAttribute("imagen4", datosPromo.get(i).get("imagen"));	
	    			};
    			}
    		} else if (datosPromo.get(i).get("tipo").equals("PPBS")){
    			if (datosPromo.get(i).get("id").equals("21")){
    				if (datosPromo.get(i).get("idPlan") == null){
        				request.setAttribute("idPlanVB1","");
        			}else {
        				request.setAttribute("idPlanVB1", datosPromo.get(i).get("idPlan"));	
        			};
        			if (datosPromo.get(i).get("nombre") == null){
        				request.setAttribute("nombreVB1","");
        			}else {
        				request.setAttribute("nombreVB1", datosPromo.get(i).get("nombre"));	
        			};
        			if (datosPromo.get(i).get("linkCotizar") == null){
        				request.setAttribute("linkCotizarVB1","");
        			}else {
        				request.setAttribute("linkCotizarVB1", datosPromo.get(i).get("linkCotizar"));	
        			};
        			if (datosPromo.get(i).get("tracker") == null){
        				request.setAttribute("trackerVB1","");
        			}else {
        				request.setAttribute("trackerVB1", datosPromo.get(i).get("tracker"));	
        			};
        			if (datosPromo.get(i).get("imagen") == null){
        				request.setAttribute("imagenVB1","");
        			}else {
        				request.setAttribute("imagenVB1", datosPromo.get(i).get("imagen"));	
        			};
        			if (datosPromo.get(i).get("linkVer") == null){
        				request.setAttribute("linkVerBasesVB1","");
        			}else {
        				request.setAttribute("linkVerBasesVB1", datosPromo.get(i).get("linkVer"));	
        			};
        			if (datosPromo.get(i).get("linkConocer") == null){
        				request.setAttribute("linkConocerVB1","");
        			}else {
        				request.setAttribute("linkConocerVB1", datosPromo.get(i).get("linkConocer"));	
        			};
    			} else if (datosPromo.get(i).get("id").equals("22")){
    				if (datosPromo.get(i).get("idPlan") == null){
        				request.setAttribute("idPlanVB2","");
        			}else {
        				request.setAttribute("idPlanVB2", datosPromo.get(i).get("idPlan"));	
        			};
        			if (datosPromo.get(i).get("nombre") == null){
        				request.setAttribute("nombreVB2","");
        			}else {
        				request.setAttribute("nombreVB2", datosPromo.get(i).get("nombre"));	
        			};
        			if (datosPromo.get(i).get("linkCotizar") == null){
        				request.setAttribute("linkCotizarVB2","");
        			}else {
        				request.setAttribute("linkCotizarVB2", datosPromo.get(i).get("linkCotizar"));	
        			};
        			if (datosPromo.get(i).get("tracker") == null){
        				request.setAttribute("trackerVB2","");
        			}else {
        				request.setAttribute("trackerVB2", datosPromo.get(i).get("tracker"));	
        			};
        			if (datosPromo.get(i).get("imagen") == null){
        				request.setAttribute("imagenVB2","");
        			}else {
        				request.setAttribute("imagenVB2", datosPromo.get(i).get("imagen"));	
        			};
        			if (datosPromo.get(i).get("linkVer") == null){
        				request.setAttribute("linkVerBasesVB2","");
        			}else {
        				request.setAttribute("linkVerBasesVB2", datosPromo.get(i).get("linkVer"));	
        			};
        			if (datosPromo.get(i).get("linkConocer") == null){
        				request.setAttribute("linkConocerVB2","");
        			}else {
        				request.setAttribute("linkConocerVB2", datosPromo.get(i).get("linkConocer"));	
        			};
    			} else if (datosPromo.get(i).get("id").equals("23")){
    				if (datosPromo.get(i).get("idPlan") == null){
        				request.setAttribute("idPlanHB1","");
        			}else {
        				request.setAttribute("idPlanHB1", datosPromo.get(i).get("idPlan"));	
        			};
        			if (datosPromo.get(i).get("nombre") == null){
        				request.setAttribute("nombreHB1","");
        			}else {
        				request.setAttribute("nombreHB1", datosPromo.get(i).get("nombre"));	
        			};
        			if (datosPromo.get(i).get("linkCotizar") == null){
        				request.setAttribute("linkCotizarHB1","");
        			}else {
        				request.setAttribute("linkCotizarHB1", datosPromo.get(i).get("linkCotizar"));	
        			};
        			if (datosPromo.get(i).get("tracker") == null){
        				request.setAttribute("trackerHB1","");
        			}else {
        				request.setAttribute("trackerHB1", datosPromo.get(i).get("tracker"));	
        			};
        			if (datosPromo.get(i).get("imagen") == null){
        				request.setAttribute("imagenHB1","");
        			}else {
        				request.setAttribute("imagenHB1", datosPromo.get(i).get("imagen"));	
        			};
        			if (datosPromo.get(i).get("linkVer") == null){
        				request.setAttribute("linkVerBasesHB1","");
        			}else {
        				request.setAttribute("linkVerBasesHB1", datosPromo.get(i).get("linkVer"));	
        			};
        			if (datosPromo.get(i).get("linkConocer") == null){
        				request.setAttribute("linkConocerHB1","");
        			}else {
        				request.setAttribute("linkConocerHB1", datosPromo.get(i).get("linkConocer"));	
        			};
    			} else if (datosPromo.get(i).get("id").equals("24")){
    				if (datosPromo.get(i).get("idPlan") == null){
        				request.setAttribute("idPlanHB2","");
        			}else {
        				request.setAttribute("idPlanHB2", datosPromo.get(i).get("idPlan"));	
        			};
        			if (datosPromo.get(i).get("nombre") == null){
        				request.setAttribute("nombreHB2","");
        			}else {
        				request.setAttribute("nombreHB2", datosPromo.get(i).get("nombre"));	
        			};
        			if (datosPromo.get(i).get("linkCotizar") == null){
        				request.setAttribute("linkCotizarHB2","");
        			}else {
        				request.setAttribute("linkCotizarHB2", datosPromo.get(i).get("linkCotizar"));	
        			};
        			if (datosPromo.get(i).get("tracker") == null){
        				request.setAttribute("trackerHB2","");
        			}else {
        				request.setAttribute("trackerHB2", datosPromo.get(i).get("tracker"));	
        			};
        			if (datosPromo.get(i).get("imagen") == null){
        				request.setAttribute("imagenHB2","");
        			}else {
        				request.setAttribute("imagenHB2", datosPromo.get(i).get("imagen"));	
        			};
        			if (datosPromo.get(i).get("linkVer") == null){
        				request.setAttribute("linkVerBasesHB2","");
        			}else {
        				request.setAttribute("linkVerBasesHB2", datosPromo.get(i).get("linkVer"));	
        			};
        			if (datosPromo.get(i).get("linkConocer") == null){
        				request.setAttribute("linkConocerHB2","");
        			}else {
        				request.setAttribute("linkConocerHB2", datosPromo.get(i).get("linkConocer"));	
        			};
    			} else if (datosPromo.get(i).get("id").equals("25")){
    				if (datosPromo.get(i).get("idPlan") == null){
        				request.setAttribute("idPlanVIB1","");
        			}else {
        				request.setAttribute("idPlanVIB1", datosPromo.get(i).get("idPlan"));	
        			};
        			if (datosPromo.get(i).get("nombre") == null){
        				request.setAttribute("nombreVIB1","");
        			}else {
        				request.setAttribute("nombreVIB1", datosPromo.get(i).get("nombre"));	
        			};
        			if (datosPromo.get(i).get("linkCotizar") == null){
        				request.setAttribute("linkCotizarVIB1","");
        			}else {
        				request.setAttribute("linkCotizarVIB1", datosPromo.get(i).get("linkCotizar"));	
        			};
        			if (datosPromo.get(i).get("tracker") == null){
        				request.setAttribute("trackerVIB1","");
        			}else {
        				request.setAttribute("trackerVIB1", datosPromo.get(i).get("tracker"));	
        			};
        			if (datosPromo.get(i).get("imagen") == null){
        				request.setAttribute("imagenVIB1","");
        			}else {
        				request.setAttribute("imagenVIB1", datosPromo.get(i).get("imagen"));	
        			};
        			if (datosPromo.get(i).get("linkVer") == null){
        				request.setAttribute("linkVerBasesVIB1","");
        			}else {
        				request.setAttribute("linkVerBasesVIB1", datosPromo.get(i).get("linkVer"));	
        			};
        			if (datosPromo.get(i).get("linkConocer") == null){
        				request.setAttribute("linkConocerVIB1","");
        			}else {
        				request.setAttribute("linkConocerVIB1", datosPromo.get(i).get("linkConocer"));	
        			};
    			} else if (datosPromo.get(i).get("id").equals("26")){
    				if (datosPromo.get(i).get("idPlan") == null){
        				request.setAttribute("idPlanVIB2","");
        			}else {
        				request.setAttribute("idPlanVIB2", datosPromo.get(i).get("idPlan"));	
        			};
        			if (datosPromo.get(i).get("nombre") == null){
        				request.setAttribute("nombreVIB2","");
        			}else {
        				request.setAttribute("nombreVIB2", datosPromo.get(i).get("nombre"));	
        			};
        			if (datosPromo.get(i).get("linkCotizar") == null){
        				request.setAttribute("linkCotizarVIB2","");
        			}else {
        				request.setAttribute("linkCotizarVIB2", datosPromo.get(i).get("linkCotizar"));	
        			};
        			if (datosPromo.get(i).get("tracker") == null){
        				request.setAttribute("trackerVIB2","");
        			}else {
        				request.setAttribute("trackerVIB2", datosPromo.get(i).get("tracker"));	
        			};
        			if (datosPromo.get(i).get("imagen") == null){
        				request.setAttribute("imagenVIB2","");
        			}else {
        				request.setAttribute("imagenVIB2", datosPromo.get(i).get("imagen"));	
        			};
        			if (datosPromo.get(i).get("linkVer") == null){
        				request.setAttribute("linkVerBasesVIB2","");
        			}else {
        				request.setAttribute("linkVerBasesVIB2", datosPromo.get(i).get("linkVer"));	
        			};
        			if (datosPromo.get(i).get("linkConocer") == null){
        				request.setAttribute("linkConocerVIB2","");
        			}else {
        				request.setAttribute("linkConocerVIB2", datosPromo.get(i).get("linkConocer"));	
        			};
    		}
    			}
    		}
    	}
    	
    }
    response.setContentType("text/html");
    if (oForm.getDatos().get("hiddSubmit") == null){
    	return mapping.findForward("continue");
    } else {
    	return mapping.findForward("inicio-promo");
    }
}
}