package cl.cencosud.ventaseguros.fichasubcategoria.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.cencosud.ventaseguros.fichasubcategoria.struts.form.NuevaFichaProductoForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class NuevaFichaProductoAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        request.getSession().removeAttribute("id_ficha");

        NuevaFichaProductoForm oForm = (NuevaFichaProductoForm) form;

        PaginaIntermediaDelegate paginaIntermediaDelegate =
            new PaginaIntermediaDelegate();

        oForm.setTipo(1);
        oForm.setHabilitar(1);

        List < Rama > ramas = paginaIntermediaDelegate.obtenerRamas();
        request.setAttribute("ramas", ramas);

        if (oForm.getRama() > 0) {
            List < Subcategoria > subcategorias =
                paginaIntermediaDelegate.obtenerSubcategorias(oForm.getRama());
            request.setAttribute("subcategorias", subcategorias);
        }

        return mapping.findForward("continue");
    }
}
