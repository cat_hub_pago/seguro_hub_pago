package cl.cencosud.ventaseguros.comparador.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import com.tinet.exceptions.system.SystemException;

import cl.cencosud.ventaseguros.comparador.model.Caracteristica;

public class ActualizarContenidoTempAction extends Action {
	
	private static final int TIPO_BENEFICIO = 1;
	private static final int TIPO_COBERTURA_PARTICULAR = 2;
	private static final int TIPO_COBERTURA_COMPARTIDA = 3;
	private static final int TIPO_EXCLUSION = 4;
	

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		String oCaract = request.getParameter("idCaract");
		String oValue = request.getParameter("valor");
		String oTipo = request.getParameter("idTipo");
		
		String mensaje = "";

		try {
			
			if (oValue != null && !oValue.trim().equals("")) {
				oValue = URLDecoder.decode(oValue, "ISO-8859-1");
			}
			
			switch (Integer.valueOf(oTipo)) {
			case TIPO_BENEFICIO: mensaje = this.updateDataContentido(request, oCaract, oValue, oTipo);break;
			case TIPO_COBERTURA_PARTICULAR: mensaje = this.updateDataContentido(request, oCaract, oValue, oTipo);break;
			case TIPO_COBERTURA_COMPARTIDA: mensaje = this.updateDataCobCompartida(request, oCaract, oValue);break;
			case TIPO_EXCLUSION: mensaje = this.updateDataContentido(request, oCaract, oValue, oTipo);break;
			}
			
			LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
			
			if (mensaje != null && !mensaje.trim().equals("")) {
				result.put("mensaje", mensaje);
			}
			
			response.setHeader("pragma", "no-cache");
			response.setHeader("cache-control", "no-cache");
			response.setDateHeader("expires", -1);
			response.setContentType("text/html");

			PrintWriter pwritter = response.getWriter();
			ObjectMapper mapper = new ObjectMapper();
			StringWriter json = new StringWriter();
			mapper.writeValue(json, result);
			pwritter.write(json.toString());
			
		} catch (IOException e) {
			throw new SystemException(e);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private String updateDataCobCompartida(HttpServletRequest request,
			 String oCaract, String oValue) {
		
		String mensaje = null;
		String estado = "";
		String tooltip = "";
		Integer codCaract = null;
		
		if (oCaract != null && !oCaract.equalsIgnoreCase("")) {
			if (oCaract.contains("desc")) {
				String[] arrTemp = oCaract.split("_");
				codCaract = Integer.parseInt(arrTemp[2]);
				estado = arrTemp[1];
			} else if (oCaract.contains("tooltip")) {
				String[] arrTemp = oCaract.split("_");
				codCaract = Integer.parseInt(arrTemp[1]);
				tooltip = oValue;
			} else if (oCaract.contains("value")){
				String[] arrTemp = oCaract.split("_");
				codCaract = Integer.parseInt(arrTemp[1]);
			}
		}
		
		
		LinkedHashMap<Integer, Caracteristica> mapCaractComp = (LinkedHashMap<Integer, Caracteristica>) 
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaractCompartidas");
		
		if (mapCaractComp == null) {
			mapCaractComp = new LinkedHashMap<Integer, Caracteristica>();
		}
		
		if (oCaract.contains("desc")) {
			if (mapCaractComp.containsKey(codCaract)) {
				Caracteristica caract = mapCaractComp.get(codCaract);
				caract.setEs_eliminado(estado);
				caract.setDescripcion(oValue);
			} else if (oValue != null && !oValue.trim().equals("")) {
				Caracteristica caract = new Caracteristica();
				caract.setId_caracteristica(codCaract);
				caract.setEs_eliminado(estado);
				caract.setDescripcion(oValue);
				mapCaractComp.put(codCaract, caract);
			} 
		} else if (oCaract.contains("tooltip")) {
			if (oValue != null) {
				if (mapCaractComp.containsKey(codCaract)) {
					Caracteristica caract = mapCaractComp.get(codCaract);
					caract.setTooltip(tooltip);
					if (!caract.getEs_eliminado().trim().equals("new")
							&& !caract.getEs_eliminado().trim().equals("mod")) {
						caract.setEs_eliminado("mod");
					}
				} 
			}
		} else if (oValue != null) {
			if (mapCaractComp.containsKey(codCaract)) {
				Caracteristica caract = mapCaractComp.get(codCaract);
				caract.setValor(oValue);
				if (!caract.getEs_eliminado().trim().equals("new")
						&& !caract.getEs_eliminado().trim().equals("mod")) {
					caract.setEs_eliminado("mod");
				}
			} 
		}
		
		if (mapCaractComp != null && mapCaractComp.size() > 0) {
			request.getSession().setAttribute(
					"cl.cencosud.ventaseguros.comparador.mapCaractCompartidas",
					mapCaractComp);
		}
		return mensaje;
	}

	@SuppressWarnings("unchecked")
	private String updateDataContentido(HttpServletRequest request, String oCaract, String oValue, String oTipo) {

		String mensaje = null;
		Integer codCaract = null;
		String estado = "";
		String tooltip = "";
		Long idPlan = null;
		
		if (oCaract != null && !oCaract.equalsIgnoreCase("")) {
			if (oCaract.contains("desc")) {
				String[] arrTemp = oCaract.split("_");
				codCaract = Integer.parseInt(arrTemp[2]);
				estado = arrTemp[1];
			} else if (oCaract.contains("tooltip")) {
				String[] arrTemp = oCaract.split("_");
				codCaract = Integer.parseInt(arrTemp[1]);
				tooltip = oValue;
			} else {
				String[] arrTemp = oCaract.split("_");
				codCaract = Integer.parseInt(arrTemp[0]);
				idPlan = Long.parseLong(arrTemp[1]);
			}
		}

		LinkedHashMap<Integer, Caracteristica> mapCaracteristicas = (LinkedHashMap<Integer, Caracteristica>) request
				.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaracteristicas");
		LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica = (LinkedHashMap<Integer, List<Caracteristica>>) request
				.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica");


		if (mapCaracteristicas != null && oCaract.contains("desc")) {
			if (mapCaracteristicas.containsKey(codCaract)) {
				Caracteristica caract = mapCaracteristicas.get(codCaract);
				caract.setEs_eliminado(estado);
				caract.setDescripcion(oValue);
			} else if (oValue != null && !oValue.trim().equals("")) {
				Caracteristica caract = new Caracteristica();
				caract.setId_caracteristica(codCaract);
				caract.setEs_eliminado(estado);
				caract.setTipo(Integer.parseInt(oTipo));
				caract.setDescripcion(oValue);
				mapCaracteristicas.put(codCaract, caract);
			} 
		} else {
			if (oCaract.contains("desc")) {
				if (oValue != null && !oValue.trim().equals("")) {
					Caracteristica caract = new Caracteristica();
					caract.setId_caracteristica(codCaract);
					caract.setEs_eliminado(estado);
					caract.setTipo(Integer.parseInt(oTipo));
					caract.setDescripcion(oValue);
					mapCaracteristicas = new LinkedHashMap<Integer, Caracteristica>();
					mapCaracteristicas.put(codCaract, caract);
				} 
			}
		}

		if (mapCaracteristicas != null && oCaract.contains("tooltip")) {
			if (tooltip != null) {
				if (mapCaracteristicas.containsKey(codCaract)) {
					Caracteristica caract = mapCaracteristicas.get(codCaract);
					caract.setTooltip(tooltip);
					if (!caract.getEs_eliminado().trim().equals("new")
							&& !caract.getEs_eliminado().trim().equals("mod")) {
						caract.setEs_eliminado("mod");
					}
				} 
			}
		} 

		if (mapPlanCaracteristica != null) {
			if (oCaract.contains("desc")) {
				if (oValue != null && !oValue.trim().equals("")) {
					if (mapPlanCaracteristica.containsKey(codCaract)) {
						List<Caracteristica> listTemp = mapPlanCaracteristica.get(codCaract);
						
						for (Caracteristica caract : listTemp) {
							caract.setDescripcion(oValue);
						}						
					} else {
						List<Caracteristica> listTemp = new ArrayList<Caracteristica>();
						Caracteristica car = new Caracteristica();
						car.setDescripcion(oValue);
						car.setId_caracteristica(codCaract);
						car.setTipo(Integer.parseInt(oTipo));
						listTemp.add(car);
						mapPlanCaracteristica.put(codCaract, listTemp);
					}
				}
			} else if (oCaract.contains("tooltip")) {
				if (oValue != null && !oValue.trim().equals("")) {
					if (mapPlanCaracteristica.containsKey(codCaract)) {
						List<Caracteristica> listTemp = mapPlanCaracteristica.get(codCaract);
						
						for (Caracteristica caract : listTemp) {
							caract.setTooltip(oValue);
						}						
					}
				}
			} else if (!oCaract.contains("desc") && !oCaract.contains("tooltip")) {
				if (oValue != null) {
					if (mapPlanCaracteristica.containsKey(codCaract)) {
						List<Caracteristica> listTemp = mapPlanCaracteristica
								.get(codCaract);
						boolean existe = false;
						String descripcionTemp = "";
						String tooltipTemp = "";

						for (Caracteristica caract : listTemp) {
							descripcionTemp = caract.getDescripcion();
							tooltipTemp = caract.getTooltip();
							
							if (caract.getId_plan() == idPlan) {
								caract.setValor(oValue);
								existe = true;
								break;
							}
						}

						if (!existe) {
							Caracteristica caract = new Caracteristica();
							caract.setId_caracteristica(codCaract);
							caract.setId_plan(idPlan);
							caract.setValor(oValue);
							caract.setTipo(Integer.parseInt(oTipo));
							caract.setDescripcion(descripcionTemp);
							caract.setTooltip(tooltipTemp);
							listTemp.add(caract);
						}

						mapPlanCaracteristica.put(codCaract, listTemp);
					} else {
						List<Caracteristica> listTemp = new ArrayList<Caracteristica>();
						Caracteristica caract = new Caracteristica();
						caract.setId_caracteristica(codCaract);
						caract.setId_plan(idPlan);
						caract.setValor(oValue);
						caract.setTipo(Integer.parseInt(oTipo));
						listTemp.add(caract);
						mapPlanCaracteristica.put(codCaract, listTemp);
					}
				}
			}
		} else if (oCaract.contains("desc")) {
			if (oValue != null && !oValue.trim().equals("")) {
					List<Caracteristica> listTemp = new ArrayList<Caracteristica>();
					Caracteristica car = new Caracteristica();
					car.setDescripcion(oValue);
					car.setId_caracteristica(codCaract);
					car.setTipo(Integer.parseInt(oTipo));
					listTemp.add(car);
					mapPlanCaracteristica = new LinkedHashMap<Integer, List<Caracteristica>>();
					mapPlanCaracteristica.put(codCaract, listTemp);
				}
		}		

		if (mapCaracteristicas != null && mapCaracteristicas.size() > 0) {
			request.getSession().setAttribute(
					"cl.cencosud.ventaseguros.comparador.mapCaracteristicas",
					mapCaracteristicas);
		}

		if (mapPlanCaracteristica != null && mapPlanCaracteristica.size() > 0) {
			request.getSession()
					.setAttribute(
							"cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica",
							mapPlanCaracteristica);
		}

		return mensaje;
	}
	

}
