package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.cencosud.ventaseguros.paginaintermedia.struts.form.ModificarPaginaIntermediaForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class AgregarProductoAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        String forward = "";

        ModificarPaginaIntermediaForm oForm =
            (ModificarPaginaIntermediaForm) form;

        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();

        long res =
            delegate.agregarProducto(oForm.getId_subcategoria(), oForm
                .getId_producto_leg());

        request.getSession().setAttribute("idSubcategoria",
            oForm.getId_subcategoria());
        if (res > 0) {
            forward = "continue";
        } else {
            forward = "error";
        }
        return mapping.findForward(forward);
    }
}
