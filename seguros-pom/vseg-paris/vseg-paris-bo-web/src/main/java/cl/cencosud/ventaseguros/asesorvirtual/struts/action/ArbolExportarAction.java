package cl.cencosud.ventaseguros.asesorvirtual.struts.action;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Preguntas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Respuestas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.cencosud.ventaseguros.asesorvirtual.util.ResultadoConverter;
import cl.cencosud.ventaseguros.asesorvirtual.util.ResultadoXppDriver;
import cl.cencosud.ventaseguros.delegate.AsesorVirtualDelegate;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ArbolExportarAction extends Action {

    private static final String PROP_FILE = "arbol.properties";

    private static final String ENCODING = "ISO-8859-1";

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        Sitio sitio = null;
        AsesorVirtualDelegate asesorVirtualDelegate =
            new AsesorVirtualDelegate();

        try {
            XStream xstream = new XStream(new ResultadoXppDriver());

            xstream.alias("sitio", Sitio.class);

            xstream.alias("seccion", Seccion.class);
            xstream.aliasAttribute(Seccion.class, "id", "id");
            xstream.aliasAttribute(Seccion.class, "titulo", "titulo");
            xstream.addImplicitCollection(Sitio.class, "secciones",
                Seccion.class);

            xstream.alias("preguntas", Preguntas.class);
            xstream.aliasAttribute(Preguntas.class, "idPrimeraPregunta",
                "idPrimeraPregunta");
            xstream.aliasAttribute(Preguntas.class, "cantidadpreguntas",
                "cantidadpreguntas");

            xstream.alias("pregunta", Pregunta.class);
            xstream.aliasAttribute(Pregunta.class, "id", "id");
            xstream.aliasAttribute(Pregunta.class, "titulo", "titulo");
            xstream.aliasAttribute(Pregunta.class, "tipoPregunta",
                "tipoPregunta");
            xstream.addImplicitCollection(Preguntas.class, "preguntas",
                Pregunta.class);

            xstream.alias("alternativa", Alternativa.class);
            xstream.aliasAttribute(Alternativa.class, "id", "id");
            xstream.aliasAttribute(Alternativa.class, "texto0", "texto0");
            xstream.aliasAttribute(Alternativa.class, "texto1", "texto1");
            xstream.aliasAttribute(Alternativa.class, "valor0", "valor0");
            xstream.aliasAttribute(Alternativa.class, "valor1", "valor1");
            xstream.aliasAttribute(Alternativa.class, "respuesta", "respuesta");
            xstream.aliasAttribute(Alternativa.class, "proximaPregunta",
                "proximaPregunta");
            xstream.addImplicitCollection(Pregunta.class, "alternativas",
                Alternativa.class);

            xstream.alias("respuestas", Respuestas.class);

            xstream.alias("resultado", Resultado.class);
            xstream.addImplicitCollection(Respuestas.class, "resultados",
                Resultado.class);

            Converter converter = new ResultadoConverter();
            xstream.registerConverter(converter);

            //Cargar
            sitio = asesorVirtualDelegate.cargarArbol(1);

            InputStream is =
                this.getClass().getClassLoader().getResourceAsStream(PROP_FILE);
            Properties prop = new Properties();
            prop.load(is);

            String filename = prop.getProperty("filename");

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            OutputStreamWriter writer =
                new OutputStreamWriter(outputStream, ENCODING);
            xstream.toXML(sitio, writer);

            String header =
                "<?xml version=\"1.0\" encoding=\"" + writer.getEncoding()
                    + "\"?>\n";

            StringBuffer xml = new StringBuffer();
            xml.append(header);
            xml.append(outputStream.toString(ENCODING));

            BufferedWriter out = new BufferedWriter(new FileWriter(filename));
            out.write(xml.toString());
            out.close();

            //Tiempo de espera.
            for (int i = 0; i < 1000; i++) {
                //
            }

            Map < String, Object > res = new HashMap < String, Object >();
            res.put("status", "ok");

            //Sincronizar archivo en los servidores.
            if (!asesorVirtualDelegate.sincronizarXML(filename)) {
                res.put("status", "error");
            }

            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, res);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;
    }
}
