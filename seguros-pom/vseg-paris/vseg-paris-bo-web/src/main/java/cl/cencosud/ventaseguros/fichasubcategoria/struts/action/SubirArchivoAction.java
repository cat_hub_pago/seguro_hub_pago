package cl.cencosud.ventaseguros.fichasubcategoria.struts.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.common.exception.IncorrectFileTypeException;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.fichasubcategoria.struts.form.SubirArchivoForm;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class SubirArchivoAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        SubirArchivoForm oForm = (SubirArchivoForm) form;

        HashMap < String, String > rpta = new HashMap < String, String >();

        FichaSubcategoriaDelegate delegate = new FichaSubcategoriaDelegate();
        long idFicha = -1;
        String tipo = "";

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            rpta.put("estado", "nofile");
            try {
                idFicha = (Long) request.getSession().getAttribute("id_ficha");
                tipo = request.getParameter("tipo");
                FormFile archivo =
                    (FormFile) oForm.getMultipartRequestHandler()
                        .getFileElements().get(tipo);
                if (idFicha > 0 && archivo.getFileSize() > 0) {
                    if (("imagen".equals(tipo) && !archivo.getContentType()
                        .endsWith("jpeg"))) {
                        throw new IncorrectFileTypeException(
                            IncorrectFileTypeException.TIPO_ARCHIVO,
                            new Object[] { "JPG" });
                    } else if (!"imagen".equals(tipo)
                        && !"text/html".equals(archivo.getContentType())) {
                        throw new IncorrectFileTypeException(
                            IncorrectFileTypeException.TIPO_ARCHIVO,
                            new Object[] { "HTML" });
                    }

                    InputStream stream = archivo.getInputStream();

                    byte[] buffer = new byte[8192];
                    int bytesLeidos = 0;
                    while ((bytesLeidos = stream.read(buffer, 0, 8192)) != -1) {
                        baos.write(buffer, 0, bytesLeidos);
                    }

                    delegate.subirArchivo(idFicha, baos.toByteArray(), tipo);

                    rpta.put("estado", "success");

                    baos.close();
                    baos = null;
                }
            } catch (IOException e) {
                rpta.put("estado", "error");
                rpta.put("mensaje", e.getMessage());

            } catch (IncorrectFileTypeException e) {
                rpta.put("estado", "error");
                rpta.put("mensaje", e.getMessage());
            }

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, rpta);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    throw new SystemException(e);
                }
                baos = null;
            }
        }

        return null;
    }
}
