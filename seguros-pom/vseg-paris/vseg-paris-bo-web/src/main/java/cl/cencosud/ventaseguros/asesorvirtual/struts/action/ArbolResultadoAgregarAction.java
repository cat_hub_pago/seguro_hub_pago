package cl.cencosud.ventaseguros.asesorvirtual.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.asesorvirtual.struts.form.ArbolResultadoAgregarForm;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ArbolResultadoAgregarAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        ArbolResultadoAgregarForm oForm = (ArbolResultadoAgregarForm) form;

        PaginaIntermediaDelegate paginaIntermediaDelegate =
            new PaginaIntermediaDelegate();
        FichaSubcategoriaDelegate fichaSubcategoriaDelegate =
            new FichaSubcategoriaDelegate();

        List < Rama > ramas = paginaIntermediaDelegate.obtenerRamas();
        request.setAttribute("ramas", ramas);

        if (oForm.getRama() > 0) {
            List < Subcategoria > subcategorias =
                paginaIntermediaDelegate.obtenerSubcategorias(oForm.getRama());
            request.setAttribute("subcategorias", subcategorias);
        }

        if (oForm.getSubcategoria() > 0) {
            List < PlanLeg > planes =
                fichaSubcategoriaDelegate
                    .obtenerPlanes(oForm.getSubcategoria());
            request.setAttribute("planes", planes);
        }

        return mapping.findForward("continue");
    }
}
