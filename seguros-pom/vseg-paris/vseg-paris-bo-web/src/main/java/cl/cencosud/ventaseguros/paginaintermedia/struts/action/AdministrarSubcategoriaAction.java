package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.tinet.common.model.exception.BusinessException;

/**
 * Despliega formulario para ingreso de nueva Subcategoria.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 17/10/2010
 */
public class AdministrarSubcategoriaAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException {

        PaginaIntermediaDelegate oDelegate = new PaginaIntermediaDelegate();

        // Obtener listado de ramas.
        List < Rama > ramas = oDelegate.obtenerRamas();
        request.setAttribute("ramas", ramas);

        return mapping.findForward("continuar");
    }

}
