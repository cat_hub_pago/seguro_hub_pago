package cl.cencosud.ventaseguros.asesorvirtual.util;

import java.io.Writer;

import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

/**
 * Formato para el texto de Resultado.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 21/09/2010
 */
public class ResultadoPrettyPrintWritter extends PrettyPrintWriter {

    /**
     * Flag que indica que se encontro el nodo buscado.
     */
    private boolean cdata = false;

    /**
     * Constructor de la clase.
     * @param writer Salida.
     */
    public ResultadoPrettyPrintWritter(Writer writer) {
        super(writer);
    }

    /**
     * Buscar el nodo de nombre "resultado".
     * @param name Nombre del nodo actual.
     * @param clazz Clase del nodo actual.
     */
    @Override
    public void startNode(String name, Class clazz) {
        super.startNode(name, clazz);
        cdata = name.equals("resultado");
    }

    /**
     * Agrega comentario CDATA solo al campo definido anteriormente.
     * @param writer Salida del texto.
     * @param text texto de salida.
     */
    @Override
    protected void writeText(QuickWriter writer, String text) {
        if (cdata) {
            writer.write("<![CDATA[");
            writer.write(text);
            writer.write("]]>");
        } else {
            writer.write(text);
        }
    }
}
