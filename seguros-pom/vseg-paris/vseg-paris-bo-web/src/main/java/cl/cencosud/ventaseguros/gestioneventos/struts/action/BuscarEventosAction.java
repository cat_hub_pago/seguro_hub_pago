package cl.cencosud.ventaseguros.gestioneventos.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;

import com.tinet.comun.util.date.DateConstant;
import com.tinet.comun.util.date.DateUtil;
import com.tinet.exceptions.system.SystemException;

/**
 * MyEclipse Struts Creation date: 09-15-2010
 *
 * XDoclet definition:
 *
 * @struts.action path="/buscarEventos" name="buscarEventosForm"
 *                input="/form/buscarEventos.jsp" scope="request"
 *                validate="true"
 */
public class BuscarEventosAction extends Action {

    private static final String EXITO = "continuar";

    /**
     * Method execute
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {
        //        BuscarEventosForm buscarEventosForm = (BuscarEventosForm) form;

        //        String flujo = EXITO;

        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();

        Date fechaInicio = null;
        Date fecha_ter = null;
        Integer estadoHabilitado = -1;

        String codigo = (String) request.getParameter("codigo");
        String fechaInicialCadena =
            (String) request.getParameter("fecha_inicial");
        String fechaTeminoCadena =
            (String) request.getParameter("fecha_termino");
        String posicion = (String) request.getParameter("posicion");
        String tipoEventoCadena = (String) request.getParameter("tipo_evento");
        String habilitado = (String) request.getParameter("habilitado");

        Integer posicionEntero = null;
        Integer tipoPaginaEntero = null;

        if (codigo.length() == 0) {
            codigo = null;
        }

        if (posicion.length() == 0) {
            posicion = null;
        } else {
            posicionEntero = Integer.parseInt(posicion);
        }

        if (tipoEventoCadena.length() == 0) {
            tipoEventoCadena = null;
        } else {
            tipoPaginaEntero = Integer.parseInt(tipoEventoCadena);
        }

        if ("false".equals(habilitado)) {
            estadoHabilitado = 0;
        } else {
            estadoHabilitado = 1;
        }

        response.setHeader("pragma", "no-cache");
        response.setHeader("cache-control", "no-cache");
        response.setDateHeader("expires", -1);
        response.setContentType("text/html");

        Map < String, Object > mError = new HashMap < String, Object >();

        try {
            PrintWriter pwriter;
            pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();

            if (fechaInicialCadena != null && !"".equals(fechaInicialCadena)) {
                try {
                    fechaInicio =
                        DateUtil.getFecha(fechaInicialCadena,
                            DateConstant.FORMAT_SIMPLE_DATE);
                } catch (IllegalArgumentException e) {
                    mError.put("error", "error_fechainicio");
                    mapper.writeValue(json, mError);
                    pwriter.write(json.toString());
                    return null;
                }
            }
            if (fechaTeminoCadena != null && !"".equals(fechaTeminoCadena)) {
                try {
                    fecha_ter =
                        DateUtil.getFecha(fechaTeminoCadena,
                            DateConstant.FORMAT_SIMPLE_DATE);
                } catch (IllegalArgumentException e) {
                    mError.put("error", "error_fechainicio");
                    mapper.writeValue(json, mError);
                    pwriter.write(json.toString());
                    return null;
                }
            }

            if (fechaInicio != null && fecha_ter != null) {
                if (DateUtil.esMayorQue(fechaInicio, fecha_ter)) {
                    mError.put("error", "error_rangoFechas");
                    mapper.writeValue(json, mError);
                    pwriter.write(json.toString());
                    return null;
                }
            }

            Map filtros = new HashMap();

            filtros.put("codigoFiltro", codigo);
            filtros.put("fechaInicio", fechaInicio);
            filtros.put("fechaTermino", fecha_ter);
            filtros.put("posicion", posicionEntero);
            filtros.put("estado", estadoHabilitado);
            filtros.put("paginaEntero", tipoPaginaEntero);

            List < Map < String, Object >> lista =
                delegate.buscarEventos(filtros);

            for (Map < String, Object > evento : lista) {

                if (evento.get("fecha_termino") != null) {
                    String sFechaTermino =
                        DateUtil.getFechaFormateada((Date) evento
                            .get("fecha_termino"),
                            DateConstant.FORMAT_SIMPLE_DATE);
                    evento.put("fecha_termino", sFechaTermino);
                }

                if (evento.get("fecha_inicio") != null) {
                    String sFechaInicio =
                        DateUtil.getFechaFormateada((Date) evento
                            .get("fecha_inicio"),
                            DateConstant.FORMAT_SIMPLE_DATE);
                    evento.put("fecha_inicio", sFechaInicio);

                }
            }

            mapper.writeValue(json, lista);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;
    }
}
