package cl.cencosud.ventaseguros.asesorvirtual.util;

import java.io.Writer;

import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;

/**
 * Driver de salida.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 21/09/2010
 */
public class ResultadoXppDriver extends XppDriver {

    /**
     * Crea salida para el driver.
     * @param out Salida.
     * @return Salida formateada.
     */
    @Override
    public HierarchicalStreamWriter createWriter(Writer out) {
        return new ResultadoPrettyPrintWritter(out);
    }

}
