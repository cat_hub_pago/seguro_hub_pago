package cl.cencosud.ventaseguros.planes.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.MantenedorETLDelegate;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ETLEnviarPlanesAction extends Action {

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        try {

            String codigo_tipo_prod_leg = request.getParameter("tipo");
            String codigo_sub_tipo_prod_leg = request.getParameter("subtipo");
            String codigo_producto_leg = request.getParameter("producto");

            //TODO: Lanzar excepcion si plan es null.
            String[] planes = request.getParameterValues("planes");

            if (codigo_tipo_prod_leg == null || "".equals(codigo_tipo_prod_leg)) {
                throw new SystemException(new Exception(
                    "Se debe denifir un tipo"));
            }

            if (codigo_sub_tipo_prod_leg == null
                || "".equals(codigo_sub_tipo_prod_leg)) {
                throw new SystemException(new Exception(
                    "Se debe denifir un subtipo"));
            }

            if (codigo_producto_leg == null || "".equals(codigo_producto_leg)) {
                throw new SystemException(new Exception(
                    "Se debe denifir un producto"));
            }

            MantenedorETLDelegate delegate = new MantenedorETLDelegate();
            delegate.enviarPlan(codigo_tipo_prod_leg, codigo_sub_tipo_prod_leg,
                codigo_producto_leg, planes);

            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, planes);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }
        return null;
    }
}
