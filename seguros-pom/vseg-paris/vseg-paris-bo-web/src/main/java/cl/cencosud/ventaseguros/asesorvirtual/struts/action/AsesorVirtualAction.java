package cl.cencosud.ventaseguros.asesorvirtual.struts.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.AsesorVirtualDelegate;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class AsesorVirtualAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        //Borrar arbol pre existente
        request.getSession().removeAttribute("sitio");

        String sincronizado = "0";
        String estado = "0";

        AsesorVirtualDelegate asesorVirtualDelegate =
            new AsesorVirtualDelegate();

        List < Map < String, Object > > lArbol =
            asesorVirtualDelegate.buscarArbol(1);

        if (!lArbol.isEmpty()) {
            sincronizado =
                ((Number) lArbol.get(0).get("es_sincronizado")).toString();

            estado = ((Number) lArbol.get(0).get("estado")).toString();
        }

        request.getSession().setAttribute("sincronizado", sincronizado);
        request.getSession().setAttribute("estado", estado);

        return mapping.findForward("continue");
    }
}
