package cl.cencosud.ventaseguros.gestioneventos.struts.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.exception.ObtenerArchivosException;
import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.exceptions.system.SystemException;

public class ObtenerArchivosAction extends Action {

    private static final String IMAGE_NOT_FOUND =
        "/cl/cencosud/ventaseguros/admin/struts/not.gif";
    private static final int BUFFER_SIZE = 4096;

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException {

        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();
        String id_evento;
        String tipo;
        try {
            ServletOutputStream out = null;

            if (request.getParameter("id_evento") != null) {

                id_evento = request.getParameter("id_evento");
                tipo = request.getParameter("tipo");

                Map < String, Object > archivo =
                    delegate.obtenerArchivo(id_evento, tipo);

                if (archivo == null) {
                    if ("imagen".equals(tipo)) {
                        out = response.getOutputStream();

                        response.setHeader("pragma", "no-cache");
                        response.setHeader("cache-control", "no-cache");
                        response.setDateHeader("expires", -1);
                        response.setContentType("image/gif");
                        response.setHeader("Content-Disposition",
                            "inline;filename=not.gif");

                        InputStream in =
                            this.getClass().getClassLoader()
                                .getResourceAsStream(IMAGE_NOT_FOUND);
                        byte[] outputByte = new byte[BUFFER_SIZE];
                        while (in.read(outputByte, 0, BUFFER_SIZE) != -1) {
                            out.write(outputByte, 0, BUFFER_SIZE);
                        }
                        in.close();
                    } else {
                        throw new ObtenerArchivosException(
                            ObtenerArchivosException.ARCHIVO_NO_ENCONTRADO,
                            ObtenerArchivosException.SIN_ARGUMENTOS);
                    }
                } else {
                    out = response.getOutputStream();
                    response.setHeader("pragma", "no-cache");
                    response.setHeader("cache-control", "no-cache");
                    response.setDateHeader("expires", -1);

                    if ("imagen".equals(tipo)) {
                        response.setContentType("image/jpeg");
                        response.setHeader("Content-Disposition",
                            "inline;filename=imagen.jpg");

                        byte[] imagen = (byte[]) archivo.get("archivo");
                        out.write(imagen, 0, imagen.length);

                    } else {
                        if (tipo != null && "archivo".equals(tipo)) {

                            response.setContentType("application/vnd.ms-excel");
                            response.setHeader("Content-Disposition",
                                "attachment; filename=rut.csv");
                            byte[] imagen = (byte[]) archivo.get("archivo");
                            out.write(imagen, 0, imagen.length);
                        }
                    }

                }
            }
        } catch (IOException e) {
            throw new SystemException(e);
        }
        return null;
    }
}
