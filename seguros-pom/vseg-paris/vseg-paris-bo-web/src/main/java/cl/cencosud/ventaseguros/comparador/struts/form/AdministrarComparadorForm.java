package cl.cencosud.ventaseguros.comparador.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import cl.cencosud.ventaseguros.gestionusuario.struts.form.ModificacionUsuarioInternoForm;
import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

public class AdministrarComparadorForm extends BuilderActionFormBaseVSP {

	private static final long serialVersionUID = 7337680321620591051L;
	private Integer tipo;
    private Integer rama;
    private Integer subcategoria;
    private Integer producto;
    private Integer editar;
    private Integer idProducto;
    private Integer idSubcategoria;
    private Integer idRama;

    public Integer getEditar() {
		return editar;
	}

	public void setEditar(Integer editar) {
		this.editar = editar;
	}
    
	public Integer getIdRama() {
		return idRama;
	}

	public void setIdRama(Integer idRama) {
		this.idRama = idRama;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public Integer getIdSubcategoria() {
		return idSubcategoria;
	}

	public void setIdSubcategoria(Integer idSubcategoria) {
		this.idSubcategoria = idSubcategoria;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Integer getRama() {
		return rama;
	}

	public void setRama(Integer rama) {
		this.rama = rama;
	}

	public Integer getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(Integer subcategoria) {
		this.subcategoria = subcategoria;
	}

	public Integer getProducto() {
		return producto;
	}

	public void setProducto(Integer producto) {
		this.producto = producto;
	}

	@Override
	public InputStream getValidationRules(HttpServletRequest request) {
		return ModificacionUsuarioInternoForm.class.getResourceAsStream("resource/validation.xml");
	}

}
