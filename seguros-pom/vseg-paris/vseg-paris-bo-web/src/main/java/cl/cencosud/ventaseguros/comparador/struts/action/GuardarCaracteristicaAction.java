package cl.cencosud.ventaseguros.comparador.struts.action;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.cencosud.ventaseguros.delegate.ComparadorDelegate;
import cl.tinet.common.seguridad.model.UsuarioInterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;

public class GuardarCaracteristicaAction extends Action {
	
	private static final int TIPO_BENEFICIO = 1;
	private static final int TIPO_COBERTURA_PARTICULAR = 2;
	private static final int TIPO_COBERTURA_COMPARTIDA = 3;
	private static final int TIPO_EXCLUSION = 4;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		UsuarioInterno usuario = (UsuarioInterno) SeguridadUtil.getUsuario(request);
		String oTipo = request.getParameter("idTipo");
		String oHabilitado = request.getParameter("habilitado");
		String oIdProducto = request.getParameter("idProducto");
		
		if (oTipo != null && Integer.valueOf(oTipo) ==  TIPO_BENEFICIO) {
			if (oHabilitado != null) {
				if (Boolean.valueOf(oHabilitado)) {
					oHabilitado = "1";
				} else {
					oHabilitado = "0";
				}
			}
		} else {
			oHabilitado = "0";
		}	
		
		switch (Integer.valueOf(oTipo)) {
		case TIPO_BENEFICIO: this.saveDataContenido(request, usuario, oHabilitado);break;
		case TIPO_COBERTURA_PARTICULAR: this.saveDataContenido(request, usuario, oHabilitado);break;
		case TIPO_COBERTURA_COMPARTIDA: this.saveDataCobCompartida(request, usuario, oIdProducto);break;
		case TIPO_EXCLUSION: this.saveDataContenido(request, usuario, oHabilitado);break;
		}
		
		LinkedHashMap<String, Object> result = new LinkedHashMap<String, Object>();
		result.put("status", "Contenido se guardo exitosamente.");
		
		response.setHeader("pragma", "no-cache");
        response.setHeader("cache-control", "no-cache");
        response.setDateHeader("expires", -1);
        response.setContentType("text/html");

        PrintWriter pwritter = response.getWriter();
        ObjectMapper mapper = new ObjectMapper();
        StringWriter json = new StringWriter();
        mapper.writeValue(json, result);
        pwritter.write(json.toString());
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void saveDataCobCompartida(HttpServletRequest request,
			UsuarioInterno usuario, String oIdProducto) {
		
		LinkedHashMap<Integer, Caracteristica> mapCaractComp = (LinkedHashMap<Integer, Caracteristica>) 
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaractCompartidas");
		
		if (mapCaractComp != null && mapCaractComp.size() > 0) {
			ComparadorDelegate delegate  = new ComparadorDelegate();
			delegate.guardarContenidoCaracteristicasCompartidas(mapCaractComp, usuario, oIdProducto);
		}
	}

	@SuppressWarnings("unchecked")
	private void saveDataContenido(HttpServletRequest request, UsuarioInterno usuario, String oHabilitado) {
		LinkedHashMap<Integer, Caracteristica> mapCaracteristicas = (LinkedHashMap<Integer, Caracteristica>)
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaracteristicas");
		LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica = (LinkedHashMap<Integer, List<Caracteristica>>)
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica");
	
		if (mapCaracteristicas != null && mapPlanCaracteristica != null && mapCaracteristicas.size() > 0 && mapPlanCaracteristica.size() > 0) {
			ComparadorDelegate delegate  = new ComparadorDelegate();			
			delegate.guardarContenidoComparadorProducto(mapCaracteristicas, mapPlanCaracteristica, usuario, oHabilitado);
		}		
	}

}
