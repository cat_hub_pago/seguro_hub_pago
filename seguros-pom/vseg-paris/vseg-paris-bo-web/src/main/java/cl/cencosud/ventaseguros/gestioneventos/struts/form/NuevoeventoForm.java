/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.gestioneventos.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

/** 
 * MyEclipse Struts
 * Creation date: 09-15-2010
 * 
 * XDoclet definition:
 * @struts.form name="nuevoeventoForm"
 */
public class NuevoeventoForm extends BuilderActionFormBaseVSP {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return NuevoeventoForm.class
            .getResourceAsStream("resource/validation.xml");
    }
}