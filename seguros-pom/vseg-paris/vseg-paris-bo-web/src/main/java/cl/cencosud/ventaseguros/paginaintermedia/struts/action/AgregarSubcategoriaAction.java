package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.cencosud.ventaseguros.paginaintermedia.struts.form.AgregarSubcategoriaForm;
import cl.tinet.common.model.exception.BusinessException;

/**
 * Agrega una nueva subcategoria.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 17/10/2010
 */
public class AgregarSubcategoriaAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException {

        PaginaIntermediaDelegate oDelegate = new PaginaIntermediaDelegate();

        AgregarSubcategoriaForm oForm = (AgregarSubcategoriaForm) form;

        String subcategoria =
            oForm.getDatos().get("subcategoria").toString().trim();
        int idRama = Integer.parseInt(oForm.getDatos().get("rama"));
        int idTipo = 0;
        if(oForm.getDatos().get("tipo") != null){
            idTipo = Integer.parseInt(oForm.getDatos().get("tipo").toString());
        }

        long idSubcategoria =
            oDelegate.agregarSubcategoria(idRama, subcategoria, idTipo);
        request.setAttribute("idSubcategoria", idSubcategoria);
        
        return mapping.findForward("continuar");
    }

}
