package cl.cencosud.ventaseguros.comparador.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;


import com.tinet.exceptions.system.SystemException;


public class ComparadorPrevisualizarPlanesAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		String oIdPlanEliminar = request.getParameter("idPlan");
		
		try {
			
			LinkedHashMap<String, ?> mapResultComparador = (LinkedHashMap<String, ?>)request.getSession().getAttribute("mapResultComparadorBack");
			
			if (oIdPlanEliminar != null && !oIdPlanEliminar.trim().equals("")) {
				//logica para eliminar un plan
			}
			
			LinkedHashMap<String, LinkedHashMap<String, ?>> result = new LinkedHashMap<String, LinkedHashMap<String,?>>();

			result.put("result", mapResultComparador);			
			
			response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwritter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, result);
            pwritter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }
		return null;
	}
	
	

}
