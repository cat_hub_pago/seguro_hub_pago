package cl.cencosud.seguroscencosud.main;

import cl.cencosud.seguroscencosud.service.CotizacionServiceImpl;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.util.Properties;

public class Main {
	
	private static Properties properties;
	protected final static Logger logger = Logger.getLogger(Main.class);
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		boolean flag = false;
		FileInputStream fis;
		
		CotizacionServiceImpl coti = new CotizacionServiceImpl();		
		
		try {
			logger.info("--------------------------------------------------------------");
			logger.info("Iniciando proceso de Generar Archivo cotizaciones para e_quotes");
			
			fis = new FileInputStream("config/config-genera-equotes.properties");
			properties = new Properties();
			properties.load(fis);
			String idsolicitud = properties.getProperty("idsolicitud");
			String rut = properties.getProperty("rut");
			
			flag = coti.obtenerArchivo(Integer.valueOf(idsolicitud), rut);
			
			if (flag){
				logger.info("El Archivo se genero exitosamente");	
			}else{
				logger.info("El Archivo no se genero");
			}
		} catch (Exception e) {
			logger.error("Proceso terminado anormalmente", e);	
		}

	}

}
