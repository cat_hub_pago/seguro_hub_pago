package cl.cencosud.seguroscencosud.dao;

import java.util.List;

import cl.cencosud.seguroscencosud.dto.CotizacionDTO;

public interface CotizacionDAO {

	public List<CotizacionDTO> obtenerArchivo(Integer idSolicitud, String rut);
}
