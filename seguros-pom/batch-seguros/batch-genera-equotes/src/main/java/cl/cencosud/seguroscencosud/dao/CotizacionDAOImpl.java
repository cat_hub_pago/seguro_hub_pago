package cl.cencosud.seguroscencosud.dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.log4j.Logger;

import cl.cencosud.seguroscencosud.dto.CotizacionDTO;

public class CotizacionDAOImpl implements CotizacionDAO {
	
	/**
     * Logger de la clase
     */
	protected final static Logger logger = Logger.getLogger(CotizacionDAOImpl.class);
	private static Properties properties;
    
    /**
     * Consulta para obtener los datos de un Contratante.
     */
    private static final String SQL_OBTENER_DATOS_CONTIZACION =
        "SELECT "+ 	
		"SOLI.ID_SOLICITUD, PL.NOMBRE, PC.NOMBRE_CIA_LEG, SOLI.FECHA_CREACION, TVL.DESCRIPCION_TIPO, " +
		"MVL.DESCRIPCION_MARCA, MOV.DESCRIPCION_MODELO, MAV.ANNIO_VEHICULO, MAV.PATENTE, " +
		"CON.RUT_CONTRATANTE || ' - ' || CON.DV_CONTRATANTE, CON.NOMBRE, CON.APELLIDO_PATERNO, " +
		"SOLI.PRIMA_MENSUAL_PESOS, CON.calle || '  ' || CON.NUMERO, CL.DESCRIPCION_COMUNA, 'Chile', " + 
		"CON.FECHA_NACIMIENTO, " +     
		"trunc((to_date((to_char(sysdate,'yyyy')||'-'||to_char(sysdate,'mm')||'-'||to_char(sysdate,'dd')),'yyyy-mm-dd')-CON.FECHA_NACIMIENTO)/365) as EDAD, " +     
		"ECL.NOMBRE_ESTADO_CIVIL, case when CON.SEXO = 'M' then  'Masculino' else 'Femenino' end as SEXO, " +
		"CON.TELEFONO_1, CON.EMAIL, SUB.TITULO_SUBCATEGORIA, PS.DESCRIPCION " + 
		"FROM SOLICITUD SOLI " +
		"JOIN PLAN_LEG PL 							ON SOLI.ID_PLAN = PL.ID_PLAN " +
		"JOIN PARAMETRO_SISTEMA PS 					ON SOLI.ESTADO_SOLICITUD = PS.VALOR " +  
		"JOIN RAMA RA 								ON SOLI.ID_RAMA = RA.ID_RAMA " +
		"JOIN CONTRATANTE CON 						ON SOLI.ID_SOLICITUD = CON.ID_SOLICITUD " +
		"LEFT JOIN MATERIA_ASEGURADA_VEHICULO MAV 	ON SOLI.ID_SOLICITUD = MAV.ID_SOLICITUD " +
		"LEFT JOIN COMUNA_LEG CL 					ON CON.ID_COMUNA = CL.ID_COMUNA " +
		"LEFT JOIN ESTADO_CIVIL_LEG ECL 				ON CON.ESTADO_CIVIL = ECL.ID_ESTADO_CIVIL " + 
		"JOIN PLAN_COMPANNIA PC 						ON PL.CODIGO_PRODUCTO_LEG = PC.CODIGO_PRODUCTO_LEG " + 
		"AND PL.CODIGO_PLAN_LEG   = PC.CODIGO_PLAN_LEG " +
		"AND PL.CODIGO_SUB_TIPO_PROD_LEG  = PC.CODIGO_SUB_TIPO_PROD_LEG " + 
		"AND PL.CODIGO_TIPO_PROD_LEG = PC.CODIGO_TIPO_PROD_LEG " +
		"LEFT JOIN TIPO_VEHICULO_LEG TVL 			ON MAV.ID_TIPO_VEHICULO = TVL.ID_TIPO_VEHICULO " +
		"LEFT JOIN MARCA_VEHICULO_LEG MVL 			ON MAV.ID_MARCA_VEHICULO = MVL.ID_MARCA_VEHICULO " +
		"LEFT JOIN MODELO_VEHICULO_LEG MOV 			ON MAV.ID_MODELO_VEHICULO = MOV.ID_MODELO_VEHICULO " +
		"JOIN PRODUCTO_LEG PROL 						ON PROL.CODIGO_TIPO_PROD_LEG = PL.CODIGO_TIPO_PROD_LEG " +  
		"AND PROL.CODIGO_SUB_TIPO_PROD_LEG = PL.CODIGO_SUB_TIPO_PROD_LEG " +
		"AND PROL.CODIGO_PRODUCTO_LEG = PL.CODIGO_PRODUCTO_LEG " + 
		"JOIN PRODUCTO PRO 							ON PROL.ID_PRODUCTO = PRO.ID_PRODUCTO_LEG " +
		"JOIN SUBCATEGORIA SUB 						ON PRO.ID_SUBCATEGORIA = SUB.ID_SUBCATEGORIA " +
		"WHERE PS.GRUPO = 'ESTADO_SOLICITUD' " + 
		"AND SOLI.ESTADO_SOLICITUD != 4 " + 
		"AND PRO.ES_ACTIVO = 1";

	public List<CotizacionDTO> obtenerArchivo(Integer idSolicitud, String rut) {
		
		List<CotizacionDTO> cotizaciones = null;
		CotizacionDTO cotizacion = null;
		
		try {
			cotizaciones = new ArrayList<CotizacionDTO>();
			Connection conn = this.getConeccion();
			String sql = SQL_OBTENER_DATOS_CONTIZACION;
			sql = sql + " AND SOLI.ID_SOLICITUD > " + idSolicitud + 
			            " AND CON.RUT_CONTRATANTE != '"+ rut + 
			            "' ORDER BY SOLI.ID_SOLICITUD";
			
			Statement stmt = conn.createStatement();
		    ResultSet rset = 
		              stmt.executeQuery(sql);
		    while (rset.next()){
		    	cotizacion = new CotizacionDTO();
		    	cotizacion.setIdSolicitud(rset.getString(1));
		    	cotizacion.setNombre_plan(rset.getString(2));
		    	cotizacion.setNombre_cia(rset.getString(3));
		    	cotizacion.setFecha_creacion(rset.getString(4));
		    	cotizacion.setDescripcion_tipo(rset.getString(5));
		    	cotizacion.setDescripcion_marca(rset.getString(6));
		    	cotizacion.setDescripcion_modelo(rset.getString(7));
		    	cotizacion.setAnnio_modelo(rset.getString(8));
		    	cotizacion.setPatente(rset.getString(9));
		    	cotizacion.setRut_contratante(rset.getString(10));
		    	cotizacion.setNombre_contratante(rset.getString(11));
		    	cotizacion.setApellido_contratante(rset.getString(12));
		    	cotizacion.setPrima_mensual(rset.getString(13));
		    	cotizacion.setDireccion(rset.getString(14));
		    	cotizacion.setComuna(rset.getString(15));
		    	cotizacion.setPais(rset.getString(16));
		    	cotizacion.setFecha_nacimiento(rset.getString(17));
		    	cotizacion.setEdad(rset.getString(18));
		    	cotizacion.setEstado_civil(rset.getString(19));
		    	cotizacion.setSexo(rset.getString(20));
		    	cotizacion.setTelefono(rset.getString(21));
		    	cotizacion.setEmail(rset.getString(22));
		    	cotizacion.setSubcategoria(rset.getString(23));
		    	cotizacion.setDescripcion(rset.getString(24));
		    	cotizaciones.add(cotizacion);
		    }
		    stmt.close();
			
		} catch (SQLException e) {
			logger.error("Error al obtener los datos", e);	
		}
		
		// TODO Apéndice de método generado automáticamente
		return cotizaciones;
	}
	
	
	private Connection getConeccion(){
		
		Connection conn = null;
		FileInputStream fis;
		try {
			fis = new FileInputStream("config/config-genera-equotes.properties");
			properties = new Properties();
			properties.load(fis);
			String server = properties.getProperty("servidor");
			String user = properties.getProperty("usuario");
			String clave = properties.getProperty("password");
			Locale.setDefault(new Locale("es","ES"));
			DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());

		    conn = DriverManager.getConnection
		          (server, user, clave);
		} catch (Exception e) {
			logger.error("Error de coneccion", e);	
		}
		
		return conn;
	}

}
