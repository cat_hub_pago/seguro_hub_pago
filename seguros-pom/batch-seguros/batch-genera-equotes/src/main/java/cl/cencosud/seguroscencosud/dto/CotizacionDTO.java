package cl.cencosud.seguroscencosud.dto;

import java.io.Serializable;

public class CotizacionDTO implements Serializable{
	
	private static final long serialVersionUID = -7759884850874120793L;
	
	String idSolicitud;
	String nombre_plan;
	String nombre_cia;
	String fecha_creacion;
	String descripcion_tipo;
	String descripcion_marca;
	String descripcion_modelo;
	String annio_modelo;
	String patente;
	String rut_contratante;
	String nombre_contratante;
	String apellido_contratante;
	String prima_mensual;
	String direccion;
	String comuna;
	String pais;
	String fecha_nacimiento;
	String edad;
	String estado_civil;
	String sexo;
	String telefono;
	String email;
	String subcategoria;
	String descripcion;
	
	
	public String getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdSolicitud(String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public String getAnnio_modelo() {
		return annio_modelo;
	}
	public void setAnnio_modelo(String annio_modelo) {
		this.annio_modelo = annio_modelo;
	}
	public String getApellido_contratante() {
		return apellido_contratante;
	}
	public void setApellido_contratante(String apellido_contratante) {
		this.apellido_contratante = apellido_contratante;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getDescripcion_marca() {
		return descripcion_marca;
	}
	public void setDescripcion_marca(String descripcion_marca) {
		this.descripcion_marca = descripcion_marca;
	}
	public String getDescripcion_modelo() {
		return descripcion_modelo;
	}
	public void setDescripcion_modelo(String descripcion_modelo) {
		this.descripcion_modelo = descripcion_modelo;
	}
	public String getDescripcion_tipo() {
		return descripcion_tipo;
	}
	public void setDescripcion_tipo(String descripcion_tipo) {
		this.descripcion_tipo = descripcion_tipo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEstado_civil() {
		return estado_civil;
	}
	public void setEstado_civil(String estado_civil) {
		this.estado_civil = estado_civil;
	}
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}
	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}
	public String getNombre_cia() {
		return nombre_cia;
	}
	public void setNombre_cia(String nombre_cia) {
		this.nombre_cia = nombre_cia;
	}
	public String getNombre_contratante() {
		return nombre_contratante;
	}
	public void setNombre_contratante(String nombre_contratante) {
		this.nombre_contratante = nombre_contratante;
	}
	public String getNombre_plan() {
		return nombre_plan;
	}
	public void setNombre_plan(String nombre_plan) {
		this.nombre_plan = nombre_plan;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getPatente() {
		return patente;
	}
	public void setPatente(String patente) {
		this.patente = patente;
	}
	public String getPrima_mensual() {
		return prima_mensual;
	}
	public void setPrima_mensual(String prima_mensual) {
		this.prima_mensual = prima_mensual;
	}
	public String getRut_contratante() {
		return rut_contratante;
	}
	public void setRut_contratante(String rut_contratante) {
		this.rut_contratante = rut_contratante;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getSubcategoria() {
		return subcategoria;
	}
	public void setSubcategoria(String subcategoria) {
		this.subcategoria = subcategoria;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	

}
