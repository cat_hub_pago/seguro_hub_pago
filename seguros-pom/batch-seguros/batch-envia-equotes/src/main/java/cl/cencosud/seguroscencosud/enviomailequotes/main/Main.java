package cl.cencosud.seguroscencosud.enviomailequotes.main;

import cl.cencosud.seguroscencosud.enviomailequotes.service.EnvioMailEquotesService;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class Main {
	protected final static Logger logger = Logger.getLogger(Main.class);
	private static Properties properties;
	
	public static void main(String args[]) {
		
		
		
		try {
			logger.info("--------------------------------------------------------------");
			logger.info("Iniciando proceso de Env�o Informe Equotes por Email ");
			
			ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("EnvioMailEquotesBatchContext.xml");
			
			
			
			try {
				FileInputStream fis;
				fis = new FileInputStream("config/config-envia-equotes.properties");
				properties = new Properties();
				properties.load(fis);
				String ruta = properties.getProperty("ruta");
				EnvioMailEquotesService envioMailEquotesService = (EnvioMailEquotesService) ctx.getBean("envioMailEquotesService");				
				
				File file = new File(ruta+"e_quotes.csv");
				if (file != null){
					envioMailEquotesService.enviarCorreo(file);
					logger.info("Proceso terminado normalmente");
				}
				
			} catch (Exception e) {
				logger.error("Proceso terminado anormalmente", e);				
			}			
		} catch(Exception e) {
			logger.error("Proceso terminado anormalmente", e);			
		} finally {
			try {
			} catch (Exception e) {}
			logger.info("--------------------------------------------------------------");
		}
	}
}
