package cl.cencosud.seguroscencosud.enviomailequotes.dao;

import java.util.List;

import cl.cencosud.seguroscencosud.enviomailequotes.model.Email;

public interface EmailDAO {

	public void add(Email email);
	public void update(Email email);
	public List getList();
	
}
