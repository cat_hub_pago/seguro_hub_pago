package cl.tinet.common.captcha.servlet.util;

import java.awt.Color;
import java.awt.Font;

import com.octo.captcha.CaptchaFactory;
import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.UniColorBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomListColorGenerator;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.SimpleTextPaster;
import com.octo.captcha.component.image.wordtoimage.ComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.FileDictionary;
import com.octo.captcha.component.word.wordgenerator.DictionaryWordGenerator;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.engine.CaptchaEngine;
import com.octo.captcha.engine.GenericCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.octo.captcha.service.multitype.GenericManageableCaptchaService;

public class CaptchaServiceSingleton {

    private static ImageCaptchaService instance;

    static {
        // FIXME Par�metros del servicio. Establecer como corresponde.
        int imageWidth = 133;
        int imageHeight = 30;
        int minWordLength = 4;
        int maxWordLength = 8;
        // 10 minutos antes de que se pierda el captcha (600 segundos).
        int timeoutSegundos = 600;
        // Tama�o de buffer (palabras) simultaneas.
        int maxBuffer = 100;
        int maxBufferBeforeGC = 100;
        
        // Colores y fuentes.
        Color[] coloresFondo = { Color.WHITE };
        Color[] coloresTexto =
            { Color.BLACK };
        Font[] fonts =
            new Font[] { Font.decode("Verdana"), Font.decode("Arial") };
        int minFontSize = 20;
        int maxFontSize = 20;

        FontGenerator fg = new RandomFontGenerator(minFontSize, maxFontSize, fonts);
        BackgroundGenerator bg =
            new UniColorBackgroundGenerator(imageWidth, imageHeight,
                new RandomListColorGenerator(coloresFondo));
        SimpleTextPaster textpaster =
            new SimpleTextPaster(minWordLength, maxWordLength,
                new RandomListColorGenerator(coloresTexto), false);
        WordToImage word2image = new ComposedWordToImage(fg, bg, textpaster);

        // Diccionario con palabras incluidas en el API.
        WordGenerator dic =
            new DictionaryWordGenerator(new FileDictionary("toddlist"));
        // Diccionario aleatorio.
        //            new RandomWordGenerator(
        //                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        CaptchaFactory factory = new GimpyFactory(dic, word2image);
        CaptchaEngine engine =
            new GenericCaptchaEngine(new CaptchaFactory[] { factory });
        instance =
            new GenericManageableCaptchaService(engine, timeoutSegundos, maxBuffer,
                maxBufferBeforeGC);
    }

    public static ImageCaptchaService getInstance() {
        return instance;
    }
}
