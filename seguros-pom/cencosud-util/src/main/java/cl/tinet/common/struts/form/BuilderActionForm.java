package cl.tinet.common.struts.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.RequestUtils;

import cl.tinet.common.bean.PopulateUtil;
import cl.tinet.common.config.AbstractConfigurator;

import com.tinet.exceptions.system.SystemException;

/**
 * TODO Falta descripcion de clase BuilderActionForm.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Aug 21, 2010
 */
public abstract class BuilderActionForm extends ValidableActionForm {

    /**
     * TODO Describir atributo datos.
     */
    private Map < String, String > datos;

    /**
     * TODO Describir m�todo getDatos.
     * @return
     */
    public Map < String, String > getDatos() {
        return datos;
    }

    /**
     * TODO Describir m�todo setDatos.
     * @param datos
     */
    public void setDatos(Map < String, String > datos) {
        this.datos = datos;
    }

    /**
     * TODO Describir m�todo reset.
     * @param mapping
     * @param request
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        this.datos = new HashMap < String, String >();
    }

    /**
     * TODO Describir m�todo buildJavaBean.
     * @param <T>
     * @param request
     * @param clase
     * @return
     */
    public < T > T buildJavaBean(HttpServletRequest request, Class < T > clase) {
        return buildJavaBean(request, clase, this.datos);
    }

    /**
     * TODO Describir m�todo buildJavaBean.
     * @param <T>
     * @param request
     * @param clase
     * @param datos
     * @return
     */
    public < T > T buildJavaBean(HttpServletRequest request, Class < T > clase, Map < String, String > datos) {
        try {
            T javabean = clase.newInstance();
            PopulateUtil.populate(javabean, datos, getConfigurator(request), RequestUtils.getUserLocale(
                request, null));
            return javabean;
        } catch (InstantiationException e) {
            throw new SystemException(e);
        } catch (IllegalAccessException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo buildForm.
     * @param request
     * @param javabean
     */
    public void buildForm(HttpServletRequest request, Object javabean) {
        this.buildForm(request, javabean, this.datos);
    }

    /**
     * TODO Describir m�todo buildForm.
     * @param request
     * @param javabean
     * @param datos
     */
    public void buildForm(HttpServletRequest request, Object javabean, Map < String, String > datos) {
        PopulateUtil.describe(javabean, getConfigurator(request), RequestUtils.getUserLocale(request,
            null));
    }

    /**
     * TODO Describir m�todo getConfigurator.
     * @param request
     * @return 
     */
    public abstract AbstractConfigurator getConfigurator(HttpServletRequest request);

}